package org.cocos2dx.cpp;

import android.app.Activity;
import android.util.Log;

import com.chartboost.sdk.*;
import com.chartboost.sdk.Libraries.CBLogging.Level;
import com.chartboost.sdk.Model.CBError.CBImpressionError;


public class ChartBoost
{
	private static Activity app;
	private static String appId;
	private static String appSig;
	static ChartBoost me;
	
	private ChartboostDelegate delegate = new ChartboostDelegate() {
	    public void didCacheInterstitial(String location)
	    {
	    	Log.d("ChartBoost", "didCacheInterstitial");
	    }
	    public void didFailToLoadInterstitial(String location, CBImpressionError error)
	    {
	    	Log.d("ChartBoost", "didFailToLoadInterstitial");
	    }
	};

	public ChartBoost(Activity application, String id, String sig) {
        app = application;
        appId = id;
        appSig = sig;
        me = this;
        
        Chartboost.startWithAppId(app, appId, appSig);
		Chartboost.setLoggingLevel(Level.ALL);
        Chartboost.setDelegate(delegate);
        Chartboost.onCreate(app);
    }
    
    public void onDestroy() {
    	if( me == null ) return;
    	
       Chartboost.onDestroy(app);
    }
	
    public void onPause() {
    	if( me == null ) return;
    	
	    Chartboost.onPause(app);
	}
	
    public void onResume() {
    	if( me == null ) return;
    	
	    Chartboost.onResume(app);
	}

    public void onStart()
	{
    	if( me == null ) return;
    	
	    Chartboost.onStart(app);
	    Chartboost.cacheInterstitial(CBLocation.LOCATION_DEFAULT);
	}

    public void onStop()
	{
    	if( me == null ) return;
    	
	    Chartboost.onStop(app);
	}
	
	public boolean onBackPressed() {
    	if( me == null ) return false;
    	
	    return Chartboost.onBackPressed();
	}

    static public void showInterstitial()
    {
    	if( me == null ) return;
    	
       	Chartboost.showInterstitial(CBLocation.LOCATION_DEFAULT);
    }

    static public void showMoreApps(String arg)
    {
    	if( me == null ) return;
    	
       	Chartboost.showMoreApps(CBLocation.LOCATION_DEFAULT);
    }

    static public boolean interstitialAvailabled()
    {
		if( me ==null )return false;
		boolean result = Chartboost.hasInterstitial(CBLocation.LOCATION_DEFAULT);
       	return result;
    }
}
