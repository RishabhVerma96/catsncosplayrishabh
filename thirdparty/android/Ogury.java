package com.stereo7.extensions;

import android.app.Activity;
import android.util.Log;
import io.presage.Presage;
import io.presage.utils.IADHandler;

public class Ogury
{
	private static Activity app;
	private static Ogury me;

	public Ogury(Activity application) {
        app = application;
        me = this;
        
        Presage.getInstance().setContext(app);
        Presage.getInstance().start();
    }
    
    static public boolean showInterstitial()
    {
    	if( me == null )return false;
    	
    	Ogury.app.runOnUiThread(new Runnable() { @Override public void run() {
	        Presage.getInstance().adToServe("interstitial", new IADHandler() {
	            
	            @Override
	            public void onAdNotFound() {
	            	Ogury.app.runOnUiThread(new Runnable() { @Override public void run() {
	                	me.nativeAnswer(false);
	                } });    	
	            }
	            
	            @Override
	            public void onAdFound() {
	            	Ogury.app.runOnUiThread(new Runnable() { @Override public void run() {
	                	me.nativeAnswer(true);
	                } });    	
	            }
	            
	            @Override
	            public void onAdClosed() {
	                Log.i("PRESAGE", "ad closed");
	            }
	        });
        } });    

        return true;
    }
    
    native public void nativeAnswer( boolean result );
}
