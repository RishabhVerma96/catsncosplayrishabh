package com.stereo7.extensions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

@SuppressLint("HandlerLeak") public class AdMob {

	public static AdMob me;
	public static Activity app;
	private static AdView adView;
	private static InterstitialAd interstitial;
	private static Handler handler;
	private static final int SHOW_Interstitial = 1;

	private static String appID;

	public AdMob(Activity application, String ID) {
		app = application;
		me = this;
		appID = ID;
		
		//load baner
		/*
		adView = new AdView(app);
		adView.setAdUnitId( ID);
		adView.setAdSize(AdSize.BANNER);

		RelativeLayout lContainerLayout = new RelativeLayout(app);
		lContainerLayout.setLayoutParams(new RelativeLayout.LayoutParams( LayoutParams.MATCH_PARENT , LayoutParams.MATCH_PARENT ));
		
		RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams( LayoutParams.WRAP_CONTENT , LayoutParams.WRAP_CONTENT );
		adParams.topMargin = 0;
		adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);
		app.addContentView(adView, adParams);
		*/
		
		//initialization interstitial ad
	    interstitial = new InterstitialAd(app);
	    interstitial.setAdUnitId(appID);
	    // Create ad request.
	    AdRequest adRequest = new AdRequest.Builder().build();
	    // Begin loading your interstitial.
	    interstitial.loadAd(adRequest);
	    
	    interstitial.setAdListener(new AdListener() {
	        @Override
	        public void onAdClosed() {
	          AdRequest adRequest = new AdRequest.Builder().addTestDevice(appID).build();
	          interstitial.loadAd(adRequest);
	        }
	    });
		
		handler = new Handler()
        {
  	      public void handleMessage(android.os.Message msg) {
  	        switch (msg.what) {
	        	case SHOW_Interstitial:
	        		if( interstitial != null )
	        		{
	        			if( interstitial.isLoaded() ) {
	        				interstitial.show();
	        			}
	        		}
	        		break;
  	        }
  	      };
  	    };


	}
	
	public static void displayInterstitial(String args) {
    	if( me == null ) return;
    	
		handler.sendEmptyMessage(SHOW_Interstitial);
	}


	public void onPause() {
    	if( me == null ) return;
		if( adView != null )
			adView.pause();
	}
	public void onResume() {
    	if( me == null ) return;
		if( adView != null )
			adView.resume();
	}
	public void onDestroy() {
    	if( me == null ) return;
		if( adView != null )
			adView.destroy();
	}
}
