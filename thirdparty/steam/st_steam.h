#ifndef __Steam_Steam_h__
#define __Steam_Steam_h__
#include "steam_api.h"

#define NS_STEAM_BEGIN namespace steam{
#define NS_STEAM_END }
#define USING_NS_STEAM using namespace steam 

#endif