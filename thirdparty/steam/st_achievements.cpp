#if STEAMBUILD == 1

#include "st_achievements.h"
#include "cocos2d.h"

#pragma comment ( lib, "sdkencryptedappticket.lib" )
#pragma comment ( lib, "steam_api.lib" )

NS_STEAM_BEGIN;

CSteamAchievements::CSteamAchievements( Achievement_t *Achievements, int NumAchievements ) :
m_iAppID( 0 ),
m_bInitialized( false ),
m_pSteamUser( NULL ),
m_CallbackUserStatsReceived( this, &CSteamAchievements::OnUserStatsReceived ),
m_CallbackUserStatsStored( this, &CSteamAchievements::OnUserStatsStored ),
m_CallbackAchievementStored( this, &CSteamAchievements::OnAchievementStored )
{
	m_pSteamUser = SteamUser();
	m_iAppID = SteamUtils()->GetAppID();
	m_pAchievements = Achievements;
	m_iNumAchievements = NumAchievements;
	RequestStats();
}

CSteamAchievements::~CSteamAchievements()
{

}

bool CSteamAchievements::RequestStats()
{
	// Is Steam loaded? If not we can't get stats.
	if( NULL == SteamUserStats() || NULL == SteamUser() )
	{
		return false;
	}
	// Is the user logged on?  If not we can't get stats.
	if( !SteamUser() || !SteamUser()->BLoggedOn() )
	{
		return false;
	}
	// Request user stats.
	return SteamUserStats() ? SteamUserStats()->RequestCurrentStats() : false;
}

bool CSteamAchievements::SetAchievement( const char* ID )
{
	// Have we received a call back from Steam yet?
	if( m_bInitialized && SteamUserStats() )
	{
		SteamUserStats()->SetAchievement( ID );
		return SteamUserStats()->StoreStats();
	}
	// If not then we can't set achievements yet
	return false;
}

bool CSteamAchievements::ClearAchievement( const char* ID )
{
	if( m_bInitialized && SteamUserStats() )
	{
		SteamUserStats()->ClearAchievement( ID );
		return SteamUserStats()->StoreStats();
	}
	return false;
}

void CSteamAchievements::OnUserStatsReceived( UserStatsReceived_t *pCallback )
{
	// we may get callbacks for other games' stats arriving, ignore them
	if( !pCallback ) return;
	if( !SteamUserStats() )return;

	if( m_iAppID == pCallback->m_nGameID )
	{
		if( k_EResultOK == pCallback->m_eResult )
		{
			OutputDebugStringA( "Received stats and achievements from Steam\n" );
			m_bInitialized = true;

			// load achievements
			for( int iAch = 0; iAch < m_iNumAchievements; ++iAch )
			{
				Achievement_t &ach = m_pAchievements[iAch];

				SteamUserStats()->GetAchievement( ach.m_pchAchievementID, &ach.m_bAchieved );
				_snprintf( ach.m_rgchName, sizeof( ach.m_rgchName ), "%s",
					SteamUserStats()->GetAchievementDisplayAttribute( ach.m_pchAchievementID,
					"name" ) );
				_snprintf( ach.m_rgchDescription, sizeof( ach.m_rgchDescription ), "%s",
					SteamUserStats()->GetAchievementDisplayAttribute( ach.m_pchAchievementID,
					"desc" ) );
			}
		}
		else
		{
			char buffer[128];
			_snprintf( buffer, 128, "RequestStats - failed, %d\n", pCallback->m_eResult );
			OutputDebugStringA( buffer );
		}
	}
}

void CSteamAchievements::OnUserStatsStored( UserStatsStored_t *pCallback )
{
	if( !pCallback ) return;
	// we may get callbacks for other games' stats arriving, ignore them
	if( m_iAppID == pCallback->m_nGameID )
	{
		if( k_EResultOK == pCallback->m_eResult )
		{
			OutputDebugStringA( "Stored stats for Steam\n" );
		}
		else
		{
			char buffer[128];
			_snprintf( buffer, 128, "StatsStored - failed, %d\n", pCallback->m_eResult );
			OutputDebugStringA( buffer );
		}
	}
}

void CSteamAchievements::OnAchievementStored( UserAchievementStored_t *pCallback )
{
	if( !pCallback ) return;
	// we may get callbacks for other games' stats arriving, ignore them
	if( m_iAppID == pCallback->m_nGameID )
	{
		OutputDebugStringA( "Stored Achievement for Steam\n" );
	}
}

NS_STEAM_END;
#endif