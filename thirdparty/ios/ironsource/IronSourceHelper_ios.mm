
#import "AppController.h"
#include <string>

namespace ironsource
{
    void initIronSource()
    {
        [AppController initialiseIronSource];
    }
    
    void showOfferWall()
    {
        [AppController showOfferWallOfIronSource];
    }
}
