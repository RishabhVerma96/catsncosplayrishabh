#ifndef IslandDefense_IronSourceHelper_h
#define IslandDefense_IronSourceHelper_h

#include "ml/ParamCollection.h"
namespace ironsource
{
    void initIronSource();
    void showOfferWall();
}

#endif /* IronSourceHelper_h */
