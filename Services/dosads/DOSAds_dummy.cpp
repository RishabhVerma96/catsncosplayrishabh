
#include "DOSAds.h"
#include <string>
#include <thread>
#include "../plugins/AdsPlugin.h"

namespace supersonic
{
    void showInterstitial(){}
	void playAd()
	{
		AdsPlugin::shared().onVideoStarted();
		AdsPlugin::shared().onVideoFinihed( AdsPlugin::Result::Ok, 0 );
	}

	bool isVideoAvailabled()
	{
		return true;
	}

    void showOfferwall(){}
}

namespace ogury
{
	bool showInterstitial()
	{
		static bool first = true;
		if( first )
		{
			first = false;
			std::thread th( [](){
				std::this_thread::sleep_for( std::chrono::milliseconds( 1000 ) );
				AdsPlugin::shared().oguryAnswer( true );
			} );
			th.detach();
		}
		else
		{
			std::thread th( [](){
				std::this_thread::sleep_for( std::chrono::milliseconds( 1000 ) );
				AdsPlugin::shared().oguryAnswer( false );
			} );
			th.detach();
		}
		return true;
	}


}
