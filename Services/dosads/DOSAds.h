
#ifndef __IslandDefense__DOSAds__
#define __IslandDefense__DOSAds__
#include <functional>

namespace supersonic
{
    void showInterstitial();
	void playAd();
	bool isVideoAvailabled();
	void showOfferwall();
}

namespace ogury
{
	bool showInterstitial();
}

#endif
