
#include "DOSAds.h"
#include "cocos2d.h"
#include "ScoreCounter.h"
#include "../plugins/AdsPlugin.h"
#include "ml/JniBind.h"

USING_NS_CC;

#define JAVA(method)JNIEXPORT void JNICALL  Java_com_stereo7_supersonic_Supersonic_##method(JNIEnv*  env, jobject thiz, int result)
#define JAVA_OGURY(method)JNIEXPORT void JNICALL  Java_com_stereo7_extensions_Ogury_##method(JNIEnv*  env, jobject thiz, bool result)

namespace {
    static const char * SonicActivity = "com.stereo7.supersonic/Supersonic";
}

namespace supersonic
{
    void onMoneyReward(bool result);
}

extern
"C"
{
	JAVA(nativeSonicReward) {
		int money = result;
		log("nativeSonicReward");
		supersonic::onMoneyReward( true );
	}
	JAVA(nativeSonicRewardOfferwall) {
		int money = result;
		ScoreCounter::shared().addMoney( kScoreCrystals, money, true );
	}

	JAVA_OGURY(nativeAnswer) {
		AdsPlugin::shared().oguryAnswer( result );
	}
}



namespace supersonic
{
    void showInterstitial()
    {
        JniMethodInfo t;
        const char * Activity = SonicActivity;
        if( JniHelper::getStaticMethodInfo( t, Activity, "showInterstitial", "()V" ) )
        {
            t.env->CallStaticVoidMethod( t.classID, t.methodID );
            t.env->DeleteLocalRef( t.classID );
        }
        else
        {
            log( "supersonic::showInterstitial::jni showInterstitial notfound" );
        }
    }
    void playAd()
    {
        JniMethodInfo t;
        const char * Activity = SonicActivity;
        if( JniHelper::getStaticMethodInfo( t, Activity, "playAd", "()V" ) )
        {
            t.env->CallStaticVoidMethod( t.classID, t.methodID );
            t.env->DeleteLocalRef( t.classID );
        }
        else
        {
        	AdsPlugin::shared().onVideoFinihed(AdsPlugin::Result::Fail, 0);
        }
    }
    void showOfferwall()
    {
    	JniMethodInfo t;
		const char * Activity = SonicActivity;
		if( JniHelper::getStaticMethodInfo( t, Activity, "showOfferwall", "()V" ) )
		{
			t.env->CallStaticVoidMethod( t.classID, t.methodID );
			t.env->DeleteLocalRef( t.classID );
		}
		else
		{
			log( "supersonic::showOfferwall::jni showOfferwall notfounded" );
		}
    }
    void onMoneyReward(bool result)
    {
    	log("onMoneyReward(bool result)");
		AdsPlugin::shared().onVideoFinihed( AdsPlugin::Result::Ok, 0 );
    }

    bool isVideoAvailabled()
    {
    	bool result(false);
    	JniMethodInfo t;
		const char * Activity = SonicActivity;
		if( JniHelper::getStaticMethodInfo( t, Activity, "isVideoAvailabled", "()Z" ) )
		{
			result = t.env->CallStaticBooleanMethod( t.classID, t.methodID );
			t.env->DeleteLocalRef( t.classID );
		}
		else
		{
			log( "supersonic::showOfferwall::jni showOfferwall notfounded" );
		}
		return result;
    }
}

namespace ogury
{
    bool showInterstitial()
    {
    	JavaBind bind( "com.stereo7.extensions", "Ogury", "showInterstitial", "" );
    	return bind.bool_call();
    }
}


#undef JAVA
