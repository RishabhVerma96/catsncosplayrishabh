
#include "DOSAds.h"
#include "AppController.h"
#include <string>
#include "ScoreCounter.h"
#include "../plugins/AdsPlugin.h"

namespace supersonic
{
	void offerwallReward( unsigned int reward )
	{
		cocos2d::ScoreCounter::shared().addMoney( cocos2d::kScoreCrystals, reward, true );
	}
	
    void showInterstitial()
	{
		[[AppController getInstance] supersonicShowInterstitial];
	}
	
    void playAd()
	{
		[AppController getInstance].supersonicCallback = std::bind( [](bool result)
		{
			AdsPlugin::shared().onVideoFinihed(result? AdsPlugin::Result::Ok : AdsPlugin::Result::Fail, 0);
		}, std::placeholders::_1);
		[[AppController getInstance] supersonicShowVideo];
	}
    void showOfferwall()
	{
		[AppController getInstance].supersonicCallbackOfferWall = std::bind( offerwallReward, std::placeholders::_1 );
		[[AppController getInstance] supersonicOfferWall];
	}
    bool isVideoAvailabled()
	{
		return [[AppController getInstance] supersonicIsVideoAvailabled];
	}
}

namespace ogury
{
    bool showInterstitial()
	{
		return false;
	}
}
