//
//  flurry.h
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 08.10.14.
//
//

#ifndef __IslandDefense__flurry__
#define __IslandDefense__flurry__

#include "ml/ParamCollection.h"

namespace flurry
{
	void logEvent( const ParamCollection & params );
}

#endif /* defined(__IslandDefense__flurry__) */
