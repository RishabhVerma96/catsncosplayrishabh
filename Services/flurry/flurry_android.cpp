//
//  flurry.cpp
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 08.10.14.
//
//

#include "flurry_.h"
#include "cocos2d.h"
#include "configuration.h"
#include "support.h"
#include "ml/common.h"
#include "ml/JniBind.h"

#define package "com.stereo7.flurry"


USING_NS_CC;

namespace flurry
{
	void logEvent( const ParamCollection & params )
	{
		if( !Config::shared().get<bool>( "useStatistic" ) )
			return;
		if( !(isTestDevice() && isTestModeActive()) )
		{
			JavaBind bind( package, "Flurry", "logEvent", "%s" );
			bind.call( params.string() );
		}
	}
}
