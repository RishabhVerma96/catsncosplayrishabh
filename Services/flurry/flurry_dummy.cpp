//
//  flurry_dummy.cpp
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 08.10.14.
//
//

#include "flurry_.h"

namespace flurry
{
	void logEvent( const ParamCollection & params )
	{
		/*
		Flurry can support max 10 parameters in one event
		*/
		assert( params.size() < 10 );
	}
}
