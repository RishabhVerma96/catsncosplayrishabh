//
//  Flurry_ios.m
//  IslandDefense
//
//  Created by Владимир Толмачев on 06.12.14.
//
//

#import <Foundation/Foundation.h>
#import "AppController.h"
#include "flurry_.h"
#include "ml/common.h"
#include "support.h"

namespace flurry
{
	void logEvent( const ParamCollection & params )
	{
		if( !(cocos2d::isTestDevice() && cocos2d::isTestModeActive()) )
		{
			[AppController flurryEvent:params];
		}
	}
}