#include "fyber.h"
#include "cocos2d.h"
#include "ml/JniBind.h"
#include "../plugins/AdsPlugin.h"

USING_NS_CC;

#define JAVA(method)JNIEXPORT void JNICALL  Java_org_cocos2dx_cpp_FyberDelegate_##method

namespace fyber
{
	bool videoAvailabled(true);
	IntrusivePtr<Node> dummy;
	const std::string scheduleKey = "fyber_update";
	struct
	{
		std::mutex mutex;
		bool isFinished;
		bool resultFinished;
		bool isStarted;
		bool resultStarted;
		bool noAds;
		void clear()
		{
			isFinished = false;
			isStarted = false;
			noAds = false;
		}
	}fyberParams;

	void update( float dt )
	{
		fyberParams.mutex.lock();

		if( fyberParams.noAds )
		{
			fyberParams.noAds = false;
			AdsPlugin::shared().onVideoNoOffers();
		}
		if( fyberParams.isStarted )
		{
			fyberParams.isStarted = false;
			AdsPlugin::shared().onVideoStarted();
		}
		if( fyberParams.isFinished )
		{
			fyberParams.isFinished = false;
			auto arg = fyberParams.resultFinished ? AdsPlugin::Result::Ok : AdsPlugin::Result::Fail;
			AdsPlugin::shared().onVideoFinihed( arg, 0 );

			auto scheduler = Director::getInstance()->getScheduler();
			scheduler->unschedule( scheduleKey, dummy.ptr() );
		}

		fyberParams.mutex.unlock();
	}


	void playAd()
	{
		if( !dummy ) dummy = Node::create();
		fyberParams.clear();
		auto scheduler = Director::getInstance()->getScheduler();
		scheduler->schedule( std::bind( fyber::update, std::placeholders::_1 ), dummy.ptr(), 0, false, scheduleKey );

		JavaBind bind( "org.cocos2dx.cpp", "FyberDelegate", "playAd", "" );
		bind.call();
	}

	bool isVideoAvailabled()
	{
		return videoAvailabled;
	}

	void showInterstitial()
	{
		JavaBind bind( "org.cocos2dx.cpp", "FyberDelegate", "showInterstitial", "" );
		bind.call();
	}

	void cacheInterstitial()
	{
		//JavaBind bind( "org.cocos2dx.cpp, "FyberDelegate", "cacheInterstitial", "" );
		//bind.call();
	}

	void cacheVideo()
	{
		//JavaBind bind( "org.cocos2dx.cpp", "FyberDelegate", "cacheVideo", "" );
		//bind.call();
	}

}

extern
"C"
{
	JAVA(nativeStart)(JNIEnv*  env, jobject thiz, bool success) {
		fyber::fyberParams.mutex.lock();
		fyber::fyberParams.isStarted = true;
		fyber::fyberParams.mutex.unlock();	}

	JAVA(nativeFinished)(JNIEnv*  env, jobject thiz, bool success) {
		fyber::fyberParams.mutex.lock();
		fyber::fyberParams.isFinished = true;
		fyber::fyberParams.resultFinished = success;
		fyber::fyberParams.mutex.unlock();
	}

	JAVA(nativeNoOffers)(JNIEnv*  env, jobject thiz) {
		fyber::fyberParams.mutex.lock();
		fyber::fyberParams.noAds = true;
		fyber::fyberParams.mutex.unlock();
	}
}
