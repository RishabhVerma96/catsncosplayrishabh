#include "fyber.h"
#include "../plugins/AdsPlugin.h"
#include "cocos2d.h"

USING_NS_CC;

namespace fyber
{
	IntrusivePtr<Node> dummy;
	const std::string scheduleKey = "fyber_update";
	struct
	{
		std::mutex mutex;
		bool isFinished;
		bool resultFinished;
		bool isStarted;
		bool resultStarted;
		bool noAds;
		void clear()
		{
			isFinished = false;
			isStarted = false;
			noAds = false;
		}
	}fyberParams;

	void update( float dt )
	{
		fyberParams.mutex.lock();

		if( fyberParams.noAds )
		{
			fyberParams.noAds = false;
			AdsPlugin::shared().onVideoFinihed( AdsPlugin::Result::Fail, 0 );
		}
		if( fyberParams.isStarted )
		{
			fyberParams.isStarted = false;
			AdsPlugin::shared().onVideoStarted();
		}
		if( fyberParams.isFinished )
		{
			fyberParams.isFinished = false;
			auto arg = fyberParams.resultFinished ? AdsPlugin::Result::Ok : AdsPlugin::Result::Fail;
			AdsPlugin::shared().onVideoFinihed( arg, 0 );

			auto scheduler = Director::getInstance()->getScheduler();
			scheduler->unschedule( scheduleKey, dummy.ptr() );
		}

		fyberParams.mutex.unlock();
	}

	void showInterstitial() {}
	void playAd()
	{
		if( !dummy ) dummy = Node::create();
		fyberParams.clear();
		auto scheduler = Director::getInstance()->getScheduler();
		scheduler->schedule( std::bind( fyber::update, std::placeholders::_1 ), dummy.ptr(), 0, false, scheduleKey );

		auto f = []()
		{
			fyberParams.mutex.lock();
			fyberParams.isStarted = true;
			fyberParams.mutex.unlock();

			std::this_thread::sleep_for( std::chrono::milliseconds( 1000 ) );

			fyberParams.mutex.lock();
			fyberParams.isFinished = true;
			fyberParams.resultFinished = true;
			fyberParams.mutex.unlock();
		};
		std::thread th( f );
		th.detach();

	}

	bool isVideoAvailabled()
	{
		return true;
	}

	void showOfferwall() {}

	void cacheInterstitial() {}
	void cacheVideo() {}
}
