#ifndef __FYBER_H__
#define __FYBER_H__
#include <functional>

namespace fyber
{
	void playAd();
	bool isVideoAvailabled();
	void showInterstitial();
	void cacheInterstitial();
	void cacheVideo();
}


#endif
