//
//  fyber_ios.c
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 13.08.15.
//
//

#include "fyber.h"
#include "AppController.h"
#include "AdsPlugin.h"

namespace fyber
{
	void started(bool result)
	{
		if( result )
			AdsPlugin::shared().onVideoStarted();
	}
	
	void finished(bool result)
	{
		AdsPlugin::shared().onVideoFinihed( result ? AdsPlugin::Result::Ok : AdsPlugin::Result::Fail, 0 );
	}
	
	void playAd()
	{
		[AppController getInstance].fyberStarted = std::bind( started, std::placeholders::_1);
		[AppController getInstance].fyberFinished = std::bind( finished, std::placeholders::_1);
		[AppController fyberPlayVideo];
	}
	
	bool isVideoAvailabled()
	{
		return true;
	}
	
	void showInterstitial()
	{
		[AppController fyberInterstitial];
	}
	
	void cacheInterstitial()
	{
	}
	
	void cacheVideo()
	{
	}
	
}