/****************************************************************************
Copyright (c) 2014-2015 Vladimir Tolmachev (Volodar)

This file is part of code base ML

ML is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ML is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ML.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#include "cocos2d.h"
#include "playservices.h"
#include "UserData.h"
#include "ml/MainThreadFunc.h"

USING_NS_CC;

namespace PlayServises
{
	ObServer<cocos2d::Ref, std::function<void()>> OnConnected;
	ObServer<cocos2d::Ref, std::function<void()>> OnConnectFailed;

	bool _isInit( false );

	void init( bool showLoginWindow )
	{

		auto connect = []()
		{
			_isInit = true;
			MainThreadFunc::shared().push_back( [](){ setUserDisplayName( "John Doe" ); } );
			MainThreadFunc::shared().push_back( [](){ setUserNickname( "DuckNukem" ); } );
			MainThreadFunc::shared().push_back( [](){ setUserId( "12312312312313" ); } );
			MainThreadFunc::shared().push_back( [](){ OnConnected.pushevent(); } );
		};
		auto fail = []()
		{
			OnConnectFailed.pushevent();
		};
		std::thread thread( connect );
		thread.detach();
	}


	bool isConnected()
	{
		return _isInit;
	}

	namespace Leaderboard
	{
		void record( const std::string& leaderBoardID, int value )
		{
			if( _isInit )
			{
				auto id = "PlayServises_Leaderboard_record" + leaderBoardID;
				int max = UserData::shared().get <int>( id );
				max = std::max( max, value );
				UserData::shared().write( id, max );
			}
		}

		void open( const std::string& leaderBoardID )
		{
			if( _isInit )
			{
				auto id = "PlayServises_Leaderboard_record" + leaderBoardID;
				auto value = UserData::shared().get( id, std::string( "0" ) );
				cocos2d::MessageBox( ("Current record: " + value).c_str(), leaderBoardID.c_str() );
			}
		}
	}
	namespace CloudUserData
	{
		std::function<void( const std::string& )> cloudNotify;
		std::function<void( bool )> UIAlertNotify;
		void load() {}
		void save( const std::string& ) {}
		void showAlertDialog() {}
	}
}
