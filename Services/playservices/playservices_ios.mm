#include "cocos2d.h"
#include "playservices.h"
#include "UserData.h"

#import <GameKit/GameKit.h>

USING_NS_CC;

@interface GameCenterHelper : UIViewController <GKGameCenterControllerDelegate>
-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController;
@end

@implementation GameCenterHelper
-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController;
{
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}
@end



namespace PlayServises
{
    bool initialized = false;
    GameCenterHelper * _gameCenterHelper = nil;
    UIViewController * _mainVC;
    UIViewController * _gameCenterViewController;
    ObServer<cocos2d::Ref, std::function<void()>> OnConnected;
    ObServer<cocos2d::Ref, std::function<void()>> OnConnectFailed;

	void init( bool showLoginWindow )
	{
        if( showLoginWindow && _gameCenterViewController )
        {
            [_mainVC presentViewController:_gameCenterViewController animated:YES completion:nil];
            _gameCenterViewController = nil;
            return;
        }
        if( _gameCenterHelper == nil && initialized == false )
        {
            GKLocalPlayer * const localPlayer = [GKLocalPlayer localPlayer];
            
            localPlayer.authenticateHandler = ^(UIViewController * gkvc, NSError * error)
            {
                if(gkvc)
                {
                    _gameCenterViewController=gkvc;
                    if( showLoginWindow )
                    {
                        [_mainVC presentViewController:gkvc animated:YES completion:nil];
                        _gameCenterViewController = nil;
                    }
                }
                else if (error)
                {
                    NSLog(@"Game Center authentication error: %@", error);
                    OnConnectFailed.pushevent();
                }
                else
                {
                    initialized = true;
                    setUserId( [[localPlayer playerID]UTF8String] );
                    setUserNickname( [[localPlayer alias]UTF8String] );
                    setUserDisplayName( [[localPlayer alias]UTF8String] );
                    OnConnected.pushevent();
                }
            };
            _gameCenterHelper = [[GameCenterHelper alloc] init];
            _mainVC = [UIApplication sharedApplication].keyWindow.rootViewController;
        }
        else if( !initialized )
        {
            OnConnectFailed.pushevent();
        }
	}

	bool isConnected()
	{
		return [GKLocalPlayer localPlayer].authenticated;
	}

	namespace Leaderboard
	{
		void record( const std::string & leaderBoardID, int value )
		{
            if( !isConnected() || leaderBoardID.empty() )
                return;
            
            GKScore * const gkScore = [[GKScore alloc] initWithLeaderboardIdentifier:[NSString stringWithUTF8String:leaderBoardID.c_str()]];
            
            gkScore.value = value;
            
            [GKScore reportScores:@[gkScore] withCompletionHandler: nil];
		}

		void open( const std::string & leaderBoardID )
		{
            GKGameCenterViewController * const gcViewController = [[GKGameCenterViewController alloc] init];
            
            gcViewController.gameCenterDelegate = _gameCenterHelper;
            
            gcViewController.viewState = GKGameCenterViewControllerStateLeaderboards;
            if( leaderBoardID == "all" )
                gcViewController.leaderboardIdentifier = nil;
            else
                gcViewController.leaderboardIdentifier = [NSString stringWithUTF8String:leaderBoardID.c_str()];
            
            [_mainVC presentViewController:gcViewController animated:YES completion:nil];
		}
	}
}
