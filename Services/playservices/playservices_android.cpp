#include "ml/JniBind.h"
#include "cocos2d.h"
#include "playservices.h"
#include "ml/MainThreadFunc.h"

USING_NS_CC;

#define METHOD(name)  Java_com_google_example_games_basegameutils_PlayServises_##name
#define package "com.google.example.games.basegameutils"

namespace PlayServises
{
	static bool _isInitialised = false;
	static bool _isConnected = false;
	ObServer<cocos2d::Ref, std::function<void()>> OnConnected;
	ObServer<cocos2d::Ref, std::function<void()>> OnConnectFailed;

	void init( bool showLoginWindow )
	{
		if( _isInitialised )
			return;
		JavaBind bind( package, "PlayServises", "connect", "" );
		bind.call();
		_isInitialised = true;
	}

	bool isConnected()
	{
		return _isConnected;
	}

	namespace Leaderboard
	{
		void record( const std::string& leaderBoardID, int value )
		{
			if( !_isConnected )
				return;
			if( leaderBoardID.empty() ) 
				return;
			JavaBind bind( package, "PlayServises", "leaderboardRecord", "%s%d" );
			bind.call( leaderBoardID, value );
		}
		void open( const std::string& leaderBoardID )
		{
			if( !_isConnected )
				return;
			if( leaderBoardID.empty() ) 
				return;
			JavaBind bind( package, "PlayServises", "leaderboardOpen", "%s" );
			bind.call( leaderBoardID );
		}
	}

	namespace Achievements
	{
		void unlock( const std::string& id)
		{
			if( !_isInitialised )
				return;
			JavaBind bind( package, "PlayServises", "achievementsUnlock", "%s" );
			bind.call( id );
		}
		void increment( const std::string& id, int value )
		{
			if( !_isInitialised )
				return;
			JavaBind bind( package, "PlayServises", "achievementsIncrement", "%s%d" );
			bind.call( id, value );
		}
		void open()
		{
			if( !_isInitialised )
				return;
			JavaBind bind( package, "PlayServises", "achievementsOpen", "" );
			bind.call();
		}
	}

	namespace CloudUserData
	{
	    std::function<void( const std::string& )> cloudNotify;
        std::function<void( bool )> UIAlertNotify;

		void load()
		{
			if( !_isConnected )
				return;
			cocos2d::log( "CloudUserData::load()" );
			JavaBind bind( package, "PlayServises", "loadSnapshot", "" );
			bind.call();

		}
		void save(const std::string& data)
		{
			if( !_isConnected )
				return;
			cocos2d::log( "CloudUserData::save()" );
			cocos2d::log( "data = %s", data.c_str() );
			JavaBind bind( package, "PlayServises", "saveSnapshot", "%s" );
			bind.call( data );
		}
		void showAlertDialog()
		{
			if( !_isConnected )
				return;
			cocos2d::log( "CloudUserData::showAlertDialog()" );
			JavaBind bind( package, "PlayServises", "showCloudAlertDialog", "" );
			bind.call();
		}
	}
}

extern
"C"
{
	JNIEXPORT void JNICALL  METHOD(playConnected)(JNIEnv*  env, jobject thiz )
	{
		RUIN_MAINTHREAD_BEGIN
		{
			PlayServises::_isConnected = true;
			//cocos2d::MessageBox( "playConnected", "PlayServises" );
			cocos2d::log( "JNICALL::playConnected()" );
			PlayServises::OnConnected.pushevent();
		}
		RUIN_MAINTHREAD_END
	}

	JNIEXPORT void JNICALL  METHOD(playConnectFailed)(JNIEnv*  env, jobject thiz )
	{
		RUIN_MAINTHREAD_BEGIN
		{
			//cocos2d::MessageBox( "playConnectFailed", "PlayServises" );
			PlayServises::_isConnected = false;
			PlayServises::_isInitialised = false;
			PlayServises::OnConnectFailed.pushevent();
		}
		RUIN_MAINTHREAD_END
	}

	JNIEXPORT void JNICALL  METHOD(playConnectionSuspended)(JNIEnv*  env, jobject thiz )
	{
		//cocos2d::MessageBox("playConnectionSuspended", "PlayServises");
	}

	JNIEXPORT void JNICALL  METHOD(activityConnectionFailed)(JNIEnv*  env, jobject thiz )
	{
		//cocos2d::MessageBox( "activityConnectionFailed", "PlayServises" );
	}

	JNIEXPORT void JNICALL  METHOD(activityConnectionSuspended)(JNIEnv*  env, jobject thiz )
	{
		//cocos2d::MessageBox("activityConnectionSuspended", "PlayServises");
	}
	
	JNIEXPORT void JNICALL  METHOD(userDisplayName)(JNIEnv*  env, jobject thiz, jstring results) {
		std::string str = JniHelper::jstring2string(results);
		//cocos2d::MessageBox( str.c_str(), "userDisplayName" );
		MainThreadFunc::shared().push_back( std::bind( [](const std::string& str){
			PlayServises::setUserDisplayName( str );
		}, str ) );
	}
	JNIEXPORT void JNICALL  METHOD(userNickname)(JNIEnv*  env, jobject thiz, jstring results) {
		std::string str = JniHelper::jstring2string(results);
		//cocos2d::MessageBox( str.c_str(), "userNickname" );
		MainThreadFunc::shared().push_back( std::bind( [](const std::string& str){
			PlayServises::setUserNickname( str );
		}, str ) );
	}
	JNIEXPORT void JNICALL  METHOD(userId)(JNIEnv*  env, jobject thiz, jstring results) {
		std::string str = JniHelper::jstring2string(results);
		//cocos2d::MessageBox( str.c_str(), "userId" );
		MainThreadFunc::shared().push_back( std::bind( [](const std::string& str){
			PlayServises::setUserId( str );
		}, str ) );
	}

	JNIEXPORT void JNICALL  METHOD( userDataLoad )( JNIEnv*  env, jobject thiz, jstring results )
	{
		std::string str = JniHelper::jstring2string( results );
		cocos2d::log( "JNICALL::userDataLoad()" );
		cocos2d::log( "str = %s", str.c_str() );
		MainThreadFunc::shared().push_back( std::bind( []( const std::string& str )
		{
			PlayServises::CloudUserData::cloudNotify( str );
		}, str ) );
	}

	JNIEXPORT void JNICALL  METHOD( UIAlertNotify )( JNIEnv*  env, jobject thiz, jboolean result )
	{
		bool res = (bool)result;
		cocos2d::log( "JNICALL::UIAlertNotify()" );
		MainThreadFunc::shared().push_back( std::bind( []( bool res )
		{
			PlayServises::CloudUserData::UIAlertNotify( res );
		}, res ) );
	}
}
#undef METHOD
#undef package
