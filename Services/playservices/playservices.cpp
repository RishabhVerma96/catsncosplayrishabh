/****************************************************************************
Copyright (c) 2014-2015 Vladimir Tolmachev (Volodar)

This file is part of code base ML

ML is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ML is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ML.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#include "cocos2d.h"
#include "playservices.h"
#include "UserData.h"
#include "ml/pugixml/pugixml.hpp"

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
std::string const xmlAttributeNameId = "id";
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
std::string const xmlAttributeNameId = "id_gamecenter";
#else
std::string const xmlAttributeNameId = "id";
#endif

USING_NS_CC;

namespace PlayServises
{
	std::string _userName;
	std::string _userNickname;
    std::string _playerId;

    void setUserId( const std::string& id )
    {
        _playerId = id;
    }
    
    void setUserDisplayName( const std::string& name )
    {
        _userName = name;
    }
    
	void setUserNickname( const std::string& name )
	{
		_userNickname = name;
	}
    
    std::string getUserId()
    {
        return _playerId;
    }

	std::string getUserDisplayName()
	{
		return _userName;
	}

	std::string getUserNickname()
	{
		return _userNickname;
	}
    
}


Leaderboard::Leaderboard()
{}

void Leaderboard::onCreate()
{
	pugi::xml_document doc;
	doc.load_file( "ini/leaderboards.xml" );
	auto root = doc.root().first_child().child( "ids" );
	auto levels = root.child( "levels" );
	auto global = root.child( "global" );

	_globalId = global.attribute( xmlAttributeNameId.c_str() ).as_string();

	for( auto child : levels )
	{
		auto index = child.attribute( "index" ).as_int();
		auto id = child.attribute( xmlAttributeNameId.c_str() ).as_string();
		_ids[index] = id;
	}
}

std::string Leaderboard::getLevelID( int index )
{
	auto id = _ids.find(index);
	size_t i = index;
	return id == _ids.end() ?
		(_ids.empty() == false ? _ids[0] : "unknown" ) :
		id->second;
}

std::string Leaderboard::getGlobalID()
{
	return _globalId;
}

int Leaderboard::getScoreGlobal()
{
	int global( 0 );
	for( auto id : _ids )
	{
		if( id.first == 0 )
			continue;
		global += getScoreLevel( id.first );
	}
	return global;
}

int Leaderboard::getScoreLevel( int index )
{
	auto key = "PlayServises_Leaderboard_record" + getLevelID( index );
	auto score = UserData::shared().get <int> (key, 0);
	return score;
}

void Leaderboard::fix( int index, int scores )
{
	auto id = getLevelID( index );
	if( id.empty() )
		return;
	auto curr = getScoreLevel( index );
	if( curr < scores )
	{
		curr = scores;
		auto key = "PlayServises_Leaderboard_record" + id;
		UserData::shared().write( key, curr );
	}
	PlayServises::Leaderboard::record( getLevelID( index ), curr );
	PlayServises::Leaderboard::record( getGlobalID(), getScoreGlobal() );
}

void Leaderboard::openLevel( int index )
{
	PlayServises::Leaderboard::open( getLevelID( index ) );
}

void Leaderboard::openGLobal()
{
	PlayServises::Leaderboard::open( getGlobalID() );
}

