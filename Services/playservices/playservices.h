/****************************************************************************
Copyright (c) 2014-2015 Vladimir Tolmachev (Volodar)

This file is part of code base ML

ML is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ML is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ML.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#ifndef __ml_PlayServices_h__
#define __ml_PlayServices_h__

#include <functional>
#include <string>
#include <map>
#include "ml/Observer.h"
#include "ml/Singlton.h"

class Leaderboard : public Singlton < Leaderboard >
{
public:
	Leaderboard();
	virtual void onCreate() override;

	std::string getLevelID( int index );
	std::string getGlobalID();

	int getScoreGlobal();
	int getScoreLevel(int index);
	
	void fix( int index, int scores );
	void openLevel( int index );
	void openGLobal();
private:
	std::string _globalId;
	std::map< int, std::string > _ids;
};

namespace PlayServises
{
	extern ObServer<cocos2d::Ref, std::function<void()>> OnConnected;
	extern ObServer<cocos2d::Ref, std::function<void()>> OnConnectFailed;

	void init( bool showLoginWindow );
	bool isConnected();

    void setUserId( const std::string& name );
    void setUserDisplayName( const std::string& name );
	void setUserNickname( const std::string& name );
    std::string getUserId();
	std::string getUserDisplayName();
	std::string getUserNickname();

	namespace Leaderboard
	{
		void record( const std::string& leaderBoardID, int value );
		void open( const std::string& leaderBoardID = "all");
	}

	namespace CloudUserData
	{
		extern std::function<void( const std::string& )> cloudNotify;
		extern std::function<void( bool )> UIAlertNotify;
		void load();
		void save(const std::string&);
		void showAlertDialog();
	}
}

#endif // #ifdef __ml_PlayServices_h__