
#include "deltadna.h"
#include "cocos2d.h"
#include "ml/JniBind.h"

USING_NS_CC;

#define JAVA(method)JNIEXPORT void JNICALL  Java_com_stereo7_extensions_DeltaDNA_##method(JNIEnv*  env, jobject thiz, jstring key, jstring value)

namespace deltadna
{
	std::map<std::string, std::string> _parameters;
	static const char * Class = "com.stereo7.extensions/DeltaDNA";

	void onAnswer( const std::string& answer );
}

extern
"C"
{
    JAVA(nativeEngageAnswer) {
		std::string str1 = JniHelper::jstring2string(key);
		std::string str2 = JniHelper::jstring2string(value);
		deltadna::_parameters[str1] = str2;
		log("%s, %s", str1.c_str(), str2.c_str() );
    }
}
    
namespace deltadna
{
    void requestEngage(const std::string& name)
    {
        JniMethodInfo t;
		JavaBind bind( "com.stereo7.extensions", "DeltaDNA", "requestEngaged", "%s" );
		if( bind.findMethodInfoWithResult("void") )
			bind.call(name);
		/*
        if( JniHelper::getStaticMethodInfo( t, Class, "requestEngaged", "(Ljava/lang/String;)V" ) )
        {
            log( "............................." );
            log( "deltadna::requestEngaged  java requestEngaged founded" );
            log( "............................." );

            jstring stringArg = t.env->NewStringUTF( name.c_str() );
            t.env->CallStaticVoidMethod( t.classID, t.methodID, stringArg );
            t.env->DeleteLocalRef( stringArg );
            t.env->DeleteLocalRef( t.classID );
        }
        else
        {
        	MessageBox("error", "error");
            log( "............................." );
            log( "deltadna::requestEngaged::jni requestEngaged notfounded" );
            log( "............................." );

        }
        */
    }

	std::string get(const std::string& name)
	{
		std::map<std::string, std::string> dummy;
        dummy["use_Ogury"] = "false";
        dummy["use_Supersonic"] = "true";
        dummy["use_Supersonic_Offerwall"] = "true";
        dummy["ads_video"] = "supersonic";

        if( _parameters.find(name) != _parameters.end() )
        	return _parameters.at(name);
        else
        	return dummy[name];
	}


}

#undef JAVA
