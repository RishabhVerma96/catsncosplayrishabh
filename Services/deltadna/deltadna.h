#ifndef __Delta_Dna__
#define __Delta_Dna__

#include <string>
#include <functional>


//use_Supersonic
//use_Ogury


namespace deltadna
{
	void requestEngage(const std::string& name);
	std::string get(const std::string& name);
}

#endif
