#include "deltadna.h"
#include "cocos2d.h"
#include "AppController.h"

USING_NS_CC;

namespace deltadna
{
	std::map<std::string, std::string> _parameters;
}

void nativeEngageAnswer( const char* str1, const char* str2 )
{
	deltadna::_parameters[str1] = str2;
}

namespace deltadna
{
    void requestEngage(const std::string& name)
    {
		[AppController requestEngage:name.c_str()];
    }

	std::string get(const std::string& name)
	{
		std::map<std::string, std::string> dummy;
        dummy["use_Ogury"] = "true";
        dummy["use_Supersonic"] = "true";
        dummy["use_Supersonic_Offerwall"] = "true";
        dummy["ads_video"] = "supersonic";

        if( _parameters.find(name) != _parameters.end() )
        	return _parameters[name];
        else
        	return dummy[name];
	}


}

#undef JAVA
