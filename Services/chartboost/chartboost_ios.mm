//
//  chartboost_ios.cpp
//  IslandDefense
//
//  Created by Владимир Толмачев on 03.12.14.
//
//

#include "AppController.h"
#include <string>

namespace chartboost
{
	void initialisation( const std::string & appID, const std::string & appSignature )
	{
	}
	
	void showInterstitial()
	{
		[AppController chartboostShowInterstitial];
	}
	
	void showMoreApps()
	{
		[AppController chartboostShowMoreApps];
	}
	
    void showRewardedVideo()
    {
        [AppController chartboostShowRewardedVideo];
    }
    
	bool isInterstitialAvailabled()
	{
		//return true;
		return [AppController chartboostInterstitialIsAvailabled];
	}
    
    bool isRewardedVideoAvailabled()
    {
        return [AppController chartboostRewardedVideoIsAvailabled];
    }
    
}
