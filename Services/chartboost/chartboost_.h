//
//  appsflyer.h
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 12.10.14.
//
//

#ifndef IslandDefense_chartboost_h
#define IslandDefense_chartboost_h

#include "ml/ParamCollection.h"

namespace chartboost
{
	void initialisation( const std::string & appID, const std::string & appSignature );
	void showRewardedVideo();
	void showInterstitial();
	void showMoreApps();
	bool isRewardedVideoAvailabled();
	bool isInterstitialAvailabled();
}

#endif
