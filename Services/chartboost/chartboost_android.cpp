//  IslandDefense
//
//  Created by Vladimir Tolmachev on 12.10.14.
//
//

#include "chartboost_.h"
#include "cocos2d.h"
#include "ml/JniBind.h"
#include "plugins/AdsPlugin.h"

USING_NS_CC;

namespace {
	static const char * package = "com.stereo7.chartboost";
}

namespace chartboost
{
	void initialisation( const std::string & appID, const std::string & appSignature )
	{
	}

	void showRewardedVideo()
	{
		JavaBind( package, "ChartBoost", "showRewardedVideo", "" ).call();
	}

	void showInterstitial()
	{
		JavaBind( package, "ChartBoost", "showInterstitial", "" ).call();
	}
	
	void showMoreApps()
	{
		JavaBind( package, "ChartBoost", "showMoreApps", "" ).call();
	}

	bool isRewardedVideoAvailabled()
	{
		return JavaBind( package, "ChartBoost", "rewardedVideoAvailabled", "" ).bool_call();
	}

	bool isInterstitialAvailabled()
	{
		return JavaBind( package, "ChartBoost", "interstitialAvailabled", "" ).bool_call();
	}

}

#define JAVA(method)JNIEXPORT void JNICALL  Java_com_stereo7_chartboost_ChartBoost_##method

extern
"C"
{
	JAVA(nativeStartVideo)(JNIEnv * env, jobject thiz, bool success) {
		AdsPlugin::shared().onVideoStarted();
	}

	JAVA(nativeFinishedVideo)(JNIEnv * env, jobject thiz, bool success) {
		AdsPlugin::shared().onVideoFinihed( success ? AdsPlugin::Result::Ok : AdsPlugin::Result::Fail, 0);
	}
}