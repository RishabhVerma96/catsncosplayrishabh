//
//  facebook.h
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 12.08.16.
//
//

#include "facebook.h"
#include "cocos2d.h"
#include "configuration.h"
#include "support.h"
#include "ml/common.h"
#include "ml/JniBind.h"

#define package "org.cocos2dx.cpp"


USING_NS_CC;

namespace facebook
{
	void logEvent( const ParamCollection & params )
	{
		if( !Config::shared().get<bool>( "useStatistic" ) )
			return;
		if( !(isTestDevice() && isTestModeActive()) )
		{
			JavaBind bind( package, "AppActivity", "facebookLogEvent", "%s" );
			bind.call( params.string() );
		}
	}
	void logPurchase( const std::string& currency, const std::string& productId, float price )
	{
		if( !Config::shared().get<bool>( "useStatistic" ) )
			return;
		if( !(isTestDevice() && isTestModeActive()) )
		{
			JavaBind bind( package, "AppActivity", "facebookLogPurchase", "%s%s%f" );
			bind.call( currency, productId, price );
		}
	}
}
