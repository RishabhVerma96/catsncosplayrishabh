//
//  facebook.h
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 12.08.16.
//
//

#ifndef __IslandDefense__facebook__
#define __IslandDefense__facebook__

#include "ml/ParamCollection.h"

namespace facebook
{
	void logEvent( const ParamCollection & params );
	void logPurchase( const std::string& currency, const std::string& productId, float price );
}

#endif /* defined(__IslandDefense__facebook__) */
