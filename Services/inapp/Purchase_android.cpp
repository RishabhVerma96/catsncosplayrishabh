#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include "purchase.h"
#include <jni.h>
#include "platform/android/jni/JniHelper.h"
#include "ml/MainThreadFunc.h"

USING_NS_CC;

static const char * Activity = "com.stereo7.extensions/InApps";

extern
"C"
{
	JNIEXPORT void JNICALL  Java_com_stereo7_extensions_InApps_nativeResultDetails(JNIEnv*  env, jobject thiz,
			bool result,
			float priceValue,
			jstring priceText,
			jstring productId,
			jstring description,
			jstring title,
			jstring currency
			)
	{
		inapp::SkuDetails details;
		details.result = result? inapp::Result::Ok : inapp::Result::Fail;
		details.priceValue = priceValue;
		details.priceText = JniHelper::jstring2string(priceText);
		details.productId = JniHelper::jstring2string(productId);
		details.description = JniHelper::jstring2string(description);
		details.title = JniHelper::jstring2string(title);
		details.currency = JniHelper::jstring2string(currency);
		MainThreadFunc::shared().push_back( std::bind( [](inapp::SkuDetails details){
			inapp::requestResultsDetails( details );
		}, details ) );
	}
	JNIEXPORT void JNICALL  Java_com_stereo7_extensions_InApps_nativeResultPurchase(JNIEnv*  env, jobject thiz, jstring results) {
		log("1");
		std::string str = JniHelper::jstring2string(results);
		log("2");
		log( "Java_org_cocos2dx_cpp_InApps_nativeResultPurchase: \n\t\t%s", str.c_str());
		log("3");
		MainThreadFunc::shared().push_back( std::bind( [](const std::string& str){
		log("4");
			inapp::requestResultsPurchase( str );
		log("5");
		}, str ) );
		log("6");
	}
}

void inapp::requestDetails( const std::string & productID )
{
	JniMethodInfo t;
	if( JniHelper::getStaticMethodInfo( t, Activity, "requestDetails", "(Ljava/lang/String;)V" ) )
	{
		jstring stringArg = t.env->NewStringUTF( productID.c_str( ) );
		t.env->CallStaticVoidMethod( t.classID, t.methodID, stringArg );
		t.env->DeleteLocalRef( stringArg );
		t.env->DeleteLocalRef( t.classID );
	}
	else
	{
		log( "............................." );
		log( "requestDetails::jni requestDetails notfound" );
		log( "............................." );
	}
}

void inapp::requestPurchase( const std::string & productID )
{
	JniMethodInfo t;
	if( JniHelper::getStaticMethodInfo( t, Activity, "purchase", "(Ljava/lang/String;)V" ) )
	{
		jstring stringArg = t.env->NewStringUTF( productID.c_str( ) );
		t.env->CallStaticVoidMethod( t.classID, t.methodID, stringArg );
		t.env->DeleteLocalRef( stringArg );
		t.env->DeleteLocalRef( t.classID );
	}
	else
	{
		log( "............................." );
		log( "requestPurchase::jni purchase notfound" );
		log( "............................." );
	}
}

void inapp::restore( const std::string & product )
{
	requestPurchase( product );
}

void inapp::confirm( const std::string & product )
{
	JniMethodInfo t;
	if( JniHelper::getStaticMethodInfo( t, Activity, "consume", "(Ljava/lang/String;)V" ) )
	{
		jstring stringArg = t.env->NewStringUTF( product.c_str( ) );
		t.env->CallStaticVoidMethod( t.classID, t.methodID, stringArg );
		t.env->DeleteLocalRef( stringArg );
		t.env->DeleteLocalRef( t.classID );
	}
	else
	{
		log( "............................." );
		log( "confirm::jni consume notfound" );
		log( "............................." );
	}
}
#endif
