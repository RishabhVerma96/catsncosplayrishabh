#if CC_TARGET_PLATFORM == CC_PLATFORM_WINRT

#include "purchase.h"
#include "MyPurchase.h"
#include <chrono>
#include <set>
#include <iostream>
#include <ppltasks.h>
#include "configuration.h"
using namespace cocos2d; 
using namespace concurrency;
using namespace Windows::Foundation::Collections;
using namespace Platform;
using namespace Windows::ApplicationModel::Store;
using namespace Windows::Storage;
using namespace Microsoft::WRL;

using namespace cocosPluginWinrtBridge;

void dummy_details( const std::string & productID )
{
		/*Windows::UI::Core::CoreDispatcher^ dispatcher;
		dispatcher = cocos2d::GLViewImpl::sharedOpenGLView()->getDispatcher();
	
		//auto inf = CurrentAppSimulator::LoadListingInformationAsync();
		//auto loadInfTask = create_async( inf );
		dispatcher->RunAsync(
			Windows::UI::Core::CoreDispatcherPriority::Normal,
			ref new Windows::UI::Core::DispatchedHandler( [productID]() {
			try {

				auto asyncOp = CurrentAppSimulator::LoadListingInformationAsync;
				auto wrapperTask = create_task( asyncOp );
				
				wrapperTask.then( [asyncOp, productID ](ListingInformation^ t ) {
					try {
						//auto result = asyncOp->GetResults();
						//Windows::ApplicationModel::Store::ProductListing
						//ProductListing productListing;
						
					//	auto listing = productListing.ProductId;
						auto purchaseName = t->Name;
						CCLOG( "name: %s", *purchaseName );
						}
					catch( Exception^ e ) {
						//OnPayResult( PayResultCodeEnum::kPayFail, "product was not purchased" );
					}
					//isRequested = true;
					log( "\nRequest complited" );
				});		
			}
			catch( Exception^ e ) {
				//this->OnPayResult( PayResultCodeEnum::kPayFail, "product was not purchased" );
			}
		}));
		

		//Windows::Foundation::IAsyncOperation<Windows::ApplicationModel::Store::ListingInformation^>^;
	*/


	static std::map<std::string, inapp::SkuDetails> items;
	if( items.empty() )
	{
		items[k::configuration::kInappGold1] = inapp::SkuDetails( 1.f, "50.00 emu", k::configuration::kInappGold1, "1540 Description", "Title", "USD", inapp::Result::Ok );
		items[k::configuration::kInappGold2] = inapp::SkuDetails( 2.f, "100.00 emu", k::configuration::kInappGold2, "3390 Description", "Title", "USD", inapp::Result::Ok );
		items[k::configuration::kInappGold3] = inapp::SkuDetails( 3.f, "150.00 emu", k::configuration::kInappGold3, "5550 Description", "Title", "USD", inapp::Result::Ok );
		items[k::configuration::kInappGold4] = inapp::SkuDetails( 4.f, "200.00 emu", k::configuration::kInappGold4, "8010 Description", "Title", "USD", inapp::Result::Ok );
		items[k::configuration::kInappGold5] = inapp::SkuDetails( 5.f, "250.00 emu", k::configuration::kInappGold5, "10780 Description", "Title", "USD", inapp::Result::Ok );
		items[k::configuration::kInappGear1] = inapp::SkuDetails( 1.f, "50.00 emu", k::configuration::kInappGear1, "300 Description", "Title", "USD", inapp::Result::Ok );
		items[k::configuration::kInappGear2] = inapp::SkuDetails( 2.f, "100.00 emu", k::configuration::kInappGear2, "600 Description", "Title", "USD", inapp::Result::Ok );
		items[k::configuration::kInappGear3] = inapp::SkuDetails( 3.f, "150.00 emu", k::configuration::kInappGear3, "1200 Description", "Title", "USD", inapp::Result::Ok );
		items[k::configuration::kInappFuel1] = inapp::SkuDetails( 1.f, "50.00 emu", k::configuration::kInappFuel1, "250 Description", "Title", "USD", inapp::Result::Ok );
		items[k::configuration::kInappHero2] = inapp::SkuDetails( 2.f, "50.00 emu", k::configuration::kInappHero2, "1 Description", "Title", "USD", inapp::Result::Ok );
		items[k::configuration::kInappHero3] = inapp::SkuDetails( 3.f, "50.00 emu", k::configuration::kInappHero3, "1 Description", "Title", "USD", inapp::Result::Ok );
		items[k::configuration::kInappAllHeroes] = inapp::SkuDetails( 1.f, "50.999emu", k::configuration::kInappAllHeroes, "1 Description", "Title", "USD", inapp::Result::Ok );
	}

	inapp::requestResultsDetails( items[productID] );
	//inapp::requestResultsDetails( items["fail"] );
}

void dummy_purchase( const std::string & productID )
{
	//std::this_thread::sleep_for( std::chrono::milliseconds( 1000 ) );
	inapp::requestResultsPurchase( "result:ok,id:" + productID );
}

void inapp::requestDetails( const std::string & productID )
{
	dummy_details( productID );
}

void inapp::requestPurchase( const std::string & productID )
{
	MyPurchase::getInstance()->loadIAPPlugin();
	MyPurchase::MyPayMode mode = MyPurchase::MyPayMode::eGoogle;
	std::map<std::string, std::string> productInfo;
	productInfo["product"] = productID;
	MyPurchase::getInstance()->payByMode( productInfo, MyPurchase::MyPayMode::eGoogle );
	
}


void inapp::confirm( const std::string & product )
{

}

void inapp::restore( const std::string & product )
{
	dummy_purchase( product );
}

#endif