//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "Purchase.h"
#include "ml/common.h"
#include "ml/ParamCollection.h"

USING_NS_CC;

static inapp::CallBackPurchase _callback_purchase;
static inapp::CallBackRequestDetails _callback_details;
static std::map<std::string, inapp::SkuDetails> _skuDetails;

inapp::SkuDetails::SkuDetails( float priceValue, std::string priceText, std::string productId, std::string description, std::string title, std::string currency, Result result)
{
	this->priceText = priceText;
	this->priceValue = priceValue;
	this->productId = productId;
	this->description = description;
	this->title = title;
	this->currency = currency;
	this->result = result;
}

void inapp::SkuDetails::prepairPriceString()
{
	auto systemlang = Application::getInstance()->getCurrentLanguage();
	if( currency == "RUB" )
	{
		priceText = toStr( static_cast<int>(priceValue) );
		priceText.append( " " );
		priceText.append( currency );
	}
	if( currency == "USB" )
	{
		priceText = "$" + toStr( static_cast<float>(priceValue) );
	}
}

void inapp::details( const std::string & productID, const CallBackRequestDetails callback )
{
	if( Config::shared().get<bool>( "useInapps" ) == false )
		return;

	_callback_details = callback;
	auto iter = _skuDetails.find( productID );
	if( iter != _skuDetails.end() )
	{
		requestResultsDetails( iter->second );
	}
	else
	{
		requestDetails( productID );
	}
}

void inapp::setCallbackPurchase( const CallBackPurchase & callback)
{
	_callback_purchase = callback;
}

void inapp::purchase( const std::string & product )
{
	if( Config::shared().get<bool>( "useInapps" ) == false )
		return;
	requestPurchase( product );
}

void inapp::requestResultsDetails( SkuDetails details )
{
	_skuDetails.emplace( details.productId, details );
	if( _callback_details )
	{
		details.prepairPriceString();
		_callback_details( details );
	}
}

void inapp::requestResultsPurchase( const std::string & detailString )
{
	log( "inapp::requestResultsPurchase 1" );
	ParamCollection params( detailString );
	log( "inapp::requestResultsPurchase 1" );
	params.tolog();
	log( "inapp::requestResultsPurchase 1" );
	PurchaseResult result;
	log( "inapp::requestResultsPurchase 1" );
	
	result.result = (params["result"] == "ok") ? Result::Ok :
		params["result"] == "canceled"? Result::Canceled : Result::Fail;

	if( params["result"] == "ok" )
	{
		result.result = Result::Ok;
	}
	else if( params["result"] == "restored" )
	{
		result.result = Result::Restored;
	}
	else if( params["result"] == "failed" )
	{
		result.result = Result::Fail;
	}
	else
	{
		result.result = Result::Canceled;
		result.errormsg = params["was canceled"];
	}
    
    result.productId = params["id"];
	result.errorcode = strTo<int>( params["errorcode"] );
	result.errormsg = params["errormsg"];
	if( params.count( "id" ) > 0 && _skuDetails.count(result.productId) > 0)
	{
		result.productDetails = _skuDetails.at( result.productId );
	}

	if( _callback_purchase )
	{
		log( "call callback" );
		_callback_purchase( result );
	}
	else
	{
		log( "callback == null" );
	}
}
