//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __Purchase_h__
#define __Purchase_h__
#include "support.h"
#include <functional>
#include <list>

using namespace cocos2d;

class inapp
{
public:
	enum class Result
	{
		Ok,
		Fail,
		Canceled,
		Restored,
	};

	struct SkuDetails
	{
		SkuDetails():priceValue(-1), result(Result::Fail){}
		SkuDetails( float priceValue, std::string priceText, std::string productId, std::string description, std::string title, std::string currency, Result result);
		void prepairPriceString();

		float priceValue;
		std::string priceText;
		std::string productId;
		std::string description;
		std::string title;
		std::string currency;
		Result result;
	};
	
	struct PurchaseResult
	{
		PurchaseResult():result(Result::Fail), errorcode(0){}
		Result result;
		std::string productId;
		std::string errormsg;
		int errorcode;
		SkuDetails productDetails;
	};

	typedef std::function< void( PurchaseResult ) > CallBackPurchase;
	typedef std::function< void( SkuDetails ) > CallBackRequestDetails;

	static void setCallbackPurchase( const CallBackPurchase & callback);
	static void details(const std::string & productID, const CallBackRequestDetails callback);
	static void purchase( const std::string & product );
	static void confirm( const std::string & product );
	static void restore( const std::string & product );

	static void requestResultsDetails( SkuDetails details );
	static void requestResultsPurchase( const std::string & detailString );
private:
	static void requestPurchase( const std::string & productID );
	static void requestDetails( const std::string & productID );
};


#endif
