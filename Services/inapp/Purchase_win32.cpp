//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) ||  (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
#include "purchase.h"
//#include "MyPurchase.h"
#include <chrono>

//call back:
//inapp::requestResultsDetails( str );
//inapp::requestResultsPurchase( str );

//purchase_win32.cpp

using namespace cocos2d;

std::function<inapp::SkuDetails( const std::string & )> _functionDummy;
void setInappDummyDetails( std::function<inapp::SkuDetails( const std::string & )> func )
{
	_functionDummy = func;
}

void dummy_details( const std::string & productID )
{
	if( _functionDummy )
	{
		auto result = _functionDummy( productID );
		inapp::requestResultsDetails( result );
		return;
	}
	static std::map<std::string, inapp::SkuDetails> items;
	if( items.empty() )
	{
		items[Config::shared().get( "inappCards1" )] = inapp::SkuDetails( 49.999f, "49.999 emu", Config::shared().get( "inappCards1" ), "8 Cards", "Title", "RUB", inapp::Result::Ok );
		items[Config::shared().get( "inappCards2" )] = inapp::SkuDetails( 99.999f, "99.999 emu", Config::shared().get( "inappCards2" ), "8 Cards", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappGold1" )] = inapp::SkuDetails( 50.999f, "$1",  Config::shared().get( "inappGold1" ), "500 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappGold2" )] = inapp::SkuDetails( 50.999f, "$2", Config::shared().get( "inappGold2" ), "1250 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappGold3" )] = inapp::SkuDetails( 50.999f, "$3", Config::shared().get( "inappGold3" ), "2500 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappGold4" )] = inapp::SkuDetails( 50.999f, "$5", Config::shared().get( "inappGold4" ), "5000 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappGold5" )] = inapp::SkuDetails( 50.999f, "$20", Config::shared().get( "inappGold5" ), "100000 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappTicket1" )] = inapp::SkuDetails( 50.999f, "50.999 emu",  Config::shared().get( "inappTicket1" ), "25 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappTicket2" )] = inapp::SkuDetails( 50.999f, "100.999 emu", Config::shared().get( "inappTicket2" ), "50 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappTicket3" )] = inapp::SkuDetails( 50.999f, "150.999 emu", Config::shared().get( "inappTicket3" ), "100 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappTicket4" )] = inapp::SkuDetails( 50.999f, "200.999 emu", Config::shared().get( "inappTicket4" ), "150 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappTicket5" )] = inapp::SkuDetails( 50.999f, "250.999 emu", Config::shared().get( "inappTicket5" ), "200 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappGear1" )] = inapp::SkuDetails( 50.999f, "50.999 emu",  Config::shared().get( "inappGear1" ), "500 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappGear2" )] = inapp::SkuDetails( 50.999f, "$1", Config::shared().get( "inappGear2" ), "2500 Description", "Title", "USD", inapp::Result::Ok );
        
		items[Config::shared().get( "inappGear3" )] = inapp::SkuDetails( 50.999f, "$2", Config::shared().get( "inappGear3" ), "10000 Description", "Title", "USD", inapp::Result::Ok );
        
		items[Config::shared().get( "inappFuel1" )] = inapp::SkuDetails( 50.999f, "$1",  Config::shared().get( "inappFuel1" ), "10 Description", "Title", "USD", inapp::Result::Ok );
#if STEAMBUILD != 1
		items[Config::shared().get( "inappHero2" )] = inapp::SkuDetails( 50.999f, "$1",  Config::shared().get( "inappHero2" ), "1 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappHero3" )] = inapp::SkuDetails( 50.999f, "$1",  Config::shared().get( "inappHero3" ), "1 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappPackage" ) + "hero4"] = inapp::SkuDetails( 1, "$1", Config::shared().get( "inappPackage" ) + "hero4", "1 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappPackage" ) + "hero5"] = inapp::SkuDetails( 2, "$1", Config::shared().get( "inappPackage" ) + "hero5", "1 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappPackage" ) + "hero6"] = inapp::SkuDetails( 3, "$1", Config::shared().get( "inappPackage" ) + "hero6", "1 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappPackage" ) + "hero7"] = inapp::SkuDetails( 1.49f, "$1", Config::shared().get( "inappPackage" ) + "hero7", "1 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappPackage" ) + "hero8"] = inapp::SkuDetails( 2.49f, "$2", Config::shared().get( "inappPackage" ) + "hero8", "1 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappPackage" ) + "hero9"] = inapp::SkuDetails( 2.49f, "2.49 emu", Config::shared().get( "inappPackage" ) + "hero9", "1 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappPackage" ) + "hero10"] = inapp::SkuDetails( 3.49f, "3.49 emu", Config::shared().get( "inappPackage" ) + "hero10", "1 Description", "Title", "USD", inapp::Result::Ok );
#endif
		items[Config::shared().get( "inappAllHeroes") ] = inapp::SkuDetails( 7.99f, "$5", Config::shared().get( "inappAllHeroes" ), "1 Description", "Title", "USD", inapp::Result::Ok );
		items[Config::shared().get( "inappPackHeroes1" )] = inapp::SkuDetails( 7.99f, "$5", Config::shared().get( "inappPackHeroes1" ), "1 Description", "Title", "USD", inapp::Result::Ok );
	}

	inapp::requestResultsDetails( items[productID] );
	//inapp::requestResultsDetails( items["fail"] );
}

void dummy_purchase( const std::string & productID )
{
	auto f = [productID]()
	{
		std::this_thread::sleep_for( std::chrono::milliseconds( 500 ) );
		//inapp::requestResultsPurchase( "result:ok,id:" + productID );
		inapp::requestResultsPurchase( "result:failed,id:" + productID );
	};
	std::thread th( f );
	th.detach();
}

void inapp::requestDetails( const std::string & productID )
{
	dummy_details( productID );
}

void inapp::requestPurchase( const std::string & productID )
{
	//MyPurchase::getInstance()->loadIAPPlugin();
	//MyPurchase::MyPayMode mode = MyPurchase::MyPayMode::eGoogle;
	//std::map<std::string, std::string> productInfo;
	//productInfo["product"] = productID;
	//MyPurchase::getInstance()->payByMode( productInfo, MyPurchase::MyPayMode::eGoogle );
	
	dummy_purchase( productID );
}

void inapp::confirm( const std::string & product )
{}

void inapp::restore( const std::string & product )
{
	dummy_purchase( product );
}

#endif
