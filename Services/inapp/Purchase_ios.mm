//
//  Purchase_ios.m
//  IslandDefense
//
//  Created by Владимир Толмачев on 07.12.14.
//
//
#include "ml/ParamCollection.h"
#include "support/support.h"
#include "Purchase.h"
#include "configuration.h"
#import "ios/iap_objc.h"


using namespace et;

enum class TransactionResult
{
	ok,
	canceled,
	restored,
	failed,
};

void requestResult( const std::string & product, const std::string & message, TransactionResult result)
{
	ParamCollection params;
	if( result == TransactionResult::ok )
		params["result"] = "ok";
	if( result == TransactionResult::failed )
		params["result"] = "failed";
	if( result == TransactionResult::canceled )
		params["result"] = "canceled";
	if( result == TransactionResult::restored )
		params["result"] = "restored";
	params["id"] = product;
	params["errormsg"] = message;
	inapp::requestResultsPurchase( params.string() );
}

void inapp::requestDetails( const std::string & productID )
{
    auto product = sharedPurchasesManager().productForIdentifier(productID);
    if( product.identifier != productID )
	{
        PurchasesManager::ProductsSet products;
        products.insert( cocos2d::Config::shared().get("inappGold1") );
        products.insert( cocos2d::Config::shared().get("inappGold2") );
        products.insert( cocos2d::Config::shared().get("inappGold3") );
        products.insert( cocos2d::Config::shared().get("inappGold4") );
        products.insert( cocos2d::Config::shared().get("inappGold5") );
        products.insert( cocos2d::Config::shared().get("inappGear1") );
        products.insert( cocos2d::Config::shared().get("inappGear2") );
        products.insert( cocos2d::Config::shared().get("inappGear3") );
        products.insert( cocos2d::Config::shared().get("inappFuel1") );
        products.insert( cocos2d::Config::shared().get("inappFuel2") );
        products.insert( cocos2d::Config::shared().get("inappFuel3") );
        products.insert( cocos2d::Config::shared().get("inappTicket1") );
        products.insert( cocos2d::Config::shared().get("inappTicket2") );
        products.insert( cocos2d::Config::shared().get("inappHero2") );
        products.insert( cocos2d::Config::shared().get("inappHero3") );
        products.insert( cocos2d::Config::shared().get("inappPackage") + "hero4" );
        products.insert( cocos2d::Config::shared().get("inappPackage") + "hero5" );
        products.insert( cocos2d::Config::shared().get("inappPackage") + "hero6" );
        products.insert( cocos2d::Config::shared().get("inappPackage") + "hero7" );
        products.insert( cocos2d::Config::shared().get("inappPackage") + "hero8" );
        products.insert( cocos2d::Config::shared().get("inappPackage") + "hero9" );
        products.insert( cocos2d::Config::shared().get("inappPackage") + "hero10" );
        products.insert( cocos2d::Config::shared().get("inappAllHeroes") );
        products.insert( cocos2d::Config::shared().get("inappPackHeroes1") );
        products.insert( cocos2d::Config::shared().get("inappLootPicker") );
        
        products.insert( cocos2d::Config::shared().get("inappInfinity1") );
        products.insert( cocos2d::Config::shared().get("inappTower1") );
        products.insert( cocos2d::Config::shared().get("inappTower2") );
        
        products.insert( "com.stereo7games.islanddefense.hero7" );
        products.insert( "com.stereo7games.islanddefense.hero8" );
        products.insert( "com.stereo7games.islanddefense.hero9" );
        products.insert( "com.stereo7games.islanddefense.hero10" );
        
		sharedPurchasesManager().checkAvailableProducts( products );
		
		auto c1 = std::bind( ::requestResult, std::placeholders::_1, "", TransactionResult::ok );
		auto c2 = std::bind( ::requestResult, std::placeholders::_1, std::placeholders::_2, TransactionResult::failed );
		auto c3 = std::bind( ::requestResult, std::placeholders::_1, "", TransactionResult::canceled );
		auto c4 = std::bind( ::requestResult, std::placeholders::_1, "", TransactionResult::restored );
		sharedPurchasesManager().onPurchaseFinished.add(0x1, c1 );
		sharedPurchasesManager().onPurchaseFailed.add(0x1, c2 );
		sharedPurchasesManager().onPurchaseCanceled.add(0x1, c3 );
		sharedPurchasesManager().onPurchaseRestored.add(0x1, c4 );
	}
	else
	{
        SkuDetails details;
        details.result = inapp::Result::Ok;
        details.productId = product.identifier;
        details.description = product.description;
        details.priceText = product.currency + ":" + floatToStr(product.price);
		details.priceValue = product.price;
		details.currency = product.currency;
		details.title = product.title;
		inapp::requestResultsDetails( details );
    }
}

void inapp::requestPurchase( const std::string & productID )
{
	sharedPurchasesManager().purchaseProduct(productID);
}

void inapp::confirm( const std::string & product )
{
}

void inapp::restore( const std::string & product )
{
	sharedPurchasesManager().restorePurchases();
}


