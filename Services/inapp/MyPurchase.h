#ifndef __MY_PURCHASE_H__
#define __MY_PURCHASE_H__

#include "ProtocolIAP.h"

class MyPurchaseResult : public cocos2d::plugin::PayResultListener
{
public:
	static void showResult();
	virtual void onPayResult( cocos2d::plugin::PayResultCode ret, const char* msg, cocos2d::plugin::TProductInfo info );
};

class MyPurchase
{
public:
	static MyPurchase* getInstance();
	static void purgePurchase();
	

	typedef enum {
		eNoneMode = 0,
		eGoogle,
	} MyPayMode;

	void unloadIAPPlugin();
	void loadIAPPlugin();
	void payByMode( cocos2d::plugin::TProductInfo info, MyPayMode mode );
	bool getRequestStatus();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	void reportConsumablePurchase( char* productId, char* transactionId );
	cocos2d::plugin::ProtocolIAP* getPlugin() { return iapPlugin; }
#endif

private:
	MyPurchase();
	virtual ~MyPurchase();

	static MyPurchase* s_pPurchase;
	cocos2d::plugin::ProtocolIAP* iapPlugin;
	MyPurchaseResult* s_pRetListener;
};

#endif // __MY_PURCHASE_H__
