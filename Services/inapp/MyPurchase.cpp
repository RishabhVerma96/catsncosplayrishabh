#include "MyPurchase.h"
#include "PluginManager.h"
#include "PluginFactory.h"
#include "cocos2d.h"
#include "Configs.h"
#include "Purchase.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define PLUGIN_NAME "IAPGooglePlay"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
#define PLUGIN_NAME "microsoftiap"
#endif

using namespace cocos2d::plugin;
using namespace cocos2d;

MyPurchase* MyPurchase::s_pPurchase = NULL;
ProtocolIAP* pIAPreq;
MyPurchase::MyPurchase()
	: s_pRetListener( NULL )
	, iapPlugin( NULL )
{

}

MyPurchase::~MyPurchase()
{
	unloadIAPPlugin();
	if( s_pRetListener )
	{
		delete s_pRetListener;
		s_pRetListener = NULL;
	}
}

MyPurchase* MyPurchase::getInstance()
{
	if( s_pPurchase == NULL ) {
		s_pPurchase = new MyPurchase();
	}
	return s_pPurchase;
}

void MyPurchase::purgePurchase()
{
	if( s_pPurchase )
	{
		delete s_pPurchase;
		s_pPurchase = NULL;
	}
	PluginManager::end();
}

void MyPurchase::loadIAPPlugin()
{
	if( s_pRetListener == NULL )
	{
		s_pRetListener = new MyPurchaseResult();
	}

	TIAPDeveloperInfo pDevInfo;

	
#if  (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	// Windows Store IAP
	{
		auto factory = plugin::PluginFactory::getInstance();
		auto dispatcher = cocos2d::GLViewImpl::sharedOpenGLView()->getDispatcher();
		factory->setDispatcher( dispatcher );
		pDevInfo["windows_store_proxy"] = "WindowsStoreProxy.xml";
	}
#endif
	iapPlugin = dynamic_cast<ProtocolIAP*>(PluginManager::getInstance()->loadPlugin( PLUGIN_NAME ));
	iapPlugin->setDebugMode( false );
	iapPlugin->setResultListener( s_pRetListener );
	iapPlugin->configDeveloperInfo( pDevInfo );
}

void MyPurchase::unloadIAPPlugin()
{
	if( iapPlugin )
	{
		PluginManager::getInstance()->unloadPlugin( PLUGIN_NAME );
		iapPlugin = NULL;
	}
}


void MyPurchase::payByMode( TProductInfo info, MyPayMode mode )
{
	ProtocolIAP* pIAP = NULL;
	switch( mode )
	{
		case eGoogle:
			pIAP = iapPlugin;
			break;
		default:
			CCLOG( "Unsupported IAP" );
			break;
	}

	if( pIAP ) {
		
		std::function<void( int, std::string& )> callback = std::bind( []( int code, std::string& str )
		{
			//XML
			pugi::xml_document doc;
			doc.load( str.c_str() );
			auto root = doc.root().last_child().first_child();			
			auto attribute = root.attribute( "ProductId" );
			std::string purchaseId = attribute.value();

			if(code == 0 )
				inapp::requestResultsPurchase( "result:ok,id:" + purchaseId );
			else
				inapp::requestResultsPurchase( "result:canceled,id:" + purchaseId );
			
			
		}, std::placeholders::_1, std::placeholders::_2 );

		pIAP->setCallback( callback );
		
		pIAP->payForProduct( info );
		pIAPreq = pIAP;
	}
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)

void MyPurchaseResult::onPayResult( PayResultCode ret, const char* msg, TProductInfo info )
{
	std::string title;
	if( ret == PayResultCode::kPaySuccess ) {
		title = "Purchase Success";
	}
	else {
		title = "Purchase Fail";
	}
	MessageBox( msg, title.c_str() );
}

void MyPurchase::reportConsumablePurchase( char* productId, char* transactionId ) {
	plugin::PluginParam productIdParam( productId );
	plugin::PluginParam transactionIdParam( transactionId );
	bool reportResult = iapPlugin->callBoolFuncWithParam( "reportConsumableFulfillment", &productIdParam, &transactionIdParam, NULL );
	std::string result = reportResult ? "Success" : "Fail";
	MessageBox( result.c_str(), "consumable fulfillment result" );

}

#endif