//
//  appgratis.h
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 12.10.14.
//
//

#ifndef IslandDefense_appgratis_h
#define IslandDefense_appgratis_h

#include <functional>
#include "ml/ParamCollection.h"

namespace appgratis
{
	typedef std::function<void(const ParamCollection&)> Callback;
	void setCallback( const Callback & callback );
	void init();
}

#endif
