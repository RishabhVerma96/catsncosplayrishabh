//
//  appgratis_dummy.cpp
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 12.10.14.
//
//

#include "appgratis.h"

namespace appgratis
{
	void setCallback( const Callback & callback )
	{
		auto dummy = []( const Callback & callback )
		{
			std::this_thread::sleep_for( std::chrono::milliseconds( 1000 ) );
			ParamCollection p;
			p["PRO"] = "nil";
			//callback( p );
		};
		std::thread thread( std::bind( dummy, callback ) );
		thread.detach();
	}

	void init()
	{
	}
}
