//
//  appgratis_android.cpp
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 12.10.14.
//
//

#include "appgratis.h"
#include "cocos2d.h"
#include <list>
#include "AppController.h"

USING_NS_CC;

namespace appgratis
{
	std::list<ParamCollection> _queue;
	Callback _callback;


	void setCallback( const Callback & callback )
	{
		_callback = callback;
		if(_callback )
		{
			for( auto& p : _queue )
			{
				_callback(p);
			}
			_queue.clear();
		}
	}

	void onRedeem( const std::string & arg )
	{
		ParamCollection pc(arg);

		if( pc.empty() )return;
		
		std::string name = pc.begin()->first;
		std::string value = pc.begin()->second;
		//if( name == "Pro" || name == "PRO" || name == "pro" )
		{
			if( _callback )
				_callback( pc );
			else
				_queue.push_back(pc);
		}
	}

	void init()
	{
		std::function<void(const char*)> func = std::bind( appgratis::onRedeem, std::placeholders::_1 );
		[AppController getInstance].BatchCallback = func;
	}
}
