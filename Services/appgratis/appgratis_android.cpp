//
//  appgratis_android.cpp
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 12.10.14.
//
//

#include "appgratis.h"
#include "cocos2d.h"
#include <jni.h>
#include "platform/android/jni/JniHelper.h"
#include <list>

USING_NS_CC;

#define JAVA(method)JNIEXPORT void JNICALL  Java_com_stereo7_extensions_AppGratisBatch_##method(JNIEnv*  env, jobject thiz, jstring results)

namespace {
	static const char * Activity = "com.stereo7.extensions/AppGratisBatch";
}

namespace appgratis
{
	void onRedeem( const std::string & arg );
}

extern
"C"
{

	JAVA(nativeOnRedeemFeature) {
		std::string str = JniHelper::jstring2string(results);
		log( "Java native nativeOnRedeem: \n\t\t%s", str.c_str());
		appgratis::onRedeem( str );
	}
	
}

namespace appgratis
{
	std::list<ParamCollection> _queue;
	Callback _callback;
	void setCallback( const Callback & callback )
	{
		_callback = callback;
		if(_callback )
		{
			for( auto& p : _queue )
			{
				_callback(p);
			}
			_queue.clear();
		}
	}

	void onRedeem( const std::string & arg )
	{
		ParamCollection pc(arg);

		if( pc.empty() )return;
		
		std::string name = pc.begin()->first;
		std::string value = pc.begin()->second;
		//if( name == "Pro" || name == "PRO" || name == "pro" )
		{
			if( _callback )
				_callback( pc );
			else
				_queue.push_back(pc);
		}
	}
	
	void init()
	{
	}


}
