#include "cocos2d.h"
#include "appodeal_.h"
#include "ml/JniBind.h"
#include "plugins/AdsPlugin.h"
#include "ml/MainThreadFunc.h"

#define package "com.stereo7.appodeal"
#define classname "mlAppodeal"
#define JAVA(method)JNIEXPORT void JNICALL  Java_com_stereo7_appodeal_mlAppodeal_##method

void appodeal::playAd()
{
	JavaBind bind( package, classname, "playAd", "" );
	bind.call();
}

bool appodeal::isVideoAvailabled()
{
	JavaBind bind( package, classname, "isVideoAvailabled", "" );
	return bind.bool_call();
}

bool appodeal::isInterstitialAvailabled()
{
	JavaBind bind( package, classname, "isInterstitialAvailabled", "" );
	return bind.bool_call();
}

void appodeal::showInterstitial()
{
	JavaBind bind( package, classname, "showInterstitial", "" );
	bind.call();
}

void appodeal::cacheInterstitial()
{
	//cocos2d::log( "Caching Interstitial." );
}

void appodeal::cacheVideo()
{
	//cocos2d::log( "Caching Video." );
}


extern
"C"
{
	JAVA(nativeStartVideo)(JNIEnv*  env, jobject thiz, bool success) {
		MainThreadFunc::shared().push_back( std::bind( [](){
			AdsPlugin::shared().onVideoStarted();
		} ) );
	}

	JAVA(nativeFinishedVideo)(JNIEnv*  env, jobject thiz, bool success) {
		MainThreadFunc::shared().push_back( std::bind( []( bool success ){
			AdsPlugin::shared().onVideoFinihed( success ? AdsPlugin::Result::Ok : AdsPlugin::Result::Fail, 0 );
		}, success ) );
	}

	JAVA(nativeNoOffersVideo)(JNIEnv*  env, jobject thiz) {
		MainThreadFunc::shared().push_back( std::bind( [](){
			cocos2d::MessageBox( "No ads available. Please try again later.", "" );
		} ) );
	}
}
