#include "cocos2d.h"
#include "appodeal_.h"
#include "../plugins/AdsPlugin.h"

USING_NS_CC;

namespace appodeal
{
	void playAd()
	{
		cocos2d::log("Playing Ad.");
		auto start = [] (){AdsPlugin::shared().onVideoStarted(); };
		auto finish = [] (){AdsPlugin::shared().onVideoFinihed(AdsPlugin::Result::Ok, 0); };
		auto action = Sequence::create(
			DelayTime::create(1),
			CallFunc::create(start),
			DelayTime::create(1),
			CallFunc::create(finish),
			RemoveSelf::create(),
			nullptr);
		auto node = Node::create();
		Director::getInstance()->getRunningScene()->addChild(node);
		node->runAction(action);
	}

	bool isVideoAvailabled() { cocos2d::log("Video is availabled."); return true; }
	bool isInterstitialAvailabled() { cocos2d::log("Video is availabled."); return true; }
	void showInterstitial() { cocos2d::log("Showing Interstitial."); }
	void cacheInterstitial() { cocos2d::log("Caching Interstitial."); }
	void cacheVideo() { cocos2d::log("Caching Video."); }
}