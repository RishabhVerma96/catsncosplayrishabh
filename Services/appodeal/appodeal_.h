//
//  fyber_ios.c
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 28.10.15.
//
//

#ifndef __APPODEAL_H__
#define __APPODEAL_H__
#include <functional>

namespace appodeal
{
	void playAd();
	bool isVideoAvailabled();
	bool isInterstitialAvailabled();
	void showInterstitial();
	void cacheInterstitial();
	void cacheVideo();
}


#endif
