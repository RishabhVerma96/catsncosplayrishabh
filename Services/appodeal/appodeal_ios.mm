//
//  fyber_ios.c
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 28.10.15.
//
//

#include "appodeal_.h"
#include "AppController.h"
#include "AdsPlugin.h"

namespace appodeal
{
	void started(bool result)
	{
		if( result )
			AdsPlugin::shared().onVideoStarted();
	}
	
	void finished(bool result)
	{
		AdsPlugin::shared().onVideoFinihed( result ? AdsPlugin::Result::Ok : AdsPlugin::Result::Fail, 0 );
	}
	
	void playAd()
	{
		[AppController getInstance].appodealStarted = std::bind( started, std::placeholders::_1);
		[AppController getInstance].appodealFinished = std::bind( finished, std::placeholders::_1);
		[AppController appodealPlayVideo];
	}
	
	bool isVideoAvailabled()
	{
		return [AppController appodealIsVideoAvailabled];
	}
	
	void showInterstitial()
	{
		[AppController appodealInterstitial];
	}
	
	void cacheInterstitial()
	{
	}
	
	void cacheVideo()
	{
	}
    
    bool isInterstitialAvailabled()
    {
        return true;
    }
	
}
