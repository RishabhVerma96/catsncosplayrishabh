//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __AdMob_h__
#define __AdMob_h__


namespace AdMob
{
	void show();
	void hide();
	void showInterstitial();
	bool isAvailabled();
}

#endif
