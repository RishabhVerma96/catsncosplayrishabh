//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include "cocos2d.h"
#include "AdMob.h"
#include "ml/JniBind.h"

USING_NS_CC;

namespace AdMob
{
	void showInterstitial()
	{
		JavaBind bind( "com.stereo7.admob", "AdMob", "showInterstitial", "" );
		bind.call();
	}

	void show()
	{
	}
	
	void hide()
	{
	}

	bool isAvailabled()
	{
		JavaBind bind( "com.stereo7.admob", "AdMob", "interstitialAvailabled", "" );
		return bind.bool_call();
	}
}
#endif

