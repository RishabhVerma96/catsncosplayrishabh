
#include "AdMob.h"
#import "AppController.h"

namespace AdMob
{
    void show()
    {
        //do nothing
    }
    
    void hide()
    {
        //do nothing
    }
    
    void showInterstitial()
    {
        [AppController admobShowInterstitial];
    }
	
	bool isAvailabled()
	{
		return true;
	}

}