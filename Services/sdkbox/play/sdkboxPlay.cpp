//
//  AdMob_dummy.cpp
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 25.11.14.
//
//
#include "cocos2d.h"
#include "PluginSdkboxPlay/PluginSdkboxPlay.h"
#include "ml/common.h"
#include <set>

USING_NS_CC;

namespace sdkbox
{
	namespace play
	{

		class IMListener : public SdkboxPlayListener {
		public:
            void onConnectionStatusChanged( int status ) {
				CCLOG( "sdkboxplay: onConnectionStatusChanged %d", status );
			}
            
            void onScoreSubmitted( const std::string& leaderboard_name, long score, bool maxScoreAllTime, bool maxScoreWeek, bool maxScoreToday ) {
				CCLOG( "sdkboxPlay: onScoreSubmitted %s", leaderboard_name.c_str() );
			}
		};

		void init()
		{
            sdkbox::PluginSdkboxPlay::init();
		}

        void submitScore(double score)
        {
            sdkbox::PluginSdkboxPlay::submitScore("denis.catcoins", score);
        }
        void showLeaderBoard()
        {
            sdkbox::PluginSdkboxPlay::showLeaderboard("denis.catcoins");
        }

        
        
//        void submitScore()
//        {
//            if( _interstitialName.empty() ) return;
//            CCLOG( "sdkboxAdmob: showInterstitial %s", _interstitialName.c_str() );
//            if( isInterstitialAvailabled() )
//                sdkbox::PluginAdMob::show( _interstitialName );
//            else
//                cacheInterstitial();
//        }
//
//        void showBanner()
//        {
//            if( _bannerName.empty() ) return;
//            if( isBannerAvailabled() )
//                sdkbox::PluginAdMob::show( _bannerName );
//            else
//                cacheBanner();
//            CCLOG( "sdkboxAdmob: showBanner %s", _bannerName.c_str() );
//        }
//
//        void hideBanner()
//        {
//            if( _bannerName.empty() ) return;
//            sdkbox::PluginAdMob::hide( _bannerName );
//            CCLOG( "sdkboxAdmob: hideBanner %s", _bannerName.c_str() );
//        }
//
//        bool isInterstitialAvailabled()
//        {
//            if( _interstitialName.empty() == false )
//                return sdkbox::PluginAdMob::isAvailable( _interstitialName );
//            else
//                return false;
//        }
//
//        bool isBannerAvailabled()
//        {
//            if( _bannerName.empty() == false )
//                return sdkbox::PluginAdMob::isAvailable( _bannerName );
//            else
//                return false;
//            CCLOG( "sdkboxAdmob: isBannerAvailabled %s", _bannerName.c_str() );
//        }
//
//        void cacheInterstitial()
//        {
//            if( _interstitialName.empty() ) return;
//            sdkbox::PluginAdMob::cache( _interstitialName );
//            CCLOG( "sdkboxAdmob: cacheInterstitial %s", _interstitialName.c_str() );
//        }
//
//        void cacheBanner()
//        {
//            if( _bannerName.empty() ) return;
//            sdkbox::PluginAdMob::cache( _bannerName );
//            CCLOG( "sdkboxAdmob: cacheBanner %s", _bannerName.c_str() );
//        }
	}
}
