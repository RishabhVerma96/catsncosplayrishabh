//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __SdkboxPlay_h__
#define __SdkboxPlay_h__
#include <string>
namespace sdkbox
{
	namespace play
	{
		void init();
        void submitScore(double score);
        void showLeaderBoard();
//        void showBanner();
//        void hideBanner();
//        bool isInterstitialAvailabled();
//        bool isBannerAvailabled();
//        void cacheInterstitial();
//        void cacheBanner();
	}
}
#endif
