//
//  AdMob_dummy.cpp
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 25.11.14.
//
//
#include "cocos2d.h"
#include "sdkboxAdmob.h"
#include "PluginAdMob/PluginAdMob.h"
#include "ml/common.h"
#include <set>

USING_NS_CC;

namespace sdkbox
{
	namespace admob
	{
		std::string _bannerName;
		std::string _interstitialName;

		class IMListener : public AdMobListener {
		public:
			virtual void adViewWillDismissScreen( const std::string &name ) {
				auto iter = _names.find( name );
				if( iter != _names.end() )
				{
					sdkbox::PluginAdMob::cache( name );
					CCLOG( "sdkboxAdmob: cache %s", name.c_str() );
				}
			}

			void adViewDidReceiveAd(const std::string &name) {
				CCLOG( "sdkboxAdmob: adViewDidReceiveAd %s", name.c_str() );
			}
			void adViewDidFailToReceiveAdWithError(const std::string &name, const std::string &msg) {
				CCLOG( "sdkboxAdmob: adViewDidFailToReceiveAdWithError %s %s", name.c_str(), msg.c_str() );
			}
			void adViewWillPresentScreen(const std::string &name) {
				CCLOG( "sdkboxAdmob: adViewWillPresentScreen %s", name.c_str() );
			}
			void adViewDidDismissScreen(const std::string &name) {
				CCLOG( "sdkboxAdmob: adViewDidDismissScreen %s", name.c_str() );
			}
			void adViewWillLeaveApplication(const std::string &name) {
				CCLOG( "sdkboxAdmob: adViewWillLeaveApplication %s", name.c_str() );
			}
		private:
			std::set<std::string> _names;
		};

		void init(std::string bannerName, std::string interstitialName)
		{
			_bannerName = bannerName;
			_interstitialName = interstitialName;
			sdkbox::PluginAdMob::init();
			sdkbox::PluginAdMob::setTestDevices( "39ba8a01b59545c2" );
			sdkbox::PluginAdMob::setTestDevices( "f9f67849b9761228" );
			sdkbox::PluginAdMob::setTestDevices( "4c39e129afded713" );
			sdkbox::PluginAdMob::setTestDevices( "a3efb4a746476a1d" );
			sdkbox::PluginAdMob::setTestDevices( "71587f7e5564e071" );
			sdkbox::PluginAdMob::setListener( new IMListener() );
			cacheInterstitial();
			cacheBanner();
			CCLOG( "sdkboxAdmob: init %s, %s", _bannerName.c_str(), _interstitialName.c_str() );
		}

		void showInterstitial()
		{
			if( _interstitialName.empty() ) return;
			CCLOG( "sdkboxAdmob: showInterstitial %s", _interstitialName.c_str() );
			if( isInterstitialAvailabled() )
				sdkbox::PluginAdMob::show( _interstitialName );
			else
				cacheInterstitial();
		}

		void showBanner()
		{
			if( _bannerName.empty() ) return;
			if( isBannerAvailabled() )
				sdkbox::PluginAdMob::show( _bannerName );
			else 
				cacheBanner();
			CCLOG( "sdkboxAdmob: showBanner %s", _bannerName.c_str() );
		}

		void hideBanner()
		{
			if( _bannerName.empty() ) return;
			sdkbox::PluginAdMob::hide( _bannerName );
			CCLOG( "sdkboxAdmob: hideBanner %s", _bannerName.c_str() );
		}

		bool isInterstitialAvailabled()
		{
			if( _interstitialName.empty() == false )
				return sdkbox::PluginAdMob::isAvailable( _interstitialName );
			else 
				return false;
		}

		bool isBannerAvailabled()
		{
			if( _bannerName.empty() == false )
				return sdkbox::PluginAdMob::isAvailable( _bannerName );
			else
				return false;
			CCLOG( "sdkboxAdmob: isBannerAvailabled %s", _bannerName.c_str() );
		}

		void cacheInterstitial()
		{
			if( _interstitialName.empty() ) return;
			sdkbox::PluginAdMob::cache( _interstitialName );
			CCLOG( "sdkboxAdmob: cacheInterstitial %s", _interstitialName.c_str() );
		}

		void cacheBanner()
		{
			if( _bannerName.empty() ) return;
			sdkbox::PluginAdMob::cache( _bannerName );
			CCLOG( "sdkboxAdmob: cacheBanner %s", _bannerName.c_str() );
		}
	}
}