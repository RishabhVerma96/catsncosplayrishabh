#include "PluginAdMob/PluginAdMob.h"
#include <jni.h>
#include "cocos2d.h"
#include "sdkboxAdmob.h"
#include "platform/android/jni/JniHelper.h"

#define JAVA(method)JNIEXPORT void JNICALL  Java_com_stereo7_sdkbox_admob_SdkboxAdmob_##method

extern
"C"
{
	JAVA( nativeInit )(JNIEnv* env, jobject thiz, jstring bannerName, jstring interstitialName) {
		std::string banner = cocos2d::JniHelper::jstring2string( bannerName );
		std::string interstitial = cocos2d::JniHelper::jstring2string(interstitialName);
		sdkbox::admob::init( banner, interstitial );
	}
}