//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __SdkboxAdMob_h__
#define __SdkboxAdMob_h__
#include <string>
namespace sdkbox
{
	namespace admob
	{
		void init(std::string bannerName, std::string interstitialName);
		void showInterstitial();
		void showBanner();
		void hideBanner();
		bool isInterstitialAvailabled();
		bool isBannerAvailabled();
		void cacheInterstitial();
		void cacheBanner();
	}
}
#endif
