//
//  AdMob_dummy.cpp
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 25.11.14.
//
//

#include "sdkboxAdmob.h"
#include <set>

namespace sdkbox
{
	namespace admob
	{
		void init( std::string, std::string interstitialName ) {}

		void showInterstitial() {}

		void showBanner() {}

		void hideBanner() {}

		bool isInterstitialAvailabled() { return false; }

		bool isBannerAvailabled() { return false; }

		void cacheInterstitial() {}

		void cacheBanner() {}
	}
}