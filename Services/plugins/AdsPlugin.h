//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 11.07.2015
//
//
#ifndef __AdsPlugin_h__
#define __AdsPlugin_h__

#include <map>
#include "ml/Singlton.h"
#include "ml/ParamCollection.h"
#include "ml/Observer.h"

class AdsPlugin : public Singlton<AdsPlugin>
{
public:
	enum class Result
	{
		Ok,
		Fail,
		Canceled,
	};
	enum class Service
	{
		none,
		admob, 
		chartboost,
		vungle,
		flurry,
		facebook,
		supersonic,
		fyber,
		deltadna,
		ogury,
        appodeal,
		chartboostAdmob,
		sdkboxAdmob,
        ironSource,
	};
	enum class Type
	{
		interstitialBanner,
		rewardVideo,
		OfferWall,
		statistic,
		banner,
	};
	static Service strToService( const std::string& service );
public:
	ObServer < AdsPlugin, std::function< void()> > observerVideoStarted;
	ObServer < AdsPlugin, std::function< void( bool )> > observerVideoResult;
	void useOgury( bool use ){ _useOgury = use; }
public:
	void use( Type type, Service service );
	void use( Type type, const std::string& service );

	void cacheVideo();
	void cacheInterstitial();
	void cacheBanner();

	bool isInterstitialAvailabled();
	void showInterstitialBanner();
	void showOfferWall();
	void showVideo();
	void showBanner();
	void hideBanner();

	bool isVideoAvailabled();
	void sendStatistic( const ParamCollection& params );

	void onVideoStarted();
	void onVideoFinihed( Result result, int reward );
	void onVideoNoOffers();
	void oguryAnswer( bool success );
    
    void initIronSource();
    void showOfferWallIronSource();
protected:
	void update( float dt );
	bool videoSS();
	bool videoVugle();
	bool videoFyber();
    bool videoAppodeal();
    bool videoChartboost();
private:
	enum class Event
	{
		video_finished_ok,
		video_finished_failed,
		video_started_ok,
		video_started_failed,
		video_nooffers,
	};
	std::mutex _mutex;
	std::deque< Event > _deque;

	friend Singlton < AdsPlugin > ;
	AdsPlugin();
	virtual ~AdsPlugin();
	virtual void onCreate() override;

	std::map<Type, std::vector<Service>> _servises;
	bool _useOgury;
	bool _waitOguryAnswer;
};


#endif
