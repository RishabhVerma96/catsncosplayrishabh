//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include <assert.h>
#include "AdsPlugin.h"
#include "ml/common.h"
#include "ml/Language.h"
#include "../admob/AdMob.h"
#include "../flurry/flurry_.h"
#include "../facebook/facebook.h"
#include "../dosads/dosads.h"
#include "../chartboost/chartboost_.h"
#include "../vungle/vungle_.h"
#include "../fyber/fyber.h"
#include "../deltadna/deltadna.h"
#include "../appodeal/appodeal_.h"
#include "../sdkbox/admob/sdkboxAdmob.h"
#include "AudioEngine.h"
#include "IronSource.h"

USING_NS_CC;

AdsPlugin::Service AdsPlugin::strToService( const std::string& service )
{
	if( service == "admob" )
		return AdsPlugin::Service::admob;
	else if( service == "chartboost" )
		return AdsPlugin::Service::chartboost;
	else if( service == "supersonic" )
		return AdsPlugin::Service::supersonic;
	else if( service == "fyber" )
		return AdsPlugin::Service::fyber;
	else if( service == "deltaDNA" )
		return AdsPlugin::Service::deltadna;
	else if( service == "ogury" )
		return AdsPlugin::Service::ogury;
	else if( service == "chartboostAdmob" )
		return AdsPlugin::Service::chartboostAdmob;
	else if( service == "appodeal" )
		return AdsPlugin::Service::appodeal;
	else if( service == "vungle" )
		return AdsPlugin::Service::vungle;
	else if( service == "sdkboxAdmob" )
		return AdsPlugin::Service::sdkboxAdmob;
	else  if( service == "none" )
		return AdsPlugin::Service::none;
	else  if( service == "flurry" )
		return AdsPlugin::Service::flurry;
	else  if( service == "facebook" )
		return AdsPlugin::Service::facebook;
    else if( service == "ironsource" ){
        return AdsPlugin::Service::ironSource;
    }
	assert( 0 );
	return AdsPlugin::Service::none;
}

AdsPlugin::AdsPlugin()
: _useOgury(false)
, _waitOguryAnswer(false)
{
}

AdsPlugin::~AdsPlugin()
{
}

void AdsPlugin::onCreate()
{
    auto scheduler = Director::getInstance()->getScheduler();
    ccSchedulerFunc func = std::bind( &AdsPlugin::update, this, std::placeholders::_1 );
    scheduler->schedule( func, this, 1, false, "adsplugin_updater" );
}

void AdsPlugin::initIronSource()
{
    ironsource::initIronSource();
}

void AdsPlugin::showOfferWallIronSource()
{
    ironsource::showOfferWall();
}

void AdsPlugin::use( Type type, Service service )
{
	_servises[type].push_back( service );
}

void AdsPlugin::use( Type type, const std::string& service )
{
	auto value = strToService( service );
	if( type == Type::interstitialBanner && value == Service::chartboostAdmob )
	{
		use( type, Service::chartboost );
		use( type, Service::admob );
	}
	else
	{
		use( type, value );
	}
}

void AdsPlugin::cacheVideo()
{
	if( _servises.find( Type::rewardVideo) == _servises.end() )
		return;
	for( auto type : _servises.at( Type::rewardVideo ) )
	{
		switch( type )
		{
			case Service::fyber:
			{
				fyber::cacheVideo();
				break;
			}
			default:
				break;
		}
	}
}

void AdsPlugin::cacheInterstitial()
{
	if( _servises.find( Type::interstitialBanner) == _servises.end() )
		return;
	for( auto type : _servises.at( Type::interstitialBanner ) )
	{
		switch( type )
		{
			case Service::fyber:
				fyber::cacheInterstitial();
				break;
			case Service::sdkboxAdmob:
				sdkbox::admob::cacheInterstitial();
			default:
				break;
		}
	}
}

void AdsPlugin::cacheBanner()
{
	if( _servises.find( Type::banner ) == _servises.end() )
		return;
	for( auto type : _servises.at( Type::banner ) )
	{
		switch( type )
		{
		case AdsPlugin::Service::admob:
			break;
		case AdsPlugin::Service::sdkboxAdmob:
			sdkbox::admob::cacheBanner();
			break;
		default:
			break;
		}
	}
}

bool AdsPlugin::isInterstitialAvailabled()
{
	for( auto type : _servises.at( Type::interstitialBanner ) )
	{
		switch( type )
		{
		case Service::admob: if( AdMob::isAvailabled() )return true;
		case Service::chartboost: if( chartboost::isInterstitialAvailabled() ) return true;
		case Service::appodeal: if( appodeal::isInterstitialAvailabled() ) return true;
		case Service::sdkboxAdmob: if( sdkbox::admob::isInterstitialAvailabled() )return true;
		case Service::fyber: return true;
		case Service::ogury: assert( 0 ); break;
		case Service::deltadna: assert( 0 );break;
		}
	}
	return false;
}

void AdsPlugin::showInterstitialBanner()
{
	if( _servises.find( Type::interstitialBanner) == _servises.end() )
		return;
	if( _useOgury && _waitOguryAnswer == false )
	{
		_waitOguryAnswer = true;
		if( ogury::showInterstitial() )
			return;
	}
	for( auto type : _servises.at( Type::interstitialBanner ) )
	{
		bool isShowed(false);
		switch( type )
		{
			case Service::admob:
				isShowed = AdMob::isAvailabled();
				if( isShowed )
					AdMob::showInterstitial();
				break;
			case Service::chartboost:
				isShowed = chartboost::isInterstitialAvailabled();
				if( isShowed )
					chartboost::showInterstitial();
				break;
			case Service::fyber:
				fyber::showInterstitial();
				break;
			case Service::ogury:
				break;
            case Service::appodeal:
                appodeal::showInterstitial();
                break;
			case Service::deltadna:
			{
				auto ss = strTo<bool>( deltadna::get( "use_Supersonic" ) );
				auto ogury = strTo<bool>( deltadna::get( "use_Ogury" ) );
				if( ss )
					supersonic::showInterstitial();
				else if( ogury )
					ogury::showInterstitial();
				else
					chartboost::showInterstitial();
				break;
			}
			case Service::sdkboxAdmob:
				sdkbox::admob::showInterstitial();
				break;
			default:
				break;
		}

		if( isShowed )
			break;
	}
}

void AdsPlugin::showOfferWall()
{
	for( auto type : _servises.at( Type::OfferWall ) )
	{
		switch( type )
		{
			case Service::supersonic:
				supersonic::showOfferwall();
				break;
            default:
                break;
		}
	}
}

void AdsPlugin::showVideo()
{
	if( _servises.find( Type::rewardVideo) == _servises.end() )
		return;
	bool isShowed(false);
	for( auto type : _servises.at( Type::rewardVideo ) )
	{
		isShowed = false;
		switch( type )
		{
			case Service::vungle:
			{
				isShowed = videoVugle();
				break;
			}
			case Service::supersonic:
			{
				isShowed = videoSS();
				break;
			}
			case Service::fyber:
			{
				isShowed = videoFyber();
				break;
			}
            case Service::appodeal:
            {
                isShowed = videoAppodeal();
                break;
            }
			case Service::deltadna:
			{
				auto ss = strTo<bool>( deltadna::get( "use_Supersonic" ) );
				if( ss )
					isShowed = videoSS();
				else
					isShowed = videoVugle();
				break;
			}
            case Service::chartboost:
            {
                isShowed = videoChartboost();
                break;
            }
			default:
				break;
		}
		if( isShowed )
			break;
	}
    if( isShowed == false ) {
		observerVideoResult.pushevent( false );
    }
    else {
        AudioEngine::shared().pauseBackgroundMusic();
    }
}

void AdsPlugin::showBanner()
{
	if( _servises.find( Type::banner ) == _servises.end() )
		return;
	for( auto type : _servises.at( Type::banner ) )
	{
		switch( type )
		{
		case Service::sdkboxAdmob:
			sdkbox::admob::showBanner();
			break;
		default:
			break;
		}
	}
}

void AdsPlugin::hideBanner()
{
	if( _servises.find( Type::banner ) == _servises.end() )
		return;
	for( auto type : _servises.at( Type::banner ) )
	{
		switch( type )
		{
		case Service::sdkboxAdmob:
			sdkbox::admob::hideBanner();
			break;
		default:
			break;
		}
	}
}

bool AdsPlugin::isVideoAvailabled()
{
	bool result( false );
	for( auto type : _servises.at( Type::rewardVideo ) )
	{
		switch( type )
		{
			case Service::vungle:
				result = result || vungle::isVideoAvailabled();
				break;
			case Service::supersonic:
				result = result || supersonic::isVideoAvailabled();
				break;
			case Service::fyber:
				result = result || fyber::isVideoAvailabled();
				break;
			case Service::appodeal:
				result = result || appodeal::isVideoAvailabled();
				break;
			case Service::deltadna:
			{
				auto ss = strTo<bool>( deltadna::get( "use_Supersonic" ) );
				if( ss )
					result = result || supersonic::isVideoAvailabled();
				else
					result = result || vungle::isVideoAvailabled();
				break;
			}
			case Service::chartboost:
				result = result || chartboost::isRewardedVideoAvailabled();
				break;
			default:
				break;
		}
	}
	return result;
}

void AdsPlugin::sendStatistic( const ParamCollection& params )
{
	for( auto type : _servises.at( Type::statistic ) )
	{
		switch( type )
		{
			case Service::flurry:
				flurry::logEvent( params );
				break;
			case Service::facebook:
				facebook::logEvent( params );
				break;
			default:
				assert(0);
		}
	}
}

void AdsPlugin::onVideoStarted()
{
	_mutex.lock();
	_deque.push_back( Event::video_started_ok );
	_mutex.unlock();
}

void AdsPlugin::onVideoFinihed( Result result, int reward )
{
	_mutex.lock();
	_deque.push_back( result == Result::Ok? Event::video_finished_ok : Event::video_finished_failed );
	_mutex.unlock();
    AudioEngine::shared().resumeBackgroundMusic();
}

void AdsPlugin::onVideoNoOffers()
{
	MessageBox( WORD( "novideoads_now" ).c_str(), "" );
	onVideoFinihed( AdsPlugin::Result::Fail, 0 );
}

void AdsPlugin::oguryAnswer( bool success )
{
	if( success == false )
	{
		showInterstitialBanner();
	}
	_waitOguryAnswer = false;
}

void AdsPlugin::update( float dt )
{
	_mutex.lock();
	for( auto event : _deque )
	{
		switch( event )
		{
		case Event::video_finished_ok:
			observerVideoResult.pushevent( true );
			break;
		case Event::video_finished_failed:
			observerVideoResult.pushevent( false );
			break;
		case Event::video_started_ok:
		    observerVideoStarted.pushevent();
			break;
		case Event::video_started_failed:
			break;
		case Event::video_nooffers:
			cocos2d::MessageBox( "No ads available. Please try again later.", "" );
			break;
		}
	}
	_deque.clear();
	_mutex.unlock();
}

bool AdsPlugin::videoSS()
{
	if( supersonic::isVideoAvailabled() ) { supersonic::playAd(); return true; }
	return false;
}

bool AdsPlugin::videoVugle()
{
	if( vungle::isVideoAvailabled() ) { vungle::playAd(); return true; }
	return false;
}

bool AdsPlugin::videoFyber()
{
    fyber::playAd();
    return true;
}

bool AdsPlugin::videoAppodeal()
{
    if( appodeal::isVideoAvailabled() )
    {
        appodeal::playAd();
        return true;
    }
    return false;
}

bool AdsPlugin::videoChartboost()
{
    if( chartboost::isRewardedVideoAvailabled() )
    {
        chartboost::showRewardedVideo();
        return true;
    }
	else
	{
		cocos2d::MessageBox( "Please try again later.", "No ads available" );
	}
    return false;
}
