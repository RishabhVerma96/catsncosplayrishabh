LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../cocos2d/cocos)

LOCAL_MODULE := services_static

LOCAL_MODULE_FILENAME := libservices

JANSSON_FILES  := $(wildcard $(LOCAL_PATH)/NDKHelper/jansson/*.c)
JANSSON_FILES  := $(JANSSON_FILES:$(LOCAL_PATH)/%=%)

LOCAL_SRC_FILES := \
$(JANSSON_FILES) \
NDKHelper/NDKHelper/NDKHelper.cpp \
NDKHelper/NDKHelper/NDKCallbackNode.cpp \
NDKHelper/NDKHelper/CallFuncNV.cpp \
admob/AdMob_android.cpp \
appodeal/appodeal_android.cpp \
inapp/Purchase_android.cpp \
inapp/Purchase.cpp \
vungle/vungle_android.cpp \
flurry/flurry_android.cpp \
chartboost/chartboost_android.cpp \
appgratis/appgratis_android.cpp \
dosads/DOSAds_android.cpp \
plugins/AdsPlugin.cpp \
deltadna/deltadna_android.cpp \
fyber/fyber_android.cpp \
playservices/playservices_android.cpp \
playservices/playservices.cpp \
sdkbox/admob/sdkboxAdmob_android.cpp \
facebook/facebook_android.cpp \
sdkbox/admob/sdkboxAdmob.cpp \
sdkbox/play/sdkboxPlay.cpp


LOCAL_C_INCLUDES += $(LOCAL_PATH)/..
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../ml
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../TowerDefenseBase
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../TowerDefenseBase/support
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../ads/sdkbox/jni

LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += adssdkbox_static


include $(BUILD_STATIC_LIBRARY)

$(call import-module,.)
$(call import-module,audio/android)
$(call import-module,../ads/sdkbox/jni)