//
//  Vundle_ios.cpp
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//

#include "vungle_.h"
#include "cocos2d.h"
#include "AppController.h"
#include "../plugins/AdsPlugin.h"

USING_NS_CC;

namespace vungle {
	void playAd()
	{
		[AppController getInstance].vungleCallback = std::bind( [](bool result)
		{
			AdsPlugin::shared().onVideoFinihed(result? AdsPlugin::Result::Ok : AdsPlugin::Result::Fail, 0);
		}, std::placeholders::_1);

		[AppController getInstance].vungleCallbackWillShow = std::bind( []()
		{
			AdsPlugin::shared().onVideoStarted();
		});
		
		[AppController vunglePlayVideo];
	}

	void AdUnavailable( const std::string & args )
	{
	}
	void onCashedAvailabled( const std::string & args )
	{
	}
	void consume()
	{
	}
    bool isVideoAvailabled()
	{
		return true;
	}
}