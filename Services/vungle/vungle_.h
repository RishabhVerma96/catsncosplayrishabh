//
//  Vundle.h
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//

#ifndef IslandDefense_Vundle_h
#define IslandDefense_Vundle_h
#include <functional>

namespace vungle {
	void playAd();
    bool isVideoAvailabled();
}
#endif
