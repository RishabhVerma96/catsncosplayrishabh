//
//  Vunde.cpp
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//

#include "vungle_.h"
#include "cocos2d.h"
#include <jni.h>
#include "platform/android/jni/JniHelper.h"
#include <chrono>
#include "../plugins/AdsPlugin.h"
#include "ml/JniBind.h"


USING_NS_CC;

#define JAVA(method)JNIEXPORT void JNICALL  Java_com_stereo7_extensions_Vungle_##method(JNIEnv*  env, jobject thiz)

static const char * Activity = "com.stereo7.extensions/Vungle";

namespace vungle
{
	void onStart();
	void onFinished();
	void AdUnavailable();
	void onCashedAvailabled();
}

extern
"C"
{
	JAVA(nativeVungleStart) {
		vungle::onStart();
	}
	JAVA(nativeVungleFinished) {
		vungle::onFinished();
	}
	JAVA(nativeVungleAdUnavailable) {
		vungle::AdUnavailable();
	}
	JAVA(nativeVungleCachedAdAvailable) {
		vungle::onCashedAvailabled();
	}
}

namespace vungle {
	static bool succesful = true;
	
	void playAd()
	{
		JavaBind bind( "com.stereo7.extensions", "Vungle", "playAd", "" );
		bind.call();
	}

	void onStart()
	{
		succesful = true;
		AdsPlugin::shared().onVideoStarted();
	}
	void onFinished()
	{
		AdsPlugin::shared().onVideoFinihed(AdsPlugin::Result::Ok, 0);
	}
	void AdUnavailable()
	{
		succesful = false;
	}
	void onCashedAvailabled()
	{
	}

	void consume()
	{
		succesful = false;
	}

    bool isVideoAvailabled()
    {
    	return succesful;
    }

}

#undef JAVA
