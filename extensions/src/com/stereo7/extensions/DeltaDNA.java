package com.stereo7.extensions;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.deltadna.android.sdk.DDNA;
//import com.deltadna.android.sdk.EventBuilder;
import com.deltadna.android.sdk.helpers.NotStartedException;
import com.deltadna.android.sdk.net.DDNANetReq;
import com.deltadna.android.sdk.net.DDNANetReqCb;

public class DeltaDNA{

    public static Activity app;
    
    private static DeltaDNA me;
    private static String appID;
    private static String collectHost;
    private static String enagageHost;
    
    public DeltaDNA(Activity application,
    					 String id,
    					 String collectHostName,
    					 String engageHostName) {
        app = application;
        me = this;
       
        DDNA.inst().init(app.getApplication());
        DDNA.inst().settings().setDebugMode(true);
        
        appID = id;
        collectHost = collectHostName;
        enagageHost = engageHostName;
        
        DDNA.inst().startSDK(appID, collectHost, enagageHost, null);
    }
    
    public void onDestroy(){
        try {
            DDNA.inst().stopSDK();
        } catch (NotStartedException e) {
            e.printStackTrace();
        }
    }
    
    public static native void nativeEngageAnswer( String arg0, String arg1 );
    
    public static void requestEngaged( String arg )
    {
    	if( me == null ) return;
    	
    	class TestEngageCb implements DDNANetReqCb{
    		public TestEngageCb(){}
    		@Override
    		public void done(DDNANetReq req) {
    			Log.d(DDNA.LOGTAG, "Using live engagement (app cb)");
    			if( req == null )
    				return;
    			if( req.responseData() == null )
    				return;
    			
    			String data = req.responseData().toString();
				try {
					JSONObject json  = new JSONObject( data );
					JSONObject params = json.getJSONObject("parameters");
					Iterator<?> keys = params.keys();

					while( keys.hasNext() ) {
					    String key = (String)keys.next();
				    	String value = params.getString(key);
						nativeEngageAnswer( key, value );
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
    		}
    		@Override
    		public void progress(DDNANetReq req) 
    		{
    			Log.d(DDNA.LOGTAG, "progress");
    		}
    		@Override
    		public void failed(DDNANetReq req) 
    		{
    			Log.d(DDNA.LOGTAG, "failed");
    		}
    	}
    	
    	JSONObject params = new JSONObject();
    	try {
			params.put("use_Supersonic", true);
			params.put("use_Ogury", true);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
    	
    	try {
    		DDNA.inst().requestEngagement(arg, params, new TestEngageCb());
    	}catch(NotStartedException e){
    		e.printStackTrace();
    	}    	
    }
}
