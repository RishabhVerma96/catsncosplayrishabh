package com.stereo7.extensions;

import java.util.HashMap;
import java.util.Map;

public class ParamCollection 
{

	private Map <String, String > _params = new HashMap <String, String >(); 
	
	public void fromString ( String string )
	{
		//param:value,param:value;
		int begin = 0;
		while( begin < string.length() )
		{
			int k = string.indexOf(',', begin);
			if( k == -1 ) 
				k = string.length();
			
			
			String pair = string.substring(begin, k);
			
			int l = pair.indexOf(':');
			if( l != -1 )
			{
				String name = pair.substring(0, l);
				String value = pair.substring(l + 1);
				_params.put(name, value);
			}
			else
			{
				_params.put(pair, "");
			}
			begin = k + 1;			
		}
	}

	/*
	public String toString()
	{
		String res = _params.toString();
		
		res = res.substring(1, res.length() - 1);
		return res;
	}
	*/
	
	public final Map<String,String> Map()
	{
		return _params;
	}
	
	public String value( String name )
	{
		String v = _params.get(name);
		return v;
	}
	
	public int Int(String name)
	{
		String v = value(name);
		if( v == "" )
			return 0;
		else
			return Integer.parseInt(v);
	}

	public float Float(String name)
	{
		String v = value(name);
		if( v.isEmpty() ) return 0;
		return Float.parseFloat(v);
	}
}
