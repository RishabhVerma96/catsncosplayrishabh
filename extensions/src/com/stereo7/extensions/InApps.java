package com.stereo7.extensions;

import android.app.Activity;
import android.content.Intent;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.SkuDetails;
import com.anjlab.android.iab.v3.TransactionDetails;


public class InApps implements BillingProcessor.IBillingHandler
{
	private static final String LOG_TAG = "id_inapp";

	private static BillingProcessor billingProcessor;
	private boolean readyToPurchase = false;
	private boolean waitPurchaseResult = false;
	private String productId;
	private static Activity app;
	private static String licenseKey;
	private static InApps me;

	private native static void nativeResultDetails(
			boolean results,
			float priceValue,
			String priceText,
			String productId,
			String description,
			String title,
			String currency );

	private native static void nativeResultPurchase(String result);
	
	public InApps(Activity application, String license) {
        app = application;
        licenseKey = license;
        billingProcessor = new BillingProcessor(app, licenseKey, this);
        me = this;
    }
	
    public void onDestroy() {
    	if( me == null )return;
        if (billingProcessor != null) 
        	billingProcessor.release();
    }
	

    static public void requestDetails( String productID )
	{
    	try{
	    	if( me == null )return;
	    	SkuDetails details = billingProcessor.getPurchaseListingDetails(productID);
	    	if( details != null )
	    	{
	    		nativeResultDetails(
	    				true,
	    				details.priceValue.floatValue(),
	    				details.priceText,
	    				details.productId,
	    				details.description,
	    				details.title,
	    				details.currency );
	    	}
	    	else
	    	{
	    		nativeResultDetails( false, 0, "", productID, "", "", "" );
	    	}
    		
    	}
    	catch(Exception e){
    	}
	}
	static public void purchase( String producTD )
	{
    	if( me == null )return;
		me.waitPurchaseResult = true;
		me.productId = producTD;
		if( billingProcessor!= null )
			billingProcessor.purchase(app, producTD);
	}
	
	static public void consume( String producTD )
	{ 
    	if( me == null )return;
		if( billingProcessor!= null )
			billingProcessor.consumePurchase(producTD);
	}
	
	
    // IBillingHandler implementation

    @Override
    public void onBillingInitialized() {
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
    	if( me == null )return;
		me.waitPurchaseResult = false;
    	String result = new String();
    	if( details != null && productId != null )
    	{
    		result += "result:ok";
    		result += ",id:" + productId;
    		result += ",productId:" + details.productId;
    		result += ",orderId:" + details.orderId;
    		result += ",purchaseToken:" + details.purchaseToken;
    		result += ",purchaseTime:" + details.purchaseTime;
    		if( details.purchaseInfo != null && details.purchaseInfo.signature != null )
    			result += ",purchaseInfo.signature:" + details.purchaseInfo.signature;
    	}
    	nativeResultPurchase( result );
    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
    	if( me == null )return;
		if( waitPurchaseResult == false )return;
		waitPurchaseResult = false;
    	String result = new String();
		result += "result:fail";
		result += ",errorcode:" + errorCode;
		result += ",id" + me.productId;
		if( error != null )
			result += ",errormsg:" + error.getMessage();
		
		nativeResultPurchase( result );
    }

    @Override
    public void onPurchaseHistoryRestored() {
    	if( me == null )return;
    }
    
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
    	if( me == null )return false;
    	if( billingProcessor == null )
    		return false;
        return billingProcessor.handleActivityResult(requestCode, resultCode, data);
    }
}
