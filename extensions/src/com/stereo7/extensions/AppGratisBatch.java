package com.stereo7.extensions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import com.batch.android.Batch;
import com.batch.android.BatchUnlockListener;
import com.batch.android.Config;
import com.batch.android.Feature;
import com.batch.android.Offer;



public class AppGratisBatch implements BatchUnlockListener
{
	static private Activity app;
	Handler handler;
	public final int MSG_ON_REDEEM = 1;
	
	String appKey;
	static AppGratisBatch me;
	android.os.Message msg1;

	@SuppressLint("HandlerLeak") 
	public AppGratisBatch( Activity app, String key)
	{
		this.app = app;
		this.appKey = key;
		me = this;
		
		handler = new Handler()
        {
  	      public void handleMessage(android.os.Message msg) {
  	        switch (msg.what) {
	        	case MSG_ON_REDEEM: 
	        	{
	        		msg1 = msg;
	        		AppGratisBatch.app.runOnUiThread(new Runnable() { @Override public void run() {
		        		nativeOnRedeemFeature((String)msg1.obj); 
	        		} });
	        		break;
	        	}
  	        }
  	      };
  	    };

		Batch.setConfig(new Config(appKey));
	}
	
	@Override
    public void onRedeemAutomaticOffer(Offer offer)
    {
    	if( me == null ) return;
    	
		String result = new String();

		for(Feature feature : offer.getFeatures())
        {
            String featureRef = feature.getReference();
            String value = feature.getValue();

            result += featureRef + ":";
    		result += value + ",";

    		Message msg = new Message();
    		msg.obj = result;
    		msg.what = MSG_ON_REDEEM;
			handler.sendMessage(msg);

            //nativeOnRedeemFeature(result);
        }
		/*
        for(Resource resource : offer.getResources() )
        {
            String resourceRef = resource.getReference();
            int quantity = resource.getQuantity();

            result += "resource" + index + resourceRef + ":";
    		result += ("resourcequantity" + index) + ( quantity + ","); 
    		index = index + 1;

            // Give the given quantity of the resource to the user
        }
        */
    }
	
	native void nativeOnRedeemFeature(String arg);
	
	public void onStart()
    {
    	if( me == null ) return;
    	
        Batch.onStart(app);
        Batch.Unlock.setUnlockListener(this);
    }

	public void onStop()
    {
    	if( me == null ) return;
    	
    	Batch.onStop(app);
    }

	public void onDestroy()
    {
    	if( me == null ) return;
    	
    	Batch.onDestroy(app);
    }

	public void onNewIntent(Intent intent)
    {
    	if( me == null ) return;
    	
       Batch.onNewIntent(app, intent);
    }
}
