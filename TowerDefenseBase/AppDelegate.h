#ifndef  _APP_DELEGATE_H_
#define  _APP_DELEGATE_H_

#include "cocos2d.h"
#include "network/HttpClient.h"

using namespace cocos2d::network;

class  AppDelegate : private cocos2d::Application
{
public:
	AppDelegate();
	virtual ~AppDelegate() override;
	virtual bool applicationDidFinishLaunching() override;
	virtual void applicationDidEnterBackground() override;
	virtual void applicationWillEnterForeground() override;

    virtual void onReceivedMemoryWarning();

protected:
	void linkPlugins();
	void applyConfigurations();
	void loadXmlValues();
	void configurePath();
#ifdef WIN32
public:
	static int screenResolutionX;
	static int screenResolutionY;
#endif
	static std::string gamePath;
	static bool isTestDevice;
};

#endif // _APP_DELEGATE_H_

