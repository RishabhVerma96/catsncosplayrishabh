#if TESTING == 1	
#ifndef __TestLocalisations_h__
#define __TestLocalisations_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "TestCase.h"
NS_CC_BEGIN

class TestLocalisations : public TestCase
{
public:
	TestLocalisations();
	virtual bool execute()override;
private:
};

NS_CC_END
#endif // #ifndef TestLocalisations
#endif