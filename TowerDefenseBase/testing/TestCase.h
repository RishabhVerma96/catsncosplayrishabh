#if TESTING == 1	
#ifndef __TestCase_h__
#define __TestCase_h__
#include "cocos2d.h"
#include "Testing.h"
NS_CC_BEGIN

class TestCase
{
public:
	virtual ~TestCase() {}
	virtual bool execute() = 0;
};

NS_CC_END
#endif // #ifndef __TestCase_h__
#endif
