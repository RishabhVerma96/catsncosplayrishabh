#if TESTING == 1	
#ifndef __TestUnits_h__
#define __TestUnits_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "TestCase.h"
#include "effects.h"
#include "support.h"

NS_CC_BEGIN

class mlEffect;

class TestUnits : public TestCase
{
public:
	TestUnits();
	virtual bool execute()override;
	static void buildUnitsList( std::list<std::string>& units );
private:
	bool isExistAtlases( const std::string& creep );
	bool isNamesValid( const std::string& creep );
	bool isExistTutorial( const std::string& creep );
	bool checkTowerFightParameters( const std::string& towername, int level, mlEffect* effectsPrev, float* radiusPrev );
	bool checkTowerLabParameters( const std::string& towername );
	bool checkTowerDamageFeatCreepArmor( const std::string& towername );

	mlEffect getEffect( const std::string& towername, int level );
	std::string getAttribute( const std::string& towername, int level, const std::string& attrname );
	std::vector<UnitLayer::type> getAllowTargets( const std::string& towername, int level );
};

NS_CC_END
#endif // #ifndef TestLevels
#endif