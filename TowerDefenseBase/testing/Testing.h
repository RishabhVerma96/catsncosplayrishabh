#if TESTING == 1	
#ifndef __Testing_h__
#define __Testing_h__
#include "cocos2d.h"
#include "macroses.h"
#include "Singlton.h"
#include "xmlLoader.h"
#include "Generics.h"
NS_CC_BEGIN

class TestCase;

class Testing : public Singlton<Testing>
{
	friend class Singlton<Testing>;
	Testing();
	virtual void onCreate()override;
public:
	static bool logToConsole;
	static void Message( const std::string& str, const std::string& parameter );
	//static void Message( const std::list<std::string>& list, const std::string& caption );
	int execute();
	bool isError( const std::string& key );
	template<class T>T getParam( const std::string& key ) { return strTo<T>( _params[key] ); }
private:
	void defaultParameters();
	void loadParameters();
private:
	std::map<std::string, std::string> _params;
	std::vector< std::shared_ptr<TestCase> > _cases;
	std::list<std::string> _messages;
	std::list<std::string> _errors;
	bool _isExistError;
};




NS_CC_END
#endif // #ifndef Testing
#endif
