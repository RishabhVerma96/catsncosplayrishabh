#if TESTING == 1	
#include "TestUnits.h"
#include "ml/common.h"
#include "LoadLevelScene.h"
#include "Unit.h"
#include "tower.h"

namespace TU
{
	auto remove_spaces = []( const std::string & desc )
	{
		std::string result = desc;
		const std::string spaces( " \n\t\r" );
		std::string::size_type k( 0 );
		while( (k = result.find_last_of( spaces )) != std::string::npos )
		{
			result.erase( k, 1 );
		}
		return result;
	};
	auto getType = []( const std::string & desc )
	{
		std::string type;
		auto k = desc.find( "[" );
		if( k != std::string::npos )
			type = desc.substr( 0, k );
		return type;
	};
	auto getParams = []( const std::string & desc )
	{
		std::string params;
		auto k = desc.find( "[" );
		if( k != std::string::npos )
		{
			int count( 1 );
			int l = k + 1;
			for( ; l < (int)desc.size() && count != 0; ++l )
			{
				if( desc[l] == '[' )++count;
				else if( desc[l] == ']' )--count;
			}
			params = desc.substr( k + 1, l - k - 2 );
		}
		return params;
	};
	auto getAttrs = []( const std::string & params )
	{
		std::vector<std::string> attr;
		int count = 0;
		int l = 0;
		for( unsigned r = 0; r < params.size(); ++r )
		{
			if( params[r] == '[' )++count;
			else if( params[r] == ']' )--count;
			if( count == 0 && params[r] == ',' )
			{
				attr.push_back( params.substr( l, r - l ) );
				l = r + 1;
			}
		}
		attr.push_back( params.substr( l ) );
		return attr;
	};
	auto buildAnimation = []( float duration, const std::string value )
	{
		auto _folder = []( std::string & string )
		{
			std::string result;
			auto k = string.find( "folder:" );
			if( k == 0 || k == 1 )
			{
				unsigned l = 0;
				for( l = 0; l < string.size(); ++l )
				{
					if( string[k + l] == ',' )
						break;
				}
				result = string.substr( k + 7, l - 7 );
				string = string.substr( k + l + 1 );
			}
			return result;
		};
		auto _frames = []( std::string & string, const std::string& folder )
		{
			auto _list = [string, folder]()mutable
			{
				std::list<std::string> list;
				std::vector<std::string> frames;
				auto k = string.find( "frames:" );
				if( k == 0 || k == 1 )
				{
					string = string.substr( k + 7 );
				}
				if( string.back() == ']' )
					string.pop_back();
				split( list, string );
				for( auto & frame : list )
				{
					frames.push_back( folder + frame );
				}
				return frames;
			};
			auto _indexes = [string, folder]()mutable
			{
				std::string _indexes( "indexes:" );
				std::list<std::string> list;
				std::vector<std::string> frames;
				auto k = string.find( _indexes );
				if( k == 0 || k == 1 )
					string = string.substr( k + _indexes.size() );
				if( string.back() == ']' )
					string.pop_back();
				split( list, string );
				assert( list.size() >= 2 );

				std::string frame = list.front();
				std::string ext;
				k = frame.find_last_of( "." );
				ext = frame.substr( k );
				frame = frame.substr( 0, k );
				list.pop_front();

				std::string indexformat;
				std::vector<int> indexes;
				while( list.empty() == false )
				{
					std::string string = list.front();
					auto k = string.find( ":" );
					if( k == std::string::npos )
					{
						int index = strTo<int>( string );
						indexes.push_back( index );

						if( indexformat.size() < string.size() )
							indexformat = string;
					}
					else
					{
						std::string a = string.substr( 0, k );
						std::string b = string.substr( k + 1 );
						if( indexformat.size() < a.size() ) indexformat = a;
						if( indexformat.size() < b.size() ) indexformat = b;
						int l = strTo<int>( a );
						int r = strTo<int>( b );
						for( int i = l; i != r; (r > l ? ++i : --i) )
						{
							indexes.push_back( i );
						}
						indexes.push_back( r );
					}
					list.pop_front();
				}

				std::string format( "%0" + toStr( (int)indexformat.size() ) + "d" );
				for( auto i : indexes )
				{
					char buffer[8];
					sprintf( buffer, format.c_str(), i );
					std::string frameext = frame + buffer + ext;
					std::string name = folder + frameext;
					frames.push_back( name );
				}

				return frames;
			};
			if( string.find( "frames:" ) != std::string::npos )
				return _list();
			else if( string.find( "indexes:" ) != std::string::npos )
				return _indexes();
			assert( 0 );
			return std::vector<std::string>();
		};

		auto str = value;
		auto folder = _folder( str );
		auto frames = _frames( str, folder );

		return frames;
	};

	void parseAction( std::vector<std::string>& out, const std::string& desc )
	{
		const std::string& cleared_desc = xmlLoader::macros::parse( TU::remove_spaces( desc ) );
		const std::string& type = TU::getType( cleared_desc );
		const std::string& params = TU::getParams( cleared_desc );
		const std::vector<std::string>& attr = TU::getAttrs( params );

		if( type == xmlLoader::k::ActionSequence ||
			type == xmlLoader::k::ActionSpawn ||
			type == xmlLoader::k::ActionRepeat ||
			type == xmlLoader::k::ActionRepeatForever )
		{
			const std::vector<std::string> & sactions = TU::getAttrs( params );
			for( auto& saction : sactions )
			{
				parseAction( out, saction );
			}
		}
		else if( type == xmlLoader::k::ActionAnimate )
		{
			auto frames = buildAnimation( 0, attr[1] );
			out.insert( out.end(), frames.begin(), frames.end() );
		}
	};
};

TestUnits::TestUnits()
{}

void TestUnits::buildUnitsList( std::list<std::string>& outunits )
{
	bool result = true;
	std::set<std::string> units;
	for( int i = 0; i < 1000; ++i )
	{
		auto file = "ini/maps/map" + toStr( i ) + ".xml";
		if( FileUtils::getInstance()->isFileExist( file ) == false )
			break;
		LoadLevelScene::parceLevel( units, GameMode::normal, i );
		LoadLevelScene::parceLevel( units, GameMode::hard, i );
		LoadLevelScene::parceLevel( units, GameMode::survival, i );
	}
	for( auto unit : units )
	{
		outunits.push_back( unit );
	}
}

bool TestUnits::execute()
{
	log( "info: start tests: units" );
	bool result = true;
	std::list<std::string> units;
	std::list<std::string> towers;
	buildUnitsList( units );
	mlTowersInfo::shared().fetch( towers );
	for( auto unit : units )
	{
		bool r = true;
		r = isExistAtlases( unit ) && r;
		r = isNamesValid( unit ) && r;
		r = isExistTutorial( unit ) && r;
		result = r && result;
		log( "info: test creep [%s] is %s", unit.c_str(), r ? "success" : "not success" );
	}
	for( auto tower : towers )
	{
		bool r = true;
		mlEffect effect( nullptr );
		float radius( 0 );
		for( int i = 1; i <= 5; ++i )
		{
			r = checkTowerFightParameters( tower, i, &effect, &radius ) && r;
		}
		r = checkTowerLabParameters( tower ) && r;
		r = checkTowerDamageFeatCreepArmor( tower ) && r;
		result = r && result;
		log( "info: test tower [%s] is %s", tower.c_str(), r ? "success" : "not success" );
	}
	return result;
}

bool TestUnits::isExistAtlases( const std::string& creep )
{
	bool result( true );
	auto checkXml = []( pugi::xml_node node, const std::string& atlas )
	{
		for( auto& child : node )
		{
			std::string name = child.attribute( "name" ).as_string();
			if( name == atlas )
				return true;
		}
		return false;
	};

	pugi::xml_document doc1, doc2, doc3;
	doc1.load_file( "ini/maings/mainlayer.xml" );
	doc2.load_file( "ini/gamescene/resources.xml" );
	doc3.load_file( ("ini/units/" + creep + ".xml").c_str() );
	auto xmlLoadingMain = doc1.root().first_child().child( "resources" ).child( "atlases" );
	auto xmlLoading = doc2.root().first_child().child( "resources" );

	std::vector<pugi::xml_node> nodes;
	nodes.push_back( xmlLoadingMain );
	for( auto& node : xmlLoading )
	{
		if( strcmp( node.attribute( "name" ).as_string(), "game" ) == 0 )
			nodes.push_back( node );
		if( node.attribute( "name" ).as_string() == creep )
			nodes.push_back( node );
	}

	std::set<std::string> atlases;
	std::set<std::string> files;
	auto root = doc3.root().first_child();
	auto actions = root.child( "actions" );

	for( auto child : actions )
	{
		auto desc = child.attribute( "value" ).as_string();
		std::vector<std::string>frames;
		TU::parseAction( frames, desc );
		for( auto frame : frames )
		{
			int k = frame.find( "::" );
			if( k != std::string::npos )
				atlases.insert( frame.substr( 0, k ) );
			else
				files.insert( frame );
		}
	}
	for( auto atlas : atlases )
	{
		bool find( false );
		for( auto node : nodes )
		{
			find = find || checkXml( node, atlas );
		}
		if( find == false )
		{
			std::stringstream ss;
			ss << "cannot find atlas [" + atlas + "] for unit [" + creep + "]";
			Testing::Message( ss.str(), "resources" );
			result = result && (Testing::shared().isError( "resources" ) == false);
		}
	}
	for( auto file : files )
	{
		if( FileUtils::getInstance()->isFileExist( file ) == false )
		{
			std::stringstream ss;
			ss << "cannot find file [" + file + "] for unit [" + creep + "]";
			Testing::Message( ss.str(), "unitfile" );
			result = result && (Testing::shared().isError( "unitfile" ) == false);
		}
	}
	return result;
}

bool TestUnits::isNamesValid( const std::string& creep )
{
	bool result( true );
	pugi::xml_document doc;
	doc.load_file( ("ini/units/" + creep + ".xml").c_str() );
	auto name = doc.root().first_child().attribute( "name" ).as_string();
	if( name != creep )
	{
		auto msg( "Creep with file [" + creep + "] have name [" + name + "]" );
		Testing::Message( msg, "unitname" );
		result = result && (Testing::shared().isError( "unitname" ) == false);
	}
	return result;
}

bool TestUnits::isExistTutorial( const std::string& creep )
{
	bool result( true );
	int index = strTo<int>( creep.substr( creep.size() - 1 ) );
	if( index < 2 )
	{
		bool r = FileUtils::getInstance()->isFileExist( "ini/tutorial/units/" + creep + ".xml" );
		if( !r )
		{
			auto msg( "Creep with name [" + creep + "] havent tutorial" );
			Testing::Message( msg, "unittutorial" );
			result = result && (Testing::shared().isError( "unittutorial" ) == false);
		}
	}
	return result;
}

bool TestUnits::checkTowerFightParameters( const std::string& towername, int level, mlEffect* effectsPrev, float* radiusPrev )
{
	bool result = false;
	std::string 
		folder= "ini/units/", 
		file = towername + toStr(level) + ".xml", 
		path = folder + file;
	do
	{
		CC_BREAK_IF( !FileUtils::getInstance()->isFileExist( path ) );
		result = true;

		const mlEffect& effects = getEffect( towername, level );

		if( effectsPrev )
		{
#define CHECK_PARAM( param ) effectsPrev->param < effects.param;
#define CHECK_PARAM_DEC( param ) effectsPrev->param > effects.param;
			bool inc_damage = CHECK_PARAM( positive.damage );
			bool inc_iceRate = CHECK_PARAM( positive.iceRate );
			bool inc_fireRate = CHECK_PARAM( positive.fireRate );
			bool inc_electroRate = CHECK_PARAM( positive.electroRate );
			bool inc_velocityRate = CHECK_PARAM_DEC( positive.velocityRate );

			bool dec_damage = CHECK_PARAM_DEC( positive.damage );
			bool dec_iceRate = CHECK_PARAM_DEC( positive.iceRate );
			bool dec_fireRate = CHECK_PARAM_DEC( positive.fireRate );
			bool dec_electroRate = CHECK_PARAM_DEC( positive.electroRate );
			bool dec_velocityRate = CHECK_PARAM( positive.velocityRate );

			int sumInc = inc_damage + inc_iceRate + inc_fireRate + inc_electroRate + inc_velocityRate;
			int sumDec = dec_damage + dec_iceRate + dec_fireRate + dec_electroRate + dec_velocityRate;
			if( sumInc == 0 )
			{
				auto msg( "Tower with name [" + towername + "] not increase damage on upgrade to level [" + toStr(level) + "]" );
				Testing::Message( msg, "tower_damageonupgrades" );
				result = result && (Testing::shared().isError( "tower_damageonupgrades" ) == false);
			}
			if( sumDec > 0 )
			{
				auto msg( "Tower with name [" + towername + "] decrease damage on upgrade to level [" + toStr(level) + "]" );
				Testing::Message( msg, "tower_damageonupgradesdec" );
				result = result && (Testing::shared().isError( "tower_damageonupgradesdec" ) == false);
			}
#undef CHECK_PARAM
#undef CHECK_PARAM_DEC
			effectsPrev->copyFrom( effects );
		}
		if( radiusPrev )
		{
			auto radius = strTo<float>( getAttribute( towername, level, "radius" ) );
			if( radius < *radiusPrev )
			{
				auto msg( "Tower with name [" + towername + "] decrease radius on upgrade to level [" + toStr( level ) + "]" );
				Testing::Message( msg, "tower_damageonupgradesdec" );
				result = result && (Testing::shared().isError( "tower_damageonupgradesdec" ) == false);
			}
		}
	}
	while( false );
	return result;
}

bool TestUnits::checkTowerLabParameters( const std::string& towername )
{
	bool result( true );
	auto& TI = mlTowersInfo::shared();
	float damage( 0 ), speed( 0 ), range( 0 );
	for( int i = 0; i < 5; ++i ) 
	{
		auto d = TI.get_dmg( towername, i );
		auto r = TI.get_rng( towername, i );
		auto s = TI.get_spd( towername, i );
		if( d < damage )
		{
			auto msg( "Tower with name [" + towername + "] not increase damage on upgrade to level [" + toStr( i ) + "]" );
			Testing::Message( msg, "tower_damageonupgrades" );
			result = result && (Testing::shared().isError( "tower_damageonupgrades" ) == false);
		}
		if( r < range )
		{
			auto msg( "Tower with name [" + towername + "] not increase range on upgrade to level [" + toStr( i ) + "]" );
			Testing::Message( msg, "tower_damageonupgrades" );
			result = result && (Testing::shared().isError( "tower_damageonupgrades" ) == false);
		}
		if( s < speed )
		{
			auto msg( "Tower with name [" + towername + "] not increase speedrate on upgrade to level [" + toStr( i ) + "]" );
			Testing::Message( msg, "tower_damageonupgrades" );
			result = result && (Testing::shared().isError( "tower_damageonupgrades" ) == false);
		}
	}
	return result;
}

bool TestUnits::checkTowerDamageFeatCreepArmor( const std::string& towername )
{
	bool result( true );
	std::list<std::string> units;
	buildUnitsList( units );
	for( auto creep : units )
	{
		auto creepType = mlUnitInfo::shared().info( creep ).layer;
		float damage = 0;
		float armor = mlUnitInfo::shared().info( creep ).armor;
		for( int i = 0; i < 5; ++i )
		{
			auto allowTargets = getAllowTargets( towername, i+1 );
			if( std::find( allowTargets.begin(), allowTargets.end(), creepType ) == allowTargets.end() &&
				std::find( allowTargets.begin(), allowTargets.end(), UnitLayer::any ) == allowTargets.end() )
				continue;

			auto effect = getEffect( towername, i + 1 );
			auto& p = effect.positive;
			bool existOtherDamage =
				(p.electroRate * p.electroTime > 0) ||
				(p.fireRate * p.fireTime > 0) ||
				(p.iceRate * p.iceTime > 0) ||
				(p.velocityRate != 1);
			if( existOtherDamage == false && p.damage <= armor )
			{
				auto msg( "Tower with name [" + towername + "] on level [" + toStr(i+1) + "]. Damage less then armor of creep [" + creep + "]" );
				Testing::Message( msg, "tower_damagelargerarmor" );
				result = result && (Testing::shared().isError( "tower_damagelargerarmor" ) == false);
			}
		}
	}
	return result;
}

mlEffect TestUnits::getEffect( const std::string& towername, int level )
{
	std::string
		folder = "ini/units/",
		file = towername + toStr( level ) + ".xml",
		path = folder + file;

	float radius( 0 );
	pugi::xml_document doc;
	doc.load_file( path.c_str() );
	auto root = doc.root().first_child();
	radius = root.attribute( "radius" ).as_float();
	auto xmlEffect = root.child( "effects" );
	while( !xmlEffect )
	{
		auto template_ = root.attribute( "template" ).as_string();
		pugi::xml_document doc;
		doc.load_file( template_ );
		root = doc.root().first_child();
		if( radius == 0 )
			radius = root.attribute( "radius" ).as_float();
		xmlEffect = root.child( "effects" );
	}

	mlEffect effects( nullptr );
	effects.load( xmlEffect );
	return effects;
}

std::string TestUnits::getAttribute( const std::string& towername, int level, const std::string& attrname )
{
	std::string
		folder = "ini/units/",
		file = towername + toStr( level ) + ".xml",
		path = folder + file;

	float radius( 0 );
	pugi::xml_document doc;
	doc.load_file( path.c_str() );
	auto root = doc.root().first_child();
	std::string param = root.attribute( attrname.c_str() ).as_string();
	while( param.empty() && root )
	{
		auto template_ = root.attribute( "template" ).as_string();
		pugi::xml_document doc;
		doc.load_file( template_ );
		root = doc.root().first_child();
		param = root.attribute( attrname.c_str() ).as_string();
	}
	return param;
}

std::vector<UnitLayer::type> TestUnits::getAllowTargets( const std::string& towername, int level )
{
	std::vector<UnitLayer::type> result;
	std::vector<std::string> ss;
	auto param = getAttribute( towername, level, "allowtargets" );
	split( ss, param );
	for( auto s : ss )
		result.push_back( strToUnitLayer( s ) );
	return result;
}

#endif