#if TESTING == 1	
#ifndef __TestLevels_h__
#define __TestLevels_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "TestCase.h"
NS_CC_BEGIN

class TestLevels : public TestCase
{
public:
	TestLevels();
	virtual bool execute()override;
private:
	bool checkRoutes( int level );
	bool isExistBasePoint( const Point& finishPoint, pugi::xml_node node );
};

NS_CC_END
#endif // #ifndef TestLevels
#endif