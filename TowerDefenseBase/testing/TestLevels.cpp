#if TESTING == 1	
#include "TestLevels.h"
#include "ml/common.h"

NS_CC_BEGIN


TestLevels::TestLevels()
{
}

bool TestLevels::execute()
{
	log( "info: start tests: levels" );
	bool result = true;
	for( int i = 0; i < 1000; ++i )
	{
		auto file = "ini/maps/map" + toStr( i ) + ".xml";
		if( FileUtils::getInstance()->isFileExist( file ) == false )
			break;
		result = checkRoutes( i ) && result;
	}
	return result;
}

bool TestLevels::checkRoutes( int level )
{
	bool result( true );
	pugi::xml_document doc;
	doc.load_file( ("ini/maps/map" + toStr( level ) + ".xml").c_str() );
	auto decorations = doc.root().first_child().child( "decorations" );
	auto routes = doc.root().first_child().child( "routes" );
	for( auto route : routes )
	{
		bool forHero = route.attribute("onlyforhero").as_bool();
		Point point;
		for( auto pointXml : route.child( "main" ) )
		{
			point.x = pointXml.attribute( "x" ).as_float();
			point.y = pointXml.attribute( "y" ).as_float();
		}
		bool r = forHero || isExistBasePoint( point, decorations );
		log( "info: route [%d] %s base point", level, r ? "have" : "have not" );
		if( !r )
		{
			std::stringstream ss;
			ss << "Route havent basepoint. level [" << level + 1 << "]. route index [" << route.attribute( "name" ).as_string() << "]";
			Testing::Message( ss.str(), "levelbasepoint" );
			result = result && (Testing::shared().isError( "levelbasepoint" ) == false);
		}
	}
	return result;
}

bool TestLevels::isExistBasePoint( const Point& finishPoint, pugi::xml_node node )
{
	bool result( false );
	for( auto child : node )
	{
		std::string name = child.name();
		if( name == std::string( "base_point" ) )
		{
			auto x = child.attribute( "x" ).as_float();
			auto y = child.attribute( "y" ).as_float();
			float distanse = finishPoint.getDistance( Point( x, y ) );
			if( distanse < 150 )
			{
				result = true;
				break;
			}
		}
	}
	return result;
}


NS_CC_END
#endif