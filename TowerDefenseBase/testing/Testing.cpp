#if TESTING == 1	
#include "Testing.h"
#include "TestLevels.h"
#include "TestUnits.h"
#include "TestLocalisations.h"
#include <iostream>
#include <fstream>
NS_CC_BEGIN



Testing::Testing()
: _isExistError(false)
{
}

void Testing::onCreate()
{
	defaultParameters();
	loadParameters();

	_cases.push_back( std::make_shared<TestLevels>() );
	_cases.push_back( std::make_shared<TestUnits>() );
	_cases.push_back( std::make_shared<TestLocalisations>() );
}

int Testing::execute()
{
	char buf[1024];
	GetCurrentDirectoryA( 1024, buf );
	log( "info: start testing. Current directory: [%s]", buf);

	bool result( true );
	for( auto testcase: _cases )
	{
		result = testcase->execute() && result;
	}
	result = result && (_isExistError == false);
	return result ? 0 : -1;
}

bool Testing::isError( const std::string& key )
{
	return getParam<std::string>( key ) == "error";
}

void Testing::defaultParameters()
{
	_params["resources"] = "error";
	_params["unitname"] = "warning";
	_params["unitfile"] = "error";
	_params["unittutorial"] = "error";
	_params["levelbasepoint"] = "error";
	_params["tower_damageonupgrades"] = "error";
	_params["tower_damageonupgradesdec"] = "error";
	_params["tower_damagelargerarmor"] = "error";
	_params["localizations"] = "warning";
}

void Testing::loadParameters()
{
	std::fstream fs( "dev/testing.txt", std::ios::in );
	if( !fs.is_open() )
		return;
	while( !fs.eof() )
	{
		char buffer[1024] = { 0 };
		fs.getline( buffer, 1024 );
		std::string line( buffer );
		if( line.empty() || line.front() == '#' )
			continue;
		auto k = line.find( ":" );
		if( k != -1 )
		{
			auto key = line.substr( 0, k );
			auto value = line.substr( k+1 );
			_params[key] = value;
		}
	}
}

bool Testing::logToConsole = false;

void Testing::Message( const std::string& str, const std::string& parameter )
{
	auto say = []( const std::string& str, bool isError )
	{
#ifdef _DEBUG
		Testing::logToConsole = true;
#endif
		std::string caption = isError ? "Error: " : "Warning: ";
		if( logToConsole ) log( (caption + str).c_str() );
		else MessageBox( str.c_str(), caption.c_str() );
	};
	auto error = Testing::shared().isError( parameter );;
	say( parameter + ":" + str, error );
	Testing::shared()._isExistError = Testing::shared()._isExistError || error;
}

NS_CC_END
#endif