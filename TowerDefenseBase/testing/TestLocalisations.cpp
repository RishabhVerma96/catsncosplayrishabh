#if TESTING == 1	
#include "TestLocalisations.h"
#include "Language.h"
#include "Testing.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
#	include <windows.h>
#	include <tchar.h>
#	include <strsafe.h>
#	include <stdio.h>
#	pragma comment(lib, "User32.lib")
#endif
NS_CC_BEGIN

void buildFileList( std::vector<std::string>& files );
void findAllTextsProperties( const std::string& file, std::vector<std::string>& properties );

TestLocalisations::TestLocalisations()
{
}

bool TestLocalisations::execute()
{
	bool result( true );
	bool isError = Testing::shared().isError( "localizations" );
	std::vector<std::string> files;
	buildFileList( files );
	for( auto& file : files )
	{
		std::vector<std::string> properties;
		findAllTextsProperties( file, properties );
		for( auto property : properties )
		{
			bool r = false;
			r = r || property.empty();
			r = r || property.front() == '#';
			auto string = Language::shared().string( property );
			if( property.empty() == false && property.front() == '#' )
			{
				r = r && string.empty() == false;
				r = r && string != property;
			}
			if( !r && property.empty() == false )
			{
				char c = property.front();
				r = c >= '0' && c <= '9';
			}
			result = isError ? !r : result;
			if( !r )
			{
				std::stringstream ss;
				ss << " File: [" << file << "], \n\tid: \t[" << property << "], \n\tvalue: \t[" << string << "].";
				Testing::Message( ss.str(), "localizations" );
			}
		}
	}


	return result;
}

void buildFolders( std::vector<std::string>& folders )
{
	WIN32_FIND_DATAA ffd;
	LARGE_INTEGER filesize;
	CHAR szDir[MAX_PATH];
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;

	GetCurrentDirectoryA( MAX_PATH, szDir );
	std::string currDir = szDir;
	StringCchCatA( szDir, MAX_PATH, "\\*" );
	hFind = FindFirstFileA( szDir, &ffd );

	if( INVALID_HANDLE_VALUE == hFind )
		return;

	do
	{
		if( ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			std::string dir = ffd.cFileName;
			if( dir == "." || dir == ".." ) continue;
			dir = currDir + "\\" + dir;
			folders.push_back( dir );

			SetCurrentDirectoryA( dir.c_str() );
			buildFolders( folders );
			SetCurrentDirectoryA( currDir.c_str() );
		}
		else
		{
			filesize.LowPart = ffd.nFileSizeLow;
			filesize.HighPart = ffd.nFileSizeHigh;
		}
	}
	while( FindNextFileA( hFind, &ffd ) != 0 );

	dwError = GetLastError();
	if( dwError != ERROR_NO_MORE_FILES )
	{
	}

	FindClose( hFind );
	return;
}

void buildFiles( std::vector<std::string>& files, const std::vector<std::string>& folders )
{
	WIN32_FIND_DATAA ffd;
	LARGE_INTEGER filesize;
	CHAR origDir[MAX_PATH];
	CHAR szDir[MAX_PATH];
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;


	GetCurrentDirectoryA( MAX_PATH, origDir );
	for( auto dir : folders )
	{
		SetCurrentDirectoryA( dir.c_str() );
		GetCurrentDirectoryA( MAX_PATH, szDir );
		std::string currDir = szDir;
		StringCchCatA( szDir, MAX_PATH, "\\*.xml" );
		hFind = FindFirstFileA( szDir, &ffd );

		if( INVALID_HANDLE_VALUE == hFind )
			continue;
		do
		{
			if( ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
				continue;
			std::string file = ffd.cFileName;

			file = currDir + "\\" + file;
			std::replace( file.begin(), file.end(), '\\', '/' );
			files.push_back( file );

			filesize.LowPart = ffd.nFileSizeLow;
			filesize.HighPart = ffd.nFileSizeHigh;

		}
		while( FindNextFileA( hFind, &ffd ) != 0 );
	}
	SetCurrentDirectoryA( origDir );

	dwError = GetLastError();
	if( dwError != ERROR_NO_MORE_FILES )
	{
	}

	FindClose( hFind );
	return;
}

void prepareFiles( std::vector<std::string>& files )
{
	CHAR szDir[MAX_PATH];
	GetCurrentDirectoryA( MAX_PATH, szDir );
	std::string currDir = szDir;
	currDir += "\\";
	std::replace( currDir.begin(), currDir.end(), '\\', '/' );
	for( auto& file : files )
	{
		file = file.substr( currDir.size() );
	}
}

void buildFileList( std::vector<std::string>& files )
{
	std::vector<std::string> folders;
	buildFolders(folders);
	buildFiles(files, folders);
	prepareFiles(files);
}

void findAllTextsProperties( const pugi::xml_node& node, std::vector<std::string>& properties )
{
	for( auto& attr : node.attributes() )
	{
		if( strcmp( attr.name(), "text" ) == 0 )
			properties.push_back( attr.value() );
	}
	for( auto& child : node )
	{
		findAllTextsProperties( child, properties );
	}

}

void findAllTextsProperties( const std::string& file, std::vector<std::string>& properties )
{
	pugi::xml_document doc;
	doc.load_file( file.c_str() );
	auto root = doc.root().first_child();
	findAllTextsProperties( root, properties );
}

NS_CC_END
#endif