#include "consts.h"
#include <mutex>
#include "configuration.h"
#include "AppDelegate.h"
#include "scenes/MainGS.h"
#include "scenes/SplashScene.h"
#include "GameLayer.h"
#include "Log.h"
#include "resources.h"
#include "ml/Audio/AudioEngine.h"
#include "ml/Language.h"
#include "MapLayer.h"
#include "ScoreCounter.h"
#include "inapp/Purchase.h"
#include "ml/loadxml/xmlLoader.h"
#include "ml/ParamCollection.h"
#include "ml/ObjectFactory.h"
#include "EventsGame.h"
#include "plugins/AdsPlugin.h"
#include "ShopLayer.h"
#include "ShopLayer2.h"
#include "appgratis/appgratis.h"
#include "ml/ImageManager.h"
#include "game/Achievements.h"
#include "UserData.h"
#include "ml/audio/AudioEngine.h"
#include "playservices/playservices.h"
#include "support/Award.h"
#include "scenes/RouleteLayer.h"
#include "HeroesSquadNode.h"
#include "scenes/lab/LaboratoryTowerItem.h"
#include "scenes/SlotMachineLayer.h"
#include "testing/Testing.h"
#include "ml/MainThreadFunc.h"
#include "game/unit/UnitHomebase.h"
#include "../Services/sdkbox/play/sdkboxPlay.h"
#include "LocalNotification.h"
#include "RapidJson.h"
#include<iostream>

#if EDITOR==1
#	include "editor/Editor.h"
#endif


#ifndef GAME_FOLDER
#	define GAME_FOLDER ""
#endif
const std::string kGameFolder( GAME_FOLDER );

USING_NS_CC;

using namespace rapidjson;

NS_CC_BEGIN;
float DesignScale(1);
bool iPad(true);
bool iPhone(false);
NS_CC_END;

bool g_isFullscreenMode( false );

void createMapsH( int num );
void convertMap( int num );
void registration();
void createWindow();
void setDesignResolution();

#ifdef WIN32
int AppDelegate::screenResolutionX(0);
int AppDelegate::screenResolutionY(0);
#endif
std::string AppDelegate::gamePath;
bool AppDelegate::isTestDevice = false;

AppDelegate::AppDelegate()
{
	//createMapsH(27);
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	addTestDevice( "39ba8a01b59545c2" );
	addTestDevice( "f9f67849b9761228" );
	addTestDevice( "4c39e129afded713" );
	addTestDevice( "a3efb4a746476a1d" );
	addTestDevice( "71587f7e5564e071" );
	addTestDevice( "5afb672983cbaea1" );
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    addTestDevice( "test" );
    addTestDevice( "17C0A2F7-0154-4DE4-A6B0-9CAF7E5B2AD6" );
    addTestDevice( "7D2A3ADC-FE0E-4643-8FFB-DB99475934DB" );
	addTestDevice( "249C79C3-01AA-401E-B9C5-C3CCF19E5C0C" );
	addTestDevice( "A49B8BC4-808E-4A61-A0D8-FE6C9C6C43A9" );
	addTestDevice( "72FFDA14-329C-491D-8CB1-8E60E2853E7D" );
    addTestDevice( "3211ADF1-9EB9-47A3-AD15-B2BC53F1874E" );
    addTestDevice( "3FCFE1A1-FE60-46D3-B536-8A39F167E4C6" );
    addTestDevice( "92C9E6BD-984E-4F93-9A2C-B382C8F40009" );
    addTestDevice( "E2A1BC84-495D-41F1-A358-022BD23E6FC3" );
    addTestDevice( "AB0232EB-6A92-49D6-9FED-99A191CF6DE5" );
	addTestDevice( "4079E335-999F-499C-BFC0-A51B134C9F26" );
	addTestDevice( "7E9983D6-1EC2-4239-80BF-5C26982C1E2C" );
	addTestDevice( "8F2F15AB-0CBF-4888-8B8B-DAA5DA639F69" ); // ZombWar iPad
	addTestDevice( "61D618B1-E56C-4FF5-A831-C95268315597" ); // ZombWar iPhone
    addTestDevice( "FDA6ED8D-6BCD-498A-A291-5A3A110845BC" );
    addTestDevice( "A01F779C-1F95-45ED-8B1C-CE40F2A7E744" );
    addTestDevice( "94D281CC-D698-4C99-9FC6-9A199F9CFC12" );

#endif
}

AppDelegate::~AppDelegate()
{
}

bool AppDelegate::applicationDidFinishLaunching()
{
	time_t t;
	time(&t);
	srand( t );
	appgratis::init();

#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
	FileUtils::getInstance()->addSearchPath( "../Resources" );
#endif

	MainThreadFunc::shared();
	configurePath();
	applyConfigurations();
	if( Config::shared().get<bool>( "lootPickerActiveFromStart" ) )
		UserData::shared().write<bool>( k::user::LootPickerBought, true );
	ScoreByTime::shared();
	LevelParams::shared();
	Achievements::shared();
	Language::shared();
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    sdkbox::play::init();
#endif

    

#if USE_CHEATS == 1
	addTestDevice( getDeviceID() );
	setTestModeActive( true );
#else
	if( isTestDevice )
		addTestDevice( getDeviceID() );
	setTestModeActive( Config::shared().get<bool>( "startInTestMode" ) );
#endif

#if TESTING == 1
	exit( Testing::shared().execute() );
#endif

//	if( Config::shared().get<bool>( "useLeaderboards" ) )
//		PlayServises::init();

#if PC != 1
	auto shopversion = Config::shared().get<int>( "shopversion" );
	inapp::CallBackPurchase cb;
	if( shopversion < 2 )
		cb = std::bind( ShopLayer::request_answer, std::placeholders::_1 );
	else 
		cb = std::bind( ShopLayer2::purchaseResult, std::placeholders::_1 );
	inapp::setCallbackPurchase( cb );
#endif

	auto setSoundEnabled = [] (bool mode){ UserData::shared().write("sound_enabled", mode ? true : false); };
	auto setMusicEnabled = [] (bool mode){ UserData::shared().write("music_enabled", mode ? true : false); };
	auto isSoundEnabled = [] (){ return UserData::shared().get("sound_enabled", true); };
	auto isMusicEnabled = [](){ return UserData::shared().get("music_enabled", true); };
	AudioEngine::callback_setSoundEnabled( std::bind( setSoundEnabled, std::placeholders::_1 ) );
	AudioEngine::callback_setMusicEnabled( std::bind( setMusicEnabled, std::placeholders::_1 ) );
	AudioEngine::callback_isSoundEnabled( std::bind( isSoundEnabled ) );
	AudioEngine::callback_isMusicEnabled( std::bind( isMusicEnabled ) );

	auto setSoundVolume = []( float volume ){ UserData::shared().write( "sound_volume", 1.0f ); };//volume
	auto setMusicVolume = []( float volume ){ UserData::shared().write( "music_volume", 0.3f ); };//volume
	auto getSoundVolume = [](){ return UserData::shared().get( "sound_volume", 1.0f ); };//1.0f
	auto getMusicVolume = [](){ return UserData::shared().get( "music_volume", 0.3f ); };//0.3f
	AudioEngine::callback_setSoundVolume( std::bind( setSoundVolume, std::placeholders::_1 ) );
	AudioEngine::callback_setMusicVolume( std::bind( setMusicVolume, std::placeholders::_1 ) );
	AudioEngine::callback_getSoundVolume( std::bind( getSoundVolume ) );
	AudioEngine::callback_getMusicVolume( std::bind( getMusicVolume ) );

	createWindow();
	setDesignResolution();
	loadXmlValues( );
	registration( );
	linkPlugins();

	if (Config::shared().get<bool>("useLeaderboards"))
		PlayServises::init(false);

#if EDITOR==1
	auto scene = EditorScene::create( );
	Director::getInstance()->runWithScene( scene );
#else
//
    ScenePointer scene;
    if(FileUtils::getInstance()->isFileExist("splash/splash.xml" ))
        scene.reset( SplashScene::create() );
    if( scene == nullptr )
        scene.reset( MainGS::scene() );
    Director::getInstance()->runWithScene( scene );
    
    
#endif
	std::list<std::string> items;
	items.push_back( Config::shared().get( "inappFuel1" ) );
	items.push_back( Config::shared().get( "inappGear1" ) );
	items.push_back( Config::shared().get( "inappGear2" ) );
	items.push_back( Config::shared().get( "inappGear3" ) );
	items.push_back( Config::shared().get( "inappGold1" ) );
	items.push_back( Config::shared().get( "inappGold2" ) );
	items.push_back( Config::shared().get( "inappGold3" ) );
	items.push_back( Config::shared().get( "inappGold4" ) );
	items.push_back( Config::shared().get( "inappGold5" ) );
    for( int i = 1; i <= Config::shared().get<int>( "heroesCount" ); ++i )
    {
        items.push_back( Config::shared().get( "inappPackage" ) + "hero" + toStr(i) );
    }
    items.push_back( Config::shared().get( "inappAllHeroes" ) );
    items.push_back( Config::shared().get( "inappPackHeroes1" ) );

#if CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID
    for( auto& item : items )
    {
        auto cb = std::bind( [](){} );
        inapp::details(item, cb);
    }
#elif CC_TARGET_PLATFORM==CC_PLATFORM_IOS
    inapp::details(Config::shared().get( "inappFuel1" ), [](inapp::SkuDetails){});
#endif

	HeroExp::shared();
#if PC == 1
	showCursor( g_isFullscreenMode );
#endif
    
//    LocalNotification::show("hey there local notification", 20, 1);

	return true;
}

void AppDelegate::applicationDidEnterBackground() {
#if PC == 1
	Director::getInstance()->getEventDispatcher()->dispatchCustomEvent( "lostFocus" );
#endif
    
    Director::getInstance()->stopAnimation();
	AudioEngine::shared().pauseAllEffects();
	AudioEngine::shared().pauseBackgroundMusic();
    ScoreByTime::shared().setInternetConnected(false);
	ScoreByTime::shared().savetime();
}

void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();
	AudioEngine::shared().resumeAllEffects();
	AudioEngine::shared().resumeBackgroundMusic();
	ScoreByTime::shared( ).checktime( );

	//This method call when window lose focus but did not iconified.
	//So we check losing focus here.
#if PC == 1
	auto glview = static_cast<GLViewImpl*>(Director::getInstance()->getOpenGLView());
	HWND hwnd = glview->getWin32Window();
	if( GetForegroundWindow() != hwnd )
	{
		applicationDidEnterBackground();
	}
#endif
}

void AppDelegate::onReceivedMemoryWarning()
{
    //Director::getInstance()->getTextureCache()->removeUnusedTextures();
}

void AppDelegate::applyConfigurations()
{
	Config::shared().setDefaultValue( "allowAllDevicesToTest", "no" );
	Config::shared().setDefaultValue( "startInTestMode", "no" );
	Config::shared().setDefaultValue( "useInapps", "yes" );
	Config::shared().setDefaultValue( "useFuel", "yes" );
	Config::shared().setDefaultValue( "useFreeFuel", "yes" );
	Config::shared().setDefaultValue( "useFreeGold", "yes" );
	Config::shared().setDefaultValue( "freeGoldAsVideo", "no" );
	Config::shared().setDefaultValue( "useStarsForUnlock", "no" );
	Config::shared().setDefaultValue( "useBoughtLevelScoresOnEveryLevel", "yes" );
	Config::shared().setDefaultValue( "useBoughtLevelScoresOnlyRestartLevel", "no" );
	Config::shared().setDefaultValue( "useAds", "yes" );
	Config::shared().setDefaultValue( "useLeaderboards", "no" );
	Config::shared().setDefaultValue( "useEula", "no" );
	Config::shared().setDefaultValue( "useUsersGift", "no" );
	Config::shared().setDefaultValue( "useRateSpeedButton", "no" );
	Config::shared().setDefaultValue( "speedRate", "2" );
	Config::shared().setDefaultValue( "useDailyReward", "no" );
	Config::shared().setDefaultValue( "hideMainLogo", "no" );
	Config::shared().setDefaultValue( "desertBuild", "yes" );
	Config::shared().setDefaultValue( "hideMoreButton", "no" );
	Config::shared().setDefaultValue( "useRateMe", "yes" );
	Config::shared().setDefaultValue( "useLinkToPaidVersion", "no" );
	Config::shared().setDefaultValue( "useRestoreButton", "no" );
	Config::shared().setDefaultValue( "useStatistic", "yes" );
	Config::shared().setDefaultValue( "levelMapSize", "1024x768" );
	Config::shared().setDefaultValue( "resourceGameSceneFolder", "images/gamescene/" );
	Config::shared().setDefaultValue( "resourceHeroRoomFolder", "images/heroroom/" );
	Config::shared().setDefaultValue( "iconForPaidGame", "icons/steampunkpremium.png" );
	Config::shared().setDefaultValue( "designresolution", "default" );
	Config::shared().setDefaultValue( "launchversion", "1" );
	Config::shared().setDefaultValue( "shopversion", "1" );
	Config::shared().setDefaultValue( "lab_usepopup", "no" );
	Config::shared().setDefaultValue( "SmartSceneOpacityBackLayer", "204" );
	Config::shared().setDefaultValue( "towerparameter_maxvalue", "10" );
	Config::shared().setDefaultValue( "blockHeroesBeforeFirstWave", "no" );
	Config::shared().setDefaultValue( "showAdInPause", "no" );
	Config::shared().setDefaultValue( "multipleActiveSkills", "no" );
	Config::shared().setDefaultValue( "levelForLaunchPromo", "5" );
	Config::shared().setDefaultValue( "launchPromoAfterHeroroom", "no" );
	Config::shared().setDefaultValue( "boughtHeroCountDisablePromo", "4" );
	Config::shared().setDefaultValue( "useFreeCoinsButton", "yes" );
	Config::shared().setDefaultValue( "useDialogs", "no" );
	Config::shared().setDefaultValue( "instanttowerbuild", "no" );
	
	Config::shared().setDefaultValue( "online_frequencyGameUpdate", "0.5" );
	Config::shared().setDefaultValue( "online_disconnectGameDuration", "10" );
	Config::shared().setDefaultValue( "online_runbot_duration", "7" );
	Config::shared().setDefaultValue( "scrollerEnabled", "no" );
	Config::shared().setDefaultValue( "showLaboratoryOnEnter", "yes" );

	Config::shared().setDefaultValue( "inappPackage", "" );
	Config::shared().setDefaultValue( "inappGold1", "gold1" );
	Config::shared().setDefaultValue( "inappGold2", "gold2" );
	Config::shared().setDefaultValue( "inappGold3", "gold3" );
	Config::shared().setDefaultValue( "inappGold4", "gold4" );
	Config::shared().setDefaultValue( "inappGold5", "gold5" );
	Config::shared().setDefaultValue( "inappGear1", "gear1" );
	Config::shared().setDefaultValue( "inappGear2", "gear2" );
	Config::shared().setDefaultValue( "inappGear3", "gear3" );
	Config::shared().setDefaultValue( "inappGear4", "gear4" );
	Config::shared().setDefaultValue( "inappGear5", "gear5" );
	Config::shared().setDefaultValue( "inappFuel1", "fuel1" );
	Config::shared().setDefaultValue( "inappFuel2", "fuel2" );
	Config::shared().setDefaultValue( "inappFuel3", "fuel3" );
	Config::shared().setDefaultValue( "inappFuel4", "fuel4" );
	Config::shared().setDefaultValue( "inappFuel5", "fuel5" );
	Config::shared().setDefaultValue( "inappTicket1", "ticket1" );
	Config::shared().setDefaultValue( "inappTicket2", "ticket2" );
	Config::shared().setDefaultValue( "inappTicket3", "ticket3" );
	Config::shared().setDefaultValue( "inappTicket4", "ticket4" );
	Config::shared().setDefaultValue( "inappTicket5", "ticket5" );
	Config::shared().setDefaultValue( "hero2", "hero2" );
	Config::shared().setDefaultValue( "hero3", "hero3" );
	Config::shared().setDefaultValue( "inappPackHeroes1", "packhero1" );
	Config::shared().setDefaultValue( "inappAllHeroes", "allheroes" );
	 
	Config::shared().setDefaultValue( "heroesCountSelectMin", "1" );
	Config::shared().setDefaultValue( "heroesAvailabled", Config::shared().get( "heroesCount" ) );

	Config::shared().setDefaultValue( "lootPickerActiveFromStart", "no" );

	xmlLoader::macros::set( "use_fuel", toStr( Config::shared().get<bool>( "useFuel" ) ) );
	xmlLoader::macros::set( "unuse_fuel", toStr( !Config::shared().get<bool>( "useFuel" ) ) );
	Config::shared().setDefaultValue( "adsTypeBanner", "none" );

	xmlLoader::macros::set( "icon_paid_version", Config::shared().get( "iconForPaidGame" ) );
	SmartScene::setOpacityBackLayer( Config::shared().get<int>( "SmartSceneOpacityBackLayer" ) );

	const Config& map = Config::shared();
	for( auto pair : map )
		xmlLoader::macros::set( pair.first, pair.second );
}

void AppDelegate::linkPlugins()
{
	AdsPlugin::shared().useOgury( false );
	AdsPlugin::shared().use( AdsPlugin::Type::statistic, AdsPlugin::Service::flurry );
	AdsPlugin::shared().use( AdsPlugin::Type::OfferWall, AdsPlugin::Service::supersonic );
	AdsPlugin::shared().use( AdsPlugin::Type::banner, AdsPlugin::Service::none );

	std::string interstitial = Config::shared().get( "adsTypeInterstitial" );
	std::string video = Config::shared().get( "adsTypeRewardVideo" );
	//std::string offerwall = Config::shared().get( "adsTypeOfferWall" );
	std::string banner = Config::shared().get( "adsTypeBanner" );

	AdsPlugin::shared().use( AdsPlugin::Type::interstitialBanner, interstitial );
    AdsPlugin::shared().use( AdsPlugin::Type::rewardVideo, video );

   // AdsPlugin::shared().use( AdsPlugin::Type::OfferWall, offerwall );
	AdsPlugin::shared().use( AdsPlugin::Type::banner, banner );
	/*
	if( interstitial == "admob" )
		AdsPlugin::shared().use( AdsPlugin::Type::interstitialBanner, AdsPlugin::Service::admob );
	if( interstitial == "chartboost" )
		AdsPlugin::shared().use( AdsPlugin::Type::interstitialBanner, AdsPlugin::Service::chartboost );
	if( interstitial == "supersonic" )
		AdsPlugin::shared().use( AdsPlugin::Type::interstitialBanner, AdsPlugin::Service::supersonic );
	if( interstitial == "fyber" )
		AdsPlugin::shared().use( AdsPlugin::Type::interstitialBanner, AdsPlugin::Service::fyber );
	if( interstitial == "deltaDNA" )
		AdsPlugin::shared().use( AdsPlugin::Type::interstitialBanner, AdsPlugin::Service::deltadna );
	if( interstitial == "ogury" )
		AdsPlugin::shared().use( AdsPlugin::Type::interstitialBanner, AdsPlugin::Service::ogury );
	if( interstitial == "chartboostAdmob" )
	{
		AdsPlugin::shared().use( AdsPlugin::Type::interstitialBanner, AdsPlugin::Service::chartboost );
		AdsPlugin::shared().use( AdsPlugin::Type::interstitialBanner, AdsPlugin::Service::admob );
	}
	if( interstitial == "appodeal" )
		AdsPlugin::shared().use( AdsPlugin::Type::interstitialBanner, AdsPlugin::Service::appodeal );

	if( video == "RewardVideoVungle" ) 
		AdsPlugin::shared().use( AdsPlugin::Type::rewardVideo, AdsPlugin::Service::vungle );
	if( video == "RewardVideoSupersonic" ) 
		AdsPlugin::shared().use( AdsPlugin::Type::rewardVideo, AdsPlugin::Service::supersonic );
	if( video == "RewardVideoFyber" ) 
		AdsPlugin::shared().use( AdsPlugin::Type::rewardVideo, AdsPlugin::Service::fyber );
	if( video == "RewardVideoDeltaDNA" ) 
		AdsPlugin::shared().use( AdsPlugin::Type::rewardVideo, AdsPlugin::Service::deltadna );
	if( video == "RewardVideoAppodeal" )
		AdsPlugin::shared().use( AdsPlugin::Type::rewardVideo, AdsPlugin::Service::appodeal );

	if( offerwall == "OfferWallSupersonic" )
		AdsPlugin::shared().use( AdsPlugin::Type::OfferWall, AdsPlugin::Service::supersonic );
		*/
}

void AppDelegate::loadXmlValues( )
{
	ParamCollection macroses;
	xmlLoader::macros::set("sound_dir", kPathSound);
	xmlLoader::macros::set("sound_ext", kSoundsEXT);
	xmlLoader::macros::set("music_dir", kPathMusic);
	xmlLoader::macros::set("music_ext", kMusicEXT);
	xmlLoader::load_paramcollection( macroses, "ini/sounds.xml" );
	for( auto i : macroses )
	{
		xmlLoader::macros::set( i.first, i.second );
	}

#if PC == 1
	xmlLoader::macros::set("PLATFORM_PC", "yes"); 
#else
	xmlLoader::macros::set("PLATFORM_MOBILE", "yes");
#endif

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	xmlLoader::macros::set( "PLATFORM_ANDROID", "yes" );
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
	xmlLoader::macros::set( "PLATFORM_IOS", "yes" );
#elif CC_TARGET_PLATFORM == CC_PLATFORM_WINRT
	xmlLoader::macros::set( "PLATFORM_WINRT", "yes" );
#endif

	xmlLoader::macros::set( "USE_HEROROOM", toStr( Config::shared().get<bool>( "useHeroRoom" ) ) );
	xmlLoader::macros::set( "NOUSE_HEROROOM", toStr( !Config::shared().get<bool>( "useHeroRoom" ) ) );
}

void AppDelegate::configurePath()
{
	std::vector<std::string> resourcePaths;
	std::vector<std::string> searchPaths;
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
	resourcePaths.push_back( "../Resources" );
#endif
	resourcePaths.push_back( "" );

	for( auto path : resourcePaths )
	{
#if PC == 1
		searchPaths.push_back( path + (path.empty() ? "" : "/") + "pc" );
#endif
		auto folder = gamePath.empty() ? kGameFolder : gamePath;
		auto p = path + (path.empty() ? "" : "/") + folder;
		searchPaths.push_back( p );
		searchPaths.push_back( path );
	}
	resourcePaths = searchPaths;
	searchPaths.clear();
	for( auto path : resourcePaths )
	{
		searchPaths.push_back( path + (path.empty() ? "" : "/") + Language::shared().current() );
		searchPaths.push_back(path);
	}

	FileUtils::getInstance()->setSearchPaths( searchPaths );

	//std::string fontType;
	//if( Language::shared().current() == "ar" )
	//	fontType = "ttf";
	//else
	//	fontType = "fnt";
	//xmlLoader::macros::set( "fonttype", fontType);
}

void createWindow()
{
#if EDITOR == 1
	float width( Config::shared().get<Size>( "levelMapSize" ).width + 300 );
	float height( Config::shared().get<Size>( "levelMapSize" ).height );
	std::string windowTITLE( "Editor: IslandDefense" );
#else
	float width( 1366 );
	float height( 768 );
	std::string windowTITLE( WORD( "window_title" ) );
#endif

#if PC == 1
	g_isFullscreenMode = true;
#endif

#ifdef _DEBUG
	g_isFullscreenMode = false;
#endif

	g_isFullscreenMode = UserData::shared().get( "fullscreen", g_isFullscreenMode );

#ifdef WIN32
#	if PC == 1
	if( AppDelegate::screenResolutionX > 0 && AppDelegate::screenResolutionY > 0 )
	{
		if( g_isFullscreenMode )
		{
			width = AppDelegate::screenResolutionX;
			height = AppDelegate::screenResolutionY;
		}
		else
		{
			if( AppDelegate::screenResolutionX >= 1366 && AppDelegate::screenResolutionY >= 768 )
			{
				width = 1366;
				height = 768;
			}
			else if( AppDelegate::screenResolutionX >= 1024 && AppDelegate::screenResolutionY >= 768 )
			{
				width = 1024;
				height = 768;
#if PC == 1
				height = 576;
#endif
			}
			else
			{
				width = 800;
				height = 600;
#if PC == 1
				height = 450;
#endif
			}
		}
	}
#	endif
#endif
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();

#if CC_TARGET_PLATFORM == CC_PLATFORM_WINRT
	if( !glview )
	{
		glview = GLViewImpl::create( windowTITLE );
		director->setOpenGLView( glview );
	}
#else
	if( !glview )
	{
		if( g_isFullscreenMode )
			glview = GLViewImpl::createWithFullScreen( windowTITLE );
		else
			glview = GLViewImpl::createWithRect( windowTITLE, Rect( 0, 0, width, height ) );
		director->setOpenGLView( glview );
	}
#endif
	director->setAnimationInterval( 1.f / 60.f );
}

void setDesignResolution()
{
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();

	std::vector<std::string> paths;
	auto size = glview->getFrameSize();


#ifndef EDITOR
	auto sx = glview->getFrameSize().width;
	auto sy = glview->getFrameSize().height;
    
    CCLOG("width :: %f height :: %f",sx,sy);
    
	float rate = sx / sy;
    float widthMax = 1136.f;
    
    //special condition for iPhoneX
    if(rate >1.8) {
        rate = std::min( rate, 2.0f );
        widthMax = 1366.f;
    }
    
    if( Config::shared().get( "designresolution" ) == "byheight" )
    {
        sy = 768.0f;
        sx = sy * rate;
    }
    else
    {
        sx = std::max( 1024.f, sx );
        sx = std::min( widthMax, sx );
        sy = sx / rate;
    }
	
#if PC == 1
	rate = std::max( 1.6f, rate );
	sy = sx / rate;
#endif
	glview->setDesignResolutionSize( sx, sy, ResolutionPolicy::SHOW_ALL );
#endif
}

void registration()
{
	Factory::shared().book<EventCreateUnit>( "createunit" );
    Factory::shared().book<EventCreateLaserBullet>( "createLaserBullet" );
	Factory::shared().book<EventCreateUnitReverseRoute>( "createunit_reverseroute" );
	Factory::shared().book<EventAreaDamage>("areadamage");
	Factory::shared().book<EventCreateEffect>("createeffect");
	Factory::shared().book<EventDestroyTarget>( "destroytarget" );
	Factory::shared().book<EventRateTowers>( "ratetowers" );
	Factory::shared().book<EventHeroTestDrive>( "herotestsrive" );
	Factory::shared().book<EventHeroMoveTo>( "heromove" );
	Factory::shared().book<Hero>("hero");
	Factory::shared().book<UnitHomebase>( "homebase" );

	Factory::shared().book<RouleteActivatorNode>( "rouleteactivator" );

	Factory::shared().book<AwardScore>( "awardscore" );
	Factory::shared().book<AwardTowerUpgrade>( "awardtowerupgrade" );
	Factory::shared().book<AwardHeroPoint>( "awardheropoint" );
	Factory::shared().book<AwardBonus>( "awardbonus" );

	Factory::shared().book<HeroesSquadNode>( "heroessquad" );
	Factory::shared().book<LaboratoryTowerItem>( "laboratorytoweritem" );
}


