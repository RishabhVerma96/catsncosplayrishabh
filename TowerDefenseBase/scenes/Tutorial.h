//
//  Tutorial.h
//  JungleDefense
//
//  Created by Vladimir Tolmachev on 24.04.14.
//
//

#ifndef __JungleDefense__Tutorial__
#define __JungleDefense__Tutorial__

#include "cocos2d.h"
#include "ml/NodeExt.h"
#include "ml/Singlton.h"
#include "ml/ParamCollection.h"
#include "support.h"
#include "ml/pugixml/pugixml.hpp"
NS_CC_BEGIN;

class Tutorial : public LayerExt
{
	DECLARE_BUILDER(Tutorial);
	bool init( const std::string & pathToXml, bool testMode = false );
	virtual bool setProperty( int intproperty, const std::string & value )override;
	bool actionCompleted();
public:
	void enter();
	void exit();
	const std::string& next( )const;
	void setForced(bool isForced);
    void setSound(std::string sound);
protected:
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	void cb_confirmTutorial( Ref*, bool use );
	void listenTouches();
	void unlistenTouches();
    
    bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event * event);
    void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event * event);
    void onTouchCanceled( cocos2d::Touch* touch, cocos2d::Event *event );
//    void touchesBegan( const std::vector<Touch*>& touches, Event *event );
//    void touchesEnded( const std::vector<Touch*>& touches, Event *event );
private:
    int lastSoundID;
	bool _testMode;
	unsigned _tapsForClose;
	std::string _nextTutorial;
	bool _forced;
    bool takeTouch;
    bool closeOnTap;
    std::string _sound;
    
    bool rectContainsPoint(cocos2d::Vec2 point);
    void traverseAllChildrun(Node* root);
};


class TutorialManager : public Singlton<TutorialManager>
{
	friend Singlton<TutorialManager>;
	TutorialManager();
	TutorialManager(TutorialManager&&);
	TutorialManager(const TutorialManager&);
	TutorialManager& operator=(const TutorialManager&);
	virtual void onCreate()override;
public:
	~TutorialManager() {}
	void setEnabled( bool enabled );
	bool dispatch( const std::string & eventname, const ParamCollection * params = nullptr );
	bool close( Tutorial * tutorial );
protected:
	bool open( const std::string & name, bool ignorePredelay = false );
	void load( );
	void loadList(const pugi::xml_node & xmlnode );
	void loadEvents( const pugi::xml_node & xmlnode, std::multimap<std::string, std::string> & events );
	bool checkOpening( const std::string& eventname )const;
	void delayedOpen( float delay, const std::string& name );
	void delayedOpenTimer( float dt, const std::string& name );
private:
	Tutorial::Pointer _current;
	struct TutorialInfo
	{
		TutorialInfo() : filename(), iPhoneXName(), onlyaftertutorial(), count( 1 ), forced( false ), predelay(0), tutorialEnded(false), tutorialStarted(false) {}
        
        std::string iPhoneXName;
		std::string filename;
		std::string onlyaftertutorial;
        std::string sound;
		int count;
		bool forced;
		float predelay;
        bool tutorialEnded;
        bool tutorialStarted;
	};
	std::map<std::string, TutorialInfo> _list;
	/*
	 Events for run tutorial. 
	 * first - eventname
	 * second - tutorial name
	 */
	std::multimap<std::string, std::string> _eventsForRun;
	/*
	 Events for close tutorial.
	 * first - eventname
	 * second - tutorial name
	 */
	std::multimap<std::string, std::string> _eventsForClose;

	std::queue<std::pair< std::string, ParamCollection > > _queueEvents;

	bool _enabled = true;
};


NS_CC_END;

#endif /* defined(__JungleDefense__Tutorial__) */
