#ifndef __BuyHeroes_h__
#define __BuyHeroes_h__
#include "cocos2d.h"
#include "macroses.h"
#include "ml/NodeExt.h"
#include "inapp/purchase.h"
NS_CC_BEGIN



class BuyHeroMenu;

class BuyHeroes : public LayerExt
{
	DECLARE_BUILDER( BuyHeroes );
	bool init(BuyHeroMenu* menu);
public:
	void setTimeString( const std::string& string );
protected:
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	void buy();
	void openstore();
	void update(float dt);
private:
	inapp::SkuDetails _details;
	LabelPointer _time;
	BuyHeroMenu* _activateMenu;
};

class BuyHeroMenu : public Menu, public NodeExt
{
	DECLARE_BUILDER( BuyHeroMenu );
	bool init();
public:
	static bool isShow();
	void update(float dt);
protected:
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	void openPromo(Ref*);
private:
	static time_t _duration;
	time_t _timestamp;
	BuyHeroes::Pointer _heroesLayer;
};


NS_CC_END
#endif // #ifndef SelectHero
