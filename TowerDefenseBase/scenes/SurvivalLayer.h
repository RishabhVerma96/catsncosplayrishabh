#pragma once

#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"

NS_CC_BEGIN

class SurvivalLayer : public LayerExt
{
public:
	struct Level 
	{
		int number;
		int livesCount;
		int price;
		std::string inapp;
		Level();
		Level( int _number, int _livesCount, int _price, std::string const & _inapp );
	};

public:
	DECLARE_BUILDER( SurvivalLayer );
	bool init();

	static int getPrice( int levelNumber );
	bool payForPlay( int levelNumber );

	virtual ccMenuCallback get_callback_by_description( std::string const & name ) override;

private:
	static std::vector<Level> _levels;
};

NS_CC_END