#ifndef __SelectHero_h__
#define __SelectHero_h__
#include "cocos2d.h"
#include "macroses.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN





class SelectHero : public LayerExt
{
	DECLARE_BUILDER( SelectHero );
	bool init();
public:
	virtual void onEnter()override;
protected:
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	void fetch( int index );
	void selectSkill( int index );
	void reset();
	void train();
	void select();
	void unselect();
	void buylevel();
	void appearance();
	void disappearance();
	void buyHero();
	void restore();

	int getBuyLevelCost()const;
	bool isHeroSelected( int heroindex );
private:
	std::vector<MenuItemPointer> _itemHero;
	LabelPointer _level;
	ProgressTimerPointer _levelTimer;
	int _currentHero;
	int _currentSkill;
};




NS_CC_END
#endif // #ifndef SelectHero