#include "SelectHero.h"
#include "support.h"
#include "UserData.h"
#include "Hero.h"
#include "MenuItem.h"
#include "ml/Language.h"
#include "ScoreCounter.h"
#include "shop/ShopLayer.h"
#include "Tutorial.h"
#include "ml/ImageManager.h"
#include "ml/SmartScene.h"
#include "ml/loadxml/xmlProperties.h"
#include "consts.h"
#include "configuration.h"
#include "flurry/flurry_.h"
NS_CC_BEGIN



const int kCounntSkills(5);

SelectHero::SelectHero()
: _currentHero(0)
{
}

SelectHero::~SelectHero( )
{
#if PC == 1
#else
	ShopLayer::observerOnPurchase().remove( _ID );
#endif
}

bool SelectHero::init()
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		initBlockLayer( "images/loading.png" );

		load( "ini/selecthero/layer.xml" );
		
		auto content = getChildByName( "content" );
		for( int i = 1;; ++i )
		{
			auto path = "menu/hero" + toStr( i );
			auto node = getNodeByPath<MenuItem>( content, path );
			if( !node )break;
			_itemHero.push_back( node );
		}
		_level = getNodeByPath<Text>( content, "level" );
		_levelTimer = getNodeByPath<ProgressTimer>( content, "level_timer" );

		setKeyDispatcherBackButton( this );

		auto currentHero = UserData::shared().hero_getSelected();
		fetch( currentHero.front() );

		float scale = Director::getInstance()->getOpenGLView()->getDesignResolutionSize().height / 850.f;
		content->setScale( scale );
		return true;
	}
	while( false );
	return false;
}

void SelectHero::onEnter()
{
	LayerExt::onEnter();
	fetch( _currentHero );
	TutorialManager::shared().dispatch( "heroroom_open" );
}

ccMenuCallback SelectHero::get_callback_by_description( const std::string & name )
{
#define callback( x ) std::bind( [this]( Ref* ){ x; }, std::placeholders::_1 )
	if( name == "close" )
		return callback( this->disappearance() );
	else if( name.find( "hero" ) == 0 )
	{
		auto index = name.substr( strlen( "hero" ) );
		return std::bind( [this, index]( Ref* ){ (this->fetch( strTo<int>( index )-1 )); }, std::placeholders::_1 );
	}
	else if( name.find( "skill" ) == 0 )
	{
		auto index = name.substr( strlen( "skill" ) );
		return std::bind( [this, index]( Ref* ){ (this->selectSkill( strTo<int>( index )-1 )); }, std::placeholders::_1 );
	}
	else if( name == "select" )
		return callback( this->select() );
	else if( name == "unselect" )
		return callback( this->unselect() );
	else if( name == "train" )
		return callback( this->train() );
	else if( name == "reset" )
		return callback( this->reset() );
	else if( name == "buylevel" )
		return callback( this->buylevel() );
	else if( name == "buyhero" )
		return callback( this->buyHero() );
	else if( name == "restore" )
		return callback( this->restore() );
	return LayerExt::get_callback_by_description( name );
#undef callback
}

void SelectHero::fetch( int index )
{
	_currentHero = index;
	auto content = getChildByName( "content" );
	auto heroname = "hero" + toStr( _currentHero + 1 );
	auto exp = HeroExp::shared().getEXP( heroname );
	auto herolevel = HeroExp::shared().getLevel( exp );

	auto heroIcons = [this, content]()
	{
		for( int index = 0; index < static_cast<int>(_itemHero.size()); ++index )
		{
			_itemHero[index]->setEnabled( _currentHero != index );
			auto selectFrame = getNodeByPath( _itemHero[index], "select" );
			if( selectFrame ) selectFrame->setVisible( isHeroSelected( index ) );
			auto newlabel = getNodeByPath( _itemHero[index], "newlabel" );
			if( newlabel )
			{
				auto key = "newlabelhs" + toStr( index );
				if( _currentHero == index )
				{
					UserData::shared().write( key, true );
				}
				auto selected = UserData::shared().get <bool>(key);
				newlabel->setVisible(!selected);
			}
		}
	};
	auto level = [this, content, heroname, herolevel]()
	{
		auto progress = herolevel - (int)herolevel;
		_level->setString( WORD( "laboratory_tower_level" ) + toStr<int>( herolevel ) );
		_levelTimer->setPercentage( progress * 100.f );
	};
	auto skin = [this, content]()
	{
		auto skin = getNodeByPath<Sprite>( content, "skin" );
		auto name = getNodeByPath<Text>( content, "heroname" );
		std::string image = Config::shared().get("resourceHeroRoomFolder") + "hero" + toStr( _currentHero + 1 ) + "/skin.png";
		xmlLoader::setProperty( skin, xmlLoader::kImage, image );
		if( name )name->setString( WORD( ("heroname_" + toStr( _currentHero + 1 )) ) );
	};
	auto skillItem = [this, content]()
	{
		for( int i = 0; i < kCounntSkills; ++i )
		{
			auto item = getNodeByPath<mlMenuItem>( content, "menu/skill" + toStr( i ) );
			assert( item );
			std::string imageN = Config::shared().get("resourceHeroRoomFolder") + "hero" + toStr( _currentHero + 1 ) + "/i" + toStr( i + 1 ) + "1.png";
			std::string imageD = Config::shared().get("resourceHeroRoomFolder") + "hero" + toStr( _currentHero + 1 ) + "/i" + toStr( i + 1 ) + "2.png";
			item->setImageNormal( imageN );
			item->setImageDisabled( imageD );
		}
	};
	auto skillDegree = [this, content, heroname]()
	{
		std::vector<unsigned> skills;
		HeroExp::shared().skills( heroname, skills );
		int i = 0;
		for( auto skill : skills )
		{
			std::vector<std::string> frames =
			{
				Config::shared().get("resourceHeroRoomFolder") + "skill_progress_0.png",
				Config::shared().get("resourceHeroRoomFolder") + "skill_progress_1.png",
				Config::shared().get("resourceHeroRoomFolder") + "skill_progress_2.png",
				Config::shared().get("resourceHeroRoomFolder") + "skill_progress_3.png",
			};

			std::string framename;
			if( i == 4 ) 
				framename = (skill == 3 ? frames[3] : frames[2]);
			else 
				framename = (skill == 3 ? frames[1] : frames[0]);

			auto node = getNodeByPath<Node>( content, "skills/skill" + toStr( i ) );
			for( unsigned k = 0; k < 3; ++k )
			{
				auto degree = node->getChildByName<Sprite*>( "degree" + toStr( k ) );
				degree->setVisible( k < skill );
				xmlLoader::setProperty( degree, xmlLoader::kImage, framename );
			}
			auto level = node->getChildByName<Sprite*>( "level" );
			if( skill > 0 )
			{
				std::string image = Config::shared().get("resourceHeroRoomFolder") + "j" + toStr(skill) + ".png";
				xmlLoader::setProperty( level, xmlLoader::kImage, image );
				level->setVisible( true );
			}
			else
			{
				level->setVisible( false );
			}
			++i;
		}
	};
	auto skillPoints = [this, content, heroname]()
	{
		auto points = HeroExp::shared().skillPoints( heroname );
		auto nodepoint = getNodeByPath<Text>( content, "points" );
		nodepoint->setString( toStr( points ) );
	};
	auto setCostBuyLevel = [this]()
	{
		bool isCurrentHeroSelected = UserData::shared().hero_getSelected().front() == _currentHero;
		auto cost = this->getBuyLevelCost();
		auto select = getNodeByPath( this, "content/menu/select" );
		auto costNode0 = getNodeByPath<Text>( this, "content/menu/buylevel/normal/cost" );
		auto costNode1 = getNodeByPath<Text>( this, "content/menu/buylevel/disabled/cost" );
		if( costNode0 ) costNode0->setString( toStr( cost ) );
		if( costNode1 ) costNode1->setString( toStr( cost ) );
		if( select ) select->setVisible( !isCurrentHeroSelected );
	};
	auto checkAvailabled = [this, heroname, herolevel]()
	{
		auto cost = this->getBuyLevelCost();

		bool isCurrentHeroSelected = isHeroSelected( _currentHero );
		bool isBought = HeroExp::shared().isHeroAvailabled( heroname );
		bool asInapp = HeroExp::shared().isHeroAsInapp( heroname );
		unsigned level = HeroExp::shared().getLevelForUnlockHero( heroname );
		auto select = getNodeByPath( this, "content/menu/select" );
		auto unselect = getNodeByPath( this, "content/menu/unselect" );
		auto buyhero = getNodeByPath( this, "content/menu/buyhero" );
		auto restore = getNodeByPath( this, "content/menu/restore" );
		auto buylevel = getNodeByPath<MenuItem>( this, "content/menu/buylevel" );
		auto freelevel = getNodeByPath<MenuItem>( this, "content/menu/freelevel" );
		auto levelnumbertext = getNodeByPath<Text>( this, "content/levelnumbertext" );
		auto train = getNodeByPath<MenuItem>( this, "content/menu/train" );
		auto reset = getNodeByPath<MenuItem>( this, "content/menu/reset" );

		bool visSelect = isBought && !isCurrentHeroSelected;
		bool visUnSelect = isBought && isCurrentHeroSelected;
		bool visBuyLevel = (isBought && isCurrentHeroSelected && herolevel < HeroExp::shared().getMaxLevel()) && cost > 0;
		bool visFreLevel = (isBought && isCurrentHeroSelected && herolevel < HeroExp::shared().getMaxLevel()) && cost == 0;
		bool visBuyHeroInapp = !isBought && asInapp;
		bool visTrain = isBought;
		bool visReset = isBought;

		if( select )select->setVisible( visSelect );
		if( unselect )unselect->setVisible( visUnSelect );
		if( buylevel )buylevel->setVisible( visBuyLevel );
		if( freelevel )freelevel->setVisible( visFreLevel );
		if( train )train->setVisible( visTrain );
		if( reset )reset->setVisible( visTrain );
		if( buyhero )buyhero->setVisible( visBuyHeroInapp );
		if( restore )restore->setVisible( visBuyHeroInapp );
		if( levelnumbertext )levelnumbertext->setVisible( !isBought && !asInapp && level > 0 );
		
		if( isBought == false )
		{
			if( asInapp )
			{
				auto details = HeroExp::shared().getHeroSkuDetails( heroname );
				std::string string = details.priceText;
				auto costnode = getNodeByPath<Text>( this, "content/menu/buyhero/normal/cost" );
				if( details.result == inapp::Result::Ok )
					if( costnode )costnode->setString( string );
				else
					if( costnode )costnode->setString( "no connection" );
			}
			else if( level > 0 )
			{
				if( levelnumbertext )
				{
					std::string text = "#complitelevelforunlock_" + toStr( level ) + "#";
					levelnumbertext->setString( WORD(text) );
				}
			}
		}
	};

	heroIcons();
	level();
	skin();
	skillItem();
	skillDegree();
	skillPoints();
	setCostBuyLevel();
	checkAvailabled();

	selectSkill( 0 );
}

void SelectHero::selectSkill( int index )
{
	_currentSkill = index;
	auto heroname = "hero" + toStr( _currentHero + 1 );
	std::vector<unsigned> skills;
	HeroExp::shared().skills( heroname, skills );
	auto points = HeroExp::shared().skillPoints( heroname );

	auto content = getChildByName( "content" );
	auto icon = [this, content]()
	{
		auto nodeicon = getNodeByPath<Sprite>( content, "skills/icon" );
		std::string image = Config::shared().get("resourceHeroRoomFolder") + "hero" + toStr( _currentHero + 1 ) + "/i" + toStr( _currentSkill + 1 ) + "1.png";
		xmlLoader::setProperty( nodeicon, xmlLoader::kImage, image );
	};
	auto desc = [this, content, heroname]()
	{
		auto nodename = getNodeByPath<Text>( content, "skills/name" );
		auto nodedesc = getNodeByPath<Text>( content, "skills/desc" );
		std::string skillname = "skill" + toStr( _currentSkill + 1 );
		std::string nameID = "#heroskill_" + heroname + "_" + skillname + "_name#"; //heroskill_heroN_skillM_name
		std::string descID = "#heroskill_" + heroname + "_" + skillname + "_desc#"; //heroskill_heroN_skillM_desc
		std::string name = Language::shared().string( nameID );
		std::string desc = Language::shared().string( descID );
		nodename->setString( name );
		nodedesc->setString( desc );
	};
	auto enable = [this, content]()
	{
		for( int i = 0; i < kCounntSkills; ++i )
		{
			auto item = getNodeByPath<MenuItem>( content, "menu/skill" + toStr( i ) );
			item->setEnabled( i != _currentSkill );
		}
	};
	auto enableTrain = [this, content, skills, points]()
	{
		auto item = getNodeByPath<MenuItem>( content, "menu/train" );
		bool enabled = skills[_currentSkill] < 3;
		enabled = enabled && points > 0;
		item->setEnabled( enabled );
	};

	icon();
	desc();
	enable();
	enableTrain();
	TutorialManager::shared().dispatch( "hero_skillselect" );
}

void SelectHero::reset()
{
	auto heroname = "hero" + toStr( _currentHero + 1 );
	std::vector<unsigned> skills( kCounntSkills );
	HeroExp::shared().setSkills( heroname, skills );

	auto skill = _currentSkill;
	fetch( _currentHero );
	selectSkill( skill );
}

void SelectHero::train()
{
	auto heroname = "hero" + toStr( _currentHero + 1 );
	std::vector<unsigned> skills;
	HeroExp::shared().skills( heroname, skills );
	skills[_currentSkill]++;
	HeroExp::shared().setSkills( heroname, skills );

	auto skill = _currentSkill;
	fetch( _currentHero );
	selectSkill( skill );

	TutorialManager::shared().dispatch( "hero_skilltrain" );
	UserData::shared().save();
}

void SelectHero::select()
{
	UserData::shared().hero_select( _currentHero );
	UserData::shared().save();
	fetch( _currentHero );
}

void SelectHero::unselect()
{
	UserData::shared().hero_unselect( _currentHero );
	UserData::shared().save();
	fetch( _currentHero );
}

void SelectHero::buylevel()
{
	auto cost = getBuyLevelCost();
	auto scores = ScoreCounter::shared().getMoney( kScoreCrystals );
	if( cost <= scores )
	{
		auto heroname = "hero" + toStr( _currentHero + 1 );
		auto exp = HeroExp::shared().getEXP( heroname );
		auto level = HeroExp::shared().getLevel( exp );
		exp = HeroExp::shared().getHeroLevelExtValue( level );
		HeroExp::shared().setEXP( heroname, exp );
		ScoreCounter::shared().subMoney( kScoreCrystals, cost, true, "heroroom:" + heroname );

		if( cost == 0 )
		{
			auto points = UserData::shared().get <int>("heropoints_gift");
			if (points > 0)
				--points;
			UserData::shared().write( "heropoints_gift", points );
		}

		fetch( _currentHero );

		ParamCollection pc;
		pc["event"] = "HeroBuyLevel";
		pc["hero"] = toStr( _currentHero );
		pc["level"] = toStr<int>( level );
		flurry::logEvent( pc );
	}
	else
	{
#if PC == 1
#else
		auto shop = ShopLayer::create( false, true, false, true );
		if( shop )
		{
			SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
			scene->pushLayer( shop, true );
		}
#endif
	}
}

void SelectHero::appearance()
{
	runEvent( "onenter" );
}

void SelectHero::disappearance()
{
	runEvent( "onexit" );
}

void SelectHero::buyHero()
{
#if PC == 1
#else
	auto heroname = "hero" + toStr( _currentHero + 1 );
	auto skuDetails = HeroExp::shared().getHeroSkuDetails( heroname );
	if( skuDetails.result == inapp::Result::Ok )
	{
		auto callback = [this]( int, int, SelectHero* selectHero)
		{
			if( HeroExp::shared().isHeroAvailabled( "hero" + toStr( _currentHero + 1 ) ) )
				UserData::shared().hero_select( _currentHero );
			selectHero->fetch( selectHero->_currentHero );
			popBlockLayer();
		};
        pushBlockLayer( true, 30 );
		ShopLayer::observerOnPurchase().add( _ID, std::bind( callback, std::placeholders::_1, std::placeholders::_2, this ) );
		inapp::purchase( skuDetails.productId );
	}
#endif
}

void SelectHero::restore()
{
#if PC == 1
#else
	auto heroname = "hero" + toStr( _currentHero + 1 );
	auto skuDetails = HeroExp::shared().getHeroSkuDetails( heroname );
	if( skuDetails.result == inapp::Result::Ok )
	{
		auto callback = [this]( int, int, SelectHero* selectHero)
		{
			selectHero->fetch( selectHero->_currentHero );
			popBlockLayer();
		};
		pushBlockLayer( true, 30 );
		ShopLayer::observerOnPurchase().add( _ID, std::bind( callback, std::placeholders::_1, std::placeholders::_2, this ) );
		inapp::restore( skuDetails.productId );
	}
#endif
}

int SelectHero::getBuyLevelCost()const
{
	auto gift = UserData::shared().get <int>("heropoints_gift");
	if (gift > 0)
		return 0;

	unsigned maxlevel = HeroExp::shared().getMaxLevel();
	auto heroname = "hero" + toStr( _currentHero + 1 );
	auto exp = HeroExp::shared().getEXP( heroname );
	unsigned level = HeroExp::shared().getLevel( exp );
	return level < maxlevel ? HeroExp::shared().getCostLevelup(level) : 0;
}

bool SelectHero::isHeroSelected( int heroindex )
{
	auto heroes = UserData::shared().hero_getSelected();
	return std::find( heroes.begin(), heroes.end(), heroindex ) != heroes.end();
}

NS_CC_END
