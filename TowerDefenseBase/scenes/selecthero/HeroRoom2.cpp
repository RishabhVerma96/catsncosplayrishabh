#include "ml/MenuItem.h"
#include "ml/Text.h"
#include "ml/Language.h"
#include "ml/ScrollMenu.h"
#include "ml/SmartScene.h"
#include "ml/ImageManager.h"
#include "ml/loadxml/xmlLoader.h"
#include "ml/Audio/AudioEngine.h"
#include "flurry/flurry_.h"
#include "HeroRoom2.h"
#include "ShopLayer.h"
#include "configuration.h"
#include "UserData.h"
#include "Hero.h"
#include "tower.h"
#include "plugins/AdsPlugin.h"
#include "inapp/purchase.h"
#include "support.h"
#include "ScoreCounter.h"
#include "MapLayer.h"
#include "DialogLayer.h"
#include "BuyHeroes.h"
#include "resources.h"
#include "EasterEggsHandler.h"
#include "Tutorial.h"
#include "support.h"

NS_CC_BEGIN

HeroRoom2::HeroRoom2()
: _openCard(nullptr)
{}

HeroRoom2::~HeroRoom2()
{
	AdsPlugin::shared().observerVideoStarted.remove( _ID );
	AdsPlugin::shared().observerVideoResult.remove( _ID );
	ShopLayer::observerOnPurchase().remove( _ID );
}

bool HeroRoom2::init()
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		initBlockLayer( "images/loading.png" );
        
        if(IS18_9Ratio) {
            load( "ini/heroroom2/layer_iPhoneX.xml" );
        }
        else {
            load( "ini/heroroom2/layer.xml" );
        }
        buildList();
		setDisapparanceOnBackButton();
        AudioEngine::shared().playEffect( kSoundShopShow );
		return true;
	}
	while( false );
	return false;
}

ccMenuCallback HeroRoom2::get_callback_by_description( const std::string & name )
{
	if( name == "train" ) return std::bind( &HeroRoom2::openHeroCard, this, std::placeholders::_1, -1 );
	if( name == "video:10xp" ) return std::bind( &HeroRoom2::showVideo, this, std::placeholders::_1, -1 );
	if( name == "purchase" ) return std::bind( &HeroRoom2::purchase, this, std::placeholders::_1, -1 );
	if( name == "getpro" ) return std::bind( &HeroRoom2::getPro, this );
    if( name == "unlockByVideo" ) return std::bind( &HeroRoom2::unlockByVideo, this, std::placeholders::_1 );
	return LayerExt::get_callback_by_description( name );
}

void HeroRoom2::onEnter()
{
	LayerExt::onEnter();
	fetchList();
	//UserData::shared().write( "heroroom_visited", true );

	if( _openCard )
	{
		_openCard = nullptr;
		runEvent( "closecard" );
		setVisible( true );
		
		auto func = [this]()
		{
			_hidenCard->setCascadeOpacityEnabled( true );
		};
		auto action = Sequence::create( DelayTime::create( 0.5f ), CallFunc::create( func ), nullptr );
		runAction( action );
	}
    
    CCLOG("scene name :: %s",dynamic_cast<SmartScene*>(getScene())->getName().c_str());
    CCLOG("my name :: %s",this->getName().c_str());
    CCLOG("my parent name :: %s",this->getParent()->getName().c_str());
}

void HeroRoom2::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
}

bool HeroRoom2::isBlurActive()
{
	return !isRunning();
}

void HeroRoom2::disappearance()
{
    LayerExt::disappearance();
    AudioEngine::shared().playEffect( kSoundShopHide );
}

void HeroRoom2::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

int HeroRoom2::computeIndexHeroByButton( MenuItem* button )
{
	Node* node = button;
	while( node )
	{
		auto name = node->getName();
		if( name.find( "hero" ) == 0 )
		{
			return strTo<int>( name.substr( strlen( "hero" ) ) ) - 1;
		}
		node = node->getParent();
	}
	assert( 0 );
	return -1;
}

void HeroRoom2::openHeroCard( Ref*sender, int indexhero )
{
	if( indexhero == -1 )
	{
		indexhero = computeIndexHeroByButton( static_cast<MenuItem*>(sender) );
	}

	auto createCard = [this, indexhero]()
	{
		auto keyNewLabel = "newlabelhs" + toStr( indexhero );
		UserData::shared().write( keyNewLabel, true );

		auto herocard = getNodeByPath<MenuItem>( this, "heroes/hero" + toStr( indexhero + 1 ) );
		xmlLoader::bookDirectory( this );
        _openCard = HeroCard::create( this, indexhero, herocard );
        _openCard->setName( "hero" + toStr( indexhero + 1 ) );
		xmlLoader::unbookDirectory( this );
		dynamic_cast<SmartScene*>(getScene())->pushLayer( _openCard, true );
		setVisible( false );
        TutorialManager::shared().dispatch("map_onselecthero");
//        CCLOG("scene name :: %s",dynamic_cast<SmartScene*>(getScene())->getName().c_str());
//        CCLOG("my name :: %s",_openCard->getName().c_str());
//        CCLOG("my parent name :: %s",_openCard->getParent()->getName().c_str());
	};

	_hidenCard = getNodeByPath<MenuItem>( this, "heroes/hero" + toStr( indexhero + 1 ) );
	auto action = Sequence::create( DelayTime::create( 0.2f ), CallFunc::create( createCard ), nullptr );
	runAction( action );
	runEvent( "opencard" );
	_hidenCard->setCascadeOpacityEnabled( false );

}

void HeroRoom2::showVideo( Ref*sender, int indexhero )
{
	pushBlockLayer(true, 10);
	auto start = [this]( LayerExt::Pointer* pointer ) mutable
	{
	};
	auto finish = [this]( bool result, LayerExt::Pointer pointer, Ref*sender, int indexhero )
	{
		popBlockLayer();
		if( result )
		{
			if( indexhero == -1 )
				indexhero = computeIndexHeroByButton( static_cast<MenuItem*>(sender) );
			auto heroname = "hero" + toStr( indexhero + 1 );
			auto herocard = getNodeByPath<mlMenuItem>( this, "heroes/" + heroname );
			float exp = HeroExp::shared().getEXP( heroname );
			int level = HeroExp::shared().getLevel( exp );
			float exp2 = level > 0 ? HeroExp::shared().getHeroLevelExtValue( level - 1 ) : 0;
			float exp3 = HeroExp::shared().getHeroLevelExtValue( level );
			exp = exp + (exp3 - exp2) * 0.35f;
			HeroExp::shared().setEXP( heroname, exp );
			fetchHeroCard( herocard, indexhero );
			if( _openCard )
				_openCard->fetch();
		}
		ParamCollection pc;
		pc["event"] = "Video";
		pc["value"] = result ? "successful" : "failed";
		pc["place"] = "shop";
		pc["product"] = "hero" + toStr( indexhero );
		flurry::logEvent( pc );
	};
	LayerExt::Pointer blocker;
	AdsPlugin::shared().observerVideoStarted.add( _ID, std::bind( start, &blocker ) );
	AdsPlugin::shared().observerVideoResult.add( _ID, std::bind( finish, std::placeholders::_1, blocker, sender, indexhero ) );
	AdsPlugin::shared().showVideo();
}

void HeroRoom2::purchase( Ref*sender, int indexhero )
{
	if( indexhero == -1 )
		indexhero = computeIndexHeroByButton( static_cast<MenuItem*>(sender) );
	std::string heroname = "hero" + toStr( indexhero + 1 );
	assert( HeroExp::shared().isHeroAsInapp( heroname ) );

	auto skuDetails = HeroExp::shared().getHeroSkuDetails( heroname );
	if( skuDetails.result == inapp::Result::Ok )
	{
		auto callback = [this]( int, int, HeroRoom2* layer, int indexhero )
		{
			layer->popBlockLayer();
			getScene()->runAction( CallFunc::create( std::bind( []( HeroRoom2* layer, int indexhero ){
				if( HeroExp::shared().isHeroAvailabled( "hero" + toStr( indexhero + 1 ) ) )
				{
					UserData::shared().hero_select( indexhero );
				}
				layer->fetchList();
				if( layer->_openCard )
					layer->_openCard->fetch();
			}, layer, indexhero ) ) );
		};
		pushBlockLayer( true, 30 );
		ShopLayer::observerOnPurchase().add( _ID, std::bind( callback, std::placeholders::_1, std::placeholders::_2, this, indexhero ) );
		inapp::purchase( skuDetails.productId );
	}
}

void HeroRoom2::unlockByVideo( Ref*sender )
{
    CCLOG("HeroUnlcok");
    int indexhero = computeIndexHeroByButton( static_cast<MenuItem*>(sender) );
    if (AdsPlugin::shared().isVideoAvailabled())
    {
        pushBlockLayer(true, 10);
        auto finish = [this]( bool result, LayerExt::Pointer pointer, Ref*sender, int indexhero )
        {
            popBlockLayer();
            if( result )
            {
                std::string heroname = "hero" + toStr( indexhero + 1 );
                int watchedVideoCount = UserData::shared().incrementWatchedVideoCountForHero(heroname);
                int requiredCount = HeroExp::shared().getRequiredVideoCountToUnlockHero(heroname);
                auto card = getNodeByPath<mlMenuItem>( this, "heroes/" + heroname );
                if(requiredCount != -1 &&  requiredCount - watchedVideoCount == 0)
                {
                    HeroExp::shared().heroBought( "hero" + toStr( indexhero + 1 ) );
                    UserData::shared().save();
                }
                
                fetchHeroCard( card, indexhero );
                if( _openCard )
                _openCard->fetch();
            }
        };
        LayerExt::Pointer blocker;
        AdsPlugin::shared().observerVideoResult.add( _ID, std::bind( finish, std::placeholders::_1, blocker, sender, indexhero ) );
        AdsPlugin::shared().showVideo();
    }
}

void HeroRoom2::getPro()
{
	if( Config::shared().get<bool>( "useDialogs" ) )
	{
		auto callback = [this]( bool answer ){
			auto url = Config::shared().get( "linkToStorePaidVersion" );
			if( answer )
				Application::getInstance()->openURL( url );
		};
		DialogLayer::createAndRun( "ini/dialogs/getpro_hero.xml", std::bind( callback, std::placeholders::_1 ) );
	}
	else
	{
		auto layer = BuyHeroes::create( nullptr );
		auto scene = dynamic_cast<SmartScene*>(getScene());
		if( scene&& layer )
		{
			auto z = layer->getLocalZOrder();
			scene->pushLayer( layer, true );
			if( z != 0 )
				layer->setLocalZOrder( z );
		}
	}
}

void HeroRoom2::buildList()
{
	auto list = getNodeByPath<ScrollMenu>( this, "heroes" );
	std::string itempath = getParamCollection().get( "template_item" );
    int heroesCount = getHeroCount();

	for( int i = 0; i < heroesCount ; ++i )
	{
		std::string heroname = "hero" + toStr( i + 1 );
		xmlLoader::macros::set( "heroname", heroname );
		xmlLoader::bookDirectory( this );
		auto item = xmlLoader::load_node<mlMenuItem>( itempath );
		xmlLoader::unbookDirectory( this );
		item->setName( heroname );
		item->setLocalZOrder( -i );
		item->setCallback( std::bind( &HeroRoom2::openHeroCard, this, std::placeholders::_1, i ) );
		list->addItem( item );
	}
	list->align( list->getAlignedColums() );
}

void HeroRoom2::fetchList()
{
    
    int heroesCount = getHeroCount();

	for( int i = 0; i < heroesCount ; ++i )
	{
		std::string heroname = "hero" + toStr( i + 1 );
		auto herocard = getNodeByPath<mlMenuItem>( this, "heroes/" + heroname );
		assert( herocard );
		fetchHeroCard( herocard, i );
	}
}

int HeroRoom2::getHeroCount()
{
    int heroesCount = Config::shared().get<int>( "heroesCount" );
    
    if(!EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kHero9Unlocked))
    {
        heroesCount-=1;
    }
    return heroesCount;
}

void HeroRoom2::fetchHeroCard( mlMenuItem *card, int heroIndex )
{
	int heroesAvailabled = Config::shared().get<int>( "heroesAvailabled" );
	std::string heroname = "hero" + toStr( heroIndex + 1 );
	bool isBought = HeroExp::shared().isHeroAvailabled( heroname );
	bool isAvailabled = heroIndex < heroesAvailabled;
	auto keyNewLabel = "newlabelhs" + toStr( heroIndex );
	auto selected = UserData::shared().get <bool>( keyNewLabel );
	bool alwaysShowExp = strTo<bool>( card->getParamCollection().get( "always_show_exp", "false" ) );

	auto desc = getNodeByPath<Text>( card, "head/desc" );
	auto name = getNodeByPath<Text>( card, "head/name" );
	auto newLabel = getNodeByPath( card, "new" );
	auto level = getNodeByPath<Text>( card, "head/level/level" );
	auto exptext = getNodeByPath<Text>( card, "menu_hp/hp/text" );
	auto expbar = getNodeByPath<Node>( card, "menu_hp/hp/bar" );
	auto exptimer = getNodeByPath<ProgressTimer>( card, "menu_hp/hp/timer" );
	auto insquad = getNodeByPath<Node>( card, "command_in" );
	auto outsquad = getNodeByPath<Node>( card, "command_out" );
	auto menu_train = getNodeByPath<Menu>( card, "menu_train" );
	auto menu_purchase = getNodeByPath<Menu>( card, "menu_purchase" );
	auto menu_getpro = getNodeByPath<Node>( card, "menu_getpro" );
	auto menu_hp_exp = getNodeByPath<Node>( card, "menu_hp/hp" );
	auto menu_hp_adstext = getNodeByPath<Node>( card, "menu_hp/text" );

	bool showWatchVideo = strTo<bool>( card->getParamCollection().get( "show_watch_video", "true" ) );	
	

	if( desc ) desc->setString( WORD( "herodesc_" + toStr( heroIndex + 1 ) ) );
	if( name ) name->setString( WORD( "heroname_" + toStr( heroIndex + 1 ) ) );
	if( insquad ) insquad->setVisible( isHeroSelected( heroIndex ) && isBought && isAvailabled );
	if( outsquad ) outsquad->setVisible( !isHeroSelected( heroIndex ) && isBought && isAvailabled );
	if( newLabel ) newLabel->setVisible( !selected );
    
    int requiredCount = HeroExp::shared().getRequiredVideoCountToUnlockHero(heroname);
    int watchedVideoCount = UserData::shared().getWatchedVideoCountForHero(heroname);
    auto remainingVideoNode = getNodeByPath<Node>( card, "menu_hp/unlockButton" );
    auto remainingVideoText = getNodeByPath<Text>( card, "menu_hp/unlockButton/countLabel" );
    if(isAvailabled && !isBought && requiredCount != -1 &&  requiredCount - watchedVideoCount > 0)
    {
        remainingVideoText->setString(intToStr(requiredCount - watchedVideoCount));
    }
    else
    {
        remainingVideoNode->setVisible(false); 
    }

    float exp = HeroExp::shared().getEXP( heroname );
	int prevlevel = level ? strTo<int>( level->getString() ) : 0;
	int maxLevel = HeroExp::shared().getMaxLevel();
	int levelvalue = HeroExp::shared().getLevel( exp );
	float exp2 = levelvalue > 0 ? HeroExp::shared().getHeroLevelExtValue( levelvalue - 1 ) : 0;
	float exp3 = levelvalue < static_cast<int>(maxLevel) ?
		HeroExp::shared().getHeroLevelExtValue( levelvalue ) : 
		exp2;
	exp = exp - exp2;
	exp2 = exp3 - exp2;
	if( level ) level->setString( toStr( levelvalue ) );
	if( exptext )
	{
		int a = strTo<int>( exptext->getString() );
		int b = exp;
		auto action = ActionText::create( 1.f, b, true, "", "/" + toStr<int>( exp2 ) );
		auto action2 = Sequence::create(
			ScaleTo::create( 0.2f, 1.2f ), ScaleTo::create( 0.2f, 1.f ),
			ScaleTo::create( 0.2f, 1.2f ), ScaleTo::create( 0.2f, 1.f ),
			ScaleTo::create( 0.2f, 1.2f ), ScaleTo::create( 0.2f, 1.f ), nullptr );
		exptext->runAction( action );
		exptext->runAction( action2 );		
	}
	if( expbar )
	{
		auto action = ScaleTo::create( 1.f, exp / exp2, 1.f );
		expbar->runAction( action );
	}
	if( exptimer )
	{
		auto action = ProgressTo::create( 1.f, exp / exp2 * 100 );
		exptimer->runAction( action );
	}

	int skillPoints = HeroExp::shared().skillPoints( heroname );
	bool isMaxLevel = levelvalue == maxLevel;
    bool trainVis = isAvailabled && isBought && skillPoints > 0;
    bool hpWatchVideoVis = isAvailabled && skillPoints >= 0 && isMaxLevel == false;
	bool hpExpVis = isAvailabled && (alwaysShowExp || skillPoints == 0) && isMaxLevel == false;
	bool purchaseVis = isAvailabled && isBought == false && HeroExp::shared().isHeroAsInapp( heroname );
	bool getproVis = isAvailabled == false;
	assert( trainVis + hpWatchVideoVis + purchaseVis + getproVis <= 3 );
    
    auto menu_hp_watchvideo = getNodeByPath<Node>( card, "menu_hp/watchvideo" );
        
    
	if( menu_train ) menu_train->setVisible( trainVis );
	if( menu_hp_watchvideo ) menu_hp_watchvideo->setVisible( hpWatchVideoVis );
	if( menu_hp_adstext ) menu_hp_adstext->setVisible( hpWatchVideoVis );
	if( menu_hp_exp ) menu_hp_exp->setVisible( hpExpVis );
	if( menu_purchase ) menu_purchase->setVisible( purchaseVis );
	if( menu_getpro ) menu_getpro->setVisible( getproVis );

	bool autoPosCardHead = strTo<bool>( card->getParamCollection().get( "auto_pos_card_head", "true" ) );
	auto levelnode = getNodeByPath( card, "head/level" );
	auto namenode = getNodeByPath( card, "head/name" );
	if (autoPosCardHead && levelnode && namenode)
	{
		auto pos = namenode->getPosition();
		pos.x = card->getContentSize().width / 2;
		auto sizename = namenode->getContentSize();
		auto sizelevel = levelnode->getContentSize();
		float width = sizename.width + sizelevel.width;
		float x1 = -width / 2 + sizename.width / 2;
		float x2 = +width / 2 - sizelevel.width / 2;
		namenode->setPosition( pos += Point( x1, 0 ) );
		levelnode->setPosition( pos += Point( x2, 0 ) );
	}

	if( levelvalue > prevlevel && prevlevel  > 0 )
	{
		xmlLoader::macros::set( "level", toStr( levelvalue ) );
		auto node = xmlLoader::load_node( "ini/heroroom2/actions/increase_level.xml" );
		if( node )
		{
			node->setPosition( level->getPosition() );
			level->getParent()->addChild( node );
		}

	}

	auto info = mlUnitInfo::shared().info( "hero/" + heroname );
	auto d = info.damage;
	auto a = info.armor;
	auto v = info.velocity;
	auto h = info.health;
    if (heroIndex == 0)
    {
        bool isHero1SpeedEasterEggUnlocked = EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kHero1DoubleSpeed);
        if (isHero1SpeedEasterEggUnlocked)
        {
            d *= 2;
            a *= 2;
            v *= 2;
            h *= 2;
        }
        
    }
	auto damage = getNodeByPath<Text>( card, "fightparams/attack" );
	auto armor = getNodeByPath<Text>( card, "fightparams/armor" );
	auto velocity = getNodeByPath<Text>( card, "fightparams/speed" );
	auto health = getNodeByPath<Text>( card, "fightparams/health" );
	if (damage)		damage->setString( toStr<int>( d ) );
	if (armor)		armor->setString( toStr<int>( a ) );
	if (velocity)	velocity->setString( toStr<int>( v ) );
	if (health)		health->setString( toStr<int>( h ) );
}

bool HeroRoom2::isHeroSelected( int heroindex )
{
	auto heroes = UserData::shared().hero_getSelected();
	return std::find( heroes.begin(), heroes.end(), heroindex ) != heroes.end();
}

/*****************************************************************************/
//MARK: class HeroCard
/*****************************************************************************/
HeroCard::HeroCard()
	: _heroIndex( -1 )
, _skillSelected( -1 )
, _roomlayer( nullptr )
{}

HeroCard::~HeroCard()
{}

bool HeroCard::init( HeroRoom2* roomlayer, int heroindex, Node* herocard )
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		_heroIndex = heroindex;
		_roomlayer = roomlayer;
		std::string heroname = "hero" + toStr( heroindex + 1 );
		xmlLoader::macros::set( "heroname", heroname );
		xmlLoader::macros::set( "card_position_x", toStr( herocard->getPosition().x ) );
		xmlLoader::macros::set( "card_position_y", toStr( herocard->getPosition().y ) );

        if(IS18_9Ratio) {
            load( "ini/heroroom2/heroinfo_iPhoneX.xml");
        }
        else {
            load( "ini/heroroom2/heroinfo.xml");
        }

		for( int i = 0; i < 5; ++i )
		{
			auto path = "info/skills/" + toStr( i );
			auto button = getNodeByPath<mlMenuItem>( this, path );
			button->getParamCollection().set( "imageN", button->getImageNormal() );
			button->getParamCollection().set( "imageS", button->getImageSelected() );
			_skillButtons.push_back( button );
            CCLOG("button name :: %s",button->getParent()->getName().c_str());
		}
		setDisapparanceOnBackButton();
		fetch();
		return true;
	}
	while( false );
	return false;
}

ccMenuCallback HeroCard::get_callback_by_description( const std::string & name )
{
	if( name == "squad:in" ) return std::bind( &HeroCard::squad, this, true );
	if( name == "squad:out" ) return std::bind( &HeroCard::squad, this, false );
	if( name == "skill:reset" ) return std::bind( &HeroCard::skillReset, this );
	if( name.find( "skill:" ) == 0 )
	{
		auto index = strTo<int>( name.substr( strlen( ("skill:") ) ) );
		return std::bind( &HeroCard::skillSelect, this, index );
	}
	if( name == "levelup" ) return std::bind( &HeroCard::buyLevel, this );
	return LayerExt::get_callback_by_description( name );
}

void HeroCard::skillSelect( int index )
{
	auto heroname = "hero" + toStr( _heroIndex + 1 );
	int heroesAvailabled = Config::shared().get<int>( "heroesAvailabled" );
	bool isBought = HeroExp::shared().isHeroAvailabled( heroname );
	bool isAvailabled = _heroIndex < heroesAvailabled;
	std::vector<unsigned> skills;
	HeroExp::shared().skills( heroname, skills );
	auto points = HeroExp::shared().skillPoints( heroname );
	auto canUpgrade = isAvailabled && isBought && points > 0 && index != -1 && skills[index] < 3;

	if( _skillSelected == index && canUpgrade )
	{
		skillUpgrade();
        TutorialManager::shared().dispatch("skill"+heroname+"upgraded");
		return;
	}
	auto button = _skillSelected == -1 ? nullptr : _skillButtons[_skillSelected];
    if( button ) {
      button->setImageNormal( button->getParamCollection().get( "imageN" ) );
        button->setSound("##selectskill##");
    }
    
	_skillSelected = index;
	button = _skillSelected == -1 ? nullptr : _skillButtons[_skillSelected];
	if( canUpgrade )
	{
        if( button ) {
            button->setImageNormal( button->getParamCollection().get( "imageS" ) );
            button->setSound("##skill##");
        }
	}

	skillShowDescription();
}

void HeroCard::skillUpgrade()
{
	auto heroname = "hero" + toStr( _heroIndex + 1 );
	auto points = HeroExp::shared().skillPoints( heroname );
	if( points > 0 )
	{
		std::vector<unsigned> skills;
		HeroExp::shared().skills( heroname, skills );
		skills[_skillSelected]++;
		HeroExp::shared().setSkills( heroname, skills );
		skillSelect( -1 );
		fetchSkills();
	}
}

void HeroCard::skillShowDescription()
{
	auto name = getNodeByPath<Text>( this, "info/skills/desc/name" );
	auto desc = getNodeByPath<Text>( this, "info/skills/desc/desc" );
	auto icon = getNodeByPath<Sprite>( this, "info/skills/desc/icon" );
	name->setVisible( _skillSelected != -1 );
	desc->setVisible( _skillSelected != -1 );
	if (icon)
		icon->setVisible( _skillSelected != -1 );
	if( _skillSelected != -1 )
	{
		std::string skillname = "skill" + toStr( _skillSelected + 1 );
		auto heroname = "hero" + toStr( _heroIndex + 1 );
		std::string nameID = "#heroskill_" + heroname + "_" + skillname + "_name#"; //heroskill_heroN_skillM_name
		std::string descID = "#heroskill_" + heroname + "_" + skillname + "_desc#"; //heroskill_heroN_skillM_desc
		name->setString( Language::shared().string( nameID ) );
		desc->setString( Language::shared().string( descID ) );

		if (icon) 
		{
			auto iconPath = "heroroom2::desc_icon_skills/" + heroname + "/" + toStr( _skillSelected + 1 ) + ".png";
			auto frame = ImageManager::shared().spriteFrame( iconPath );
			if (frame)
				icon->setSpriteFrame( frame );
			else
				icon->setTexture( iconPath );
		}
	}
}

void HeroCard::skillReset()
{
	auto heroname = "hero" + toStr( _heroIndex + 1 );
	std::vector<unsigned> skills( 5 );
	HeroExp::shared().setSkills( heroname, skills );

	auto skill = _skillSelected;
	fetchSkills();
	skillSelect( -1 );
}

void HeroCard::buyLevel()
{
	auto cost = getBuyLevelCost();
	auto score = ScoreCounter::shared().getMoney( kScoreCrystals );
	if( cost > score )
	{
#if PC != 1
		auto scene = getSmartScene();
		auto shop = MapLayer::createShop( 1, 1 );
		if( scene && shop )
			scene->pushLayer( shop, true );
#endif
	}
	else
	{
		auto heroname = "hero" + toStr( _heroIndex + 1 );
		auto exp = HeroExp::shared().getEXP( heroname );
		auto level = HeroExp::shared().getLevel( exp );
		exp = HeroExp::shared().getHeroLevelExtValue( level );
		HeroExp::shared().setEXP( heroname, exp );
		ScoreCounter::shared().subMoney( kScoreCrystals, cost, true, "heroroom:" + heroname );

		if( cost == 0 )
		{
			auto points = UserData::shared().get <int>("heropoints_gift");
			if (points > 0)
				--points;
			UserData::shared().write( "heropoints_gift", points );
		}
		UserData::shared().save();
		fetch();

		ParamCollection pc;
		pc["event"] = "HeroBuyLevel";
		pc["hero"] = toStr( _heroIndex );
		pc["level"] = toStr<int>( level );
		flurry::logEvent( pc );
	}
}

void HeroCard::squad( bool select )
{
	if (select)
	{
		UserData::shared().hero_select( _heroIndex );
	}
	else
	{
		UserData::shared().hero_unselect( _heroIndex );
	}
	fetchSquad();
}

void HeroCard::fetch()
{
	fetchCard();
	fetchSquad();
	fetchPreview();
	fetchParameters();
	fetchSkills();
	fetchMenu();

	auto infonode = getNodeByPath( this, "info" );
	auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	float scale = std::min( 1.f, dessize.width / infonode->getContentSize().width );
	infonode->setScale( scale );
}

void HeroCard::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
}

bool HeroCard::isBlurActive()
{
	return !isRunning();
}

void HeroCard::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

void HeroCard::fetchCard()
{
	auto card = getNodeByPath<mlMenuItem>( this, "card" );
	HeroRoom2::fetchHeroCard( card, _heroIndex );

	std::string heroname = "hero" + toStr( _heroIndex + 1 );
	int heroesAvailabled = Config::shared().get<int>( "heroesAvailabled" );
	bool isBought = HeroExp::shared().isHeroAvailabled( heroname );
	bool isAvailabled = _heroIndex < heroesAvailabled;
	int level = HeroExp::shared().getLevel( HeroExp::shared().getEXP( heroname ) );
	int max = 15;

	auto menu_train = getNodeByPath<Menu>( card, "menu_train" );
	auto menu_hp_exp = getNodeByPath<Node>( card, "menu_hp/hp" );
	auto menu_hp_adstext = getNodeByPath<Node>( card, "menu_hp/text" );
	auto menu_hp_watchvideo = getNodeByPath<Node>( card, "menu_hp/watchvideo" );
	auto menu_hp_train = getNodeByPath<Node>( card, "menu_hp/train" );
	auto menu_purchase = getNodeByPath<Menu>( card, "menu_purchase" );	
	auto insquad = getNodeByPath<Node>( card, "command_in" );
	auto outsquad = getNodeByPath<Node>( card, "command_out" );
	menu_train->setVisible( false );
	menu_purchase->setVisible( false );
	if( menu_hp_exp )			menu_hp_exp->setVisible( isAvailabled && isBought && level < max );
	if( menu_hp_adstext )		menu_hp_adstext->setVisible( isAvailabled && isBought && level < max );
	if( menu_hp_watchvideo )	menu_hp_watchvideo->setVisible( isAvailabled && isBought && level < max );
	if( menu_hp_train ) menu_hp_train->setVisible( false );
	if (insquad)	insquad->setVisible( false );
	if (outsquad)	outsquad->setVisible( false );
}

void HeroCard::fetchSquad()
{
	auto in = getNodeByPath<Node>( this, "info/menu_squad/insquad" );
	auto out = getNodeByPath<Node>( this, "info/menu_squad/outsquad" );
	auto buttonin = getNodeByPath<Node>( this, "info/menu_squad/button_insquad" );
	auto buttonout = getNodeByPath<Node>( this, "info/menu_squad/button_outsquad" );

	int heroesAvailabled = Config::shared().get<int>( "heroesAvailabled" );
	std::string heroname = "hero" + toStr( _heroIndex + 1 );
	bool isBought = HeroExp::shared().isHeroAvailabled( heroname );
	bool isAvailabled = _heroIndex < heroesAvailabled;
	bool selected = HeroRoom2::isHeroSelected( _heroIndex );
	in->setVisible( selected && isAvailabled && isBought);
	buttonin->setVisible( selected && isAvailabled  && isBought );
	out->setVisible( !selected && isAvailabled  && isBought );
	buttonout->setVisible( !selected && isAvailabled  && isBought );
}

void HeroCard::fetchPreview()
{
	auto sprite = getNodeByPath<Sprite>( this, "info/preview/hero" );
	auto action = xmlLoader::load_action_from_file( "ini/heroroom2/actions/hero" + toStr( _heroIndex + 1 ) + ".xml" );
	if( sprite && action )
	{
		sprite->stopAllActions();
		sprite->runAction( action );
	}
}

void HeroCard::fetchParameters()
{
    
    
	std::string name = "hero/hero" + toStr( _heroIndex + 1 );
	auto info = mlUnitInfo::shared().info( name );
	auto d = info.damage;
	auto a = info.armor;
	auto v = info.velocity;
	auto h = info.health;
    
    
    if (_heroIndex + 1 == 1)
    {
        bool isHero1SpeedEasterEggUnlocked = EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kHero1DoubleSpeed);
        if (isHero1SpeedEasterEggUnlocked)
        {
            d *= 2;
            a *= 2;
            h *= 2;
            v *= 2;
        }
    }    
    auto damage = getNodeByPath<Text>( this, "info/fightparams/attack" );
	auto armor = getNodeByPath<Text>( this, "info/fightparams/armor" );
	auto velocity = getNodeByPath<Text>( this, "info/fightparams/speed" );
	auto health = getNodeByPath<Text>( this, "info/fightparams/health" );
    
	if (damage)		damage->setString( toStr<int>( d ) );
	if (armor)		armor->setString( toStr<int>( a ) );
	if (velocity)	velocity->setString( toStr<int>( v ) );
	if (health)		health->setString( toStr<int>( h ) );
}

void HeroCard::fetchSkills()
{
	std::string heroname = "hero" + toStr( _heroIndex + 1 );
	int heroesAvailabled = Config::shared().get<int>( "heroesAvailabled" );
	bool isBought = HeroExp::shared().isHeroAvailabled( heroname );
	bool isAvailabled = _heroIndex < heroesAvailabled;
	std::vector<unsigned> skills;
	HeroExp::shared().skills( heroname, skills );
	auto pointCount = HeroExp::shared().skillPoints( heroname );

	assert( skills.size() <= _skillButtons.size() );
	for( size_t i = 0; i < skills.size(); ++i )
	{
		auto level = skills[i];
		auto button = _skillButtons[i];
		auto label = getNodeByPath<Text>( button, "points" );
		if( label ) label->setString( toStr( level ) + "/3" );

		bool canUpgrade = isAvailabled && pointCount > 0 && level < 3 && isBought;
		button->useScaleEffect( !canUpgrade );
		if( canUpgrade )
			button->setImageSelected( button->getParamCollection().get( "imageS" ) );
		else
			button->setImageSelected( button->getParamCollection().get( "imageN" ) );
	}
	for( size_t i = skills.size(); i < _skillButtons.size(); ++i )
		_skillButtons[i]->setVisible( false );

	auto points = getNodeByPath<Text>( this, "info/skills/reset/points" );
	auto reset = getNodeByPath<MenuItem>( this, "info/skills/reset" );
	if( points )points->setString( toStr( pointCount ) );
	if( reset )reset->setVisible( isAvailabled && isBought );

	xmlLoader::macros::set( "points", toStr( pointCount ) );
	auto node = xmlLoader::load_node( "ini/heroroom2/actions/increase_skillpoint.xml" );
	if( node )
	{
		node->setPosition( points->getPosition() );
		points->getParent()->addChild( node );
	}
}

void HeroCard::fetchMenu()
{
	std::string heroname = "hero" + toStr( _heroIndex + 1 );
	int heroesAvailabled = Config::shared().get<int>( "heroesAvailabled" );
	bool isBought = HeroExp::shared().isHeroAvailabled( heroname );
	bool asInapp = HeroExp::shared().isHeroAsInapp(heroname);
	unsigned availabledAfterLevel = HeroExp::shared().getLevelForUnlockHero(heroname);
	bool isAvailabled = _heroIndex < heroesAvailabled;
	int level = HeroExp::shared().getLevel( HeroExp::shared().getEXP( heroname ) );
	int max = 15;
	auto levelup =  getNodeByPath<mlMenuItem>( this, "info/menu/levelup" );
	auto purchase = getNodeByPath<MenuItem>( this, "info/menu/purchase" );
	auto getpro = getNodeByPath<MenuItem>(this, "info/menu/getpro");
	auto levelnumbertext = getNodeByPath<Text>(this, "info/menu/levelnumbertext");

	if( purchase )
	{
		purchase->setVisible( !isBought && isAvailabled && asInapp );
		auto skuDetails = HeroExp::shared().getHeroSkuDetails( heroname );
		auto purchasetext = getNodeByPath<Text>( purchase, "normal/text" );
		if( purchasetext )
		{
			std::string str = purchasetext->getString() + skuDetails.priceText;
			purchasetext->setString( str );
		}
	}
	if( levelup )
	{
		levelup->setVisible( isAvailabled && isBought && level < max );
		auto cost = getNodeByPath<Text>( levelup, "normal/cost" );
		if( cost )cost->setString( toStr( getBuyLevelCost() ) );
		if (levelup->getImageDisabled().empty() == false)
		{
			auto dcost = getNodeByPath<Text>(levelup, "disabled/cost");
			if (dcost)dcost->setString(toStr(getBuyLevelCost()));
			bool isEnoughMoney = ScoreCounter::shared().getMoney(kScoreCrystals) >= getBuyLevelCost();
			levelup->setEnabled(isEnoughMoney);			
		}
	}
	if( getpro )
	{
		getpro->setVisible( !isAvailabled );
	}
	if (levelnumbertext && !isBought && !asInapp && availabledAfterLevel > 0)
	{
		levelnumbertext->setVisible(true);
		std::string text = "#complitelevelforunlock_" + toStr(availabledAfterLevel) + "#";
		levelnumbertext->setString(WORD(text));
	}
}

int HeroCard::getBuyLevelCost()const
{
	auto gift = UserData::shared().get <int>("heropoints_gift");
	if (gift > 0)
		return 0;

	unsigned maxlevel = HeroExp::shared().getMaxLevel();
	auto heroname = "hero" + toStr( _heroIndex + 1 );
	auto exp = HeroExp::shared().getEXP( heroname );
	unsigned level = HeroExp::shared().getLevel( exp );
	return level < maxlevel ? HeroExp::shared().getCostLevelup( level ) : 0;
}


NS_CC_END
