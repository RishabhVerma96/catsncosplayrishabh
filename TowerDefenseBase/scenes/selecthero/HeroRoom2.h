#ifndef __HeroRoom2_h__
#define __HeroRoom2_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "ml/MenuItem.h"
NS_CC_BEGIN

class HeroCard;

class HeroRoom2 : public LayerExt, public LayerBlur
{
	DECLARE_BUILDER( HeroRoom2 );
	bool init();
public:
	static void fetchHeroCard( mlMenuItem *card, int heroIndex );
	static bool isHeroSelected( int heroindex );

	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	virtual void onEnter()override;
	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;
    virtual void disappearance() override;
protected:
	virtual bool isBlurActive() override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;
	int computeIndexHeroByButton( MenuItem* button );
	void openHeroCard( Ref*sender, int indexhero );
	void showVideo( Ref*sender, int indexhero );
	void purchase( Ref*sender, int indexhero );
    void unlockByVideo( Ref*sender );
	void getPro();

	void buildList();
	void fetchList();

private:
	IntrusivePtr<HeroCard> _openCard;
	IntrusivePtr<Node> _hidenCard;
    
    int getHeroCount();

};

class HeroCard : public LayerExt, public LayerBlur
{
	DECLARE_BUILDER( HeroCard );
	bool init( HeroRoom2* roomlayer, int heroindex, Node* herocard );
public:
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	void fetch();
	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;
protected:
	virtual bool isBlurActive() override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;
	void skillSelect( int index );
	void skillUpgrade();
	void skillShowDescription();
	void skillReset();

	void buyLevel();
	void squad( bool select );

	void fetchCard();
	void fetchSquad();
	void fetchPreview();
	void fetchParameters();
	void fetchSkills();
	void fetchMenu();

	int getBuyLevelCost()const;
private:
	int _heroIndex;
	int _skillSelected;
	HeroRoom2* _roomlayer;
	std::vector<mlMenuItem::Pointer> _skillButtons;
    
};




NS_CC_END
#endif // #ifndef HeroRoom2
