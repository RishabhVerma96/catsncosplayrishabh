#ifndef __SlotMachineLayer_h__
#define __SlotMachineLayer_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "support/Award.h"
NS_CC_BEGIN

class SlotMachineLayer : public LayerExt
{
	DECLARE_BUILDER( SlotMachineLayer );
	bool init();

public:
	struct Combination
	{
		int _multiplier;
		std::string _value;

		Combination( int multiplier = 1, std::string value = "" ) :
			_multiplier( multiplier ), _value( value ) {}
	};

public:
	virtual ccMenuCallback get_callback_by_description( const std::string & name ) override;
	virtual bool loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode ) override;
protected:
	virtual void disappearance()override;
	void spin();
	void stop();
	void getAward();
	void buildMachine();
	void buildReel( int index );
	void generateCombination();
	void setStake( int value );
	void update( float dt );
private:
	bool _locked;
	bool _stakeDone;
	float _velocity;
	int _reelCount;
	float _reelHeight;
	float _sectionHeight;
	int _sectionCount;
	int _stake;
	int _sevenId;
	std::vector< int > _resultCombination;
	std::vector< int > _currentCombination;
	std::vector< bool > _upsideDown;
	std::map < std::string, int > _winCombinations;

	LabelPointer _rewardNode;
};

NS_CC_END
#endif // #ifndef SlotMachineLayer
