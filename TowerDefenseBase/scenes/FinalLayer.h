#ifndef __FinalLayer_h__
#define __FinalLayer_h__
#include "cocos2d.h"
#include "macroses.h"
#include "ml/NodeExt.h"
#include "inapp/purchase.h"
#include "types.h"
NS_CC_BEGIN





class FinalLayer : public LayerExt
{
	DECLARE_BUILDER( FinalLayer );
	bool init();
public:
	virtual void onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event ) override;
protected:
	virtual ccMenuCallback get_callback_by_description( const std::string & name );
	void removeScoreLayer();
private:
};


NS_CC_END
#endif // #ifndef SelectHero