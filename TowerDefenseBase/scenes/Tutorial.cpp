//
//  Tutorial.cpp
//  JungleDefense
//
//  Created by Vladimir Tolmachev on 24.04.14.
//
//
#include "ml/SmartScene.h"
#include "ml/loadxml/xmlProperties.h"
#include "ml/ImageManager.h"
#include "ml/Language.h"
#include "ml/Audio/AudioEngine.h"
#include "Tutorial.h"
#include "resources.h"
#include "gameboard.h"
#include "GameLayer.h"
#include "consts.h"
#include "UserData.h"
#include "ScoreCounter.h"
#include "configuration.h"
#include "Hero.h"
NS_CC_BEGIN;

const std::string ksAutoPosition( "autopos" );
const std::string ksParent( "parent" );
const std::string ksAsBlockingLayer( "asblockinglayer" );
const std::string ksCloseByTap( "closebytap" );
const std::string ksTakeTouch( "taketouch" );
const std::string ksCountTapsForClose( "counttapforclose" );
const std::string ksNextTutorial( "nexttutorial" );
const std::string ksShopGift( "shopgift" );
const std::string ksSetLevelScore( "setlevelsscore" );
const std::string ksSelectHero( "selecthero" );
const std::string ksResetHeroSkills( "resetheroskills" );
const std::string ksHerominlevel( "herominlevel" );
const int kAutoPosition( xmlLoader::kUserProperties + 1 );
const int kParent( xmlLoader::kUserProperties + 2 );
const int kAsBlockingLayer( xmlLoader::kUserProperties + 3 );
const int kCloseByTap( xmlLoader::kUserProperties + 4 );
const int kTakeTouch( xmlLoader::kUserProperties + 5 );
const int kNextTutorial( xmlLoader::kUserProperties + 6 );
const int kShopGift( xmlLoader::kUserProperties + 7 );
const int kSetLevelScore( xmlLoader::kUserProperties + 8 );
const int kSelectHero( xmlLoader::kUserProperties + 9 );
const int kResetHeroSkills( xmlLoader::kUserProperties + 10 );
const int kHerominlevel( xmlLoader::kUserProperties + 11 );
const int kCountTapsForClose( xmlLoader::kUserProperties + 12 );

/******************************************************************************/
//MARK: Tutorial
/******************************************************************************/

Tutorial::Tutorial()
: _testMode(false)
, _tapsForClose(1)
, takeTouch(false)
, closeOnTap(false)
{}

Tutorial::~Tutorial()
{}

bool Tutorial::init( const std::string & pathToXml, bool testMode )
{
	_testMode = testMode;
	load( pathToXml );

	//assert( getParent() != nullptr );
	//{
	//	auto scene = Director::getInstance()->getRunningScene();
	//	auto smartscene = dynamic_cast<SmartScene*>(scene);
	//}

	return true;
}

bool Tutorial::setProperty( int intproperty, const std::string & value )
{
	Node * node( nullptr );
	switch( intproperty )
	{
		case xmlLoader::kPos:
			setPosition( getPosition() + strTo<Point>( value ) );
			break;
		case kAutoPosition:
			node = getNodeByPath( Director::getInstance()->getRunningScene(), value );
			if( node )
				setPosition( getPosition() + node->getPosition() );
			else
				setPosition( Point( Director::getInstance()->getOpenGLView()->getDesignResolutionSize() / 2 ) );
			break;
		case kParent:
			//assert( _testMode || getParent() == nullptr );
			if( getParent() ) removeFromParent();
			node = getNodeByPath( Director::getInstance()->getRunningScene(), value );
//            traverseAllChildrun(Director::getInstance()->getRunningScene());
            CCLOG("Discovered node :: %s",node->getName().c_str());
            if( node ) {
                node->addChild( this );
            }
			break;
		case kAsBlockingLayer:
			if( strTo<bool>( value ) )
			{
				auto scene = Director::getInstance()->getRunningScene();
				auto smartscene = dynamic_cast<SmartScene*>(scene);
				smartscene->pushLayer( this, true );
                setGlobalZOrder(10000.0f);
			}
			break;
		case kCountTapsForClose:
			_tapsForClose = strTo<int>( value );
			if( _tapsForClose > 0 )
				listenTouches();
			else
				unlistenTouches();
			break;
        case kTakeTouch:
            takeTouch = strTo<bool>( value );
            if (takeTouch) {
                listenTouches();
            }
            break;
		case kCloseByTap:
            closeOnTap = strTo<bool>( value );
			if( strTo<bool>( value ) )
				listenTouches();
			else
				unlistenTouches();
			break;
		case kNextTutorial:
			_nextTutorial = value;
			break;
		case kShopGift:
			UserData::shared().write( k::user::ShopGift, value );
			break;
		case kSetLevelScore:
			ScoreCounter::shared().setMoney( kScoreLevel, strTo<int>( value ), false );
			break;
		case kSelectHero:
			UserData::shared().hero_select( strTo<int>( value ) );
			break;
		case kResetHeroSkills:
		{
			auto heroname = "hero" + toStr( strTo<int>( value ) + 1 );
			std::vector<unsigned> skills( 5 );
			HeroExp::shared().setSkills( heroname, skills );
			break;
		}
		case kHerominlevel:
		{
			auto current = UserData::shared().hero_getSelected();
			auto heroname = "hero" + toStr( current.front() + 1 );
			auto exp = HeroExp::shared().getEXP( heroname );
			auto expmin = HeroExp::shared().getHeroLevelExtValue( strTo<int>( value ) ) + 1;
			exp = std::max( exp, expmin );
			HeroExp::shared().setEXP( heroname, exp );
			break;
		}
		default:
			return NodeExt::setProperty( intproperty, value );
	}
	return true;
}

void Tutorial::traverseAllChildrun(cocos2d::Node *root) {
    
    
    for (int i = 0; i < root->getChildren().size(); i++) {
        Node *childNode = root->getChildren().at(i);
        CCLOG("Child name : %s parent name :: %s",childNode->getName().c_str(),root->getName().c_str());
        if(childNode->getChildrenCount() > 0) {
            traverseAllChildrun(childNode);
        }
    }
}

void Tutorial::enter()
{
	runEvent( "onenter" );
    lastSoundID = AudioEngine::shared().playEffect( _sound, true, 0 );
}

void Tutorial::exit()
{
	runEvent( "onexit" );
    AudioEngine::shared().stopEffect(lastSoundID);
}

const std::string& Tutorial::next()const
{
	return _nextTutorial;
};

ccMenuCallback Tutorial::get_callback_by_description( const std::string & name )
{
	if( name == "confirm_tutorial_yes" )
		return std::bind( &Tutorial::cb_confirmTutorial, this, std::placeholders::_1, true );
	if( name == "confirm_tutorial_no" )
		return std::bind( &Tutorial::cb_confirmTutorial, this, std::placeholders::_1, false );
	return nullptr;
}

void Tutorial::cb_confirmTutorial( Ref*, bool use )
{
	UserData::shared().write( k::user::UseTitorial, use );
	TutorialManager::shared().close( this );
}

void Tutorial::listenTouches()
{
    auto *listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2( Tutorial::onTouchBegan, this );
    listener->onTouchEnded = CC_CALLBACK_2( Tutorial::onTouchEnded, this );
    listener->setSwallowTouches(true);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority( listener, this );
}

void Tutorial::unlistenTouches()
{
	Director::getInstance()->getEventDispatcher()->removeEventListenersForTarget( this );
}

bool Tutorial::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event * event) {
//    CCLOG("inside tutorial touch began");
    auto location = touch->getLocation();
    location = this->convertToNodeSpace( location );
    
    if(takeTouch && _forced) {
        CCLOG("it should swallow");
        return !rectContainsPoint(location);
    }
    else {
        CCLOG("do not swallow");
    }
    return (closeOnTap || (takeTouch && _forced));
}

void Tutorial::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event * event) {
//    CCLOG("inside tutorial touch ended");
    
    if( --_tapsForClose == 0 && closeOnTap) {
        TutorialManager::shared().close( this );
    }
}

void Tutorial::onTouchCanceled( Touch* touch, Event *event ) {
    CCLOG("inside tutorial touch Canceled");
}

bool Tutorial::rectContainsPoint(cocos2d::Vec2 point) {
    Node *clickableArea = getChildByName("clickableArea");
    if(clickableArea != nullptr) {
        Rect clickableRect = clickableArea->getBoundingBox();
//        CCLOG("Clickable rectangle area :: %f %f %f %f",clickableRect.origin.x, clickableRect.origin.y, clickableRect.size.width, clickableRect.size.height);
//        CCLOG("Touch location x :: %f y :: %f",point.x, point.y);
        if(clickableRect.containsPoint(point)) {
//            CCLOG("point inside clickable bounds");
            return true;
        }
    }
    return false;
}

//void Tutorial::touchesBegan( const std::vector<Touch*>& touches, Event *event )
//{
//    CCLOG("inside tutorial touch began");
//}
//
//void Tutorial::touchesEnded( const std::vector<Touch*>& touches, Event *event )
//{
//
//    if( --_tapsForClose == 0 && !takeTouch) {
//        TutorialManager::shared().close( this );
//    }
//
//}

void Tutorial::setForced(bool isForced) {
	_forced = isForced;
}

void Tutorial::setSound(std::string sound) {
    _sound = sound;
}

bool Tutorial::actionCompleted() {
	return !_forced;
}

/******************************************************************************/
//MARK: TutorialManager
/******************************************************************************/

TutorialManager::TutorialManager()
{
	xmlLoader::bookProperty( ksAutoPosition, kAutoPosition );
	xmlLoader::bookProperty( ksParent, kParent );
	xmlLoader::bookProperty( ksAsBlockingLayer, kAsBlockingLayer );
	xmlLoader::bookProperty( ksCloseByTap, kCloseByTap );
    xmlLoader::bookProperty( ksTakeTouch, kTakeTouch );
	xmlLoader::bookProperty( ksNextTutorial, kNextTutorial );
	xmlLoader::bookProperty( ksShopGift, kShopGift);
	xmlLoader::bookProperty( ksSetLevelScore, kSetLevelScore );
	xmlLoader::bookProperty( ksSelectHero, kSelectHero );
	xmlLoader::bookProperty( ksResetHeroSkills, kResetHeroSkills );
	xmlLoader::bookProperty( ksHerominlevel, kHerominlevel );
	xmlLoader::bookProperty( ksCountTapsForClose, kCountTapsForClose );
}

TutorialManager::TutorialManager( TutorialManager&& )
{}

TutorialManager::TutorialManager( const TutorialManager& )
{}

TutorialManager& TutorialManager::operator=(const TutorialManager&)
{
	return *this;
}

void TutorialManager::onCreate()
{
	load();
}

void TutorialManager::setEnabled( bool enabled )
{
	_enabled = enabled;
}

bool TutorialManager::dispatch( const std::string & eventname, const ParamCollection * params )
{
	CCLOG("tutorial event dispatched :: %s",eventname.c_str());
	if( !_enabled )
		return false;

    
	bool result( false );

	if( _current && _current->isRunning() == false)
	{
		close( _current );
		result = true;
	}

	if( !_current && _queueEvents.empty() == false )
	{
		auto pair = _queueEvents.front();
		_queueEvents.pop();
		if( dispatch(pair.first, &pair.second) )
		{
			return true;
		}
	}

	if( _current)
	{
		auto range = _eventsForClose.equal_range( eventname );
		for( auto iter = range.first; iter != range.second; ++iter )
		{
			if( iter->second == _current->getName() )
			{
				if( close( _current ) == false )
					dispatch( eventname, params );
				result = true;
				break;
			}
		}
		//if( result == false )
		//{
		//	auto range = _eventsForRun.equal_range( eventname );
		//	for( auto iter = range.first; iter != range.second; ++iter )
		//	{
		//		if( checkOpening( iter->second ) )
		//		{
		//			std::pair< std::string, ParamCollection > pair;
		//			pair.first = eventname;
		//			if( params ) pair.second = *params;
		//			_queueEvents.push( pair );
		//		}
		//	}
		//}

	}
	else
	{
		auto range = _eventsForRun.equal_range( eventname );
		for( auto iter = range.first; iter != range.second; ++iter )
		{
			std::string name = iter->second;
			if( open( name ) )
			{
				result = true;
				break;
			}
		}
	}
	return result;
}

bool TutorialManager::close( Tutorial * tutorial )
{
	if( _current && tutorial == _current )
	{
		int value = UserData::shared().get <int>("tutorial" + _current->getName());
		UserData::shared().write("tutorial" + _current->getName(), value + 1);

		_current->exit();
		_current->removeFromParent();

		std::string next = _current->next();
		_current.reset( nullptr );

		if( next.empty() == false )
			open( next );

		return next.empty() == false;
	}
	else
	{
		assert( tutorial );
		tutorial->exit();
		tutorial->removeFromParent();
		return true;
	}
}

bool TutorialManager::open( const std::string & name, bool ignorePredelay )
{
	if( _list.find( name ) == _list.end() )
        return false;
	const TutorialInfo& info = _list[name];
	bool result( false );

	int value = UserData::shared().get <int>("tutorial" + name, 0);
	if (checkOpening(name))
	{
		UserData::shared().write( "tutorial" + name, value + 1 );

		assert( info.filename.empty() == false );
		if( ignorePredelay || info.predelay <= 0.001f )
		{
            if(IS18_9Ratio && !info.iPhoneXName.empty()) {
                _current = Tutorial::create( info.iPhoneXName );
            }
            else {
                _current = Tutorial::create( info.filename );
            }
			_current->setName( name );
            _current->setSound(info.sound);
			_current->setForced(info.forced);
			_current->enter();
            CCLOG("tutorial started :: %s sound name :: %s",info.filename.c_str(),info.sound.c_str());
		}
		else
		{
			delayedOpen( info.predelay, name );
		}
		result = true;
	}

	return result;
}

bool TutorialManager::checkOpening( const std::string& tutorialname )const
{
	auto iter = _list.find( tutorialname );
	if (iter == _list.end())
		return false;
	const TutorialInfo& info = iter->second;
 
    if (info.tutorialStarted) {
        UserData::shared().write("ShowTutorial", 0);
    }
    
    int showTutorial = UserData::shared().get<int>("ShowTutorial");
    
	bool isshow( true );
	int value = UserData::shared().get("tutorial" + tutorialname, 0);
	bool rejection = UserData::shared().get(k::user::UseTitorial, true);
	std::string after = info.onlyaftertutorial;
	bool showprevios = after.empty() ? true : UserData::shared().get <int>("tutorial" + after) > 0;
	isshow = isshow && showprevios;
    //#parth changes
    isshow = isshow && value < info.count;
	isshow = isshow && (rejection || info.forced);
    isshow = isshow && (showTutorial < 1);
    
    
    if (info.tutorialEnded) {
        UserData::shared().write("ShowTutorial", 1);
    }
    
	return isshow;
}

void TutorialManager::delayedOpen( float delay, const std::string& name )
{
	auto scheduler = Director::getInstance()->getScheduler();
	auto callback = std::bind( &TutorialManager::delayedOpenTimer, this, std::placeholders::_1, name );
	scheduler->schedule( callback, this, delay, false, "TutorialManagerDelayedOpen");
}

void TutorialManager::delayedOpenTimer( float dt, const std::string& name )
{
	open( name, true );
	auto scheduler = Director::getInstance()->getScheduler();
	scheduler->unschedule( "TutorialManagerDelayedOpen", this );
}

void TutorialManager::load()
{
	pugi::xml_document doc;
	doc.load_file( "ini/tutorial/tutorials.xml" );
	auto root = doc.root().first_child();
	auto xmllist = root.child( "list" );
	auto xmlevents = root.child( "events" );
	auto xmlrun = xmlevents.child( "run" );
	auto xmlclose = xmlevents.child( "close" );

	loadList( xmllist );
	loadEvents( xmlrun, _eventsForRun );
	loadEvents( xmlclose, _eventsForClose );
}

void TutorialManager::loadList( const pugi::xml_node & xmlnode )
{
	FOR_EACHXML( xmlnode, child )
	{
		auto dispatch = true;
		if( child.attribute( "dispatch" ) )
		{
			std::string value = child.attribute( "dispatch" ).as_string();
			dispatch = strTo<bool>( xmlLoader::macros::parse( value ) );
		}
		if (dispatch == false)
			continue;

		TutorialInfo info;
		std::string name = child.name();
		info.filename = child.attribute( "filename" ).value();
        info.iPhoneXName = child.attribute( "iPhoneXName" ).value();
		info.onlyaftertutorial = child.attribute( "after" ).value( );
        info.sound = child.attribute("sound").value();
		info.count = child.attribute( "count" ).as_int( 1 ) * 2;
		info.forced = child.attribute( "forced" ).as_bool( false );
        info.tutorialEnded = child.attribute("tutorialEnded").as_bool();
        info.tutorialStarted = child.attribute("tutorialStarted").as_bool();
//		std::string forcedString;
//		if(info.forced){
//			forcedString = std::string("true");
//		}else{
//			forcedString = std::string("false");
//		}
//		CCLOG("tutorial name :: %s forced :: %s",info.filename.c_str(),forcedString.c_str());
		info.predelay = child.attribute( "predelay" ).as_float( 0 );
		_list[name] = info;
	}
}

void TutorialManager::loadEvents( const pugi::xml_node & xmlnode, std::multimap<std::string, std::string> & events )
{
	FOR_EACHXML( xmlnode, child )
	{
		std::string eventname = child.name();
		std::string tutorial = child.attribute( "value" ).value();
		events.insert( std::pair<std::string, std::string>( eventname, tutorial ) );
	}
}


NS_CC_END;
