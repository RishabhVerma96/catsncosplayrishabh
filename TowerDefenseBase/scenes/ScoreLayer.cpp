//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "ml/SmartScene.h"
#include "ScoreLayer.h"
#include "ScoreCounter.h"
#include "ShopLayer.h"
#include "ShopLayer2.h"
#include "configuration.h"
#include "../Services/sdkbox/play/sdkboxPlay.h"

NS_CC_BEGIN

ScoreLayer::ScoreLayer()
: _updateScores( false )
, _useActionForScore(false)
, _gold( 0 )
, _fuel( 0 )
, _star( 0 )
, _ticket( 0 )

{
	ScoreCounter::shared().observer( kScoreTime ).add( _ID, std::bind( &ScoreLayer::change_time, this, std::placeholders::_1 ) );
	ScoreCounter::shared().observer( kScoreFuel ).add( _ID, std::bind( &ScoreLayer::change_fuel, this, std::placeholders::_1 ) );
	ScoreCounter::shared().observer( kScoreCrystals ).add( _ID, std::bind( &ScoreLayer::change_real, this, std::placeholders::_1 ) );
	ScoreCounter::shared().observer( kScoreStars ).add( _ID, std::bind( &ScoreLayer::change_star, this, std::placeholders::_1 ) );
	ScoreCounter::shared().observer( kScoreTicket ).add( _ID, std::bind( &ScoreLayer::change_ticket, this, std::placeholders::_1 ) );
}

ScoreLayer::~ScoreLayer()
{
	ScoreCounter::shared().observer( kScoreTime ).remove( _ID );
	ScoreCounter::shared().observer( kScoreFuel ).remove( _ID );
	ScoreCounter::shared().observer( kScoreCrystals ).remove( _ID );
	ScoreCounter::shared().observer( kScoreStars ).remove( _ID );
	ScoreCounter::shared().observer( kScoreTicket ).remove( _ID );
}

bool ScoreLayer::init()
{
	do
	{
		CC_BREAK_IF( !Layer::init() );
		CC_BREAK_IF( !NodeExt::init() );

        if (Config::shared().get<bool>( "useFuel" )) {
            NodeExt::load( "ini", "scorelayer_fuel.xml" );
        } else {
            NodeExt::load( "ini", "scorelayer.xml" );
        }

		std::string pathToTimeAdvanced = getParamCollection().get( "pathto_fueltimeadvanced", "fueltimeadvanced" );
		std::string pathToGold = getParamCollection().get( "pathto_gold", "valuegold" );
		std::string pathToFuel = getParamCollection().get( "pathto_fuel", "valuefuel" );
		std::string pathToTime = getParamCollection().get( "pathto_fueltime", "valuetime" );
		std::string pathToStar = getParamCollection().get( "pathto_star", "valuestar" );
		std::string pathToTicket = getParamCollection().get( "pathto_ticket", "valueticket" );

		_timeNodeAdvanced = getNodeByPath( this, pathToTimeAdvanced );
		_goldNode = getNodeByPath<Text>( this, pathToGold );
		_fuelNode = getNodeByPath<Text>( this, pathToFuel );
		_timeNode = getNodeByPath<Text>( this, pathToTime );
		_starNode = getNodeByPath<Text>( this, pathToStar );
		_ticketsNode = getNodeByPath<Text>( this, pathToTicket );
		auto shop = getNodeByPath<MenuItem>( this, "menu/shop" );
		CC_BREAK_IF( !_goldNode );
		CC_BREAK_IF( !_fuelNode );
		CC_BREAK_IF( !_timeNode );
		CC_BREAK_IF( !_starNode );

		change_fuel( ScoreCounter::shared().getMoney( kScoreFuel ) );
		change_time( ScoreCounter::shared().getMoney( kScoreTime ) );
		change_real( ScoreCounter::shared().getMoney( kScoreCrystals ) );
		change_star( ScoreCounter::shared().getMoney( kScoreStars ) );
		change_ticket( ScoreCounter::shared().getMoney( kScoreTicket ) );
		if( shop )
		{
			shop->setCallback( std::bind( &ScoreLayer::cb_shop, this, std::placeholders::_1 ) );
			if( Config::shared().get<bool>( "useInapps" ) == false )
				shop->setVisible( false );
		}

		if( Config::shared().get<bool>( "useFuel" ) == false )
		{
			_fuelNode->setVisible( false );
			_timeNode->setVisible( false );
		}

		scheduleUpdate();
		return true;
	}
	while( false );
	return false;
}

ccMenuCallback ScoreLayer::get_callback_by_description( const std::string & name )
{
	if( name == "shop" ) return std::bind( &ScoreLayer::cb_shop, this, std::placeholders::_1 );
	if( name.find( "shop:" ) == 0 ) return ShopLayer2::dispatch_query( name );
	return NodeExt::get_callback_by_description( name );
}

bool ScoreLayer::setProperty( const std::string & stringproperty, const std::string & value )
{
	if( stringproperty == "useaction" )_useActionForScore = strTo<bool>( value );
	else return NodeExt::setProperty( stringproperty, value );
	return true;
}

void ScoreLayer::update( float )
{
	if( _updateScores )
		updateScores();
}

void ScoreLayer::updateScores()
{
	if( _timeNode )
		_timeNode->setString( _time );
	change( _fuelNode, _fuel );
	change( _goldNode, _gold );
	change( _starNode, _star );
	change( _ticketsNode, _ticket );
	_updateScores = false;
}

void ScoreLayer::change_time( int score )
{
	int min = int( score ) / 60;
	int sec = int( score ) - min * 60;

	std::stringstream ss;
	if( min != 0 ) ss << min << "m ";
	if( sec != 0 ) ss << sec << "s";
	_time = ss.str();
	_updateScores = true;
}

void ScoreLayer::change_fuel( int score )
{
	_fuel = score;
	_updateScores = true;
	bool vis = score < ScoreByTime::shared().max_fuel();
	if( _timeNode )
		_timeNode->setVisible( vis );
	if( _timeNodeAdvanced )
		_timeNodeAdvanced->setVisible( vis );
	if( Config::shared().get<bool>( "useFuel" ) == false )
	{
		if( _fuelNode )
			_fuelNode->setVisible( false );
		if( _timeNode )
			_timeNode->setVisible( false );
		if( _timeNodeAdvanced )
			_timeNodeAdvanced->setVisible( false );
	}
}

void ScoreLayer::change_real( int score )
{
	_gold = score;
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    sdkbox::play::submitScore(score);
#endif
	_updateScores = true;
}

void ScoreLayer::change_star( int score )
{
	_star = score;
	_updateScores = true;
}

void ScoreLayer::change_ticket( int score )
{
	_ticket = score;
	_updateScores = true;
}

void ScoreLayer::cb_shop( Ref*sender )
{
#if PC != 1
	SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
	if( scene->getChildByName( "shop" ) ) return;

	auto shop = ShopLayer::create( Config::shared().get<bool>( "useFreeFuel" ), true, false, false );
	if( shop )
	{
		scene->pushLayer( shop, true );
	}
#endif
}

void ScoreLayer::change( LabelPointer label, int value )
{
	if( !label )
		return;
	if( _useActionForScore == false )
	{
		label->setString( toStr( value ) );
	}
	else
	{
		auto current = label->getString();
		if( current.empty() == false )
		{
			auto action = ActionText::create( 0.5f, value, true );
			label->runAction( action );
		}
		else
		{
			label->setString( toStr( value ) );
		}
	}
}



NS_CC_END
