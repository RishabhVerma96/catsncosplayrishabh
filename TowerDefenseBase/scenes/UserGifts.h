#ifndef __UserGifts_h__
#define __UserGifts_h__

#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "ml/Scissor.h"
#include "NodeExt.h"
#include "support/Award.h"
NS_CC_BEGIN

class UserGifts :
	public LayerExt	
{
	DECLARE_BUILDER( UserGifts );
	bool init();
public:
protected:
	virtual ccMenuCallback get_callback_by_description( const std::string & name ) override;
	virtual bool loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )override;
private:
	std::vector< Award::Pointer > _awards;
};

NS_CC_END


#endif // #ifdef __UserGifts_h__