#include "ShareLayer.h"
#include "ml/Config.h"
#include "UserData.h"
#include "consts.h"
#include "ScoreCounter.h"

NS_CC_BEGIN

const std::vector<std::string> ShareLayer::networks( { "facebook", "twitter" } );

ShareLayer::ShareLayer()
{
}

ShareLayer::~ShareLayer()
{
}

bool ShareLayer::init(const std::string & network)
{
	do
	{
		auto shareAward = Config::shared().get<int>( "shareAward" );
		xmlLoader::macros::set( "share_award", toStr( shareAward ) );

		NodeExt::load( "ini/sharelayer.xml" );

		xmlLoader::macros::erase( "share_award" );

		_network = network;
		CC_BREAK_IF( runEvent( network ) == false );
		
		return true;
	} while( false );
	return false;
}

ccMenuCallback ShareLayer::get_callback_by_description( const std::string & name )
{
	if( name.find( "openurl:" ) == 0 )
	{
		auto urlName = name.substr( strlen( "openurl:" ) );
		auto url = Config::shared().get( urlName );

		auto cb = [this,url]( Ref* )
		{
			auto shareAward = Config::shared().get<int>( "shareAward" );
			Application::getInstance()->openURL( url );
			runEvent( "awarded_" + _network );
			ScoreCounter::shared().addMoney( kScoreCrystals, shareAward, true );
			UserData::shared().write( k::user::SharePrefix + _network, true );
			UserData::shared().save();
		};


		return cb;
	}
	return LayerExt::get_callback_by_description( name );
}


NS_CC_END