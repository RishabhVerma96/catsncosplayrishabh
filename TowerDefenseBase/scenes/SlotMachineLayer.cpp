#include "ml/loadxml/xmlProperties.h"
#include "ml/Audio/AudioEngine.h"
#include "ml/ObjectFactory.h"
#include "ml/SmartScene.h"
#include "plugins/AdsPlugin.h"
#include "SlotMachineLayer.h"
#include "resources.h"
#include "support.h"
#include "ScoreCounter.h"
#include "UserData.h"
#include "MapLayer.h"
#include "GameLayer.h"
#include "Tutorial.h"
#include "GameScene.h"

NS_CC_BEGIN

SlotMachineLayer::SlotMachineLayer()
: _locked(false)
, _stakeDone(false)
, _velocity(500.f)
, _reelCount(3)
, _reelHeight(360.f)
, _sectionCount(3)
, _stake(0)
, _upsideDown(false)
, _sevenId(0)
{}
SlotMachineLayer::~SlotMachineLayer()
{}

bool SlotMachineLayer::init()
{
	do
	{
		CC_BREAK_IF(!LayerExt::init());
		load("ini/slotmachine/layer.xml");

		setDisapparanceOnBackButton();
		buildMachine();

		_rewardNode = getNodeByPath<Text>(this, "reward/value");

		runEvent("show");

		return true;
	} while (false);
	return false;
}

ccMenuCallback SlotMachineLayer::get_callback_by_description(const std::string & name)
{
	if (name == "spin")
		return std::bind(&SlotMachineLayer::spin, this);
	else if (name == "get")
		return std::bind(&SlotMachineLayer::getAward, this);
	else if (name == "close")
		return std::bind(&SlotMachineLayer::disappearance, this);
	else if (name.find("stake") == 0)
	{
		auto value = name.substr(strlen("stake"));
		return std::bind(&SlotMachineLayer::setStake, this, strToInt(value));
	}
	else
		return LayerExt::get_callback_by_description(name);
}

bool SlotMachineLayer::loadXmlEntity(const std::string & tag, pugi::xml_node & xmlnode)
{
	if (tag == "slotmachine")
	{
		_reelCount = xmlnode.attribute("reel_count").as_int();
		_sectionHeight = xmlnode.attribute("section_height").as_float();		
		_sectionCount = xmlnode.attribute("section_count").as_int();
		_reelHeight = _sectionHeight * _sectionCount;
		_velocity = xmlnode.attribute("velocity").as_float();
		_sevenId = xmlnode.attribute("seven_id").as_int();
		for (auto xmlCombination : xmlnode.child("combinations"))
		{
			int multiplier = xmlCombination.attribute("multiplier").as_int();
			std::string value = xmlCombination.attribute("value").as_string();
			_winCombinations[value] = multiplier;
		}

	}
	else
		return LayerExt::loadXmlEntity(tag, xmlnode);
	return true;
}

void SlotMachineLayer::buildMachine()
{
	for (int j = 0; j < _reelCount; j++)
	{
		buildReel(j);
		_currentCombination.push_back(1);
	}
}

void SlotMachineLayer::buildReel(int index)
{
	float pos = 0.f;
	for (int i = 0; i < 2; i++)
	{
		auto node = Node::create();
		node->setName(toStr(i));
		node->setPositionY(_reelHeight * i);
		node->addChild(xmlLoader::load_node("ini/slotmachine/reel.xml"));

		getNodeByPath<Node>(this, "slotmachine/clip/reel_" + toStr(index))->addChild(node);
		_upsideDown.push_back(false);
	}

}

void SlotMachineLayer::spin()
{
	if (!_stakeDone)
		return;
	_rewardNode->setString("");

	ScoreCounter::shared().subMoney(kScoreCrystals, _stake, true, "slotmachine");
	_stakeDone = false;
	_locked = true;
	runEvent("unpressbuttons");

	scheduleUpdate();
	generateCombination();

	for (int i = 0; i < _reelCount; i++)
	{
		float dy = _reelHeight * _resultCombination[i] / _sectionCount;
		float duration = abs(dy / _velocity);

		xmlLoader::macros::set("duration", toStr(duration));
		xmlLoader::macros::set("dy", toStr(-dy));
		auto action0 = xmlLoader::load_action_from_file("ini/slotmachine/spin_action.xml");
		auto action1 = action0->clone();
		xmlLoader::macros::erase("duration");
		xmlLoader::macros::erase("dy");

		std::string parent = "slotmachine/clip/reel_" + toStr(i);
		getNodeByPath<Node>(this, parent + "/0")->runAction(action0);
		getNodeByPath<Node>(this, parent + "/1")->runAction(action1);

		if (i == _reelCount - 1)
		{
			auto call = Sequence::createWithTwoActions(
				DelayTime::create(duration),
				CallFunc::create([this]{
				AudioEngine::shared().playEffect(kSoundTada);
				this->stop();
			})
				);
			runAction(call);

			int const ticksCount = _resultCombination[i] - _currentCombination[i];
			int const ticksCountHalf = ticksCount / 2;
			float const dt = duration / ticksCountHalf;
			Vector<FiniteTimeAction *> ticks;
			for (int i = 0; i < ticksCountHalf; ++i)
			{
				ticks.pushBack(DelayTime::create(dt));
				ticks.pushBack(CallFunc::create([]{ AudioEngine::shared().playEffect(kSoundGameWaveIcon); }));
			}
			runAction(Sequence::create(ticks));
		}

	}

	this->runEvent("onspin");
}

void SlotMachineLayer::generateCombination()
{
	for (int i = 0; i < _reelCount; ++i)
	{
		_resultCombination.push_back(20 + rand() % 20 + rand() % _sectionCount);
		std::sort(_resultCombination.begin(), _resultCombination.end());
	}
};

void SlotMachineLayer::disappearance()
{
	if (_locked)
		return;
	if (this->runEvent("hide") == false)
		runAction( CallFunc::create( [this](){removeFromParent(); } ) );
}

void SlotMachineLayer::stop()
{
	for (size_t i = 0; i < _currentCombination.size(); i++)
	{
		_currentCombination[i] = (_currentCombination[i] + _resultCombination[i]) % _sectionCount;
		_resultCombination.empty();
	}
	getAward();

	unscheduleUpdate();
	_locked = false;
	runEvent("onstop");
}

//TODO: hardcoded award.
void SlotMachineLayer::getAward()
{
	std::vector<int> a(_currentCombination.begin(), _currentCombination.end());
	std::sort(a.begin(), a.end());
	
	int multiplier = 0;

	//3 matched symbols
	if (a[0] == a[1] && a[1] == a[2])
	{
		if (a[0] == _sevenId)
			multiplier = _winCombinations["777"];
		else 
			multiplier = _winCombinations["xxx"];		
	} 
	//2 matched symbols
	else if (a[0] == a[1] || a[1] == a[2])
	{
		if (a[1] == _sevenId)
			multiplier = _winCombinations["77x"];
		else
			multiplier = _winCombinations["xx"];
	}

	int award = _stake * multiplier;
	_rewardNode->setString(toStr(award));
	ScoreCounter::shared().addMoney(kScoreCrystals, award, true);

	_stake = 0;
}

void SlotMachineLayer::update(float dt)
{
	for (int i = 0; i < _reelCount; i++)
	{
		int index = _upsideDown[i] ? 1 : 0;
		std::string parent = "slotmachine/clip/reel_" + toStr(i) + "/";
		auto reelNode = getNodeByPath<Node>(this, parent + toStr(index));
		auto pos = reelNode->getPositionY();
		if (pos <= -_reelHeight)
		{
			_upsideDown[i] = !_upsideDown[i];
			reelNode->setPositionY(pos + _reelHeight * 2);
		}
	}
}

void SlotMachineLayer::setStake(int value)
{
	if (_locked)
		return;

	if (ScoreCounter::shared().getMoney(kScoreCrystals) < value)
	{
		SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
		if (scene)
		{
			MapLayer* map = dynamic_cast<MapLayer*>(scene->getMainLayer().ptr());
			if (map)
				map->cb_shop(nullptr, 1, 1);
			else
			{
				auto gamescene = dynamic_cast<GameScene*>(scene);
				if( gamescene )
					gamescene->openShop( nullptr, false );
			}
		}
	}
	else
	{
		_stake = value;
		_stakeDone = true;
		runEvent("unpressbuttons");
		runEvent("onstake" + toStr(value));
	}
}

NS_CC_END
