#ifndef __HeroesSquadNode_h__
#define __HeroesSquadNode_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN





class HeroesSquadNode : public Node, public NodeExt
{
	DECLARE_BUILDER( HeroesSquadNode );
	bool init();
public:
	virtual void onEnter()override;
	virtual void onExit()override;
	virtual ccMenuCallback get_callback_by_description( const std::string & name ) override;
	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;
protected:
	void heroroom( int slot );
	void video( int slot );
	void videoResult( int slot, bool result );
	void onChangeHeroes();
	void onChangeHeroExp();
private:
	bool _showEXP;
};




NS_CC_END
#endif // #ifndef HeroesSquadNode