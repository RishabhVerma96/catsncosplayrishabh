//
//  SceneLoader.h
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 2016.02.19
//
//
#ifndef __DialogLayer_h__
#define __DialogLayer_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN

class DialogLayer : public LayerExt, public LayerBlur
{
	DECLARE_BUILDER( DialogLayer );
	static DialogLayer::Pointer showForShop( const std::string& pathToXml, int shopIndex, int shopTab );
	static DialogLayer::Pointer createAndRun( const std::string& pathToXml, const std::function<void( bool )>& callback );
	bool init( const std::string& pathToXml, const std::function<void( bool )>& callback );
public:
	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override;
	virtual bool isBlurActive() override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;
protected:
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	void cb_answer( bool answer );
private:
	std::function<void( bool )> _callback;
};

NS_CC_END
#endif // #ifndef DialogLayer