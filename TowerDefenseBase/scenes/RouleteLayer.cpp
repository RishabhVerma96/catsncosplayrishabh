#include "ml/loadxml/xmlProperties.h"
#include "ml/Audio/AudioEngine.h"
#include "ml/ObjectFactory.h"
#include "ml/SmartScene.h"
#include "plugins/AdsPlugin.h"
#include "RouleteLayer.h"
#include "resources.h"
#include "support.h"
#include "ScoreCounter.h"
#include "UserData.h"
#include "DialogLayer.h"
#include "ShopLayer2.h"

NS_CC_BEGIN


/****************************************************************************/
//MARK:	RouleteLayer
/****************************************************************************/
RouleteActivatorNode::RouleteActivatorNode()
: _activateRoulette(false)
{
	ScoreCounter::shared().observer( kScoreTicket ).add( _ID, std::bind( &RouleteActivatorNode::onChangeTicketsCount, this, std::placeholders::_1 ) );
	onChangeTicketsCount( ScoreCounter::shared().getMoney( kScoreTicket ) );
}

RouleteActivatorNode::~RouleteActivatorNode()
{
	ScoreCounter::shared().observer( kScoreTicket ).remove( _ID );
}

bool RouleteActivatorNode::init()
{
	do
	{
		CC_BREAK_IF( !Node::init() );
		CC_BREAK_IF( !NodeExt::init() );

		return true;
	}
	while( false );
	return false;
}

void RouleteActivatorNode::onEnter()
{
	Node::onEnter();
	runAction( CallFunc::create( [this](){
		if( _activateRoulette )
		{
			_activateRoulette = false;
			auto layer = RouleteLayer::create();
			SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
			if( scene && layer )
				scene->pushLayer( layer, true );
		}
	} ) );
}

void RouleteActivatorNode::onExit()
{
	Node::onExit();
}

void RouleteActivatorNode::onChangeTicketsCount( int count )
{
	if( count >= 5 )
	{
		_activateRoulette = true;
	}
	if( isTestDevice() && isTestModeActive() )
		_activateRoulette = false;
}


/****************************************************************************/
//MARK:	RouleteLayer
/****************************************************************************/
RouleteLayer::RouleteLayer()
: _locked(false)
, _angularVelocity(1.f)
, _angularDumping(0.1f)
, _wheelRadius(0)
, _timeToDumping(1)
, _awardIndex(-1)
, _useDialog(false)
{}

RouleteLayer::~RouleteLayer()
{}

bool RouleteLayer::init()
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		load( "ini/roulete/layer.xml" );

		EventListenerKeyboard * event = EventListenerKeyboard::create();
		event->onKeyReleased = std::bind( [this]( EventKeyboard::KeyCode key, Event* )
		{
			if( key == EventKeyboard::KeyCode::KEY_BACK )
			{
				this->disappearance();
			}
		}, std::placeholders::_1, std::placeholders::_2 );
		getEventDispatcher()->addEventListenerWithSceneGraphPriority( event, this );

		buildWheel();

		runEvent( "onenter" );

		return true;
	}
	while( false );
	return false;
}

bool RouleteLayer::setProperty( const std::string & stringproperty, const std::string & value )
{
	if( stringproperty == "usedialog" )
		_useDialog = strTo<bool>( value ) && Config::shared().get<bool>( "useDialogs" );
	else return LayerExt::setProperty( stringproperty, value );
	return true;
}

ccMenuCallback RouleteLayer::get_callback_by_description( const std::string & name )
{
	if( name == "spin" )
		return std::bind( &RouleteLayer::spin, this );
	else if( name == "get" )
		return std::bind( &RouleteLayer::getAward, this );
	else if( name == "not_enought_ok" )
		return std::bind( &RouleteLayer::closeNotEnought, this );
	else if( name == "close" )
		return std::bind( &RouleteLayer::disappearance, this );
	else
		return LayerExt::get_callback_by_description( name );
}

bool RouleteLayer::loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )
{
	if( tag == "wheel" )
	{
		_wheelRadius = xmlnode.attribute( "radius" ).as_float();
		_angularVelocity = xmlnode.attribute( "velocity" ).as_float();
		_angularDumping = xmlnode.attribute( "damping" ).as_float();
		for( auto xmlAward : xmlnode.child( "awards" ) )
		{
			std::string type = xmlAward.attribute( "type" ).as_string();
			auto award = Factory::shared().build<Award>( type );
			award->load( xmlAward );
			_awards.push_back( award );
			_probabilities.push_back( xmlAward.attribute( "probability" ).as_int() );
		}
	}
	else
		return LayerExt::loadXmlEntity( tag, xmlnode );
	return true;
}

void RouleteLayer::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
}

bool RouleteLayer::isBlurActive()
{
	return !isRunning();
}

void RouleteLayer::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

void RouleteLayer::disappearance()
{
	if( _locked )
		return;
	if( this->runEvent( "onexit" ) == false )
		removeFromParent();
}

void RouleteLayer::spin()
{
	if( ScoreCounter::shared().getMoney( kScoreTicket ) < 5 )
	{
		if( _useDialog )
			showNotEnoughtDialog();
		else
			showNotEnoughtMessage();
		return;
	}
	ScoreCounter::shared().subMoney( kScoreTicket, 5, true, "roulete" );
	scheduleUpdate();
	_locked = true;
	generateAward();
	
	float const degPerSec = 180.0f;
	int const step = 45;
	int const angle = _awardIndex * step;
	float const angleTime = angle / degPerSec;
	float const loopTime = 360.0f / degPerSec;

	xmlLoader::macros::set( "angle", toStr( angle ) );
	xmlLoader::macros::set( "angleTime", toStr( angleTime ) );
	xmlLoader::macros::set( "loopTime", toStr( loopTime ) );
	auto action = xmlLoader::load_action_from_file( "ini/roulete/spin_action.xml" );
	xmlLoader::macros::erase( "angle" );
	xmlLoader::macros::erase( "angleTime" );
	xmlLoader::macros::erase( "loopTime" );
	getNodeByPath<Node>( this, "wheel" )->runAction( action );
	float duration = static_cast<FiniteTimeAction*>(action.ptr())->getDuration();
	auto call = Sequence::createWithTwoActions(
		DelayTime::create( duration ),
		CallFunc::create( [this]{
			AudioEngine::shared().playEffect( kSoundTada );
			this->stop();
		} ) 
	);
	runAction( call );

	int const ticksCount = int( (duration * degPerSec) / step - 2.0f);
	int const ticksCountHalf = ticksCount / 2;
	float const averageTickDuration = duration / ticksCount;
	Vector<FiniteTimeAction *> ticks;
	for( int i = 0; i < ticksCount; ++i )
	{
		float const dt = ( 0.5f + 1.4f * pow( fabsf( i - ticksCountHalf ) / ticksCountHalf, 3.0f ) ) * averageTickDuration;
		ticks.pushBack( DelayTime::create( dt ) );
		ticks.pushBack( CallFunc::create( []{ AudioEngine::shared().playEffect( kSoundRouletteTick ); } ) );
	}
	runAction( Sequence::create( ticks ) );
	
	runEvent( "onspin" );
}

void RouleteLayer::stop()
{
	unscheduleUpdate();
	_locked = false;
	runEvent( "onstop" );
	showCongratulationMessage();
}

void RouleteLayer::showCongratulationMessage()
{
	auto award = _awards[_awardIndex];

	std::string path;
	if( award->type() == AwardType::score ) path = "ini/roulete/congratulation_award_crystals.xml";
	if( award->type() == AwardType::heroPoints ) path = "ini/roulete/congratulation_award_heropoint.xml";
	if( award->type() == AwardType::towerUpgrade ) path = "ini/roulete/congratulation_award_towerpoint.xml";
	xmlLoader::macros::set( "path_to_award_icon", path );
	if( award->type() == AwardType::score )
	{
		AwardScore* score = (AwardScore*)award.ptr();
		xmlLoader::macros::set( "score", score->getScoreName() );
		xmlLoader::macros::set( "count", toStr( score->getCount() ) );
	}
	if( award->type() == AwardType::towerUpgrade )
	{
		AwardTowerUpgrade* score = (AwardTowerUpgrade*)award.ptr();
		xmlLoader::macros::set( "towername", score->getTowerName() );
	}

	xmlLoader::bookDirectory( this );
	_congratulation = xmlLoader::load_node<LayerExt>( "ini/roulete/congratulation.xml" );
	xmlLoader::unbookDirectory( this );

	auto scene = dynamic_cast<SmartScene*>(getScene());
	if( scene && _congratulation )
	{
		scene->pushLayer( _congratulation, true );
		_congratulation->runEvent( "onenter" );
	}
}

void RouleteLayer::showNotEnoughtMessage()
{
	xmlLoader::bookDirectory( this );
	_notEnought = xmlLoader::load_node<LayerExt>( "ini/roulete/noenought.xml" );
	xmlLoader::unbookDirectory( this );

	auto scene = dynamic_cast<SmartScene*>(getScene());
	if( scene && _notEnought )
	{
		scene->pushLayer( _notEnought, true );
		_notEnought->runEvent( "onenter" );
	}
}

void RouleteLayer::showNotEnoughtDialog()
{
	DialogLayer::showForShop( "ini/dialogs/roulete.xml", 1, 2 );
}

void RouleteLayer::closeNotEnought()
{
	if( _notEnought )
		_notEnought->runEvent( "onexit" );
}

void RouleteLayer::getAward()
{
	auto award = _awards[_awardIndex];
	if( award )
		award->get();
	_awards[_awardIndex].reset( nullptr );
	UserData::shared().save();
	runEvent( "onrecv" );
	runEvent( "onexit" );
	if( _congratulation )
		_congratulation->runEvent( "onexit" );

	auto useAds = Config::shared().get<bool>( "useAds" );
	if( useAds )
		AdsPlugin::shared().showVideo();
}

void RouleteLayer::buildWheel()
{
	std::vector<Point>points;
	computePointsByRadius( points, _wheelRadius, _awards.size(), -90 );
	for( size_t i = 0; i < _awards.size(); ++i )
	{
		auto& award = _awards[i];
		std::string path;
		if( award->type() == AwardType::score ) path = "ini/roulete/award_crystals.xml";
		if( award->type() == AwardType::heroPoints ) path = "ini/roulete/award_heropoint.xml";
		if( award->type() == AwardType::towerUpgrade ) path = "ini/roulete/award_towerpoint.xml";

		if( award->type() == AwardType::score )
		{
			AwardScore* score = (AwardScore*)award.ptr();
			xmlLoader::macros::set( "score", score->getScoreName() );
			xmlLoader::macros::set( "count", toStr( score->getCount() ) );
		}
		if( award->type() == AwardType::towerUpgrade )
		{
			AwardTowerUpgrade* score = (AwardTowerUpgrade*)award.ptr();
			xmlLoader::macros::set( "towername", score->getTowerName() );
		}
		auto icon = xmlLoader::load_node( path );

		auto pos = points[i];
		auto angle = -360.f / _awards.size() * i;
		getNodeByPath<Node>( this, "wheel" )->addChild( icon );
		icon->setPosition( pos );
		icon->setRotation( angle );

		//auto label = LabelTTF::create( toStr( i ), "Arial", 20 );
		//getNodeByPath<Node>( this, "wheel" )->addChild( label );
		//label->setPosition( pos / 2 );
		//label->setRotation( angle );
	}
}

void RouleteLayer::generateAward()
{
	std::vector<size_t> indexes;
	size_t index( 0 );
	for( auto probability : _probabilities )
	{
		for( int i = 0; i < probability; ++i )
		{
			indexes.push_back( index );
		}
		++index;
	}
	index = rand() % indexes.size();
	_awardIndex = indexes[index];
}

void RouleteLayer::update( float dt )
{
}

NS_CC_END
