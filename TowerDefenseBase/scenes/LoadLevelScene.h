#ifndef __LoadLevelScene_h__
#define __LoadLevelScene_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "ml/SmartScene.h"
#include "game/gameboard.h"
#include "LayerLoader.h"
NS_CC_BEGIN

class LoadLevelScene : public SmartScene, public NodeExt
{
	DECLARE_BUILDER( LoadLevelScene );
	bool init( int level, GameMode mode, bool sessionWithBot );
public:
	static LoadLevelScene* getInstance();
	static void parceLevel( std::set<std::string>&out, GameMode mode, int index );
	void loadInGameResources( const std::string& pack );
	
	virtual void onEnter()override;
protected:
	virtual bool loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )override;
	void checkContent();
	void createLoading();
	void loadResources();
	
	void onLoadingFinished();
	void generateBotInfo();
private:
	int _levelIndex;
	GameMode _levelMode;
	bool _popSceneOnEnter;
	bool _runSessionWithBot;
	IntrusivePtr<LayerLoader> _loader;

	std::set<std::string> _units;
	std::map<std::string, std::vector< std::pair<std::string, std::string> > >_resourcePacks;
};




NS_CC_END
#endif // #ifndef LoadLevelScene
