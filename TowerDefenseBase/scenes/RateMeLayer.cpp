//
//  RateMeLayer.cpp
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 12.12.14.
//
//

#include "RateMeLayer.h"
#include "support.h"
#include "configuration.h"
#include "ScoreCounter.h"
#include "UserData.h"
#include "ml/JniBind.h"

NS_CC_BEGIN;

void cb_open_application_store();
void rateMe();


RateMeLayer::RateMeLayer()
{
}

RateMeLayer::~RateMeLayer()
{
}

bool RateMeLayer::init()
{
	do
	{
		CC_BREAK_IF(UserData::shared().get <bool>("award_for_rate"));
		NodeExt::load("ini/ratemelayer.xml");
		
		setKeyDispatcherBackButton(this);
		
		runEvent("onenter");
		
		return true;
	}
	while( false );
	return false;
}

ccMenuCallback RateMeLayer::get_callback_by_description( const std::string & name )
{
	if( name == "rateme" )
	{
		auto rate = [this](Ref*)mutable
		{
			runEvent("showaward");
			ScoreCounter::shared().addMoney(kScoreCrystals, 200, true);
			UserData::shared().write("award_for_rate", true);
            UserData::shared().save();

            runAction( Sequence::createWithTwoActions(DelayTime::create(0.1f), CallFunc::create( [this]{ cb_open_application_store(); } ) ) );
		};
		auto callback = std::bind( rate, std::placeholders::_1 );
		return callback;
	}
	else if( name == "close" )
	{
		auto close = [this](Ref*)mutable
		{
			runEvent("onexit");
		};
		auto callback = std::bind( close, std::placeholders::_1 );
		return callback;
	}
	return nullptr;
}


NS_CC_END;
