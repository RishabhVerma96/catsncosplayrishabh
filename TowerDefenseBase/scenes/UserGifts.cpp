#include "ml/Text.h"
#include "ml/ObjectFactory.h"
#include "UserGifts.h"
#include "UserData.h"
#include "configuration.h"
#include "Tutorial.h"

NS_CC_BEGIN

UserGifts::UserGifts()
{
}

UserGifts::~UserGifts()
{
}

bool UserGifts::init()
{
	do
	{
		CC_BREAK_IF( !Config::shared().get<bool>( "useUsersGift" ) );
		CC_BREAK_IF( !FileUtils::getInstance()->isFileExist( "ini/usersgift.xml" ) );

		CC_BREAK_IF( !LayerExt::init() );
		load( "ini/usersgift.xml" );

		auto answer = UserData::shared().get <int>( "gift_counter", 0 );
		CC_BREAK_IF( answer >= (int)_awards.size() );

		//TODO: Implement other award types.
		auto award = _awards[answer];
		CC_BREAK_IF( award->type() != AwardType::bonus );

		AwardBonus* bonus = (AwardBonus*)award.ptr();
		getNodeByPath( this, "gifts/" + toStr( bonus->getBonusIndex() ) )->setVisible( true );

		auto bonusCount = bonus->getCount();
		if (bonusCount > 1)
		{
			auto countNode = getNodeByPath<Text>( this, "gifts/count" );
			countNode->setVisible( true );
			countNode->setString( "x" + toStr( bonusCount ) );
		}

		return true;
	} while( false );
	return false;
}

ccMenuCallback UserGifts::get_callback_by_description( const std::string & name )
{
	if( name == "get_gift" )
	{
		return std::bind( [this]( Ref* ){
			auto answer = UserData::shared().get <int>( "gift_counter", 0 );
			auto award = _awards[answer % _awards.size()];
			if (award)
				award->get();
            ++answer;
            UserData::shared().write( "gift_counter", answer );
            UserData::shared().save();
            TutorialManager::shared().dispatch("gift_received");
			removeFromParent();
		}, std::placeholders::_1 );
	}	
	else return nullptr;
}

bool UserGifts::loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )
{
	if (tag == "awards")
	{
		for (auto xmlAward : xmlnode)
		{
			std::string type = xmlAward.attribute( "type" ).as_string();
			auto award = Factory::shared().build<Award>( type );
			award->load( xmlAward );
			_awards.push_back( award );
		}
	}
	else
		return LayerExt::loadXmlEntity( tag, xmlnode );
	return true;
}

NS_CC_END
