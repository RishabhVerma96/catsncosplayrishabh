#include "ml/Audio/AudioEngine.h"
#include "SettingsLayer.h"
#include "ScoreCounter.h"
#include "UserData.h"
#include "Config.h"

USING_NS_CC;

static std::string const facebookShareLinkPrefix = "https://www.facebook.com/sharer/sharer.php?u=";
static std::string const facebookShareActionName = "fb_share_store";

SettingsLayer::SettingsLayer() {}
SettingsLayer::~SettingsLayer() {}

bool SettingsLayer::init()
{
	do {
		CC_BREAK_IF( !LayerExt::init() );
		CC_BREAK_IF( !FileUtils::getInstance()->isFileExist( "ini/settings_ui.xml" ) );

		bool const soundEnabled = AudioEngine::shared().isMusicEnabled() || AudioEngine::shared().isSoundEnabled();

		xmlLoader::macros::set( "sound_enabled", toStr( soundEnabled ) );
		xmlLoader::macros::set( "sound_disabled", toStr( !soundEnabled ) );

		NodeExt::load( "ini/settings_ui.xml" );

		xmlLoader::macros::erase( "sound_enabled" );
		xmlLoader::macros::erase( "sound_disabled" );

		runEvent( "show" );

		return true;
	} while( false );
	return false;
}

ccMenuCallback SettingsLayer::get_callback_by_description( std::string const & name )
{
	if( name == "sound_on" )
		return [this, name]( Ref * ) {
			AudioEngine::shared().soundEnabled( true );
			AudioEngine::shared().musicEnabled( true );
			this->runEvent( name );
		};
	else if( name == "sound_off" )
		return [this, name]( Ref * ) {
			AudioEngine::shared().soundEnabled( false );
			AudioEngine::shared().musicEnabled( false );
			this->runEvent( name );
		};
	else if( name.find( facebookShareActionName ) == 0 )
		return[this, name]( Ref * ) {
			int const reward = strTo<int>( name.substr( facebookShareActionName.size() ) );

			Application::getInstance()->openURL( facebookShareLinkPrefix + Config::shared().get("linkToFacebookSharing") );

			this->runAction( Sequence::createWithTwoActions(
				DelayTime::create( 1.0f ),
				CallFunc::create( [reward] {
					bool const shared = UserData::shared().get(facebookShareActionName, false);
					if( !shared )
					{
						ScoreCounter::shared().addMoney( kScoreCrystals, reward, true );
						UserData::shared().write( facebookShareActionName, true );
						UserData::shared().save();
					}
				} )
			) );
		};
	else if( name.find( "mailto:" ) == 0 )
		return [name]( Ref * ){ Application::getInstance()->openURL( name ); };
	else
		return [this, name]( Ref * ) { this->runEvent( name ); };
}