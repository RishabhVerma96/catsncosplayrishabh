#ifndef __ShareLayer__
#define __ShareLayer__

#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"

NS_CC_BEGIN


class ShareLayer : public LayerExt
{
	DECLARE_BUILDER( ShareLayer );
	bool init( const std::string & network );
public:
	static const std::vector<std::string> networks;
protected:
	virtual ccMenuCallback get_callback_by_description( const std::string & name ) override;
private:
	std::string _network;
};

NS_CC_END

#endif