//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __MAINGS__
#define __MAINGS__
#include "cocos2d.h"
#include "ml/Audio/AudioMenu.h"
#include "ml/ScrollMenu.h"
#include "ml/NodeExt.h"
#include "ml/ParamCollection.h"
#include "ml/types.h"

NS_CC_BEGIN;

class MainGS : public LayerExt, public LayerBlur
{
	DECLARE_BUILDER( MainGS );
public:
	bool init();  
	static ScenePointer scene();
	
	virtual void onEnter();
	
	virtual void disappearance();

public:
	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;
	bool isLoadedResources()const;
protected:
	virtual bool isBlurActive() override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;

	virtual void load( pugi::xml_node & root )override;
	virtual ccMenuCallback get_callback_by_description( const std::string & name );

	void fadeIn();
	void fadeOut();
	void rateme();

	void pushGame(Ref* );
    void popUp(Ref* );
    void shareButtonPressed( Ref* );
    void leaderboardButtonPressed( Ref* );
    void restoreButtonPressed( Ref* );
	void loadResources( std::function<void( )> callback );
	void onResourcesDidLoaded( );
	void onResourcesDidLoaded_runMap( );
	void createDevMenu();
protected:
	struct loading
	{
		std::vector< std::pair<std::string, std::string> > atlases;
		std::vector< std::string > resources;
	}m_loadingList;
	MenuPointer m_menu;
	AudioMenu::Pointer m_menuAudio;
	bool m_resourceIsLoaded;
	LayerExt::Pointer _blockLayer;
    
};

NS_CC_END;

#endif
