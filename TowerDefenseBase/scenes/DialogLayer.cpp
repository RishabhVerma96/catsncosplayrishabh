//
//  SceneLoader.h
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 2016.02.19
//
//
#include "DialogLayer.h"
#include "ml/SmartScene.h"
#include "ShopLayer2.h"
NS_CC_BEGIN

DialogLayer::DialogLayer()
{
}

DialogLayer::~DialogLayer( )
{
}

DialogLayer::Pointer DialogLayer::showForShop( const std::string& pathToXml, int shopIndex, int shopTab )
{
	auto callback = []( bool answer, int index, int tab ){
		if( answer )
		{
			auto shop = ShopLayer2::create( index, tab );
			auto scene = dynamic_cast<SmartScene*>(Director::getInstance()->getRunningScene());
			assert( shop && scene );
			if( shop && scene )
				scene->pushLayer( shop, true );
		}
	};
	return DialogLayer::createAndRun( pathToXml, std::bind( callback, std::placeholders::_1, shopIndex, shopTab ) );
}

DialogLayer::Pointer DialogLayer::createAndRun( const std::string& pathToXml, const std::function<void( bool )>& callback )
{
	auto dialog = DialogLayer::create( pathToXml, callback );
	auto scene = dynamic_cast<SmartScene*>(Director::getInstance()->getRunningScene());
	assert( dialog && scene );
	if( dialog && scene )
		scene->pushLayer( dialog, true );
	return dialog;

}

bool DialogLayer::init( const std::string& pathToXml, const std::function<void( bool )>& callback )
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		_callback = callback;
		setDisapparanceOnBackButton();
		load( pathToXml );
		return true;
	}
	while( false );
	return false;
}

void DialogLayer::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
}

bool DialogLayer::isBlurActive()
{
	return !isRunning();
}

void DialogLayer::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

ccMenuCallback DialogLayer::get_callback_by_description( const std::string & name )
{
	if( name == "yes" )return std::bind( &DialogLayer::cb_answer, this, true );
	if( name == "no" )return std::bind( &DialogLayer::cb_answer, this, false );
	return NodeExt::get_callback_by_description( name );
}

void DialogLayer::cb_answer( bool answer )
{
	retain();
	if( answer )
		removeFromParent();
	else
		disappearance();
	if( _callback )
		_callback( answer );
	release();
}

NS_CC_END