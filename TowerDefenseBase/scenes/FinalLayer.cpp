#include "FinalLayer.h"
#include "configuration.h"
#include "SmartScene.h"
#include "EulaLayer.h"
#include "ml/Audio/AudioEngine.h"
#include "resources.h"
#include "UserData.h"
#include "consts.h"
#include "ScoreLayer.h"
NS_CC_BEGIN

const std::string loadPath = "ini/finallayer/layer.xml";

FinalLayer::FinalLayer()
{}

FinalLayer::~FinalLayer()
{}

bool FinalLayer::init()
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		CC_BREAK_IF( !FileUtils::getInstance()->isFileExist( loadPath ) );
//        CC_BREAK_IF( UserData::shared().get<bool>( k::user::FinalMovieShown, false ) );

		load( loadPath );
		removeScoreLayer();
		UserData::shared().write( k::user::FinalMovieShown, true );
		AudioEngine::shared().stopAllEffects();
		AudioEngine::shared().playMusic( kMusicMap );
		setKeyboardEnabled( true );

		return true;
	}
	while( false );
	return false;
}

void FinalLayer::removeScoreLayer()
{
	auto scene = Director::getInstance()->getRunningScene();
	auto scores = scene->getChildByName( "scores" );
	if( scores )
	{
		scores->setVisible( false );
	}
}

void FinalLayer::onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event )
{
	if( keyCode == EventKeyboard::KeyCode::KEY_SPACE )
	{
		auto layer = FinalLayer::create();
		auto scene = dynamic_cast<SmartScene*>(getScene());
		scene->pushLayer( layer, true );
	}
	if( keyCode == EventKeyboard::KeyCode::KEY_ESCAPE )
	{
		UserData::shared().write( k::user::FinalMovieShown, true );
		Director::getInstance()->popScene();
	}
}

ccMenuCallback FinalLayer::get_callback_by_description( const std::string & name )
{
	if( name == "quit" )
	{
		return []( Ref* ) {
			UserData::shared().save();
			Director::getInstance()->popScene();
		};
	}
	else
		return LayerExt::get_callback_by_description( name );
	return nullptr;
}

NS_CC_END
