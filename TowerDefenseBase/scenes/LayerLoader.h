//
//  SceneLoader.h
//  JungleDefense
//
//  Created by Vladimir Tolmachev on 26.05.14.
//
//

#ifndef __JungleDefense__LayerLoader__
#define __JungleDefense__LayerLoader__
#include "cocos2d.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN;


class LayerLoader : public Layer, public NodeExt
{
	LayerLoader();
	virtual ~LayerLoader();
public:
	static LayerLoader * create(const std::vector<std::string> & resources, const std::function<void()> & callback, bool multiplpayerLoadingReporting = false );
	
	void addPlists(const std::vector< std::pair<std::string, std::string> > & resources);
    void loadCurrentTexture();
	void setMinimalLoadingDuration( float seconds ) { _duration = seconds; }
private:
	void update( float dt );
	void start();
	void progress(Texture2D * texture, const std::string & resourcename);
	float getLoadingPercent()const;

	void checkFinishLoading();
	void checkLoadedPlist( const std::string & resourcename );

	void on_load_textures();
	void on_finished_loading();
private:
    std::vector< std::pair<std::string, std::string> > _resources;
	std::vector< std::pair<std::string, std::string> > _plists;
	unsigned _progress;
	float _duration;
	float _timer;
	float _multiplayerLoadingReportTimer;
	bool _multiplayerLoadingReporting;
	std::function<void()> _callback;
	Sprite* _barBG;
	ProgressTimer * _barTimer;
	bool _callbacked = false;
	float _progressTimer;
};


NS_CC_END;
#endif /* defined(__JungleDefense__LayerLoader__) */
