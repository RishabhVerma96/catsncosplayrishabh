//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 08.31.2015.
//
//
#include "EulaLayer.h"
#include "UserData.h"
#include "configuration.h"
NS_CC_BEGIN

EulaLayer::EulaLayer()
{
}

EulaLayer::~EulaLayer( )
{
}

bool EulaLayer::init()
{
	do
	{
		CC_BREAK_IF(!Config::shared().get<bool>("useEula"));
		auto answer = UserData::shared().get <bool>("eula_accept");
		CC_BREAK_IF(answer);

		CC_BREAK_IF( !LayerExt::init() );
		load( "ini/eula.xml" );
		return true;
	}
	while( false );
	return false;
}

ccMenuCallback EulaLayer::get_callback_by_description( const std::string & name )
{
	if( name == "yes" )
	{
		return std::bind( [this]( Ref* ){ 
			UserData::shared().write( "eula_accept", true );
			UserData::shared().save();
			removeFromParent(); 
		}, std::placeholders::_1 );
	}
	else if( name == "no" )
	{
		return std::bind( []( Ref* ){ 
			Director::getInstance()->end();
		}, std::placeholders::_1 );
	}
	else return nullptr;
}

NS_CC_END