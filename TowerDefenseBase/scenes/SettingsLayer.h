#pragma once

#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"

NS_CC_BEGIN

class SettingsLayer : public LayerExt
{
public:
	DECLARE_BUILDER( SettingsLayer );
	bool init();

	virtual ccMenuCallback get_callback_by_description( std::string const & name ) override;
};

NS_CC_END