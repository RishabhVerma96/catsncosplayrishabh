#include "AboutLayer.h"

USING_NS_CC;

AboutLayer::AboutLayer() {}
AboutLayer::~AboutLayer() {}

bool AboutLayer::init()
{
	do {
		CC_BREAK_IF( !LayerExt::init() );
		CC_BREAK_IF( !FileUtils::getInstance()->isFileExist( "ini/about.xml" ) );
		
		NodeExt::load( "ini/about.xml" );

		runEvent( "show" );

		return true;
	}
	while( false );
	return false;
}

ccMenuCallback AboutLayer::get_callback_by_description( std::string const & name )
{
	if( name.find( "://" ) != std::string::npos ) // cause of "http://url", "market://...", etc.
		return [name]( Ref * ){ Application::getInstance()->openURL( name ); };
	else
		return [this, name]( Ref * ){ this->runEvent( name ); };
}