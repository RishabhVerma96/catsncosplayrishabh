#ifndef PopShop_h
#define PopShop_h
#include "cocos2d.h"
//#include "macroses.h"
#include "ml/NodeExt.h"
#include "ml/ScrollMenu.h"
NS_CC_BEGIN

class PopShop : public ScrollMenu, public NodeExt
{
    DECLARE_BUILDER( PopShop );
    bool init();
protected:
    MenuItemPointer buildItem(const std::string & itemname);
    void fadeenter();
    void fadeexit();
    void cb_buy( Ref*, const std::string & itemname );
    void cb_close( Ref* );
    
private:
    float _scaleFactor;
    Point _zeroPosition;
    std::vector<int> candyCount;
    std::vector<std::string> candyName;
};
NS_CC_END
#endif /* PopShop_h */
