#include "PopShop.h"
//#include "consts.h"
//#include "resources.h"
#include "ml/Audio/AudioEngine.h"
#include "ml/Language.h"
//#include "ml/loadxml/xmlProperties.h"
//#include "ml/SmartScene.h"
//#include "MapLayer.h"
//#include "GameScene.h"

NS_CC_BEGIN

PopShop::PopShop()
: _scaleFactor( 1 )
, _zeroPosition()
{}

PopShop::~PopShop()
{}

bool PopShop::init()
{
    do
    {
        CCLOG("\n In the popshop.cpp \n");

        auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
        
        CC_BREAK_IF( !ScrollMenu::init() );
        CC_BREAK_IF( !NodeExt::init() );
      
        NodeExt::load( "ini/popshop/popshop.xml" );
        
        auto bg = getChildByName( "bg" );
        if( bg )
        {
            auto size = bg->getContentSize();
            size.width = std::min( dessize.width, size.width );
            size.height += bg->getPositionY();
            float sx = dessize.width / size.width;
            _scaleFactor = sx;
            bg->setScale( _scaleFactor );
            
            _zeroPosition.x = dessize.width / 2;
            _zeroPosition.y = size.height / 2 * _scaleFactor - 10;
            setPosition( _zeroPosition );
        }
        Size grid;
        Size content;
        candyName = {"Cotton Candy" , "Christmas Candy", "Confectionery", "Gumball", "Halloween Candy", "Umbrella Candy"};
        for( int i = 6; i > 0; i-- )
        {
            candyCount.push_back(0);
            auto itemname = candyName[i-1];
            auto item = buildItem( itemname );
            addItem( item );
            
            item->setScale( _scaleFactor );
            grid = item->getContentSize();
            grid.width *= _scaleFactor;
            grid.height *= _scaleFactor;
            content.width += grid.width;
            content.height = grid.height;
        }

        setAlignedStartPosition( Point( -_zeroPosition.x, -275 ) * _scaleFactor );
        setGrisSize( grid );
        align( 99 );
        
        Size scissor( dessize.width, 464 );
        setScissorRect( getAlignedStartPosition(), scissor * _scaleFactor );
        setScissorEnabled( true );
        setScrollEnabled( true );
        setContentSize( content );
        setAllowScrollByY( false );
        setMouseScrollEnabled(false);
        setMouseScrollSpeed( 50 );
        
        if( content.width < scissor.width )
        {
            float diff = scissor.width - content.width;
            Point point = getAlignedStartPosition();
            point.x += diff / 2;
            setAlignedStartPosition( point );
            align( 99 );
        }
        
        auto menu = bg->getChildByName( "menu" );
        if( menu )
        {
            auto close = menu->getChildByName<MenuItem*>( "close" );
            close->setCallback( std::bind( &PopShop::cb_close, this, std::placeholders::_1 ) );
            if( close->getPosition().equals( Point::ZERO ) )
            {
                close->setScale( _scaleFactor );
                Point pos = close->getPosition();
                pos.y *= _scaleFactor;
                pos.x = dessize.width / 2 - 35;
                close->setPosition( pos );
            }
        }
        
        fadeenter();
        return true;
    }
    while( false );
    return false;
}

void PopShop::cb_close(cocos2d::Ref *)
{
    fadeexit();
}

void PopShop::fadeenter()
{
    if( runEvent( "appearance" ) == false )
    {
        static auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
        setPosition( _zeroPosition + Point( 0, -dessize.height ) );
        auto action = EaseBackOut::create( MoveTo::create( 0.5f, _zeroPosition ) );
        runAction( action );
    }
}

void PopShop::fadeexit()
{
    
    if( runEvent( "disappearance" ) == false )
    {
        static auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
        auto action = EaseBackIn::create( MoveTo::create( 0.5f, _zeroPosition + Point( 0, -dessize.height ) ) );
        runAction( Sequence::create( action, RemoveSelf::create(), nullptr ) );
    }
}

MenuItemPointer PopShop::buildItem(const std::string & itemname)
{
    MenuItemPointer item = xmlLoader::load_node<MenuItem>("ini/popshop/popshopitem.xml");
    item->setName(itemname);
    item->setCallback(std::bind(&PopShop::cb_buy, this, std::placeholders::_1, itemname));
    
    auto conteiner = item->getChildByName("conteiner");
    auto name = conteiner->getChildByName <Text*>("name");
    auto nodeMain = conteiner->getChildByName("main");
    auto icon = nodeMain ? nodeMain->getChildByName <Sprite*>("icon") : nullptr;
    auto buyButton = getNodeByPath <MenuItem>(nodeMain, "menu/buy");
    if (name)
    {
        std::string textid = itemname;
        name->setString(WORD(textid));
    }
    if (icon)
    {
        std::string image = "images/popshop/" + itemname + ".png";
        xmlLoader::setProperty(icon, xmlLoader::kImage, image);
    }
    if (buyButton)
    {
        buyButton->setCallback(std::bind(&PopShop::cb_buy, this, std::placeholders::_1, itemname));
    }
    return item;
}

void PopShop::cb_buy( Ref*, const std::string & itemname )
{
    int candyCountLocation;
    auto item = getItemByName( itemname );
    if( !item )
        return;
    CCLOG("You buy: %s ", &itemname);
    
    for(int i = 0; i < candyName.size(); i++)
    {
        if(itemname == candyName[i])
        {
            candyCount[i]++;
            candyCountLocation = i;
        }
    }
    CCLOG("Total %s = %d ",&itemname,candyCount[candyCountLocation]);
}

NS_CC_END
