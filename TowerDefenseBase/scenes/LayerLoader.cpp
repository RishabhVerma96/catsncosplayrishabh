//
//  SceneLoader.cpp
//  JungleDefense
//
//  Created by Vladimir Tolmachev on 26.05.14.
//
//

#include "LayerLoader.h"
#include "resources.h"
#include "support.h"
#include "ImageManager.h"
#include "online/OnlineConnector.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include <android/log.h>
#define  LOG_TAG    "LayerLoader"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#else
#define LOGD(...) cocos2d::log(__VA_ARGS__)
#endif
NS_CC_BEGIN

namespace {
	float const multiplayerLoadingReportPeriod = 0.5f;
}

LayerLoader * LayerLoader::create( const std::vector<std::string> & resources, const std::function<void()> & callback, bool multiplpayerLoadingReporting )
{
	auto layer = new LayerLoader;
    for( auto res : resources )
        layer->_resources.emplace_back(res, res);
	layer->_callback = callback;
	layer->_multiplayerLoadingReporting = multiplpayerLoadingReporting;
	//CCLOG( "LayerLoader: layer->start()" );
	layer->start();
	layer->autorelease();
	return layer;
}

LayerLoader::LayerLoader()
: _resources()
, _progress(0)
, _callback()
, _barBG( nullptr )
, _barTimer( nullptr )
, _duration( -1.f )
, _timer( 0.f )
, _multiplayerLoadingReportTimer( 0.f )
, _multiplayerLoadingReporting( false )
, _progressTimer( 0.f )
{
	auto desSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	setPosition( desSize.width / 2, desSize.height / 3 );
	
	_barBG = ImageManager::sprite( "images/maings/progbar1.png" );
	_barBG->setName( "bg" );
	_barTimer = ProgressTimer::create( ImageManager::sprite( "images/maings/progbar2.png" ) );
	_barTimer->setName( "timer" );
	_barTimer->setBarChangeRate( Point( 1, 0 ) );
	_barTimer->setMidpoint( Point( 0, 0.5f ) );
	_barTimer->setType( ProgressTimer::Type::BAR );
	Node* node = Node::create();
	node->setName( "barnode" );
	node->addChild( _barBG );
	node->addChild( _barTimer, 1 );
	addChild( node );
	node->setPosition( strTo<Point>( "frame:0x-0.1" ) );

	load( "ini/loadprogress.xml" );
}

LayerLoader::~LayerLoader()
{}

void LayerLoader::addPlists( const std::vector< std::pair<std::string, std::string> > & resources )
{
	_plists.insert( _plists.end(), resources.begin(), resources.end() );
	for( auto plist : resources )
	{
		if( plist.second.empty() ) plist.second = plist.first;
		auto k = plist.first.find( ".plist" );
		if( k == plist.first.size( ) - 6 )
		{
			std::string texture = plist.first.substr( 0, k ) + ".png";
			_resources.emplace_back(plist.second, texture);
			//textureCache->addImageAsync( texture, std::bind( &LayerLoader::progress, this, std::placeholders::_1, plist.second ) );
		}
	}
}

void LayerLoader::update( float dt )
{
	_timer += dt;
	checkFinishLoading();
	float percent = getLoadingPercent();
	_barTimer->setPercentage( percent );

	if( _multiplayerLoadingReporting )
	{
		_multiplayerLoadingReportTimer += dt;
		if( _multiplayerLoadingReportTimer > multiplayerLoadingReportPeriod )
		{
			_multiplayerLoadingReportTimer -= multiplayerLoadingReportPeriod;
			OnlineConnector::shared().loadingReport( std::min<int>(100, percent) );
		}
	}

	_progressTimer += dt;
	if( _progressTimer > 1.f && _progress < _resources.size() )
	{
		_progressTimer = 0.f;
		auto textureCache = Director::getInstance()->getTextureCache();
		auto resource = _resources[_progress];
		auto texture = textureCache->getTextureForKey( resource.second );
		textureCache->unbindImageAsync( resource.second );
		if ( texture == nullptr )
		{
			textureCache->addImageAsync( resource.second, std::bind( &LayerLoader::progress, this, std::placeholders::_1, resource.first ) );
			//CCLOG( "LayerLoader::update() callback did not called, image not loaded, name=%s", resource.second.c_str() );
		}
		else
		{
			progress( texture, resource.first );
			//CCLOG( "LayerLoader::update() callback did not called, image loaded, name=%s", resource.second.c_str() );
		}
	}
}

void LayerLoader::start()
{
	scheduleUpdate();
	_progress = 0;
	_progressTimer = 0.f;
	//auto textureCache = Director::getInstance()->getTextureCache();
	//for( auto resource : _resources )
	//{
	//	textureCache->addImageAsync(resource.second, std::bind( &LayerLoader::progress, this, std::placeholders::_1, resource.first) );
	//}
}

void LayerLoader::loadCurrentTexture()
{
	//CCLOG( "LayerLoader::loadCurrentTexture(), _progress = %d, _resources.size() = %d", _progress, _resources.size() );
    if( _progress >= _resources.size() )
        return;
    
    //Texture2D::setDefaultAlphaPixelFormat(Texture2D::PixelFormat::RGBA4444);
    auto resource = _resources[_progress];
    auto textureCache = Director::getInstance()->getTextureCache();
#if USE_CHEATS == 1
	if( FileUtils::getInstance()->isFileExist( resource.second ) == false )
	{
		MessageBox( resource.second.c_str(), "Cannot load" );
	}
#endif

    textureCache->addImageAsync( resource.second, std::bind( &LayerLoader::progress, this, std::placeholders::_1, resource.first) );
}

void LayerLoader::progress(Texture2D * texture, const std::string & resourcename)
{
	//CCLOG( "LayerLoader: progress(), _progress = %d, resourcename = %s", _progress, resourcename.c_str() );
	++_progress;
	_progressTimer = 0.f;
	retain();
	checkLoadedPlist( resourcename );
    
	loadCurrentTexture();
	checkFinishLoading();
	release();
}

void LayerLoader::checkFinishLoading()
{
	bool finished = _progress >= _resources.size();
	if( _duration > 0 )
	{
		finished = finished && _timer >= _duration;
		//CCLOG( "LayerLoader: checkFinishLoading(), _progress = %d, _resources.size() = %d", _progress, _resources.size() );
	}
	if( finished )
		on_load_textures();
}

float LayerLoader::getLoadingPercent()const
{
	float t = (_progress + 1) / (float)_resources.size();
#ifndef _DEBUG
	if( _duration > 0 )
	{
		float T = _timer / _duration;
		t = std::min( t, T );
	}
#endif
	return t * 100.f;
}

void LayerLoader::checkLoadedPlist( const std::string & resourcename )
{
	for( auto & plist : _plists )
	{
		if( plist.second == resourcename )
		{
			ImageManager::shared( ).load_plist( plist.first, plist.second );
		}
	}
}

void LayerLoader::on_load_textures()
{
	unscheduleUpdate();
	Texture2D::setDefaultAlphaPixelFormat( Texture2D::PixelFormat::RGBA8888 );
	on_finished_loading();
}

void LayerLoader::on_finished_loading()
{
	if( _callback && !_callbacked)
		_callback();

	_callbacked = true;
}

NS_CC_END;
