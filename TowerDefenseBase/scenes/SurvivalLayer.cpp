#include "playservices/playservices.h"
#include "shop/ShopLayer2.h"
#include "inapp/Purchase.h"
#include "LoadLevelScene.h"
#include "SurvivalLayer.h"
#include "configuration.h"
#include "ScoreCounter.h"
#include "SmartScene.h"
#include "UserData.h"
#include "Tutorial.h"
#include "MapLayer.h"
#include "GameLayer.h"

USING_NS_CC;

namespace
{
	std::string const xmlPathIcon = "images/survival/lvl_%d.png";
	std::string const xmlPathLevels = "ini/survival/levels.xml";
	std::string const xmlPathBaseUI = "ini/survival/base_ui.xml";
	std::string const xmlPathLevelUI = "ini/survival/level_panel_ui.xml";

	class LevelColumn : public NodeExt_
	{
		DECLARE_BUILDER( LevelColumn );
		bool init( SurvivalLayer::Level const & lvl, IntrusivePtr<SurvivalLayer> survivalLayer )
		{
			do
			{
				CC_BREAK_IF( !Node::init() );
				CC_BREAK_IF( !FileUtils::getInstance()->isFileExist( xmlPathLevelUI ) );

				_survivalLayer = survivalLayer;
				_level = lvl;

				bool const available = isBought();

				xmlLoader::macros::set( "lives_count", toStr( lvl.livesCount ) );
				xmlLoader::macros::set( "level_count", toStr( lvl.number ) );
				xmlLoader::macros::set( "level_count_plus_one", toStr( lvl.number + 1 ) );
				xmlLoader::macros::set( "level_price", available ? toStr( lvl.price ) : "" );
				xmlLoader::macros::set( "level_type", available ? "play" : "buy" );
				xmlLoader::macros::set( "level_locked", toStr(!available) );
				xmlLoader::macros::set( "level_score", toStr( Leaderboard::shared().getScoreLevel( lvl.number ) ) );
				NodeExt::load( xmlPathLevelUI );
				xmlLoader::macros::erase( "lives_count" );
				xmlLoader::macros::erase( "level_count" );
				xmlLoader::macros::erase( "level_count_plus_one" );
				xmlLoader::macros::erase( "level_price" );
				xmlLoader::macros::erase( "level_type" );
				xmlLoader::macros::erase( "level_locked" );
				xmlLoader::macros::erase( "level_score" );

				_score = getChildByName<Label *>( "score" );
				
				if( !available )
				{
					auto buttonText = dynamic_cast<Label *>(getChildByPath( "info/menu/play/normal/price" ));
					if( buttonText )
					{
						inapp::details( lvl.inapp, [buttonText]( inapp::SkuDetails product ){
							buttonText->setString( product.priceText );
						} );
					}
				}

				return true;
			}
			while( false );
			return false;
		}

		virtual ccMenuCallback get_callback_by_description( std::string const & name ) override
		{
			if( name == "play" )
			{
				return [this]( Ref * ){
					if( _survivalLayer->payForPlay( this->_level.number ) )
					{
						Director::getInstance()->pushScene( LoadLevelScene::create( this->_level.number, GameMode::survival, false ) );
					}
				};
			}
			else if( name == "buy" )
			{
				return [this]( Ref * ) {
					inapp::setCallbackPurchase( [this]( inapp::PurchaseResult result ) {
						if( result.result == inapp::Result::Fail || result.result == inapp::Result::Canceled )
							return;

						setBought( true );
						this->init( this->_level, _survivalLayer );
					} );
					inapp::purchase( this->_level.inapp );
				};
			}
			else if( name == "show_scores" )
			{
				return [this]( Ref * ) { Leaderboard::shared().openLevel(_level.number); };
			}
			else
			{
				return NodeExt_::get_callback_by_description( name );
			}
		}

		bool isBought()
		{
			return _level.inapp.empty() || UserData::shared().get(_level.inapp, false);
		}

		void setBought(bool val)
		{
			UserData::shared().write( this->_level.inapp, val );
			UserData::shared().save();
		}

	private:
		IntrusivePtr<SurvivalLayer> _survivalLayer;
		SurvivalLayer::Level _level;
		IntrusivePtr<Label> _score;
	};

	LevelColumn::LevelColumn() {}
	LevelColumn::~LevelColumn() {}
}

SurvivalLayer::SurvivalLayer() {}
SurvivalLayer::~SurvivalLayer() {}

SurvivalLayer::Level::Level(){}
SurvivalLayer::Level::Level( int _number, int _livesCount, int _price, std::string const & _inapp )
: number( _number )
, livesCount( _livesCount )
, price( _price )
, inapp( _inapp )
{}

std::vector<SurvivalLayer::Level> SurvivalLayer::_levels;

bool SurvivalLayer::init()
{
	do
	{
		CC_BREAK_IF( !FileUtils::getInstance()->isFileExist( xmlPathLevels ) );
		CC_BREAK_IF( !FileUtils::getInstance()->isFileExist( xmlPathBaseUI ) );
		CC_BREAK_IF( !LayerExt::init() );

		NodeExt::load( xmlPathBaseUI );

		pugi::xml_document doc;
		doc.load_file( xmlPathLevels.c_str() );

		_levels.clear();
		for( auto xmlLevel : doc.child( "levels" ) )
		{
			std::string inapp = xmlLevel.attribute( "inapp" ).as_string();
			if (inapp != "")
			{
				auto it = Config::shared().find(inapp);
				if (it == Config::shared().end())
					inapp = "";
				else
					inapp = it->second;
			}

			_levels.push_back( Level(
				xmlLevel.attribute( "number" ).as_int(),
				xmlLevel.attribute( "lives" ).as_int(),
				xmlLevel.attribute( "price" ).as_int(),
				inapp
			) );
		}

		Node * const levelsNode = getChildByName( "levels" );
		CC_BREAK_IF( levelsNode == nullptr );

		std::vector<IntrusivePtr<Node> > replace;

		for( auto node : levelsNode->getChildren() )
		{
			try
			{
				int const levelNum = std::atoi( node->getName().c_str() );
				auto levelUI = LevelColumn::create( _levels[ levelNum - 1 ], this );
				if( levelUI )
				{
					levelUI->setName( node->getName() );
					levelUI->setPosition( node->getPosition() );
					replace.push_back( levelUI );
				}
			}
			catch( ... )
			{
				continue;
			}
		}
		levelsNode->removeAllChildren();
		
		for( auto node : replace )
		{
			levelsNode->addChild( node.ptr() );
		}

		runEvent( "show" );

		return true;
	}
	while( false );
	return false;
}

int SurvivalLayer::getPrice( int levelNumber )
{
	int const money = ScoreCounter::shared().getMoney( kScoreCrystals );

	int levelPrice = -1;
	for( Level & level : _levels )
		if( level.number == levelNumber )
			return level.price;

	assert(false && "Level not found");
	return -1;
}

bool SurvivalLayer::payForPlay( int levelNumber )
{
	int const money = ScoreCounter::shared().getMoney( kScoreCrystals );

	int levelPrice = getPrice( levelNumber );

	removeFromParent();
	if( money < levelPrice )
	{
		auto scene = dynamic_cast<SmartScene*>(Director::getInstance()->getRunningScene());
		auto map = dynamic_cast<MapLayer*>(scene->getMainLayer().ptr());
		map->openShopFromLaboratory();
		return false;
	}

	return true;
}

ccMenuCallback SurvivalLayer::get_callback_by_description( std::string const & name )
{
	return [this, name]( Ref * ) { 
		if( name == "show_score" )
			Leaderboard::shared().openGLobal();

		runEvent( name );
	};

}
