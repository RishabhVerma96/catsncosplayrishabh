#ifndef __RouleteLayer_h__
#define __RouleteLayer_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "support/Award.h"
NS_CC_BEGIN

class RouleteActivatorNode : public Node, public NodeExt
{
	DECLARE_BUILDER( RouleteActivatorNode );
	bool init();
public:
	virtual void onEnter();
	virtual void onExit();
protected:
	void onChangeTicketsCount(int count);
public:
	bool _activateRoulette;
};


class RouleteLayer : public LayerExt, public LayerBlur
{
	DECLARE_BUILDER( RouleteLayer );
	bool init();

public:
	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;
	virtual ccMenuCallback get_callback_by_description( const std::string & name ) override;
	virtual bool loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )override;
	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override;
	virtual bool isBlurActive() override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;
protected:
	void disappearance();
	void spin();
	void stop();
	void showCongratulationMessage();
	void showNotEnoughtMessage();
	void showNotEnoughtDialog();
	void closeNotEnought();
	void getAward();
	void buildWheel();
	void generateAward();
	void update(float dt);
private:
	bool _locked;
	bool _useDialog;
	float _angularVelocity;
	float _angularDumping;
	float _wheelRadius;
	float _timeToDumping;
	size_t _awardIndex;
	std::vector< Award::Pointer > _awards;
	std::vector< int > _probabilities;

	LayerExt::Pointer _congratulation;
	LayerExt::Pointer _notEnought;
};

NS_CC_END
#endif // #ifndef RouleteLayer