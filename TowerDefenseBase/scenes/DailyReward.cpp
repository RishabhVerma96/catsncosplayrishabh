#include <ctime>

#include "ml/loadxml/xmlLoader.h"
#include "ml/Audio/AudioEngine.h"
#include "ScoreCounter.h"
#include "DailyReward.h"
#include "SmartScene.h"
#include "resources.h"
#include "UserData.h"

USING_NS_CC;

static std::string const gameOpenComboCount = "GameOpenComboCount";
static std::string const gameOpenLastTime = "GameOpenLastTime";
static std::string const xmlPathPopUp = "ini/daily_reward/popup.xml";
static std::string const xmlPathConfig = "ini/daily_reward/gifts.xml";

DailyReward::DailyReward() {}
DailyReward::~DailyReward() {}

bool DailyReward::init()
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		CC_BREAK_IF( !FileUtils::getInstance()->isFileExist( xmlPathConfig ) );

		pugi::xml_document doc;
		doc.load_file( xmlPathConfig.c_str() );

		for( auto gift : doc.child( "gifts" ) )
		{
			std::string const tag = gift.name();
			GiftType type =
				tag == "gold" ? Gold :
				tag == "fuel" ? Fuel :
				tag == "ticket" ? Ticket :
				tag == "item1" ? Item1 :
				tag == "item2" ? Item2 :
				tag == "item3" ? Item3 :
				tag == "item4" ? Item4 :
				tag == "item5" ? Item5 :
				/*g == "item6"*/ Item6 ;

			_gifts.push_back( Gift( type, gift.attribute( "count" ).as_int() ) );
		}

		CC_BREAK_IF( _gifts.empty() );

		_resultGift = generateGift();

		return true;
	} while( false );

	return false;
}

std::tm DailyReward::getTimeToNight()
{
	time_t const cur = time( nullptr );
	std::tm const curTM = *localtime( &cur );

	std::tm result;
	memset( &result, 0, sizeof( std::tm ) );
	result.tm_hour = 23 - curTM.tm_hour;
	result.tm_min = 59 - curTM.tm_min;
	result.tm_sec = 59 - curTM.tm_sec;

	return result;
}

std::tm DailyReward::getTimeToNext24hGift()
{
	int64_t const nextGiftTime = getLastGiftTime() + 60 * 60 * 24;
	std::time_t const leftTime = std::max<int64_t>( 0, nextGiftTime - time( nullptr ) );
	std::tm const leftDate = *gmtime( &leftTime );

	return leftDate;
}

bool DailyReward::isGifted24hPassed()
{
	int64_t const nextGiftTime = getLastGiftTime() + 60 * 60 * 24;
	return nextGiftTime < time( nullptr );

}

bool DailyReward::isGiftedToday()
{
	std::time_t const todayTime = time( nullptr );
	std::tm const todayDate = *localtime( &todayTime );

	std::time_t const lastOpenTime = getLastGiftTime();
	std::tm const lastOpenDate = *localtime( &lastOpenTime );

	return todayDate.tm_yday == lastOpenDate.tm_yday;
}

bool DailyReward::isGiftedYesterday()
{
	std::time_t const yesterdayTime = time( nullptr ) - 60 * 60 * 24;
	std::tm const yesterdayDate = *localtime( &yesterdayTime );

	std::time_t const lastOpenTime = getLastGiftTime();
	std::tm const lastOpenDate = *localtime( &lastOpenTime );

	return yesterdayDate.tm_yday == lastOpenDate.tm_yday;
}

int DailyReward::getComboCount()
{
	return UserData::shared().get(gameOpenComboCount, 0);
}

time_t DailyReward::getLastGiftTime()
{
	return static_cast<unsigned int>(UserData::shared().get(gameOpenLastTime, 0));
}

void DailyReward::incComboCount()
{
	UserData::shared().write( gameOpenComboCount, getComboCount() + 1 );
	UserData::shared().save();
}

void DailyReward::resetComboCount()
{
	UserData::shared().write( gameOpenComboCount, 0 );
	UserData::shared().save();
}

void DailyReward::giveGift()
{
	UserData::shared().write( gameOpenLastTime, (uint64_t)time( nullptr ) );
	UserData::shared().save();

	switch( _resultGift.type )
	{
		case Gold:
			ScoreCounter::shared().addMoney( kScoreCrystals, _resultGift.count, true );
			break;
		case Fuel:
			ScoreCounter::shared().addMoney( kScoreFuel, _resultGift.count, true );
			break;
		case Ticket:
			ScoreCounter::shared().addMoney( kScoreTicket, _resultGift.count, true );
			break;
		case Item1:
		case Item2:
		case Item3:
		case Item4:
		case Item5:
		case Item6:
			UserData::shared().bonusitem_add( (_resultGift.type - Item1) + 1, _resultGift.count );
			UserData::shared().save();
			break;
	}
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\

DailyRewardInstant::DailyRewardInstant() {}
DailyRewardInstant::~DailyRewardInstant() {}

bool DailyRewardInstant::init()
{
	do
	{
		CC_BREAK_IF( !DailyReward::init() );
		CC_BREAK_IF( !FileUtils::getInstance()->isFileExist( xmlPathPopUp ) );

		int const comboLighten = getComboCount();

		xmlLoader::macros::set( "win_type", toStr<int>( _resultGift.type ) );
		xmlLoader::macros::set( "win_count", toStr( _resultGift.count ) );

		for( int i = 0; i < (int)_gifts.size(); ++i )
		{
			std::string const prefix = "box" + toStr( i + 1 );
			xmlLoader::macros::set( prefix + "_active", toStr( i <= comboLighten ? 1 : 0 ) );
			xmlLoader::macros::set( prefix + "_type", toStr<int>( _gifts[i].type ) );
			xmlLoader::macros::set( prefix + "_count", toStr( _gifts[i].count ) );
		}

		NodeExt::load( xmlPathPopUp );

		for( int i = 0; i < (int)_gifts.size(); ++i )
		{
			std::string const prefix = "box" + toStr( i + 1 );
			xmlLoader::macros::erase( prefix + "_active" );
			xmlLoader::macros::erase( prefix + "_type" );
			xmlLoader::macros::erase( prefix + "_count" );
		}

		MenuItem * node = getNodeByPath<MenuItem>( this, "bg/menu/close" );
		if( node )
		{
			dynamic_cast<MenuItem *>(node)->setCallback( std::bind( &DailyRewardInstant::cb_close, this, std::placeholders::_1 ) );
		}

		playIntro();

		return true;
	} while( false );

	return false;
}

void DailyRewardInstant::cb_close( Ref * )
{
	playOutro();
	giveGift();
}

void DailyRewardInstant::playIntro()
{
	runEvent( "appearance" );

	Node * node = getNodeByPath( this, "bg/subtitle" );

	if( node )
	{
		node->runAction(
			Sequence::createWithTwoActions(
				DelayTime::create( 0.3f ),
				EaseBackOut::create( ScaleTo::create( 0.5f, node->getScaleY() ) )
			)
		);
		node->setScale( 0.0f );
	}

	node = getNodeByPath( this, "bg/prize" );
	if( node )
	{
		node->runAction(
			Sequence::create(
				DelayTime::create( 0.7f ),
				CallFunc::create( [this] { AudioEngine::shared().playEffect( kSoundTada ); } ),
				EaseBackOut::create( ScaleTo::create( 0.5f, node->getScaleY() ) ),
				nullptr
			)
		);
		node->setScale( 0.0f );
	}
}

void DailyRewardInstant::playOutro()
{
	if( !runEvent( "disappearance" ) )
	{
		removeFromParent();
	}
}

DailyReward::Gift DailyRewardInstant::generateGift()
{
	if( isGiftedYesterday() )
		incComboCount();
	else
		resetComboCount();

	srand( std::time( nullptr ) );

	int const combo = getComboCount();
	int const giftsCount = _gifts.size();
	int const giftNum = combo < giftsCount ? combo : (rand() % giftsCount);
	
	return _gifts[giftNum];
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\


DailyRewardRoulette::DailyRewardRoulette() {}
DailyRewardRoulette::~DailyRewardRoulette() {}

bool DailyRewardRoulette::init()
{
	do
	{
		CC_BREAK_IF( !DailyReward::init() );
		CC_BREAK_IF( !FileUtils::getInstance()->isFileExist( xmlPathPopUp ) );


		xmlLoader::macros::set( "giftAngle", toStr( _giftAngle + 1440 ) );
		xmlLoader::macros::set( "giftNum", toStr( _giftNum + 1 ) );
		NodeExt::load( xmlPathPopUp );
		xmlLoader::macros::erase( "giftAngle" );
		xmlLoader::macros::erase( "giftNum" );

		_timer = getChildByName<Label *>( "timer" );
		_button = static_cast<MenuItem *>( getChildByPath( "menu/spin" ) );

		runEvent( "show" );

		scheduleUpdate();
		return true;
	}
	while( false );
	return false;
}

DailyReward::Gift DailyRewardRoulette::generateGift()
{
	srand( std::time( nullptr ) );
	_giftNum = rand() % _gifts.size();
	_giftAngle = _giftNum * 360.0f / _gifts.size();
	return _gifts[ _giftNum ];
}


void DailyRewardRoulette::update( float dt )
{
	static float t = 0.0f;
	if( (t += dt) > 0.5f )
	{
		t = 0;
		std::tm left = getTimeToNext24hGift();
		_timer->setString( StringUtils::format("%02d : %02d : %02d", left.tm_hour, left.tm_min, left.tm_sec) );
		_button->setVisible( isGifted24hPassed() );
	}
}

ccMenuCallback DailyRewardRoulette::get_callback_by_description( std::string const & name )
{
	if( name == "spin" )
	{
		return [this, name]( Ref * button ) {
			static_cast<MenuItem *>( button )->setEnabled( false );
			
			float winTime = 5.0f;
			auto rotate = dynamic_cast<ActionInterval *>(this->getAction( "rotate" ).ptr());
			if( rotate != nullptr )
				winTime = rotate->getDuration();

			this->runAction( Sequence::createWithTwoActions(
				DelayTime::create( winTime ),
				CallFunc::create( std::bind( &DailyReward::giveGift, this ) )
			));
			
			this->runEvent( name );
		};
	}
	else
		return [this, name]( Ref * ){ this->runEvent( name ); };
}
