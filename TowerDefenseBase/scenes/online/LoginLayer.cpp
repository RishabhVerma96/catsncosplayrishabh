#include "ml/SmartScene.h"
#include "ml/MenuItem.h"
#include "ml/ScrollMenu.h"
#include "Services/playservices/playservices.h"
#include "LoginLayer.h"
#include "online/RapidJson.h"
#include "support.h"
#include "Config.h"
#include "online/OnlineConnector.h"
#include "FindOpponentLayer.h"
#include "support/LogLayer.h"
#include "ScoreCounter.h"
#include "DialogLayer.h"
USING_NS_CC;

namespace
{
	float const statisticUpdatePeriod = 1.0f;
}

LoginLayer::LoginLayer() 
{}

LoginLayer::~LoginLayer()
{
	OnlineConnector::shared().onConnectionChanged.remove( _ID );
	OnlineConnector::shared().onLogged.remove( _ID );
	OnlineConnector::shared().onStatistic.remove( _ID );
	PlayServises::OnConnected.remove( _ID );
	PlayServises::OnConnectFailed.remove( _ID );

	Director::getInstance()->getScheduler()->unscheduleAllForTarget( this );
}

bool LoginLayer::init()
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		LogLayer::shared();
		NodeExt::load( "ini/multiplayer/login.xml" );
		setDisapparanceOnBackButton();

		if( PlayServises::isConnected() == false )
		{
			PlayServises::OnConnected.add(_ID, std::bind( &LoginLayer::startLogin, this ) );
			PlayServises::OnConnectFailed.add( _ID, std::bind( &LoginLayer::cannotLogin, this ) );
			PlayServises::init( true );
		}
		else
		{
			startLogin();
		}




		return true;
	}
	while( false );
	return false;
}

void LoginLayer::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
}

bool LoginLayer::isBlurActive()
{
	return !isRunning();
}

void LoginLayer::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

ccMenuCallback LoginLayer::get_callback_by_description( std::string const & name )
{
	if( name == "find_opponent" )
		return [this]( Ref * ){ startSearchOpponent(); };
	if( name == "reconnect" )
		return [this]( Ref * ){ startLogin(); };
	else
		return LayerExt::get_callback_by_description( name );
}

void LoginLayer::onEnter()
{
	LayerExt::onEnter();
	refreshStatistic(0);
}

void LoginLayer::onExit()
{
	LayerExt::onExit();
}

bool LoginLayer::setProperty( const std::string & name, const std::string & value )
{
	return LayerExt::setProperty( name, value );
}

void LoginLayer::onConnectionFailed()
{
	runEvent( "connection_failed" );
}

void LoginLayer::openShop()
{
	int currency, cost;
	LevelParams::shared().getPayForLevel( 0, GameMode::multiplayer, currency, cost );
	int tab = currency == kScoreCrystals ? 1 : 2;
	DialogLayer::showForShop( "ini/dialogs/nogold.xml", 1, tab );
}

void LoginLayer::startLogin()
{
    if( !requestInternetConnectionStatus() )
    {
        showErrorDialog( true );
        return;
    }

    if( !OnlineConnector::shared().checkUserInfo() )
        return;
    
	OnlineConnector::shared().onConnectionChanged.add( _ID, std::bind( &LoginLayer::onConnectionFailed, this ) );
	auto callback = std::bind( &LoginLayer::refreshStatistic, this, std::placeholders::_1 );
	Director::getInstance()->getScheduler()->schedule( callback, this, 10, false, "refreshstatistic" );
	
	OnlineConnector::shared().onLogged.add( _ID, std::bind( [&]( bool result, int id )
	{
		if( result )
		{
			_playerId = id;
			runEvent( "connection_ok" );
			requestStatistic( _playerId );
		}
	},
	std::placeholders::_1, std::placeholders::_2 ) );

	OnlineConnector::shared().login();
}

void LoginLayer::cannotLogin()
{
    showErrorDialog( !requestInternetConnectionStatus() );
}

void LoginLayer::showErrorDialog( bool networkError )
{
    std::string path;
    
    if( networkError )
    {
        path = "ini/dialogs/multiplayer_no_connection.xml";
    }
    else
    {
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
        path = "ini/dialogs/ios_gamecenter_on_disabled.xml";
#elif CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
        path = "ini/dialogs/android_playservices_on_disabled.xml";
#else
        path = "ini/dialogs/ios_gamecenter_on_disabled.xml";
#endif
    }
    
    auto call = CallFunc::create( std::bind([&](const std::string& path, LayerExt*layer){
        auto dialog = DialogLayer::create( path, std::bind( [&]( bool, LayerExt*layer ){
            layer->removeFromParent();
        }, std::placeholders::_1, layer ) );
        getSmartScene()->pushLayer( dialog );
    }, path, this ) );
    runAction( call );
}

void LoginLayer::startSearchOpponent()
{
	int currency, cost, exist;
	LevelParams::shared().getPayForLevel( 0, GameMode::multiplayer, currency, cost );
	exist = ScoreCounter::shared().getMoney( currency );
	if( cost <= exist )
	{
		getSmartScene()->pushLayer( FindOpponentLayer::create() );
	}
	else
	{
		openShop();
	}
}

void LoginLayer::requestStatistic( int id )
{
	OnlineConnector::shared().onStatistic.add( _ID, [&]( bool result, RapidJsonNode const & response )
	{
		if( result )
		{
			this->displayStatistic( response );
			runEvent( "statistic_ok" );

			auto scoreNode = response.node( "scores" );
			if( scoreNode )
			{
				bool requestMyData( true );
				this->displayLeaderboards( scoreNode );
				size_t size = scoreNode.size();
				for( size_t i = 0; requestMyData && i < size; ++i )
				{
					auto id = scoreNode.at( i ).get<std::string>( "id" );
					if( strTo<int>( id ) == OnlineConnector::shared().getPlayerId() )
					{
						displayPlayerStatistic( scoreNode.at( i ) );
						requestMyData = false;
					}
				}
				if( requestMyData )
				{
					auto id = OnlineConnector::shared().getPlayerId();
					if( id != -1 )
						requestStatistic( id );
				}
				else
				{
					OnlineConnector::shared().onStatistic.remove( _ID );
				}
			}
			else
			{
				this->addOrUpdateItemToLeaderboard( response, 20 );
				displayPlayerStatistic( response );
				OnlineConnector::shared().onStatistic.remove( _ID );
			}
		}
		else
		{
			runEvent( "statistic_fail" );
		}
	} );

	OnlineConnector::shared().statistic(20, 0, id);
}

void LoginLayer::refreshStatistic( float dt )
{
	if( OnlineConnector::shared().isConnected() )
		LoginLayer::requestStatistic( -1 );
}

void LoginLayer::displayPlayerStatistic( const RapidJsonNode& json )
{
	auto name = getNodeByPath<Text>( this, getParamCollection().get( "pathto_player_name" ) );
	auto victory = getNodeByPath<Text>( this, getParamCollection().get( "pathto_player_victory" ) );
	auto defeat = getNodeByPath<Text>( this, getParamCollection().get( "pathto_player_defeat" ) );
	auto rating = getNodeByPath<Text>( this, getParamCollection().get( "pathto_player_rating" ) );
	auto score = getNodeByPath<Text>( this, getParamCollection().get( "pathto_player_score" ) );
	int defeatcount = strTo<int>( json.get<std::string>( "game_count" ) ) - strTo<int>( json.get<std::string>( "win_count" ) );
	if( name ) name->setString( json.get<std::string>( "nickname" ) );
	if( victory ) victory->setString( json.get<std::string>( "win_count" ) );
	if( defeat ) defeat->setString( toStr( defeatcount ) );
	if( rating ) rating->setString( json.get<std::string>( "rank" ) );
	if( score ) score->setString( json.get<std::string>( "score" ) );
}

void LoginLayer::displayStatistic( const RapidJsonNode& response )
{
	if( response.node( "online_count" ) && response.node("player_count") )
	{
		int const onlineCount = response.get<int>( "online_count" );
		int const playerCount = response.get<int>( "player_count" );

		auto onlineOutOfTotalLabelPath = getParamCollection().get( "pathto_online_out_of_total", "somepath" );
		auto label = getNodeByPath<Label>( this, onlineOutOfTotalLabelPath );
		if( label )
			label->setString( StringUtils::format( "%d/%d", onlineCount, playerCount ) );
	}
}

void LoginLayer::displayLeaderboards( const RapidJsonNode& leaderboardJson )
{
	for( size_t i = 0; i < leaderboardJson.size(); ++i )
	{
		auto node = leaderboardJson.at( i );
		addOrUpdateItemToLeaderboard( node, i );
	}
}

void LoginLayer::addOrUpdateItemToLeaderboard( const RapidJsonNode& json, int index )
{
	auto buildItem = [&]( IntrusivePtr<mlMenuItem> item )
	{
		auto name = json.get<std::string>( "nickname" );
		auto victory = json.get<std::string>( "win_count" );
		auto gamecount = json.get<std::string>( "game_count" );
		auto score = json.get<std::string>( "score" );
		auto rank = json.get<std::string>( "rank" );
		xmlLoader::macros::set( "player_position", rank );
		xmlLoader::macros::set( "player_name", name );
		xmlLoader::macros::set( "player_victory", victory );
		xmlLoader::macros::set( "player_defeat", toStr( strTo<int>( gamecount ) - strTo<int>( victory ) ) );
		xmlLoader::macros::set( "player_score", score );
		if( item ) {
			item->getChildByName("normal")->removeAllChildren();
			item->load( "ini/multiplayer/leaderboard_item.xml" );
		}
		else
		{
			item = xmlLoader::load_node<mlMenuItem>( "ini/multiplayer/leaderboard_item.xml" );
		}
		auto color = item->getParamCollection().get( "color" + toStr(index % 2) );
		if( getNodeByPath( item, "normal/bg" ) )
			getNodeByPath( item, "normal/bg" )->setColor( strTo<Color3B>( color ) );
		return item;
	};
	auto pathToMenu = getParamCollection().get( "pathto_leaderboard_menu", "somepath" );
	auto menu = getNodeByPath<ScrollMenu>( this, pathToMenu );
	if( menu )
	{
		if( static_cast<size_t>(index) < menu->getItemsCount() )
		{
			buildItem( dynamic_cast<mlMenuItem *>(menu->getItem( index )) );
		}
		else
		{
			menu->addItem( buildItem( nullptr ) );
			menu->align( menu->getAlignedColums() );
		}
	}
}