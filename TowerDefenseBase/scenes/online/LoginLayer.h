#pragma once

#include "cocos2d.h"
#include "online/onlineutils.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"

NS_CC_BEGIN

class LoginLayer : public LayerExt, public LayerBlur
{
public:
	DECLARE_BUILDER( LoginLayer );
	bool init();

	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override;
	virtual bool isBlurActive()override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override;
	virtual ccMenuCallback get_callback_by_description( std::string const & name ) override;
	virtual void onEnter() override;
	virtual void onExit() override;
	virtual bool setProperty( const std::string & name, const std::string & value ) override;
private:
	void onConnectionFailed();
	void openShop();
	void startLogin();
	void cannotLogin();
    void showErrorDialog( bool networkError );
	void startSearchOpponent();
	void requestStatistic( int id = -1 );
	void refreshStatistic( float dt );
	void displayPlayerStatistic( const RapidJsonNode& json );
	void displayStatistic( const RapidJsonNode& json );
	void displayLeaderboards( const RapidJsonNode& json );
	void addOrUpdateItemToLeaderboard( const RapidJsonNode& json, int index );
private:
	int _playerId = -1;
};

NS_CC_END