#pragma once

#include "cocos2d.h"
#include "ml/NodeExt.h"
#include "online/OnlineConnector.h"

NS_CC_BEGIN

class WaitOpponentLayer : public LayerExt
{
	DECLARE_BUILDER( WaitOpponentLayer );
	bool init();
public:
	virtual void onEnter() override;
	virtual void onExit() override;
	virtual bool setProperty( const std::string & name, const std::string & value ) override;
	void activateBot();
private:
	void playGame();
	void stopGame();
	void requestLoading();
	void responseLoading( bool result, int opponentPercent );
	void displayLoadingProgress( int percent );
private:
	std::string _labelProgressPath;
	bool _botMode = false;
};

NS_CC_END