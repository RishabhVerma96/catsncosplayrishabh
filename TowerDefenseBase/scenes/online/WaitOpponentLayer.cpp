#include "WaitOpponentLayer.h"
#include "LoadLevelScene.h"
NS_CC_BEGIN

namespace
{
	float const waitOpponentPeriod = 0.5f;
}

WaitOpponentLayer::WaitOpponentLayer()
{
	OnlineConnector::shared().onLoadingReport.add( _ID, std::bind( &WaitOpponentLayer::responseLoading, this, std::placeholders::_1, std::placeholders::_2 ) );
}

WaitOpponentLayer::~WaitOpponentLayer( )
{
	OnlineConnector::shared().onLoadingReport.remove( _ID );
}

bool WaitOpponentLayer::init()
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		load( "ini/multiplayer/waitopponent.xml" );
		return true;
	}
	while( false );
	return false;
}

void WaitOpponentLayer::onEnter()
{
	LayerExt::onEnter();
	requestLoading();
}

void WaitOpponentLayer::onExit()
{
	LayerExt::onExit();
}

bool WaitOpponentLayer::setProperty( const std::string & name, const std::string & value )
{
	if( name == "label_progress" )
	{
		_labelProgressPath = value;
		return true;
	}
	else
		return LayerExt::setProperty( name, value );
}

void WaitOpponentLayer::activateBot()
{
	_botMode = true;
	displayLoadingProgress( 100 );
	//runEvent( "on_sessionwithbot" );
}

void WaitOpponentLayer::playGame()
{
	disappearance();
}

void WaitOpponentLayer::requestLoading()
{
	if( !_botMode )
		OnlineConnector::shared().loadingReport( 101 );
}

void WaitOpponentLayer::responseLoading( bool result, int opponentPercent )
{
	if( opponentPercent == 101 )
	{
		playGame();
	}
	else
	{
		displayLoadingProgress( opponentPercent );
		runAction( Sequence::createWithTwoActions(
			DelayTime::create( waitOpponentPeriod ),
			CallFunc::create( std::bind( &WaitOpponentLayer::requestLoading, this ) )
		) );
	}
}

void WaitOpponentLayer::displayLoadingProgress( int percent )
{
	if( !_labelProgressPath.empty() )
	{
		auto label = getNodeByPath<Label>( this, _labelProgressPath );
		if( label )
			label->setString( StringUtils::format( "%d%%", std::min(std::max( 0, percent ), 100) ) );
	}
}

NS_CC_END