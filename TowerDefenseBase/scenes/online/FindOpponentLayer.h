#ifndef __FindOpponentLayer_h__
#define __FindOpponentLayer_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "online/OnlineConnector.h"
NS_CC_BEGIN





class FindOpponentLayer : public LayerExt
{
	DECLARE_BUILDER( FindOpponentLayer );
	bool init();
public:
	virtual void onEnter()override;
	virtual void onExit()override;
protected:
	void startSession( bool sessionWithBot );
	void find_send( SearchOpponentState state, float delay );
	void find_recv( bool result, float elapsed, SearchOpponentState state );
	void runBot();
private:
	bool _sessionRunned;
	SearchOpponentState _localState;
	SearchOpponentState _opponentState;
};




NS_CC_END
#endif // #ifndef FindOpponentLayer