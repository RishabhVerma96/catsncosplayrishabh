#include "FindOpponentLayer.h"
#include "LoadLevelScene.h"
NS_CC_BEGIN

namespace
{
	float const findOpponentPeriod = 1.f;
	const std::string keyRunBot( "FindOpponentLayer_runBot" );
	const std::string keyRunBotDuration( "online_runbot_duration" );
}

FindOpponentLayer::FindOpponentLayer()
	: _sessionRunned( false )
	, _localState( SearchOpponentState::wait )
	, _opponentState( SearchOpponentState::cancel )
{
	OnlineConnector::shared().onSearhOpponent.add( _ID, std::bind( &FindOpponentLayer::find_recv, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3 ) );
}

FindOpponentLayer::~FindOpponentLayer( )
{
	OnlineConnector::shared().onSearhOpponent.remove( _ID );
	auto scheduler = Director::getInstance()->getScheduler();
	scheduler->unscheduleAllForTarget( this );
}

bool FindOpponentLayer::init()
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		load( "ini/multiplayer/findopponent.xml" );
		return true;
	}
	while( false );
	return false;
}

void FindOpponentLayer::onEnter()
{
	LayerExt::onEnter();
	find_send( SearchOpponentState::wait, 0 );
	
	auto scheduler = Director::getInstance()->getScheduler();
	auto callback = std::bind( &FindOpponentLayer::runBot, this );
	auto duration = Config::shared().get<float>( keyRunBotDuration );
	scheduler->schedule( callback, this, duration, false, keyRunBot );
}

void FindOpponentLayer::onExit()
{
	if( !_sessionRunned )
		find_send( SearchOpponentState::cancel, 0 );
	LayerExt::onExit();
}

void FindOpponentLayer::startSession( bool sessionWithBot )
{
	auto scheduler = Director::getInstance()->getScheduler();
	scheduler->unscheduleAllForTarget( this );

	_sessionRunned = true;
	auto scene = LoadLevelScene::create( 0, GameMode::multiplayer, sessionWithBot );
	Director::getInstance()->pushScene( scene );
	removeFromParent();
}

void FindOpponentLayer::find_send( SearchOpponentState state, float delay )
{
	_localState = state;

	if( delay <= 0.01f )
	{
		OnlineConnector::shared().searchOpponent( state );
	}
	else
	{
		auto tag = 0x0123;
		auto action = Sequence::createWithTwoActions(
			DelayTime::create( delay ),
			CallFunc::create( std::bind( &FindOpponentLayer::find_send, this, state, 0 ) )
			);
		if( getActionByTag( tag ) )
			stopActionByTag( tag );
		action->setTag( tag );
		runAction( action );
	}
}

void FindOpponentLayer::find_recv( bool result, float elapsed, SearchOpponentState state )
{
	_opponentState = state;

	if( result && _sessionRunned == false )
	{
		auto scheduler = Director::getInstance()->getScheduler();
		scheduler->unscheduleAllForTarget( this );

		if( state == SearchOpponentState::ready )
		{
			find_send( SearchOpponentState::ready, 0 );
			startSession(false);
		}
		else if( state == SearchOpponentState::wait )
		{
			find_send( SearchOpponentState::ready, findOpponentPeriod );
			runEvent( "opponent_ok" );
		}
	}
	else if( result == false )
	{
		runEvent( "opponent_failed" );
		find_send( SearchOpponentState::wait, findOpponentPeriod - elapsed );
	}
}

void FindOpponentLayer::runBot()
{
	if( _localState == SearchOpponentState::wait && _opponentState == SearchOpponentState::cancel ) {
		find_send( SearchOpponentState::cancel, 0 );
		startSession( true );
	}
}

NS_CC_END