//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 08.31.2015.
//
//
#ifndef __EulaLayer_h__
#define __EulaLayer_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "ml/Scissor.h"
NS_CC_BEGIN

class EulaLayer : public LayerExt
{
	DECLARE_BUILDER( EulaLayer );
	bool init();
public:
protected:
	virtual ccMenuCallback get_callback_by_description( const std::string & name ) override;
private:
};

NS_CC_END
#endif // #ifndef EulaLayer