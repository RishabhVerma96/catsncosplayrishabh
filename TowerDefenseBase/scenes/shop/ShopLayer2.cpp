#include "ml/SmartScene.h"
#include "ml/Language.h"
#include "ml/ImageManager.h"
#include "ml/Audio/AudioEngine.h"
#include "flurry/flurry_.h"
#include "gamegs/GameScene.h"
#include "ShopLayer.h"
#include "ShopLayer2.h"
#include "ScoreCounter.h"
#include "UserData.h"
#include "consts.h"
#include "game/unit/Hero.h"
#include "plugins/AdsPlugin.h"
#include "Tutorial.h"
#include "ScoreLayer.h"
#include "DialogLayer.h"
#include "tower.h"
NS_CC_BEGIN


void ShopLayer2::Dispatcher::onCreate()
{
	pugi::xml_document doc;
	doc.load_file( "ini/shop/default_params.xml" );
	auto root = doc.root().first_child();

	for( auto child : root )
	{
		std::string name = child.attribute( "name" ).as_string();
		std::string price = child.attribute( "price" ).as_string();
		bool inapp = child.attribute( "inapp" ).as_bool( true );
		int value = child.attribute( "value" ).as_int();
		bool consumable = child.attribute( "consumable" ).as_bool( true );

		std::string productname( name );
		auto saveddesc = UserData::shared().get( k::user::PurchaseSavedValue + productname );
		if( saveddesc.empty() == false )
		{
			value = strTo<int>( saveddesc );
		}

		if( inapp )
		{
			productname = Config::shared().get( "inappPackage" ) + name;
		}
		products[productname].isInapp = inapp;
		products[productname].count = value;
		products[productname].price = price;
		products[productname].consumable = consumable;
	}
}

ShopLayer2* ShopLayer2_instance( nullptr );

ShopLayer2::ShopLayer2()
: _useDialog(false)
{
	ShopLayer2_instance = this;
}

ShopLayer2::~ShopLayer2( )
{
	ShopLayer2_instance = nullptr;
	AdsPlugin::shared().observerVideoResult.remove( _ID );
}

ccMenuCallback ShopLayer2::dispatch_query( const std::string & callbackDescription )
{
	if( callbackDescription.find( "shop:" ) == 0 )
	{
		ParamCollection pc( callbackDescription );
		int type = strTo<int>( pc.get( "shop" ) );
		int index = strTo<int>( pc.get( "index" ) );
		int tab = strTo<int>( pc.get( "tab" ) );
		switch( type )
		{
			case 1:
				//TODO: open ShopLayer
				assert( 0 );
				break;
			case 2:
				return std::bind( []( Ref*, int index, int tab )
				{
					auto scene = dynamic_cast<SmartScene*>(Director::getInstance()->getRunningScene());
					auto gamescene = dynamic_cast<GameScene*>(scene);
					assert( scene );
					if( !scene )return;
					auto current = getNodeByPath<ShopLayer2>( scene, "shop" );
					if( current )
					{
						current->openTab( tab );
					}
					else if( gamescene )
					{
						gamescene->openShop2( nullptr, false );
					}
					else
					{
						auto layer = ShopLayer2::create( index, tab );
						if( layer )
						{
							scene->pushLayer( layer, true );
							TutorialManager::shared().dispatch( index == 1 ? "map_openshop" : "level_openshop" );
						}
					}
				}, std::placeholders::_1, index, tab );
				break;
		}
	}
	assert( 0 );
	return nullptr;
}

bool ShopLayer2::init( int index, int tab )
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		CC_BREAK_IF( !LayerBlur::init() );
		initBlockLayer( "images/loading.png" );
		setDisapparanceOnBackButton();
		loadDefaultValues();
		checkGifts();
		xmlLoader::macros::set( "internet_enabled", toStr( requestInternetConnectionStatus() ) );
		std::string path = "ini/shop2/layer" + toStr( index ) + ".xml";
		load( path );

		setBonusesCount();
		openTab( tab );
		requestDetails();
		schedule( schedule_selector( ShopLayer2::details_dispather ) );

		return true;
	}
	while( false );
	return false;

}

void ShopLayer2::onEnter()
{
	LayerExt::onEnter();
	auto scene = Director::getInstance()->getRunningScene();
	auto scores = scene->getChildByName( "scorelayer" );
	if( !scores )
	{
		_scoreLayer = ScoreLayer::create();
		scene->addChild( _scoreLayer, 999 );
	}
}

void ShopLayer2::onExit()
{
	LayerExt::onExit();
	if( _scoreLayer )
	{
		_scoreLayer->removeFromParent();
		_scoreLayer.reset( nullptr );
	}
}

bool ShopLayer2::setProperty( std::string const & name, std::string const & value )
{
	if( name == "useDialog" || name == "usedialog" )
		_useDialog = strTo<bool>( value ) && Config::shared().get<bool>( "useDialogs" );
	else
		return LayerExt::setProperty( name, value );
	return true;
}

ccMenuCallback ShopLayer2::get_callback_by_description( const std::string & name )
{
	if( name.find( "purchase:" ) == 0 )
	{
		auto product = name.substr( strlen( "purchase:" ) );
		return std::bind( &ShopLayer2::purchase, this, product );
	}
	if( name.find( "video:" ) == 0 )
	{
		auto product = name.substr( strlen( "video:" ) );
		return std::bind( ShopLayer2::video, product, this );
	}
	if( name.find( "item:" ) == 0 )
	{
		auto product = name.substr( strlen( "item:" ) );
		return std::bind( &ShopLayer2::purchaseItem, this, product );
	}
	if( name.find( "gift:" ) == 0 )
	{
		auto product = name.substr( strlen( "gift:" ) );
		return std::bind( &ShopLayer2::gift, this, product );
	}
	return LayerExt::get_callback_by_description( name );
}

void ShopLayer2::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
}

bool ShopLayer2::isBlurActive()
{
	return !isRunning();
}

void ShopLayer2::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

void ShopLayer2::openTab( int index )
{
	runEvent( "showtab" + toStr( index ) );
}

void ShopLayer2::loadDefaultValues()
{
	pugi::xml_document doc;
	doc.load_file( "ini/bonusitems.xml" );
	auto root = doc.root().first_child();
	for( auto xmlNode : root )
	{
		std::string name = xmlNode.name();
		_bonuses[name].cost = xmlNode.attribute( "cost" ).as_int();
		_bonuses[name].index = xmlNode.attribute( "default" ).as_int();
		xmlLoader::macros::set( name + "_cost", toStr( _bonuses[name].cost ) );
	}
}

void ShopLayer2::checkGifts()
{
	std::string gift = UserData::shared().get( k::user::ShopGift );
	xmlLoader::macros::set( gift + "_actived", toStr( gift.empty() == false ) );
}

void ShopLayer2::requestDetails()
{
	for( auto& item : Dispatcher::shared().products )
	{
		if( item.second.isInapp )
			inapp::details( item.first, std::bind( &ShopLayer2::details_ansfer, this, std::placeholders::_1 ) );
	}
}

void ShopLayer2::details_ansfer( inapp::SkuDetails result )
{
	_queueDetails.push( result );
}

void ShopLayer2::details_dispather( float dt )
{
	while( _queueDetails.empty() == false )
	{
		inapp::SkuDetails details = _queueDetails.front();
		_queueDetails.pop();

		if( details.result == inapp::Result::Ok )
		{
			if( details.description.empty() == false )
			{
				Dispatcher::shared().products.at( details.productId ).description = details.description;
				Dispatcher::shared().products.at( details.productId ).count = strTo<int>( details.description );
				Dispatcher::shared().products.at( details.productId ).price = details.priceText;
			}
			UserData::shared().write( k::user::PurchaseSavedValue + details.productId, details.description );
		}
	}
	fetchItems();
}

void ShopLayer2::fetchItems()
{
	int indexTab( 1 );
	auto package = Config::shared().get( "inappPackage" );
	while( true )
	{
		auto tab = getNodeByPath( this, "tab" + toStr( indexTab ) );
		if( !tab )
			break;
		++indexTab;
		for( auto child : tab->getChildren() )
		{
			auto item = dynamic_cast<mlMenuItem*>(child);
			if( !item )
				continue;
			auto name = item->getName();
			auto productname = package + name;
			auto iter = Dispatcher::shared().products.find( productname );
			if( iter == Dispatcher::shared().products.end() )
				continue;
			if( iter->second.isInapp == false )
				continue;			
			auto desc = getNodeByPath<Text>( item, item->getParamCollection().get( "pathto_desc" ) );
			auto cost = getNodeByPath<Text>( item, item->getParamCollection().get( "pathto_cost" ) );
			if( desc && iter->second.consumable )
				desc->setString( toStr( iter->second.count ) );
			if( cost )
				cost->setString( iter->second.price );
			if( iter->second.consumable == false )
			{
				bool isPurchased = UserData::shared().get<bool>( productname + "_purchased" );
				auto buyNode = getNodeByPath( item, item->getParamCollection().get( "pathto_buy" ) );
				auto purchasedNode = getNodeByPath( item, item->getParamCollection().get( "pathto_purchased" ) );
				item->setEnabled( isPurchased == false );
				if( buyNode )
					buyNode->setVisible( isPurchased == false );
				if( purchasedNode )
					purchasedNode->setVisible( isPurchased == true );					
			}
		}
	}
}

void ShopLayer2::purchase( const std::string& product )
{
	pushBlockLayer(true, 30);
	inapp::setCallbackPurchase( std::bind( ShopLayer2::purchaseResult, std::placeholders::_1 ) );
	auto id = Config::shared().get( "inappPackage" ) + product;
	inapp::purchase( id );
}

void ShopLayer2::purchaseItem( const std::string& itemname )
{
	int index( 0 );
	auto iter = _bonuses.find( itemname );
	if( iter == _bonuses.end() )
	{
		assert( 0 );
		return;
	}
	index = iter->second.index;
	auto cost = iter->second.cost;
	auto money = ScoreCounter::shared().getMoney( kScoreCrystals );
	if( cost <= money )
	{
		UserData::shared().bonusitem_add( index, 1 );
		setBonusesCount();
		ScoreCounter::shared().subMoney( kScoreCrystals, cost, true, "itemshop." + itemname );
		AudioEngine::shared().playEffect( kSoundShopPurchase );
		UserData::shared().save();
		runEvent( "bonusitem_purchased_" + itemname );

		ParamCollection pc;
		pc["event"] = "BonusitemPurchase";
		pc["item"] = itemname;
		flurry::logEvent( pc );
	}
	else
	{
		auto callback = [this]( bool answer ){
			if( answer ) this->openTab( 1 );
		};
		if( _useDialog )
			DialogLayer::createAndRun( "ini/dialogs/shop_gold.xml", std::bind( callback, std::placeholders::_1 ) );
		else
			callback( true );
	}
}

void ShopLayer2::gift( const std::string& name )
{
	int type( -1 );
	if( name == "gift_gold" ) type = kScoreCrystals;
	else if( name == "gift_fuel" ) type = kScoreFuel;
	else if( name == "gift_gear" ) type = kScoreLevel;
	else if( name == "gift_ticket" ) type = kScoreTicket;

	if( type != -1 )
	{
		auto count = Dispatcher::shared().products[name].count;
		ScoreCounter::shared().addMoney( type, count, true );
		UserData::shared().write( k::user::ShopGift, std::string("") );
		xmlLoader::macros::set( name + "_actived", toStr( false ) );
	}
	disappearance();
}

void ShopLayer2::purchaseResult( inapp::PurchaseResult result )
{
	IntrusivePtr<Node> target = Node::create();
	auto callback = []( float, inapp::PurchaseResult result, IntrusivePtr<Node> target )
	{
		auto scheduler = Director::getInstance()->getScheduler();
		scheduler->unschedule( "purchaseResultSafeThreadCallback", target.ptr() );
		ShopLayer2::purchaseResultSafeThread( result );
	};
	auto scheduler = Director::getInstance()->getScheduler();
	scheduler->schedule( std::bind( callback, std::placeholders::_1, result, target ), 
		target.ptr(), 0, false, "purchaseResultSafeThreadCallback" );
}

void ShopLayer2::purchaseResultSafeThread( inapp::PurchaseResult result )
{
	if( ShopLayer2_instance )
		ShopLayer2_instance->popBlockLayer();
	if( result.result == inapp::Result::Ok || result.result == inapp::Result::Restored )
	{
		auto product = result.productId;
		auto count = ShopLayer2_instance ?
			ShopLayer2::Dispatcher::shared().products[product].count :
			0;
		auto consumable = ShopLayer2_instance ?
			ShopLayer2::Dispatcher::shared().products[product].consumable :
			0;
		if( consumable == false )
		{
			UserData::shared().write( product + "_purchased", true );
		}
		auto dispatchGold = [result, count, product]()
		{
			ScoreCounter::shared().addMoney( kScoreCrystals, count, true );
			inapp::confirm( product );
		};
		auto dispatchTickets = [result, count, product]()
		{
			ScoreCounter::shared().addMoney( kScoreTicket, count, true );
			inapp::confirm( product );
		};
		auto dispatchGear = [result, count, product]()
		{
			ScoreCounter::shared().addMoney( kScoreLevel, count, false );
			std::string key = k::user::BoughtScores + toStr( kScoreLevel );
			int boughtScores = UserData::shared().get(key, 0) + count;
			UserData::shared().write( key, boughtScores );
			inapp::confirm( product );
		};
		auto dispatchFuel = [result, count, product]()
		{
			ScoreCounter::shared().addMoney( kScoreFuel, count, true );
			UserData::shared().write( k::user::MaxFuelValue, count );
			inapp::confirm( product );
		};
		auto dispatchPackHero = [result, count, product]( int index )
		{
			auto startIndex = HeroExp::shared().getNonPurchasableHeroesCount();
			auto count = Config::shared().get<int>( "heroesCount" );
			for( int i = startIndex; i < count; ++i )
				HeroExp::shared().heroBought( "hero" + toStr( i + 1 ) );
			ScoreCounter::shared().addMoney( kScoreCrystals, 1000, true );
		};
		auto dispatchHero = [result, count, product]()
		{
			std::string hero;
			hero = product.substr( Config::shared().get( "inappPackage" ).size() );
			HeroExp::shared().heroBought( hero );
		};
		auto dispatchSurvival = [product]()
		{
			UserData::shared().write( product, true );
			UserData::shared().save();
			inapp::confirm( product );
		};
		auto dispatchTower = [product]()
		{
			mlTowersInfo::shared().set_purchased_by_inapp( product );
			inapp::confirm( product );
		};
		auto dispatchLootPicker = []()
		{
			UserData::shared().write( k::user::LootPickerBought, true );
			UserData::shared().save();
		};
		

		int typeScore = -1;
		bool dispatched( true );
		if( 0 );
		else if( product == Config::shared().get( "inappGold1" ) ) { dispatchGold(); typeScore = kScoreCrystals; }
		else if( product == Config::shared().get( "inappGold2" ) ) { dispatchGold(); typeScore = kScoreCrystals; }
		else if( product == Config::shared().get( "inappGold3" ) ) { dispatchGold(); typeScore = kScoreCrystals; }
		else if( product == Config::shared().get( "inappGold4" ) ) { dispatchGold(); typeScore = kScoreCrystals; }
		else if( product == Config::shared().get( "inappGold5" ) ) { dispatchGold(); typeScore = kScoreCrystals; }
		else if( product == Config::shared().get( "inappGear1" ) ) { dispatchGear(); typeScore = kScoreLevel; }
		else if( product == Config::shared().get( "inappGear2" ) ) { dispatchGear(); typeScore = kScoreLevel; }
		else if( product == Config::shared().get( "inappGear3" ) ) { dispatchGear(); typeScore = kScoreLevel; }
		else if( product == Config::shared().get( "inappGear4" ) ) { dispatchGear(); typeScore = kScoreLevel; }
		else if( product == Config::shared().get( "inappGear5" ) ) { dispatchGear(); typeScore = kScoreLevel; }
		else if( product == Config::shared().get( "inappFuel1" ) ) { dispatchFuel(); typeScore = kScoreFuel; }
		else if( product == Config::shared().get( "inappFuel2" ) ) { dispatchFuel(); typeScore = kScoreFuel; }
		else if( product == Config::shared().get( "inappFuel3" ) ) { dispatchFuel(); typeScore = kScoreFuel; }
		else if( product == Config::shared().get( "inappFuel4" ) ) { dispatchFuel(); typeScore = kScoreFuel; }
		else if( product == Config::shared().get( "inappFuel5" ) ) { dispatchFuel(); typeScore = kScoreFuel; }
		else if( product == Config::shared().get( "inappTicket1" ) ) { dispatchTickets(); typeScore = kScoreTicket; }
		else if( product == Config::shared().get( "inappTicket2" ) ) { dispatchTickets(); typeScore = kScoreTicket; }
		else if( product == Config::shared().get( "inappTicket3" ) ) { dispatchTickets(); typeScore = kScoreTicket; }
		else if( product == Config::shared().get( "inappTicket4" ) ) { dispatchTickets(); typeScore = kScoreTicket; }
		else if( product == Config::shared().get( "inappTicket5" ) ) { dispatchTickets(); typeScore = kScoreTicket; }
		else if( product == Config::shared().get( "inappPackHeroes1" ) ) dispatchPackHero( 1 );
		else if( product.find( Config::shared().get( "inappPackage" ) + "hero" ) == 0 )dispatchHero();
		else if( product == Config::shared().get( "inappInfinity1" ) ) { dispatchSurvival(); }
		else if( product == Config::shared().get( "inappTower1" ) ) { dispatchTower(); }
		else if( product == Config::shared().get( "inappTower2" ) ) { dispatchTower(); }
		else if( product == Config::shared().get( "inappLootPicker" ) ) { dispatchLootPicker(); typeScore = kScoreLootPicker; }
		else dispatched = false;

		if( dispatched )
		{
			ShopLayer::observerOnPurchase().pushevent( typeScore, count );
			UserData::shared().save();
		}
	}
	else //result.result == inapp::Result::Failed
	{
		ShopLayer::observerOnFailed().pushevent();
	}
	ParamCollection pc;
	if( result.result == inapp::Result::Ok )
		pc["event"] = "PurchaseSuccessful";
	else if( result.result == inapp::Result::Restored )
		pc["event"] = "PurchaseRestored";
	else if( result.result == inapp::Result::Fail )
		pc["event"] = "PurchaseFailed";
	else if( result.result == inapp::Result::Canceled )
		pc["event"] = "PurchaseCanceled";
	else 
		pc["event"] = "Purchase";

	pc["level"] = toStr( UserData::shared().level_getCountPassed() );
	pc["errormsg"] = result.errormsg;
	pc["errorcode"] = toStr( result.errorcode );
	if( result.result == inapp::Result::Ok )
		pc["value"] = result.productId;
	else if( result.result == inapp::Result::Fail )
		pc["value"] = "failed";
	else
		pc["value"] = "canceled";



	flurry::logEvent( pc );
}

void ShopLayer2::video( const std::string& product, LayerExt* instance )
{
	int type( -1 );
	if( product == "gold" ) type = kScoreCrystals;
	if( product == "fuel" ) type = kScoreFuel;
	if( product == "gear" ) type = kScoreLevel;
	if( product == "ticket" ) type = kScoreTicket;
	if( instance )
		instance->pushBlockLayer( true, 10 );
	int id = ShopLayer2_instance ? ShopLayer2_instance->_ID : 0;
	AdsPlugin::shared().observerVideoResult.add( id, std::bind( ShopLayer2::videoResult, std::placeholders::_1, type, instance ) );
	AdsPlugin::shared().showVideo();
}

void ShopLayer2::videoResult( bool result, int const type, LayerExt* instance )
{
	if( instance )
		instance->popBlockLayer();

	std::string product;
	if( type == kScoreFuel )product = "free_fuel";
	if( type == kScoreCrystals )product = "free_gold";
	if( type == kScoreTicket )product = "free_ticket";
	if( type == kScoreLevel )product = "free_gear";
	if( result )
	{
		auto current = ScoreCounter::shared().getMoney( type );
		auto increase = Dispatcher::shared().products[product].count;
		if( increase == 0 ) increase = 10;
		if( type == kScoreFuel )
		{
			auto max = ScoreByTime::shared().max_fuel();
			auto summ = current + increase;
			summ = std::min( max, summ );
			auto diff = summ - current;
			increase = std::max( 0, diff );
		}
		ScoreCounter::shared().addMoney( type, increase, true );
		if( increase == 0 )
		{
			auto text = WORD( "fuel_not_incremented" );
			if( text.empty() == false )
				MessageBox( text.c_str(), "" );
		}
	}

	ParamCollection pc;
	pc["event"] = "Video";
	pc["value"] = result ? "successful" : "failed";
	pc["place"] = "shop";
	pc["product"] = product;
	flurry::logEvent( pc );
}

int ShopLayer2::getBonusCount( const std::string & itemname )
{
	auto iter = _bonuses.find( itemname );
	if (iter == _bonuses.end())
	{
		assert( 0 );
		return 0;
	}
	int index = iter->second.index;
	return UserData::shared().bonusitem_count( index );
}

void ShopLayer2::setBonusesCount()
{
	int indexTab( 1 );
	while (true)
	{
		auto tab = getNodeByPath( this, "tab" + toStr( indexTab ) );
		if (!tab)
			break;
		++indexTab;
		for (auto child : tab->getChildren())
		{
			auto item = dynamic_cast<mlMenuItem*>(child);
			if (!item)
				continue;
			auto name = item->getName();
			auto countLabel = getNodeByPath<Text>( item, item->getParamCollection().get( "pathto_count" ) );
			if (countLabel)
			{
				int count = getBonusCount( name );
				countLabel->setString( "x" + toStr( count ) );
			}
		}
	}
}
	

NS_CC_END