#ifndef __ShopLayer2_h__
#define __ShopLayer2_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "ml/Singlton.h"
#include "inapp/purchase.h"
NS_CC_BEGIN



class ShopLayer2 : public LayerExt, public LayerBlur
{
public:
	struct Product
	{
		int count;
		std::string description;
		std::string price;
		bool isInapp;
		bool consumable;
	};

	class Dispatcher : public Singlton<Dispatcher>
	{
	public:
		void onCreate();
		void getDefault( const std::string& product );
	public:
		std::map< std::string, Product > products;
	};

public:
	static ccMenuCallback dispatch_query( const std::string & callbackDescription );
private:
	DECLARE_BUILDER( ShopLayer2 );
	/*
	 * index: 
	 * 0 - gold, fuel, items, 
	 * 1 - gold, gears, items, 
	 * tab: selected tab
	 */
	bool init( int index, int tab );
	//
public:
	virtual void onEnter()override;
	virtual void onExit()override;
	virtual bool setProperty( std::string const & name, std::string const & value ) override;
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override;
	virtual bool isBlurActive() override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;
	void openTab( int index );
protected:
	void loadDefaultValues();
	void checkGifts();
	void requestDetails();
	void details_ansfer( inapp::SkuDetails result );
	void details_dispather( float dt );
	void fetchItems();

	void purchase( const std::string& product );
	void purchaseItem( const std::string& itemname );
	void gift( const std::string& name );

	int getBonusCount( const std::string & itemname );
	void setBonusesCount();
public:
	static void purchaseResult( inapp::PurchaseResult result );
	static void purchaseResultSafeThread( inapp::PurchaseResult result );
	static void video( const std::string& product, LayerExt* instance );
	static void videoResult( bool result, const int typeScore, LayerExt* instance );
private:
	struct BonusItem
	{
		int cost;
		int index;
	};
	std::map< std::string, BonusItem > _bonuses;
	std::queue<inapp::SkuDetails> _queueDetails;
	LayerPointer _scoreLayer;
	bool _useDialog;
};




NS_CC_END
#endif // #ifndef ShopLayer2