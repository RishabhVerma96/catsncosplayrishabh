//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __ShopLayer_h__
#define __ShopLayer_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/ScrollMenu.h"
#include "inapp/Purchase.h"
#include "ml/ObServer.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN

#if PC != 1
enum eFreeType : char
{
    FREE_GOLD = 0,
    FREE_FUEL = 1,
    FREE_MEAT = 2,
    FREE_HEART = 3
};

class ShopLayer : public ScrollMenu, public NodeExt
{
public:
	struct Params
	{
		Params()
		:showFreeFuel(false)
		,showFreeGold(false)
		,showLevelScores(false)
		,autoClosingOnPurchase(false)
		{}
		
		bool showFreeFuel;
		bool showFreeGold;
		bool showLevelScores;
		bool autoClosingOnPurchase;
	};
	/*
	 * first - type scores
	 * first - count
	 */
	typedef ObServer< ShopLayer, std::function<void()> > Observer_OnClose;
	typedef std::function<void( int, int )> FunctionOnBuy;
	typedef ObServer<ShopLayer, FunctionOnBuy> ObserverOnPurchase;
	typedef ObServer<ShopLayer, std::function<void()>> ObserverOnFailed;
protected:
	DECLARE_BUILDER( ShopLayer );
	bool init( bool usefreeFuel, bool showFreeGold, bool asLevelScores, bool autoclose );
	bool init( const Params & params );
	bool loadDefaulParams();
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
public:
	Observer_OnClose& observerOnClose() { return _observerOnClose; }
	static ObserverOnFailed& observerOnFailed();
	static ObserverOnPurchase& observerOnPurchase();
	void onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event );

	static void request_answer( inapp::PurchaseResult result );
protected:
	MenuItemPointer buildItem( const std::string & id );
	//MenuItemPointer buildItemFreeFuel();
	void close( Ref*sender );

	void request( Ref * sender, const std::string & purchase );
	void show_video( Ref * sender, bool freeFuel );
	void show_video_started( bool success );
	void show_video_finished( bool successful, bool freeFuel );
	void show_video_gold( int reward );

	void details_ansfer( inapp::SkuDetails result );

	void push_details( inapp::SkuDetails result );
	void details_dispather( float dt );
    int getScoreType(eFreeType freeType);

	void _pause();
	void _resume();

	void fadeexit();
	void fadeenter();
private:
	float _scaleFactor;
	Point _zeroPosition;
	Observer_OnClose _observerOnClose;
	static ObserverOnFailed _observerOnFailed;
	static ObserverOnPurchase _observerOnPurchase;
	std::map< std::string, unsigned> _itemsValue;
	std::map< std::string, std::string> _itemsDescDefault;


	std::queue<inapp::SkuDetails> _queueDetails;
	std::mutex _queueDetailsMutex;
	bool _autoClosing;
    eFreeType _freeType;
	LayerExt::Pointer _blockLayer;
};
#else
class ShopLayer : public Layer
{
public:
	typedef std::function<void( int, int )> FunctionOnBuy;
	typedef ObServer<ShopLayer, FunctionOnBuy> ObserverOnPurchase;
	typedef ObServer<ShopLayer, std::function<void()>> ObserverOnFailed;
protected:
	DECLARE_BUILDER( ShopLayer );
	bool init( bool usefreeFuel, bool showFreeGold, bool asLevelScores, bool autoclose );
public:
	static ObserverOnPurchase& observerOnPurchase();
	static ObserverOnFailed& observerOnFailed();
private:
	static ObserverOnPurchase _observerOnPurchase;
	static ObserverOnFailed _observerOnFailed;
};
#endif



NS_CC_END
#endif // #ifndef ShopLayer
