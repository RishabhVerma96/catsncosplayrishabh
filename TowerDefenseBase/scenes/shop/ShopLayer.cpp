//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include <thread>
#include <chrono>

#include "ml/loadxml/xmlLoader.h"
#include "ml/ImageManager.h"
#include "ml/Audio/AudioEngine.h"
#include "ml/SmartScene.h"
#include "ml/loadxml/xmlProperties.h"

#include "ShopLayer.h"
#include "consts.h"
#include "resources.h"
#include "ScoreCounter.h"
#include "inapp/Purchase.h"
#include "UserData.h"
#include "flurry/flurry_.h"
#include "configuration.h"
#include "Tutorial.h"
#include "Language.h"
#include "plugins/AdsPlugin.h"
#include "game/unit/Hero.h"
#include "fyber/fyber.h"

NS_CC_BEGIN

#if PC != 1
const std::string kShopIcons( "images/shop/icons/" );

namespace
{
	ShopLayer * instance(nullptr);
};

ShopLayer::ObserverOnPurchase ShopLayer::_observerOnPurchase;
ShopLayer::ObserverOnFailed ShopLayer::_observerOnFailed;
ShopLayer::ObserverOnPurchase& ShopLayer::observerOnPurchase() { return _observerOnPurchase; }
ShopLayer::ObserverOnFailed& ShopLayer::observerOnFailed() { return _observerOnFailed; }

ShopLayer::ShopLayer()
: _scaleFactor( 1 )
, _zeroPosition()
, _observerOnClose()
, _itemsValue()
, _queueDetails()
, _queueDetailsMutex()
, _autoClosing( false )
{
	//assert( !instance );
	instance = this;
	setName( "shoplayer" );
}

ShopLayer::~ShopLayer()
{
	assert(instance);
	instance = nullptr;
	AdsPlugin::shared().observerVideoResult.remove( _ID );
}

bool ShopLayer::init( const Params & params )
{
	return init( params.showFreeFuel, params.showFreeGold, params.showLevelScores, params.autoClosingOnPurchase );
}

bool ShopLayer::init( bool usefreeFuel, bool showFreeGold, bool asLevelScores, bool autoclose )
{
    if( Config::shared().get<bool>( "useInapps" ) == false ) {
		return false;
    }

    if( Config::shared().get<bool>( "useFuel" ) == false ) {
        usefreeFuel = false;
    }
    _autoClosing = autoclose;

    do
    {
        auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
        CC_BREAK_IF( !ScrollMenu::init() );
        CC_BREAK_IF( !NodeExt::init() );
        
        loadDefaulParams();
        
        setCascadeOpacityEnabled( true );
        setCascadeColorEnabled( true );
        setKeyboardEnabled( true );
        
        NodeExt::load( "ini/shop/shop.xml" );
        
        auto bg = getChildByName( "bg" );
        if( bg )
        {
            auto size = bg->getContentSize();
            size.width = std::min( dessize.width, size.width );
            size.height += bg->getPositionY();
            float sx = dessize.width / size.width;
            _scaleFactor = sx;
            bg->setScale( _scaleFactor );
            
            _zeroPosition.x = dessize.width / 2;
            _zeroPosition.y = size.height / 2 * _scaleFactor - 10;            
			setPosition( _zeroPosition );
        }
        
        Size grid;
        Size content;
		std::list< std::string > items;

		if( Config::shared().get<std::string>( "online_store" ) != "" )
			items.push_back( "online_store" );

		if( usefreeFuel )
			items.push_back( "free_fuel" );
		if( !asLevelScores && Config::shared().get<bool>( "useFuel" ) )
			items.push_back( Config::shared().get( "inappFuel1" ) );
		
		if( showFreeGold && Config::shared().get<bool>( "useFreeGold" ) )
			items.push_back( "free_gold" );

		if( asLevelScores )
        {
            items.push_back( "free_heart" );
            items.push_back( "free_meat" );
			items.push_back( Config::shared().get( "inappGear2" ) );
			items.push_back( Config::shared().get( "inappGear3" ) );
        }
        else
        {
			items.push_back( Config::shared().get( "inappGold1" ) );
			items.push_back( Config::shared().get( "inappGold2" ) );
			items.push_back( Config::shared().get( "inappGold3" ) );
			items.push_back( Config::shared().get( "inappGold4" ) );
			items.push_back( Config::shared().get( "inappGold5" ) );
        }
        
		
		std::string gift = UserData::shared().get(k::user::ShopGift);
		if (gift.empty() == false)
        {
            items.push_front( gift );
        }

        for( auto& item : items )
        {
            MenuItemPointer menuitem = buildItem( item );
            
            addItem( menuitem );
            
            menuitem->setScale( _scaleFactor );
            
            grid = menuitem->getContentSize();
            grid.width *= _scaleFactor;
            grid.height *= _scaleFactor;
            content.width += grid.width;
            content.height = grid.height;
        }
        
        if( gift.empty() == false )
        {
            auto iter = std::find( items.begin(), items.end(), gift );
            assert( iter != items.end() );
            if( iter != items.end() )
                items.erase( iter );
        }
        
        for( auto& item : items )
        {
			if( item != "free_fuel" &&
				item != "free_gold" &&
                item != "free_meat" &&
                item != "free_heart" &&
				item != gift )
			{
				inapp::details( item, std::bind( &ShopLayer::details_ansfer, this, std::placeholders::_1 ) );
			}
        }
        
        setAlignedStartPosition( Point( -_zeroPosition.x, -275 ) * _scaleFactor );
        setGrisSize( grid );
        align( 99 );
        
        Size scissor( dessize.width, 464 );
        setScissorRect( getAlignedStartPosition(), scissor * _scaleFactor );
        setScissorEnabled( true );
        setScrollEnabled( true );
        setContentSize( content );
        setAllowScrollByY( false );
        
        if( content.width < scissor.width )
        {
            float diff = scissor.width - content.width;
            Point point = getAlignedStartPosition();
            point.x += diff / 2;
            setAlignedStartPosition( point );
            align( 99 );
        }
        
		auto menu = getChildByName( "menu" );
		if( menu )
		{
			auto close = menu->getChildByName<MenuItem*>( "close" );
			if( close )
			{
				close->setCallback( std::bind( &ShopLayer::close, this, std::placeholders::_1 ) );
				if( close->getPosition().equals( Point::ZERO ) )
				{
					close->setScale( _scaleFactor );
					Point pos = close->getPosition();
					pos.y *= _scaleFactor;
					pos.x = dessize.width / 2 - 35;
					close->setPosition( pos );
				}
			}
		}

		auto lambda = [this]( EventKeyboard::KeyCode key, Event* )mutable
		{
			if( key == EventKeyboard::KeyCode::KEY_BACK )
				this->fadeexit();
		};
		EventListenerKeyboard * event = EventListenerKeyboard::create();
		event->onKeyReleased = std::bind( lambda, std::placeholders::_1, std::placeholders::_2 );
		getEventDispatcher()->addEventListenerWithSceneGraphPriority( event, this );

        fadeenter();
        schedule( schedule_selector( ShopLayer::details_dispather ) );
        
        return true;
    }
    while (false);
    
    return false;
}

bool ShopLayer::loadDefaulParams()
{
	pugi::xml_document doc;
	doc.load_file( "ini/shop/default_params.xml" );
	auto root = doc.root().first_child();

	FOR_EACHXML( root, child )
	{
		std::string name = child.attribute( "name" ).as_string();
		std::string desc = child.attribute( "price" ).as_string();
		bool inapp = child.attribute( "inapp" ).as_bool( true );
		int value = child.attribute( "value" ).as_int();

		if( inapp )
		{
			_itemsValue[ Config::shared().get( "inappPackage" ) + name ] = value;
			_itemsDescDefault[Config::shared().get( "inappPackage" ) + name] = desc;
		}
		else
		{
			_itemsValue[ name ] = value;
			_itemsDescDefault[ name ] = desc;
		}
	}
	return root != nullptr;
}

ccMenuCallback ShopLayer::get_callback_by_description( const std::string & name )
{
	if( name == "close" )return std::bind( &ShopLayer::close, this, std::placeholders::_1 );
	return NodeExt::get_callback_by_description( name );
}

MenuItemPointer ShopLayer::buildItem( const std::string & id )
{
	auto gift = UserData::shared().get(k::user::ShopGift);
	if (gift == id)
	{
		auto p = Config::shared().get( "inappPackage" );
		xmlLoader::macros::set( "shopitem_caption", toStr( _itemsValue.at(id) ) );
		xmlLoader::macros::set( "shopitem_cost", _itemsDescDefault.at( id ) );
	}
	else
	{
		xmlLoader::macros::set( "shopitem_caption", toStr( _itemsValue.at( id ) ) );
		xmlLoader::macros::set( "shopitem_cost", _itemsDescDefault.at(id) );
	}

	std::string filename = id;
	if( Config::shared().get( "inappPackage" ).empty() == false &&
		filename.find( Config::shared().get( "inappPackage" ) ) == 0)
	{
		filename = id.substr( Config::shared().get( "inappPackage" ).size() );
	}
	std::string path = "ini/shop/" + filename + ".xml";
	MenuItemPointer item = xmlLoader::load_node<MenuItem>( path );

	xmlLoader::macros::erase( "shopitem_caption" );
	xmlLoader::macros::erase( "shopitem_cost" );

	auto button = getNodeByPath<MenuItem>( item, "menu/buy" );
	auto callback = std::bind( &ShopLayer::request, this, std::placeholders::_1, id );

	bool enabled;

	if(id != gift)
		enabled = requestInternetConnectionStatus();
	else
		enabled = true;



	item->setName( id );
	item->setEnabled( enabled );
	item->setCallback( callback );

	if( button )
	{
		button->setName( id );
		button->setEnabled( enabled);
		button->setCallback( callback );
	}
	return item;
}

void ShopLayer::push_details( inapp::SkuDetails result )
{
	_queueDetails.push( result );
}

void ShopLayer::details_dispather( float dt )
{
	while( _queueDetails.empty() == false )
	{
		inapp::SkuDetails details = _queueDetails.front();
		_queueDetails.pop();

		if( details.result == inapp::Result::Ok )
		{
			if( details.description.empty() == false )
				_itemsValue.at(details.productId) = strTo<int>( details.description );
			UserData::shared().write( k::user::PurchaseSavedValue + details.productId, details.description);
			
			auto item = getMenuItemByName( details.productId );
			if( item )
			{
				auto text = item->getChildByName<Text*>( "text" );
				auto menu = item->getChildByName<Menu*>( "menu" );
				auto button = menu ? menu->getChildByName<MenuItem*>( details.productId ) : nullptr;
				auto image = button ? button->getChildByName( "normal" ) : nullptr;
				auto cost = image ? image->getChildByName<Text*>( "cost" ) : nullptr;
				if( cost )
					cost->setString( details.priceText );
				if( text && details.description.empty() == false )
					text->setString( toStr(strTo<int>(details.description)) );
				item->setEnabled( true );
				button->setEnabled( true );
			}
		}
	}
}

void ShopLayer::details_ansfer( inapp::SkuDetails result )
{
	push_details( result );
}

void ShopLayer::close( Ref*sender )
{
	auto func = [this]()
	{
		retain();
		_observerOnClose.pushevent();
		removeFromParent();
		release();
	};

	runAction( Sequence::createWithTwoActions(
		DelayTime::create( 0.5f ),
		CallFunc::create( func )
		) );
	fadeexit();
}

void ShopLayer::request( Ref*sender, const std::string & purchase )
{
	std::string gift = UserData::shared().get(k::user::ShopGift);
	if (purchase == "free_gold")
	{
		if( Config::shared().get<bool>( "freeGoldAsVideo" ) )
        {
            _freeType = eFreeType::FREE_GOLD;
            show_video( sender, false );
        }
		else
			AdsPlugin::shared().showOfferWall();
	}
	else if( purchase == "free_fuel" )
	{
        _freeType = eFreeType::FREE_FUEL;
		show_video( sender, true );
	}
    else if( purchase == "free_meat" )
    {
        _freeType = eFreeType::FREE_MEAT;
        show_video( sender, true );
    }
    else if( purchase == "free_heart" )
    {
        _freeType = eFreeType::FREE_HEART;
        show_video( sender, true );
    }
	else if( purchase == "online_store" )
	{
		auto url = Config::shared().get( "online_store" );
		Application::getInstance()->openURL( url );
	}
	else if( purchase != gift )
	{
		_pause();
		inapp::purchase( purchase );
	}
	else
	{
		_pause();
		inapp::PurchaseResult result;
		result.errorcode = 0;
		result.productId = purchase;
		result.result = inapp::Result::Ok;
		request_answer( result );
		UserData::shared( ).write( k::user::ShopGift, std::string() );
		TutorialManager::shared().dispatch( "shop_getgift" );

		getItemByName( gift )->setVisible( false );
		if( _autoClosing == false )
			close(nullptr);
	}
}

void ShopLayer::request_answer( inapp::PurchaseResult aresult )
{
	const inapp::PurchaseResult result = aresult;
	CCLOG( "ShopLayer::request_answer" );

	if( result.result == inapp::Result::Fail || result.result == inapp::Result::Canceled )
	{
		_observerOnFailed.pushevent();
	}
	while( result.result == inapp::Result::Ok || result.result == inapp::Result::Restored )
	{
		CCLOG( "ShopLayer::request_answer Ok" );
		std::string purchase = result.productId;
		bool dispatch( true );

		int type = -1;
		int value(0);

		bool gift( false );
        
		if( purchase.find( Config::shared().get( "inappPackage" ) + "hero") == 0 )
		{
			std::string hero;
            if( Config::shared().get( "inappPackage" ).empty() == false )
                hero = purchase.substr(Config::shared().get( "inappPackage" ).size());
			else
				hero = purchase;
			
			HeroExp::shared().heroBought( hero );
			_observerOnPurchase.pushevent( 0, 0 );
			break;
		}
		if( purchase == Config::shared().get( "inappPackHeroes1" ) )
		{
			auto startIndex = HeroExp::shared().getNonPurchasableHeroesCount();
			auto count = Config::shared().get<int>( "heroesCount" );
			for( int i = startIndex; i < count; ++i )
				HeroExp::shared().heroBought( "hero" + toStr( i + 1 ) );
			_observerOnPurchase.pushevent( 0, 0 );
			ScoreCounter::shared().addMoney( kScoreCrystals, 1000, true );
			break;
		}
		else if( purchase == Config::shared().get( "inappFuel1" ) ) type = kScoreFuel;
		else if( purchase == Config::shared().get( "inappGold1" ) ) type = kScoreCrystals;
		else if( purchase == Config::shared().get( "inappGold2" ) ) type = kScoreCrystals;
		else if( purchase == Config::shared().get( "inappGold3" ) ) type = kScoreCrystals;
		else if( purchase == Config::shared().get( "inappGold4" ) ) type = kScoreCrystals;
		else if( purchase == Config::shared().get( "inappGold5" ) ) type = kScoreCrystals;
		else if( purchase == Config::shared().get( "inappGear1" ) ) type = kScoreLevel;
		else if( purchase == Config::shared().get( "inappGear2" ) ) type = kScoreLevel;
		else if( purchase == Config::shared().get( "inappGear3" ) ) type = kScoreLevel;
		else if( purchase == "gift_gear" ) { type = kScoreLevel; gift = true; }
		else if( purchase == "gift_gold" ) { type = kScoreCrystals; gift = true; }
		else if( purchase == "gift_fuel" ) { type = kScoreFuel; gift = true; }
		else
		{
			CCLOG( "product not defined [%s]", purchase.c_str() );
			dispatch = false;
		}
		
		if( instance )
		{
			if( gift )
			{
				auto iter = instance->_itemsValue.find( purchase );
				if( iter == instance->_itemsValue.end() )
					iter = instance->_itemsValue.find( Config::shared().get( "inappPackage" ) + purchase );
				value = iter->second;
			}
			else
				value = instance->_itemsValue.at(purchase);
		}
		else
		{
			value = UserData::shared().get <int>(k::user::PurchaseSavedValue + purchase);
		}

		if( dispatch )
		{
			assert( type != -1 );
			if( purchase == Config::shared().get( "inappFuel1" ) )
			{
				UserData::shared().write( k::user::MaxFuelValue, 250 );
				ScoreByTime::shared().checkMaxValue();
				ScoreCounter::shared().addMoney( kScoreFuel, value, true );
			}
			else
			{
				ScoreCounter::shared().addMoney( type, value, type != kScoreLevel );
			}

			if( gift == false )
			{
				std::string key = k::user::BoughtScores + toStr( type );
				int boughtScores = UserData::shared().get <int>(key, 0) + value;
				UserData::shared().write(key, boughtScores);
				UserData::shared().save();
				inapp::confirm( purchase );
			}
			_observerOnPurchase.pushevent( type, value );
		}

		auto call = CallFunc::create( []()
		{
			AudioEngine::shared().playEffect( kSoundShopPurchase );
		} );
		if( instance )
			instance->runAction( Sequence::createWithTwoActions( DelayTime::create( 0.1f ), call ) );
		break;
	}
	

	if( instance )
		instance->_resume();

	ParamCollection pc;
	pc["event"] = "Purchase";
	pc["level"] = toStr( UserData::shared().level_getCountPassed() );
	pc["errormsg"] = result.errormsg;
	pc["errorсode"] = toStr(result.errorcode);
	if( result.result == inapp::Result::Ok )
		pc["value"] = result.productId;
	else if( result.result == inapp::Result::Fail )
		pc["value"] = "failed";
	else
		pc["value"] = "canceled";

	flurry::logEvent( pc );

	if( instance && instance->_autoClosing && result.result == inapp::Result::Ok )
	{
		instance->close( nullptr );
	}
	UserData::shared().save();
}

void ShopLayer::show_video( Ref * sender, bool freeFuel )
{
	_pause();
    AdsPlugin::shared().observerVideoResult.add( _ID, std::bind( &ShopLayer::show_video_finished, this, std::placeholders::_1, true ) );
    AdsPlugin::shared().showVideo();
}

void ShopLayer::show_video_started( bool success )
{
}

void ShopLayer::show_video_finished( bool successful, bool freeFuel1 )
{
	_resume();
	ParamCollection pc;
	pc["event"] = "Video";
	pc["value"] = successful ? "successful" : "failed";
    if (_freeType == eFreeType::FREE_GOLD)
    {
        pc["product"] = "free_gold";
    }
    else if (_freeType == eFreeType::FREE_FUEL)
    {
        pc["product"] = "free_fuel";
    }
    else if (_freeType == eFreeType::FREE_MEAT)
    {
        pc["product"] = "free_meat";
    }
    else if (_freeType == eFreeType::FREE_HEART)
    {
        pc["product"] = "free_heart";
    }
	pc["place"] = "shop";
	flurry::logEvent( pc );

	if( successful )
	{
        const int scoreType = getScoreType(_freeType);

		int count = ScoreCounter::shared().getMoney( scoreType );
		int increase = 10;
		if( _freeType == FREE_FUEL )
		{
			auto iter = _itemsValue.find( "free_fuel" );
			if( iter != _itemsValue.end() && iter->second > 0 )
			{
				increase = iter->second;
			}

			if( count >= ScoreByTime::shared().max_fuel() )
			{
				auto text = WORD( "fuel_not_incremented" );
				if( text.empty() == false )
					MessageBox( text.c_str(), "" );
			}
			int ncount = count + increase;
			ncount = std::min( ncount, ScoreByTime::shared().max_fuel() );
			ncount = std::max( ncount, count );
			ScoreCounter::shared().setMoney( scoreType, ncount, true );
		}
		else if ( _freeType == FREE_GOLD )
		{
			auto iter = _itemsValue.find("free_gold");
			if (iter != _itemsValue.end() && iter->second > 0)
				increase = iter->second;

			ScoreCounter::shared().addMoney(scoreType, increase, true);
		}
        else if ( _freeType == FREE_MEAT )
        {
            auto iter = _itemsValue.find("free_meat");
            if (iter != _itemsValue.end() && iter->second > 0)
                increase = iter->second;
            
            ScoreCounter::shared().addMoney(scoreType, increase, true);
        }
        else if ( _freeType == FREE_HEART )
        {
            auto iter = _itemsValue.find("free_heart");
            if (iter != _itemsValue.end() && iter->second > 0)
                increase = iter->second;
            
            ScoreCounter::shared().addMoney(scoreType, increase, true);
        }

		AdsPlugin::shared().cacheVideo();

	}
	else
	{
		if( AdsPlugin::shared().isVideoAvailabled() == false )
		{
			cocos2d::MessageBox( "No ads available. Please try again later.", "" );
			return;
		}
	}
}

int ShopLayer::getScoreType(eFreeType freeType)
{
    if (freeType == FREE_FUEL)
    {
        return kScoreFuel;
    }
    else if (freeType == FREE_GOLD)
    {
        return kScoreCrystals;
    }
    else if (freeType == FREE_MEAT)
    {
        return kScoreLevel;
    }
    else
    {
        return kScoreHealth;
    }
}

void ShopLayer::show_video_gold( int reward )
{
	AudioEngine::shared().resumeAllEffects();
	AudioEngine::shared().resumeBackgroundMusic();
    ScoreCounter::shared().addMoney( kScoreCrystals, reward, true );
}

void ShopLayer::_pause()
{

	AudioEngine::shared().pauseAllEffects();
	AudioEngine::shared().pauseBackgroundMusic();
	SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
	_blockLayer = LayerExt::create();
	auto sprite = ImageManager::shared().sprite("images/loading.png");
	_blockLayer->addChild(sprite);
	sprite->runAction(RepeatForever::create(RotateBy::create(1, 360)));
	sprite->setPosition( Point(Director::getInstance()->getOpenGLView()->getDesignResolutionSize()/2) );
	if( scene )
	{
		scene->pushLayer( _blockLayer, true );
		auto action = Sequence::create( DelayTime::create( 10 ), RemoveSelf::create(), nullptr );
		_blockLayer->runAction( action );

	}
}

void ShopLayer::_resume()
{
	AudioEngine::shared().resumeAllEffects();
	AudioEngine::shared().resumeBackgroundMusic();
	if( _blockLayer )
	{
		_blockLayer->removeFromParent();
		_blockLayer.reset(nullptr);
	}
}

void ShopLayer::fadeexit()
{
	if( runEvent( "disappearance" ) == false )
	{
		static auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
		auto action = EaseBackIn::create( MoveTo::create( 0.5f, _zeroPosition + Point( 0, -dessize.height ) ) );
		runAction( action );
	}
	AudioEngine::shared().playEffect( kSoundShopHide );
}

void ShopLayer::fadeenter()
{
	if( runEvent( "appearance" ) == false )
	{
		static auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
		setPosition( _zeroPosition + Point( 0, -dessize.height ) );
		auto action = EaseBackOut::create( MoveTo::create( 0.5f, _zeroPosition ) );
		runAction( action );
	}
	AudioEngine::shared().playEffect( kSoundShopShow );
}

void ShopLayer::onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event )
{
	if( keyCode == EventKeyboard::KeyCode::KEY_BACK )
		close( nullptr );
}
#else // PC == 1

ShopLayer::ObserverOnPurchase ShopLayer::_observerOnPurchase;
ShopLayer::ObserverOnFailed ShopLayer::_observerOnFailed;
ShopLayer::ObserverOnPurchase& ShopLayer::observerOnPurchase() { return _observerOnPurchase; }
ShopLayer::ObserverOnFailed& ShopLayer::observerOnFailed() { return _observerOnFailed; }

ShopLayer::ShopLayer()
{
}

ShopLayer::~ShopLayer()
{
}

bool ShopLayer::init( bool usefreeFuel, bool showFreeGold, bool asLevelScores, bool autoclose )
{	
	return false;
}


#endif // PC
NS_CC_END
