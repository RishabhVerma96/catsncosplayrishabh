#include "LaunchLevelLayer2.h"
#include "ml/loadxml/xmlLoader.h"
#include "ml/loadxml/xmlProperties.h"
#include "ml/ImageManager.h"
#include "ml/Language.h"
#include "Services/playservices/playservices.h"
#include "support.h"
#include "UserData.h"
#include "MapLayer.h"
#include "ScoreCounter.h"
#include "ShopLayer.h"
#include "ShopLayer2.h"
#include "selecthero/HeroRoom2.h"
#include "consts.h"
NS_CC_BEGIN





LaunchLevelLayer2::LaunchLevelLayer2()
: _currentMode(GameMode::normal)
, _currentLevel(-1)
{
}

LaunchLevelLayer2::~LaunchLevelLayer2( )
{
}

bool LaunchLevelLayer2::init( int indexlevel, bool isSurvival )
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		CC_BREAK_IF( !LayerBlur::init() );

		_currentMode = isSurvival ? GameMode::survival : GameMode::normal;
		_currentLevel = indexlevel;

		int type, costHard, costNormal;
		LevelParams::shared().getPayForLevel( indexlevel, GameMode::normal, type, costNormal );
		LevelParams::shared().getPayForLevel( indexlevel, GameMode::hard, type, costHard );

		int goldNorm = LevelParams::shared().getAwardGold( indexlevel, 3, GameMode::normal );
		int goldHard = LevelParams::shared().getAwardGold( indexlevel, 1, GameMode::hard );
		int ticketsNorm = LevelParams::shared().getTickets( indexlevel, GameMode::normal );
		int ticketsHard = LevelParams::shared().getTickets( indexlevel, GameMode::hard );
		std::string caption = WORD( "gamechoose_level" ) + toStr( indexlevel + 1 );
		int starsNormal = UserData::shared().level_getScoresByIndex( indexlevel );
		int starsHard = UserData::shared().get<int>(k::user::LevelStars + toStr(indexlevel));
		bool withHeroes = indexlevel >= Config::shared().get<int>( "minLevelHero" );
		withHeroes = withHeroes && Config::shared().get<bool>( "useHero" );

		xmlLoader::macros::set( "levelindex", toStr( indexlevel + 1 ) );
		xmlLoader::macros::set( "cost_normalmode", toStr( costNormal ) );
		xmlLoader::macros::set( "cost_hardmode", toStr( costHard ) );
		xmlLoader::macros::set( "gold_normalmode", toStr( goldNorm ) );
		xmlLoader::macros::set( "gold_hardmode", toStr( goldHard ) );
		xmlLoader::macros::set( "tickets_normalmode", toStr( ticketsNorm ) );
		xmlLoader::macros::set( "tickets_hardmode", toStr( ticketsHard ) );
		xmlLoader::macros::set( "preview_caption", caption );
		xmlLoader::macros::set( "use_fuel", toStr( Config::shared().get<bool>( "useFuel" ) ) );
		xmlLoader::macros::set( "unuse_fuel", toStr( !Config::shared().get<bool>( "useFuel" ) ) );
		xmlLoader::macros::set( "with_heroes", toStr( withHeroes ) );

		int starsMax = LevelParams::shared().getMaxStars( indexlevel, GameMode::normal );
		for (int i = 0; i < starsMax; ++i)
		{
			xmlLoader::macros::set( "star" + toStr( i + 1 ) + "_visible", toStr( i < starsNormal ) );
		}

		xmlLoader::macros::set( "starhard_visible", toStr( starsHard >= 3 ) );

		NodeExt::load( isSurvival == false ? "ini/map/choose2.xml" : "ini/map/choose2_survival.xml" );
		fetch();
		
		EventListenerKeyboard * event = EventListenerKeyboard::create();
		event->onKeyReleased = std::bind( [this]( EventKeyboard::KeyCode key, Event* )mutable
		{
			if( key == EventKeyboard::KeyCode::KEY_BACK )
				this->disappearance();
		}, std::placeholders::_1, std::placeholders::_2 );

		getEventDispatcher()->addEventListenerWithSceneGraphPriority( event, this );
		runEvent( "appearance" );
        
        if( isSurvival )
        {
            if( PlayServises::isConnected() == false )
                PlayServises::init(true);
        }
        
		return true;
	}
	while( false );
	return false;
}

void LaunchLevelLayer2::onEnter()
{
	LayerExt::onEnter();
	fetch();
}

void LaunchLevelLayer2::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
}

bool LaunchLevelLayer2::isBlurActive()
{
	return !isRunning();
}

void LaunchLevelLayer2::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

ccMenuCallback LaunchLevelLayer2::get_callback_by_description( const std::string & name )
{
	if( name == "normal" ) return std::bind( &LaunchLevelLayer2::cb_gamemode, this, false );
	if( name == "hard" ) return std::bind( &LaunchLevelLayer2::cb_gamemode, this, true );
	if( name == "close" ) return std::bind( &LaunchLevelLayer2::disappearance, this );
	if( name == "leaderboards_level" ) return std::bind( [this](){ Leaderboard::shared().openLevel( _currentLevel ); } );
	if( name == "leaderboards_total" ) return std::bind( [this](){ Leaderboard::shared().openGLobal(); } );

	return LayerExt::get_callback_by_description( name );
}

void LaunchLevelLayer2::fetch()
{
	auto normal = getNodeByPath( this, "normal" );
	auto hard = getNodeByPath( this, "hard" );
	auto normalbutton = getNodeByPath( this, "caption/normal" );
	auto hardbutton = getNodeByPath( this, "caption/hard" );

	if( normal )
		normal->setVisible( _currentMode == GameMode::normal );
	if( normalbutton )
		normalbutton->setVisible( _currentMode == GameMode::normal );
	if( hard )
		hard->setVisible( _currentMode == GameMode::hard );
	if( hardbutton )
		hardbutton->setVisible( _currentMode == GameMode::hard );
}

void LaunchLevelLayer2::disappearance()
{
	this->runEvent( "disappearance" );
}

void LaunchLevelLayer2::cb_gamemode( bool hard )
{
	_currentMode = hard ? GameMode::hard : GameMode::normal;
	fetch();
}


NS_CC_END