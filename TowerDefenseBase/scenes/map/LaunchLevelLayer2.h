#ifndef __LaunchLevelLayer2_h__
#define __LaunchLevelLayer2_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "game/gameboard.h"
NS_CC_BEGIN





class LaunchLevelLayer2 : public LayerExt, public LayerBlur
{
	DECLARE_BUILDER( LaunchLevelLayer2 );
	bool init( int indexlevel, bool isSurvival );
public:
	virtual void onEnter()override;
	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override;
	virtual bool isBlurActive()override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override;
	virtual ccMenuCallback get_callback_by_description( const std::string & name ) override;
protected:
	void fetch();
	void disappearance();
	void cb_gamemode( bool hard );
private:
	GameMode _currentMode;
	int _currentLevel;
};




NS_CC_END
#endif // #ifndef LaunchLevelLayer2