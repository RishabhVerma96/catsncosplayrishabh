//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "MapLayer.h"
#include "UserData.h"
#include "ml/ImageManager.h"
#include "ml/MenuItem.h"
#include "ml/Audio/AudioEngine.h"
#include "ml/loadxml/xmlProperties.h"
#include "plugins/AdsPlugin.h"
#include "ShopLayer.h"
#include "ShopLayer2.h"
#include "GameLayer.h"
#include "ScoreCounter.h"
#include "resources.h"
#include "support.h"
#include "lab/Laboratory.h"
#include "lab/Laboratory2.h"
#include "DailyReward.h"
#include "ScoreLayer.h"
#include "Tutorial.h"
#include "configuration.h"
#include "consts.h"
#include "tower.h"
#include "scenes/itemshop/ItemShop.h"
#include "scenes/popshop/PopShop.h"
#include "selecthero/SelectHero.h"
#include "selecthero/HeroRoom2.h"
#include "RateMeLayer.h"
#include "Language.h"
#include "MenuItem.h"
#include "LoadLevelScene.h"
#include "BuyHeroes.h"
#include "MenuItemTextBG.h"
#include "AutoPlayer.h"
#include "playservices/playservices.h"
#include "RouleteLayer.h"
#include "SurvivalLayer.h"
#include "scenes/online/LoginLayer.h"
#include "LaunchLevelLayer.h"
#include "LaunchLevelLayer2.h"
#include "SettingsLayer.h"
#include "SlotMachineLayer.h"
#include "DialogLayer.h"
#include "scenes/gamegs/GamePauseScene.h"
#include "ShareLayer.h"
#include "ml/CutSceneHandler.h"
#include "LocalNotification.h"
#include "SpecialUpgrades.h"

NS_CC_BEGIN

namespace
{
	int getMsFromLastCall()
	{
		static clock_t lastCall = clock();
		clock_t thisCall = clock();
		clock_t diff = thisCall - lastCall;
		lastCall = thisCall;
		return diff > 0 ? (diff * 1000 / CLOCKS_PER_SEC) : 1000;
	}

	std::string const xmlFogOfWar = "ini/map/fog_of_war.xml";
}

auto setKeyDispatcher = []( LayerExt* layer )
{
	EventListenerKeyboard * event = EventListenerKeyboard::create();
	event->onKeyReleased = std::bind(
									 [layer](EventKeyboard::KeyCode key, Event*)mutable
									 {
										 if( key == EventKeyboard::KeyCode::KEY_BACK )
											 layer->runEvent( "onexit" );
									 }, std::placeholders::_1, std::placeholders::_2);
	
	layer->getEventDispatcher()->addEventListenerWithSceneGraphPriority(event, layer);
};


SmartScene::Pointer MapLayer::scene()
{
	do
	{
		auto layer = MapLayer::create();
		auto score = ScoreLayer::create();
		CC_BREAK_IF( !layer );
		CC_BREAK_IF( !score );
		auto scene = SmartScene::create( layer );
		scene->setName( "MapScene" );
		scene->addChild( score, 999 );
		
		return scene;
	}
	while( false );
	return nullptr;
}

void MapLayer::prepairNodeByConfiguration( NodeExt* nodeext )
{
	if( nodeext == nullptr )
		return;
	auto node = nodeext->as_node_pointer();
	auto pathto_leaderboards = nodeext->getParamCollection().get( "pathto_leaderboards", "unknowpath" );
	
	auto leaderboards = getNodeByPath( node, pathto_leaderboards );
	if( leaderboards )
		leaderboards->setVisible( Config::shared().get<bool>( "useLeaderboards" ) );
}

MapLayer::MapLayer()
: _updateLocations( true )
, tutorialDispatched( false )
, _showLaboratoryOnEnter( false )
, _selectedLevelIndex( -1 )
, _isTouching( false )
, _useDialog( false )
, _doNotHideBanner( false )
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
, _editMode(false)
, _editPoint(nullptr)
#endif
{
	setName( "maplayer" );
}

MapLayer::~MapLayer()
{
	MouseHoverScroll::shared().setNode( nullptr );
	MouseHoverScroll::shared().setScroller( nullptr );
}

bool MapLayer::init()
{
	do
	{
		CC_BREAK_IF( !LayerExt::init() );
		CC_BREAK_IF( !LayerBlur::init() );
		initBlockLayer( "images/loading.png" );
		setKeyboardEnabled( true );
		xmlLoader::macros::set( "adsvideo_availabled", toStr( AdsPlugin::shared().isVideoAvailabled() ) );

		NodeExt::load( "ini/map", "maplayer.xml" );
		prepairNodeByConfiguration( this );
		
		_map = getChildByName( "map" );
		CC_BREAK_IF( !_map );
	
		_scrollInfo = make_intrusive<ScrollTouchInfo>();
		_scrollInfo->node = _map;
		
		_menuLocations = ScrollMenu::create();
		_menuLocations->setName( "locations" );
		_menuLocations->setPosition( Point::ZERO );
		_map->addChild( _menuLocations, 1 );

		auto desSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
		float scale = desSize.height / _map->getContentSize().height;
		_map->setScale( scale );
		
		removeUnUsedButtons();
		
		auto touchL = EventListenerTouchAllAtOnce::create();
		touchL->onTouchesBegan = CC_CALLBACK_2( MapLayer::scrollBegan, this );
		touchL->onTouchesMoved = CC_CALLBACK_2( MapLayer::scrollMoved, this );
		touchL->onTouchesEnded = CC_CALLBACK_2( MapLayer::scrollEnded, this );
		_eventDispatcher->addEventListenerWithSceneGraphPriority( touchL, this );
		
		_fogOfWarEnabled = FileUtils::getInstance()->isFileExist( xmlFogOfWar );
		activateLocations();

		createDevMenu();
    
        if(OPEN_ALL_LEVELS) {
            this->openAllLevels();
        }
        
		return true;
	}
	while( false );
	return false;
}

void MapLayer::displayLeaderboardScore()
{
	auto pathto_score = getParamCollection().get( "pathto_leaderboardsscore", "unknowpath" );
	auto scoreNode = getNodeByPath<Text>( this, pathto_score );
	if( scoreNode )
	{
		auto score = Leaderboard::shared().getScoreGlobal();
		auto string = toStr( score );
		size_t k = string.size();
		while( k > 3 )
		{
			k -= 3;
			string.insert( k, " " );
		}

		scoreNode->setString( WORD("leaderboard_score") + string );
	}
}

void MapLayer::removeUnUsedButtons()
{
	auto menu = getChildByName( "menu" );
	if( menu )
	{
		auto shop = menu->getChildByName<MenuItem*>( "shop" );
		auto paid = menu->getChildByName<MenuItem*>( "paid" );
		auto heroes = menu->getChildByName<MenuItem*>("heroes");
		
		if( paid&& Config::shared().get<bool>( "useLinkToPaidVersion" ) == false )
			paid->setVisible( false );
		if( shop&& Config::shared().get<bool>( "useInapps" ) == false ) {
			shop->setVisible( false );
			auto ishop = menu->getChildByName<MenuItem*>("itemshop");
			if( ishop )
				ishop->setPosition(shop->getPosition());
		}
		if (heroes&& Config::shared().get<bool>( "useHero" ) == false)
			heroes->setVisible(false);
	}
}

void MapLayer::onEnter()
{
	LayerExt::onEnter();

	this->scheduleUpdate();
#if PC == 1
	MouseHoverScroll::shared().setScroller( _scrollInfo );
	MouseHoverScroll::shared().setNode( _map );
	MouseHoverScroll::shared().enable();
#endif
	
	AudioEngine::shared().playMusic( kMusicMap );
	//else if( _updateLocations )
	//{
	//	activateLocations();
	//}
	
	if (Config::shared().get<bool>( "useInapps" ) == false) {
		SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
		auto scores = scene->getChildByName("scorelayer");
		if(scores)
		{
			auto menu = scores->getChildByName("menu");
			auto shop = menu ? menu->getChildByName( "shop" ) : nullptr;
			if (shop) {
				shop->setVisible(false);
				shop->setPositionY(-9999);
			}
		}
	}
	
	int result = UserData::shared().get <int>(k::user::LastGameResult, k::user::GameResultValueNone);
	if (result == k::user::GameResultValueWin)
		activateLocations();
	
	if( _menuLocations )
		_menuLocations->setEnabled( true );
	
	
	auto notifyOnEnter = []() {
		TutorialManager::shared().dispatch( "map_onenter" );
	};
	auto notifyGameResult = [this](){
		
		bool dispatched( false );
        
		int countlose = UserData::shared().get <int>("lose_counter", 0);
		int result = UserData::shared().get <int>(k::user::LastGameResult, k::user::GameResultValueNone);

		if (result == k::user::GameResultValueWin)
		{
			countlose = 0;

			std::list<std::string> towers;
			mlTowersInfo::shared().fetch( towers );
			std::string towername = towers.front();
            
            int skillpointCount = HeroExp::shared().skillPoints( "hero1" );
			int level = UserData::shared().tower_upgradeLevel( towername );
			int scores = ScoreCounter::shared().getMoney( kScoreCrystals );
			int cost = mlTowersInfo::shared().getCostLab( towername, level + 1 );
            int levelPlaying = UserData::shared().get<int>(kLevelPlaying);
            
            if(levelPlaying == 2) {
                dispatched = TutorialManager::shared().dispatch( "map_afterwin"+ toStr(levelPlaying) );
                tutorialDispatched = dispatched;
            }
            else if (levelPlaying == 1 && skillpointCount >= 1) {
                dispatched = TutorialManager::shared().dispatch( "map_afterwin"+ toStr(levelPlaying) );
                tutorialDispatched = dispatched;
            }
            else if(level < 3&& cost <= scores )
			{
                if(level <= 1) {
                    dispatched = TutorialManager::shared().dispatch( "map_afterwin"+ toStr(levelPlaying) );
                    tutorialDispatched = dispatched;
                }
                else {
                    dispatched = TutorialManager::shared().dispatch( "map_afterwin" );
                }
				AdsPlugin::shared().hideBanner();
			}
            
			if( dispatched == false )
			{
				dispatched = TutorialManager::shared().dispatch( "map_showheroroom" );
			}
			if( dispatched == false ) {
				if (Config::shared().get<bool>( "useInapps" )) {
					//prevent showing lab when shop is not able
					dispatched = TutorialManager::shared().dispatch( "map_afterwin_force" );
				} else {
					dispatched = true;
				}
			}
		}
		if( result == k::user::GameResultValueFail )
		{
			countlose++;
			dispatched = TutorialManager::shared().dispatch( "map_afterlose" );
		}
		if( countlose > 0 )
		{
			if( TutorialManager::shared().dispatch( "map_losenumber" + toStr( countlose ) ) )
			{
				AdsPlugin::shared().hideBanner();
				dispatched = true;
				countlose = 0;
			}
			UserData::shared().write( "lose_counter", countlose );
		}
		UserData::shared().write( k::user::LastGameResult, k::user::GameResultValueNone );
		
        bool isLevelWon = LevelParams::shared().isLastLevelWon();
		if( !dispatched  && !tutorialDispatched)
		{
			if( _showLaboratoryOnEnter && !isLevelWon)
			{
				_showLaboratoryOnEnter = false;
				auto run = [this]()
				{
					bool maxlevel = true;
					std::list<std::string> towers;
					mlTowersInfo::shared().fetch( towers );
					for( auto tower : towers )
						maxlevel = maxlevel&& UserData::shared().tower_upgradeLevel( tower ) == 5;
					if( !maxlevel )
					{
						cb_lab( nullptr );
					}
				};
				runAction( CallFunc::create( run ) );
			}
			
			showDailyReward();
		}
	};
	
	runAction( CallFunc::create( notifyOnEnter ) );
	
	int levelResult = UserData::shared().get <int>(k::user::LastGameResult, k::user::GameResultValueNone);
	bool leveFinished =
		levelResult == k::user::GameResultValueWin ||
		levelResult == k::user::GameResultValueFail;
	if (leveFinished)
	{
		runAction( CallFunc::create( notifyGameResult ) );
	}
	
//    if (levelResult == k::user::GameResultValueWin) {
//        openRateMeWindowIfNeeded();
//    }
	if (UserData::shared().get <int>(kUser_passedLevels) > 1)
	{
		showDailyReward();
	}
	
	displayLeaderboardScore();
	createPromoMenu();
	xmlLoader::macros::set( "adsvideo_availabled", toStr( AdsPlugin::shared().isVideoAvailabled() ) );

	for( auto & network : ShareLayer::networks )
	{
		bool shared = UserData::shared().get<bool>( k::user::SharePrefix + network, false );
		auto networkNode = getNodeByPath( this, "menu_add/" + network );
		if( networkNode )
			networkNode->setVisible( shared == false );
	}

	AdsPlugin::shared().showBanner();
	if( _doNotHideBanner ) _doNotHideBanner = false;
}

void MapLayer::onExit()
{
	LayerExt::onExit();
	this->unscheduleUpdate();
#if PC == 1
	MouseHoverScroll::shared().disable();
#endif
	if( _doNotHideBanner == false )
		AdsPlugin::shared().hideBanner();
}

void MapLayer::load( pugi::xml_node& root )
{
	NodeExt::load( root );
	
	for( auto xmlLocation : root.child( "locations" ) )
		_locations.push_back( loadLocation( xmlLocation ) );
	for( auto xmlLocation : root.child( "locations_survival" ) )
		_locationsSurvival.push_back( loadLocation( xmlLocation ) );

	UserData::shared().write( kUser_locationsCount, (int)_locations.size() );
}

MapLayer::Location MapLayer::loadLocation( const pugi::xml_node& node )
{
	Location loc;
	loc.pos = strTo<Point>( node.attribute( "pos" ).as_string() );
	loc.posLock = strTo<Point>( node.attribute( "poslock" ).as_string() );
	loc.a = strTo<Point>( node.attribute( "controlA" ).as_string() );
	loc.b = strTo<Point>( node.attribute( "controlB" ).as_string() );
    loc.skip = node.attribute( "skip" ).as_bool( false );
	loc.starsForUnlock = node.attribute( "stars" ).as_int( 0 );
	loc.afterLevel = node.attribute( "afterlevel" ).as_int( -1 );
	loc.unlockFrame = node.attribute( "unlockframe" ).as_string();
	loc.unlockText = node.attribute( "unlocktext" ).as_string();
	loc.survivalMode = node.attribute( "survival" ).as_bool( false );
	return loc;
}

bool MapLayer::setProperty( const std::string & stringproperty, const std::string & value )
{
	if( stringproperty == "usedialog" )
		_useDialog = strTo<bool>( value ) && Config::shared().get<bool>( "useDialogs" );
	else return LayerExt::setProperty( stringproperty, value );
	return true;
}



ccMenuCallback MapLayer::get_callback_by_description( const std::string& name )
{
	if( name == "back" )
		return CC_CALLBACK_1( MapLayer::cb_back, this );
    else if( name == "specialupgrades" )
        return CC_CALLBACK_1( MapLayer::cb_specialUpgrades, this );
	else if( name == "laboratory" )
		return CC_CALLBACK_1( MapLayer::cb_lab, this );
	else if( name == "laboratory2" )
		return CC_CALLBACK_1( MapLayer::cb_lab2, this );
	else if( name == "itemshop" )
		return CC_CALLBACK_1( MapLayer::cb_itemshop, this );
    //Rishabh Code
    else if(name == "popshop")
        return CC_CALLBACK_1(MapLayer::cb_popshop, this );
    //
    
	else if( name == "shop" )
		return std::bind(&MapLayer::cb_shop, this, std::placeholders::_1, 1, 1 );
	else if( name == "heroes" || name == "heroes2" )
	{
		auto cb = [this](Ref*, int heroroomindex)
		{
			LayerPointer layer;
			if( heroroomindex == 1 )
				layer = SelectHero::create();
			else if( heroroomindex == 2 )
				layer = HeroRoom2::create();

			if( layer )
			{
				SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
				scene->pushLayer( layer, true );
				TutorialManager::shared().dispatch( "map_openheroes" );
			}
		};
		int index = name == "heroes" ? 1 : 2;
		return std::bind( cb, std::placeholders::_1, index );
	}
	else if( name == "paidversion" )
		return CC_CALLBACK_1( MapLayer::cb_paidversion, this );
	else if( name == "pushgame_normalmode" )
		return std::bind(&MapLayer::cb_game, this, std::placeholders::_1, GameMode::normal );
	else if( name == "pushgame_hardmode" )
		return std::bind(&MapLayer::cb_game, this, std::placeholders::_1, GameMode::hard );
	else if( name == "pushgame_survivalmode" )
		return std::bind(&MapLayer::cb_game, this, std::placeholders::_1, GameMode::survival );
	else if( name == "unlock" )
		return std::bind(&MapLayer::cb_unlock, this, std::placeholders::_1 );
	else if( name == "leaderboard" )
		return std::bind( []( Ref* ){Leaderboard::shared().openGLobal(); }, std::placeholders::_1 );
	else if( name == "leaderboard_level" )
		return std::bind( [this]( Ref* ){Leaderboard::shared().openLevel( _selectedLevelIndex ); }, std::placeholders::_1 );
	else if( name == "roulete" )
		return std::bind( [this]( Ref* ){
			auto layer = RouleteLayer::create();
			SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
			scene->pushLayer( layer, true );
		}, std::placeholders::_1 );
	else if( name == "survival" )
		return [this]( Ref * ){
			auto survival = SurvivalLayer::create();
			if( survival )
				dynamic_cast<SmartScene*>(getScene())->pushLayer( survival, true );
		};
	else if( name == "multiplayer" )
		return std::bind( &MapLayer::cb_game, this, std::placeholders::_1, GameMode::multiplayer );
	else if( name == "daily_reward" )
		return [this]( Ref * ){
			auto drr = DailyRewardRoulette::create();
			if( drr )
				dynamic_cast<SmartScene*>(getScene())->pushLayer( drr, true );
		};
	else if( name == "settings" )
		return [this]( Ref * ){
		auto settings = SettingsLayer::create();
		if( settings )
			dynamic_cast<SmartScene*>(getScene())->pushLayer( settings, true );
		};
	else if( name.find( "shop:" ) == 0 )
	{
		return ShopLayer2::dispatch_query( name );
	}
	else if( name.find( "video:" ) == 0 )
	{
		std::string score = name.substr( strlen( "video:" ) );
		return std::bind( ShopLayer2::video, score, this );
	}
	else if ( name == "slotmachine" )
		return [this]( Ref * ){
		auto slotmachine = SlotMachineLayer::create();
		if (slotmachine)
			dynamic_cast<SmartScene*>(getScene())->pushLayer( slotmachine, true );
		};
	else if( name == "options" )
		return [this]( Ref * ){
		auto options = GamePauseLayer::create(nullptr, "ini/maings/settings.xml");
		if( options )
			dynamic_cast<SmartScene*>(getScene())->pushLayer( options, true );
	};
	else if( name.find( "share:" ) == 0 )
	{
		std::string network = name.substr( strlen( "share:" ) );
		return [this, network]( Ref* ){
			auto shareLayer = ShareLayer::create( network );
			if( shareLayer )
				dynamic_cast<SmartScene*>(getScene())->pushLayer( shareLayer, true );
		};
	}
	return LayerExt::get_callback_by_description(name);
}

void MapLayer::scrollBegan( const std::vector<Touch*>& touches, Event * event )
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
	_editMode = (GetAsyncKeyState( VK_SPACE ) != 0);
	if( _editMode )
	{
		touchEditModeBegan( touches, event );
		return;
	}
#endif
	Touch* touch = touches[0];
	_scrollInfo->node = _map;
	_scrollInfo->nodeposBegan = _map->getPosition();
	_scrollInfo->touchBegan = touch->getLocation();
	_scrollInfo->touchID = touch->getID();
	
	_isTouching = true;
}

void MapLayer::scrollMoved( const std::vector<Touch*>& touches, Event * event )
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
	_editMode = (GetAsyncKeyState( VK_SPACE ) != 0);
	if( _editMode )
	{
		touchEditModeMoved( touches, event );
		return;
	}
#endif
	Touch* touch = touches[0];
	if( touch&& _scrollInfo->node )
	{
		Point location = touch->getLocation();
		Point shift = location - _scrollInfo->touchBegan;
		Point pos = _scrollInfo->nodeposBegan + shift;
		Size winsize = Director::getInstance()->getWinSize();
		Point fitpos = _scrollInfo->fitPosition( pos, winsize );
		
		_scrollInfo->lastShift = Point( 0, 0 );
		_scrollInfo->node->setPosition( fitpos );

		_velocity = touch->getDelta();
	}
}

void MapLayer::scrollEnded( const std::vector<Touch*>& touches, Event * event )
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
	if( _editMode )
	{
		touchEditModeEnded( touches, event );
		return;
	}
#endif
	_isTouching = false;
}

void MapLayer::mouseHover(Event*event)
{
}

#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
void MapLayer::touchEditModeBegan( const std::vector<Touch*>& touch, Event * event )
{
	if( getMsFromLastCall() < 333 ) // double click
	{
		auto src = _locations.back();
		Location dst;
		dst.pos = (touch.front()->getLocation() - _map->getPosition()) / _map->getScale();
		dst.a = dst.pos * 1.33f - src.pos * 0.33f;
		dst.b = dst.pos * 1.67f - src.pos * 0.67f;
		dst.posLock = src.posLock;

		if( src.pos.getDistance( dst.pos ) < 10 )
			_locations.pop_back();
		else
 			_locations.push_back(dst);
		UserData::shared().write( kUser_locationsCount, (int)_locations.size() );
	}

	auto getTouchedPoint = [this, touch]( std::vector<Location>& locations )
	{
		Point* point( nullptr );
		for( auto& loc : locations )
		{
			auto p = _map->getPosition();

			if( (loc.pos * _map->getScale() + p).getDistance( touch.front()->getLocation() ) < 10 )
				point = &loc.pos;
			else if( (loc.a * _map->getScale() + p).getDistance( touch.front()->getLocation() ) < 10 )
				point = &loc.a;
			else if( (loc.b * _map->getScale() + p).getDistance( touch.front()->getLocation() ) < 10 )
				point = &loc.b;
		}
		return point;
	};
	_editPoint = getTouchedPoint( _locations );
	if( _editPoint == nullptr )
		_editPoint = getTouchedPoint( _locationsSurvival );
}

void MapLayer::touchEditModeMoved( const std::vector<Touch*>& touch, Event * event )
{
	if( _editPoint )
	{
		*_editPoint += touch.front()->getDelta() * _map->getScale();
		
		
		for( auto node : _curveMarkers )
			node->removeFromParent();
		_curveMarkers.clear();
		for( unsigned i = 0; i < _locations.size(); ++i )
			buildCurve( i, false );
	}
}

void MapLayer::touchEditModeEnded( const  std::vector<Touch*>& touch, Event * event )
{
	_editPoint = nullptr;
	activateLocations();
	
	auto doc = std::make_shared<pugi::xml_document>();
	doc->load_file( "ini/map/maplayer.xml" );
	auto root = doc->root().first_child();
	root.remove_child( "locations" );
	root.remove_child( "locations_survival" );
	auto loc1 = root.append_child( "locations" );
	auto loc2 = root.append_child( "locations_survival" );
	for( auto& loc : _locations )
	{
		auto node = loc1.append_child( "location" );
		node.append_attribute( "pos" ).set_value( toStr( loc.pos ).c_str() );
		node.append_attribute( "controlA" ).set_value( toStr( loc.a ).c_str() );
		node.append_attribute( "controlB" ).set_value( toStr( loc.b ).c_str() );
	}
	for( auto& loc : _locationsSurvival )
	{
		auto node = loc2.append_child( "location" );
		node.append_attribute( "pos" ).set_value( toStr( loc.pos ).c_str() );
		node.append_attribute( "afterlevel" ).set_value( toStr( loc.afterLevel ).c_str() );
	}

	doc->save_file( "ini/map/maplayer.xml" );
}

#endif

void MapLayer::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
	if( isTestModeActive() )
	{
		_editMode = (GetAsyncKeyState( VK_SPACE ) != 0);

		std::vector<Point> points;
		if( _editMode )
		{
			renderer->render();
			for( auto loc : _locations )
			{
				auto p = _map->getPosition();
				DrawPrimitives::drawCircle( loc.pos * _map->getScale() + p, 5, 0, 4, false );
				DrawPrimitives::drawCircle( loc.a * _map->getScale() + p, 5, 0, 4, false );
				DrawPrimitives::drawCircle( loc.b * _map->getScale() + p, 5, 0, 4, false );
				points.push_back( loc.pos * _map->getScale() + p );
				points.push_back( loc.a * _map->getScale() + p );
				points.push_back( loc.b * _map->getScale() + p );
			}
			for( auto loc : _locationsSurvival )
			{
				auto p = _map->getPosition();
				DrawPrimitives::drawCircle( loc.pos * _map->getScale() + p, 5, 0, 4, false );
			}
			DrawPrimitives::drawPoly( points.data(), points.size(), false );
			renderer->render();
		}
		_menuLocations->setEnabled( !_editMode );
	}
#endif
}

bool MapLayer::isBlurActive()
{
	return !isRunning();
}

void MapLayer::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

void MapLayer::update(float delta)
{
	if( _isTouching )
	{
		_velocity *= 0.75f;
	}
	else
	{
		_velocity *= 0.95f;
		if (!_scrollInfo->node) return;
		Size winsize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
		
		if (_velocity.getLengthSq() > 0.001f)
		{
			Point pos = _scrollInfo->node->getPosition() + _velocity;
			Point fitpos = _scrollInfo->fitPosition(pos, winsize);
			_scrollInfo->node->setPosition(fitpos);
		}
	}
#if PC == 1
	MouseHoverScroll::shared().update( delta );
#endif
}

void MapLayer::showWindow( NodePointer window )
{
	auto gl = Director::getInstance()->getOpenGLView();
	auto dessize = gl->getDesignResolutionSize();
	
	auto scene = getScene();
	scene->addChild( window, getLocalZOrder() + 2 );
	onExit();
	
	auto shadow = ImageManager::sprite( kPathSpriteSquare );
	if( shadow )
	{
		shadow->setName( "shadow" );
		shadow->setScaleX( dessize.width );
		shadow->setScaleY( dessize.height );
		shadow->setColor( Color3B( 0, 0, 0 ) );
		shadow->setOpacity( 0 );
		shadow->setPosition( Point( dessize / 2 ) );
		scene->addChild( shadow, getLocalZOrder() + 1 );
		shadow->runAction( FadeTo::create( 0.2f, 204 ) );
	}
}

void MapLayer::windowDidClosed()
{
	onEnter();
	
	auto scene = getScene();
	auto shadow = scene->getChildByName( "shadow" );
	if( shadow )
	{
		auto destroyer = std::bind(&Node::removeFromParent, shadow );
		auto a0 = FadeTo::create( 0.2f, 0 );
		auto a1 = CallFunc::create( destroyer );
		shadow->runAction( Sequence::createWithTwoActions( a0, a1 ) );
	}
}

void MapLayer::openRateMeWindowIfNeeded()
{
	static bool isShowing = false;
	int wincounter = UserData::shared().get <int>(k::user::GameWinCounter);

	if( !Config::shared().get<bool>( "useRateMe" ) || wincounter % 3 || isShowing )
		return;
	
	//open RateMe with delay
	auto call = CallFunc::create([this](){
		auto scene = static_cast<SmartScene*>(getScene());
		auto layer = RateMeLayer::create();
		if (layer) {
			scene->pushLayer(layer, true);
		}
		isShowing = false;
	});
	isShowing = true;
	runAction( Sequence::createWithTwoActions( DelayTime::create( 0.3f ), call ) );
}

void MapLayer::cb_back( Ref*sender )
{
	Director::getInstance()->popScene();
}

LayerPointer MapLayer::createShop( int layer, int tab )
{
	auto version = Config::shared().get<int>( "shopversion" );
	LayerPointer shop;
	if( version < 2 )
	{
		shop = ShopLayer::create( Config::shared().get<bool>( "useFreeFuel" ), true, false, false );
	}
	else if( version == 2 )
	{
		shop = ShopLayer2::create( layer, tab );
	}
	return shop;
}

void MapLayer::cb_shop( Ref*sender, int layer, int tab )
{
#if PC != 1
	auto shop = createShop( layer, tab );
	if( shop )
	{
		SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
		scene->pushLayer( shop, true );
		TutorialManager::shared().dispatch( "map_openshop" );
	}
#endif
}

void MapLayer::cb_paidversion( Ref*sender )
{
	//openUrl( Config::shared().get<>( "paidVersionUrl );
	auto layer = BuyHeroes::create( nullptr );
	auto scene = dynamic_cast<SmartScene*>(getScene());
	if( scene&& layer )
	{
		auto z = layer->getLocalZOrder();
		scene->pushLayer( layer, true );
		if( z != 0 )
			layer->setLocalZOrder( z );
	}
}

void MapLayer::cb_lab( Ref*sender )
{
	int version = Config::shared().get<int>( "labversion" );
	if( version < 2 )
	{
		SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
		auto layer = Laboratory::create();
		scene->pushLayer( layer, true );
		TutorialManager::shared().dispatch( "map_openlab" );
	}
	else
	{
		cb_lab2( sender );
	}
}

void MapLayer::cb_specialUpgrades(Ref*sender){
    SmartScene* scene = dynamic_cast<SmartScene*>(getScene());
    auto layer = SpecialUpgrades::create();
    scene->pushLayer(layer);
    
    TutorialManager::shared().dispatch("map_specialupgrades");
}

void MapLayer::cb_lab2( Ref*sender )
{
	auto layer = Laboratory2::create();
	getSmartScene()->pushLayer( layer, true );
	TutorialManager::shared().dispatch( "map_openlab" );
}

void MapLayer::cb_itemshop( Ref*sender )
{
	SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
	auto layer = ItemShop::create();
	scene->pushLayer( layer, true );
	
	TutorialManager::shared().dispatch( "map_openitemshop" );
}
//
void MapLayer::cb_popshop(cocos2d::Ref *sender)
{
    SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
    auto layer = PopShop::create();
    scene->pushLayer( layer, true );
}
//

void MapLayer::cb_game( Ref*sender, GameMode mode )
{
	if( mode != GameMode::multiplayer )
	{
		auto choose = getScene()->getChildByName<LayerExt*>( "choose" );

		if( _menuLocations )
			_menuLocations->setEnabled( false );

		int cost, type;
		LevelParams::shared().getPayForLevel( _selectedLevelIndex, mode, type, cost );
		int fuel = ScoreCounter::shared().getMoney( kScoreFuel );
		if( fuel < cost )
		{
			if( _useDialog == false )
			{
				bool dispatched = TutorialManager::shared().dispatch( "map_haventfuel" );
				if( !Config::shared().get<bool>( "useInapps" ) || !dispatched )
				{
					cb_shop( sender, 1, 2 );
					if( Config::shared().get<bool>( "useInapps" ) == false )
					{
						_menuLocations->setEnabled( true );
					}
				}
				if( choose&& dispatched )
				{
					if( !choose->runEvent( "onexit" ) )
						choose->disappearance();
				}
			}
			else
			{
				DialogLayer::showForShop( "ini/dialogs/nofuel.xml", 1, 2 );
			}
		}
		else
		{
			TutorialManager::shared().dispatch( "map_rungame" );
			_updateLocations = true;
			runLevel( _selectedLevelIndex, mode );
			if( choose )
				choose->removeFromParent();
			_showLaboratoryOnEnter = true && Config::shared().get<bool>("showLaboratoryOnEnter");
		}
	}
	else
	{
		unsigned passed = UserData::shared().level_getCountPassed();
		if( passed < 3 )
		{
			auto layer = buildWindowLevelLocked( "ini/map/multiplayer_locked.xml", 0, 3 );
			getSmartScene()->pushLayer( layer, true );
			return;
		}
		else
		{
			auto layer = LoginLayer::create();
			getSmartScene()->pushLayer( layer );
		}
	}
}

void MapLayer::cb_gamelock( Ref*sender, int index )
{
	_selectedLevelIndex = index;
	
	auto layer = buildUnlockWindow( index );
	SmartScene * scene = static_cast<SmartScene*>(getScene());
	assert( scene );
	scene->pushLayer( layer, true );
}

void MapLayer::cb_showChoose( Ref*sender, int index )
{
	_selectedLevelIndex = index;
	bool isSurvival = index >= (int)_locations.size();
	if( isSurvival )
	{
		unsigned passed = UserData::shared().level_getCountPassed();
		size_t i = index - _locations.size();
		bool availabled = _locationsSurvival[i].afterLevel <= (int)passed;
		if( !availabled )
		{
			auto layer = buildWindowLevelLocked( "ini/map/level_locked.xml", index, _locationsSurvival[i].afterLevel );
			getSmartScene()->pushLayer( layer, true );
			return;
		}
	}
	
	auto layer = buildChooseWindow( index );
	if( layer )
	{
		SmartScene * scene = getSmartScene();
		assert( scene );
		_doNotHideBanner = true;
		scene->pushLayer( layer, true );
		
		TutorialManager::shared().dispatch( "map_onchoose" );
	}
	else
	{
		cb_game( sender, GameMode::normal );
	}
}

void MapLayer::cb_unlock( Ref*sender )
{
	UserData::shared().write( k::user::LevelUnlocked + toStr( _selectedLevelIndex ), true );
	ScoreCounter::shared().subMoney( kScoreStars, _locations[_selectedLevelIndex].starsForUnlock, true, "map:unlock" );
	activateLocations();
	auto choose = getScene()->getChildByName<LayerExt*>( "choose" );
	if( choose )
	{
		choose->runEvent( "onexit" );
	}
}

void MapLayer::runLevel( int levelIndex, GameMode mode )
{
	if( levelIndex < static_cast<int>(_locations.size() + _locationsSurvival.size()) )
	{
        if (levelIndex == 0)
        {
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
            int cutSceneValue = 1;
            CutSceneHandler::getInstance()->showCutScene(cutSceneValue, this, [levelIndex, mode](){
                auto loadScene = LoadLevelScene::create( levelIndex, mode, false );
                Director::getInstance()->pushScene( loadScene );
            });
#else
            auto loadScene = LoadLevelScene::create( levelIndex, mode, false );
            Director::getInstance()->pushScene( loadScene );
#endif
        }
        else
        {
            auto loadScene = LoadLevelScene::create( levelIndex, mode, false );
            Director::getInstance()->pushScene( loadScene );
        }
	}
	else
	{
		auto player = AutoPlayer::getInstance();
		if( player )
		{
			Director::getInstance()->getScheduler()->unscheduleAllForTarget( player );
			player->release();
		}
	}
    UserData::shared().write(kLevelPlaying, levelIndex);
	UserData::shared().save();
}

void MapLayer::updateFogOfWar()
{
	if( !_fogOfWarEnabled )
		return;

	int const lastLevel = std::min<int>( UserData::shared().level_getCountPassed() + 1, _locations.size() );
	float fromX = 0;
	float toX = _map->getContentSize().width;
	for( int i = 0; i < lastLevel; ++i )
	{
		if( _locations[i].pos.x > fromX )
			fromX = _locations[i].pos.x;
	}

	static std::string const fogOfWarName = "fogOfWar";

	NodePointer fow = _map->getChildByName( fogOfWarName );
	
	if( fow != nullptr )
	{
		fow->removeFromParent();
	}

	xmlLoader::macros::Temporal macros = {
		{ "from_x", toStr( fromX ) },
		{ "to_x", toStr( toX ) }
	};
	fow = xmlLoader::load_node( xmlFogOfWar );

	if( fow )
	{
		fow->setName( fogOfWarName );
		_map->addChild( fow );
	}
}

void MapLayer::activateLocations()
{
	for( auto node : _curveMarkers )
	{
		node->removeFromParent();
	}
	_curveMarkers.clear();
	
	unsigned passed = UserData::shared().level_getCountPassed();
	_menuLocations->removeAllItems();
	
	bool showpath( false );
	std::string key = "map_level_" + toStr( passed ) + "_pathshowed";
	showpath = UserData::shared().get<int>(key) == 0;

	float predelayLastFlagAppearance = (showpath&& passed > 0) ? 4 : 0.5f;
	xmlLoader::macros::set( "flag_delay_appearance", toStr( predelayLastFlagAppearance ) );
	
    int levelindex = 0;
    float startingDelay = 0.01f;
	for( unsigned i = 0; i < _locations.size() && levelindex <= passed; ++i )
	{
		int fuel, type;
		LevelParams::shared().getPayForLevel( levelindex, GameMode::normal, type, fuel );
		xmlLoader::macros::set( "fuel_for_level", toStr( fuel ) );
		Location loc = _locations[i];
        
        if(loc.skip) {
            float delay = buildCurve( i, levelindex, showpath && levelindex == passed, startingDelay);
            startingDelay += delay;
        }
        else {
            buildCurve( i, levelindex, showpath && levelindex == passed, startingDelay);
            startingDelay = 0.0f;
            
            auto flag = createFlag( i, levelindex );
            _menuLocations->addItem( flag );
            if( _locations[i].starsForUnlock > 0 )
            {
                TutorialManager::shared().dispatch( "unlocked_location" );
            }
            levelindex++;
        }
	}
	
	xmlLoader::macros::erase( "flag_delay_appearance" );
	UserData::shared().write( key, 1 );
	
	activateSurvivalLocations();
	updateFogOfWar();

	_updateLocations = false;
}

void MapLayer::activateSurvivalLocations()
{
	unsigned passed = UserData::shared().level_getCountPassed();
	for( size_t i = 0; i < _locationsSurvival.size(); ++i )
	{
		size_t index = _locations.size() + i;
		auto flag = createFlag( index ,index);
		//flag->setEnabled( _locationsSurvival[i].afterLevel <= passed );
		_menuLocations->addItem( flag );
	}
}

std::vector<Point> buildPoints( Point a, Point b, Point c, Point d )
{
	std::vector<Point> points;
	std::vector<float> times;
	auto push = [&points,&times]( float time, Point point )mutable
	{
		points.push_back( point );
		times.push_back( time );
	};
	auto insert = [&points,&times]( int pos, float time, Point point )mutable
	{
		points.insert( points.begin() + pos, point );
		times.insert( times.begin() + pos, time );
	};
	auto K = []( Point L, Point R, Point S )
	{
		auto d0 = S - L;
		auto d1 = R - S;
		if( d0.length() < 5 )
			return false;
		float k0 = d0.y == 0 ? 0 : d0.x / d0.y;
		float k1 = d1.y == 0 ? 0 : d1.x / d1.y;
		return fabs( k0 - k1 ) > 0.2f;
	};
	push( 0.00, compute_bezier( a, b, c, d, 0.00 ) );
	push( 0.25, compute_bezier( a, b, c, d, 0.25 ) );
	push( 0.50, compute_bezier( a, b, c, d, 0.50 ) );
	push( 0.75, compute_bezier( a, b, c, d, 0.75 ) );
	push( 1.00, compute_bezier( a, b, c, d, 1.00 ) );
	
	bool exit2( false );
	unsigned currentIndex( 0 );
	while( currentIndex < points.size() - 1 )
	{
		exit2 = true;
		Point L = points[currentIndex];
		Point R = points[currentIndex + 1];
		float Ltime = times[currentIndex];
		float Rtime = times[currentIndex + 1];
		do
		{
			float t = (Ltime + Rtime) / 2.f;
			Point p = compute_bezier( a, b, c, d, t );
			if( K( L, R, p ) )
			{
				insert( currentIndex + 1, t, p );
				exit2 = false;
			}
			else
			{
				exit2 = true;
			}
			Rtime = t;
			R = p;
		}
		while( !exit2 );
		++currentIndex;
	}
	
	std::vector<Point> points2;
	Point P = points[0];
	unsigned index = 1;
	float D = 18;
	float E = 0;
	for( ; index < points.size(); ++index )
	{
		Point r = points[index] - P;
		while( r.getLength() > D - E )
		{
			Point rn = r.getNormalized();
			P = P + rn * (D);
			points2.push_back( P );
			E = 0;
			r = points[index] - P;
		}
		E += r.getLength();
	}
	
	return points2;
}

float MapLayer::buildCurve( int index, int levelNumber, bool showpath , float startingDelay)
{
    int passed = UserData::shared().level_getCountPassed();
	bool availabled = levelNumber <= passed;
	if( index == 0 ) return 0.0f;
	if( availabled == false ) return 0.0f;
    
	Point a = _locations[index - 1].pos;
	Point b = _locations[index - 1].a;
	Point c = _locations[index - 1].b;
	Point d = _locations[index].pos;
	
	auto points = buildPoints( a, b, c, d );
	int iteration( 0 );
	float kdelay = 2.f / points.size();
	for( auto point : points )
	{
		auto pointSprite = ImageManager::sprite( "images/map/point.png" );
		pointSprite->setPosition( point );
		_map->addChild( pointSprite );
		_curveMarkers.push_back( pointSprite );
		
		if( showpath )
		{
			auto delay = DelayTime::create( startingDelay + static_cast<float>(iteration)* kdelay + 2 );
			auto scale = EaseBackOut::create( ScaleTo::create( 0.2f, 1 ) );
			auto action = Sequence::createWithTwoActions( delay, scale );
			
			pointSprite->setScale( 0 );
			pointSprite->runAction( action );
		}
		++iteration;
	}
    return (static_cast<float>(iteration)* kdelay + 2);
}

mlMenuItem::Pointer MapLayer::createFlag( int index , int levelNumber)
{
	bool isSurvival = index >= (int)_locations.size();
	const auto& location = isSurvival == false ?
		_locations[index] :
		_locationsSurvival[index - _locations.size()];

	Point position = location.pos;
	int passed = UserData::shared().level_getCountPassed();

	int levelStars = UserData::shared().level_getScoresByIndex( levelNumber );
	int levelStartIncludeHardMode = UserData::shared().get <int>(k::user::LevelStars + toStr(levelNumber));
	bool levelLocked = location.starsForUnlock > 0;
	levelLocked = levelLocked&& (UserData::shared().get <bool>(k::user::LevelUnlocked + toStr(levelNumber)) == false);
	if (Config::shared().get<bool>("useStarsForUnlock") == false)
	{
		levelLocked = false;
	}
	
	std::string path;
	ccMenuCallback callback;
	bool buildIndicator( false );
	std::string flagResource;
	flagResource = "flag_" + (levelStartIncludeHardMode <= 3 ? toStr( levelStartIncludeHardMode ) : std::string( "hard" ));
	if( isSurvival )
	{
		path = "ini/map/flag_survival.xml";
	}
	else if( levelNumber < passed )
	{
		path = "ini/map/flag.xml";
	}
	else if( levelLocked )
	{
		buildIndicator = true;
		path = "ini/map/flag_locked.xml";
		position = location.posLock;
	}
	else
	{
		path = "ini/map/flag2.xml";
	}
	
	if( levelLocked == false )
	{
		callback = std::bind(&MapLayer::cb_showChoose, this, std::placeholders::_1, levelNumber );
	}
	else
	{
		callback = std::bind(&MapLayer::cb_gamelock, this, std::placeholders::_1, levelNumber );
	}
	
	xmlLoader::macros::set( "flag_position", toStr( position ) );
	xmlLoader::macros::set( "flag_image", flagResource );
	xmlLoader::macros::set( "not_survival", toStr( !location.survivalMode ) );
	auto flagnode = xmlLoader::load_node<mlMenuItem>( path );
	xmlLoader::macros::erase( "not_survival" );
	xmlLoader::macros::erase( "flag_position" );
	xmlLoader::macros::erase( "flag_image" );
	
	flagnode->setName( "flag" + toStr( levelNumber ) );
	flagnode->setCallback( callback );
	
	if (UserData::shared().get <int>("map_level_appearance" + toStr(levelNumber) + "_" + toStr(levelStars)) == 0)
	{
		if( index != passed )
		{
			UserData::shared().write( "map_level_appearance" + toStr( levelNumber ) + "_" + toStr( levelStars ), 1 );
		}
		flagnode->runEvent( "star" + toStr( levelStars ) + "_show" );
	}
	else
	{
		flagnode->runEvent( "star" + toStr( levelStars ) );
	}
	
	flagnode->setPosition( position );
	
	return flagnode;
}

LayerPointer MapLayer::buildChooseWindow( int level )
{
	bool survivalMode = level >= (int)_locations.size();
	survivalMode = survivalMode ? survivalMode : _locations[level].survivalMode;
	xmlLoader::bookDirectory( this );
	LayerPointer layer;
	auto version = Config::shared().get<int>( "launchversion" );
	if( version == 1 )
		layer = LaunchLevelLayer::create( level );
	else if(version == 2)
		layer = LaunchLevelLayer2::create( level, survivalMode );
	xmlLoader::unbookDirectory( this );

	return layer;
}

LayerPointer MapLayer::buildUnlockWindow( int level )
{
	auto load = [this]()
	{
		xmlLoader::bookDirectory( this );
		auto layer = xmlLoader::load_node<LayerExt>( "ini/map/unlock.xml" );
		xmlLoader::unbookDirectory(this );
		return layer;
	};
	auto buildCloseMenu = [this]( LayerExt * layer )
	{
		auto item = MenuItemImage::create( "images/square.png", "images/square.png", std::bind( [layer]( Ref* )mutable
																							   {
																								   layer->runEvent( "onexit" );
																							   },
																							   std::placeholders::_1 ) );
		item->getNormalImage()->setOpacity( 1 );
		item->getSelectedImage()->setOpacity( 1 );
		item->setScale( 9999 );
		auto menu = Menu::create( item, nullptr );
		layer->addChild( menu, -9999 );
	};
	auto buildIndicator = [this, level]( LayerExt*layer )
	{
		auto indicator = layer->getChildByName<Sprite*>( "progress_frame" );
		assert( indicator );
		Rect rect = indicator->getTextureRect();
		float defaultWidth = rect.size.width;
		int needStar = _locations[level].starsForUnlock;
		int stars = ScoreCounter::shared().getMoney( kScoreStars );
		float progress = std::min( 1.f, float( stars ) / float( needStar ) );
		float width = defaultWidth * progress;
		rect.size.width = width;
		indicator->setTextureRect( rect );
		
		auto label = layer->getChildByName<Text*>( "progress_text" );
		assert( label );
		label->setString( toStr( stars ) + "/" + toStr( needStar ) );
		
		auto cost = static_cast<Text*>(layer->getChildByPath( "menu/unlock/normal/text" ));
		auto cost2 = static_cast<Text*>(layer->getChildByPath( "menu/unlock_gray/normal/text" ));
		cost->setString( toStr( needStar ) );
		cost2->setString( toStr( needStar ) );
		
		auto button = static_cast<MenuItem*>(layer->getChildByPath( "menu/unlock" ));
		auto button_gray = static_cast<MenuItem*>(layer->getChildByPath( "menu/unlock_gray" ));
		if( stars < needStar )
		{
			button->setVisible( false );
			button_gray->setVisible( true );
		}
	};
	auto setMacroses = [level,this]()
	{
		xmlLoader::macros::set( "unlock_image", this->_locations[level].unlockFrame );
		xmlLoader::macros::set( "unlock_text", this->_locations[level].unlockText );
	};
	
	auto unsetMacroses = [level]()
	{
		xmlLoader::macros::erase( "unlock_image" );
		xmlLoader::macros::erase( "unlock_text" );
	};
	
	setMacroses();
	auto layer = load();
	unsetMacroses();
	if( layer )
	{
		buildCloseMenu( layer );
		buildIndicator( layer );
		layer->runEvent( "onenter" );
		setKeyDispatcher( layer );
	}
	return layer;
}

LayerPointer MapLayer::buildWindowLevelLocked( const std::string& path, int index, int unlockedAfterLevel )
{
	xmlLoader::macros::set( "levelindex", toStr( index + 1 ) );
	xmlLoader::macros::set( "unlock_after_level", toStr( unlockedAfterLevel ) );
	auto layer = xmlLoader::load_node<Layer>( path );
	xmlLoader::macros::erase( "levelindex" );
	xmlLoader::macros::erase( "unlock_after_level" );
	return layer;
}

void MapLayer::onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event )
{
	if( keyCode == EventKeyboard::KeyCode::KEY_BACK )
		Director::getInstance()->popScene();
 
	if( isTestDevice()&& isTestModeActive() )
	{
		if( keyCode == EventKeyboard::KeyCode::KEY_F1 )
		{
            this->openAllLevels();
		}
		if( keyCode == EventKeyboard::KeyCode::KEY_F2 )
		{
			ScoreCounter::shared().addMoney( kScoreCrystals, 10000, true );
		}
		if( keyCode == EventKeyboard::KeyCode::KEY_F3 )
		{
			ScoreCounter::shared().addMoney( kScoreFuel, 50, true );
		}
		if( keyCode == EventKeyboard::KeyCode::KEY_F4 )
		{
			ScoreCounter::shared().addMoney( kScoreTicket, 100, true );
		}
		if( keyCode == EventKeyboard::KeyCode::KEY_1 ) { auto player = AutoPlayer::create( true, true, 1, false ); player->retain(); }
		if( keyCode == EventKeyboard::KeyCode::KEY_2 ) { auto player = AutoPlayer::create( true, false, 3, false ); player->retain(); }
		if( keyCode == EventKeyboard::KeyCode::KEY_3 ) { auto player = AutoPlayer::create( false, false, 99, true ); player->retain(); }
		if( keyCode == EventKeyboard::KeyCode::KEY_4 ) { auto player = AutoPlayer::create( false, false, 99, true ); player->retain(); player->setGameMode( GameMode::hard ); }
		if( keyCode == EventKeyboard::KeyCode::KEY_5 ) { auto player = AutoPlayer::create( true, true, 99, true ); player->retain(); }
		if( keyCode == EventKeyboard::KeyCode::KEY_6 ) { auto player = AutoPlayer::create( true, false, 99, true ); player->retain(); }
	}
}

void MapLayer::openAllLevels() {
    size_t pass = static_cast<size_t>(UserData::shared().level_getCountPassed());
    if( pass < _locations.size() )
    {
        pass = _locations.size();
        UserData::shared().level_setCountPassed( pass );
        for( size_t i = 0; i < pass; ++i )
        {
            UserData::shared().level_setScoresByIndex( i, 1 );
        }
        activateLocations();
    }
}

bool MapLayer::openShopFromLaboratory()
{
	bool useInnaps = Config::shared().get<bool>( "useInapps" );
	if( useInnaps&& !TutorialManager::shared().dispatch( "lab_haventgold" ) )
	{
		cb_shop( nullptr, 1, 1 );
		return true;
	}
	return false;
}

void MapLayer::createPromoMenu()
{
	auto menu = getChildByName( "promomenu" );
	if( menu == nullptr && BuyHeroMenu::isShow())
	{
		auto menu = BuyHeroMenu::create();
		if( menu )
			addChild( menu, 999 );
	}
	else if( menu&& BuyHeroMenu::isShow() == false )
	{
		menu->removeFromParent();
	}
}

void MapLayer::createDevMenu()
{
	if( isTestDevice()&& isTestModeActive() )
	{
		auto item = [this]( Menu * menu, std::string text, EventKeyboard::KeyCode key, Point pos )
		{
			auto sendKey = [this]( Ref* sender, EventKeyboard::KeyCode key )mutable
			{
				this->onKeyReleased( key, nullptr );
			};

			auto i = MenuItemTextBG::create( text, Color4F::GRAY, Color3B::BLACK, std::bind( sendKey, std::placeholders::_1, key ) );
			menu->addChild( i );
			i->setPosition( pos );
			i->setScale( 1.5f );
		};

		auto menu = Menu::create();
		menu->setPosition( 0, 0 );
		addChild( menu, 9999 );
		Point pos( 150, 200 );
		float Y( 45 );
		item( menu, "Open All", EventKeyboard::KeyCode::KEY_F1, pos ); pos.y += Y;
		item( menu, "Tickets", EventKeyboard::KeyCode::KEY_F4, pos ); pos.y += Y;
		item( menu, "Gold", EventKeyboard::KeyCode::KEY_F2, pos ); pos.y += Y;
		item( menu, "Fuel", EventKeyboard::KeyCode::KEY_F3, pos ); pos.y += Y;
		
		item( menu, "play current level", EventKeyboard::KeyCode::KEY_1, pos ); pos.y += Y;
		item( menu, "play all game", EventKeyboard::KeyCode::KEY_2, pos ); pos.y += Y;
		item( menu, "fast test all levels", EventKeyboard::KeyCode::KEY_3, pos ); pos.y += Y;
		item( menu, "fast test all hards", EventKeyboard::KeyCode::KEY_4, pos ); pos.y += Y;
		item( menu, "fast test current level", EventKeyboard::KeyCode::KEY_5, pos ); pos.y += Y;
		item( menu, "fast test from cur level", EventKeyboard::KeyCode::KEY_6, pos ); pos.y += Y;
	}
}

void MapLayer::showDailyReward()
{
	static int showCount = 0;

	if( !Config::shared().get<bool>( "useDailyReward" ) || showCount > 0 || DailyReward::isGiftedToday() )
		return;
	
	auto creation = CallFunc::create( [this]() {
		auto dr = DailyRewardInstant::create();
		if( dr )
			dynamic_cast<SmartScene *>(getScene())->pushLayer( dr, true );
		--showCount;
	} );

	++showCount;
	runAction( Sequence::createWithTwoActions( DelayTime::create( 0.25f ), creation ) );
}

NS_CC_END
