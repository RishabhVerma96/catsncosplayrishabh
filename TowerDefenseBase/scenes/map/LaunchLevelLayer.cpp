#include "LaunchLevelLayer.h"
#include "ml/loadxml/xmlLoader.h"
#include "ml/loadxml/xmlProperties.h"
#include "ml/ImageManager.h"
#include "ml/Language.h"
#include "support.h"
#include "UserData.h"
#include "MapLayer.h"
#include "ScoreCounter.h"
NS_CC_BEGIN





LaunchLevelLayer::LaunchLevelLayer()
{}

LaunchLevelLayer::~LaunchLevelLayer()
{}

bool LaunchLevelLayer::init( int indexlevel )
{
	do
	{
		auto buildCloseMenu = [this]()
		{
			auto item = MenuItemImage::create( "images/square.png", "images/square.png",
				std::bind( [this]( Ref* )mutable
			{
				this->runEvent( "onexit" );
			},
				std::placeholders::_1 ) );
			item->getNormalImage()->setOpacity( 1 );
			item->getSelectedImage()->setOpacity( 1 );
			item->setScale( 9999 );
			auto menu = Menu::create( item, nullptr );
			this->addChild( menu, -9999 );
		};

		auto buildPreviewLevel = [this, indexlevel]()
		{
			float kWidthPreview = 280;
			float kHeightPreview = 270;

			auto sizeParamIt = getParamCollection().find("previewSize");
			if (sizeParamIt != getParamCollection().end())
			{
				auto size = strTo<Point>(sizeParamIt->second);
				kWidthPreview = size.x;
				kHeightPreview = size.y;
			}

			for( std::string const & prName : { "preview", "preview2" } )
			{
				auto preview = getNodeByPath( this, prName );
				if( preview != nullptr )
				{
					auto sprite = ImageManager::sprite( "images/maps/map" + toStr( indexlevel + 1 ) + ".jpg" );
					auto sx = kWidthPreview / sprite->getContentSize().width;
					auto sy = kHeightPreview / sprite->getContentSize().height;
					sprite->setScale( sx, sy );
					preview->addChild( sprite, -1 );
				}
			}
		};

		auto setMacroses = [indexlevel]()
		{
			int type, costHard, costNormal;
			LevelParams::shared().getPayForLevel( indexlevel, GameMode::normal, type, costNormal );
			LevelParams::shared().getPayForLevel( indexlevel, GameMode::hard, type, costHard );

			int goldNorm = LevelParams::shared().getAwardGold( indexlevel, 3, GameMode::normal );
			int goldHard = LevelParams::shared().getAwardGold( indexlevel, 1, GameMode::hard );
			int gearNorm = LevelParams::shared().getStartGear( indexlevel, GameMode::normal );
			int gearHard = LevelParams::shared().getStartGear( indexlevel, GameMode::hard );
			int wavesNorm = LevelParams::shared().getWaveCount( indexlevel, GameMode::normal );
			int wavesHard = LevelParams::shared().getWaveCount( indexlevel, GameMode::hard );
			int livesNorm = LevelParams::shared().getLives( indexlevel, GameMode::normal );
			int livesHard = LevelParams::shared().getLives( indexlevel, GameMode::hard );
			int ticketsNorm = LevelParams::shared().getTickets( indexlevel, GameMode::normal );
			int ticketsHard = LevelParams::shared().getTickets( indexlevel, GameMode::hard );
			std::string excludeNorm = "";
			std::string excludeHard = LevelParams::shared().getExclude( indexlevel, GameMode::hard );
			std::string caption = WORD( "gamechoose_level" ) + toStr( indexlevel + 1 );
			xmlLoader::macros::set( "cost_normalmode", toStr( costNormal ) );
			xmlLoader::macros::set( "cost_hardmode", toStr( costHard ) );
			xmlLoader::macros::set( "gold_normalmode", toStr( goldNorm ) );
			xmlLoader::macros::set( "gold_hardmode", toStr( goldHard ) );
			xmlLoader::macros::set( "gear_normalmode", toStr( gearNorm ) );
			xmlLoader::macros::set( "gear_hardmode", toStr( gearHard ) );
			xmlLoader::macros::set( "waves_normalmode", toStr( wavesNorm ) );
			xmlLoader::macros::set( "waves_hardmode", toStr( wavesHard ) );
			xmlLoader::macros::set( "lives_normalmode", toStr( livesNorm ) );
			xmlLoader::macros::set( "lives_hardmode", toStr( livesHard ) );
			xmlLoader::macros::set( "tickets_normalmode", toStr( ticketsNorm ) );
			xmlLoader::macros::set( "tickets_hardmode", toStr( ticketsHard ) );
			xmlLoader::macros::set( "exclude_normalmode", excludeNorm );
			xmlLoader::macros::set( "exclude_hardmode", excludeHard );
			xmlLoader::macros::set( "preview_caption", caption );
		};

		auto unsetMacroses = [indexlevel]()
		{
			xmlLoader::macros::erase( "cost_hardmode" );
			xmlLoader::macros::erase( "cost_normalmode" );
			xmlLoader::macros::erase( "gold_hardmode" );
			xmlLoader::macros::erase( "gold_normalmode" );
			xmlLoader::macros::erase( "gear_normalmode" );
			xmlLoader::macros::erase( "gear_hardmode" );
			xmlLoader::macros::erase( "waves_normalmode" );
			xmlLoader::macros::erase( "waves_hardmode" );
			xmlLoader::macros::erase( "lives_normalmode" );
			xmlLoader::macros::erase( "lives_hardmode" );
			xmlLoader::macros::erase( "tickets_normalmode" );
			xmlLoader::macros::erase( "tickets_hardmode" );
			xmlLoader::macros::erase( "exclude_normalmode" );
			xmlLoader::macros::erase( "exclude_hardmode" );
			xmlLoader::macros::erase( "preview_caption" );
		};

		auto buildStars = [this, indexlevel]()
		{
			int starsN = 3;
			int starsH = LevelParams::shared().getMaxStars( indexlevel, GameMode::hard );
			std::list< std::string > normal;
			std::list< std::string > hard;
			auto pNormal = dynamic_cast<NodeExt*>(this->getChildByName( "normal" ));
			auto pHard = dynamic_cast<NodeExt*>(this->getChildByName( "hard" ));
			std::string image = pNormal->getParamCollection().at( "starimage" );
			split( normal, pNormal->getParamCollection().at( "star" + toStr( starsN ) ) );
			split( hard, pHard->getParamCollection().at( "star" + toStr( starsH ) ) );
			assert( normal.size() == starsN );
			assert( hard.size() == starsH );
			auto it = normal.begin();
			for( int i = 0; i < starsN; ++i )
			{
				Point pos = strTo<Point>( *(it++) );
				Sprite * star = ImageManager::sprite( image );
				assert( star );
				star->setPosition( pos );
				pNormal->getChildByPath( "stars" )->addChild( star );
			}
			it = hard.begin();
			for( int i = 0; i < starsH; ++i )
			{
				Point pos = strTo<Point>( *(it++) );
				Sprite * star = ImageManager::sprite( image );
				assert( star );
				star->setPosition( pos );
				pHard->getChildByPath( "stars" )->addChild( star );
			}
		};

		auto checkHardMode = [this, indexlevel]()
		{
			int pass = UserData::shared().level_getCountPassed();
			bool locked = false; //#parth change//pass <= indexlevel;
			auto hard = this->getChildByName( "hard" );
			auto hardlock = this->getChildByName( "hard_lock" );
			hard->setVisible( !locked );
			hardlock->setVisible( locked );

		};
		auto setKeyDispatcher = []( LayerExt* layer )
		{
			EventListenerKeyboard * event = EventListenerKeyboard::create();
			event->onKeyReleased = std::bind( [layer]( EventKeyboard::KeyCode key, Event* )mutable
			{
				if( key == EventKeyboard::KeyCode::KEY_BACK )
					layer->runEvent( "onexit" );
			}, std::placeholders::_1, std::placeholders::_2 );

			layer->getEventDispatcher()->addEventListenerWithSceneGraphPriority( event, layer );
		};

		CC_BREAK_IF( !LayerExt::init() );
		setMacroses();
		NodeExt::load( "ini/map/choose.xml" );
		unsetMacroses();

		MapLayer::prepairNodeByConfiguration( this );
		buildCloseMenu();
		buildPreviewLevel();
		buildStars();
		checkHardMode();
		setKeyDispatcher(this);
		runEvent( "onenter" );

		return true;
	}
	while( false );
	return false;
}

ccMenuCallback LaunchLevelLayer::get_callback_by_description(const std::string & name)
{
	if (name == "close")
	{
		return std::bind([this]()mutable
		{
			this->runEvent("onexit");
		});
	}

	return nullptr;
}




NS_CC_END
