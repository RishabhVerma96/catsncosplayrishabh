#pragma once

#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"

NS_CC_BEGIN


class LaunchLevelLayer : public LayerExt
{
	DECLARE_BUILDER(LaunchLevelLayer);
	bool init(int indexlevel);

protected:
	virtual ccMenuCallback get_callback_by_description(const std::string &name) override;
};


NS_CC_END