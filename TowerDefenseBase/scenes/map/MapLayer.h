//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __Map_h__
#define __Map_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "support.h"
#include "ml/SmartScene.h"
#include "ml/NodeExt.h"
#include "ml/ScrollMenu.h"
#include "gameboard.h"
//#include "../../../cocos2d/extensions/GUI/CCScrollView/CCScrollView.h"

//USING_NS_CC_EXT;

NS_CC_BEGIN

class MapLayer : public LayerExt, public LayerBlur
{
	struct Location
	{
		Point pos;
		Point posLock;
		Point a;
		Point b;
        bool skip;
		bool survivalMode;
		int starsForUnlock;
		int afterLevel;
		std::string unlockFrame;
		std::string unlockText;
	};

	friend class AutoPlayer;
	DECLARE_BUILDER( MapLayer );
	bool init();
public:
	static SmartScene::Pointer scene();
	static void prepairNodeByConfiguration(NodeExt* node);
	static LayerPointer createShop( int layer, int tab );

	virtual void onEnter();
	virtual void onExit();
	void cb_shop( Ref*sender, int layer, int tab );
	void cb_paidversion( Ref*sender );
	void onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event );

	bool openShopFromLaboratory();
protected:
	void displayLeaderboardScore();
	void removeUnUsedButtons();
	virtual void load( pugi::xml_node& root )override;
	Location loadLocation( const pugi::xml_node& node );
	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;
	virtual ccMenuCallback get_callback_by_description( const std::string& name )override;
	
	void scrollBegan( const std::vector<Touch*>& touch, Event * event );
	void scrollMoved( const std::vector<Touch*>& touch, Event * event );
	void scrollEnded( const  std::vector<Touch*>& touch, Event * event );
	void mouseHover(Event*);
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
	void touchEditModeBegan( const std::vector<Touch*>& touch, Event * event );
	void touchEditModeMoved( const std::vector<Touch*>& touch, Event * event );
	void touchEditModeEnded( const  std::vector<Touch*>& touch, Event * event );
#endif
	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override;
	virtual bool isBlurActive() override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;

	
	void update(float delta);
	
    void cb_specialUpgrades( Ref*sender );
	void cb_back( Ref*sender );
	void cb_lab( Ref*sender );
	void cb_lab2( Ref*sender );
	void cb_itemshop( Ref*sender );
    //
    void cb_popshop( Ref*sender);
    //
	void cb_game( Ref*sender, GameMode mode );
	void cb_gamelock( Ref*sender, int index );
	void cb_showChoose( Ref*sender, int index );
	void cb_unlock( Ref*sender );

	void runLevel( int levelIndex, GameMode mode );
	
	void showWindow( NodePointer window );
	void windowDidClosed();
	
	void openRateMeWindowIfNeeded();

	void updateFogOfWar();
	void activateLocations();
	void activateSurvivalLocations();
	float buildCurve( int index, int levelNumber, bool shopath, float startingDelay );
	mlMenuItem::Pointer createFlag( int index, int levelNumber);
	LayerPointer buildChooseWindow( int level );
	LayerPointer buildUnlockWindow( int level );
	LayerPointer buildWindowLevelLocked( const std::string& path, int index, int unlockedAfterLevel );
	
	void createPromoMenu();
	void createDevMenu();
	void showDailyReward();

private:
	NodePointer _map;
	ScrollMenu::Pointer _menuLocations;
	Vec2 _velocity;
	bool _isTouching;
    bool tutorialDispatched;
	
	std::vector<Location> _locations;
	std::vector<Location> _locationsSurvival;
	bool _updateLocations;
	bool _fogOfWarEnabled;
	bool _showLaboratoryOnEnter;
	std::vector< Node* > _curveMarkers;
	IntrusivePtr<ScrollTouchInfo> _scrollInfo;
	unsigned _selectedLevelIndex;
	bool _useDialog;
	bool _doNotHideBanner;
    
    void openAllLevels();
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
	bool _editMode;
	Point* _editPoint;
#endif
};

NS_CC_END
#endif // #ifndef Map
