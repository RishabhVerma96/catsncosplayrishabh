//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __ScoreLayer_h__
#define __ScoreLayer_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN





class ScoreLayer : public Layer, public NodeExt
{
	DECLARE_BUILDER( ScoreLayer );
	bool init();
public:
	virtual ccMenuCallback get_callback_by_description( const std::string & name ) override;
	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;
protected:
	void update( float );
	void updateScores();
	void change_time( int score );
	void change_fuel( int score );
	void change_real( int score );
	void change_star( int score );
	void change_ticket( int score );
	void cb_shop( Ref*sender );
	void change( LabelPointer label, int value );
private:
	bool _updateScores;
	bool _useActionForScore;
	int _gold;
	int _fuel;
	int _star;
	int _ticket;
	std::string _time;
	NodePointer _timeNodeAdvanced;
	LabelPointer _goldNode;
	LabelPointer _fuelNode;
	LabelPointer _timeNode;
	LabelPointer _starNode;
	LabelPointer _ticketsNode;
};




NS_CC_END
#endif // #ifndef ScoreLayer