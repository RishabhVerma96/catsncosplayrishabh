#include "BuyHeroes.h"
#include "support.h"
#include "UserData.h"
#include "Hero.h"
#include "MenuItem.h"
#include "ml/Language.h"
#include "ScoreCounter.h"
#include "shop/ShopLayer.h"
#include "ml/SmartScene.h"
#include "configuration.h"
NS_CC_BEGIN



BuyHeroes::BuyHeroes()
: _activateMenu(nullptr)
{}

BuyHeroes::~BuyHeroes()
{
#if PC == 1
#else
	ShopLayer::observerOnPurchase().remove( _ID );
#endif
}

bool BuyHeroes::init(BuyHeroMenu* menu)
{
	do
	{
		_activateMenu = menu;
		CC_BREAK_IF( !Layer::init() );
		CC_BREAK_IF( !NodeExt::init() );

		load( "ini/promo/heroeslayer.xml" );

		_time = getNodeByPath<Text>( this, "timer" );

		auto callback = [this]( inapp::SkuDetails details )
		{
			auto button = getNodeByPath<MenuItem>( this, "menu/buy" );
			if( details.result == inapp::Result::Ok )
			{
				if( button )
					button->setEnabled( true );
				_details = details;
				if( _details.result == inapp::Result::Ok )
				{
					std::string string = _details.priceText;
					auto costnode = getNodeByPath<Text>( this, "menu/buy/normal/cost" );
					if( costnode ) costnode->setString( string );

					auto setCost = [this]( Text*label, inapp::SkuDetails details )
					{
						details.prepairPriceString();
						if( label )
							label->setString( details.priceText );
					};
					setCost( getNodeByPath<Text>( this, "cost" ), _details );
					inapp::SkuDetails details2 = _details;
					details2.priceValue *= 2.f;
					setCost( getNodeByPath<Text>( this, "cost_old" ), details2 );
				}
			}
			else
			{
				if( button )
					button->setEnabled( false );
			}
		};
		if( Config::shared().get<bool>( "useHeroRoom" ) )
		{
			inapp::details( Config::shared().get( "inappPackHeroes1" ), std::bind( callback, std::placeholders::_1 ) );
		}

		runEvent( "appearance" );
		scheduleUpdate();

		EventListenerKeyboard * event = EventListenerKeyboard::create();
		event->onKeyReleased = std::bind(
			[this]( EventKeyboard::KeyCode key, Event* )mutable
		{
			if( key == EventKeyboard::KeyCode::KEY_BACK )
				this->runEvent( "disappearance" );
		}, std::placeholders::_1, std::placeholders::_2 );

		getEventDispatcher()->addEventListenerWithSceneGraphPriority( event, this );


		return true;
	}
	while( false );
	return false;
}

void BuyHeroes::setTimeString( const std::string& string )
{
	if( _time )
		_time->setString( string );
}

ccMenuCallback BuyHeroes::get_callback_by_description( const std::string & name )
{
#define callback( x ) std::bind( [this]( Ref* ){ x; }, std::placeholders::_1 )
	if( name == "buy" )
		return callback( this->buy() );
	else if( name == "linktofullversion")
		return callback( this->openstore() );
	else  if( name == "close" )
		return std::bind( [this]( Ref* ){ runEvent( "disappearance" ); }, std::placeholders::_1 );
	return LayerExt::get_callback_by_description( name );
#undef callback
}

void BuyHeroes::buy()
{
#if PC == 1
#else
	if( _details.result == inapp::Result::Ok )
	{
		auto callback = [this]( int, int, BuyHeroes* inst )
		{
			runEvent( "disappearance" );
		};
		ShopLayer::observerOnPurchase().add( _ID, std::bind( callback, std::placeholders::_1, std::placeholders::_2, this ) );
		inapp::purchase( _details.productId );
	}
#endif
}

void BuyHeroes::openstore()
{
    Application::getInstance()->openURL( Config::shared().get( "linkToStorePaidVersion" ) );
}

void BuyHeroes::update(float dt)
{
	if(_activateMenu )
		_activateMenu->update(dt);
}

/************************************************************/
//MARK:	class BuyHeroMenu
/************************************************************/

time_t BuyHeroMenu::_duration = 60 * 60 * 24;

BuyHeroMenu::BuyHeroMenu()
: _timestamp( 0 )
{}

BuyHeroMenu::~BuyHeroMenu()
{}

bool BuyHeroMenu::isShow()
{
	if( !Config::shared().get<bool>( "useHeroesPromo" ) )
		return false;

	if( Config::shared().get<bool>( "launchPromoAfterHeroroom" ) )
	{
		if( !UserData::shared().get<bool>( "heroroom_visited" ) )
			return false;
	}
	else
	{
		auto levels = UserData::shared().level_getCountPassed();
		int after = Config::shared().get<int>( "levelForLaunchPromo" );
		if( levels < after )
			return false;
	}

	int countBought( 0 );
	int nonPurchasableHeroesCount = HeroExp::shared().getNonPurchasableHeroesCount();
	for( int i = nonPurchasableHeroesCount + 1; i <= 10; ++i )
	{
		countBought += HeroExp::shared().isHeroAvailabled( "hero" + toStr(i) );
	}
	if( countBought >= Config::shared().get<int>( "boughtHeroCountDisablePromo" ) )
		return false;

	time_t timestamp = UserData::shared().get("BuyHeroMenutimestamp", 0);
	if (timestamp > 0)
	{
		time_t current;
		time( &current );
		time_t elapsed = current - timestamp;
		if( elapsed >= _duration )
			return false;
	}

	return true;
}

bool BuyHeroMenu::init()
{
	do
	{
		CC_BREAK_IF( !Menu::init() );
		CC_BREAK_IF( !NodeExt::init() );
		load( "ini/promo/heroesicon.xml" );

		_timestamp = UserData::shared().get("BuyHeroMenutimestamp", 0);
		if (_timestamp == 0)
		{
			time( &_timestamp );
			UserData::shared().write( "BuyHeroMenutimestamp", (int)_timestamp );

			runAction( Sequence::create(
				DelayTime::create( 1 ),
				CallFunc::create( [this](){openPromo( nullptr ); } ),
				nullptr ) );
				
		}

		scheduleUpdate();
		update( 0 );
		return true;
	}
	while( false );
	return false;
}

ccMenuCallback BuyHeroMenu::get_callback_by_description( const std::string & name )
{
	if( name == "open" ) return std::bind( &BuyHeroMenu::openPromo, this, std::placeholders::_1 );
	else return NodeExt::get_callback_by_description( name );
}

void BuyHeroMenu::openPromo( Ref* )
{
	auto layer = BuyHeroes::create( this );
	_heroesLayer = layer;
	auto scene = dynamic_cast<SmartScene*>(getScene());
	if( scene && layer )
	{
		auto z = layer->getLocalZOrder();
		scene->pushLayer( layer, true );
		if( z != 0 )
			layer->setLocalZOrder( z );
	}
}

void BuyHeroMenu::update( float dt )
{
	time_t current;
	time( &current );
	auto elapsed = current - _timestamp;
	auto timeleft = _duration - elapsed;
	if( timeleft > 0 )
	{
		std::string time;

		int hrs = int( timeleft ) / 3600;
		int min = (int( timeleft ) - hrs * 3600) / 60;
		int sec = int( timeleft ) - hrs * 3600 - min * 60;

		std::string H = toStr( hrs );
		std::string M = toStr( min );
		std::string S = toStr( sec );
		while( H.size() < 2 ) H = "0" + H;
		while( M.size() < 2 ) M = "0" + M;
		while( S.size() < 2 ) S = "0" + S;
		time = H + ":" + M + ":" + S;

		auto timer = getNodeByPath<Text>( this, "timer" );
		if( timer )
			timer->setString( time );

		if( _heroesLayer )
			_heroesLayer->setTimeString( time );
	}
	else
	{
		removeFromParent();
	}
}


NS_CC_END
