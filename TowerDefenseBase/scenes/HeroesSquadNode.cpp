#include "ml/MenuItem.h"
#include "ml/SmartScene.h"
#include "ml/Text.h"
#include "HeroesSquadNode.h"
#include "UserData.h"
#include "support.h"
#include "selecthero/HeroRoom2.h"
#include "game/unit/hero.h"
#include "plugins/AdsPlugin.h"
NS_CC_BEGIN





HeroesSquadNode::HeroesSquadNode()
: _showEXP(true)
{
}

HeroesSquadNode::~HeroesSquadNode()
{
	AdsPlugin::shared().observerVideoStarted.remove( _ID );
	AdsPlugin::shared().observerVideoResult.remove( _ID );
	UserData::shared().observerHeroes.remove( _ID );
}

bool HeroesSquadNode::init()
{
	do
	{
		CC_BREAK_IF( !Node::init() );
		CC_BREAK_IF( !NodeExt::init() );

		UserData::shared().observerHeroes.add( _ID, std::bind( &HeroesSquadNode::onChangeHeroes, this ) );

		xmlLoader::macros::set( "heroslot1", "1" );
		xmlLoader::macros::set( "heroslot2", "1" );
		auto heroes = UserData::shared().hero_getSelected();
		int maxselect = Config::shared().get<int>( "heroesCountSelectMax" );
		for( int i = 0; i < maxselect; ++i )
		{
			xmlLoader::macros::set( "hero" + toStr( i + 1 ) + "_isselect", toStr( static_cast<int>(heroes.size()) > i ) );
			xmlLoader::macros::set( "hero" + toStr( i + 1 ) + "_nonselect", toStr( static_cast<int>(heroes.size()) <= i ) );
			xmlLoader::macros::set( "heroslot" + toStr( i + 1 ), toStr( i < static_cast<int>(heroes.size()) ? heroes[i] + 1 : 1 ) );
		}

		return true;
	}
	while( false );
	return false;
}

void HeroesSquadNode::onEnter()
{
	Node::onEnter();
	onChangeHeroExp();
}

void HeroesSquadNode::onExit()
{
	Node::onExit();
	AdsPlugin::shared().observerVideoStarted.remove( _ID );
	AdsPlugin::shared().observerVideoResult.remove( _ID );
}

ccMenuCallback HeroesSquadNode::get_callback_by_description( const std::string & name )
{
	if( name == "changehero1" ) return std::bind( &HeroesSquadNode::heroroom, this, 0 );
	if( name == "changehero2" ) return std::bind( &HeroesSquadNode::heroroom, this, 1 );
	if( name == "video1" ) return std::bind( &HeroesSquadNode::video, this, 0 );
	if( name == "video2" ) return std::bind( &HeroesSquadNode::video, this, 1 );
	return NodeExt::get_callback_by_description( name );
}

bool HeroesSquadNode::setProperty( const std::string & stringproperty, const std::string & value )
{
	if( stringproperty == "showEXP" )
		_showEXP = strTo<bool>(value);
	else
		return NodeExt::setProperty( stringproperty, value );
	return true;
}

void HeroesSquadNode::heroroom( int slot )
{
	auto scene = dynamic_cast<SmartScene*>(getScene());
	auto layer = HeroRoom2::create();
	if( layer && scene )
	{
		scene->pushLayer( layer, true );
		UserData::shared().hero_setActiveSlot( slot );
	}
}

void HeroesSquadNode::video( int slot )
{
	auto heroes = UserData::shared().hero_getSelected();
	if( slot < static_cast<int>(heroes.size()) )
	{
		AdsPlugin::shared().observerVideoStarted.add( _ID, std::bind( [](){} ) );
		AdsPlugin::shared().observerVideoResult.add( _ID, std::bind( &HeroesSquadNode::videoResult, this, slot, std::placeholders::_1 ) );
		AdsPlugin::shared().showVideo();
	}
	else
	{
		MessageBox( "Please choose hero before", "" );
	}
}

void HeroesSquadNode::videoResult( int slot, bool result )
{
	if( result )
	{
		auto heroes = UserData::shared().hero_getSelected();
		auto heroname = "hero" + toStr( heroes[slot] + 1 );
		float exp = HeroExp::shared().getEXP( heroname );
		int level = HeroExp::shared().getLevel( exp );
		//if( level < 15 )
		{
			float exp2 = level > 0 ? HeroExp::shared().getHeroLevelExtValue( level - 1 ) : 0;
			float exp3 = level < static_cast<int>(HeroExp::shared().getMaxLevel()) ?
				HeroExp::shared().getHeroLevelExtValue( level ) :
				exp2;
			exp = exp + (exp3 - exp2) * 0.1f;
			HeroExp::shared().setEXP( heroname, exp );
		}
		onChangeHeroExp();
	}
}

void HeroesSquadNode::onChangeHeroes()
{
	auto heroes = UserData::shared().hero_getSelected();
	int maxselect = Config::shared().get<int>( "heroesCountSelectMax" );
	for( int i = 0; i < maxselect; ++i )
	{
		auto hero = getNodeByPath<mlMenuItem>( this, "hero" + toStr( i + 1 ) + "/hero" );
		auto add = getNodeByPath( this, "hero" + toStr( i + 1 ) + "/add" );
		auto change = getNodeByPath( this, "hero" + toStr( i + 1 ) + "/change" );
		hero->setVisible( static_cast<int>(heroes.size()) > i );
		change->setVisible( static_cast<int>(heroes.size()) > i );
		add->setVisible( static_cast<int>(heroes.size()) <= i );
		if( static_cast<int>(heroes.size()) > i )
		{
			std::string dir = getParamCollection().get( "pathtoiconsfolder" );
			if( dir.empty() )
				dir = "images/map/choose/icon_heroes/";
			auto image = dir + "hero" + toStr( heroes[i] + 1 ) + ".png";
			hero->setImageNormal( image );
		}
	}
}

void HeroesSquadNode::onChangeHeroExp()
{
	auto heroes = UserData::shared().hero_getSelected();
	int maxselect = Config::shared().get<int>( "heroesCountSelectMax" );
	for( int i = 0; i < maxselect; ++i )
	{
		if( i >= (int)heroes.size() )
		{
			auto node = getNodeByPath( this, "hero" + toStr( i + 1 ) + "/xp" );
			if( node )
				node->setVisible( false );
			continue;
		}
		auto heroname = "hero" + toStr( heroes[i] + 1 );

		float exp = HeroExp::shared().getEXP( heroname );
		int levelvalue = HeroExp::shared().getLevel( exp );
		float exp2 = levelvalue > 0 ? HeroExp::shared().getHeroLevelExtValue( levelvalue - 1 ) : 0;
		float exp3 = levelvalue < static_cast<int>(HeroExp::shared().getMaxLevel()) ?
			HeroExp::shared().getHeroLevelExtValue( levelvalue ) :
			exp2;
		exp = exp - exp2;
		exp2 = levelvalue < 15 ? exp3 - exp2 : exp2;

		auto heroNode = getNodeByPath( this, "hero" + toStr( i + 1 ) );
		auto progress = getNodeByPath<Node>( heroNode, "xp" );
		auto progressText = getNodeByPath<Text>( heroNode, "xp/text" );
		auto progressNode = getNodeByPath( heroNode, "xp/bar" );
		if( progress )
		{
			progress->setVisible( _showEXP && levelvalue < 15 );
		}
		if( progressNode )
		{
			auto action = Sequence::create(
				DelayTime::create( 0.f ),
				ScaleTo::create( 2.f, exp / exp2, 1.f ),
				nullptr );
			progressNode->runAction( action );
		}
		if( progressText )
		{
			int a = strTo<int>( progressText->getString() );
			int b = exp;
			auto actionText = ActionText::create( 2.f, b, true, "", "/" + toStr<int>( exp2 ) );
			auto action = Sequence::create(
				DelayTime::create( 0.f ),
				actionText.ptr(),
				nullptr );
			progressText->runAction( action );
		}
	}
}


NS_CC_END