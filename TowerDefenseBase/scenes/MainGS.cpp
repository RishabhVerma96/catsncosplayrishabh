//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//

#include "MainGS.h"
#include "LayerLoader.h"
#include "ml/ImageManager.h"
#include "ml/Audio/AudioEngine.h"
#include "ml/Language.h"
#include "ml/loadxml/xmlProperties.h"
#include "GameLayer.h"
#include "MenuItemTextBG.h"
#include "resources.h"
#include "consts.h"
#include "MapLayer.h"
#include "Achievements.h"

#include "Log.h"
#include "UserData.h"
#include "ScoreCounter.h"

#include "MenuCreateTower.h"
#include "lab/Laboratory.h"
#include "shop/ShopLayer.h"
#include "Tower.h"
#include "VictoryMenu.h"
#include "inapp/Purchase.h"
#include "appgratis/appgratis.h"
#include "SmartScene.h"
#include "configuration.h"
#include "dosads/DOSAds.h"
#include "deltadna/deltadna.h"
#include "plugins/AdsPlugin.h"
#include "inapp/Purchase.h"
#include "fyber/fyber.h"
#include "gamegs/GamePauseScene.h"
#include "gamegs/GamePopUpScene.h"
#include "flurry/flurry_.h"
#include "chartboost/chartboost_.h"
#include "EulaLayer.h"
#include "editor/TestScene.h"
#include "AboutLayer.h"
#include "ml/Share/ShareHandler.h"
#include "../Services/sdkbox/play/sdkboxPlay.h"

/*
*/
extern bool iPad;
NS_CC_BEGIN;

class AppgratisRedeemUnlocker : public Node, public NodeExt
{
	DECLARE_BUILDER( AppgratisRedeemUnlocker );
	bool init( MainGS* mainLayer )
	{
		do
		{
			CC_BREAK_IF( !Node::init() );
			CC_BREAK_IF( !NodeExt::init() );
			CC_BREAK_IF( !mainLayer );

			_mainLayer = mainLayer;

			appgratis::setCallback( std::bind( &AppgratisRedeemUnlocker::onRedeem, this, std::placeholders::_1 ) );
			scheduleUpdate();
			return true;
		}
		while( false );
		return false;
	}

public:
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override
	{
		if( name == "close_redeem_win" )
		{
			assert( _redeemLayer );
			return std::bind( &LayerExt::disappearance, _redeemLayer.ptr() );
		}
		return NodeExt::get_callback_by_description( name );
	}


	void onRedeem( const ParamCollection & pc )
	{
		_mutex.lock();
		_params.push_back( pc );
		_mutex.unlock();
	}

	void update( float dt )
	{
		_mutex.lock();
		if( _mainLayer->isLoadedResources() )
		{
			for( auto& pc : _params )
			{
				unlock( pc );
			}
			_params.clear();
		}
		_mutex.unlock();
	}

	void unlock( const ParamCollection & pc )
	{
		std::string redeemName;
		for( auto pair : pc )
		{
			redeemName = pair.first;
			int redeem = UserData::shared().get <int>( redeemName );
			if( redeem == 1 )
				continue;

			UserData::shared().write( redeemName, 1 );

			if( redeemName == "Pro" || redeemName == "PRO" || redeemName == "pro" )
			{
				xmlLoader::bookDirectory( this );
				_redeemLayer = LayerExt::create();
				_redeemLayer->load( "ini/appgratis_redeem_win.xml" );
				xmlLoader::unbookDirectory( this );

				SmartScene * scene = (SmartScene*)getScene();
				scene->pushLayer( _redeemLayer, true );

				int gold = strTo<int>( _redeemLayer->getParamCollection().get( "gold", "10000" ) );
				int fuel = strTo<int>( _redeemLayer->getParamCollection().get( "fuel", "10000" ) );
				std::string action = _redeemLayer->getParamCollection().get( "action", "max" );
				if( action == "add" )
				{
					ScoreCounter::shared().addMoney( kScoreCrystals, gold, true );
					ScoreCounter::shared().addMoney( kScoreFuel, fuel, true );
				}
				else if( action == "max" )
				{
					int curr = ScoreCounter::shared().getMoney( kScoreCrystals );
					curr = std::max( gold, curr );
					ScoreCounter::shared().setMoney( kScoreCrystals, curr, true );
					ScoreCounter::shared().setMoney( kScoreFuel, fuel, true );
				}
				UserData::shared().write( k::user::UnShowAd, 1 );
				UserData::shared().write( "PRO_SHOWED", 1 );
#ifndef _DEBUG
				UserData::shared().save();
#endif
			}
		}
	}

private:
	std::mutex _mutex;
	std::vector<ParamCollection> _params;
	MainGS* _mainLayer;
	LayerExt::Pointer _redeemLayer;
};

AppgratisRedeemUnlocker::AppgratisRedeemUnlocker() {}
AppgratisRedeemUnlocker::~AppgratisRedeemUnlocker() {}


void printNodes()
{
	//	auto nodes = getNodesList();
	//	int index(0);
	//	for( auto i : nodes )
	//	{
	//		log( "[%d]: 0x%x ->refcount(%d) -> %d ", index++, int(long(i)), i->getReferenceCount(), i->_ID );
	//	}
}

MainGS::MainGS()
: m_menu( nullptr )
, m_menuAudio( nullptr )
, m_resourceIsLoaded( false )
{}

MainGS::~MainGS()
{}

ScenePointer MainGS::scene()
{
	__push_auto( "MainGS::scene" );
	auto layer = MainGS::create();
	auto scene = SmartScene::create( layer );
	scene->setName( kMainGSSceneName );

	scene->runAction( Sequence::create( DelayTime::create( 0.1f ), CallFunc::create( [scene]()mutable{
		auto eula = EulaLayer::create();
		if( eula )
			scene->pushLayer( eula, true );
	} ), nullptr ) );

	return scene;
}

bool MainGS::init()
{
	do
	{
		mlTowersInfo::shared();
		CC_BREAK_IF( !LayerExt::init() );

		ImageManager::shared().load_plist( "images/logo.plist", "logo" );

		NodeExt::load( "ini/maings", "mainlayer.xml" );
		m_menu.reset( getChildByName<Menu*>( "mainmenu" ) );

		m_menu->setEnabled( false );

		runEvent( "oncreate" );
        
        if (!Config::shared().get<bool>( "hideMainLogo" )) {
            runEvent("showLogo");
        } else {
            runEvent("showTitle");
        }

		loadResources( std::bind( &MainGS::onResourcesDidLoaded, this ) );
		
		if( Config::shared().get<bool>( "useRestoreButton" ) == false )
		{
			auto node = getNodeByPath(this, "mainmenu/restore");
			if( node )node->setVisible( false );
		}

#if PC == 1
		auto settings = getNodeByPath( this, "menusettings" );
		if( settings )settings->setVisible( true );
#else
#	if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
		auto promo = getNodeByPath( this, "menupromo" );
		if( promo )
			promo->setVisible( true );
#	endif
#endif

		setDisapparanceOnBackButton();
		auto batch = AppgratisRedeemUnlocker::create(this);
		addChild( batch );

		if( isTestDevice() )
		{
			createDevMenu();
		}

		return true;
	}
	while( false );
	return false;
}

void MainGS::load( pugi::xml_node & root )
{
	NodeExt::load( root );

	auto xmlRes = root.child( "resources" );
	auto xml_tex = xmlRes.child( "textures" );
	auto xml_atl = xmlRes.child( "atlases" );
	for( auto child : xml_tex )
	{
		m_loadingList.resources.push_back( child.attribute( "path" ).as_string() );
	}
	for( auto child : xml_atl )
	{
		std::string path = child.attribute( "path" ).as_string();
		std::string name = child.attribute( "name" ).as_string();
		m_loadingList.atlases.push_back( std::pair<std::string, std::string>( path, name ) );
	}
}

ccMenuCallback MainGS::get_callback_by_description( const std::string & name )
{
	if( name == "pushGame" ) return CC_CALLBACK_1( MainGS::pushGame, this );
    if( name == "popUp" ) return CC_CALLBACK_1(MainGS::popUp, this);
    if( name == "shareButtonPressed" ) return CC_CALLBACK_1( MainGS::shareButtonPressed, this );
    if( name == "leaderboardButtonPressed" ) return CC_CALLBACK_1( MainGS::leaderboardButtonPressed, this );
    if( name == "restoreButtonPressed" ) return CC_CALLBACK_1( MainGS::restoreButtonPressed, this );
	if( name == "showvideo" )
	{
		return std::bind( [](Ref*){
			AdsPlugin::shared().showVideo();
		}, std::placeholders::_1 );
	}
	//if (name == "moreApps") return std::bind([](Ref*) { AdsPlugin::shared().showMoreApps(); }, std::placeholders::_1);
	if( name == "exit" ) return std::bind( []( Ref* ) { Director::getInstance()->end(); }, std::placeholders::_1 );
	if( name == "about" ) return [this]( Ref * ){
		AboutLayer * const about = AboutLayer::create();
		if( about )
			static_cast<SmartScene *>(this->getScene())->pushLayer( about, true );
	};
	if (name == "restore") return std::bind([this](Ref*)
	{
		auto callback = [this]( int, int )
		{
			if( _blockLayer )
			{
				_blockLayer->removeFromParent();
				_blockLayer.reset(nullptr);
			}
		};
#if PC == 1
#else
		ShopLayer::observerOnPurchase().add( _ID, std::bind( callback, std::placeholders::_1, std::placeholders::_2 ) );
		inapp::restore("");
#endif
		
		
		SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
		_blockLayer = LayerExt::create();
		auto sprite = ImageManager::shared().sprite("images/loading.png");
		_blockLayer->addChild(sprite);
		sprite->runAction(RepeatForever::create(RotateBy::create(1, 360)));
		sprite->setPosition( Point(Director::getInstance()->getOpenGLView()->getDesignResolutionSize()/2) );
		if( scene )
			scene->pushLayer( _blockLayer, true );

		scene->runAction(Sequence::create(
		   DelayTime::create(10),
		   CallFunc::create(std::bind( callback, 0, 0 )),
		   nullptr ));

	}, std::placeholders::_1);

	if( name == "settings" )
	{
		auto cb = [this](Ref*)
		{
			xmlLoader::bookDirectory( this );
			auto settings = GamePauseLayer::create( nullptr, "ini/maings/settings.xml", false );
			auto scene = static_cast<SmartScene*>(getScene());
			if( scene && settings )
				scene->pushLayer( settings, true );
			xmlLoader::unbookDirectory( this );
		};
		return std::bind( cb, std::placeholders::_1 );
	}

	if (name.find("openurl:") == 0)
	{
		auto urlName = name.substr(strlen("openurl:"));
		auto url = Config::shared().get(urlName);

		auto cb = [urlName, url] (Ref*)
		{
			ParamCollection pc;
			pc["event"] = "promo_button";
			pc["link"] = urlName;
			flurry::logEvent(pc);
            Application::getInstance()->openURL( url );
		};

		return cb;
	}

	return nullptr;
}

void MainGS::onEnter()
{
	__push_auto_check( "MainGS::onEnter" );
	LayerExt::onEnter();
	AudioEngine::shared().playMusic( kMusicMainMenu );

	setKeyboardEnabled( true );
}

void MainGS::disappearance()
{
	Director::getInstance()->end();
}

void MainGS::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
}

bool MainGS::isLoadedResources()const
{
	return m_resourceIsLoaded;
}

bool MainGS::isBlurActive()
{
	return !isRunning();
}

void MainGS::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

void MainGS::loadResources( std::function<void()> callback )
{
	if( m_loadingList.resources.empty() == false ||
		m_loadingList.atlases.empty() == false )
	{
		auto layer = LayerLoader::create( m_loadingList.resources, callback );
		layer->addPlists( m_loadingList.atlases );
		addChild( layer, 999 );
		layer->setVisible( false );
		layer->setTag( 999 );
		layer->loadCurrentTexture();
	}
	else
	{
		runAction( CallFunc::create( [this](){ onResourcesDidLoaded(); } ) );
	}
}

void MainGS::pushGame( Ref* )
{
	//AdsPlugin::shared().showInterstitialBanner();
	//return;
	if( m_menu->isEnabled() == false ) return;
	m_menu->setEnabled( false );
	if( m_resourceIsLoaded )
		onResourcesDidLoaded_runMap();
	else
		loadResources( std::bind( &MainGS::onResourcesDidLoaded_runMap, this ) );

}

void MainGS::popUp( Ref* )
{
    CCLOG("Pop up clicked");
    
    xmlLoader::bookDirectory( this );
    auto PopUp = GamePopUpLayer::create("ini/maings/popup.xml");
    auto scene = static_cast<SmartScene*>(getScene());
    if( scene && PopUp )
        scene->pushLayer( PopUp, true );
    xmlLoader::unbookDirectory( this );
    
    if( m_menu->isEnabled() == false ) return;
    m_menu->setEnabled( true );
}



void MainGS::shareButtonPressed( Ref* )
{
//
//    int videoCount = UserData::shared().getWatchedVideoCountForHero("hero1");
//    videoCount = UserData::shared().incrementWatchedVideoCountForHero("hero1");
//    videoCount = UserData::shared().getWatchedVideoCountForHero("hero1");
//    int hero1 = HeroExp::shared().getRequiredVideoCountToUnlockHero("hero1");
//    int hero2 = HeroExp::shared().getRequiredVideoCountToUnlockHero("hero2");
//    AdsPlugin::shared().showVideo();
    ShareHandler::getInstance()->showSharePopup();
}

void MainGS::leaderboardButtonPressed( Ref* )
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    sdkbox::play::showLeaderBoard();
#endif
}

void MainGS::restoreButtonPressed( Ref* )
{
    
}

void MainGS::onResourcesDidLoaded()
{
	m_menu->setEnabled( true );
	removeChildByTag( 999 );
	
	if( m_resourceIsLoaded == false )
	{
        //hide "More" button if needed
        if (Config::shared().get<bool>( "hideMoreButton" ) == false )
        {
            runEvent( "resourcesloaded2" );
        }

		runEvent( "resourcesloaded" );
	}

	m_resourceIsLoaded = true;

	//deltadna::requestEngage("chooseAds");

}

void MainGS::createDevMenu()
{
	if( isTestDevice() == false )
		return;
	auto menu = Menu::create();
	menu->setPosition( 0, 0 );
	addChild( menu );

	static auto activator = []( Ref*sender )
	{
		static int counter = 0;
		if( isTestModeActive() == false )
		{
			if( ++counter == 10 )
			{
				counter = 0;
				setTestModeActive( true );
				static_cast<Node*>(sender)->getParent()->setOpacity( 255 );
			}
		}
		else
		{
			if( ++counter == 5 )
			{
				counter = 0;
				setTestModeActive( false );
				static_cast<Node*>(sender)->getParent()->setOpacity( 0 );
			}
		}
	};
	auto userdata = [](Ref*)
	{
		UserData::shared().clear();
	};
	auto testscene = [](Ref*)
	{
		Director::getInstance()->pushScene( TestScene::create() );
	};
	
	auto i = MenuItemTextBG::create( "++", Color4F::GRAY, Color3B::BLACK, std::bind( activator, std::placeholders::_1 ) );
	auto user = MenuItemTextBG::create( "clear UD", Color4F::GRAY, Color3B::BLACK, std::bind( userdata, std::placeholders::_1 ) );
	auto test = MenuItemTextBG::create( "test scene", Color4F::GRAY, Color3B::BLACK, std::bind( testscene, std::placeholders::_1 ) );
	i->setScale( 5 );
	i->setPosition( 10, 10 );
	i->setCascadeOpacityEnabled( true );
	menu->addChild( i );

	test->setPosition( Point( 50, 200 ) );
	test->setCascadeOpacityEnabled( true );
	menu->addChild( test );
	user->setPosition( Point(50, 100) );
	user->setCascadeOpacityEnabled( true );
	menu->addChild( user );
	menu->setCascadeOpacityEnabled(true);
	if( isTestModeActive() )
		menu->setOpacity( 255 );
	else
		menu->setOpacity( 0 );
}

void MainGS::onResourcesDidLoaded_runMap()
{
	AdsPlugin::shared().cacheVideo();
	AdsPlugin::shared().cacheInterstitial();
	onResourcesDidLoaded();
	auto scene = MapLayer::scene();
	Director::getInstance()->pushScene( scene );
}

void MainGS::rateme()
{}


NS_CC_END;
