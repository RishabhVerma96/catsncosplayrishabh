#include "LoadLevelScene.h"
#include "LayerLoader.h"
#include "consts.h"
#include "gamegs/GameLayer.h"
#include "gamegs/GameScene.h"
#include "ml/ImageManager.h"
#include "support/UserData.h"
#include "game/tower.h"
#include "online/OnlineConnector.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include <android/log.h>
#define  LOG_TAG    "LayerLoader"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#else
#define LOGD(...) cocos2d::log(__VA_ARGS__)
#endif

NS_CC_BEGIN

static LoadLevelScene* LoadLevelSceneInst(nullptr);

void LoadLevelScene::parceLevel( std::set<std::string>&out, GameMode mode, int index )
{
	pugi::xml_document doc;
	std::string mapFile;
	if( mode != GameMode::multiplayer )
		mapFile = "map" + toStr( index ) + ".xml";
	else
		mapFile = "online_" + toStr( index ) + ".xml";
	std::string pathToFile = FileUtils::getInstance()->fullPathForFilename( kDirectoryToMaps + mapFile );
	doc.load_file( pathToFile.c_str() );

	auto xmlTagWaves =
		mode == GameMode::normal ? k::xmlTag::LevelWaves :
		mode == GameMode::hard ? k::xmlTag::LevelWavesHard :
		mode == GameMode::multiplayer ? k::xmlTag::LevelWaves :
		/*evelMode == GameMode::surv*/ k::xmlTag::LevelWavesSurvival;

	auto xmlWaves = doc.root().first_child().child( xmlTagWaves );

	auto getUnitName = []( const pugi::xml_node& node )
	{
		std::string unit;
		auto def = node.attribute( "defaultname" );
		auto name = node.attribute( "name" );
		if( def ) unit = def.as_string();
		if( name )unit = name.as_string();
		return unit;
	};

	for( auto wave : xmlWaves )
	{
		auto name = getUnitName( wave );
		if( name.empty() == false )
			out.insert( name );
		for( auto unit : wave )
		{
			auto name = getUnitName( unit );
			if( name.empty() == false )
				out.insert( name );
		}
	}
}

LoadLevelScene::LoadLevelScene()
: _levelIndex( -1 )
, _levelMode( GameMode::normal )
, _popSceneOnEnter(false)
, _runSessionWithBot(false)
{
	setName( "LoadLevelScene" );
	LoadLevelSceneInst = this;
}

LoadLevelScene::~LoadLevelScene()
{
	ImageManager::shared().unloadReservedPlists();
	LoadLevelSceneInst = nullptr;
}

bool LoadLevelScene::init( int level, GameMode mode, bool sessionWithBot )
{
	do
	{
		auto layer = Layer::create();
		layer->setName( "mainlayer" );
		CC_BREAK_IF( !SmartScene::init( layer ) );
		_levelIndex = level;
		_levelMode = mode;

		//TODO: debug
		//_levelIndex = 0;
		//_levelMode = GameMode::multiplayer;
		//

		bool multiplayer = _levelMode == GameMode::multiplayer;

		std::string const path = multiplayer ? "ini/gamescene/online_loading.xml" : "ini/gamescene/loading.xml";

		if( multiplayer )
		{
			_runSessionWithBot = sessionWithBot;
			if( _runSessionWithBot )
				generateBotInfo();

			xmlLoader::macros::set( "opponent_name", OnlineConnector::shared().getOpponentNickname() );
			xmlLoader::macros::set( "opponent_score", toStr( OnlineConnector::shared().getOpponentScore() ) );
			xmlLoader::macros::set( "player_name", OnlineConnector::shared().getPlayerNickname() );
			xmlLoader::macros::set( "player_score", toStr( OnlineConnector::shared().getPlayerScore() ) );
			xmlLoader::macros::set( "bet", "50" );
			xmlLoader::macros::set( "reward", "90" );
		}

		createLoading();
		load( path );
		checkContent();
		parceLevel( _units, _levelMode, _levelIndex );
		loadResources();

		auto duration = strTo<float>( getParamCollection().get( "minloading_duration", "-1" ) );
		_loader->setMinimalLoadingDuration( duration );

		return true;
	}
	while( false );
	return false;
}

LoadLevelScene* LoadLevelScene::getInstance()
{
	return LoadLevelSceneInst;
}

void LoadLevelScene::loadInGameResources( const std::string& packname )
{
	auto pack = _resourcePacks.find( packname );
	if( pack == _resourcePacks.end() )
		return;
	
	for( auto res : pack->second )
	{
		Texture2D::setDefaultAlphaPixelFormat(Texture2D::PixelFormat::RGBA4444);
		ImageManager::shared().load_plist( res.first, res.second );
		Texture2D::setDefaultAlphaPixelFormat(Texture2D::PixelFormat::RGBA8888);
		ImageManager::shared().addUnloadPlist( res.second );
	}
}

void LoadLevelScene::onEnter()
{
	Scene::onEnter();
	if( _popSceneOnEnter )
	{
		if( _loader )
			_loader->removeFromParent();
		auto delay = DelayTime::create( 2 );
		auto call = CallFunc::create( [](){ Director::getInstance()->popScene();} );
		runAction( Sequence::createWithTwoActions(delay, call) );
	}
}

bool LoadLevelScene::loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )
{
	if( tag == "resources" )
	{
		for( auto xmlPack : xmlnode )
		{
			std::vector< std::pair<std::string, std::string> > pack;
			for( auto xmlRes : xmlPack )
			{
				std::string name = xmlRes.attribute( "name" ).as_string();
				std::string path = xmlRes.attribute( "path" ).as_string();
				pack.emplace_back( path, name );
			}
			std::string packName = xmlPack.attribute( "name" ).as_string();
			_resourcePacks[packName] = pack;
		}
	}
	else
		return NodeExt::loadXmlEntity( tag, xmlnode );
	return true;
}

void LoadLevelScene::checkContent()
{
	auto contentNode = getNodeByPath( this, "content" );
	if( !contentNode || contentNode->getChildrenCount() == 0 )
		return;
	const auto& children = contentNode->getChildren();
	auto count = children.size();
	size_t index = static_cast<size_t>(rand()) % count;
	auto node = children.at( index );
	node->setVisible( true );
	auto heroname = node->getName();
	auto& info = mlUnitInfo::shared().info( "hero/" + heroname );
	auto text_atk = getNodeByPath<Text>( node, "stats/atk/value" );
	auto text_arm = getNodeByPath<Text>( node, "stats/arm/value" );
	auto text_vlc = getNodeByPath<Text>( node, "stats/vlc/value" );
	auto text_hlt = getNodeByPath<Text>( node, "stats/hlt/value" );
	if( text_atk )text_atk->setString( toStr<int>( info.damage ) );
	if( text_arm )text_arm->setString( toStr<int>( info.armor ) );
	if( text_vlc )text_vlc->setString( toStr<int>( info.velocity ) );
	if( text_hlt )text_hlt->setString( toStr<int>( info.health ) );
}

void LoadLevelScene::createLoading()
{
	_loader = LayerLoader::create( std::vector<std::string>(), std::bind( &LoadLevelScene::onLoadingFinished, this ), _levelMode == GameMode::multiplayer && !_runSessionWithBot );
	_loader->setName( "loader" );
	pushLayer( _loader, true );
}

void LoadLevelScene::loadResources()
{
	auto addPlist = [this]( const std::string& name )
	{
		auto pack = _resourcePacks.find( name );
		if( pack == _resourcePacks.end() )
		{
#if USE_CHEATS == 1
			MessageBox( name.c_str(), "Unknow creep" );
#endif
			return;
		}
		_loader->addPlists( pack->second );
		for( auto res : pack->second )
		{
			ImageManager::shared().addUnloadPlist( res.second );
		}
	};

	for( auto& unit : _units )
	{
		if( unit.empty() )
			continue;
		addPlist( unit );
	}

	std::vector<unsigned> heroes = UserData::shared().hero_getSelected();
	addPlist( "game" );
	for( auto hero : heroes )
		addPlist( "hero" + toStr( hero + 1 ) );
	_loader->loadCurrentTexture();
}

void LoadLevelScene::onLoadingFinished()
{
	auto scene = GameScene::create( _levelIndex, _levelMode, _runSessionWithBot );
	Director::getInstance()->pushScene( scene );
	_popSceneOnEnter = true;
	
	if( _loader ) 
		_loader->setVisible( false );
}

void LoadLevelScene::generateBotInfo()
{
	std::vector<std::string> first;
	std::vector<std::string> last;
	pugi::xml_document doc;
	doc.load_file( "ini/multiplayer/bots.xml" );
	auto xmlfirst = doc.root().first_child().child( "names" ).child( "first" );
	auto xmllast = doc.root().first_child().child( "names" ).child( "last" );
	for( auto child : xmlfirst )first.push_back( child.attribute( "value" ).as_string() );
	for( auto child : xmllast )last.push_back( child.attribute( "value" ).as_string() );
	float scatterRating = doc.child( "rating" ).attribute( "scatter" ).as_float() * 0.01f;

	auto name = first[rand() % first.size()] + " " + last[rand() % last.size()];
	auto rating = (1.f + CCRANDOM_MINUS1_1() * scatterRating) * OnlineConnector::shared().getPlayerScore();

	OnlineConnector::shared().setOpponentName(name);
	OnlineConnector::shared().setOpponentScore( rating );
}


NS_CC_END