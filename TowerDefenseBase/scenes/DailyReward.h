#pragma once

#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"

NS_CC_BEGIN

class DailyReward : public LayerExt {
public:
	enum GiftType // Do not modify old values, only add
	{
		Gold = 0,
		Fuel = 1,
		Ticket = 6,
		Item1 = 7,
		Item2 = 8,
		Item3 = 9,
		Item4 = 10,
		Item5 = 11,
		Item6 = 12,
	};

	struct Gift
	{
		GiftType type;
		int count;

		Gift( GiftType iType = Gold, int iCount = 0 ) : type( iType ), count( iCount ) {}
	};

public:
	DECLARE_BUILDER( DailyReward );
	bool init();

	static std::tm getTimeToNight();
	static std::tm getTimeToNext24hGift();
	static bool isGifted24hPassed();
	static bool isGiftedToday();
	static bool isGiftedYesterday();
	static int getComboCount();
	static time_t getLastGiftTime();
	static void incComboCount();
	static void resetComboCount();

	void giveGift();
	virtual Gift generateGift() = 0;

protected:
	std::vector<Gift> _gifts;
	Gift _resultGift;
};

class DailyRewardInstant : public DailyReward
{
public:
	DECLARE_BUILDER( DailyRewardInstant );
	bool init();
	void cb_close(Ref *);

private:
	void playIntro();
	void playOutro();
	virtual DailyReward::Gift generateGift();

private:
};


class DailyRewardRoulette : public DailyReward
{
public:
	DECLARE_BUILDER( DailyRewardRoulette );
	bool init();

private:
	virtual DailyReward::Gift generateGift();
	virtual void update( float dt ) override;
	virtual ccMenuCallback get_callback_by_description( std::string const & name ) override;

private:
	IntrusivePtr<Label> _timer;
	IntrusivePtr<MenuItem> _button;
	float _giftAngle;
	int _giftNum;
	float _spinTimeCount = 5.0f;
	float _spinDx0 = -0.1f;
	float _spinDx1 = 0.05f;
};

NS_CC_END