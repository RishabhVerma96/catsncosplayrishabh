//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 08.12.15.
//
//
#include "ml/Language.h"
#include "TowerDescriptionLayer.h"
#include "UserData.h"
#include "game/tower.h"
#include "ScoreCounter.h"
#include "EasterEggsHandler.h"

NS_CC_BEGIN

TowerDescriptionLayer::TowerDescriptionLayer()
: _callbackUpgrade()
{
}

TowerDescriptionLayer::~TowerDescriptionLayer( )
{
}

bool TowerDescriptionLayer::init( std::string const & tower, std::function<bool()> upgradeCallback, const std::string& pathToXml )
{
	do
	{
		CC_BREAK_IF( !Config::shared().get<bool>("lab_usepopup") );
		CC_BREAK_IF( !LayerExt::init() );
        
		setDisapparanceOnBackButton();

		maxUpgradeLevel = mlTowersInfo::shared().get_max_level( tower );
		
		_callbackUpgrade = upgradeCallback;

		int const curLevel = UserData::shared().tower_upgradeLevel( tower );
		int const nxtLevel = std::min( curLevel + 1, maxUpgradeLevel );
		bool const upgrade_availabled = curLevel < maxUpgradeLevel;
		int const price = mlTowersInfo::shared().getCostLab( tower, nxtLevel );
		int const money = ScoreCounter::shared().getMoney( kScoreCrystals );
		bool const enoughMoney = money >= price;

        int multiplier = 1;
        if (tower == "tower_gun")
        {
            bool isEasterEggDoubleDamageUnlcoked = EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kDoubleDamageMageTower);
            if (isEasterEggDoubleDamageUnlcoked)
            {
                multiplier=2;
            }
        }

		int const dmg = mlTowersInfo::shared().get_dmg( tower, curLevel )*multiplier;
		int const rng = mlTowersInfo::shared().get_rng( tower, curLevel );
		int const rte = mlTowersInfo::shared().get_spd( tower, curLevel );

		int const dmgInc = mlTowersInfo::shared().get_dmg( tower, nxtLevel )*multiplier - dmg;
		int const rngInc = mlTowersInfo::shared().get_rng( tower, nxtLevel ) - rng;
		int const rteInc = mlTowersInfo::shared().get_spd( tower, nxtLevel ) - rte;

		xmlLoader::macros::set( "tower_name", tower );
		xmlLoader::macros::set( "tower_name_localised", WORD( tower + "_name" ) );
		xmlLoader::macros::set( "tower_level", toStr( curLevel ) );
		xmlLoader::macros::set( "tower_levelnext", toStr( nxtLevel ) );
		xmlLoader::macros::set( "tower_dmg", toStr( dmg ) );
		xmlLoader::macros::set( "tower_rng", toStr( rng ) );
		xmlLoader::macros::set( "tower_rte", toStr( rte ) );
		xmlLoader::macros::set( "tower_dmg_inc", dmgInc > 0 ? "+" + toStr( dmgInc ) : "" );
		xmlLoader::macros::set( "tower_rng_inc", rngInc > 0 ? "+" + toStr( rngInc ) : "" );
		xmlLoader::macros::set( "tower_rte_inc", rteInc > 0 ? "+" + toStr( rteInc ) : "" );
		xmlLoader::macros::set( "tower_dmg_vis", toStr( dmgInc > 0 ) );
		xmlLoader::macros::set( "tower_rng_vis", toStr( rngInc > 0 ) );
		xmlLoader::macros::set( "tower_rte_vis", toStr( rteInc > 0 ) );
		xmlLoader::macros::set( "tower_price", toStr( price  ) );
		xmlLoader::macros::set( "upgrade_availabled", toStr( upgrade_availabled ) );
		xmlLoader::macros::set( "enough_money", toStr( enoughMoney ) );

		NodeExt::load( pathToXml );

		xmlLoader::macros::erase( "tower_name" );
		xmlLoader::macros::erase( "tower_level" );
		xmlLoader::macros::erase( "tower_dmg" );
		xmlLoader::macros::erase( "tower_rng" );
		xmlLoader::macros::erase( "tower_rte" );
		xmlLoader::macros::erase( "tower_dmg_inc" );
		xmlLoader::macros::erase( "tower_rng_inc" );
		xmlLoader::macros::erase( "tower_rte_inc" );
		xmlLoader::macros::erase( "tower_price" );
		xmlLoader::macros::erase( "upgrade_availabled" );
		xmlLoader::macros::erase( "enough_money" );

		return true;
	}
	while( false );

	return false;
}

ccMenuCallback TowerDescriptionLayer::get_callback_by_description( const std::string & name )
{
	if( name == "upgrade" ) return std::bind( &TowerDescriptionLayer::cb_upgrade, this );
	return LayerExt::get_callback_by_description( name );
}

void TowerDescriptionLayer::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
}

bool TowerDescriptionLayer::isBlurActive()
{
	return !isRunning();
}

void TowerDescriptionLayer::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

void TowerDescriptionLayer::cb_upgrade()
{
	if( _callbackUpgrade() )
		disappearance();
}



NS_CC_END
