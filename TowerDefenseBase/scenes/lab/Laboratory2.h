//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 08.12.15.
//
//
#ifndef __Laboratory2_h__
#define __Laboratory2_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN





class Laboratory2 : public LayerExt, public LayerBlur
{
	DECLARE_BUILDER( Laboratory2 );
	bool init();
public:
	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;
	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;
protected:
	virtual bool isBlurActive() override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;

	void buildItems();
	void showDescription( const std::string& name );
	bool upgradeTower( const std::string& name );
private:
	bool _useDialog;
};




NS_CC_END
#endif // #ifndef Laboratory2