//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 08.12.15.
//
//
#include "ml/ScrollMenu.h"
#include "ml/SmartScene.h"
#include "ml/audio/AudioEngine.h"
#include "flurry/flurry_.h"
#include "Laboratory2.h"
#include "LaboratoryTowerItem.h"
#include "TowerDescriptionLayer.h"
#include "game/tower.h"
#include "map/MapLayer.h"
#include "UserData.h"
#include "consts.h"
#include "ScoreCounter.h"
#include "Achievements.h"
#include "Tutorial.h"
#include "DialogLayer.h"
NS_CC_BEGIN





Laboratory2::Laboratory2()
: _useDialog (false)
{
}

Laboratory2::~Laboratory2( )
{
}

bool Laboratory2::init()
{
	do
	{
		load( "ini/laboratory2/layer.xml" );
		setDisapparanceOnBackButton();
		buildItems();
		return true;
	}
	while( false );
	return false;
}

bool Laboratory2::setProperty( const std::string & stringproperty, const std::string & value )
{
	if( stringproperty == "usedialog" )
		_useDialog = strTo<bool>( value ) && Config::shared().get<bool>( "useDialogs" );
	else return LayerExt::setProperty( stringproperty, value );
	return true;
}

void Laboratory2::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
}

bool Laboratory2::isBlurActive()
{
	return !isRunning();
}

void Laboratory2::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

void Laboratory2::buildItems()
{
	auto pathtomenu = getParamCollection().get( "pathto_scroll" );
	auto scroll = getNodeByPath<ScrollMenu>( this, pathtomenu );
	assert( scroll );
	
	std::list<std::string> towers;
	mlTowersInfo::shared().fetch( towers );
	for( auto tower : towers )
	{
		int curLevel = UserData::shared().tower_upgradeLevel( tower ) + 1;
		int maxLevel = 5;

		xmlLoader::macros::set( "towername", tower );
		xmlLoader::macros::set( "towerlevel", toStr( curLevel ) );
		auto item = xmlLoader::load_node<LaboratoryTowerItem>( "ini/laboratory2/items/" + tower + ".xml" );
		assert( item );
		item->setCallback( std::bind( &Laboratory2::showDescription, this, tower ) );
		scroll->addItem( item );
	}
	scroll->align( scroll->getAlignedColums() );
}

void Laboratory2::showDescription( const std::string& name )
{
	auto callback = std::bind( &Laboratory2::upgradeTower, this, name );
	auto layer = TowerDescriptionLayer::create( name, callback, "ini/laboratory2/itemDescription.xml");
	if( layer )
	{
		getSmartScene()->pushLayer( layer, true );
		TutorialManager::shared().dispatch( "lab_clickupgrade" );
	}
}

bool Laboratory2::upgradeTower( const std::string& name )
{
	int level = UserData::shared().tower_upgradeLevel( name ) + 1;
	int cost = mlTowersInfo::shared().getCostLab( name, level );
	int score = ScoreCounter::shared().getMoney( kScoreCrystals );
	if( score < cost )
	{
		if( _useDialog == false )
		{
			auto scene = getSmartScene();
			auto map = dynamic_cast<MapLayer*>(scene->getMainLayer().ptr());
			bool result = map->openShopFromLaboratory();
			if( result == false )
			{
				disappearance();
				return true;
			}
		}
		else
		{
			std::string path = level > 1 ? "ini/dialogs/towerupgrade.xml" : "ini/dialogs/towerunlock.xml";
			DialogLayer::showForShop( path, 1, 1 );
			return false;
		}
	}
	else
	{
		level = std::min( level, 5 );
		UserData::shared().tower_upgradeLevel( name, level );
		ScoreCounter::shared().subMoney( kScoreCrystals, cost, true, "laboratory:" + name );

		Achievements::shared().process( "lab_buyupgrade", 1 );
		AudioEngine::shared().playEffect( kSoundLabUpgrade );
		UserData::shared().save();

		ParamCollection pc;
		pc["event"] = "LaboratoryUpgrade";
		pc["tower"] = name;
		pc["level"] = toStr( level );
		flurry::logEvent( pc );
	}

	return score >= cost;
}

NS_CC_END