#include "ml/Text.h"
#include "ml/loadxml/xmlProperties.h"
#include "LaboratoryTowerItem.h"
#include "UserData.h"
#include "tower.h"
#include "ScoreCounter.h"
NS_CC_BEGIN





LaboratoryTowerItem::LaboratoryTowerItem()
{
	UserData::shared().observerTowerUpgrade.add( _ID, std::bind( &LaboratoryTowerItem::onTowerUpgraded, this, std::placeholders::_1 ) );
}

LaboratoryTowerItem::~LaboratoryTowerItem( )
{
	UserData::shared().observerTowerUpgrade.remove( _ID );
}

bool LaboratoryTowerItem::init()
{
	do
	{
		CC_BREAK_IF( !MenuItemImage::init() );
		return true;
	}
	while( false );
	return false;
}

void LaboratoryTowerItem::onEnter()
{
	mlMenuItem::onEnter();
	fetch();
	runEvent( "onenter" );
}

void LaboratoryTowerItem::fetch()
{
	auto towername = getName();
	int curLevel = UserData::shared().tower_upgradeLevel( towername );
	int maxLevel = 5;
	int cost = mlTowersInfo::shared().getCostLab( towername, curLevel + 1 );
	bool existGoldOnUpgrade = cost <= ScoreCounter::shared().getMoney( kScoreCrystals );
	bool buyIconVisibled = curLevel == 0;
	bool upgradeIconVisibled = curLevel > 0 && curLevel < maxLevel && existGoldOnUpgrade;
	auto image = getParamCollection().get( "levelimage" + toStr( std::max( curLevel, 1 ) ) );

	auto icon = getNodeByPath<Sprite>( this, getParamCollection().get( "pathto_icon" ) );
	auto levelnode = getNodeByPath<Text>( this, getParamCollection().get( "pathto_level" ) );
	auto iconUpgrade = getNodeByPath( this, getParamCollection().get( "pathto_iconupgrade" ) );
	auto iconBuy = getNodeByPath( this, getParamCollection().get( "pathto_iconbuy" ) );

	if( icon ) xmlLoader::setProperty( icon, xmlLoader::kImage, image );
	if( levelnode ) levelnode->setString( toStr( curLevel ) );
	if( iconUpgrade ) iconUpgrade->setVisible( upgradeIconVisibled );
	if( iconBuy ) iconBuy->setVisible( buyIconVisibled );
}

void LaboratoryTowerItem::onTowerUpgraded( const std::string& towername )
{
	if( towername != getName() )
		return;
	fetch();
}



NS_CC_END