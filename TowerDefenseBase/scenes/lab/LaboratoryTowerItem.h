#ifndef __LaboratoryTowerItem_h__
#define __LaboratoryTowerItem_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/MenuItem.h"
NS_CC_BEGIN





class LaboratoryTowerItem : public mlMenuItem
{
	DECLARE_BUILDER( LaboratoryTowerItem );
	bool init();
public:
	virtual void onEnter();
protected:
	void fetch();
	void onTowerUpgraded( const std::string& towername );
private:
};




NS_CC_END
#endif // #ifndef LaboratoryTowerItem