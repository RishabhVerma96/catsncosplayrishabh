//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 08.12.15.
//
//
#ifndef __TowerDescriptionLayer_h__
#define __TowerDescriptionLayer_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN





class TowerDescriptionLayer : public LayerExt, public LayerBlur
{
	DECLARE_BUILDER( TowerDescriptionLayer );
	bool init( std::string const & tower, std::function<bool()> upgradeCallback, const std::string& pathToXml );
public:
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;
protected:
	virtual bool isBlurActive() override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) override;
	void cb_upgrade();
private:
	std::function<bool()> _callbackUpgrade;
	int maxUpgradeLevel;
};




NS_CC_END
#endif // #ifndef TowerDescriptionLayer
