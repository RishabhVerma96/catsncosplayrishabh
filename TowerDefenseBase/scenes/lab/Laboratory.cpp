//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "Laboratory.h"
#include "tower.h"
#include "UserData.h"
#include "ml/SmartScene.h"
#include "flurry/flurry_.h"
#include "MapLayer.h"
#include "ScoreCounter.h"
#include "resources.h"
#include "ml/Audio/AudioEngine.h"
#include "configuration.h"
#include "ml/Language.h"
#include "ml/loadxml/xmlProperties.h"
#include "Tutorial.h"
#include "Achievements.h"
#include "TowerDescriptionLayer.h"
#include "inapp/Purchase.h"
#include "EasterEggsHandler.h"
NS_CC_BEGIN

static std::string const xmlPathDescriptionPopUp = "ini/laboratory/itemDescription.xml";

Laboratory::Laboratory()
: _scaleFactor( 1 )
{}

Laboratory::~Laboratory()
{}

bool Laboratory::init()
{
	do
	{
		auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
		CC_BREAK_IF( !ScrollMenu::init() );
		CC_BREAK_IF( !NodeExt::init() );
		CC_BREAK_IF( !LayerBlur::init() );
		setCascadeOpacityEnabled( true );
		setCascadeColorEnabled( true );
		setKeyboardEnabled( true );

		NodeExt::load( "ini/laboratory", "lab.xml" );

		bool const autoPositioning = getParamCollection().find( "position" ) == getParamCollection().end();

		auto imagesPathIt = getParamCollection().find( "laboratory_images_path" );
		if( imagesPathIt != getParamCollection().end() )
			m_imagesPath = imagesPathIt->second;

		auto bg = getChildByName( "bg" );
		if( bg && autoPositioning )
		{
			auto size = bg->getContentSize();
			size.width = std::min( dessize.width, size.width );
			size.height += bg->getPositionY();
			float sx = dessize.width / size.width;
			_scaleFactor = sx;
			bg->setScale( _scaleFactor );

			_zeroPosition.x = dessize.width / 2;
			_zeroPosition.y = size.height / 2 * _scaleFactor - 10;
			setPosition( _zeroPosition );
		}
		else
		{
			_scaleFactor = 1.0f;
		}

		auto caption = getChildByName("text");
		if( caption &&caption->getPosition().equals( Point::ZERO ) )
		{
			caption->setPositionY( 240.f );
			caption->setPositionX( 0.f );
		}

		_container = Node::getChildByName<ScrollMenu *>( "container" );

		if( _container == nullptr )
			_container = this;

		Size grid;
		Size content;
		std::list<std::string> towers;
		mlTowersInfo::shared().fetch( towers );
		for( auto & tower : towers )
		{
			auto item = buildItem( tower );
			_container->addItem( item );
			
			item->setScale( _scaleFactor );

			grid = item->getContentSize();
			grid.width *= _scaleFactor;
			grid.height *= _scaleFactor;
			content.width += grid.width;
			content.height = grid.height;

			int level = UserData::shared().tower_upgradeLevel( tower );
			int maxLevel = mlTowersInfo::shared().get_max_level( tower );
			
			setIndicator( tower, false);
			setCost( tower );
			setParam( tower, false );
            setIcon( tower, false );
			setPlayable( tower );
		}
        
		if( _container.ptr() == this ) // old xml-style
		{
			setAlignedStartPosition( Point( -_zeroPosition.x, -275 ) * _scaleFactor );
			setGrisSize( grid );
			align( 99 );

			Size scissor( dessize.width, 465 );
			if( getScissorRect().equals( Rect( 0, 0, dessize.width, dessize.height ) ) )
				setScissorRect( getAlignedStartPosition(), scissor * _scaleFactor );

			setScissorEnabled( true );
			setScrollEnabled( true );
			setContentSize( content );
			setAllowScrollByY( false );

			if(content.width < scissor.width)
			{
				float diff = scissor.width - content.width;
				Point point = getAlignedStartPosition();
				point.x += diff / 2;
				setAlignedStartPosition(point);
				align(99);
			}
		}

		auto menu = getChildByName( "menu" );
		if( menu )
		{
			auto close = menu->getChildByName<MenuItem*>( "close" );
			close->setCallback( std::bind( &Laboratory::cb_close, this, std::placeholders::_1 ) );
			if( close->getPosition().equals( Point::ZERO ) )
			{
				close->setScale( _scaleFactor );
				Point pos = close->getPosition();
				pos.y *= _scaleFactor;
				pos.x = dessize.width / 2 - 35;
				close->setPosition( pos );
			}
		}

		_container->align( _container->getAlignedColums() );

		fadeenter();
		return true;
	}
	while( false );
	return false;
}

void Laboratory::onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event )
{
	if( keyCode == EventKeyboard::KeyCode::KEY_BACK )
		cb_close( nullptr );
}

void Laboratory::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		ScrollMenu::visit( renderer, parentTransform, parentFlags );
}

bool Laboratory::isBlurActive()
{
	return !isRunning();
}

void Laboratory::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	ScrollMenu::visit( renderer, parentTransform, parentFlags );
}

MenuItemPointer Laboratory::buildItem( const std::string & tower )
{
	int level = UserData::shared().tower_upgradeLevel( tower );
	
	xmlLoader::macros::set( "tower_name", tower );
	MenuItemPointer item = xmlLoader::load_node<MenuItem>( "ini/laboratory/item.xml" );
	xmlLoader::macros::erase( "tower_name" );

	item->setName( tower );
	item->setCallback( std::bind( &Laboratory::cb_select, this, std::placeholders::_1, tower ) );
	auto conteiner = item->getChildByName( "conteiner" );
	if( conteiner )
	{
		conteiner->setCascadeColorEnabled( true );
		conteiner->setCascadeOpacityEnabled( true );
		auto infonode = conteiner->getChildByName( "info" );
		auto mainnode = conteiner->getChildByName( "main" );
		mainnode->setCascadeColorEnabled( true );
		mainnode->setCascadeOpacityEnabled( true );

		auto caption = conteiner->getChildByName<Text*>( "name" );
		auto icon2 = conteiner->getChildByName<Sprite*>( "icon2" );
		auto icon_lock = conteiner->getChildByName<Sprite*>( "icon_locked" );

		auto indicator = mainnode->getChildByName( "indicator" );

		if( caption )
		{
			std::string name = tower + "_name";
			name = WORD( name );
			caption->setString( name );
		}
		if( icon2 )
		{
			xmlLoader::setProperty( icon2, xmlLoader::kImage, m_imagesPath + "icon_" + tower + ".png" );
		}
		if( indicator )
		{
			indicator->setCascadeColorEnabled( true );
			indicator->setCascadeOpacityEnabled( true );
		}

		auto menuinfo = conteiner->getChildByName<Menu*>( "menu_info" );
		if( menuinfo )
		{
			auto info = menuinfo->getChildByName<MenuItem*>( "info" );
			info->setCallback( std::bind( &Laboratory::cb_info, this, std::placeholders::_1, tower ) );
			auto info_no_money = menuinfo->getChildByName<MenuItem*>( "info_no_money" );
			if( info_no_money )
			{
				info_no_money->setCallback( std::bind( &Laboratory::cb_info, this, std::placeholders::_1, tower ) );

				int level = UserData::shared().tower_upgradeLevel( tower );
				int maxLevel = mlTowersInfo::shared().get_max_level( tower );
				int cost = mlTowersInfo::shared().getCostLab( tower, level + 1 );
				int money = ScoreCounter::shared().getMoney( kScoreCrystals );
				bool enoughMoney = money >= cost;
				info->setVisible( enoughMoney && (!(level == maxLevel)));
				info_no_money->setVisible( !enoughMoney || (level == maxLevel) );
			}
		}

		auto menuPlayable = mainnode->getChildByName<Menu*>( "menu_playable" );
		if( menuPlayable )
		{
			menuPlayable->getChildByName<MenuItem *>( "turn_on" )
				->setCallback( std::bind( &Laboratory::cb_playable, this, std::placeholders::_1, tower, true ) );

			menuPlayable->getChildByName<MenuItem *>( "turn_off" )
				->setCallback( std::bind( &Laboratory::cb_playable, this, std::placeholders::_1, tower, false ) );
		}

		auto menu = mainnode->getChildByName<Menu*>( "menu" );
		if( menu )
		{
			auto button = menu->getChildByName<MenuItem*>( "buy" );

			button->setName( tower );
			button->setCallback( std::bind( &Laboratory::cb_upgrade, this, std::placeholders::_1, tower ) );
		}
		auto menu_confirm = mainnode->getChildByName<Menu*>( "menu_confirm" );
		if( menu_confirm )
		{
			auto ok = menu_confirm->getChildByName<MenuItem*>( "confirm" );
			auto no = menu_confirm->getChildByName<MenuItem*>( "cancel" );

			ok->setName( tower );
			no->setName( tower );
			ok->setCallback( std::bind( &Laboratory::cb_confirm, this, std::placeholders::_1, tower ) );
			no->setCallback( std::bind( &Laboratory::cb_cancel, this, std::placeholders::_1, tower ) );
		}

		if( infonode )
		{
			auto text = infonode->getChildByName<Text*>( "text" );
			text->setString( mlTowersInfo::shared().get_desc( tower, 1 ) );
		}

		if( level == 0 )
		{
			item->setEnabled( false );
			item->setOpacity( 128 );
			menu_confirm->setEnabled( false );
			menu->setEnabled( false );
			menuinfo->setEnabled( false );
			menu->setOpacity( 128 );

			mainnode->setVisible( false );
			icon_lock->setVisible( true );
		}
		else if( !mlTowersInfo::shared().get_purchased( tower ) )
		{
			std::string const towerInapp = mlTowersInfo::shared().get_inapp_id( tower );
			int const towerPrice = mlTowersInfo::shared().get_coin_price( tower );
			bool const coinPurchase = towerPrice > 0;
			item->setEnabled( false );
			mainnode->setVisible( false );

			auto menuPurchase = conteiner->getChildByName<Menu *>( "menu_purchase" );
			auto purchaseButton = menuPurchase->getChildByName<MenuItem *>( "purchase" );
			auto text = menuPurchase->getChildByName<Label *>( "text" );
			auto coin = menuPurchase->getChildByName( "coin" );

			if( menuPurchase )
				menuPurchase->setVisible( true );
			
			if( purchaseButton )
			{
				purchaseButton->setCallback( std::bind( &Laboratory::cb_purchase, this, std::placeholders::_1, tower ) );
				if( coinPurchase )
					text->setString( toStr(towerPrice) );
				else
				{
					inapp::details( towerInapp, [text]( inapp::SkuDetails product ){
						text->setString( product.priceText );
					} );
				}

				if( coin )
					coin->setVisible( coinPurchase );
			}
		}
	}

	return item;
}

ccMenuCallback Laboratory::get_callback_by_description( const std::string & name )
{
	if( name == "close" )
		return std::bind( &Laboratory::cb_close, this, std::placeholders::_1 );

	if( name.find("mode_") == 0 )
	{
		std::string mode = name.substr( strlen( "mode_" ) );
		return [this, name, mode]( Ref * sender ) {
			this->cb_mode( sender, mode );
			this->runEvent( name );
		};
	}

	return NodeExt::get_callback_by_description( name );
}

void Laboratory::cb_mode( Ref * sender, const std::string & mode )
{
	std::string active;
	std::vector<std::string> inactives;

	if( mode == "select" )
	{
		active = "menu_playable";
		inactives = { "menu", "menu_confirm" };
	}
	else
	{
		active = "menu";
		inactives = { "menu_playable", "menu_confirm" };
	}

	for( size_t i = 0; i < _container->getItemsCount(); ++i )
	{
		Node * const conteiner = _container->getItem( i )->getChildByName( "conteiner" );
		if( conteiner == nullptr )
			continue;
		
		Node * const mainNode = conteiner->getChildByName( "main" );
		if( mainNode == nullptr )
			continue;

		auto node = mainNode->getChildByName( active );
		if( node )
			node->setVisible( true );

		for( auto inactive : inactives )
		{
			node = mainNode->getChildByName( inactive );
			if( node )
				node->setVisible( false );
		}
	}
}

void Laboratory::cb_playable( Ref *, const std::string & tower, bool state )
{
	mlTowersInfo::shared().set_playable( tower, state );
	this->setPlayable( tower );
}


void Laboratory::cb_purchase( Ref*, const std::string & tower )
{
	std::string const towerInapp = mlTowersInfo::shared().get_inapp_id( tower );
	int const towerPrice = mlTowersInfo::shared().get_coin_price( tower );

	auto onPurchased = [this, tower]( inapp::PurchaseResult res ){
		if( res.result == inapp::Result::Fail || res.result == inapp::Result::Canceled )
			return;

		mlTowersInfo::shared().set_purchased( tower );

		auto item = _container->getMenuItemByName( tower );
		auto conteiner = item ? item->getChildByName( "conteiner" ) : nullptr;
		auto main = conteiner ? conteiner->getChildByName( "main" ) : nullptr;
		auto menuPurchase = conteiner ? conteiner->getChildByName( "menu_purchase" ) : nullptr;
		if( item )
			item->setEnabled( true );
		if( main )
			main->setVisible( true );
		if( menuPurchase )
			menuPurchase->setVisible( false );
	};

	if( towerPrice > 0 )
	{
		int const money = ScoreCounter::shared().getMoney( kScoreCrystals );
		if( money < towerPrice )
		{
			SmartScene * scene = dynamic_cast<SmartScene *>(getScene());
			if( scene )
			{
				MapLayer * map = dynamic_cast<MapLayer *>(scene->getMainLayer().ptr());
				if( map )
					map->cb_shop( nullptr, 1, 1 );
			}
		}
		else
		{
			ScoreCounter::shared().subMoney( kScoreCrystals, towerPrice, true, "laboratory" );

			inapp::PurchaseResult res;
			res.result = inapp::Result::Ok;
			onPurchased( res );
		}
	}
	else
	{
		inapp::setCallbackPurchase( onPurchased );
		inapp::purchase( towerInapp );
	}
}

void Laboratory::cb_select( Ref*, const std::string & tower )
{
	selectTower( tower );
}

void Laboratory::cb_info( Ref * sender, const std::string & tower )
{
	auto callback = std::bind( &Laboratory::cb_confirmAndCloseDescription, this, tower );
	auto desc = TowerDescriptionLayer::create( tower, callback, xmlPathDescriptionPopUp );
	if( desc )
	{
		dynamic_cast<SmartScene *>( getScene() )->pushLayer( desc, true );
	}
	else
	{
		selectTower( tower );
		auto item = dynamic_cast<MenuItem*>(sender);
		assert( item );
		switchInfoBox( tower );
	}
//    TutorialManager::shared( ).dispatch( "lab_clickupgrade" );
}

void Laboratory::cb_upgrade( Ref*, const std::string & tower )
{
	int level = UserData::shared().tower_upgradeLevel( tower );
	int maxLevel = mlTowersInfo::shared().get_max_level( tower );
	
	selectTower( tower );
	showConfirmMenu( tower, !(level == maxLevel) );
	setParam( tower, !(level == maxLevel) );
	setIcon( tower, !(level == maxLevel) );
	setIndicator( tower, !(level == maxLevel) );

    TutorialManager::shared( ).dispatch( "lab_clickupgrade" );
}

void Laboratory::cb_confirm( Ref*, const std::string & tower )
{
	int level = UserData::shared().tower_upgradeLevel( tower ) + 1;
	int cost = mlTowersInfo::shared().getCostLab( tower, level );
	int score = ScoreCounter::shared().getMoney( kScoreCrystals );

	if( score < cost )
	{
		if( Config::shared().get<bool>( "useInapps" ) )
		{
			if( Config::shared().get<bool>( "useInapps" ) && TutorialManager::shared().dispatch( "lab_haventgold" ) )
			{
				cb_close( nullptr );
				return;
			}
			else
			{
				SmartScene * scene = dynamic_cast<SmartScene*>(getScene( ));
				if( scene )
				{
					MapLayer::Pointer map( dynamic_cast<MapLayer*>(scene->getMainLayer( ).ptr( )) );
					if( map )
						map->cb_shop( nullptr, 1, 1 );
				}
			}
		}
		else
		{
			cb_cancel(nullptr, tower);
			return;
		}
	}
	else
	{
		selectTower( "" );
		showConfirmMenu( tower, false );
		upgradeTower( tower );
		ScoreCounter::shared().subMoney( kScoreCrystals, cost, true, "laboratory:" + tower );
		normalStateForAllItemExcept( "" );
		AudioEngine::shared().playEffect( kSoundLabUpgrade );
		UserData::shared().save();

		ParamCollection pc;
		pc["event"] = "LaboratoryUpgrade";
		pc["tower"] = tower;
		pc["level"] = toStr( level );
		flurry::logEvent( pc );
	}
	setParam( tower, false );
	setIcon( tower, false );

	TutorialManager::shared( ).dispatch( "lab_clickconfirm" );
}

bool Laboratory::cb_confirmAndCloseDescription( const std::string & tower )
{
	cb_confirm( nullptr, tower );
	bool autoclose = strTo<bool>( getParamCollection().get( "autoclosedescription", "yes" ) );
	return true;
}

void Laboratory::cb_cancel( Ref*, const std::string & tower )
{
	int level = UserData::shared().tower_upgradeLevel( tower );
	int maxLevel = mlTowersInfo::shared().get_max_level( tower );
	
	selectTower( "" );
	showConfirmMenu( tower, !(level == maxLevel) );
	normalStateForAllItemExcept( "" );
	setIndicator( tower, !(level == maxLevel) );
}

void Laboratory::selectTower( const std::string & tower )
{
	normalStateForAllItemExcept( tower );
	unsigned count = getItemsCount();
	for( unsigned i = 0; i < count; ++i )
	{
		auto item = getItem( i );
		unsigned opacity = tower.empty() == false ?
			(item->getName() == tower ? 255 : 64) :
			255;
		item->setOpacity( opacity );
	}
}

void Laboratory::showConfirmMenu( const std::string & itemname, bool mode )
{
    CCLOG("show confirm menu called: ");
	int level = UserData::shared().tower_upgradeLevel( itemname );
	int maxLevel = mlTowersInfo::shared().get_max_level( itemname );
	auto item = _container->getItemByName( itemname );
	assert( item );
	auto conteiner = item->getChildByName( "conteiner" );
	auto mainnode = conteiner->getChildByName( "main" );
	auto menu0 = mainnode->getChildByName( "menu" );
	auto menu1 = mainnode->getChildByName( "menu_confirm" );

	if(menu0)
		menu0->setVisible( !mode && level != maxLevel );
	if(menu1)
		menu1->setVisible( mode );
}

void Laboratory::switchInfoBox( const std::string & itemname, bool forceHideInfo )
{
	int level = UserData::shared().tower_upgradeLevel( itemname );
	int maxLevel = mlTowersInfo::shared().get_max_level( itemname );
	auto item = _container->getItemByName( itemname );
	assert( item );
	auto conteiner = item->getChildByName( "conteiner" );
	auto infonode = conteiner->getChildByName( "info" );
	auto mainnode = conteiner->getChildByName( "main" );

	if (level > 0)
	{
		bool infovisible = forceHideInfo ? false : (infonode && !infonode->isVisible());
		if(infonode)
			infonode->setVisible(infovisible);
		if(mainnode)
			mainnode->setVisible(!infovisible);
	}

	auto menuinfo = conteiner->getChildByName<Menu*>( "menu_info" );
	if( menuinfo )
	{
		auto info = menuinfo->getChildByName<MenuItem*>( "info" );
		auto info_no_money = menuinfo->getChildByName<MenuItem*>( "info_no_money" );
		int cost = mlTowersInfo::shared().getCostLab( itemname, level + 1 );
		int money = ScoreCounter::shared().getMoney( kScoreCrystals );
		bool enoughMoney = info_no_money ? money >= cost : true;
		if( info )
			info->setVisible( enoughMoney && (!(level == maxLevel)));
		if( info_no_money )
			info_no_money->setVisible( !enoughMoney || (level == maxLevel) );
	}
}

void Laboratory::upgradeTower( const std::string & tower )
{
	int level = UserData::shared().tower_upgradeLevel( tower ) + 1;
	int maxLevel = mlTowersInfo::shared().get_max_level( tower );
	level = std::min( level, maxLevel );
	UserData::shared().tower_upgradeLevel( tower, level );
	
	setIndicator( tower, !(level == maxLevel) );
	setCost( tower );
	Achievements::shared().process( "lab_buyupgrade", 1 );
//    TutorialManager::shared().dispatch("lab_buyupgrade");
}

void Laboratory::setIndicator( const std::string & tower, bool nextLevel )
{
	int level = UserData::shared().tower_upgradeLevel( tower );
	int maxLevel = mlTowersInfo::shared().get_max_level( tower );
	
//	if( nextLevel )
//		level = std::min( level + 1, maxLevel );
	auto item = _container->getItemByName( tower );
	auto conteiner = item->getChildByName( "conteiner" );
	auto main = conteiner ? conteiner->getChildByName( "main" ) : nullptr;
	auto indicator = main ? main->getChildByName( "indicator" ) : nullptr;
	if (indicator)
	{
		for (int i = 0; i < maxLevel; ++i)
		{
			auto node = indicator->getChildByName(toStr(i + 1));
			if (node)
				node->setVisible(i < level);
		}

		auto captionText = level > 0 ? WORD("laboratory_tower_level") + toStr(level) : "";

		auto caption = indicator->getChildByName<Text*>("caption");
		caption->setString(captionText);
		
		auto captionShadow = indicator->getChildByName<Text*>("caption_shadow");
		if (captionShadow)
			captionShadow->setString(captionText);
	}

	if ( !nextLevel )//level == maxLevel &&
	{
		auto menu = main->getChildByName("menu");
		auto menu2 = main->getChildByName("menu_confirm");
		if (menu)
			menu->setVisible(false);
		if (menu2)
			menu2->setVisible(false);
	}
}

void Laboratory::setCost( const std::string & tower )
{
	int level = UserData::shared().tower_upgradeLevel( tower );
	auto item = _container->getItemByName( tower );
	int cost = mlTowersInfo::shared().getCostLab( tower, level + 1 );
	auto cost0 = dynamic_cast<Text*>(getNodeByPath( item, "conteiner/main/menu/" + tower + "/normal/cost" ));
	auto cost1 = dynamic_cast<Text*>(getNodeByPath( item, "conteiner/main/menu_confirm/cost" ));
	if( cost0 ) cost0->setString( toStr( cost ) );
	if( cost1 ) cost1->setString( toStr( cost ) );
}

void Laboratory::setParam(const std::string & tower, bool nextLevel)
{
	auto item = _container->getItemByName( tower );
	auto conteiner = item->getChildByName("conteiner");
	if (!conteiner)
		return;

	auto mainnode = conteiner->getChildByName("main");
	if (!mainnode)
		return;

	int level = UserData::shared().tower_upgradeLevel(tower);
	if (nextLevel)
		++level;

	// Old layout
    int multiplier = 1;
    if (tower == "tower_gun")
    {
        bool isEasterEggDoubleDamageUnlcoked = EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kDoubleDamageMageTower);
        if (isEasterEggDoubleDamageUnlcoked)
        {
            multiplier=2;
        }
    }

	std::vector <std::pair <std::string, std::string> > entries =
	{
        
        
		// Old layout
		{ "dmg", WORD("laboratory_tower_attack") + toStr(mlTowersInfo::shared().get_dmg(tower, level)*multiplier) },
		{ "rng", WORD("laboratory_tower_range") + toStr(mlTowersInfo::shared().get_rng(tower, level)) },
		{ "spd", WORD("laboratory_tower_speed") + toStr(mlTowersInfo::shared().get_spd(tower, level)) },

		// New layout
		{ "dmg_value", toStr(mlTowersInfo::shared().get_dmg(tower, level)*multiplier) },
		{ "rng_value", toStr(mlTowersInfo::shared().get_rng(tower, level)) },
		{ "spd_value", toStr(mlTowersInfo::shared().get_spd(tower, level)) }
	};

	for (auto entry : entries)
	{
		auto textNode = mainnode->getChildByName <Text*>(entry.first);
		if (textNode)
			textNode->setString(entry.second);
	}
}

void Laboratory::setIcon( const std::string & tower, bool nextLevel )
{
	int level = UserData::shared().tower_upgradeLevel( tower );
	int maxLevel = mlTowersInfo::shared().get_max_level( tower );
	if( nextLevel ) ++level;
	level = std::min( std::max( 1, level ), maxLevel );

	std::string texture = m_imagesPath + "icons/" + tower + toStr( level ) + ".png";
	auto item = _container->getItemByName( tower );
	auto conteiner = item->getChildByName( "conteiner" );
	if( !conteiner )return;
	auto icon = conteiner->getChildByName<Sprite*>( "icon" );
	if( icon ) xmlLoader::setProperty( icon, xmlLoader::kImage, texture );
}

void Laboratory::setPlayable( const std::string & tower )
{
	bool const isPlayable = mlTowersInfo::shared().get_playable( tower );

	auto item = _container->getItemByName( tower );
	if( item == nullptr )
		return;

	auto conteiner = item->getChildByName( "conteiner" );
	if( conteiner == nullptr )
		return;

	auto playable = conteiner->getChildByName( "playable" );
	if( playable != nullptr )
		playable->setVisible( isPlayable );
	
	auto mainNode = conteiner->getChildByName( "main" );
	if( mainNode == nullptr )
		return;

	auto menuPlayable = mainNode->getChildByName( "menu_playable" );
	if( menuPlayable == nullptr )
		return;

	auto on = menuPlayable->getChildByName( "turn_on" );
	auto off = menuPlayable->getChildByName( "turn_off" );
	if( on != nullptr && off != nullptr )
	{
		on->setVisible( !isPlayable );
		off->setVisible( isPlayable );
	}

	int const playableCount = mlTowersInfo::shared().get_playable_count();
	int const playableCountMax = mlTowersInfo::shared().get_playable_count_max();

	auto playableCounter = getNodeByPath<Label>( this, "menu_select/playable_count_text" );
	if( playableCounter != nullptr )
		playableCounter->setString( StringUtils::format( "%d/%d", playableCount, playableCountMax ) );

	bool const activateMenus = playableCount == playableCountMax;
	for( Node * submenu : getChildren() )
	{
		Menu * menu = dynamic_cast<Menu *>(submenu);
		if( menu )
		{
			for( auto but : menu->getChildren() )
			{
				MenuItem * button = dynamic_cast<MenuItem *>(but);
				if( button )
					button->setEnabled( activateMenus );
			}
		}
	}
	if( !activateMenus )
		cb_mode( nullptr, "select" );
}

void Laboratory::normalStateForAllItemExcept( const std::string & tower )
{
	for( unsigned i = 0; i < getItemsCount(); ++i )
	{
		auto item = getItem( i );
		if( item->getName() != tower )
		{
			showConfirmMenu( item->getName(), false );
			switchInfoBox( item->getName(), true ); //true
			setParam( item->getName(), false );
			setIcon( item->getName(), false );
		}
	}
}

void Laboratory::cb_close( Ref* )
{
	auto func = [this]()
	{
		if( this->_callbackOnClosed )
			this->_callbackOnClosed();
		this->removeFromParent();
	};

	fadeexit();
	runAction( Sequence::createWithTwoActions(
		DelayTime::create( 0.5f ),
		CallFunc::create( func )
	) );
}

void Laboratory::fadeexit()
{
	if( runEvent( "disappearance" ) == false )
	{
		static auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
		auto action = EaseBackIn::create( MoveTo::create( 0.5f, _zeroPosition + Point( 0, -dessize.height ) ) );
		runAction( action );
	}
	AudioEngine::shared().playEffect( kSoundShopHide );
}

void Laboratory::fadeenter()
{
	if( runEvent( "appearance" ) == false )
	{
		static auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
		setPosition( _zeroPosition + Point( 0, -dessize.height ) );
		auto action = EaseBackOut::create( MoveTo::create( 0.5f, _zeroPosition ) );
		runAction( action );
	}
	AudioEngine::shared().playEffect( kSoundShopShow );
}




NS_CC_END
