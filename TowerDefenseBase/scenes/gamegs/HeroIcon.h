#ifndef __HeroIcon_h__
#define __HeroIcon_h__
#include "cocos2d.h"
#include "macroses.h"
#include "ml/MenuItem.h"
#include "Hero.h"
NS_CC_BEGIN





class HeroIcon : public mlMenuItem
{
	DECLARE_BUILDER( HeroIcon );
	bool init( const std::string & heroname, const std::string& path, const ccMenuCallback & callback );
public:
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;
	virtual void setEnabled( bool var )override;
	virtual void onEnter()override;
	void setHero( Hero::Pointer hero );
	Hero::Pointer getHero() { return _hero; }
	void showCancel( bool mode );
	void listenHeroHealth( float current, float health );
	void resurrect( bool showDialog );

protected:
private:
	Hero::Pointer _hero;
	ProgressTimerPointer _timer;
	SpritePointer _cancelImage;
	MenuPointer _resurrectionMenu;
	std::string _unselectedHero;
	std::string _selectedHero;
	bool _useDialog;
	bool _resurectHeroOnEnter;
};




NS_CC_END
#endif // #ifndef HeroIcon