#include "GamePopUpScene.h"
NS_CC_BEGIN;
GamePopUpLayer::GamePopUpLayer():_scaleFactor(1)
{}

bool GamePopUpLayer::init(const std::string &path)
{
    do
    {
        Size desSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
        CC_BREAK_IF( !Menu::init() );
        setPosition( Point( desSize / 2 ) );
        setContentSize( Size::ZERO );
        setKeyboardEnabled( true );
        load( path );
        _close = getChildByName<MenuItem*>( "close" );
        auto bg = getChildByName( "bg" );
        if( bg )
        {
            auto size = bg->getContentSize();
            float sx = std::min<float>( 1, desSize.width / size.width );
            float sy = std::min<float>( 1, desSize.height / size.height );
            float s = std::min<float>( sx, sy );
            s = std::min<float>( 1, s );
            _scaleFactor = s;
        }
        return true;
    }
    while(false);
    return false;
}

GamePopUpLayer::~GamePopUpLayer()
{
    removeAllChildrenWithCleanup(true);
}

void GamePopUpLayer::onEnter()
{
    Menu::onEnter();
    setKeyboardEnabled( true );
    if( _close )
        _close->setEnabled( true );
}

void GamePopUpLayer::cb_close(cocos2d::Ref *sender)
{
    auto func = CallFunc::create( std::bind([this] () {
        setEnabled(false);
        removeFromParent();
    }) );
    runAction( Sequence::create( func, nullptr ) );
}

ccMenuCallback GamePopUpLayer::get_callback_by_description(const std::string &name)
{
    if( name == "close" )
        return CC_CALLBACK_1( GamePopUpLayer::cb_close, this );
    return nullptr;
}
NS_CC_END;
