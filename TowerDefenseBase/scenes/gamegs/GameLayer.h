//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__
#pragma warning ( disable : 4996 )
#include "ml/ScrollMenu.h"
#include "ml/Scroller.h"
#include "gameboard.h"
#include "GameInterface.h"
#include "support.h"
#include "preprocessor.h"
#include "MenuCreateTower.h"
#include "MenuTower.h"
#include "MenuDig.h"
#include "Decoration.h"
#include "LootDropManager.h"
#include "EasterEggsPopup.h"
#include "EasterEggsHandler.h"

NS_CC_BEGIN;

class Icon;
class GameLayer;
class mlMenuItem;

namespace zorder
{
	const int bg( -1000 );
	const int earth( -999 );
	const int creep_default( 10 );
	const int creep_earth( 10 );
	const int tower( 20 );
	const int tower_cariage( 19 );
	const int tower_turret( 25 );
	const int bullet( 30 );
	const int tree( 40 );

	const int creep_sky( 99998 );
	const int bullet_sky( 99997 );

	const int sky( 99999 );
	const int creep_indicator( 100000 );
};

class GameLayer : public LayerExt, public LayerBlur
{
	DECLARE_BUILDER( GameLayer );
	virtual bool init( GameScene* scene, GameBoard* board );
public:
	static const std::string eventFocusLostName;

public:
	GameBoard& getGameBoard();
	LootDropManager::Pointer getLootDropManager();

	virtual void onEnter();
	virtual void onExit();

	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;

	GameInterface* getInterface();
	void setInterface( GameInterface* node );

	//build level:
	void clear();
	void startGame();
	void loadLevel( int index, const pugi::xml_node & root );
	static void createDecorFromXmlNode( const pugi::xml_node & node, Decoration::Pointer & outNode );
	void createMenuTower();
	void createMenuCreateTower();
	void createMenuDig();
	void closeMenuTower();
	void closeMenuCreateTower();
	void closeMenuDig();

	const Point convertToGameSpase( const Point& point )const;

	void excludeTower( const std::string & towername );

	const std::vector<TowerPlace::Pointer>& getTowerPlaces()const;
	TowerPlace::Pointer addTowerPlace( const TowerPlaseDef & def );
	TowerPlace::Pointer getTowerPlaceInLocation( const Point & location );
	TowerPlace::Pointer getTowerPlace( size_t index );
	TowerPlace::Pointer getTowerPlace( const std::string& name );
	TowerPlace::Pointer getSelectedTowerPlaces();
	unsigned getTowerPlaceIndex( const Point & location );
	void setSelectedTowerPlaces( TowerPlace::Pointer place );
	void eraseTowerPlace( TowerPlace::Pointer place );
	void resetSelectedPlace();

    void selectDesant( UnitDesant::Pointer desant );
    UnitDesant* getSelectedDesant();
    UnitDesant* getAvailableDesant();
    UnitDesant* getAvailableDesant2();

    
	void selectHero( Hero::Pointer hero );
	Hero* getSelectedHero();

	std::vector<Decoration*> getDecorations( const std::string & name );

	static Node * createTree( int index );
	static Node * createStone( int index );

	void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags );
	bool isBlurActive();
	void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags );

	void onClickByObject( Unit::Pointer object );
	void onClickByTowerPlace( TowerPlace::Pointer place );
	void markTowerPlaceOnLocation( const Point& position );

	void onCreateUnit( Unit*unit );
	void onDeathUnit( Unit*unit );
	void onDeathCanceled( Unit*unit );

	void onFirstWave();
	void onStartWave( const WaveInfo & wave );
	void onWaveFinished();
    void _onFinishGame( FinishLevelParams* params , EasterEggsParams* easterParams );
	void onFinishGame( FinishLevelParams* params , EasterEggsParams* easterParams );
    
    const EasterEggType isAnyEasterEggCompleted(FinishLevelParams* params, EasterEggsParams* easterParams);

	void onClickBoxMenu();

	void buyLevelsMoney( int count );

	void shake( float value = 1.f );

	void openStatisticWindow( FinishLevelParams* params );
    void openEasterEggUnlockWindow( const EasterEggType easterType, const EasterPopupCloseCB& closeCallback );
	void flyCameraAboveMap( const Point & wavestart );
public:
	void addObject( Node * object, int zOrder );
	void removeObject( Node * object );
	Node* getObjectsNode();
	void update( float dt );

	Scroller* getMainLayer() { return _mainlayer; }

	void createEffect( Unit*base, Unit*target, const std::string & effect );

	void createAddMoneyNodeForWave( unsigned count, const Point & position );

	Unit * getObjectInLocation( const Point & location );

	IntrusivePtr<MenuCreateTower>& getMenuCreateTower() { return _menuCreateTower; };
    const IntrusivePtr<MenuCreateTower>& getMenuCreateTower()const { return _menuCreateTower; };
    IntrusivePtr<MenuTower>& getMenuTower(){ return _menuTower; };
    const IntrusivePtr<MenuTower>& getMenuTower()const { return _menuTower; };
    IntrusivePtr<MenuDig>& getMenuDig(){ return _menuDig; };
    const IntrusivePtr<MenuDig>& getMenuDig()const { return _menuDig; };

	void achievementsObtained( const std::string & name );
	void achievementsWindowClose( Ref * ref );

	void selectUnit( Unit* node );
	void startWaveAfter( const WaveInfo & wave, float duration );
	void startWave( WaveIcon* icon, float elapsed, float duration );
	void setFastMode( bool enabled );
	void loadUserGifts();
    
    void runDesant2Heal();
    void selectHeroAfterTowerClose();
    
protected:
	void createLootDropManager();

protected:
	CC_SYNTHESIZE( GameScene*, _gameScene, GameScene );
	IntrusivePtr<LootDropManager> _lootDropManager;
	IntrusivePtr<GameBoard> _board;
	Scroller::Pointer _mainlayer;
	SpritePointer _bg;
	NodePointer _objects;
	GameInterface::Pointer _interface;
	std::vector<TowerPlace::Pointer>_towerPlaces;
	TowerPlace::Pointer _selectedPlace;
	Unit::Pointer _selectedUnit;
	Hero::Pointer _selectedHero;
    UnitDesant::Pointer _selectedDesant;
    std::string _soundOnWave;

	std::list<Node*>_fragments;
	std::list<std::string> _excludedTowers;

	bool _fastModeEnabled;

	unsigned _scoresForStartWave;
	unsigned _boughtScoresForSession;

	IntrusivePtr< MenuCreateTower> _menuCreateTower;
	IntrusivePtr< MenuTower> _menuTower;
	IntrusivePtr< MenuDig> _menuDig;;
	bool _runFlyCamera;
	bool _firstWaveStarted;
	std::vector<std::pair<int, int>> _userGiftsLevels;
};

NS_CC_END

#endif // __HELLOWORLD_SCENE_H__
