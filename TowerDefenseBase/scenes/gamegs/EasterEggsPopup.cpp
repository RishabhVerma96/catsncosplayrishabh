//
//  EasterEggsPopup.cpp
//  Easter Island
//
//  Created by Zeeshan Amjad on 30/10/2017.
//

#include "EasterEggsPopup.h"

NS_CC_BEGIN


EasterEggsPopup::EasterEggsPopup()
: m_callback(nullptr)
{
}

EasterEggsPopup::~EasterEggsPopup( )
{
}

bool EasterEggsPopup::init( const int level, const EasterPopupCloseCB& callback )
{
    do
    {
        CC_BREAK_IF( !LayerExt::init() );
        
        m_callback = callback;
                
        auto listener = EventListenerKeyboard::create();
        listener->onKeyReleased = CC_CALLBACK_2(EasterEggsPopup::onKeyReleased, this);
        _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

        std::string ini = StringUtils::format("easterpopup%d.xml", level);
        NodeExt::load( "ini/easterPopup", ini );

        auto menu = getChildByPath( "menu" );
        auto close = menu?menu->getChildByName<MenuItem*>( "close" ) : nullptr;
        if( menu )
        {
            if( close ) close->setCallback( CC_CALLBACK_1( EasterEggsPopup::cb_close, this ) );
        }
        
        return true;
    }
    while( false );
    return false;
}

void EasterEggsPopup::cb_close( Ref* )
{
    close();
}

void EasterEggsPopup::close()
{
    if(m_callback)
    {
        m_callback();
    }
    removeFromParent();
}

void EasterEggsPopup::onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event )
{
    if( keyCode == EventKeyboard::KeyCode::KEY_BACK )
    close();
}

NS_CC_END
