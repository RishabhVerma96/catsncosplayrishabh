//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __TowerPlace_h__
#define __TowerPlace_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN


struct TowerPlaseDef
{
	TowerPlaseDef() :position(), isActive( true ), opponent(false){}
	Point position;
	bool isActive;
	bool opponent;
};


class TowerPlace : public SpriteExt
{
	DECLARE_BUILDER( TowerPlace );
	bool init( const TowerPlaseDef & def );
public:
	bool checkClick( const Point & location, float & outDistance );
	void selected();
	void unselected();
protected:
	void changeView();
	void changeViewByEvents();
	void changeViewHard();
private:
	CC_SYNTHESIZE( unsigned, _cost, Cost );
	CC_SYNTHESIZE( int, _index, Index );
	CC_PROPERTY( bool, _active, Active );
	bool _opponentPlace;
	bool _runEvents;
};




NS_CC_END
#endif // #ifndef TowerPlace