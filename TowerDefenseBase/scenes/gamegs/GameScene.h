//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 2016.03.02
//
//
#ifndef __GameScene_h__
#define __GameScene_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/SmartScene.h"
#include "support.h"
#include "GameLayer.h"
#include "ScoreNode.h"
#include "LevelStatisticLayer.h"
#include "online/GameBoardOnline.h"
#include "online/OnlineConnector.h"
NS_CC_BEGIN

class GameScene : public SmartScene, public NodeExt
{
	DECLARE_BUILDER( GameScene );
	bool init( int levelIndex, GameMode mode, bool sessionWithBot );
public:
	virtual void clearStack()override;
	virtual void onEnter()override;
	virtual void onExit()override;

	void closeLevel();
	void restartLevel();
	bool tryRestartLevel( Layer* initiator );
	GameLayer* getGameLayer();
	
	void openPause( Ref*sender );
	void openShop( Ref*sender, bool gears );
	void openShop2( Ref*sender, bool gears );
	LevelStatisticLayer::Pointer openLevelStatistic( 
		LevelStatisticLayer::Result outcome, 
		int gold, 
		int tickets,
		int costfuel, 
		const FinishLevelParams & params
	);

	void openWaitOpponentLayer();
	void closeWaitOpponentLayer();
	void onCloseWaitOpponentLayer();
	void displayName( const std::string& name, bool opponent );
	void connectStatus( bool isConnected );

	void onLevelFinished( FinishLevelParams params );
protected:
	void showAdOnPause();
	virtual void onLayerPushed( LayerPointer layer )override;
	virtual void onLayerPoped( LayerPointer layer )override;
	bool isCanOpenPause()const;
	bool isCanOpenShop()const;

	void multiplayerReportAction( FinishAction act );
	void multiplayerOnOpponentActionEnable( bool enable );
	void multiplayerOnOpponentAction( bool succes, FinishAction act );
	bool multiplayerTryRestart();

	void runLevel();
	void runMultiplayer();
	void createGameLayer();
	void createGameLayerOnline( GameBoardType type );
	void createScoreNode();
	void createInterfaceLocal();
	void createInterfaceOpponent();
	void loadLevel();
	void onConnectionChanged( bool connection );
private:
	std::vector<GameLayer::Pointer> _gameLayers;
	ScoreNode::Pointer _scoreNode;
	CC_SYNTHESIZE_READONLY( int, _levelIndex, LevelIndex );
	CC_SYNTHESIZE_READONLY( GameMode, _levelMode, LevelMode );
	CC_SYNTHESIZE_READONLY( bool, _sessionWithBot, SessionWithBot );
	LayerPointer _layerPause;
	LayerPointer _layerShop;
	LayerExt::Pointer _waitOpponentLayer;
	LevelStatisticLayer::Pointer _layerStatistic;

	FinishAction _multiplayerOpponentFinishAction;
	FinishAction _multiplayerLocalFinishAction;
	int _multiplayerSessionId;
};

NS_CC_END
#endif // #ifndef GameScene