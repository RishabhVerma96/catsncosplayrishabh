//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 2016.03.02
//
//
#include "ml/ImageManager.h"
#include "ml/Config.h"
#include "ml/Text.h"
#include "ScoreNode.h"
#include "resources.h"
#include "ScoreCounter.h"
#include "gameboard.h"
#include "GameLayer.h"
#include "UserData.h"

NS_CC_BEGIN





ScoreNode::ScoreNode()
: _scores()
, _displaySurvivalWavesAsInfinity( true )
{}

ScoreNode::~ScoreNode()
{
	ScoreCounter::shared().observer( kScoreLevel ).remove( _ID );
	ScoreCounter::shared().observer( kScoreHealth ).remove( _ID );
	ScoreCounter::shared().observer( kScoreSurvival ).remove( _ID );
	ScoreCounter::shared().observer( kScoreOpponentLevel ).remove( _ID );
	ScoreCounter::shared().observer( kScoreOpponentHealth ).remove( _ID );
	ScoreCounter::shared().observer( kScoreOpponentSurvival ).remove( _ID );
}

bool ScoreNode::init( GameLayer* layer, const std::string& path )
{
	_gameLayer = layer;
	Node::init();
	load( path );
	
    _enemies = getNodeByPath<Label>( this, getParamCollection().get( "pathto_enemies", "enemies" ) );
	_waves = getNodeByPath<Label>( this, getParamCollection().get( "pathto_waves", "waves" ) );
	_healths = getNodeByPath<Label>( this, getParamCollection().get( "pathto_health", "health" ) );
	_healthsIcon = getNodeByPath<Sprite>( this, getParamCollection().get( "pathto_healthicon", "healthicon" ) );
	_golds = getNodeByPath<Label>( this, getParamCollection().get( "pathto_gold", "gold" ) );
	_survivalScore = getNodeByPath<Label>( this, getParamCollection().get( "pathto_survivalscore", "survivalscore" ) );
	_healthsOpponent = getNodeByPath<Label>( this, getParamCollection().get( "pathto_health_opponent", "health" ) );
	_healthsIconOpponent = getNodeByPath<Sprite>( this, getParamCollection().get( "pathto_healthicon_opponent", "healthicon" ) );
	_goldsOpponent = getNodeByPath<Label>( this, getParamCollection().get( "pathto_gold_opponent", "gold" ) );
	_survivalScoreOpponent = getNodeByPath<Label>( this, getParamCollection().get( "pathto_survivalscore_opponent", "survivalscore" ) );
	_displaySurvivalWavesAsInfinity = (getParamCollection().isExist( "waves_format_infinity" ) == false);

	std::string font;
	if( FileUtils::getInstance()->isFileExist( kFontStroke ) )
		font = kFontStroke;
	else
		font = "fonts/font.fnt";

	if( _healths == nullptr )
	{
		_healths = Text::create( font, "" );
		_healths->setName( "health" );
		_healths->setAnchorPoint( Point( 0, 0.5f ) );
        _healths->setPosition( Point( 190, -25 ) );
		_healths->setScale( 0.5f );
		addChild( _healths, 1 );
	}

	if( _healthsIcon == nullptr )
	{
		_healthsIcon = ImageManager::sprite( Config::shared().get( "resourceGameSceneFolder" ) + "icon_lifes.png" );
		_healthsIcon->setName( "health_icon" );
		_healthsIcon->setPosition( 162, -30 );
		addChild( _healthsIcon );
	}
	if( _golds == nullptr )
	{
		_golds = Text::create( font, "" );
		_golds->setName( "gold" );
		_golds->setAnchorPoint( Point( 0, 0.5f ) );
		_golds->setPosition( Point( 110, -25 ) );
		_golds->setScale( 0.5f );
		addChild( _golds, 1 );

        auto icon = ImageManager::sprite( Config::shared().get( "resourceGameSceneFolder" ) + "icon_gold1.png" );
        icon->setName( "gold_icon" );
        icon->setPosition( 87, -30 );
        addChild( icon );
	}
	if( _waves == nullptr )
	{
		_waves = Text::create( font, "" );
		_waves->setName( "waves" );
		_waves->setAnchorPoint( Point( 0, 0.5f ) );
		_waves->setPosition( Point( 85, -70 ) );
		_waves->setScale( 0.5f );
		addChild( _waves, 1 );

		auto icon = ImageManager::sprite( Config::shared().get( "resourceGameSceneFolder" ) + "icon_wave1.png" );
		icon->setName( "waves_icon" );
		icon->setPosition( 62, -75 );
		addChild( icon );
	}
    
    if( _enemies == nullptr ) {
        _enemies = Text::create(font, "Enemies");
        _enemies->setName( "enemies" );
        _enemies->setAnchorPoint( Point( 0, 0.5f ) );
        _enemies->setPosition( Point( 170, -70 ) );
        _enemies->setScale( 0.5f );
        addChild( _enemies, 1 );
        
        auto icon = ImageManager::sprite( Config::shared().get( "resourceGameSceneFolder" ) + "icon_wave2.png" );
        icon->setName( "enemies_icon" );
        icon->setPosition( 142, -75 );
        addChild( icon );
    }

	ScoreCounter::shared().observer( kScoreLevel ).add( _ID, std::bind( &ScoreNode::on_change_money, this, std::placeholders::_1, false ) );
	ScoreCounter::shared().observer( kScoreHealth ).add( _ID, std::bind( &ScoreNode::on_change_lifes, this, std::placeholders::_1, false ) );
	ScoreCounter::shared().observer( kScoreSurvival ).add( _ID, std::bind( &ScoreNode::updateSurvivalScore, this, std::placeholders::_1, false ) );
	ScoreCounter::shared().observer( kScoreOpponentLevel ).add( _ID, std::bind( &ScoreNode::on_change_money, this, std::placeholders::_1, true ) );
	ScoreCounter::shared().observer( kScoreOpponentHealth ).add( _ID, std::bind( &ScoreNode::on_change_lifes, this, std::placeholders::_1, true ) );
	ScoreCounter::shared().observer( kScoreOpponentSurvival ).add( _ID, std::bind( &ScoreNode::updateSurvivalScore, this, std::placeholders::_1, true ) );

	auto& observerWaves = _gameLayer->getGameBoard().getWaveGenerator().getObserverWaveCounter();
	observerWaves.add( _ID, std::bind( &ScoreNode::updateWaves, this, std::placeholders::_1, std::placeholders::_2 ) );

    auto& observerEnemies = _gameLayer->getGameBoard().getWaveGenerator().getObserverEnemiesCounter();
    observerEnemies.add( _ID, std::bind( &ScoreNode::updateEnemiesCount, this, std::placeholders::_1, std::placeholders::_2 ) );
    
	if( FileUtils::getInstance()->isFileExist( "ini/gamescene/scores.xml" ) )
		NodeExt::load( "ini/gamescene/scores.xml" );

	return true;
}

void ScoreNode::updateEnemiesCount(int remainingEnemies, int TotalEnemies) {

    std::string format = "%d";
    std::string string = StringUtils::format( format.c_str(), remainingEnemies );
    if( string.empty() == false )
        _enemies->setString( string );
}

void ScoreNode::updateWaves( int index, int count )
{
	GameMode const gameMode = _gameLayer->getGameBoard().getGameMode();
	bool const infinityMode = gameMode == GameMode::survival || gameMode == GameMode::multiplayer;
	if( _displaySurvivalWavesAsInfinity && infinityMode )
	{
		std::string const infIconPath = Config::shared().get( "resourceGameSceneFolder" ) + "icon_wave_inf.png";

		if( FileUtils::getInstance()->isFileExist( infIconPath ) )
		{
			_waves->setVisible( false );
			auto wavesIcon = getChildByName<Sprite *>( "waves_icon" );
			if( wavesIcon )
			{
				wavesIcon->setTexture( infIconPath );
				wavesIcon->setAnchorPoint( Vec2( 0.25f, 0.5 ) );
			}
		}
		else
		{
			_waves->setString( "8" );
			_waves->setRotation( 90 );
			_waves->setAnchorPoint( Point( 0.2f, -0.3f ) );
		}
	}
	else
	{
		std::string key = infinityMode ? "waves_format_infinity" : "waves_format";
		std::string format = getParamCollection().get( key, "%d/%d" );
		std::string string = StringUtils::format( format.c_str(), index, count );
		if( string.empty() == false )
			_waves->setString( string );
	}
}

void ScoreNode::setVisibleSurvivalScore( bool visible )
{
	runEvent( visible ? "show_survival" : "hide_survival" );
}

void ScoreNode::updateSurvivalScore( int score, bool forOpponent )
{
	auto node = forOpponent ? _survivalScoreOpponent : _survivalScore;
	if( node )
	{
		if( forOpponent == false )
		{
			node->stopActionByTag( 0x123 );
			auto action = ActionText::create( 1.0f, score, true );
			node->runAction( action );
			action->setTag( 0x123 );
		}
		else
		{
			node->setString( toStr( score ) );
		}
	}
}

void ScoreNode::displayName( const std::string& name, bool forOpponent )
{
	std::string namePath = forOpponent ?
		"pathto_nickname_opponent" :
		"pathto_nickname";
	namePath = getParamCollection().get( namePath, namePath );
	auto label = getNodeByPath<Label>( this, namePath );
	if( label )
		label->setString( name );
}

void ScoreNode::on_change_lifes( int score, bool forOpponent )
{
	auto node = forOpponent ? _healthsOpponent : _healths;
	auto icon = forOpponent ? _healthsIconOpponent : _healthsIcon;
    CCLOG("Starting health :: %s",node->getString().c_str() );
    std::string initialScoreString = node->getString();
	int health = std::max<int>( 0, score );
	node->setString( toStr( health ) );
	if( runEvent( "onPlayerDamaged" ) == false )
	{
		auto run = []( Node*node )
		{
			float s = node->getScale();
			if( node->getActionByTag( 0x12 ) )
				return;

			auto action = EaseBackInOut::create( Sequence::create(
				ScaleTo::create( 0.5f, s * 1.5f ),
				ScaleTo::create( 0.5f, s * 1.0f ),
				nullptr ) );
			action->setTag( 0x12 );
			node->runAction( action );
		};

		run( node );
		run( icon );
	}
    if(UserData::shared().get<bool>("Vibration_Enabled",true) && !initialScoreString.empty()) {
        cocos2d::Device::vibrate(0.4f);
    }
    
}

void ScoreNode::on_change_money( int score, bool forOpponent )
{
	auto node = forOpponent ? _goldsOpponent : _golds;
	int currency = forOpponent ? kScoreOpponentLevel : kScoreLevel;
	int prev = _scores[currency];

	if( prev != score )
	{
		score = std::max<int>( 0, score );
		_scores[currency] = score;
		auto action = ActionText::create( 0.2f, score, true );
		action->setTag( 1 );
		node->stopActionByTag( 1 );
		node->runAction( action );
	}
}



NS_CC_END
