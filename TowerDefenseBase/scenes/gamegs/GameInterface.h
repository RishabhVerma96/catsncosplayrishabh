//
//  IslandDefense
//
//  Created by Vladimir Tolmachevﾂ on 2016.03.02
//
//
#ifndef __GameInterface_h__
#define __GameInterface_h__
#include "cocos2d.h"
#include "macroses.h"
#include "ml/MenuItem.h"
#include "BoxMenu.h"
#include "MenuItemCooldown.h"
#include "HeroIcon.h"
#include "WaveIcon.h"
#include "game/gameboard.h"

NS_CC_BEGIN

class GameLayer;
class GameScene;
class LootDropManager;

struct touchInfo
{
	touchInfo( Node * _nodeBegan = nullptr, Touch * _touch = nullptr )
		: nodeBegin( _nodeBegan )
		, nodeEnd( nullptr )
		, touch( _touch )
		, id( 0 )
	{
		static unsigned _id;
		id = _id++;
	}
	~touchInfo()
	{}
	bool operator < (const touchInfo & src)const
	{
		return id < src.id;
	};

	NodePointer nodeBegin;
	NodePointer nodeEnd;
	Touch* touch;
	unsigned id;
};

enum class Skill
{
	desant,
	bomb,
	heroskill,
	lootpicker,
};

class GameInterface : public Node, public NodeExt
{
	DECLARE_BUILDER( GameInterface );
	bool init( GameLayer* layer, GameScene* scene, bool onlyScroll );
	void createDevMenu();
	void createHeroMenu();
	void createHeroSkillMenu( const std::string& skill, Hero::Pointer hero );
	void createHeroSkillMenu( Hero::Pointer skill );
	void createTouchListeners();
	void createStandartButtons();
	void createSkillButtons();
	void createBoxMenu();
	void createRateButton();
public:
public:
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	virtual void onEnter()override;

	bool scrollBegan( const std::vector<Touch*>& touches );
	bool scrollMoved( const std::vector<Touch*>& touches );
	bool scrollEnded( const std::vector<Touch*>& touches );
	void onTouchesBegan( const std::vector<Touch*>& touches, Event *event );
	void onTouchesMoved( const std::vector<Touch*>& touches, Event *event );
	void onTouchesEnded( const std::vector<Touch*>& touches, Event *event );
	void onTouchesCancelled( const std::vector<Touch*>&touches, Event *event );
	bool onTouchSkillBegan( Touch* touch, Event *event, Skill skill );
	void onTouchSkillEnded( Touch* touch, Event *event, Skill skill );
	void onTouchSkillCanceled( Touch* touch, Event *event );
	bool onTouchHeroBegan( Touch* touch, Event *event );
	void onTouchHeroMoved( Touch* touch, Event *event );
	void onTouchHeroEnded( Touch* touch, Event *event );
	void onTouchHeroCanceled( Touch* touch, Event *event );
    bool onTouchDesantBegan( Touch* touch, Event *event );
    void onTouchDesantMoved( Touch* touch, Event *event );
    void onTouchDesantEnded( Touch* touch, Event *event );
    void onTouchDesantCanceled( Touch* touch, Event *event );

    
	void onEmptyTouch( const Point & touchlocation );
	void onForbiddenTouch( const Point & touchlocation );
	void onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event );

	void onStartGame();
	void onSelectHero( Hero* hero );
    void onSelectDesant( UnitDesant* desant);
	void onHeroDead( Hero* hero );
	void onHeroResurrected( Hero* hero );
    void onDesantDead( UnitDesant* desant );
    void onDesantResurrected( UnitDesant* desant );
	void onClickByTower();
	void onClickByHero();
    void onClickByDesant();
	void onClickByTowerPlace();

	void menuFastModeEnabled( Ref*, bool enabled );
	void menuSkill( Ref*sender, Skill skill );
	void menuSkillCancel( Ref*sender );

	void menuHero( Ref*sender );
    void menuDesant( Ref*sender );
    void menuDesantHeal( Ref*sender );

    void selectHeroAutomatically();
    
	void updateTouchListeners();
	void setTouchDisabled();
	void setTouchNormal();
	void setTouchSkill( Ref*sender, Skill skill );
	void setTouchHero();
    void setTouchDesantMove();

	void resetSkillButtons();

	void disableHeroSkillButtons();
	void createIconForWave( const Route & route, const WaveInfo & wave, UnitLayer::type type, const std::list<std::string> & icons, float delay );
	void removeIconsForWave();
	void showWaveIcons();

	void connectStatus( bool isConnected );

	BoxMenu* getBoxMenu();
protected:
	void setSpeedRate( float rate );
private:
	enum class TouchType
	{
		normal,
		desant,
        desantmove,
		bomb,
		hero,
		heroskill,
		lootpicker,
		none,
	};

	GameLayer* _gameLayer;
	GameScene* _gameScene;
	LootDropManager* _lootDropManager;
	BoxMenu::Pointer _boxMenu;
	mlMenuItem::Pointer _buttonRateNormal;
	mlMenuItem::Pointer _buttonRateFast;
	mlMenuItem::Pointer _buttonPause;
	mlMenuItem::Pointer _buttonShop;
	MenuItemCooldown::Pointer _buttonDesant;
    MenuItemCooldown::Pointer _buttonDesant2;
    MenuItemCooldown::Pointer _buttonDesantHeal;
    MenuItemCooldown::Pointer _buttonBomb;
	MenuItemCooldown::Pointer _buttonLootPicker;
	std::map<Hero::Pointer, std::vector<MenuItemCooldown::Pointer>> _buttonsHeroSkill;
	std::map<Hero::Pointer, HeroIcon::Pointer> _buttonsHero;
	MenuItemCooldown::Pointer _buttonSelectedSkill;
	MenuPointer _menu;
	unsigned _skillsCount;

	bool _firstLaunch;
	bool _skillModeActived;
	bool _isIntteruptHeroMoving;
	CC_SYNTHESIZE( bool, _onlyScroll, IsOnlyScroll );
	IntrusivePtr<EventListener> _touchListenerNormal;
	IntrusivePtr<EventListener> _touchListenerDesant;
    IntrusivePtr<EventListener> _touchListenerDesantMove;
	IntrusivePtr<EventListener> _touchListenerBomb;
	IntrusivePtr<EventListener> _touchListenerLootPicker;
	IntrusivePtr<EventListener> _touchListenerHero;
	IntrusivePtr<EventListener> _touchListenerHeroSkill;
	TouchType _touchNext;
	TouchType _touchActive;
	int _touchesLock;
	CC_PROPERTY( bool, _enabled, Enabled );
	std::map<unsigned, touchInfo> _touches;
	IntrusivePtr<ScrollTouchInfo> _scrollInfo;
	bool _scrollBegan;

	CC_SYNTHESIZE( float, _waveCooldown, WaveCooldown );
	std::vector<WaveIcon::Pointer> _waveIcons;
	bool _connectStatus;
};


NS_CC_END
#endif // #ifndef GameInterface
