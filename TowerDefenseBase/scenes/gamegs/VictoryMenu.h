//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __VictoryMenu_h__
#define __VictoryMenu_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN



class GameScene;

class VictoryMenu : public LayerExt
{
	DECLARE_BUILDER( VictoryMenu );
	bool init( GameScene* scene, bool victory, int scores, int stars, int levelIndex );
public:
	void onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event );
protected:
	void cb_restart( Ref* );
	void cb_close( Ref* );
    void cb_doublereward( Ref* );
    void cb_share( Ref* );
	void restart();
	void close();
private:
	GameScene* _gameScene;
	bool _victory;
    
    void videoResult( bool result, LayerExt* layer );
    std::vector<std::string> getRandomTipsForLevel(int levelIndex);
};




NS_CC_END
#endif // #ifndef VictoryMenu
