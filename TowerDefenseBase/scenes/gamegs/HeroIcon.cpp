#include "HeroIcon.h"
#include "configuration.h"
#include "ml/ImageManager.h"
#include "ml/loadxml/xmlProperties.h"
#include "ScoreCounter.h"
#include "DialogLayer.h"
NS_CC_BEGIN


namespace
{
	const std::string ext( ".png" );
	const std::string empty( "" );
};


HeroIcon::HeroIcon()
: _useDialog( false )
, _resurectHeroOnEnter( false )
{
}

HeroIcon::~HeroIcon( )
{
}

bool HeroIcon::init( const std::string & heroname, const std::string& path, const ccMenuCallback & callback )
{
	do
	{
		CC_BREAK_IF( !mlMenuItem::initWithNormalImage( empty, empty, empty, empty, empty, callback ) );
		load( path );
		std::string cancel = Config::shared().get("resourceGameSceneFolder") + "icon_x_10.png";
		std::string normal = Config::shared().get("resourceGameSceneFolder") + heroname + "_2" + ext;
		std::string selected = Config::shared().get("resourceGameSceneFolder") + heroname + "_3" + ext;
		std::string disabled = Config::shared().get("resourceGameSceneFolder") + heroname + "_1" + ext;

		_unselectedHero = normal;
		_selectedHero = selected;
		
		std::string timer = Config::shared().get("resourceGameSceneFolder") + "hero_progressbar1" + ext;
		std::string timer_bg = Config::shared().get("resourceGameSceneFolder") + "hero_progressbar2" + ext;

		setImageNormal( normal );
		setImageDisabled( disabled );

		_timer = getNodeByPath<ProgressTimer>( this, getParamCollection().get( "pathto_timer", "timer" ) );
		if( !_timer )
		{
			auto progress = Node::create();
			Sprite * image = ImageManager::sprite( timer );
			CC_BREAK_IF( !image );
			_timer = ProgressTimer::create( image );
			CC_BREAK_IF( !_timer );

			image->setAnchorPoint( Point::ANCHOR_BOTTOM_LEFT );
			_timer->setType( ProgressTimer::Type::BAR );
			_timer->setMidpoint( Point( 0.0f, 0.5f ) );
			_timer->setBarChangeRate( Point( 1, 0 ) );
			_timer->setPercentage( 0 );

			progress->addChild( ImageManager::sprite( timer_bg ) );
			progress->addChild( _timer, 1 );

			addChild( progress );
			Point pos;
			pos.x = 189;
			pos.y = -18;
			progress->setPosition( pos );
		}
		_resurrectionMenu = getNodeByPath<Menu>( this, getParamCollection().get( "pathto_resurrectionmenu", "resurrectionmenu" ) );
		auto text = getNodeByPath<Text>( this, getParamCollection().get( "pathto_resurrectioncost", "resurrectioncost" ) );
		if( text )
			text->setString( toStr( HeroExp::shared().getCostResurrection() ) );

		if( 0 )
		{
			_cancelImage = ImageManager::sprite( cancel );
			getNormalImage()->addChild( _cancelImage );
			_cancelImage->setPosition( Point( getNormalImage()->getContentSize() ) - Point( 5, 5 ) );
			_cancelImage->setVisible( false );
			_cancelImage->setScale( 1.5f );
			
			auto s1 = ScaleTo::create( 0.4f, 1.2f );
			auto s2 = ScaleTo::create( 0.4f, 1.0f );
			auto d = DelayTime::create( 1 );
			auto s = Sequence::create( s1, s2, d, nullptr );
			auto action = RepeatForever::create( EaseInOut::create( s, 0.5f ) );
			_cancelImage->runAction( action );
		}
		
		return true;
	}
	while( false );
	return false;
}

ccMenuCallback HeroIcon::get_callback_by_description( const std::string & name )
{
	if( name == "resurrect" )return std::bind( &HeroIcon::resurrect, this, true );
	return mlMenuItem::get_callback_by_description( name );
}

bool HeroIcon::setProperty( const std::string & stringproperty, const std::string & value )
{
	if( stringproperty == "usedialog" )
		_useDialog = strTo<bool>( value ) && Config::shared().get<bool>( "useDialogs" );
	else return mlMenuItem::setProperty( stringproperty, value );
	return true;
}

void HeroIcon::setEnabled( bool var )
{
	mlMenuItem::setEnabled( var );
	if( _resurrectionMenu )
	{
		_resurrectionMenu->setVisible( !var && _useDialog );
	}
}

void HeroIcon::onEnter()
{
	mlMenuItem::onEnter();
	if( _resurectHeroOnEnter )
		resurrect( false );
}

void HeroIcon::setHero( Hero::Pointer hero )
{
	assert( !_hero );
	assert( hero );
	_hero = hero;
	_hero->observerHealth.add( _ID, std::bind( &HeroIcon::listenHeroHealth, this, std::placeholders::_1, std::placeholders::_2) );
}

void HeroIcon::showCancel( bool mode )
{
	if( _cancelImage )
		_cancelImage->setVisible( mode );
	if( _hero )
		_hero->runEvent( mode ? "onselect" : "ondeselect" );

	std::string image = mode ? _selectedHero : _unselectedHero;
	xmlLoader::setProperty( getNormalImage(), xmlLoader::kImage, image );
}

void HeroIcon::listenHeroHealth( float current, float health )
{
	float percent = current / health * 100.f;
	_timer->setPercentage( percent );

	bool enabled = _hero->current_state().get_name() != Hero::State::state_death;
	unsigned char opacity = enabled ? 255 : 128;
	setEnabled( enabled );
	getNormalImage()->setOpacity( opacity );
}

void HeroIcon::resurrect( bool showDialog )
{
	auto cost = HeroExp::shared().getCostResurrection();
	if( cost <= ScoreCounter::shared().getMoney( kScoreCrystals ) )
	{
		ScoreCounter::shared().subMoney( kScoreCrystals, cost, true, "Hero_resurrect" );
		_hero->dieStateFinish();
		_resurectHeroOnEnter = false;
	}
	else if( showDialog && _useDialog )
	{
		_resurectHeroOnEnter = true;
		DialogLayer::showForShop( "ini/dialogs/heroresurrect.xml", 2, 1 );
	}

};

NS_CC_END
