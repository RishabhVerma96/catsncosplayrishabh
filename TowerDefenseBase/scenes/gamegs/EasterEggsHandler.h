//
//  EasterEggsHandler.h
//  Easter Island
//
//  Created by Zeeshan Amjad on 01/11/2017.
//

#ifndef EasterEggsHandler_h
#define EasterEggsHandler_h

#include <stdio.h>

enum EasterEggType
{
    kNone = 0,
    kDuplicateDesant = 1,
    kDroneTower = 2,
    kHealingAbility = 3,
    kDoubleDamageMageTower = 4,
    kHero1DoubleSpeed = 5,
    kHero9Unlocked = 6,
    kIceTower = 7
};


class EasterEggsHandler
{
private:
    EasterEggsHandler();
    virtual ~EasterEggsHandler();
    bool init();
    
public:
    static EasterEggsHandler* getInstance();
    bool isEasterEggUnlocked( const EasterEggType type );
    void unlockEasterEgg( const EasterEggType type );
};

#endif /* EasterEggsHandler_h */
