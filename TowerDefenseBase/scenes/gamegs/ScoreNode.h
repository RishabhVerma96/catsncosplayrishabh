//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 2016.03.02
//
//
#ifndef __ScoreNode_h__
#define __ScoreNode_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN

class GameLayer;

class ScoreNode : public Node, public NodeExt
{
private:
	DECLARE_BUILDER( ScoreNode );
	bool init( GameLayer* layer, const std::string& path );
public:
	void updateWaves( int index, int count );
    void updateEnemiesCount( int remainingEnemies, int TotalEnemies );
	void setVisibleSurvivalScore( bool visible );
	void updateSurvivalScore( int score, bool forOpponent );
	void displayName( const std::string& name, bool forOpponent );
protected:
	void on_change_lifes( int score, bool forOpponent );
	void on_change_money( int score, bool forOpponent );
protected:
	GameLayer* _gameLayer;
	std::map<int, int> _scores;
	LabelPointer _waves;
    LabelPointer _enemies;
	LabelPointer _healths;
	LabelPointer _golds;
	LabelPointer _survivalScore;
	SpritePointer _healthsIcon;
	
	LabelPointer _healthsOpponent;
	LabelPointer _goldsOpponent;
	LabelPointer _survivalScoreOpponent;
	SpritePointer _healthsIconOpponent;
	bool _displaySurvivalWavesAsInfinity;
};

NS_CC_END
#endif // #ifndef ScoreNode
