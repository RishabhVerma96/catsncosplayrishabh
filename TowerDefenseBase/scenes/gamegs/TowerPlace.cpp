//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "ml/ImageManager.h"
#include "ml/Animation.h"
#include "ml/loadxml/xmlProperties.h"
#include "TowerPlace.h"
#include "support.h"
#include "ScoreCounter.h"
#include "configuration.h"
NS_CC_BEGIN





TowerPlace::TowerPlace( )
: _index(0)
, _cost(0)
, _active(true)
, _runEvents(false)
{}

TowerPlace::~TowerPlace( )
{}

bool TowerPlace::init( const TowerPlaseDef & def )
{
	do
	{
		CC_BREAK_IF( !SpriteExt::init() );
		const std::string& path = "ini/gamescene/towerplace.xml";
		if( CCFileUtils::getInstance()->isFileExist( path ) )
		{
			load( path );
			_runEvents = true;
		}
		_active = def.isActive;
		_opponentPlace = def.opponent;
		changeView();
		setPosition( def.position );
		return true;
	}
	while( false );
	return false;
}

void TowerPlace::changeView()
{
	if( _runEvents )
		changeViewByEvents();
	else
		changeViewHard();
}

void TowerPlace::changeViewByEvents()
{
	if( _active )
		if( _opponentPlace == false )
			runEvent( "activeSelf" );
		else
			runEvent( "opponentSelf" );
	else
		if( _opponentPlace == false )
			runEvent( "unactiveSelf" );
		else
			runEvent( "unactiveOpponent" );
}

void TowerPlace::changeViewHard()
{
	if( _active )
	{
		float duration = 2.0f + CCRANDOM_MINUS1_1()*0.1f;

		std::vector<std::string> frames;
		if( _opponentPlace == false )
		{
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_01.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_02.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_03.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_04.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_05.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_06.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_07.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_08.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_09.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_10.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_11.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_12.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_13.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_14.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "active_slot/active_slot_15.png" );
		}
		else
		{
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10001.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10002.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10003.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10004.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10005.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10006.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10007.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10008.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10009.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10010.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10011.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10012.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10013.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10014.png" );
			frames.push_back( Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10015.png" );
		}
		auto anim = createAnimation( frames, duration );
		assert( anim );

		auto animate = RepeatForever::create( Animate::create( anim ) );
		auto action = animate ;
		
		auto scaleUpDown = RepeatForever::create(Sequence::create(ScaleTo::create(0.5f, 1.15f), ScaleTo::create(0.5f, 1.0f), NULL));
		
		runAction(scaleUpDown);
		runAction( action );
	}
	else
	{
		std::string texturename;
		if( _opponentPlace == false )
			texturename = Config::shared().get( "resourceGameSceneFolder" ) + "unactive_slot.png";
		else 
			texturename = Config::shared().get( "resourceGameSceneFolder" ) + "opponent_slot/10015.png";
		auto frame = ImageManager::shared().spriteFrame( texturename );
		if( frame )
		{
			setSpriteFrame( frame );
			return;
		}
		xmlLoader::setProperty( this, xmlLoader::kImage, texturename );
	}
}

bool TowerPlace::checkClick( const Point & location, float & outDistance )
{
	outDistance = getPosition( ).getDistance( location );
	return checkRadiusByEllipse( location, getPosition(), 100 );
}

void TowerPlace::selected( )
{
	//auto s0 = ScaleTo::create( 0.5f, 1.5f );
	//auto s1 = ScaleTo::create( 0.5f, 1.0f );
	//auto action = RepeatForever::create( Sequence::create( s0, s1, nullptr ) );
	//runAction( action );
}

void TowerPlace::unselected( )
{
	//stopAllActions( );
	//setScale( 1 );
}

bool TowerPlace::getActive( void )
{
	return _active;
}

void TowerPlace::setActive( bool var )
{
	assert( _active == false );

	_active = true;
	changeView();
}



NS_CC_END
