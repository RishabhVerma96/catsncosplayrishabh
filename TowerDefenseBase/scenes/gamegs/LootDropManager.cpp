#include "LootDropManager.h"
#include "ObjectFactory.h"
#include "GameLayer.h"
#include "support/UserData.h"
#include "Tutorial.h"

NS_CC_BEGIN


/*****************************************************************************/
//MARK: class Loot
/*****************************************************************************/

Loot::Loot()
:_picked(false)
{}

Loot::~Loot()
{}

bool Loot::init()
{
	do
	{
		CC_BREAK_IF( !Node::init() );
		CC_BREAK_IF( !NodeExt::init() );
		load( "ini/gamescene/lootdrop/loot.xml" );
		return true;
	}
	while( false );
	return false;
}


/*****************************************************************************/
//MARK: class LootPicker
/*****************************************************************************/

LootPicker::LootPicker()
: _parent(nullptr)
{}

LootPicker::~LootPicker()
{
}

bool LootPicker::init( GameBoard* board, LootDropManager * parent )
{
	_parent = parent;
	return UnitUnstoppable::init(board, "ini/gamescene/lootdrop/", "picker.xml");
}

bool LootPicker::checkTargetByRadius( const Node * target )const
{
	assert( target );
	Point a = getPosition();
	Point b = target->getPosition();
	return checkRadiusByEllipse( a, b, _radius );	
}

void LootPicker::on_movefinish()
{
	UnitUnstoppable::on_movefinish();
	_parent->onPickerMoveFinish();
}

/*****************************************************************************/
//MARK: class LootDropManager
/*****************************************************************************/

LootDropManager::LootDropManager()
: _nodropProbability(0)
, _fullProbability(0)
, _picker(nullptr)
, _board(nullptr)
, _pickerMoveFinished(false)
{}

LootDropManager::~LootDropManager()
{
	_picker = nullptr;
}

bool LootDropManager::init( GameBoard * board )
{
	do
	{	
		_board = board;

		pugi::xml_document doc;
		doc.load_file( "ini/gamescene/lootdrop/awards.xml" );
		for( auto node : doc.first_child() )
		{
			std::string type = node.attribute( "type" ).as_string();
			int probability = node.attribute( "probability" ).as_int();
			Award::Pointer award;
			if( type == "none" )
			{
				_nodropProbability = probability;
			}
			else
			{
				auto award = Factory::shared().build<Award>( type );
				award->load( node );
				_awards.push_back( award );
				_probabilities.push_back( probability );
			}
			_fullProbability += probability;
		}
		return true;
	}
	while( false );
	return false;
}

Award::Pointer LootDropManager::generateAward()
{
	std::vector<size_t> indexes;
	size_t index( 0 );
	for( auto probability : _probabilities )
	{
		for( int i = 0; i < probability; ++i )
		{
			indexes.push_back( index );
		}
		++index;
	}
	index = rand() % indexes.size();
	auto awardIndex = indexes[index];
	return _awards[awardIndex];
}

void LootDropManager::dropLoot(Unit::Pointer unit)
{
	int tutorialValue = UserData::shared().get( "tutoriallootdrop", 0 );
	if( _nodropProbability >= rand() % _fullProbability && tutorialValue > 0)
		return;
	auto loot = Loot::create();
	loot->setName( "loot" + toStr( static_cast<int>( _lootObjects.size() ) ) );
	loot->setPosition( unit->getPosition() );
	addLootObject( loot );
	loot->runEvent( "drop" );
	TutorialManager::shared().dispatch( "lootdrop" );
}

void LootDropManager::pickLoot(Loot::Pointer loot)
{
	auto award = generateAward();
	std::string awardName;
	switch( award->type() )
	{
		case AwardType::score: 
		{
			auto awardScore = static_cast<AwardScore*>( award.ptr() );
			awardName = awardScore->getScoreName();
			break;
		}
		case AwardType::bonus:
		{
			auto awardBonus = static_cast<AwardBonus*>( award.ptr() );
			awardName = "bonus_" + toStr( awardBonus->getBonusIndex() );
			break;
		}
		default:
			break;
	}

	auto lootIcon = loot->getChildByName( "icon" );
	auto awardIcon = loot->getChildByName( awardName );
	assert( lootIcon );
	if( awardIcon )
	{
		lootIcon->setVisible( false );
		awardIcon->setVisible( true );
	}

	auto awardPtr = award.ptr();
	auto lootPtr = loot.ptr();
	auto flyAction = static_cast<FiniteTimeAction*>( loot->getAction( "fly_" + awardName ).ptr() );
	auto rewardAction = CallFunc::create( [awardPtr, lootPtr, this]()
	{
		awardPtr->get();
		_board->getGameLayer()->getInterface()->getBoxMenu()->displayCountItems();
		removeLootObject( lootPtr );
		lootPtr->removeFromParent();
	} );

	if( flyAction )
	{
		auto gameinterface = _board->getGameLayer()->getInterface();
		auto worldpos = loot->convertToWorldSpace( Point::ZERO );
		auto pos = gameinterface->convertToNodeSpace( worldpos );
		loot->removeFromParent();
		gameinterface->addChild( loot );
		loot->setPosition( pos );

		auto seq = Sequence::createWithTwoActions( flyAction, rewardAction );
		loot->runAction( seq );
	}
	else
	{
		loot->runAction( rewardAction );
	}
	
}

bool LootDropManager::createPicker(Point pos)
{
	float distance( 200 );
	bool onlyCreepsRoutes( true );
	TripleRoute route = _board->getRoute( UnitLayer::any, pos, distance, onlyCreepsRoutes );
	if( route.main.empty() )
		return false;
	std::reverse( route.main.begin(), route.main.end() );
	
	_picker = LootPicker::create( _board, this );

	_board->getGameLayer()->addObject( _picker, 0 );
	_picker->setPosition( route.main.front() );
	_picker->moveByRoute( route.main );
	_pickerMoveFinished = false;
	return true;
}

void LootDropManager::update( float dt )
{
	if( _picker == nullptr )
		return;
	_picker->update( dt );
	for( auto child : _lootObjects )
	{
		auto loot = static_cast<Loot*>( child.ptr() );
		if( loot->getPicked() )
			continue;
		if( _picker->checkTargetByRadius( loot ) )
		{			
			pickLoot( loot );
			loot->setPicked( true );
		}
	}
	if( _pickerMoveFinished )
	{
		_board->getGameLayer()->removeObject( _picker );
		_picker = nullptr;
	}
}

void LootDropManager::onPickerMoveFinish()
{
	_pickerMoveFinished = true;
}

void LootDropManager::addLootObject(NodePointer node)
{
	_lootObjects.push_back( node );
	_board->getGameLayer()->addObject( node, 0 );
}

void LootDropManager::removeLootObject( NodePointer node )
{
	auto iter = std::find( _lootObjects.begin(), _lootObjects.end(), node );
	assert( iter != _lootObjects.end() );
	_lootObjects.erase( iter );
	_board->getGameLayer()->removeObject( node );
}
NS_CC_END