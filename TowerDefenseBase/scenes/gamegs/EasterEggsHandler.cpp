//
//  EasterEggsHandler.cpp
//  Easter Island
//
//  Created by Zeeshan Amjad on 01/11/2017.
//

#include "EasterEggsHandler.h"
#include "cocos2d.h"

using namespace cocos2d;

static EasterEggsHandler* m_instance = nullptr;

EasterEggsHandler::EasterEggsHandler()
{
    
}

EasterEggsHandler::~EasterEggsHandler()
{
    
}

EasterEggsHandler* EasterEggsHandler::getInstance()
{
    if (!m_instance)
    {
        m_instance = new EasterEggsHandler();
        m_instance->init();
    }
    return m_instance;
}

bool EasterEggsHandler::init()
{
//    const std::string& key = StringUtils::format("EASTER_EGG_TYPE_%d", static_cast<int>(EasterEggType::kDoubleDamageMageTower));
//    UserDefault::getInstance()->setBoolForKey(key.c_str(), false);
//    UserDefault::getInstance()->flush();
    
    return true;
}

bool EasterEggsHandler::isEasterEggUnlocked( const EasterEggType type )
{
    const std::string& key = StringUtils::format("EASTER_EGG_TYPE_%d", static_cast<int>(type));
    return UserDefault::getInstance()->getBoolForKey(key.c_str(), false);
}

void EasterEggsHandler::unlockEasterEgg( const EasterEggType type )
{
    const std::string& key = StringUtils::format("EASTER_EGG_TYPE_%d", static_cast<int>(type));
    UserDefault::getInstance()->setBoolForKey(key.c_str(), true);
    UserDefault::getInstance()->flush();
    
    if(type == kDuplicateDesant) {
        const std::string& key2 = StringUtils::format("EASTER_EGG_TYPE_%d", static_cast<int>(kHealingAbility));
        UserDefault::getInstance()->setBoolForKey(key2.c_str(), true);
        UserDefault::getInstance()->flush();
    }
    
}
