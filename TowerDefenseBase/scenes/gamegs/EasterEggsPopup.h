//
//  EasterEggsPopup.h
//  Easter Island
//
//  Created by Zeeshan Amjad on 30/10/2017.
//

#ifndef EasterEggsPopup_h
#define EasterEggsPopup_h

#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN

typedef std::function<void()> EasterPopupCloseCB;

class EasterEggsPopup : public LayerExt
{
    DECLARE_BUILDER( EasterEggsPopup );
    bool init( const int level, const EasterPopupCloseCB& callback );
public:
    void onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event );
protected:
    void cb_close( Ref* );
    void close();
private:
    EasterPopupCloseCB m_callback;
};

NS_CC_END

#endif /* EasterEggsPopup_h */
