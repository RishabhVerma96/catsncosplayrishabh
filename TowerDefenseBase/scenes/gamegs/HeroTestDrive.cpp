//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 2016.02.02
//
//
#include "services/flurry/flurry_.h"
#include "ml/SmartScene.h"
#include "HeroTestDrive.h"
#include "GameScene.h"
#include "game/unit/Hero.h"
#include "UserData.h"
#include "shop/ShopLayer.h"
#include "DialogLayer.h"
#include "BuyHeroes.h"
#include "ScoreCounter.h"
NS_CC_BEGIN

static HeroTestDriver* HeroTestDriver_instance( nullptr );
HeroTestDriver* HeroTestDriver_getInstance()
{
	return HeroTestDriver_instance;
}

HeroTestDriver::HeroTestDriver()
: _exp( 0.f )
, _showOfferOnVictory( false )
{
	assert( HeroTestDriver_instance == nullptr );
	HeroTestDriver_instance = this;
}
HeroTestDriver::~HeroTestDriver()
{
	HeroTestDriver_instance = nullptr;
}

bool HeroTestDriver::init( const std::string& heroname )
{
	_hero = heroname;
	return true;
}

void HeroTestDriver::setHero()
{
	auto index = strTo<int>( _hero.substr( strlen( "hero" ) ) ) - 1;
	std::vector<unsigned> heroes;
	heroes.push_back( index );
	GameBoard::heroesOnLevel = heroes;

	_exp = HeroExp::shared().getEXP( _hero );
	auto exp = HeroExp::shared().getHeroLevelExtValue( 14 );
	HeroExp::shared().setEXP( _hero, exp, false );
}

void HeroTestDriver::restoreHero()
{
	GameBoard::heroesOnLevel.clear();
	HeroExp::shared().setEXP( _hero, _exp );
	UserData::shared().save();
}

void HeroTestDriver::showOfferOnVictory()
{
	_showOfferOnVictory = true;
}

void HeroTestDriver::onLevelFinished( GameScene* gamescene, bool result )
{
	if( result && _showOfferOnVictory )
	{
		auto layer = HeroTestDriveLayer::create( gamescene, _hero, false, true );
		if( gamescene )
			gamescene->pushLayer( layer, true );
	}
	removeFromParent();
}

HeroTestDriveLayer::HeroTestDriveLayer()
: _gameScene(nullptr)
, _restartLevel(false)
{
}

HeroTestDriveLayer::~HeroTestDriveLayer( )
{
	ShopLayer::observerOnFailed().remove( _ID );
	ShopLayer::observerOnPurchase().remove( _ID );
}

bool HeroTestDriveLayer::didLaunched( GameScene* gamescene )
{
	auto level = gamescene->getGameLayer()->getGameBoard().getCurrentLevelIndex();
	auto level_min = Config::shared().get<float>( "minLevelHero" );
	if( level < level_min )
		return false;
	auto hero = HeroExp::shared().getHeroOnTestDrive();
	if( hero.empty() == false )
	{
		auto layer = HeroTestDriveLayer::create( gamescene, hero, true );
		auto scene = dynamic_cast<SmartScene*>(Director::getInstance()->getRunningScene());
		if( scene && layer )
		{
			UserData::shared().write( "HeroTestDriveCountLaunched" + hero, true );
			scene->pushLayer( layer, true );
			HeroExp::shared().popHeroOnTestDrive();
			return true;
		}
	}
	return false;
}

void HeroTestDriveLayer::onLevelStatisticLayerDidShowed( GameScene* layer, bool victory )
{
	if( HeroTestDriver_instance )
		HeroTestDriver_instance->onLevelFinished( layer, victory );
}

bool HeroTestDriveLayer::init( GameScene* scene, const std::string& heroname, bool restartLevel, bool activateOffer )
{
	do
	{
		_gameScene = scene;
		CC_BREAK_IF( !_gameScene );
		CC_BREAK_IF( !LayerExt::init() );
		LayerExt::initBlockLayer( "images/loading.png" );
		_hero = heroname;
		_restartLevel = restartLevel;

		xmlLoader::macros::set( "heroname", heroname );
		load( "ini/gamescene/hero_testdrive.xml" );
		xmlLoader::macros::erase( "heroname" );
		applyParams();

		if( activateOffer )
			this->activateOffer();

		if( activateOffer )
			statistic_hire();
		else
			statistic_try();

		return true;
	}
	while( false );
	return false;
}

void HeroTestDriveLayer::createHeroesNow()
{
	auto& board = _gameScene->getGameLayer()->getGameBoard();
	board.createHeroes();
}

void HeroTestDriveLayer::activateOffer()
{
	runEvent( "activateOffer_" + _hero );
}

bool HeroTestDriveLayer::loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )
{
	if( tag == "params" )
	{
		for( auto xml : xmlnode )
		{
			std::string name = xml.name();
			xmlLoader::load_paramcollection( _params[name], xml );
		}
	}
	else 
		return LayerExt::loadXmlEntity( tag, xmlnode );
	return true;
}

ccMenuCallback HeroTestDriveLayer::get_callback_by_description( const std::string & name )
{
	if( name == "try" ) return std::bind( &HeroTestDriveLayer::tryHero, this );
	if( name == "purchase" ) return std::bind( &HeroTestDriveLayer::purchase, this );
	if( name == "close" ) return std::bind( &HeroTestDriveLayer::close, this, false );
	if( name == "close_track" ) return std::bind( &HeroTestDriveLayer::close, this, true );
	return LayerExt::get_callback_by_description( name );
}

void HeroTestDriveLayer::close( bool track )
{
	retain();
	removeFromParent();
	if( _restartLevel )
	{
		restartLevel();
	}
	if( track )
		statistic_button_close();
	release();
}

void HeroTestDriveLayer::applyParams()
{
	auto textNode = getNodeByPath<Text>( this, getParamCollection().get( "pathto_button_text", "menu/try/button/text" ) );
	if( textNode )
	{
		auto text = _params[_hero].get( "try_text" );
		if( text.empty() == false )
			textNode->setString( text );
	}
}

void HeroTestDriveLayer::tryHero()
{
	retain();
	removeFromParent();
	auto driver = HeroTestDriver::create( _hero );
	driver->setHero();
	if( _restartLevel )
	{
		restartLevel();
	}
	else
	{
		createHeroesNow();
	}
	driver->restoreHero();
	_gameScene->addChild( driver );
	auto& board = _gameScene->getGameLayer()->getGameBoard();

	auto rate = board.getWaveGenerator().getUnitsRate();
	rate *= strTo<float>( _params[_hero].get( "creeps_rate", "1" ) );
	board.getWaveGenerator().setUnitsRate( rate );

	bool offer = strTo<bool>( _params[_hero].get( "offer_purchase" ) );
	if( offer )
		driver->showOfferOnVictory();

	release();
}

void HeroTestDriveLayer::purchase()
{
	int heroesAvailabled = Config::shared().get<int>( "heroesAvailabled" );
	int index = strTo<int>( _hero.substr( 4 ) );
	int availabled = index <= heroesAvailabled;
	if( availabled )
	{
		auto productID = Config::shared().get( "inappPackage" ) + _hero;
		ShopLayer::observerOnFailed().add( _ID, std::bind( &HeroTestDriveLayer::purchaseAnswer, this, false ) );
		ShopLayer::observerOnPurchase().add( _ID, std::bind( &HeroTestDriveLayer::purchaseAnswer, this, true ) );
		pushBlockLayer( true, 30 );
		inapp::purchase( productID );
		statistic_button_hire();
	}
	else
	{
		//auto callback = [this]( bool answer ){
		//	auto url = Config::shared().get( "linkToStorePaidVersion" );
		//	if( answer )
		//		Application::getInstance()->openURL( url );
		//	disappearance();
		//};
		//DialogLayer::createAndRun( "ini/dialogs/getpro_hero.xml", std::bind( callback, std::placeholders::_1 ) );
		
		auto layer = BuyHeroes::create( nullptr );
		auto z = layer->getLocalZOrder();
		getSmartScene()->pushLayer( layer, true );
		if( z != 0 ) layer->setLocalZOrder( z ); 

		auto score = getScene()->getChildByName( "scorelayer" );
		if( score )score->setVisible( false );
	}
}

void HeroTestDriveLayer::purchaseAnswer( bool success )
{
	popBlockLayer();
	if( success )
	{
		runEvent( "purchased" );
		auto index = strTo<int>( _hero.substr( strlen( "hero" ) ) ) - 1;
		UserData::shared().hero_select( index );

		statistic_purchasing();
	}
	else
	{
		disappearance();
	}
}

void HeroTestDriveLayer::restartLevel()
{
	auto score = ScoreCounter::shared().getMoney( kScoreFuel );
	_gameScene->restartLevel();
	ScoreCounter::shared().setMoney( kScoreFuel, score, true );
}

void HeroTestDriveLayer::statistic_try()
{
	ParamCollection pc;
	pc["event"] = "TestDrive_ShowTry";
	pc["hero"] = _hero;
	pc["level"] = toStr( _gameScene->getGameLayer()->getGameBoard().getCurrentLevelIndex() );
	pc["wave"] = toStr<int>( _gameScene->getGameLayer()->getGameBoard().getWaveGenerator().getWaveIndex() );
	flurry::logEvent( pc );
}
void HeroTestDriveLayer::statistic_hire()
{
	ParamCollection pc;
	pc["event"] = "TestDrive_ShowHire";
	pc["hero"] = _hero;
	pc["level"] = toStr( _gameScene->getGameLayer()->getGameBoard().getCurrentLevelIndex() );
	flurry::logEvent( pc );
}
void HeroTestDriveLayer::statistic_button_hire()
{
	ParamCollection pc;
	pc["event"] = "TestDrive_PressButtonHire";
	pc["hero"] = _hero;
	pc["level"] = toStr( _gameScene->getGameLayer()->getGameBoard().getCurrentLevelIndex() );
	flurry::logEvent( pc );
}
void HeroTestDriveLayer::statistic_button_close()
{
	ParamCollection pc;
	pc["event"] = "TestDrive_PressButtonClose";
	pc["hero"] = _hero;
	pc["level"] = toStr( _gameScene->getGameLayer()->getGameBoard().getCurrentLevelIndex() );
	flurry::logEvent( pc );
}
void HeroTestDriveLayer::statistic_purchasing()
{
	ParamCollection pc;
	pc["event"] = "TestDrive_PurchasingSuccessful";
	pc["hero"] = _hero;
	pc["level"] = toStr( _gameScene->getGameLayer()->getGameBoard().getCurrentLevelIndex() );
	flurry::logEvent( pc );
}

NS_CC_END