//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __GAMEPAUSE_SCENE__
#define __GAMEPAUSE_SCENE__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "ml/Slider.h"

NS_CC_BEGIN;

class GameLayer;

class GamePauseLayer : public Menu, public NodeExt, public LayerBlur
{
	DECLARE_BUILDER( GamePauseLayer );
	bool init( GameScene* scene, const std::string& path, bool showScores = true );
public:
	virtual void onEnter();
	virtual void onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event );
	virtual bool isBlurActive()override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override;
	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override;
	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;

	void onNotEnought( int scoreType );
protected:
	virtual ccMenuCallback get_callback_by_description( const std::string & name );

	void cb_resume( Ref*sender );
	void cb_restart( Ref*sender );
	void cb_exit( Ref*sender );
	void cb_sound( Ref*sender, bool enabled );
	void cb_music( Ref*sender, bool enabled );
    void cb_vibration( Ref*sender, bool enabled );
	void cb_soundVolume( Ref*sender );
	void cb_musicVolume( Ref*sender );
	
#if PC == 1
	void cb_openOptions( Ref* sender );
	void cb_cancelOptions( Ref* sender );
	void cb_saveOptions( Ref* sender );
	void cb_openControls( Ref* sender );
	void cb_closeControls( Ref* sender );
#endif

	void checkAudio();
	void gameresume();
	void restart();
	void exit();
	void shop_did_closed( );

	void fadeexit();
	void fadeenter();
	void checkFullscreen();
protected:
	GameScene* _gameScene;
	//SpritePointer m_shadow;
	MenuItemPointer _resume;
	MenuItemPointer _restart;
	MenuItemPointer _quit;
	MenuItemPointer _store;
	MenuItemPointer _music_on;
	MenuItemPointer _music_off;
	MenuItemPointer _sound_on;
	MenuItemPointer _sound_off;
    MenuItemPointer _vibration_on;
    MenuItemPointer _vibration_off;
	mlSlider::Pointer _sound_volume;
	mlSlider::Pointer _music_volume;
	bool _showScores;
	float _scaleFactor;
	bool _useDialog;
	bool _hideScoresOnExit;

#if PC == 1
	float _presavedMusicVolume;
	float _presavedSoundVolume;
	bool _presavedFullscreen;
	bool _optionsIsOpen;
	bool _controlsIsOpen;
#endif
};

NS_CC_END;

#endif
