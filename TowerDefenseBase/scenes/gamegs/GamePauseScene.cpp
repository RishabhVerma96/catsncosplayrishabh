//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "GameScene.h"
#include "GamePauseScene.h"
#include "consts.h"
#include "MenuItemTextBG.h"
#include "ml/ScrollMenu.h"
#include "gameboard.h"
#include "ml/Audio/AudioEngine.h"
#include "ml/Audio/AudioMenu.h"
#include "resources.h"
#include "ml/Language.h"
#include "ScoreCounter.h"
#include "ml/SmartScene.h"
#include "ShopLayer.h"
#include "ShopLayer2.h"
#include "ScoreLayer.h"
#include "configuration.h"
#include "MenuItem.h"
#include "UserData.h"
#include "DialogLayer.h"
#include "plugins/AdsPlugin.h"


NS_CC_BEGIN;

float kFadeDuration( 0.2f );

class DialogRestartGame : public LayerExt
{
	DECLARE_BUILDER( DialogRestartGame );
	bool init()
	{
		LayerExt::init();
		load( "ini/maings/dialogclosegame.xml" );
		return true;
	}
	ccMenuCallback get_callback_by_description( const std::string & name )
	{
		if( name == "yes" ) return std::bind( []( Ref* ){ Director::getInstance()->end(); }, std::placeholders::_1 );
		if( name == "no" ) return std::bind( [this]( Ref* ){ removeFromParent(); }, std::placeholders::_1 );
		return LayerExt::get_callback_by_description( name );
	}
};
DialogRestartGame::DialogRestartGame() {}
DialogRestartGame::~DialogRestartGame() {}

GamePauseLayer::GamePauseLayer()
: _scaleFactor( 1 )
, _showScores(false)
, _useDialog( false )
, _hideScoresOnExit( false )
#if PC == 1
, _optionsIsOpen( false )
, _controlsIsOpen( false )
#endif
{
};

bool GamePauseLayer::init( GameScene* scene, const std::string& path, bool showScores )
{
	do
	{
		_gameScene = scene;
		Size desSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
		CC_BREAK_IF( !Menu::init() );
		setPosition( Point( desSize / 2 ) );
		setContentSize( Size::ZERO );
		setKeyboardEnabled( true );
		_showScores = showScores;
		
		int costType = 0;
		int costValue = 0;
		if( _gameScene )
		{
			auto& board = _gameScene->getGameLayer()->getGameBoard();
			int indexLevel = board.getCurrentLevelIndex();
			GameMode mode = board.getGameMode();
			LevelParams::shared().getPayForLevel( indexLevel, mode, costType, costValue );

			if( costType == kScoreCrystals )
			{
				xmlLoader::macros::set( "costgold", toStr( costValue ) );
				xmlLoader::macros::set( "cost_currency", "gold" );
			}
			else if( costType == kScoreFuel )
			{
				xmlLoader::macros::set( "costfuel", toStr( costValue ) );
				xmlLoader::macros::set( "cost_currency", "fuel" );
			}

			bool const multiplayer = mode == GameMode::multiplayer;
			xmlLoader::macros::set( "multiplayer", toStr( multiplayer ) );
			xmlLoader::macros::set( "not_multiplayer", toStr( !multiplayer ) );
		}
		load( path );

		_resume = getChildByName<MenuItem*>( "resume" );
		_restart = getChildByName<MenuItem*>( "restart" );
		auto restart_fuel = getChildByName<MenuItem*>( "restart_fuel" );
		_quit = getChildByName<MenuItem*>( "quit" );
		_store = getChildByName<MenuItem*>( "store" );

		auto optionsParent = getParamCollection().get( "options_parent", "" );
		_music_on = getNodeByPath<MenuItem>( this, optionsParent + "music_on");
		_music_off = getNodeByPath<MenuItem>( this, optionsParent + "music_off" );
		_sound_on = getNodeByPath<MenuItem>( this, optionsParent + "sound_on" );
		_sound_off = getNodeByPath<MenuItem>( this, optionsParent + "sound_off" );
        _vibration_on = getNodeByPath<MenuItem>( this, optionsParent + "vibration_on" );
        _vibration_off = getNodeByPath<MenuItem>( this, optionsParent + "vibration_off" );
		_music_volume = getNodeByPath<mlSlider>( this, optionsParent + "music_volume" );
		_sound_volume = getNodeByPath<mlSlider>( this, optionsParent + "sound_volume" );
        
		auto cost = getNodeByPath<Text>( _restart, "normal/cost" );
		if( !cost ) cost = getNodeByPath<Text>( restart_fuel, "normal/cost" );
		
		if( cost )cost->setString( toStr( costValue ) );

		auto bg = getChildByName( "bg" );
		if( bg )
		{
			auto size = bg->getContentSize();
			float sx = std::min<float>( 1, desSize.width / size.width );
			float sy = std::min<float>( 1, desSize.height / size.height );
			float s = std::min<float>( sx, sy );
			s = std::min<float>( 1, s );
			_scaleFactor = s;
		}
        
		auto button_restart = getNodeByPath( this, "restart" );
		auto button_restart_withfuel = getNodeByPath( this, "restart_fuel" );
        if (Config::shared().get<bool>( "useFuel" )) 
		{
			if( button_restart_withfuel == nullptr )
			{
				auto restart_text = getNodeByPath( button_restart, "normal/restart_text" );
				auto restart_fuel_icon = getNodeByPath( button_restart, "normal/icon" );
				auto restart_fuel_cost = getNodeByPath( button_restart, "normal/cost" );
				if( restart_text )
					restart_text->setPositionY( restart_text->getPositionY() + 14.f );
				if( restart_fuel_icon )
					restart_fuel_icon->setVisible( true );
				if( restart_fuel_cost )
					restart_fuel_cost->setVisible( true );
			}
        }
		if( button_restart && restart_fuel )
			button_restart->setVisible( Config::shared().get<bool>( "useFuel" ) == false );
		if( button_restart_withfuel )
			button_restart_withfuel->setVisible( Config::shared().get<bool>( "useFuel" ) );

		fadeenter();
		checkAudio();
		checkFullscreen();

		return true;
	}
	while( false );
	return false;
}

GamePauseLayer::~GamePauseLayer()
{
	removeAllChildrenWithCleanup( true );
	//Director::getInstance()->getTextureCache()->removeUnusedTextures();
};

void GamePauseLayer::onEnter()
{
	Menu::onEnter();
	setKeyboardEnabled( true );
	AudioEngine::shared().playEffect( kSoundGamePauseOn );

	if( _showScores )
	{
		auto scene = Director::getInstance()->getRunningScene();
		auto scores = scene->getChildByName( "scorelayer" );
		if( !scores )
		{
			auto scores = ScoreLayer::create();
			scene->addChild( scores, 999 );
			_hideScoresOnExit = true;
		}
	}
	if( _resume ) _resume->setEnabled( true );
	if( _restart ) _restart->setEnabled( true );
}

void GamePauseLayer::onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event )
{
	if( keyCode == EventKeyboard::KeyCode::KEY_BACK )
	{
#if PC != 1
		cb_resume( nullptr );
#elif PC == 1
		if( _optionsIsOpen )
			cb_cancelOptions( nullptr );
		else if( _controlsIsOpen )
			cb_closeControls( nullptr );
		else
			cb_resume( nullptr );
#endif
	}
}

void GamePauseLayer::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		Menu::visit( renderer, parentTransform, parentFlags );
}

bool GamePauseLayer::setProperty( const std::string & stringproperty, const std::string & value )
{
	if( stringproperty == "usedialog" )
		_useDialog = strTo<bool>( value ) && Config::shared().get<bool>( "useDialogs" );
	else return NodeExt::setProperty( stringproperty, value );
	return true;
}

bool GamePauseLayer::isBlurActive()
{
	return !isRunning();
}

void GamePauseLayer::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	Menu::visit( renderer, parentTransform, parentFlags );
}

ccMenuCallback GamePauseLayer::get_callback_by_description( const std::string & name )
{
	if( name == "resume" ) return CC_CALLBACK_1( GamePauseLayer::cb_resume, this );
	if( name == "restart" ) return CC_CALLBACK_1( GamePauseLayer::cb_restart, this );
	if( name == "quit" ) return CC_CALLBACK_1( GamePauseLayer::cb_exit, this );
	if( name == "music_on" ) return CC_CALLBACK_1( GamePauseLayer::cb_music, this, false );
	if( name == "music_off" ) return CC_CALLBACK_1( GamePauseLayer::cb_music, this, true );
	if( name == "sound_on" ) return CC_CALLBACK_1( GamePauseLayer::cb_sound, this, false );
	if( name == "sound_off" ) return CC_CALLBACK_1( GamePauseLayer::cb_sound, this, true );
    if( name == "vibration_on" ) return CC_CALLBACK_1( GamePauseLayer::cb_vibration, this, false );
    if( name == "vibration_off" ) return CC_CALLBACK_1( GamePauseLayer::cb_vibration, this, true );
    
	if( name == "fullscreen" )
	{
		auto cb = [this]( Ref*sender )
		{
			auto fullscreen = !UserData::shared().get("fullscreen", true);
			UserData::shared().write("fullscreen", fullscreen);
			UserData::shared().save();
			checkFullscreen();
			auto useSaveButton = strTo<bool>( getParamCollection().get( "use_save_button", "no" ) );
			if( useSaveButton == false ) 
			{
				auto layer = DialogRestartGame::create();
				auto scene = static_cast<SmartScene*>(getScene());
				scene->pushLayer( layer, true );
			}
		};
		return std::bind( cb, std::placeholders::_1 );
	}
	if( name == "sound_volume" ) return CC_CALLBACK_1( GamePauseLayer::cb_soundVolume, this );
	if( name == "music_volume" ) return CC_CALLBACK_1( GamePauseLayer::cb_musicVolume, this );
	if( name == "quit_game" ) return std::bind( []( Ref* ) { Director::getInstance()->end(); }, std::placeholders::_1 );
#if PC == 1
	if( name == "open_options" ) return CC_CALLBACK_1( GamePauseLayer::cb_openOptions, this );
	if( name == "save_options" ) return CC_CALLBACK_1( GamePauseLayer::cb_saveOptions, this );
	if( name == "cancel_options" ) return CC_CALLBACK_1( GamePauseLayer::cb_cancelOptions, this );
	if( name == "open_controls" ) return CC_CALLBACK_1( GamePauseLayer::cb_openControls, this );
	if( name == "close_controls" ) return CC_CALLBACK_1( GamePauseLayer::cb_closeControls, this );
#endif
	return nullptr;
}

void GamePauseLayer::cb_resume( Ref*sender )
{
	AdsPlugin::shared().hideBanner();
	auto func = CallFunc::create( std::bind( &GamePauseLayer::gameresume, this ) );
	runAction( Sequence::create( DelayTime::create( kFadeDuration ), func, nullptr ) );
	fadeexit();
}

void GamePauseLayer::cb_restart( Ref*sender )
{
	AdsPlugin::shared().hideBanner();
	if( _resume ) _resume->setEnabled( false );
	if( _restart ) _restart->setEnabled( false );
	_gameScene->tryRestartLevel( this );
}

void GamePauseLayer::onNotEnought( int scoreType )
{
#if PC != 1
	int const shopTab = scoreType == kScoreFuel ? 2 : 1;

	if( _useDialog == false )
	{
		LayerPointer shop;
		auto version = Config::shared().get<int>( "shopversion" );
		if( version < 2 )
			shop = ShopLayer::create( Config::shared().get<bool>( "useFreeFuel" ), false, false, false );
		else if( version == 2 )
			shop = ShopLayer2::create( 1, shopTab );

		if( shop )
		{
			auto scene = Director::getInstance()->getRunningScene();
			auto smartscene = dynamic_cast<SmartScene*>(scene);
			assert( smartscene );
			smartscene->pushLayer( shop, true );
		}
	}
	else
	{
		DialogLayer::showForShop( "ini/dialogs/nofuel.xml", 1, 2 );
	}
#endif
}

void GamePauseLayer::cb_exit( Ref*sender )
{
	AdsPlugin::shared().hideBanner();
	auto delay = DelayTime::create( kFadeDuration );
	auto func = CallFunc::create( std::bind( &GamePauseLayer::exit, this ) );
	runAction( Sequence::create( delay, func, nullptr ) );
	fadeexit();

	if( _resume ) _resume->setEnabled( false );
	if( _restart ) _restart->setEnabled( false );
}

void GamePauseLayer::cb_sound( Ref*sender, bool enabled )
{
	AudioEngine::shared().soundEnabled(enabled);
	checkAudio();
}

void GamePauseLayer::cb_music( Ref*sender, bool enabled )
{
	AudioEngine::shared().musicEnabled(enabled);
	checkAudio();
}

void GamePauseLayer::cb_vibration( Ref* sender, bool enable) {
    UserData::shared().write("Vibration_Enabled", enable);
    UserData::shared().save();
    checkAudio();
}

void GamePauseLayer::cb_soundVolume( Ref*sender )
{
	auto slider = dynamic_cast<mlSlider*>(sender);
	assert( slider );
	AudioEngine::shared().setSoundVolume( slider->getProgress() );
}

void GamePauseLayer::cb_musicVolume( Ref*sender )
{
	auto slider = dynamic_cast<mlSlider*>(sender);
	assert( slider );
	AudioEngine::shared().setMusicVolume( slider->getProgress() );
}

void GamePauseLayer::checkAudio()
{
	bool s = AudioEngine::shared().isSoundEnabled();
    bool m = AudioEngine::shared().isMusicEnabled();
    bool v = UserData::shared().get<bool>("Vibration_Enabled",true);
    
	if( _sound_off )_sound_off->setVisible( !s );
	if( _sound_on  )_sound_on->setVisible( s );
	if( _music_off )_music_off->setVisible( !m );
	if( _music_on )_music_on->setVisible( m );
    if(_vibration_on) _vibration_on->setVisible(v);
    if(_vibration_off) _vibration_off->setVisible(!v);
    
	float sv = AudioEngine::shared().getSoundVolume();
	float mv = AudioEngine::shared().getMusicVolume();
	if( _sound_volume ) _sound_volume->setProgress( sv );
	if( _music_volume ) _music_volume->setProgress( mv );
}

void GamePauseLayer::gameresume()
{
	AudioEngine::shared().resumeAllEffects();
	setEnabled( false );
	removeFromParent();
}

void GamePauseLayer::restart()
{
	setEnabled( false );
	removeFromParent();
	_gameScene->restartLevel();
}

void GamePauseLayer::exit()
{
	setEnabled( false );
	Director::getInstance()->popScene();
	removeFromParent();
}

void GamePauseLayer::shop_did_closed()
{
	setEnabled( true );
}

void GamePauseLayer::fadeexit()
{
	auto scene = Director::getInstance()->getRunningScene();
	auto scores = scene->getChildByName("scorelayer");
	if( scores && _hideScoresOnExit )
	{
		scores->removeFromParent();
	}

	if( runEvent( "hide" ) )
		return;

	runAction( ScaleTo::create( kFadeDuration, _scaleFactor * 1.2f ) );
	runAction( FadeOut::create( kFadeDuration ) );
}

void GamePauseLayer::fadeenter()
{
	AdsPlugin::shared().showBanner();
	if( runEvent( "show" ) )
		return;

	setScale( _scaleFactor * 1.2f );
	runAction( ScaleTo::create( kFadeDuration, _scaleFactor * 1.f ) );
	runAction( FadeIn::create( kFadeDuration ) );
}

void GamePauseLayer::checkFullscreen()
{
	auto fullscreen = UserData::shared().get("fullscreen", true);
	auto optionsParent = getParamCollection().get( "options_parent", "" );
	auto item = getNodeByPath<mlMenuItem>(this, optionsParent + "fullscreen");
	if (item)
	{
		auto on = item->getParamCollection().get( "on", "" );
		auto off = item->getParamCollection().get( "off", "" );
		item->setImageNormal( fullscreen ? on : off );
	}
}

#if PC == 1
void GamePauseLayer::cb_openOptions( Ref* sender )
{
	runEvent( "open_options" );
	_presavedMusicVolume = AudioEngine::shared().getMusicVolume();
	_presavedSoundVolume = AudioEngine::shared().getSoundVolume();
	_presavedFullscreen = UserData::shared().get( "fullscreen", true );
	checkAudio();
	checkFullscreen();
	_optionsIsOpen = true;
}

void GamePauseLayer::cb_saveOptions( Ref* sender )
{
	auto fullscreen = UserData::shared().get( "fullscreen", true );
	if( fullscreen != _presavedFullscreen )
	{
		auto layer = DialogRestartGame::create();
		auto scene = static_cast<SmartScene*>(getScene());
		scene->pushLayer( layer, true );
	}
	runEvent( "close_options" );
	_optionsIsOpen = false;
}

void GamePauseLayer::cb_cancelOptions( Ref* sender )
{
	runEvent( "close_options" );
	AudioEngine::shared().setMusicVolume( _presavedMusicVolume );
	AudioEngine::shared().setSoundVolume( _presavedSoundVolume );
	UserData::shared().write( "fullscreen", _presavedFullscreen );
	UserData::shared().save();
	_optionsIsOpen = false;
}

void GamePauseLayer::cb_openControls( Ref* sender )
{
	runEvent( "open_controls" );
	_controlsIsOpen = true;
	//const std::string path = "ini/hotkeys.xml";
	//pugi::xml_document doc;
	//if( !FileUtils::getInstance()->isFileExist( path ) ) return;
	//doc.load_file( path.c_str() );
	//auto root = doc.root().first_child();
	//for( auto child : root )
	//{
	//	std::string name = child.attribute( "name" ).as_string();
	//	if( name.empty() == false )
	//	{
	//		std::string key = child.text().as_string();
	//		_keyCodes[name] = strTo<EventKeyboard::KeyCode>( key );
	//	}
	//}
}

void GamePauseLayer::cb_closeControls( Ref* sender )
{
	runEvent( "close_controls" );
	_controlsIsOpen = false;
}
#endif
NS_CC_END;
