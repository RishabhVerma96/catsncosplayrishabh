//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "ml/Language.h"
#include "MenuTower.h"
#include "ScoreCounter.h"
#include "GameLayer.h"
#include "Tower.h"
#include "support.h"
#include "Tutorial.h"
#include "configuration.h"
#include "EasterEggsHandler.h"

NS_CC_BEGIN



MenuTower::MenuTower()
: _waitSellConfirm( false )
, _waitUpgradeConfirm( false )
, _disabled ( true )
{}

MenuTower::~MenuTower()
{
	ScoreCounter::shared().observer( kScoreLevel ).remove( _ID );
}

bool MenuTower::init( GameLayer* layer )
{
	do
	{
		_gameLayer = layer;
		ScrollMenu::init();
		NodeExt::init();

		NodeExt::load( "ini/gamescene", "menutower.xml" );

		_upgrade = getMenuItemByName( "upgrade" );
		_upgradeUn = getMenuItemByName( "upgrade_un" );
		_confirm = getMenuItemByName( "confirm" );
		_confirmUn = getMenuItemByName( "confirm_un" );
		_sell = getMenuItemByName( "sell" );
		_lock = getMenuItemByName( "lock" );

		buttons = { _upgrade, _upgradeUn, _confirm, _confirmUn, _sell, _lock };

		_upgrade->setCallback( std::bind( &MenuTower::activateUpgrade, this, std::placeholders::_1, true ) );
		_upgradeUn->setCallback( std::bind( &MenuTower::activateUpgrade, this, std::placeholders::_1, false ) );
		_sell->setCallback( std::bind( &MenuTower::activateSell, this, std::placeholders::_1, false ) );
		_lock->setCallback( std::bind( &MenuTower::lockClick, this, std::placeholders::_1 ) );

		_desc.node = getChildByName( "desc" );
		if( _desc.node )
		{
			_desc.name.reset( _desc.node->getChildByName<Text*>( "name" ) );
			_desc.text.reset( _desc.node->getChildByName<Text*>( "text" ) );
			_desc.dmg.reset( _desc.node->getChildByName<Text*>( "dmg" ) );
			_desc.rng.reset( _desc.node->getChildByName<Text*>( "rng" ) );
			_desc.spd.reset( _desc.node->getChildByName<Text*>( "spd" ) );
		}

		setVisible( false );
		return true;
	}
	while( false );
	return false;
}

void MenuTower::appearance()
{
	_disabled = false;
	setEnabled( true );
	setVisible( true );
	hideConfirmButton();
	runEvent( "appearance" );
	ScoreCounter::shared().observer( kScoreLevel ).add( _ID, std::bind( &MenuTower::onChangeMoney, this, std::placeholders::_1 ) );
	onChangeMoney( ScoreCounter::shared().getMoney( kScoreLevel ) );

	_confirm->setVisible( false );
	_confirmUn->setVisible( false );

	scheduleUpdate();
	showRadius( _gameLayer->getObjectsNode(), _unit->getPosition(), _unit->getRadius() );
}

void MenuTower::disappearance()
{
	if( _disabled == false )
	{
		setEnabled( false );
		hideConfirmButton();
		runEvent( "disappearance" );
		_unit.reset( nullptr );
		ScoreCounter::shared().observer( kScoreLevel ).remove( _ID );

		_confirm->setVisible( false );
		_confirmUn->setVisible( false );

		_waitSellConfirm = false;
		_waitUpgradeConfirm = false;

		hideRadius( _gameLayer->getObjectsNode() );
		unscheduleUpdate();
		_disabled = true;
	}
}

void MenuTower::setUnit( Unit::Pointer unit )
{
	_unit = unit;
	if( _unit )
	{
		std::string cost1 = toStr( mlTowersInfo::shared().getCost( _unit->getName(), _unit->getLevel() + 1 ) );
		std::string cost2 = toStr( mlTowersInfo::shared().getSellCost( _unit->getName(), _unit->getLevel() ) );

		_upgrade->getChildByName<Text*>( "cost" )->setString( cost1 );
		_upgradeUn->getChildByName<Text*>( "cost" )->setString( cost1 );
		_sell->getChildByName<Text*>( "cost" )->setString( cost2 );
	}

	if( _unit )
	{
		setPosition( _unit->getPosition() );

		buildDescription( _unit->getLevel() );
	}
}

void MenuTower::buildDescription( unsigned _level )
{
	std::string name = _unit->getName();
	std::string localization = WORD( name + "_name" );
	unsigned level = std::min( _level, _unit->getMaxLevel() );
    int multiplier = 1;
    if (name == "tower_gun")
    {
        bool isEasterEggDoubleDamageUnlcoked = EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kDoubleDamageMageTower);
        if (isEasterEggDoubleDamageUnlcoked)
        {
            multiplier=2;
        }
    }    
	std::string dmg = toStr( mlTowersInfo::shared().get_dmg( name, level )*multiplier );
	std::string rng = toStr( mlTowersInfo::shared().get_rng( name, level ) );
	std::string spd = toStr( mlTowersInfo::shared().get_spd( name, level ) );
	std::string txt = mlTowersInfo::shared().get_desc( _unit->getName(), 1 );
	if( _desc.name )_desc.name->setString( localization );
	if( _desc.dmg )_desc.dmg->setString( dmg );
	if( _desc.rng )_desc.rng->setString( rng );
	if( _desc.spd )_desc.spd->setString( spd );
	if( _desc.text )_desc.text->setString( txt );
}

void MenuTower::activateUpgrade( Ref * sender, bool availebledButton )
{
	if( _disabled )return;
	if( Config::shared().get<bool>( "instanttowerbuild" ) )
	{
		confirmUpgrade( sender, availebledButton );
		return;
	}

	buildDescription( _unit->getLevel() + 1 );
	_confirm->setCallback( std::bind( &MenuTower::confirmUpgrade, this, std::placeholders::_1, true ) );
	_confirmUn->setCallback( std::bind( &MenuTower::confirmUpgrade, this, std::placeholders::_1, false ) );

	_confirm->setVisible( true );
	_confirm->setPosition( _upgrade->getPosition() );
	_confirmUn->setPosition( _upgrade->getPosition() );

	_waitSellConfirm = false;
	_waitUpgradeConfirm = true;
	onChangeMoney( ScoreCounter::shared().getMoney( kScoreLevel ) );

	//float rate = mlTowersInfo::shared().rate( _unit->getName() );
	float radius = mlTowersInfo::shared().radiusInPixels( _unit->getName(), _unit->getLevel() + 1 );
	showRadius( _gameLayer->getObjectsNode(), _unit->getPosition(), radius /** rate*/ );
	runEvent( "onclick" );
}

void MenuTower::confirmUpgrade( Ref * sender, bool availebledButton )
{
	if( _disabled )return;
	assert( _confirmCurrent );
	assert( _unit );
	auto& board = _gameLayer->getGameBoard();
	board.upgradeTower( _unit, false );
	_confirm->setVisible( false );
	_confirmUn->setVisible( false );

	disappearance();

	if( !availebledButton )
	{
        if (Config::shared().get<bool>( "useInapps" )) {
            TutorialManager::shared( ).dispatch( "level_haventgear_upgrade" );
        }
	}
}

void MenuTower::activateSell( Ref * sender, bool availebledButton )
{
	if( _disabled )return;
	if( Config::shared().get<bool>( "instanttowerbuild" ) )
	{
		confirmSell( sender, availebledButton );
		return;
	}

	_confirm->setCallback( std::bind( &MenuTower::confirmSell, this, std::placeholders::_1, true ) );

	_confirmUn->setVisible( false );
	_confirm->setVisible( true );
	_confirm->setPosition( _sell->getPosition() );

	_waitSellConfirm = true;
	_waitUpgradeConfirm = false;

	hideRadius( _gameLayer->getObjectsNode() );
}

void MenuTower::confirmSell( Ref * sender, bool availebledButton )
{
	if( _disabled )return;
	assert( _unit );
	_gameLayer->getGameBoard().removeTower( _unit );
	_confirm->setVisible( false );

	disappearance();
}

void MenuTower::lockClick( Ref * sender )
{
	disappearance();
}

void MenuTower::showConfirmButton()
{}

void MenuTower::hideConfirmButton()
{}

void MenuTower::onChangeMoney( int money )
{
	int cost = mlTowersInfo::shared().getCost( _unit->getName(), _unit->getLevel() + 1 );

	_upgrade->setVisible( cost <= money );
	_upgradeUn->setVisible( cost > money );

	if( !_waitSellConfirm )
	{
		auto hist = _confirmCurrent;
		if( cost <= money )
		{
			_confirmCurrent = _confirm;
			if( _waitUpgradeConfirm )
			{
				_confirm->setVisible( true );
				_confirmUn->setVisible( false );
			}
		}
		else
		{
			_confirmCurrent = _confirmUn;
			if( _waitUpgradeConfirm )
			{
				_confirm->setVisible( false );
				_confirmUn->setVisible( true );
			}
		}
	}

	checkLockedUpgrade();
}

void MenuTower::checkLockedUpgrade()
{
	unsigned level = _unit->getLevel();
	unsigned max = _unit->getMaxLevel();
	unsigned max2 = _unit->getMaxLevelForLevel();

	if( level == max )
	{
		_lock->setVisible( false );
		_upgrade->setVisible( false );
		_upgradeUn->setVisible( false );
	}
	else if( level == max2 )
	{
		_lock->setVisible( true );
		_upgrade->setVisible( false );
		_upgradeUn->setVisible( false );
	}
	else
	{
		_lock->setVisible( false );
	}

}

void MenuTower::update( float )
{
	if (!_unit)
		return;

	if (radiusTowerIsHidden())
		showRadius( _gameLayer->getObjectsNode(), _unit->getPosition(), _unit->getRadius() );

	auto pos = _unit->getPosition();
	auto point = _gameLayer->getMainLayer()->convertToWorldSpace(pos);
	setPosition(point);

	auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();

	auto fullOffset = Vec2();
	for (auto button : buttons)
		if (button)
		{
			auto rect = button->getBoundingBox();
			rect.origin += point;

			const float threshold = 10.0f;

			auto offsetY = 0.0f;
			if (rect.getMinY() < threshold)
				offsetY = threshold - rect.getMinY();
			else if (rect.getMaxY() > dessize.height - threshold)
				offsetY = dessize.height - threshold - rect.getMaxY();

			if (fabs(offsetY) > fabs(fullOffset.y))
				fullOffset.y = offsetY;
		}

	setPosition(point + fullOffset);

	if( _desc.node )
	{
		auto pos = _desc.node->getPosition( );
		pos.x = fabs( pos.x );
		_desc.node->setAnchorPoint( Point( 0, 0.5f ) );
		if( point.x > dessize.width / 2 )
		{
			pos.x = -pos.x;
			_desc.node->setAnchorPoint( Point( 1, 0.5f ) );
		}
		_desc.node->setPosition( pos );
	}
}

NS_CC_END
