#ifndef GamePopUpScene_h
#define GamePopUpScene_h
#include <stdio.h>
#include "cocos2d.h"
#include "ml/NodeExt.h"
#include "GameScene.h"
NS_CC_BEGIN;

class GameLayer;
class GamePopUpLayer : public Menu, public NodeExt
{
    DECLARE_BUILDER( GamePopUpLayer );
    bool init(const std::string& path);
public:
    virtual void onEnter();
    
protected:
    virtual ccMenuCallback get_callback_by_description( const std::string & name );
    void cb_close( Ref*sender );
    
protected:
    GameScene* _gameScene;
    MenuItemPointer _close;
    float _scaleFactor;
};

NS_CC_END;

#endif 
