//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "VictoryMenu.h"
#include "GameScene.h"
#include "support.h"
#include "ScoreCounter.h"
#include "ml/SmartScene.h"
#include "ShopLayer.h"
#include "ScoreLayer.h"
#include "configuration.h"
#include "FinalLayer.h"
#include "UserData.h"
#include "consts.h"
#include "ml/Share/ShareHandler.h"
#include "ml/CutSceneHandler.h"
#include "AdsPlugin.h"

NS_CC_BEGIN


VictoryMenu::VictoryMenu()
: _victory(false)
{
}

VictoryMenu::~VictoryMenu( )
{
}

bool VictoryMenu::init( GameScene* scene, bool victory, int scores, int stars, int levelIndex)
{
	do
	{
		_gameScene = scene;
		CC_BREAK_IF( !LayerExt::init() );

		_victory = victory;
        LevelParams::shared().setLastLevelWon(victory);

        std::vector<std::string> randomTips = this->getRandomTipsForLevel(levelIndex);
        
		auto listener = EventListenerKeyboard::create();
		listener->onKeyReleased = CC_CALLBACK_2(VictoryMenu::onKeyReleased, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
		
        std::string ini = victory? "victory.xml" : "defeat.xml";
		NodeExt::load( "ini/gamescene", ini );
        
        if(levelIndex == 3){
            UserData::shared().write( "heroroom_visited", true );
        }
        
        if(!victory) {
            Text *hint = getChildByName<Text*>("hint");
            hint->setString(randomTips.at(random(0, ((int)randomTips.size()) - 1)));
        }
        
		auto menu = getChildByPath( "menu" );
		auto close = menu?menu->getChildByName<MenuItem*>( "close" ) : nullptr;
        auto share = menu?menu->getChildByName<MenuItem*>( "share" ) : nullptr;
		auto restart = menu ? menu->getChildByName<MenuItem*>( "restart" ) : nullptr;
		auto restart_fuel = menu ? menu->getChildByName<MenuItem*>( "restart_fuel" ) : nullptr;
        auto double_reward = menu ? menu->getChildByName<MenuItem*>( "doublereward" ) : nullptr;
        
		auto cost = getNodeByPath<Text>( restart, "normal/cost" );
		if( !cost ) cost = getNodeByPath<Text>( restart_fuel, "normal/cost" );

		if( menu )
		{
			if( close ) close->setCallback( CC_CALLBACK_1( VictoryMenu::cb_close, this ) );
            if( double_reward ) double_reward->setCallback( CC_CALLBACK_1( VictoryMenu::cb_doublereward, this ) );
            if( share ) share->setCallback( CC_CALLBACK_1( VictoryMenu::cb_share, this ) );
			if( restart ) restart->setCallback( CC_CALLBACK_1( VictoryMenu::cb_restart, this ) );
			if( restart_fuel ) restart_fuel->setCallback( CC_CALLBACK_1( VictoryMenu::cb_restart, this ) );
		}
		if( cost )
		{
			auto& board = _gameScene->getGameLayer()->getGameBoard();
			int index = board.getCurrentLevelIndex();
			GameMode mode = board.getGameMode();

			int value, type; 
			LevelParams::shared().getPayForLevel( index, mode, type, value );
			cost->setString( toStr( value ) );
		}

		auto starnode = getChildByName( "star" + toStr( stars ) );
		if( starnode ) starnode->setVisible( true );

		auto label = dynamic_cast<Text*>(getChildByPath("score/value"));
		if (label)
		{
			label->setString("0");
			auto action = this->getAction("score_animation");
			if (!action)
				action = ActionText::create(2, scores);

			label->runAction(action);
		}

		auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
		auto size = getContentSize();
		float sx = std::min<float>( 1, dessize.width / size.width );
		float sy = std::min<float>( 1, dessize.height / size.height );
		float s = std::min( sx, sy );
		setScale( s );

		runEvent( "run" );
		
		auto scene = Director::getInstance()->getRunningScene();
		auto scores = scene->getChildByName("scorelayer");
		if(!scores)
		{
			auto scores = ScoreLayer::create();
			scores->setName("scorelayer");
			scene->addChild(scores, 999);
		}
        
		if( Config::shared().get<bool>( "useFuel" ) )
		{
			if( restart_fuel == nullptr )
			{
				auto restart_text = getNodeByPath( restart, "normal/restart_text" );
				auto restart_fuel_icon = getNodeByPath( restart, "normal/icon" );
				auto restart_fuel_cost = getNodeByPath( restart, "normal/cost" );
				if( restart_text )
					restart_text->setPositionY( restart_text->getPositionY() + 14.f );
				if( restart_fuel_icon )
					restart_fuel_icon->setVisible( true );
				if( restart_fuel_cost )
					restart_fuel_cost->setVisible( true );
			}
		}
		if( restart && restart_fuel )
			restart->setVisible( Config::shared().get<bool>( "useFuel" ) == false );
		if( restart_fuel ) 
			restart_fuel->setVisible( Config::shared().get<bool>( "useFuel" ) );

		return true;
	}
	while( false );
	return false;
}

std::vector<std::string> VictoryMenu::getRandomTipsForLevel(int levelIndex) {
    
    std::vector<std::string> randomTips;
    CCLOG("randome tips for level %d is here ",levelIndex);
    switch (levelIndex) {
        case 0:
            randomTips = {"Always attack near your towers so they can join in on the fight.",
                "Don't give up, this is just the first level.",
                "Save your Power Punches for large groups of enemies.",};
            break;
           
        case 1:
            randomTips = {"Plasma blasts work great on birds.",
                "Power punches works great on flying enemies",
                "Be sure to use a variety of towers during battle.",};
            break;
            
        case 2:
            randomTips = {"Be sure to use a variety of towers during battle.",
                "A great place to set Tesla towers are near exits & next to DJ towers.",
                "Save your Power Punches for large groups of enemies.",};
            break;
            
        case 3:
            randomTips = {"It's best to attack near the center of the map.",
                "Be sure to use a variety of towers during battle.",
                "Save your Power Punches for large groups of enemies.",};
            break;
            
        case 4:
            randomTips = {"It's best to attack near the center of the map.",
                "Be sure to use a variety of towers during battle.",
                "Save your Power Punches for large groups of enemies.",};
            break;
            
        case 5:
            randomTips = {"Always attack near your towers so they may deal additional damage.",
                "Take out Evil Sir Meows A Lot FIRST to prevent him from healing enemies.",
                "If needed, use your Special Items to defeat the slime boss.",
                "Don't overuse Denis. He should constantly be in and out of battle & allowed to recover.",};
            break;
            
        case 6:
            randomTips = {"Always attack near your towers so they may join in on the fight.",
                "Don't overuse Denis. He should constantly be in and out of battle & allowed to recover.",
                "Save your Power Punch for large groups of enemies.",};
            break;
            
        case 7:
            randomTips = {"Level 2 towers are recommended at this point.",
                "DJ towers don't deal damage & should never be placed in isolation.",
                "A level 4 hero & level 2 towers are recommended at this point in the game.",
                "Keep Sir Meows A Lot in s safe spot on the map in order to use Plasma blast consistently.",
                "A great place to set Tesla towers are near exits & next to DJ towers.",};
            break;
            
        case 8:
            randomTips = {"It's best to quickly set your towers AFTER calling the 1st enemy wave.",
                "A level 4 hero & level 2 towers are recommended at this point in the game.",
                "Always destroy faster moving enemies first.",
                "Sir Meows A Lot should be used as a BACKUP to catch enemies about to cross.",
                "Keep Sir Meows A Lot in s safe spot on the map in order to use Plasma blast consistently.",};
            break;
            
        case 9:
            randomTips = {"Center your landmines and set them near exits to increase the chance of detonation.",
                "A level 5 hero & level 2 towers are recommended at this point in the game.",
                "A great place to set Tesla towers are near exits & next to DJ towers.",
                "It's best to quickly set your towers AFTER calling the 1st enemy wave.",};
            break;
            
        case 10:
            randomTips = {"For levels like this, start by setting your towers in the middle.",
                "A level 5 hero & level 2 towers are recommended at this point in the game.",
                "To optimize damage dealt by towers, set them in the middle of the map.",
                "Use your Power punches & Plasa blasts wisely. Timing is the key to victory.",};
            break;
            
        case 11:
            randomTips = {"A level 6 hero & level 2 towers are recommended at this point in the game.",
                "Take out Evil Sir Meows A Lot FIRST to prevent him from healing enemies.",
                "If needed, use your Special Items to defeat the slime boss.",
                "Don't overuse Denis. He should constantly be in and out of battle & allowed to recover.",};
            break;
            
        case 12:
            randomTips = {"A level 7 or higher hero & at least one level 3 tower are recommended at this point.",
                "Try out different tower combinations and placements if you’re having a hard time beating a level.",
                "Always destroy faster moving enemies first.",};
            break;
            
        case 13:
            randomTips = {"You should have at least one level 3 tower at this point of the game.",
                "To optimize damage dealt by Drone towers, set them in the middle of the map.",
                "Always destroy faster moving enemies first.",};
            break;
            
        case 14:
            randomTips = {"You should have a couple level 3 towers at this point of the game.",
                "Sir Meows A Lot should be used as a BACKUP to catch enemies about to cross.",
                "Always destroy faster moving enemies first.",};
            break;
            
        case 15:
            randomTips = {"You should have a couple level 3 towers at this point of the game.",
                "To optimize damage dealt by Drone towers, set them in the middle of the map.",
                "Always destroy faster moving enemies first.",};
            break;
            
        case 16:
            randomTips = {"You should have a few level 3 towers at this point of the game.",
                "To optimize damage dealt by towers, set them in the middle of the map.",
                "Always destroy faster moving enemies first.",};
            break;
            
        case 17:
            randomTips = {"A level 9+ hero & level 3 towers are recommended for this level.",
                "Take out Evil Sir Meows A Lot FIRST to prevent him from healing enemies.",
                "If needed, use your Special Items to defeat the slime boss.",
                "To optimize damage dealt by towers, set them in the middle of the map first.",};
            break;
            
        case 18:
            randomTips = {"Level 3 towers, at least 1 level 4 tower, a level 10+ hero, & 3/3 Power Punch are recommended for this level.",
                "Level 3 towers, at least 1 level 4 tower, a level 10+ hero, & 3/3 Power punch are recommended for this level.",
                "Level 3 towers, at least 1 level 4 tower, a level 10+ hero, & 3/3 Power punch are recommended for this level.",};
            break;
            
        case 19:
            randomTips = {"Level 3 towers, at least 2 level 4 towers, a level 10+ hero, & 3/3 landmines + Power Punch are recommended for this level.",
                "A level 10 or higher hero & level 3 towers are recommended from here on out.",
                "To optimize damage dealt by towers, set them in the middle of the map first.",};
            break;
            
        case 20:
            randomTips = {"Level 3 towers, a few level 4 towers, a level 10+ hero, & 3/3 landmines + Power Punch are recommended for this level.",
                "Always destroy faster moving enemies first.",
                "Keep Sir Meows A Lot in s safe spot on the map in order to use Plasma blast consistently.",};
            break;
            
        case 21:
            randomTips = {"Level 3 towers, a few level 4 towers, a level 11+ hero, & 3/3 landmines + Power Punch are recommended for this level.",
                "To optimize damage dealt by towers, set them in the middle of the map first.",
                "3/3 Power Punch is recommended for this level.",};
            break;
            
        case 22:
            randomTips = {"Level 4 towers, a level 12+ hero, & 3/3 landmines + Power Punches are recommended for this level.",
                "Level 4 towers and a level 14 hero recommended.",
                "Always destroy faster moving enemies first.",
                "3/3 landmines are recommended for this level.",};
            break;
            
        case 23:
            randomTips = {"A Level 13+ hero and level 4 towers are recommended for the final battle.",
                "Always destroy faster moving enemies first.",
                "Center your landmines and set them near exits to increase the chance of detonation.",
                "If needed, use your Special Items to defeat Evil Sir Meows A Lot.",};
            break;
        
        default:
            break;
    }
    
    return randomTips;
}

void VictoryMenu::cb_restart( Ref* )
{
	auto& board = _gameScene->getGameLayer()->getGameBoard();
	int index = board.getCurrentLevelIndex();
	GameMode mode = board.getGameMode();
	int cost, type;
	LevelParams::shared().getPayForLevel( index, mode, type, cost );

	int fuel = ScoreCounter::shared( ).getMoney( kScoreFuel );
	if( cost <= fuel )
	{
		restart();
	}
	else
	{
#if PC != 1
		auto shop = ShopLayer::create(Config::shared().get<bool>( "useFreeFuel" ), false, false, false);
		if( shop )
		{
			auto scene = Director::getInstance()->getRunningScene();
			auto smartscene = dynamic_cast<SmartScene*>(scene);
			assert(smartscene);
			smartscene->pushLayer(shop, true);
		}
#endif
	}
	
}

void VictoryMenu::cb_close( Ref* )
{
    GameBoard board = _gameScene->getGameLayer()->getGameBoard();
    if ( _victory && (board.getCurrentLevelIndex() == 5 || board.getCurrentLevelIndex() == 11 ||
        board.getCurrentLevelIndex() == 17 || board.getCurrentLevelIndex() == 23))
    {
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
        int cutsceneValue = 2;
        if (board.getCurrentLevelIndex() == 11) {
            cutsceneValue = 3;
        }
        else if (board.getCurrentLevelIndex() == 17) {
            cutsceneValue = 4;
        }
        else if (board.getCurrentLevelIndex() == 23) {
            cutsceneValue = 5;
        }
        CutSceneHandler::getInstance()->showCutScene(cutsceneValue, this->getParent(), [=](){
            CCLOG("Video Ended Callback");
            close();
        });
#else
        close();
#endif
    }
    else
    {
        close();
    }
}

void VictoryMenu::cb_doublereward( Ref* ){
    CCLOG("double reward clicked");
    auto label = dynamic_cast<Text*>(this->getChildByPath("score/value"));
    if (label != nullptr)
    {
        int scorenumber = std::stoi(label->getString());
        int addCoins = 75;
        label->setString(std::to_string(scorenumber + addCoins));
        ScoreCounter::shared().addMoney( kScoreCrystals, addCoins, true );
    }

    AdsPlugin::shared().observerVideoResult.add( _ID, std::bind( &VictoryMenu::videoResult, this, std::placeholders::_1, this ) );
    AdsPlugin::shared().showVideo();
}

void VictoryMenu::videoResult(bool result, LayerExt* layer) {
    AdsPlugin::shared().observerVideoResult.remove(_ID);
    close();
}

void VictoryMenu::cb_share( Ref* )
{
    ShareHandler::getInstance()->showSharePopup();
}

void VictoryMenu::restart( )
{
	auto scene = Director::getInstance()->getRunningScene();
	auto scores = scene->getChildByName("scorelayer");
	if(scores)
	{
		scores->removeFromParent();
	}

	_gameScene->restartLevel();
}

void VictoryMenu::close()
{
	auto scene = Director::getInstance()->getRunningScene();
	auto scores = scene->getChildByName("scorelayer");
	if(scores)
	{
		scores->removeFromParent();
	}

	bool needPopScene = true;
	int index = _gameScene->getGameLayer()->getGameBoard().getCurrentLevelIndex();
	int count = UserData::shared().get( kUser_locationsCount, 0 );
	if( _victory && (index + 1) == count )
	{
		auto layer = FinalLayer::create();
		if( layer )
		{
			SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
			scene->pushLayer( layer, true );
			needPopScene = false;
		}
	}

	if( needPopScene )
		Director::getInstance()->popScene();
}

void VictoryMenu::onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event )
{
	if( keyCode == EventKeyboard::KeyCode::KEY_BACK )
		close();
}

NS_CC_END
