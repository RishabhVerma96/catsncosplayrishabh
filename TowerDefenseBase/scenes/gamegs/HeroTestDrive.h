//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 2016.02.02
//
//
#ifndef __HeroTestDrive_h__
#define __HeroTestDrive_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
NS_CC_BEGIN

class GameScene;

class HeroTestDriveLayer : public LayerExt
{
	DECLARE_BUILDER( HeroTestDriveLayer );
	bool init( GameScene* scene, const std::string& heroname, bool restartLevel, bool activateOffer = false );
public:
	static bool didLaunched( GameScene* scene );
	static void onLevelStatisticLayerDidShowed( GameScene* scene, bool victory );
	void createHeroesNow();
	void activateOffer();
	virtual bool loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )override;
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	void close( bool track );
protected:
	void applyParams();
	void tryHero();
	void purchase();
	void purchaseAnswer(bool success);
	void restartLevel();

	void statistic_try();
	void statistic_hire();
	void statistic_button_hire();
	void statistic_button_close();
	void statistic_purchasing();
private:
	GameScene* _gameScene;
	std::string _hero;
	bool _restartLevel;
	std::map<std::string, ParamCollection> _params;
};

class HeroTestDriver : public Node
{
	DECLARE_BUILDER( HeroTestDriver );
	bool init( const std::string& heroname );
public:
	void setHero();
	void restoreHero();
	void showOfferOnVictory();
	void onLevelFinished( GameScene* layer, bool result );
private:
	std::string _hero;
	std::vector<unsigned> _selectedHeroes;
	float _exp;
	bool _showOfferOnVictory;
};


NS_CC_END
#endif // #ifndef HeroTestDrive