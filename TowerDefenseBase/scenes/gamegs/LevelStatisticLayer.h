#ifndef __LevelStatisticLayer_h__
#define __LevelStatisticLayer_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "game/gameboard.h"
#include "online/OnlineConnector.h"
NS_CC_BEGIN



class GameScene;

class LevelStatisticLayer : public LayerExt, public LayerBlur
{
public:
	enum Result
	{
		Lose,
		Win,
		Neutral
	};

public:
	DECLARE_BUILDER( LevelStatisticLayer );
	bool init( GameScene* scene, Result outcome, int gold, int tickets, int costfuel, FinishLevelParams const & params );
	void onNotEnought( int scoreType );
public:
	virtual void onEnter()override;
	virtual bool isBlurActive()override;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override;
	virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override;
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;

	void destroySelf();
	void exit();
	void restart();

private:
	void multiplayerOnStatistic( bool success, OnlineConnector::PlayerInfo const & info, bool local );

private:
	GameScene* _gameScene;
	Result _outcome;
	bool _useDialog;

	LabelPointer _multiplayerLocalWinCount;
	LabelPointer _multiplayerOpponentWinCount;
};




NS_CC_END
#endif // #ifndef LevelStatisticLayer