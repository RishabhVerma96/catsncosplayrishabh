#include "ml/SmartScene.h"
#include "ml/language.h"
#include "BoxMenu.h"
#include "GameLayer.h"
#include "GameScene.h"
#include "UserData.h"
#include "scenes/itemshop/ItemShop.h"
#include "shop/ShopLayer2.h"
#include "Tutorial.h"
#include "DialogLayer.h"
#include "Generics.h"
NS_CC_BEGIN





BoxMenu::BoxMenu()
: _selectedItem( 0 )
, _descriptionItem( 0 )
, _timer(0)
, _isItemSelected( false )
, _alwaysOpen( false )
, _useDialog(false)
, _isItemCollapsed(true)
{}

BoxMenu::~BoxMenu()
{}

bool BoxMenu::init( GameLayer* layer, const std::string & xml )
{
    do
    {
        _gameLayer = layer;
        CC_BREAK_IF( !Menu::init() );
        CC_BREAK_IF( !init_machine() );
        
        load( xml );
        
        initBonusItems();
        for( int i = 0; i < 10; ++i )
        {
            auto path = "item" + toStr( i + 1 );
            auto item = getNodeByPath<mlMenuItem>( this, path );
            if( !item )
                break;
            item->onSelect.add( _ID, std::bind( &BoxMenu::startTimerDesc, this, i + 1 ) );
            item->onDeselect.add( _ID, std::bind( &BoxMenu::stopTimerDesc, this, i + 1 ) );
        }
        
        scheduleUpdate();
        displayCountItems();
        
        _alwaysOpen = strTo<bool>( getParamCollection().get( "alwaysopen", "false" ) );
        bool openOnStart = strTo<bool>( getParamCollection().get( "openonstart", "false" ) );
        if( openOnStart )
        {
            push_event( event_open );
            process();
        }
        
        
        return true;
    }
    while( false );
    return false;
}

bool BoxMenu::init_machine()
{
    add_state(state_close, FS_BIND(&BoxMenu::callback_close, this)).set_string_name("close");
    add_state( state_open, FS_BIND( &BoxMenu::callback_open, this ) ).set_string_name( "open" );
    add_state( state_wait, FS_BIND( &BoxMenu::callback_wait, this ) ).set_string_name( "wait" );
    
    add_event( event_close );
    add_event( event_open );
    add_event( event_wait );
    add_event( event_cancel );
    
    FiniteState::Machine::state( state_close ).add_transition( event_open, state_open );
    FiniteState::Machine::state( state_open ).add_transition( event_close, state_close );
    FiniteState::Machine::state( state_open ).add_transition( event_wait, state_wait );
    FiniteState::Machine::state( state_wait ).add_transition( event_close, state_close );
    FiniteState::Machine::state( state_wait ).add_transition( event_cancel, state_open );
    
    FiniteState::Machine::state( state_close ).add_onDeactivateCallBack( FS_BIND( &BoxMenu::close_deactivate, this ) );
    
    start( state_close );
    
    return true;
}

void BoxMenu::onEnter()
{
    Menu::onEnter();
    displayCountItems();
}

bool BoxMenu::isItemSelected()
{
    return _alwaysOpen ? _selectedItem > 0 : _isItemSelected;
}

bool BoxMenu::setProperty( const std::string & stringproperty, const std::string & value )
{
    if( stringproperty == "usedialog" )
        _useDialog = strTo<bool>( value ) && Config::shared().get<bool>( "useDialogs" );
    else return NodeExt::setProperty( stringproperty, value );
    return true;
}

ccMenuCallback BoxMenu::get_callback_by_description( const std::string & name )
{
    auto close = [this]( Ref* )mutable{ push_event( event_close ); process(); };
    auto open = [this]( Ref* )mutable{ push_event( event_open ); process(); };
    
    auto item = [this]( Ref*, unsigned index )mutable
    {
        int count = UserData::shared().bonusitem_count( index );
        if( count > 0 )
        {
            if( _selectedItem != index )
            {
                _isItemSelected = true;
                push_event( event_cancel );
                process();
                
                _selectedItem = index;
                push_event( event_wait );
                TutorialManager::shared().dispatch( "boxmenu_item_did_selected" );
            }
            else
            {
                push_event( event_cancel );
            }
            _gameLayer->onClickBoxMenu();
            process();
        }
        else
        {
            auto shop = ItemShop::create();
            if( shop )
            {
                SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
                scene->pushLayer( shop, true );
            }
//            assert( _useDialog );
//            std::string path = "ini/dialogs/boxmenu" + toStr( index ) + ".xml";
//            DialogLayer::showForShop( path, 2, 3 );
        }
    };
    
    auto itemshop = [this]( Ref* )mutable
    {
        if (_isItemCollapsed)
        {
            TutorialManager::shared().dispatch("level" + toStr(_gameLayer->getGameBoard().getCurrentLevelIndex()) + "uncollapseitems");
            runEvent( "unCollapseItems" );
            _isItemCollapsed = false;
            this->setSwallowTouchOfListner(false);
        }
        else
        {
            _selectedItem = 0;
            push_event( event_cancel );
            process();
            TutorialManager::shared().dispatch("level" + toStr(_gameLayer->getGameBoard().getCurrentLevelIndex()) + "collapseitems");
            runEvent( "collapseItems" );
            _isItemCollapsed = true;
            this->setSwallowTouchOfListner(true);
            _gameLayer->selectHeroAfterTowerClose();
        }
    };
    auto itemshop2 = [this]( Ref* )mutable
    {
        GameScene * scene = _gameLayer->getGameScene();
        scene->openShop2( nullptr, false );
    };
    
    
    if( name == "close" )
        return _alwaysOpen ? NodeExt::get_callback_by_description( name ) : std::bind( close, std::placeholders::_1 );
    else if( name == "open" )
        return std::bind( open, std::placeholders::_1 );
    else if( name == "itemshop" )
        return std::bind( itemshop, std::placeholders::_1 );
    else if( name.find( "item" ) == 0 )
    {
        auto value = name.substr( strlen( "item" ) );
        return std::bind( item, std::placeholders::_1, strTo<int>( value ) );
    }
    else if( name.find( "shop:" ) == 0 )
        return ShopLayer2::dispatch_query( name );
    else if( name == "openshop2" )
        return std::bind( itemshop2, std::placeholders::_1 );
    else
        return NodeExt::get_callback_by_description( name );
}

bool BoxMenu::onTouchBegan( Touch* touch, Event* event )
{
    auto location = touch->getLocation();
    touchbeganPoint = this->convertToNodeSpace( location );
    
    auto state = current_state().get_name();
    switch( state )
    {
        case state_close:
        {
            _isItemSelected = false;
            bool clicked = Menu::onTouchBegan( touch, event );
            if( clicked )
            {
                //getEventDispatcher()->dispatchCustomEvent( GameLayer::eventFocusLostName );
                _gameLayer->onClickBoxMenu();
            }
            return clicked;
        }
        case state_open:
        {
            bool touchedItem = Menu::onTouchBegan( touch, event );
            bool autoclose = strTo<bool>( getParamCollection().get( "autoclose", "false" ) );
            if( touchedItem == false && autoclose && !_alwaysOpen)
            {
                push_event( event_close );
                process();
            }
            if( touchedItem )
                _gameLayer->onClickBoxMenu();
            return touchedItem;
        }
        case state_wait:
        {
            return true;
        }
    }
    return Menu::onTouchBegan( touch, event );
}

void BoxMenu::onTouchEnded(Touch *pTouch, Event *pEvent) {
    CCLOG("box menu touch ended");
    auto location = pTouch->getLocation();
    touchEndPoint = this->convertToNodeSpace( location );
    
    float distance = touchbeganPoint.distance(touchEndPoint);
    if (distance <=5) {
        auto state = current_state().get_name();
        switch( state )
        {
            case state_wait:
            {
                bool touchedItem = Menu::onTouchBegan( pTouch, pEvent );
                if( touchedItem == false )
                {
                    if( createItem( pTouch->getLocation() ) )
                    {
                        bool autoclose = strTo<bool>( getParamCollection().get( "autoclose", "false" ) );
                        if( autoclose && !_alwaysOpen )
                        {
                            push_event( event_close );
                            process();
                        }
                        else
                        {
                            push_event( event_cancel );
                            process();
                            _gameLayer->selectHeroAfterTowerClose();
                        }
                        TutorialManager::shared().dispatch( "boxmenu_item_did_created" );
                    }
                }
            }
        }
    }
    
    Menu::onTouchEnded(pTouch, pEvent);
}

void BoxMenu::close(){
    if( _alwaysOpen )
        push_event( event_cancel );
    else
        push_event( event_close );
    process();
}

bool BoxMenu::createItem( const Point & location )
{
    assert( _selectedItem != 0 );
    if( _selectedItem > 0 )
    {
        auto point = _gameLayer->getMainLayer()->convertToNodeSpace( location );
        auto& board = _gameLayer->getGameBoard();
        auto item = board.createBonusItem( point, _bonuses[_selectedItem - 1] );
        if( item )
        {
            runEvent( "collapseItems" );
            _isItemCollapsed = true;
            UserData::shared().bonusitem_sub( _selectedItem, 1 );
            displayCountItems();
        }
        else
        {
            if( _gameLayer->getInterface() )
                _gameLayer->getInterface()->onForbiddenTouch( location );
        }
        
        
        return item != nullptr;
    }
    return false;
}

void BoxMenu::displayCountItems()
{
    auto display = [this]( int index )
    {
        auto menuitem = getChildByName<MenuItemSprite*>( "item" + toStr( index ) );
        
        
        std::string normalImagePath =   "images/gamescene/item" + toStr( index ) + ".png";
        std::string greyImagePath =   "images/gamescene/item" + toStr( index ) + "_d.png";

        
        auto label = menuitem->getChildByName<Text*>( "count" );
        int count = UserData::shared().bonusitem_count( index );
        
        
        if (count==0) {
            menuitem->setNormalImage(Sprite::create(greyImagePath));
        }
        else
        {
            menuitem->setNormalImage(Sprite::create(normalImagePath));
        }
        
        if( _useDialog )
        {
            std::string string = count > 0 ? toStr( count ) : "+";
            label->setString( string );
            
        }
        else
        {
//            menuitem->setEnabled( count > 0 );
            label->setString( toStr( count ) );
        }
    };
    for( size_t i = 0; i < _bonuses.size(); ++i )
        display( (int)i + 1 );
}

void BoxMenu::callback_open()
{
    runEvent( "open2" );
    _selectedItem = 0;
}

void BoxMenu::callback_close()
{
    runEvent( "close" );
    TutorialManager::shared().dispatch( "boxmenu_did_close" );
}

void BoxMenu::callback_wait()
{
    assert( _selectedItem != 0 );
    runEvent( "item" + toStr( _selectedItem ) );
}

void BoxMenu::close_deactivate()
{
    runEvent( "open" );
    TutorialManager::shared().dispatch( "boxmenu_did_open" );
}

void BoxMenu::update( float dt )
{
    if( _descriptionItem > 0 )
    {
        _timer += dt;
        if( _timer > 0.5f )
        {
            runEvent( "desc_show" );
            _descriptionItem = 0;
        }
    }
}

void BoxMenu::startTimerDesc( int index )
{
    _timer = 0.f;
    _descriptionItem = index;
    
    Node* desc( nullptr );
    if( (desc = getNodeByPath( this, "desc" )) != nullptr )
    {
        std::string description = _bonuses[index-1] + "_desc";
        std::string name = _bonuses[index - 1] + "_name";
        getNodeByPath<Text>( desc, "desc" )->setString( WORD( description ) );
        getNodeByPath<Text>( desc, "name" )->setString( WORD( name ) );
    }
}

void BoxMenu::stopTimerDesc( int index )
{
    _descriptionItem = 0;
    runEvent( "desc_hide" );
}

void BoxMenu::initBonusItems()
{
    pugi::xml_document doc;
    doc.load_file( "ini/bonusitems.xml" );
    auto root = doc.root().first_child();
    _bonuses.resize( std::distance( root.begin(), root.end() ) );
    for ( auto xmlNode : root )
    {
        std::string name = xmlNode.name();
        auto index = xmlNode.attribute( "default" ).as_int();
        _bonuses[index - 1] = name;
    }
}

NS_CC_END

