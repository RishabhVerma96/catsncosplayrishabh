//
//  IslandDefense
//
//  Created by Vladimir Tolmachev‚ on 27.09.14.
//
//
#include "ml/Animation.h"
#include "ml/ScrollMenu.h"
#include "ml/Audio/AudioEngine.h"
#include "ml/ImageManager.h"
#include "ml/Language.h"

#include "playservices/playservices.h"
#include "GameLayer.h"
#include "GameScene.h"
#include "support.h"
#include "tower.h"
#include "ScoreCounter.h"
#include "consts.h"
#include "MenuItemTextBG.h"
#include "GamePauseScene.h"
#include "Log.h"
#include "UserData.h"
#include "Achievements.h"
#include "admob/AdMob.h"
#include "MenuCreateTower.h"
#include "ShootsEffects.h"
#include "VictoryMenu.h"
#include "ShopLayer.h"
#include "ShopLayer2.h"
#include "plugins/AdsPlugin.h"
#include "game/Airbomb.h"
#include "configuration.h"
#include "Tutorial.h"
#include "UnitInfo.h"
#include "RateMeLayer.h"
#include "UserGifts.h"
#include "LevelStatisticLayer.h"
#include "game/unit/Bullet.h"
#include "online/GameBoardOnline.h"
#include "ml/CutSceneHandler.h"
#include "ml/Share/ShareHandler.h"

//#include "Animations.h"

NS_CC_BEGIN;


const std::string kExt_png( ".png" );
const std::string kSuffixUn( "_un" );

const std::string GameLayer::eventFocusLostName = "GameSceneFocusLost";

GameLayer::GameLayer()
: _gameScene(nullptr)
, _board( nullptr )
, _bg( nullptr )
, _objects( nullptr )
, _interface( nullptr )
, _selectedPlace( nullptr )
, _scoresForStartWave( 0 )
, _boughtScoresForSession( 0 )
, _runFlyCamera( true )
, _fastModeEnabled( false )
, _firstWaveStarted( false )
, _lootDropManager(nullptr)
{}

GameLayer::~GameLayer()
{
	ShopLayer::observerOnPurchase().remove( _ID );

	_lootDropManager = nullptr;
	_board->clear();
	ShootsEffectsClear();

	Director::getInstance()->getScheduler()->setTimeScale( 1 );
	ScoreCounter::shared().observer( kScoreLevel ).remove( _ID );

	OnlineConnector::shared().onGameFinish.remove( _ID );

	getEventDispatcher()->removeCustomEventListeners( eventFocusLostName );
}

GameBoard& GameLayer::getGameBoard()
{
	return *_board;
}

LootDropManager::Pointer GameLayer::getLootDropManager()
{
	return _lootDropManager;
}

bool GameLayer::init( GameScene* scene, GameBoard* board )
{
	if( !Layer::init() )
	{
		return false;
	}
	_gameScene = scene;
	_board.reset( board );
	Size desSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	Size winSize = Director::getInstance()->getWinSize();
	Size sizeMap = Config::shared().get<Size>( "levelMapSize" );

	_mainlayer = Scroller::create();
	_mainlayer->setName( "mainlayer" );
	_mainlayer->setVisibled( desSize );
	_mainlayer->enableScroller( Config::shared().get<bool>( "scrollerEnabled" ) );
#if PC==1
	_mainlayer->setMouseHoverEnabled( true );
#endif
	addChild( _mainlayer );
	float sx = winSize.width / sizeMap.width;
	float sy = winSize.height / sizeMap.height;
	float scale = std::max( sx, sy );
	_mainlayer->setScrollScale( scale );

	_objects = Node::create();
	_objects->setName( "objects" );
	_mainlayer->addChild( _objects, 1 );

	assert( _bg == nullptr );

	_mainlayer->setContentSize( sizeMap );
	_mainlayer->setAnchorPoint( Point::ZERO );

	setName( "gamelayer" );

	auto on_purchase = [this]( int type, int value )
	{
		if( type == kScoreLevel )
			this->buyLevelsMoney( value );
	};
	ShopLayer::observerOnPurchase().add( _ID, std::bind( on_purchase, std::placeholders::_1, std::placeholders::_2 ) );
	Achievements::shared().setCallbackOnAchievementObtained( std::bind( &GameLayer::achievementsObtained, this, std::placeholders::_1 ) );
	UserData::shared().write( k::user::LastGameResult, k::user::GameResultValueNone );

	if( FileUtils::getInstance()->isFileExist( "ini/gamescene/lootdrop/awards.xml" ) )
	{
		createLootDropManager();
	}

	load( "ini/gamescene", "gamelayer.xml" );
	runEvent( "oncreate" );
	loadUserGifts();

	return true;
}

void GameLayer::createLootDropManager()
{
	_lootDropManager = LootDropManager::create( _board );
}

void GameLayer::clear()
{
	Achievements::shared().setCallbackOnAchievementObtained( nullptr );
	ShootsEffectsClear();

	if( _bg )
		_bg->removeFromParent();
	_bg = nullptr;
	_objects->removeAllChildren();
	_objects = nullptr;
	_lootDropManager = nullptr;
	for( auto i : _towerPlaces ) removeChild( i ); _towerPlaces.clear();

	_fragments.clear();

	_menuTower->setUnit( nullptr );
	_menuTower->disappearance();

	unschedule( schedule_selector( GameLayer::update ) );

	removeAllChildrenWithCleanup( true );
}

void GameLayer::startGame()
{
	AudioEngine::shared().playEffect( kSoundGameStart );
	if( _interface )
		_interface->setEnabled( true );

	schedule( schedule_selector( GameLayer::update ) );
}

void GameLayer::loadLevel( int index, const pugi::xml_node & root )
{
	auto desSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();

	std::string mapFile = root.attribute( "bg" ).as_string();
	if( mapFile.empty() )
		mapFile = "images/maps/map" + toStr( index + 1 ) + ".jpg";
	_bg = ImageManager::sprite( mapFile );
	_bg->setAnchorPoint( Point( 0, 0 ) );
	_mainlayer->addChild( _bg, -1 );
	_bg->setLocalZOrder( -99999 );

	auto winSize = Director::getInstance()->getWinSize();
	auto sizeMap = _bg->getContentSize();
	float sx = winSize.width / sizeMap.width;
	float sy = winSize.height / sizeMap.height;
	float scale = std::max( sx, sy );
	_mainlayer->setScale( scale );
	_mainlayer->setContent( _bg->getContentSize() );
	_mainlayer->update( 0 );
	if( Config::shared().get<bool>( "scrollerEnabled" ) )
		_mainlayer->setScroll( 0.1f, Point::ZERO );

	GameMode mode = _board->getGameMode();
	auto decorations = root.child( "decorations" );
	auto xmlparams = root.child( mode == GameMode::normal ? k::xmlTag::LevelParams : k::xmlTag::LevelParamsHard );
	if( !xmlparams )
		xmlparams = root;

	for( auto child = decorations.first_child(); child; child = child.next_sibling() )
	{
		Decoration::Pointer object( nullptr );
		GameLayer::createDecorFromXmlNode( child, object );
		if( object )
		{
			int z = object->getLocalZOrder();
			addObject( object, object->getLocalZOrder() );
			if( z != 0 )
			{
				object->setLocalZOrder( z );
			}
		}
	}

	if( _interface && !_interface->getIsOnlyScroll() )
	{
		auto waveCooldown = xmlparams.attribute( "wave_cooldown" ).as_float( 20 );
		_interface->setWaveCooldown( waveCooldown );
	}

	pugi::xml_document doc;
	doc.load_file( "ini/gameparams.xml" );
	for( const auto& xml : doc.root().first_child() )
	{
		std::string name = xml.attribute( "name" ).as_string();
		auto value = xml.attribute( "value" );
		if( name == "max_score_for_start_wave" )
			_scoresForStartWave = value.as_int();
	}

	_board->startGame();

	auto devMenuLevelNumber = _interface->getChildByName<Label *>( "devMenuLevelNumber" );
	if( devMenuLevelNumber )
		devMenuLevelNumber->setString( StringUtils::format( "Level #%d", (int)(_board->getCurrentLevelIndex() + 1) ) );

}

void GameLayer::excludeTower( const std::string & towername )
{
	_menuCreateTower->addExludedTower( towername );
}

void GameLayer::onEnter()
{
	Layer::onEnter();
	_mainlayer->setVisibledSizeScale( getScale() );

	//Director::getInstance()->getTextureCache()->removeUnusedTextures();
	AdMob::hide();
    std::string music = _board->isGameStarted() ? _board->getBattleSound() == "" ? kMusicGameBattle : _board->getBattleSound()
                                                : _board->getPeaceSound() == "" ? kMusicGamePeace : _board->getPeaceSound();
    
    if (_board->isGameStarted() && _soundOnWave != "")
    {
        music = _soundOnWave;
    }
	AudioEngine::shared().playMusic( music );
	AudioEngine::shared().resumeAllEffects();

	for( auto i : _objects->getChildren() )
	{
		auto unit = dynamic_cast<Unit*> (i);
		if( unit )
		{
			auto fire = unit->getChildByName( "fire" );
			if( fire ) fire->setVisible( true );
		}
	}

	if( _fastModeEnabled )
		Director::getInstance()->getScheduler()->setTimeScale( 2.f );
}

void GameLayer::onExit()
{
	Layer::onExit();
	setKeyboardEnabled( false );
	AdMob::show();
	if( _objects )
	{
		for( auto i : _objects->getChildren() )
		{
			auto unit = dynamic_cast<Unit*> (i);
			if( unit )
			{
				auto fire = unit->getChildByName( "fire" );
				if( fire ) fire->setVisible( false );
			}
		}
	}
	Director::getInstance()->getScheduler()->setTimeScale( 1.f );
};

bool GameLayer::setProperty( const std::string & stringproperty, const std::string & value )
{
	if( stringproperty == "shake" )
		shake( strTo<float>( value ) );
	else
		return NodeExt::setProperty( stringproperty, value );
	return true;
}

GameInterface* GameLayer::getInterface()
{
	return _interface;
}

void GameLayer::setInterface( GameInterface* node )
{
	_interface = node;
	if( _interface && !_interface->getIsOnlyScroll() )
	{
		createMenuCreateTower();
		createMenuTower();
		createMenuDig();
	}
}

TowerPlace::Pointer GameLayer::addTowerPlace( const TowerPlaseDef  & def )
{
	if( getTowerPlaceInLocation( def.position ) )
		return nullptr;
	TowerPlace::Pointer place = TowerPlace::create( def );
	place->setName( "towerplace" + toStr<int>( _towerPlaces.size() ) );
	place->setIndex( _towerPlaces.size() );
	_towerPlaces.push_back( place );
	addObject( place, zorder::earth + 1 );
	return place;
}

TowerPlace::Pointer GameLayer::getTowerPlaceInLocation( const Point & location )
{
	auto index = getTowerPlaceIndex( location );
	if( index != -1 )
	{
		return _towerPlaces[index];
	}
	return nullptr;
}

TowerPlace::Pointer GameLayer::getTowerPlace( size_t index )
{
	return _towerPlaces[index];
}

TowerPlace::Pointer GameLayer::getTowerPlace( const std::string& name )
{
	for( auto place : _towerPlaces )
		if( place->getName() == name ) return place;
	return nullptr;
}

unsigned GameLayer::getTowerPlaceIndex( const Point & location )
{
	unsigned result( -1 ), index( 0 );
	float mind( 999999 );
	float distance( 0 );
	for( auto p : _towerPlaces )
	{
		bool c = p->checkClick( location, distance );
		if( c && distance < mind )
		{
			result = index;
			mind = distance;
		}
		++index;
	}
	return result;
}

void GameLayer::eraseTowerPlace( TowerPlace::Pointer place )
{
	auto iplace = std::find( _towerPlaces.begin(), _towerPlaces.end(), place );
	if( iplace != _towerPlaces.end() )
	{
		removeObject( place );
		_towerPlaces.erase( iplace );
		_selectedPlace = nullptr;
	}

	if( _interface  && !_interface->getIsOnlyScroll() )
		markTowerPlaceOnLocation( Point( -9999, -9999 ) );
}

void GameLayer::setSelectedTowerPlaces( TowerPlace::Pointer place )
{
	_selectedPlace = place;
}

TowerPlace::Pointer GameLayer::getSelectedTowerPlaces()
{
	return _selectedPlace;
}

const std::vector<TowerPlace::Pointer>& GameLayer::getTowerPlaces()const
{
	return _towerPlaces;
}

void GameLayer::resetSelectedPlace()
{
	_selectedPlace.reset( nullptr );
}


void GameLayer::selectDesant( UnitDesant::Pointer desant )
{
    if( Config::shared().get<bool>( "blockHeroesBeforeFirstWave" ) && _board->isGameStarted() == false )
        return;
    
    if(desant)
        desant->runEvent("onselect");
    if(_selectedDesant && _selectedDesant!=desant)
        _selectedDesant->runEvent("ondeselect" );
    
    _selectedDesant = desant;
    if( _interface )
        _interface->onSelectDesant( _selectedDesant );

}

UnitDesant* GameLayer::getSelectedDesant()
{
    return _selectedDesant;
}

UnitDesant* GameLayer::getAvailableDesant()
{
    return getGameBoard().getDesant();
}
UnitDesant* GameLayer::getAvailableDesant2()
{
    return getGameBoard().getDesant2();
}



void GameLayer::runDesant2Heal()
{
    UnitDesant* desant2 = getAvailableDesant2();
    if (desant2 && desant2->getCurrentHealth() > 0)
    {
        desant2->runEvent("on_heal" );
        
        int healAmount  = desant2->getDesantHeal();
        int healRadius  = desant2->getDesantHealRadius();
        desant2->updateHealth(healAmount);
        
        UnitDesant* desant1 = getAvailableDesant();
        if(desant1 && desant1->getCurrentHealth() > 0)
        {
            if (desant1->getPosition().distance(desant2->getPosition()) <= healRadius)
            {
                desant1->updateHealth(healAmount);
            }
        }
        
        auto heros = getGameBoard().getHeroes();
        for(auto hero : heros)
        {
            if(hero && hero->getCurrentHealth() > 0)
            {
                if (hero->getPosition().distance(desant2->getPosition()) <= healRadius)
                {
                    hero->updateHealth(healAmount);
                }
            }
        }
    }
}


void GameLayer::selectHero( Hero::Pointer hero )
{
	if( Config::shared().get<bool>( "blockHeroesBeforeFirstWave" ) && _board->isGameStarted() == false )
		return;

   // if(hero != nullptr) {
        _selectedHero = hero;
   // }
	
	if( _interface )
		_interface->onSelectHero( _selectedHero );
}

Hero* GameLayer::getSelectedHero()
{
	return _selectedHero;
}

std::vector<Decoration*> GameLayer::getDecorations( const std::string & name )
{
	std::vector<Decoration*> result;
	for( auto object : _objects->getChildren() )
	{
		if( object->getName() == name )
		{
			auto decor = dynamic_cast<Decoration*>(object);
			if( decor )
				result.push_back( decor );
		}
	}
	return result;
}

void GameLayer::createDecorFromXmlNode( const pugi::xml_node & xmlnode, Decoration::Pointer & outNode )
{
	std::string name = xmlnode.name();
	std::string actiondesc = xmlnode.attribute( "action" ).as_string();
	float x = xmlnode.attribute( "x" ).as_float();
	float y = xmlnode.attribute( "y" ).as_float();
	float z = xmlnode.attribute( "z" ).as_float();

	std::string pathToXml = "ini/maps/animations/" + name + ".xml";
	pugi::xml_document doc;
	doc.load_file( pathToXml.c_str() );
	auto root = doc.root().first_child();

	auto decoration = Decoration::create();
	if( decoration )
	{
		xmlLoader::load( decoration, root );
		decoration->setName( xmlnode.name() );
		decoration->setPosition( x, y );
		decoration->setStartPosition( Point( x, y ) );
		decoration->setLocalZOrder( z == 0 ? -y : z );
		decoration->setActionDescription( actiondesc );

		if( actiondesc.size() > 0 )
		{
			auto action = xmlLoader::load_action( actiondesc );
			decoration->setAction( action );
		}
	}
	outNode.reset( decoration );
}

void GameLayer::createMenuTower()
{
	_menuTower = MenuTower::create( this );
	_menuTower->disappearance();
	_interface->addChild( _menuTower, 999999 );
}

void GameLayer::createMenuCreateTower()
{
	_menuCreateTower = MenuCreateTower::create( this );
	_menuCreateTower->disappearance();
	_interface->addChild( _menuCreateTower, 999999 );
}

void GameLayer::createMenuDig()
{
	_menuDig = MenuDig::create( this );
	_menuDig->disappearance();
	_interface->addChild( _menuDig, 999999 );
}

void GameLayer::closeMenuTower()
{
	if( _menuTower )_menuTower->disappearance();
}

void GameLayer::closeMenuCreateTower()
{
	if( _menuCreateTower )_menuCreateTower->disappearance();
}

void GameLayer::closeMenuDig()
{
	if( _menuDig )_menuDig->disappearance();
}

const Point GameLayer::convertToGameSpase( const Point& point )const
{
	return _mainlayer->convertToNodeSpace( point );
}

Node* GameLayer::createTree( int index )
{
	////std::string texture = "images/objects/tree" + toStr(index) +".png";
	//
	//Node * tree( nullptr );
	//tree = Node::create();
	//tree->setPosition( 200, 200 );
	//
	//int count = rand() % 2 + 3;
	//for( int i = 0; i < count; ++i )
	//{
	//	Sprite * s = ImageManager::sprite( kPlistCreep + "tree" + toStr( index ) + ".png" );
	//	float x = (rand() % 41 - 20);
	//	float y = (rand() % 41 - 20);
	//	s->setPosition( Point( x, y ) );
	//	tree->addChild( s );
	//
	//	float amplitude = 7;
	//	float duration = CCRANDOM_MINUS1_1() * (amplitude / 2) + amplitude;
	//	x = rand() % int( amplitude ) - amplitude / 2;
	//	y = rand() % int( amplitude ) - amplitude / 2;
	//	FiniteTimeAction * m0 = MoveBy::create( duration, Point( x, y ) );
	//	FiniteTimeAction * m1 = MoveBy::create( duration, Point( -x, -y ) );
	//	s->runAction( RepeatForever::create( Sequence::create( m0, m1, nullptr ) ) );
	//}
	//
	//return tree;
	assert( 0 );
	return nullptr;

}

void GameLayer::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
}

bool GameLayer::isBlurActive()
{
	return !isRunning();
}

void GameLayer::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

void GameLayer::onClickByObject( Unit::Pointer unit )
{
	if( unit->getType() == UnitType::tower )
	{
		bool showMenu = strTo<bool>( unit->getParamCollection().get( "showmenu", "yes" ) );

		if( showMenu && unit != _selectedUnit && !_interface->getBoxMenu()->isItemSelected() )
		{
			_menuTower->setUnit( unit );

			Point point = unit->getPosition();
			_menuTower->setPosition( point );
			_menuTower->appearance();
			_selectedUnit.reset( unit );
		}
		else
		{
			if( _selectedUnit )
			{
				_selectedUnit->runEvent( "ondeselect" );
			}
			_menuTower->disappearance();
			_selectedUnit.reset( nullptr );
		}

		_interface->onClickByTower();
	}
	else if( unit->getType() == UnitType::hero )
	{
		if( unit->current_state().get_name() != Unit::State::state_death )
		{
			selectHero( dynamic_cast<Hero*>(unit.ptr()) );
			_interface->onClickByHero();
		}
	}
    else if( unit->getType() == UnitType::desant )
    {
        selectDesant( dynamic_cast<UnitDesant*>(unit.ptr()) );
        _interface->onClickByDesant();
    }

}

void GameLayer::onClickByTowerPlace( TowerPlace::Pointer place )
{
	_interface->onClickByTowerPlace();
	AudioEngine::shared().playEffect( kSoundGameTowerPlaceSelect );
	std::string event = "level" + toStr( _board->getCurrentLevelIndex() ) + "_selectplace";
	TutorialManager::shared().dispatch( event );
}

void GameLayer::markTowerPlaceOnLocation( const Point& position )
{
	auto hist = _selectedPlace;
	_selectedPlace = nullptr;

	auto index = getTowerPlaceIndex( position );
	if( index != -1 )
	{
		_selectedPlace = _towerPlaces[index];
	}

	if( (!_selectedPlace && hist != _selectedPlace) || (hist == _selectedPlace) )
	{
        //#parth
		_menuCreateTower->disappearance();
		_selectedPlace = nullptr;
	}

	if( hist )
		hist->unselected();
	if( _selectedPlace )
		_selectedPlace->selected();

	if( _selectedPlace && !_interface->getBoxMenu()->isItemSelected() )
	{
		_menuTower->disappearance();
		_menuDig->disappearance();
		if( _selectedPlace )
		{
			if( _selectedPlace->getActive() )
			{
				_menuCreateTower->appearance();
				_menuCreateTower->setClickPoint( _selectedPlace->getPosition() );
			}
			else
			{
				_menuCreateTower->disappearance();
				_menuDig->appearance();
				_menuDig->setClickPoint( _selectedPlace->getPosition() );
				//_selectedPlace = nullptr;
			}
		}
	}
}

void GameLayer::onCreateUnit( Unit*unit )
{
	UnitType type = unit->getType();
	switch( type )
	{
		case UnitType::tower:
		{
			std::string event = "level" + toStr( _board->getCurrentLevelIndex() ) + "_buildtower";
			TutorialManager::shared().dispatch( event );
			break;
		}
		case UnitType::creep:
		{
			bool isExist = FileUtils::getInstance()->isFileExist( "ini/tutorial/units/" + unit->getName() + ".xml" );
			if( isExist )
			{
				std::string key = "showunitinfo_" + unit->getName();
				bool showed = UserData::shared().get <bool>( key );
				if( !showed && getGameBoard().getGameMode() != GameMode::multiplayer )
				{
					auto info = UnitInfo::create( unit->getName() );
					if( info )
					{
						UserData::shared().write( key, true );
						if( _interface  && !_interface->getIsOnlyScroll() )
							_interface->addChild( info );
					}
				}
			}
			break;
		}
		case UnitType::hero:
		{
			break;
		}

		default:
			break;
	}
}

void GameLayer::selectHeroAfterTowerClose() {
    _interface->selectHeroAutomatically();
}

void GameLayer::onDeathUnit( Unit*unit )
{
	UnitType type = unit->getType();
	switch( type )
	{
		case UnitType::tower:
			break;
		case UnitType::creep:
			if( _board->getGameMode() == GameMode::survival )
			{
				auto score = _board->getScoreForUnit( unit );
				xmlLoader::macros::set( "score", toStr( score ) );
				auto node = xmlLoader::load_node<NodeExt_>( "ini/gamescene/scorefly.xml" );
				if( node )
				{
					addObject( node, 0 );
					node->setLocalZOrder( unit->getLocalZOrder() + 100 );
					node->setPosition( unit->getPosition() );
					node->runEvent( "run" );
				}
			}
			if( _lootDropManager )
				_lootDropManager->dropLoot( unit );
			break;
		case UnitType::hero:
		{
			auto hero = dynamic_cast<Hero*>(unit);
			if( hero )
			{
				if( _selectedHero.ptr() == hero )
				{
					selectHero( nullptr );
                    _interface->setTouchNormal();
				}
				if( _interface && !_interface->getIsOnlyScroll() )
					_interface->onHeroDead( hero );
			}
			break;
		}
        case UnitType::desant:
        {
            auto desant = dynamic_cast<UnitDesant*>(unit);
            if( desant )
            {
                if( _selectedDesant.ptr() == desant )
                {
                    selectDesant(nullptr);
                }
                if( _interface && !_interface->getIsOnlyScroll() )
                    _interface->onDesantDead( desant );
            }
            break;
        }
		default:
			break;
	}
}

void GameLayer::onDeathCanceled( Unit*unit )
{
	UnitType type = unit->getType();
	switch( type )
	{
		case UnitType::tower:
			break;
		case UnitType::creep:
			break;
		case UnitType::hero:
		{
			auto hero = dynamic_cast<Hero*>(unit);
            if( hero ) {
                if( _interface && !_interface->getIsOnlyScroll() ) {
                    _interface->onHeroResurrected( hero );
                }
                selectHero(hero);
                _interface->setTouchHero();
            }
			break;
		}
        case UnitType::desant:
        {
            auto desant = dynamic_cast<UnitDesant*>(unit);
            if( desant )
                if( _interface && !_interface->getIsOnlyScroll() )
                    _interface->onDesantResurrected( desant );
            break;
        }

        default:
			break;
	}
}

void GameLayer::onFirstWave()
{
//    LayerPointer layer;
//    auto scene = dynamic_cast<GameScene*>(getScene());
//    int levelIndex = getGameBoard().getCurrentLevelIndex();
//    layer = VictoryMenu::create( scene, false, 50, 1, levelIndex );
//    
//    getSmartScene()->pushLayer( layer, true );
    
	//#parth change
	auto startPoint = _board->getHeroes().front()->getPosition(); ///_board->getCreepsRoutes().front().main.front();
	flyCameraAboveMap( startPoint );
	int index = _board->getCurrentLevelIndex();
	if( !(index == 1 && !Config::shared().get<bool>( "useInapps" )) )
	{
		std::string event = "level" + toStr( index ) + "_enter";
		TutorialManager::shared().dispatch( event );

		if( _board->getHeroes().size() > 0 )
		{
			std::string heroEvent = "level" + toStr( index ) + "_hero";
			TutorialManager::shared().dispatch( heroEvent );
		}
	}
}

void GameLayer::onStartWave( const WaveInfo & wave )
{
    _soundOnWave = wave.soundOnWave;
	std::string event = "level" + toStr( _board->getCurrentLevelIndex() ) + "_startwave" + toStr( wave.index );
	TutorialManager::shared().dispatch( event );

	if( _userGiftsLevels.empty() && wave.index == 0 )
	{
		auto userGift = UserGifts::create();
		if( userGift )
			_gameScene->pushLayer( userGift, true );
	}
	else
	{
		auto giftCounter = UserData::shared().get <int>( "gift_counter", 0 );
		if( (int)_userGiftsLevels.size() > giftCounter )
		{
			auto giftLevel = _userGiftsLevels[giftCounter].first;
			auto giftWave = _userGiftsLevels[giftCounter].second;
			if( _board->getCurrentLevelIndex() == giftLevel && wave.index == giftWave )
			{
				auto userGift = UserGifts::create();
				if( userGift )
					_gameScene->pushLayer( userGift, true );
			}
		}
	}
	//move code here

	if( getGameBoard().getGameMode() == GameMode::survival )
	{
		xmlLoader::macros::Temporal macros = { { "wave_number", toStr( wave.index ) } };
		auto alert = xmlLoader::load_node( "ini/new_wave_alert.xml" );

		if( alert )
			addChild( alert.ptr() );
	}
}

void GameLayer::onWaveFinished()
{
	if( _interface && !_interface->getIsOnlyScroll() )
		_interface->showWaveIcons();
}

void GameLayer::_onFinishGame( FinishLevelParams* params , EasterEggsParams* easterParams )
{
    _board->stopAllUnitsSounds();
    auto reaction = [this] ( FinishLevelParams * params ) {
        runAction( Sequence::createWithTwoActions(
                                                  DelayTime::create( 1.0f ),
                                                  CallFunc::create( std::bind( &GameLayer::openStatisticWindow, this, params ) )
                                                  ) );
        
        bool success = params->livecurrent > 0;
        if( _interface && !_interface->getIsOnlyScroll() )
        {
            _menuCreateTower->disappearance();
            _menuTower->disappearance();
            _menuDig->disappearance();
            _interface->setTouchDisabled();
            _interface->setEnabled( false );
        }
        AudioEngine::shared().playEffect( success ? kSoundGameFinishSuccess : kSoundGameFinishFailed );
        
        UserData::shared().write(
                                 k::user::LastGameResult,
                                 success ? k::user::GameResultValueWin : k::user::GameResultValueFail );
        
        if( success )
        {
            int wincounter = UserData::shared().get <int>( k::user::GameWinCounter );
            ++wincounter;
            UserData::shared().write( k::user::GameWinCounter, wincounter );
        }
    };
    
    auto easterEgg = [this, reaction] ( FinishLevelParams * params, const EasterEggType easterType ) {
        runAction( Sequence::createWithTwoActions(
                                                  DelayTime::create( 1.0f ),
                                                  CallFunc::create( std::bind( &GameLayer::openEasterEggUnlockWindow, this, easterType, [this, reaction, params]()->void
                                                                              {
                                                                                  reaction(params);
                                                                              }) )
                                                  ) );
    };
    
    if( _board->getGameMode() == GameMode::multiplayer && OnlineConnector::shared().isConnected() )
    {
        OnlineConnector & oc = OnlineConnector::shared();
        oc.onGameFinish.add( _ID, [this, reaction, params]( bool, bool win ){
            if( win && params->livecurrent <= 0 )
            params->livecurrent = 1;
            
            if( !win && params->livecurrent > 0 )
            params->livecurrent = 0;
            
            reaction( params );
        } );
        oc.winReport( params->livecurrent > 0, getGameScene()->getSessionWithBot() );
    }
    else
    {
        const EasterEggType easterType = isAnyEasterEggCompleted(params, easterParams);
        if(easterType != EasterEggType::kNone)
        {
            easterEgg( params , easterType);
        }
        else
        {
            reaction( params );
        }
    }
}

void GameLayer::onFinishGame( FinishLevelParams* params , EasterEggsParams* easterParams )
{
    float delay = 1.0f;
    if(_board->getCurrentLevelIndex() == 23)
    {
        delay = 2.789f;
    }
    runAction( Sequence::createWithTwoActions(
                                              DelayTime::create( delay ),
                                              CallFunc::create( std::bind( &GameLayer::_onFinishGame, this, params, easterParams ) )
                                              ) );
}

const EasterEggType GameLayer::isAnyEasterEggCompleted(FinishLevelParams* params, EasterEggsParams* easterParams)
{
    
    bool success = params->livecurrent > 0;

    int currentLevel = _board->getCurrentLevelIndex();
    EasterEggType returnType = EasterEggType::kNone;
    
    if (success){
//        if(currentLevel == 8 && !EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kDroneTower))
//        {
//            if(!easterParams->isDroneTowerUsed)
//            {
//                EasterEggsHandler::getInstance()->unlockEasterEgg(EasterEggType::kDroneTower);
//                returnType = EasterEggType::kDroneTower;
//            }
//        }
//        else if(currentLevel == 10 && !EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kIceTower))
//        {
//            if(!easterParams->isIceTowerUsed)
//            {
//                EasterEggsHandler::getInstance()->unlockEasterEgg(EasterEggType::kIceTower);
//                ScoreCounter::shared().addMoney( kScoreCrystals, 1000, true );
//                returnType = EasterEggType::kIceTower;
//            }
//        }
//
//        else if(_board->getCurrentLevelIndex() == 6 && !EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kHealingAbility))
//        {
//            if(!easterParams->isDesantUsed)
//            {
//                EasterEggsHandler::getInstance()->unlockEasterEgg(EasterEggType::kHealingAbility);
//                returnType = EasterEggType::kHealingAbility;
//            }
//        }
//        else if(_board->getCurrentLevelIndex() == 12 && !EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kHero1DoubleSpeed))
//        {
//            if(!easterParams->isHeroUsed && ! easterParams->isMineUsed)
//            {
//                EasterEggsHandler::getInstance()->unlockEasterEgg(EasterEggType::kHero1DoubleSpeed);
//                returnType = EasterEggType::kHero1DoubleSpeed;
//            }
//        }
//        else
            if(currentLevel == 4 &&
                !EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kDuplicateDesant) &&
                _board->_statisticsParams.livecurrent > 0)
        {
            if(easterParams->isEasterEnemyPassed)
            {
                EasterEggsHandler::getInstance()->unlockEasterEgg(EasterEggType::kDuplicateDesant);
                returnType = EasterEggType::kDuplicateDesant;
            }
        }
//        else if(currentLevel == 14 && !EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kDoubleDamageMageTower))
//        {
//            if(!easterParams->isMageTowerUsed)
//            {
//                EasterEggsHandler::getInstance()->unlockEasterEgg(EasterEggType::kDoubleDamageMageTower);
//                returnType = EasterEggType::kDoubleDamageMageTower;
//            }
//        }

        if (!EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kHero9Unlocked))
        {
            int totalLevels = UserData::shared().get( kUser_locationsCount, 0 );
            int passedLevels = UserData::shared().get( kUser_passedLevels, 0 );
            bool allLevels3Stars = true;
            if (passedLevels == totalLevels)
            {
                for (int i=0; i<passedLevels; i++) {
                    int stars = UserData::shared().level_getScoresByIndex( i );
                    if (stars < 3)
                    {
                        allLevels3Stars= false;
                        break;
                    }
                }
            }
            else
            {
                allLevels3Stars = false;
            }
            if(allLevels3Stars)
            {
                EasterEggsHandler::getInstance()->unlockEasterEgg(EasterEggType::kHero9Unlocked);
                returnType = EasterEggType::kHero9Unlocked;
            }
        }
    }
    return returnType;
}

void GameLayer::onClickBoxMenu()
{
	if( _menuDig )_menuDig->disappearance();
	if( _menuCreateTower )_menuCreateTower->disappearance();
	if( _menuTower )_menuTower->disappearance();
	_selectedPlace = nullptr;
	_selectedUnit = nullptr;
	if( _interface )
	{
        _interface->setTouchNormal();
        _interface->updateTouchListeners();
	}
}


void GameLayer::buyLevelsMoney( int count )
{
	_boughtScoresForSession += count;
}

void GameLayer::shake( float value )
{
	const float x = 2.0f * value;
	const float t = 0.05f * value;

	Vector<FiniteTimeAction*> actions;
	actions.pushBack( MoveBy::create( t, Point( 0, +1 * x ) ) );
	actions.pushBack( MoveBy::create( t, Point( 0, -2 * x ) ) );
	actions.pushBack( MoveBy::create( t, Point( 0, +1 * x ) ) );
	actions.pushBack( MoveBy::create( t, Point( -0.5*x, 0 ) ) );
	actions.pushBack( MoveBy::create( t, Point( 1 * x, 0 ) ) );
	actions.pushBack( MoveBy::create( t, Point( -0.5*x, 0 ) ) );
	actions.pushBack( MoveBy::create( t, Point( 0, 2 * x ) ) );
	actions.pushBack( MoveBy::create( t, Point( 0, -4 * x ) ) );
	actions.pushBack( MoveBy::create( t, Point( 0, 2 * x ) ) );
	actions.pushBack( MoveBy::create( t, Point( -0.75*x, 0 ) ) );
	actions.pushBack( MoveBy::create( t, Point( 1.5*x, 0 ) ) );
	actions.pushBack( MoveBy::create( t, Point( -0.75*x, 0 ) ) );
	actions.pushBack( MoveBy::create( t, Point( 0, -2 * x ) ) );
	actions.pushBack( MoveBy::create( t, Point( 0, 4 * x ) ) );
	actions.pushBack( MoveBy::create( t, Point( 0, -2 * x ) ) );
	runAction( Sequence::create( actions ) );
}

void GameLayer::openEasterEggUnlockWindow( const EasterEggType easterType, const EasterPopupCloseCB& closeCallback)
{
    unschedule( schedule_selector( GameLayer::update ) );
    
    int level = static_cast<int>(easterType);
    LayerPointer layer = EasterEggsPopup::create(level, closeCallback);
    getSmartScene()->pushLayer( layer, true );
    
    AudioEngine::shared().stopMusic();
    AudioEngine::shared().playMusic( kMusicEasterEgg, false );
    AudioEngine::shared().playEffect( kSoundUnlockEaster );
}

void GameLayer::openStatisticWindow( FinishLevelParams* params )
{
	unschedule( schedule_selector( GameLayer::update ) );
	//	AdMob::show();

	LevelStatisticLayer::Result outcome =
		(_board->getGameMode() == GameMode::survival) ? LevelStatisticLayer::Neutral :
		(params->livecurrent > 0) ? LevelStatisticLayer::Win :
		/*  ELSE                                       */ LevelStatisticLayer::Lose;

	bool win = outcome == LevelStatisticLayer::Win;
	int level = _board->getCurrentLevelIndex();
	int stars = params->stars;
	int gold = LevelParams::shared().getAwardGold( level, stars, _board->getGameMode() );
	int tickets = LevelParams::shared().getTickets( level, _board->getGameMode() );

	int scoreType( -1 );
	int costfuel( 0 );
	LevelParams::shared().getPayForLevel( level, _board->getGameMode(), scoreType, costfuel );
	//int costfuel = LevelParams::shared().getFuel( level, _board->getGameMode() );
	if( _board->getGameMode() == GameMode::survival )
	{
		gold = LevelParams::shared().getAwardGold( level, 1, _board->getGameMode() );
		gold *= params->wavesCompleteCount;
	}

	LayerPointer layer;
	auto type = getParamCollection().get( "levelstatistic_type" );
	auto scene = dynamic_cast<GameScene*>(getScene());
	if( type == "LevelStatisticLayer" )
	{
		layer = _gameScene->openLevelStatistic( outcome, gold, tickets, costfuel, *params );
	}
	else
	{
        int levelIndex = getGameBoard().getCurrentLevelIndex();
		layer = VictoryMenu::create( scene, win, gold, stars, levelIndex );
	}

	getSmartScene()->pushLayer( layer, true );

	bool showad = UserData::shared().get<int>( k::user::UnShowAd ) == 0;
    bool showAdForCutScene = (_board->getCurrentLevelIndex() != 0 && _board->getCurrentLevelIndex() != 5 &&
                              _board->getCurrentLevelIndex() != 11 && _board->getCurrentLevelIndex() != 17 &&
                              _board->getCurrentLevelIndex() != 23);
    
	if( showad && Config::shared().get<bool>( "useAds" ) && showAdForCutScene)
	{
		AdsPlugin::shared().cacheInterstitial();
		auto showAd = []()
		{
			AdsPlugin::shared().showInterstitialBanner();
		};
		auto delay = DelayTime::create( 0 );
		auto call = CallFunc::create( std::bind( showAd ) );
		auto action = Sequence::createWithTwoActions( delay, call );
		if( layer )
			layer->runAction( action );
	}

	std::string music = win ? kMusicVictory : kMusicDefeat;
	AudioEngine::shared().stopMusic();
	AudioEngine::shared().playEffect( music );
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    if (win && (level == 5 || level == 11 || level == 23))
    {
        std::string ratePopupKey = std::string("RATE_POPUP_") + toStr(level);
        if (!UserDefault::getInstance()->getBoolForKey(ratePopupKey.c_str(), false))
        {
            ShareHandler::getInstance()->showRatePopup();
            UserDefault::getInstance()->setBoolForKey(ratePopupKey.c_str(), true);
            UserDefault::getInstance()->flush();
        }
    }
#endif

}

void GameLayer::flyCameraAboveMap( const Point & wave )
{
	auto computePointFinish = [this]( const Point & wavebegan )
	{
		auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
		auto mapsize = _bg->getContentSize();
		mapsize.width *= this->_mainlayer->getScaleX();
		mapsize.height *= this->_mainlayer->getScaleY();
		mapsize.width *= this->getScaleX();
		mapsize.height *= this->getScaleY();
		
		bool byHeight = (mapsize.height / dessize.height - mapsize.width / dessize.width) >= 0;

		Point wave = wavebegan;
		
//#parth change all commented line in this function is commented by parth
//		wave.x /= this->_mainlayer->getScaleX();
//		wave.y /= this->_mainlayer->getScaleY();
		Point res;
		res.x = 0.0f;
		res.y = 0.0f;
		
		if (Director::getInstance()->getWinSize().height < wave.y) {
			res.y = Director::getInstance()->getWinSize().height - wave.y;
		}
		
		if (Director::getInstance()->getWinSize().width < wave.x) {
			res.x = Director::getInstance()->getWinSize().width - wave.x;
		}
//		if( byHeight && wave.y > dessize.height / 2 )
//		{
//			res.x = 0;
//			res.y = dessize.height - mapsize.height;
//		}
//		else if( !byHeight && wave.x > dessize.width / 2 )
//		{
//			res.x = dessize.width - mapsize.width;
//			res.y = 0;
//		}
//		else
//		{
//			res = Point( 0, 0 );
//		}
//		if( dessize.height > mapsize.height )
//		{
//			res.y = (dessize.height - mapsize.height);
//		}
		return res;
	};
	auto computePointStart = [this]( const Point & finish )
	{
		auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
		auto mapsize = _bg->getContentSize();
		mapsize.width *= this->_mainlayer->getScaleX();
		mapsize.height *= this->_mainlayer->getScaleY();
		mapsize.width *= this->getScaleX();
		mapsize.height *= this->getScaleY();
		bool byHeight = (mapsize.height / dessize.height - mapsize.width / dessize.width) >= 0;

		Point res;

		if( byHeight && finish.y < 0 || !byHeight && finish.x < 0 )
		{
			res = Point( 0, 0 );
		}
		else if( byHeight )
		{
			res.x = 0;
			res.y = dessize.height - mapsize.height;
		}
		else if( !byHeight )
		{
			res.x = dessize.width - mapsize.width;
		}
		if( dessize.height > mapsize.height )
		{
			res.y = (dessize.height - mapsize.height);
		}

		return res;
	};
	auto createAction = [this]( const Point & start, const Point & end )
	{
		this->_mainlayer->setPosition( start );

		auto predelay = DelayTime::create( 0.05f );
		auto move2 = EaseInOut::create( MoveTo::create( 1.5f, end ), 2 );
		auto call = CallFunc::create( [this]()
		{
			if( _interface )
				_interface->onStartGame();
		} );

		return Sequence::create( predelay, move2, call, nullptr );
	};

	Point finish = computePointFinish( wave );
	Point start = computePointStart( finish );
	CCLOG("Start point :: %f %f",start.x,start.y);
	CCLOG("Finish point :: %f %f",finish.x,finish.y);
	this->_mainlayer->runAction( createAction( start, finish ) );
}

void GameLayer::createEffect( Unit*base, Unit*target, const std::string & effect )
{
	if( _board->getGameMode() == GameMode::multiplayer )
	{
		auto board = static_cast<GameBoardOnline*>(_board.ptr());
		if( board->getType() == GameBoardType::opponent )
			return;
	}
	auto effects = ShootsEffectsCreate( base, target, effect );
	for( auto& effect : effects )
	{
		int z = effect->getLocalZOrder();
		addObject( effect, 0 );
		if( z != 0 )
			effect->setLocalZOrder( z );
	}
}

void GameLayer::createAddMoneyNodeForWave( unsigned count, const Point & position )
{
	if( count > 0 )
	{
		xmlLoader::macros::set( "scores", toStr( count ) );
		xmlLoader::macros::set( "position", toStr( position ) );
		auto node = xmlLoader::load_node( "ini/gamescene/gearforwave.xml" );
		xmlLoader::macros::erase( "scores" );
		xmlLoader::macros::erase( "position" );
		if( _interface && !_interface->getIsOnlyScroll() )
			_interface->addChild( node );
	}
}

void GameLayer::addObject( Node * object, int zOrder )
{
	_objects->addChild( object, -object->getPositionY() );
}

void GameLayer::removeObject( Node * object )
{
	if( _objects )
		_objects->removeChild( object );
}

Node* GameLayer::getObjectsNode()
{
	return _objects;
}

void GameLayer::update( float dt )
{
	_board->update( dt );
	if( _interface )
		_interface->updateTouchListeners();
	if( _lootDropManager )
		_lootDropManager->update(dt);
    
}

Unit * GameLayer::getObjectInLocation( const Point & location )
{
	Vector<Node*> objects = _objects->getChildren();

	float distance( 2048 * 2048 );
	Unit * result( NULL );
	for( int i = 0; i < objects.size(); ++i )
	{
		Unit * object = dynamic_cast<Unit*>(objects.at( i ));
        if( !object ) {
			continue;
        }
        if (object->getType() == UnitType::desant) {
            continue;
        }
        
		float d = object->getPosition().getDistance( location );
		if( d < 100 && d < distance )
		{
			result = object;
			distance = d;
		}
	}
	if( dynamic_cast<Bullet*>(result) )
		result = nullptr;
	return result;
}

void GameLayer::achievementsObtained( const std::string & nameachiv )
{
	return;
}

void GameLayer::achievementsWindowClose( Ref * ref )
{
	auto scene = Director::getInstance()->getRunningScene();
	auto node = scene->getChildByTag( 1 );

	auto a1 = EaseBackIn::create( ScaleTo::create( 0.5f, 0 ) );
	auto a2 = CallFunc::create( std::bind( &Director::popScene, Director::getInstance() ) );
	node->runAction( Sequence::create( a1, a2, nullptr ) );
}

void GameLayer::selectUnit( Unit* node )
{
	_selectedUnit = node;
}

void GameLayer::startWaveAfter( const WaveInfo & wave, float duration )
{
	if( wave.index == 0 )
	{
		runEvent( "deferred_starwave" );
	}
	auto call = CallFunc::create( [&](){
		startWave( nullptr, 0, 0 );
	} );
	runAction( Sequence::create( DelayTime::create( duration ), call, nullptr ) );
}

void GameLayer::startWave( WaveIcon* icon, float elapsed, float duration )
{
	float percent( 0 );
	if( duration > 0.001f )
	{
		percent = elapsed / duration;
		percent = std::max( 0.f, percent );
		percent = std::min( 1.f, percent );
		percent = 1 - percent;
		float award = static_cast<float>(_scoresForStartWave)* percent;
		int score = static_cast<int>(award);
		if( score > 0 && icon )
		{
			ScoreCounter::shared().addMoney( kScoreLevel, score, false );
			createAddMoneyNodeForWave( score, icon->getPosition() );
		}
	}
	_board->resumeWaves();

	if( _interface && !_interface->getIsOnlyScroll() )
		_interface->removeIconsForWave();
    
//    const std::string& battleSound = _board->getBattleSound() == "" ? kMusicGameBattle : _board->getBattleSound();
////    battleSound =
//    
//    AudioEngine::shared().playMusic( battleSound );

	std::string event = "level" + toStr( _board->getCurrentLevelIndex() ) + "_startwave";
	TutorialManager::shared().dispatch( event );

	if( !_firstWaveStarted )
		_firstWaveStarted = true;

	runEvent( "start_wave" );
}

void GameLayer::setFastMode( bool enabled )
{
	_fastModeEnabled = enabled;
	auto rate = Config::shared().get<float>( "speedRate" );
	Director::getInstance()->getScheduler()->setTimeScale( enabled ? rate : 1.f );
}

void GameLayer::loadUserGifts()
{
	pugi::xml_document doc;
	doc.load_file( "ini/usersgift.xml" );
	auto root = doc.root().first_child();
	auto giftsLevelNode = root.child( "levels" );
	for( const auto& xml : giftsLevelNode )
	{
		auto level = xml.attribute( "level" ).as_int();
		auto value = xml.attribute( "wave" ).as_int();
		_userGiftsLevels.push_back( std::make_pair( level, value ) );
	}
}


NS_CC_END;
