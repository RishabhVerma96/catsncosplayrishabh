//
//  IslandDefense
//
//  Created by Vladimir Tolmachevﾂ on 2016.03.02
//
//
#include "ml/ImageManager.h"
#include "ml/Language.h"
#include "MenuItemTextBG.h"
#include "GameInterface.h"
#include "GameScene.h"
#include "ShopLayer2.h"
#include "Tutorial.h"
#include "ScoreCounter.h"
#include "support/AutoPlayer.h"
#include "support/UserData.h"
#include "consts.h"
#include "shop/ShopLayer.h"
#include "EasterEggsHandler.h"
#include <string>

NS_CC_BEGIN

static int zOrderInterfaceMenu( 99 );
static int zOrderInterfaceWaveIcon( 100 );

GameInterface::GameInterface()
: _gameLayer( nullptr )
, _enabled( true )
, _waveCooldown( 0 )
, _firstLaunch(true)
, _skillModeActived( false )
, _isIntteruptHeroMoving( false )
, _touchActive( TouchType::none )
, _touchNext( TouchType::none )
, _connectStatus(true)
, _scrollBegan(false)
, _touchesLock(0)
{}

GameInterface::~GameInterface()
{
	ShopLayer::observerOnPurchase().remove( _ID );
}

bool GameInterface::init( GameLayer* layer, GameScene* scene, bool onlyScroll )
{
	setName( "interface" );
	Node::init();
	NodeExt::init();
	this->setName( "interface" );
	_gameLayer = layer;
	_lootDropManager = layer->getLootDropManager();
	_gameScene = scene;
	_menu = nullptr;
	_skillsCount = 0;

	_onlyScroll = onlyScroll;
	if( layer )
	{
		_scrollInfo = make_intrusive<ScrollTouchInfo>();
		createTouchListeners();

		if( !onlyScroll )
		{
			_menu = Menu::create();
			_menu->setName( "menu" );
			_menu->setEnabled( false );
			_menu->setPosition( Point::ZERO );
			this->addChild( _menu, zOrderInterfaceMenu );
			createDevMenu();
            createStandartButtons();
			createSkillButtons();
			createBoxMenu();
			if( Config::shared().get<bool>( "useRateSpeedButton" ) )
				createRateButton();
			auto event = EventListenerKeyboard::create();
			event->onKeyReleased = std::bind( &GameInterface::onKeyReleased, this, std::placeholders::_1, std::placeholders::_2 );
			getEventDispatcher()->addEventListenerWithSceneGraphPriority( event, this );
			load( "ini/gamescene/interface.xml" );
		}

	}

	return true;
}

void GameInterface::createDevMenu()
{
	if( isTestDevice() && isTestModeActive() )
	{
		auto item = [this]( Menu * menu, std::string text, EventKeyboard::KeyCode key, Point pos )
		{
			auto sendKey = [this]( Ref* sender, EventKeyboard::KeyCode key )mutable
			{
				this->onKeyReleased( key, nullptr );
			};

			auto i = MenuItemTextBG::create( text, Color4F::GRAY, Color3B::BLACK, std::bind( sendKey, std::placeholders::_1, key ) );
			menu->addChild( i );
			i->setPosition( pos );
			i->setScale( 1.5f );
		};

		auto menu = Menu::create();
		menu->setPosition( 0, 0 );
		addChild( menu );
		Point pos( 25, 100 );
		float Y( 43 );

		auto levelNum = Label::createWithSystemFont( "Level #NN", "Arial", Y * 0.3f );
		levelNum->setName( "devMenuLevelNumber" );
		levelNum->setAnchorPoint( Vec2( 0, 0.5f ) );
		levelNum->setPosition( 5, pos.y );
		addChild( levelNum );
		pos.y += Y * 0.6f;

		item( menu, "R 1", EventKeyboard::KeyCode::KEY_1, pos ); pos.y += Y;
		item( menu, "R 5", EventKeyboard::KeyCode::KEY_5, pos ); pos.y += Y;
		item( menu, "R 9", EventKeyboard::KeyCode::KEY_9, pos ); pos.y += Y;
		item( menu, "R 0", EventKeyboard::KeyCode::KEY_0, pos ); pos.y += Y;
		item( menu, "R 99", EventKeyboard::KeyCode::KEY_F9, pos ); pos.y += Y;
		item( menu, "WIN", EventKeyboard::KeyCode::KEY_F1, pos ); pos.y += Y;
		pos.y += Y;
		item( menu, "+GEARs", EventKeyboard::KeyCode::KEY_F5, pos ); pos.y += Y;
		item( menu, "+GEMs", EventKeyboard::KeyCode::KEY_F6, pos ); pos.y += Y;
	}
}

void GameInterface::createHeroMenu()
{
	auto heroes = _gameLayer->getGameBoard().getHeroes();

	std::string path( "ini/gamescene/heroicon.xml" );
	if( _gameLayer->getGameScene()->getLevelMode() == GameMode::multiplayer )
		path = "ini/gamescene/online_heroicon.xml";

    //Task is almost done all we need to do is find the reference of hero's object and send it to callback
	for( int i = 0; i < static_cast<int>(heroes.size()); ++i )
	{
		auto hero = heroes[i];
		if( _buttonsHero.find( hero ) != _buttonsHero.end() )
			continue;
		auto callback = std::bind( &GameInterface::menuHero, this, std::placeholders::_1 );
		auto heroicon = HeroIcon::create( hero->getName(), path, callback );
		_buttonsHero[hero] = heroicon;
		heroicon->setEnabled( true );
		heroicon->setName( "hero" + toStr( i + 1 ) );
		heroicon->setHero( hero );
		_menu->addChild( heroicon );

		//auto hero2 = dynamic_cast<Hero2*>(hero.ptr());
		std::string skill = hero->getSkill();
		if( skill.empty() )
		{
			createHeroSkillMenu( hero );
		}
		else
		{
			createHeroSkillMenu( skill, hero );
		}

		auto pos0 = strTo<Point>( getParamCollection().get( "button_hero" + toStr( i + 1 ) ) );
		heroicon->setPosition( pos0 );
	}
	_gameLayer->runEvent( "showheromenu" );
	if( _gameLayer->getGameBoard().isGameStarted() )
	{
		for( auto pair : _buttonsHeroSkill )
		{
			for( auto button : pair.second )
				button->run();
		}
	}

}

void GameInterface::createHeroSkillMenu( const std::string& skill, Hero::Pointer hero )
{
	auto folder = Config::shared().get( "resourceGameSceneFolder" );
	std::string back( folder + "button_" + skill + "_2.png" );
	std::string forward( folder + "button_" + skill + "_1.png" );
	std::string cancel( folder + "button_" + skill + "_3.png" );
    
	std::vector<unsigned> skills;
	HeroExp::shared().skills( hero->getName(), skills );
	unsigned level = skills[4];

	float duration( 0 );
	duration = _gameLayer->getGameBoard().getSkillParams().skills.at( skill ).cooldown.at( level );
	
	auto callback2 = std::bind( &GameInterface::menuSkill, this, std::placeholders::_1, Skill::heroskill );
	auto heroskill = MenuItemCooldown::create( "", "", 0, nullptr, cancel );
	++_skillsCount;
	heroskill->setName( "heroskill" + toStr( _skillsCount ) );
	heroskill->setSound( "##sound_button##" );
	heroskill->init( back, forward, duration, callback2, cancel );
	heroskill->setAnimationOnFull( skill );
	heroskill->load( "ini/gamescene/heroskillbutton_override.xml" );
	_menu->addChild( heroskill );
	_buttonsHeroSkill[hero].push_back( heroskill );

	auto pos1 = strTo<Point>( getParamCollection().get( "button_heroskill" + toStr( _skillsCount ) ) );
	heroskill->setPosition( pos1 );
	heroskill->setEnabled( false );
}

void GameInterface::createHeroSkillMenu( Hero::Pointer hero )
{
	auto folder = Config::shared().get( "resourceGameSceneFolder" );
	auto activeSkills = hero->getActiveSkills();
	for( auto skill : activeSkills )
	{
		auto name = skill->getName();
		std::string back( folder + "button_" + name + "_2.png" );
		std::string forward( folder + "button_" + name + "_1.png" );
		std::string cancel( folder + "button_" + name + "_3.png" );
        CCLOG("button name :: %s",back.c_str());
		std::vector<unsigned> skills;
		HeroExp::shared().skills( hero->getName(), skills );
		unsigned level = skills[strTo<int>( skill->getNeedUnitSkill() )];

		float duration( 0 );

		if(_gameScene->getLevelIndex() != 0) {
			duration = _gameLayer->getGameBoard().getSkillParams().skills.at( skill->getName() ).cooldown.at( level );
		}
		else {
			duration = 1;
		}
		
		auto callback2 = std::bind( &GameInterface::menuSkill, this, std::placeholders::_1, Skill::heroskill );
		auto heroskill = MenuItemCooldown::create( "", "", 0, nullptr, cancel );
		++_skillsCount;
		heroskill->setName( "heroskill" + toStr( _skillsCount ) );
		heroskill->setSound( "##sound_button##" );
		heroskill->init( back, forward, duration, callback2, cancel );
		heroskill->setAnimationOnFull( skill->getName() );
		heroskill->load( "ini/gamescene/heroskillbutton_override.xml" );

		_menu->addChild( heroskill );

		auto call = []( MenuItemCooldown::Pointer heroskill, bool skillExecution )
		{
			if( skillExecution == false )
			{
				if( !heroskill->isRunning() )
				{
					heroskill->setEnabled( true );
				}
			}
			else
			{
				heroskill->setEnabled( false );
			}
		};

		hero->observerOnChangeExecution().add( heroskill->_ID, std::bind( call, heroskill, std::placeholders::_1 ) );

		_buttonsHeroSkill[hero].push_back( heroskill );

		auto pos1 = strTo<Point>( getParamCollection().get( "button_heroskill" + toStr( _skillsCount ) ) );
		heroskill->setPosition( pos1 );
		heroskill->setEnabled( false );
	}
}

void GameInterface::createTouchListeners()
{
	auto touchListenerN = EventListenerTouchAllAtOnce::create();
	touchListenerN->onTouchesBegan = std::bind( &GameInterface::onTouchesBegan, this, std::placeholders::_1, std::placeholders::_2 );
	touchListenerN->onTouchesMoved = std::bind( &GameInterface::onTouchesMoved, this, std::placeholders::_1, std::placeholders::_2 );
	touchListenerN->onTouchesEnded = std::bind( &GameInterface::onTouchesEnded, this, std::placeholders::_1, std::placeholders::_2 );
	touchListenerN->onTouchesCancelled = std::bind( &GameInterface::onTouchesCancelled, this, std::placeholders::_1, std::placeholders::_2 );

	auto touchListenerSD = EventListenerTouchOneByOne::create();
	touchListenerSD->onTouchBegan = std::bind( &GameInterface::onTouchSkillBegan, this, std::placeholders::_1, std::placeholders::_2, Skill::desant );
	touchListenerSD->onTouchEnded = std::bind( &GameInterface::onTouchSkillEnded, this, std::placeholders::_1, std::placeholders::_2, Skill::desant );
	touchListenerSD->onTouchCancelled = std::bind( &GameInterface::onTouchSkillCanceled, this, std::placeholders::_1, std::placeholders::_2 );
	touchListenerSD->setSwallowTouches( true );

	auto touchListenerSB = EventListenerTouchOneByOne::create();
	touchListenerSB->onTouchBegan = std::bind( &GameInterface::onTouchSkillBegan, this, std::placeholders::_1, std::placeholders::_2, Skill::bomb );
	touchListenerSB->onTouchEnded = std::bind( &GameInterface::onTouchSkillEnded, this, std::placeholders::_1, std::placeholders::_2, Skill::bomb );
	touchListenerSB->onTouchCancelled = std::bind( &GameInterface::onTouchSkillCanceled, this, std::placeholders::_1, std::placeholders::_2 );
	touchListenerSB->setSwallowTouches( true );

	auto touchListenerSP = EventListenerTouchOneByOne::create();
	touchListenerSP->onTouchBegan = std::bind( &GameInterface::onTouchSkillBegan, this, std::placeholders::_1, std::placeholders::_2, Skill::lootpicker );
	touchListenerSP->onTouchEnded = std::bind( &GameInterface::onTouchSkillEnded, this, std::placeholders::_1, std::placeholders::_2, Skill::lootpicker );
	touchListenerSP->onTouchCancelled = std::bind( &GameInterface::onTouchSkillCanceled, this, std::placeholders::_1, std::placeholders::_2 );
	touchListenerSP->setSwallowTouches( true );

	auto touchListenerSH = EventListenerTouchOneByOne::create();
	touchListenerSH->onTouchBegan = std::bind( &GameInterface::onTouchSkillBegan, this, std::placeholders::_1, std::placeholders::_2, Skill::heroskill );
	touchListenerSH->onTouchEnded = std::bind( &GameInterface::onTouchSkillEnded, this, std::placeholders::_1, std::placeholders::_2, Skill::heroskill );
	touchListenerSH->onTouchCancelled = std::bind( &GameInterface::onTouchSkillCanceled, this, std::placeholders::_1, std::placeholders::_2 );
	touchListenerSH->setSwallowTouches( true );

	auto touchListenerH = EventListenerTouchOneByOne::create();
    touchListenerH->onTouchBegan = std::bind( &GameInterface::onTouchHeroBegan, this, std::placeholders::_1, std::placeholders::_2 );
    touchListenerH->onTouchMoved = std::bind( &GameInterface::onTouchHeroMoved, this, std::placeholders::_1, std::placeholders::_2 );
    touchListenerH->onTouchEnded = std::bind( &GameInterface::onTouchHeroEnded, this, std::placeholders::_1, std::placeholders::_2 );
    touchListenerH->onTouchCancelled = std::bind( &GameInterface::onTouchHeroCanceled, this, std::placeholders::_1, std::placeholders::_2 );

    
    auto touchListenerDesant = EventListenerTouchOneByOne::create();
    touchListenerDesant->onTouchBegan = std::bind( &GameInterface::onTouchDesantBegan, this, std::placeholders::_1, std::placeholders::_2 );
    touchListenerDesant->onTouchMoved = std::bind( &GameInterface::onTouchDesantMoved, this, std::placeholders::_1, std::placeholders::_2 );
    touchListenerDesant->onTouchEnded = std::bind( &GameInterface::onTouchDesantEnded, this, std::placeholders::_1, std::placeholders::_2 );
    touchListenerDesant->onTouchCancelled = std::bind( &GameInterface::onTouchDesantCanceled, this, std::placeholders::_1, std::placeholders::_2 );

    

	_touchListenerNormal.reset( touchListenerN );
	_touchListenerDesant.reset( touchListenerSD );
	_touchListenerBomb.reset( touchListenerSB );
	_touchListenerLootPicker.reset( touchListenerSP );
	_touchListenerHeroSkill.reset( touchListenerSH );
	_touchListenerHero.reset( touchListenerH );
    _touchListenerDesantMove.reset( touchListenerDesant );

}

void GameInterface::createStandartButtons()
{
	auto cbShop = std::bind( &GameScene::openShop, _gameScene, std::placeholders::_1, true );
	auto cbPause = std::bind( &GameScene::openPause, _gameScene, std::placeholders::_1 );

	auto folder = Config::shared().get( "resourceGameSceneFolder"  );
	const std::string kPathButtonShop( folder + "button_shop.png" );
	const std::string kPathButtonPauseNormal( folder + "icon_pause.png" );

	_buttonShop = mlMenuItem::create( kPathButtonShop, cbShop );
	_buttonShop->setName( "shop" );
	_menu->addChild( _buttonShop );
	if( Config::shared().get<bool>( "useInapps" ) == false )
		_buttonShop->setPositionY( -9999.f );

	_buttonPause = mlMenuItem::create( kPathButtonPauseNormal, cbPause );
	_buttonPause->setName( "pause" );
	_menu->addChild( _buttonPause );
}

void GameInterface::createSkillButtons()
{
	auto folder = Config::shared().get( "resourceGameSceneFolder" );
	const std::string kPathButtonDesantBack( folder + "button_desant_2_1.png" );
	const std::string kPathButtonDesantForward( folder + "button_desant_2.png" );
	const std::string kPathButtonDesantCancel( folder + "button_desant_2_3.png" );

    const std::string kPathButtonDesant2Back( folder + "button_easteregg_2.png" );
    const std::string kPathButtonDesant2Forward( folder + "button_easteregg_1.png" );
    const std::string kPathButtonDesant2Cancel( folder + "button_easteregg_3.png" );

    const std::string kPathButtonDesantHealBack( folder + "button_heal_2.png" );
    const std::string kPathButtonDesantHealForward( folder + "button_heal_1.png" );
    const std::string kPathButtonDesantHealCancel( folder + "button_heal_3.png" );

    const std::string kPathButtonBombBack( folder + "button_desant_1_1.png" );
	const std::string kPathButtonBombForward( folder + "button_desant_1.png" );
	const std::string kPathButtonBombCancel( folder + "button_desant_1_3.png" );
	const std::string KPathButtonLootPickerBack( folder + "lootdrop/button_picker_d.png" );
	const std::string KPathButtonLootPickerForward( folder + "lootdrop/button_picker_n.png" );
	const std::string KPathButtonLootPickerCancel( folder + "lootdrop/button_picker_n.png" );
	const std::string cancel( folder + "icon_x_10.png" );

	auto cbDesant = std::bind( &GameInterface::menuDesant, this, std::placeholders::_1 ); //￢￻￱￮￤￪￠ ￤￥￱￠￭￲￠
    auto cbDesantHeal = std::bind( &GameInterface::menuDesantHeal, this, std::placeholders::_1 ); //￢￻￱￮￤￪￠ ￤￥￱￠￭￲￠
    auto cbBomb = std::bind( &GameInterface::menuSkill, this, std::placeholders::_1, Skill::bomb );   //￢￻￡￰￮￱ ￡￮￬￡￻
	float cdd = 0;
	_buttonDesant = MenuItemCooldown::create( kPathButtonDesantBack, kPathButtonDesantForward, cdd, cbDesant, kPathButtonDesantCancel );
	_buttonDesant->setAnimationOnFull( "airstike_animation1" );
	_buttonDesant->setName( "desant" );
	_buttonDesant->setSound( "##sound_button##" );
	_buttonDesant->setEnabled( false );
	_menu->addChild( _buttonDesant );

    bool isDuplicateDesantUnlocked = EasterEggsHandler::getInstance()->isEasterEggUnlocked(kDuplicateDesant);
    bool isHealUnlocked = EasterEggsHandler::getInstance()->isEasterEggUnlocked(kHealingAbility);

    _buttonDesant2 = MenuItemCooldown::create( kPathButtonDesant2Back, kPathButtonDesant2Forward, 0, cbDesant, kPathButtonDesant2Cancel );
    _buttonDesant2->setAnimationOnFull( "airstike_animation1" );
    _buttonDesant2->setName( "desant2" );
    _buttonDesant2->setSound( "##sound_button##" );
    _buttonDesant2->setEnabled( false );
    _menu->addChild( _buttonDesant2 );
	
    float desantHeal = _gameLayer->getGameBoard().getSkillParams().skills.at( "desantHeal" ).cooldown[0];

    _buttonDesantHeal = MenuItemCooldown::create( kPathButtonDesantHealBack, kPathButtonDesantHealForward, desantHeal, cbDesantHeal, kPathButtonDesantHealCancel );
    _buttonDesantHeal->setAnimationOnFull( "airstike_animation1" );
    _buttonDesantHeal->setName( "desantHeal" );
    _buttonDesantHeal->setSound( "##sound_button##" );
    _buttonDesantHeal->setEnabled( false );
    _menu->addChild( _buttonDesantHeal );

    
    _buttonDesant2->setVisible(false);
    _buttonDesant2->setEnabled(false);

    _buttonDesantHeal->setVisible(false);
    _buttonDesantHeal->setEnabled(false);

    
    if (isDuplicateDesantUnlocked)
    {
        _buttonDesant2->setVisible(true);
        _buttonDesant2->setEnabled(true);
        if(isHealUnlocked)
        {
            _buttonDesantHeal->setVisible(true);
            _buttonDesantHeal->setEnabled(true);
        }
    }
    
	float cda = _gameLayer->getGameBoard().getSkillParams().skills.at( "airplane" ).cooldown[0];
    if(_gameScene->getLevelIndex() == 1) {
        cda = 1.0f;
    }
	_buttonBomb = MenuItemCooldown::create( kPathButtonBombBack, kPathButtonBombForward, cda, cbBomb, kPathButtonBombCancel );
	_buttonBomb->setAnimationOnFull( "airstike_animation2" );
	_buttonBomb->setName( "bomb" );
	_buttonBomb->setSound( "##sound_button##" );
	_buttonBomb->setEnabled( false );
	_menu->addChild( _buttonBomb );

	if( _lootDropManager )
	{
		auto cbLootPicker = std::bind( &GameInterface::menuSkill, this, std::placeholders::_1, Skill::lootpicker );   //￢￻￡￰￮￱ ￡￮￬￡￻
		float cda = _gameLayer->getGameBoard().getSkillParams().skills.at( "lootpicker" ).cooldown[0];
		_buttonLootPicker = MenuItemCooldown::create( KPathButtonLootPickerBack, KPathButtonLootPickerForward, cda, cbLootPicker, KPathButtonLootPickerCancel );
		_buttonLootPicker->setAnimationOnFull( "airstike_animation2" );
		_buttonLootPicker->setName( "lootpicker" );
		_buttonLootPicker->setSound( "##sound_button##" );
		_buttonLootPicker->setEnabled( false );
		_menu->addChild( _buttonLootPicker );
		if( UserData::shared().get<bool>( k::user::LootPickerBought, false ) == false )
		{
			ShopLayer::observerOnPurchase().add( _ID, [this]( int type, int )
			{
				if( type == kScoreLootPicker )
				{
					_buttonLootPicker->run();
					ShopLayer::observerOnPurchase().remove( _ID );
				}
			} );
		}
	}
}

void GameInterface::menuDesantHeal( Ref*sender )
{
    _buttonDesantHeal->run();
    _gameLayer->runDesant2Heal();
}

void GameInterface::createBoxMenu()
{
    CCLOG("box menu created");
	std::string path( "ini/gamescene/boxmenu.xml" );
	if( _gameLayer->getGameScene()->getLevelMode() == GameMode::multiplayer )
		path = "ini/gamescene/online_boxmenu.xml";
	_boxMenu = BoxMenu::create( _gameLayer, path );
	addChild( _boxMenu );
}

void GameInterface::createRateButton()
{
	auto folder = Config::shared().get( "resourceGameSceneFolder" );
	const std::string kPathButtonSpeed1( folder + "speed_button_x1.png" );
	const std::string kPathButtonSpeed2( folder + "speed_button_x2.png" );
	auto cb_fast = std::bind( &GameInterface::menuFastModeEnabled, this, std::placeholders::_1, true );
	auto cb_normal = std::bind( &GameInterface::menuFastModeEnabled, this, std::placeholders::_1, false );

	_buttonRateNormal = mlMenuItem::create( kPathButtonSpeed1, cb_normal );
	_buttonRateFast = mlMenuItem::create( kPathButtonSpeed2, cb_fast );

	_buttonRateNormal->setName( "ratenormal" );
	_buttonRateFast->setName( "ratefast" );

	_menu->addChild( _buttonRateNormal );
	_menu->addChild( _buttonRateFast );

	_buttonRateNormal->setVisible( false );
}

ccMenuCallback GameInterface::get_callback_by_description( const std::string & name )
{
	if( name.find( "shop:" ) == 0 )
		return ShopLayer2::dispatch_query( name );
	if( name == "openshop" )
		return std::bind( &GameScene::openShop2, _gameScene, std::placeholders::_1, true );
	else
		return NodeExt::get_callback_by_description( name );
}

void GameInterface::onEnter()
{
	Node::onEnter();
	if( _firstLaunch )
	{
		_firstLaunch = false;

		auto node = _gameLayer->getMainLayer();
		Point pos = node->getPosition();
		auto size = _gameLayer->getContentSize();
		size.width *= _gameLayer->getScaleX();
		size.height *= _gameLayer->getScaleY();
		_scrollInfo->node = node;
		pos = _scrollInfo->fitPosition( pos, size );
		node->setPosition( pos );
	}
}

void GameInterface::menuHero( Ref*sender )
{
	auto prev = _gameLayer->getSelectedHero();
	HeroIcon* icon = dynamic_cast<HeroIcon*>(sender);
	if( icon )
		_gameLayer->selectHero( icon->getHero() );
	auto curr = _gameLayer->getSelectedHero();

	_boxMenu->close();
	if( prev != curr )//_touchActive == TouchType::hero == false )
		setTouchHero();
	else
		setTouchNormal();
}

void GameInterface::selectHeroAutomatically() {
    auto heroes = _gameLayer->getGameBoard().getHeroes();
    
    auto prevHero = _gameLayer->getSelectedHero();
    
    if( heroes[0] != nullptr ) {
        _gameLayer->selectHero( heroes[0] );
    }
    
    auto currHero = _gameLayer->getSelectedHero();
    
    if( prevHero != currHero ) {
        setTouchHero();
    }
}

void GameInterface::menuDesant( Ref*sender )
{
    MenuItemCooldown* button  = (MenuItemCooldown*) sender;
    int desantTag = 1;
    
    
    
    if(button->getName() == "desant2")
    {
        desantTag  = 2;
    }
    
    auto prev = _gameLayer->getSelectedDesant();
    
    if (prev)
    {
        if (prev->getDesantType() == desantTag)
        {
            setTouchNormal();
            _gameLayer->selectDesant(nullptr);
        }
        else
        {
            auto desant = _gameLayer->getAvailableDesant();
            if (desantTag == 2)
            {
                desant = _gameLayer->getAvailableDesant2();
            }
            _gameLayer->selectDesant(desant);
        }
    }
    else
    {
        auto desant = _gameLayer->getAvailableDesant();
        if (desantTag == 2)
        {
            desant = _gameLayer->getAvailableDesant2();
        }
        _gameLayer->selectDesant(desant);
    }
//
//    HeroIcon* icon = dynamic_cast<HeroIcon*>(sender);
//    if( icon )
//        _gameLayer->selectHero( icon->getHero() );
//    auto curr = _gameLayer->getSelectedHero();
//
//    _boxMenu->close();
//    if( prev != curr )//_touchActive == TouchType::hero == false )
//        setTouchHero();
//    else
//        setTouchNormal();
}

bool GameInterface::scrollBegan( const std::vector<Touch*>& touches )
{
	_scrollBegan = true;
	return _gameLayer->getMainLayer()->touchesBegan( touches );
}

bool GameInterface::scrollMoved( const std::vector<Touch*>& touches )
{
	return _gameLayer->getMainLayer()->touchesMoved( touches );
}

bool GameInterface::scrollEnded( const std::vector<Touch*>& touches )
{
	_scrollBegan = false;
	return _gameLayer->getMainLayer()->touchesEnded( touches );
}

void GameInterface::onTouchesBegan( const std::vector<Touch*>& touches, Event *event )
{
	if( !_enabled )
		return;

	++_touchesLock;
	if( scrollBegan( touches ) == false && !_onlyScroll )
	{
		for( auto i : touches )
		{
			Touch * touch = dynamic_cast<Touch*>(i);
			
			auto location = touch->getLocation();
			location = _gameLayer->convertToGameSpase( location );

			Node * node = _gameLayer->getObjectInLocation( location );
			if( node == nullptr )
			{
				node = _gameLayer->getTowerPlaceInLocation( location );
			}

			if( node )
			{
				touchInfo ti( node, touch );
				_touches[touch->getID()] = ti;
			}
		}
	}
}

void GameInterface::onTouchesMoved( const std::vector<Touch*>& touches, Event *event )
{
	if( !_enabled )
		return;
	scrollMoved( touches );
}

void GameInterface::onTouchesEnded( const std::vector<Touch*>& touches, Event *event )
{
	_isIntteruptHeroMoving = false;
	if( !_enabled )
		return;

	if( _touchesLock == 0 )
		return;

	bool wasScroll = scrollEnded( touches );

	if( !wasScroll && !_onlyScroll)
	{
		for( auto i : touches )
		{
			Touch * touchEnd = i;
			touchInfo & touchBegin = _touches[touchEnd->getID()];
			Point location = _gameLayer->convertToGameSpase( touchEnd->getLocation() );
			Point startLocation = _gameLayer->convertToGameSpase( touchEnd->getStartLocation() );

			Unit * node = _gameLayer->getObjectInLocation( location );
			if( location.getDistance( startLocation ) < 50 )
			{
				auto place = _gameLayer->getTowerPlaceInLocation( location );
				if( place && touchBegin.nodeBegin.ptr() == place )
				{
					_isIntteruptHeroMoving = true;
					_gameLayer->onClickByTowerPlace( place );
				}
				else if( node && node == touchBegin.nodeBegin )
				{
					//#parth change
//					_isIntteruptHeroMoving = true;
					_gameLayer->onClickByObject( node );
				}
				else
				{
					_gameLayer->closeMenuTower();
					onEmptyTouch( touchEnd->getLocation() );
				}
				_gameLayer->closeMenuDig();
				_gameLayer->markTowerPlaceOnLocation( location );
			}
			if( node == nullptr )
			{
				_gameLayer->selectUnit( nullptr );
			}
			_touches.erase( _touches.find( touchEnd->getID() ) );
		}
	}

	for( auto i : touches )
	{
		if( _touches.find( i->getID() ) != _touches.end() )
			_touches.erase( _touches.find( i->getID() ) );
	}

	_touchesLock = std::max( _touchesLock - 1, 0 );
}

void GameInterface::onTouchesCancelled( const std::vector<Touch*>&touches, Event *event )
{
	onTouchesEnded( touches, event );
}

bool GameInterface::onTouchSkillBegan( Touch* touch, Event *event, Skill skill )
{
    CCLOG("inside GameInterface touch began");
	return true;
}

void GameInterface::onTouchSkillEnded( Touch* touch, Event *event, Skill skill )
{
	MenuItemCooldown* button( nullptr );
	auto location = touch->getLocation();
	location = _gameLayer->convertToGameSpase( location );
	bool dispatched( false );

	std::string skillName;
	UnitActiveSkill::Pointer activeSkill;
	switch( skill )
	{
		case Skill::desant:
			skillName = "desant";
			break;
		case Skill::bomb:
		{
			float dist_water( 9999 );
			float dist_earth( 9999 );

			if( checkPointOnRoute( _gameLayer->getGameBoard().getCreepsRoutes(), location, _gameLayer->getGameBoard().getSkillParams().distanceToRoute, UnitLayer::earth, &dist_earth ) )
				skillName = "airplane_earth";
			else if( checkPointOnRoute( _gameLayer->getGameBoard().getCreepsRoutes(), location, _gameLayer->getGameBoard().getSkillParams().distanceToRoute, UnitLayer::sea, &dist_water ) )
				skillName = "airplane_water";

			break;
		}
		case Skill::lootpicker:
			dispatched = _lootDropManager->createPicker( location );
			break;		
		case Skill::heroskill:
			for( auto pair : _buttonsHeroSkill )
			{
				int index = 0;
				for( auto menuItem : pair.second )
				{
					if( menuItem->isActive() )
					{
						skillName = pair.first->getSkill();
						if( skillName.empty() )
						{
							auto skills = pair.first->getActiveSkills();
							activeSkill = skills[index];
						}
						else
						{
							skillName = pair.first->getSkill();
						}
						button = menuItem;
						dispatched = false;
						break;
					}
					else
					{
                        _gameLayer->getGameBoard()._easterParams.isHeroUsed = true;
						dispatched = true;
					}
					++index;
				}
				if( dispatched == false ) break;
			}
			break;
	}

	if( dispatched == false )
	{
		if( _gameLayer->getGameBoard().createActiveSkillUnit( skillName, location ) )
		{
            if(UserData::shared().get<bool>("Vibration_Enabled",true)) {
                cocos2d::Device::vibrate(0.4f);
            }
			dispatched = true;
		}
		else if( activeSkill )
		{
            _gameLayer->getGameBoard()._easterParams.isMineUsed = true;
			dispatched = activeSkill->startExecution( location );
		}
	}

	if( dispatched )
	{
		if( skill == Skill::desant )
		{
			//#parth change
            _buttonDesant->run();
			TutorialManager::shared().dispatch( "usedesant" );
		}
		else if( skill == Skill::bomb )
		{
			_buttonBomb->run();
            _gameLayer->getGameBoard()._easterParams.isDesantUsed = true;
			TutorialManager::shared().dispatch( "useairbomb" );
		}
		else if( skill == Skill::lootpicker )
		{
			_buttonLootPicker->run();
			TutorialManager::shared().dispatch( "uselootpicker" );
		}
		else
		{
			if( button )
			{
				button->run();
//				TutorialManager::shared().dispatch( "use" + skillName );
                if (activeSkill->getName().find("_landmine") != std::string::npos) {
                    TutorialManager::shared().dispatch( "placelevel" + std::to_string(_gameScene->getLevelIndex()) + "_landmine" );
                }
                
				TutorialManager::shared().dispatch( "place" + activeSkill->getName() );
			}
		}
	}

	if( dispatched )
	{
		_buttonSelectedSkill.reset( nullptr );
		setTouchNormal();
	}
	else
	{
		onForbiddenTouch( touch->getLocation() );
	}
    
    selectHeroAutomatically();
}

void GameInterface::onTouchSkillCanceled( Touch* touch, Event *event )
{
	setTouchNormal();
}

bool GameInterface::onTouchHeroBegan( Touch* touch, Event *event )
{
	std::vector<Touch*> touches;
	touches.push_back( touch );
	onTouchesBegan( touches, event );
	return true;
}

void GameInterface::onTouchHeroMoved( Touch* touch, Event *event )
{
	std::vector<Touch*> touches;
	touches.push_back( touch );
	onTouchesMoved( touches, event );
}

void GameInterface::onTouchHeroEnded( Touch* touch, Event *event )
{
	std::vector<Touch*> touches;
	touches.push_back( touch );
	onTouchesEnded( touches, event );

	if( _isIntteruptHeroMoving )
		return;

	auto location = touch->getLocation();
	if( location.getDistance( touch->getStartLocation() ) > 100 )
		return;

	location = _gameLayer->convertToGameSpase( location );
	assert( _gameLayer->getSelectedHero() );
    // move hero to selected position
	_gameLayer->getGameBoard().moveHero( _gameLayer->getSelectedHero(), location );
    
    if(_touchNext != TouchType::hero) {
        setTouchNormal();
    }
    
}

void GameInterface::onTouchHeroCanceled( Touch* touch, Event *event )
{
	setTouchNormal();
}


bool GameInterface::onTouchDesantBegan( Touch* touch, Event *event )
{
    std::vector<Touch*> touches;
    touches.push_back( touch );
    onTouchesBegan( touches, event );
    return true;

}
void GameInterface::onTouchDesantMoved( Touch* touch, Event *event )
{
    std::vector<Touch*> touches;
    touches.push_back( touch );
    onTouchesMoved( touches, event );

}
void GameInterface::onTouchDesantEnded( Touch* touch, Event *event )
{
    std::vector<Touch*> touches;
    touches.push_back( touch );
    onTouchesEnded( touches, event );
    
    if( _isIntteruptHeroMoving )
        return;
    
    auto location = touch->getLocation();
    if( location.getDistance( touch->getStartLocation() ) > 100 )
        return;
    
    location = _gameLayer->convertToGameSpase( location );
    assert( _gameLayer->getSelectedDesant() );
    _gameLayer->getGameBoard().moveDesant( _gameLayer->getSelectedDesant(), location );
    setTouchNormal();
    selectHeroAutomatically();

}
void GameInterface::onTouchDesantCanceled( Touch* touch, Event *event )
{
    setTouchNormal();
}



void GameInterface::onKeyReleased( EventKeyboard::KeyCode keyCode, Event* event )
{
	if( keyCode == EventKeyboard::KeyCode::KEY_BACK )
		_gameScene->openPause( nullptr );
	if( isTestDevice() && isTestModeActive() )
	{
#if PC != 1
		if( keyCode == EventKeyboard::KeyCode::KEY_0 )  setSpeedRate( 0 );
		if( keyCode == EventKeyboard::KeyCode::KEY_1 )  setSpeedRate( 1 );
		if( keyCode == EventKeyboard::KeyCode::KEY_2 )  setSpeedRate( 2 );
		if( keyCode == EventKeyboard::KeyCode::KEY_3 )  setSpeedRate( 3 );
		if( keyCode == EventKeyboard::KeyCode::KEY_4 )  setSpeedRate( 4 );
		if( keyCode == EventKeyboard::KeyCode::KEY_5 )  setSpeedRate( 5 );
		if( keyCode == EventKeyboard::KeyCode::KEY_6 )  setSpeedRate( 6 );
		if( keyCode == EventKeyboard::KeyCode::KEY_7 )  setSpeedRate( 7 );
		if( keyCode == EventKeyboard::KeyCode::KEY_8 )  setSpeedRate( 8 );
		if( keyCode == EventKeyboard::KeyCode::KEY_9 )  setSpeedRate( 9 );
		if( keyCode == EventKeyboard::KeyCode::KEY_F9 ) setSpeedRate( 99 );
		if( keyCode == EventKeyboard::KeyCode::KEY_F1 ) _gameLayer->getGameBoard().finishGame();
		if( keyCode == EventKeyboard::KeyCode::KEY_F5 ) ScoreCounter::shared().addMoney( kScoreLevel, 1000, false );
		if( keyCode == EventKeyboard::KeyCode::KEY_F6 ) ScoreCounter::shared().addMoney( kScoreCrystals, 1000, false );
		if( keyCode == EventKeyboard::KeyCode::KEY_F2 )
		{
			ScoreCounter::shared().setMoney( kScoreHealth, 0, false );
			_gameLayer->getGameBoard().finishGame();
		}
#endif
	}
}

void GameInterface::setSpeedRate( float rate )
{
	Director::getInstance()->getScheduler()->setTimeScale( rate );
	auto autoPlayer = AutoPlayer::getInstance();
	if ( autoPlayer != nullptr ) {
		autoPlayer->setRate( rate );
	}
}

void GameInterface::onStartGame()
{
//    #parth
//    setTouchNormal();
	if( _menu )
	{
		_menu->setEnabled( true );
		//#parth change
        _buttonDesant->run();
		_buttonBomb->run();
        _buttonDesant2->run();
        _buttonDesantHeal->run();
		if( _buttonLootPicker && UserData::shared().get<bool>( k::user::LootPickerBought, false ) )
			_buttonLootPicker->run();
	}
	for( auto pair : _buttonsHeroSkill )
	{
		for( auto button : pair.second )
			button->run();
	}
}

void GameInterface::onSelectHero( Hero* hero )
{
    for( auto pair : _buttonsHero ) {
        pair.second->showCancel( pair.first == hero );
    }
		
    if( hero ) {
        CCLOG("************* Hero Selected *************");
        setTouchHero();
    }
    else if( _touchActive == TouchType::hero ) {
        CCLOG("************* Hero Touch set to normal *************");
        //setTouchNormal();
    }

}

void GameInterface::onSelectDesant( UnitDesant* desant)
{
    if( desant ){
        
        int desantType = desant->getDesantType();
		//#parth change
        _buttonDesant->showCancel(desantType==1);
        _buttonDesant2->showCancel(desantType==2);

        setTouchDesantMove();
    }
    else if( _touchActive == TouchType::desantmove )
    {
        setTouchNormal();
		////#parth change
        _buttonDesant->showCancel(false);
        _buttonDesant2->showCancel(false);

    }
}

void GameInterface::onHeroDead( Hero* hero )
{
	if( _buttonsHeroSkill.empty() ) return;
	const auto& buttons = _buttonsHeroSkill.at( hero );
	for( auto button : buttons )
	{
		button->stop();
		if( _buttonSelectedSkill == button )
		{
			_buttonSelectedSkill.reset( nullptr );
			if( _touchActive == TouchType::heroskill )
				setTouchNormal();
		}
	}
}

void GameInterface::onHeroResurrected( Hero* hero )
{
	if( _buttonsHeroSkill.empty() ) return;
	const auto& buttons = _buttonsHeroSkill.at( hero );
	for( auto button : buttons )
		button->run();
}


void GameInterface::onDesantDead( UnitDesant* desant )
{
    
    if(desant->getDesantType() == 1)
    {
		//#parth change
        _buttonDesant->stop();
        _buttonBomb->stop();
//		_buttonBomb->pauseItem();
    }
    else {
        _buttonDesant2->stop();
        _buttonDesantHeal->stop();
    }
    
    
//    if( _touchActive == TouchType::desantmove )
//        setTouchNormal();

}

void GameInterface::onDesantResurrected( UnitDesant* desant )
{
    
    if(desant->getDesantType() == 1)
    {
		//#parth change
        _buttonDesant->run();
        _buttonBomb->run();
//		_buttonBomb->resumeItem();
    }
    else {
        _buttonDesant2->run();
        _buttonDesantHeal->run();
    }
}


void GameInterface::onClickByTower()
{
	if( _touchActive == TouchType::hero || _touchActive == TouchType::desantmove )
		setTouchNormal();
}

void GameInterface::onClickByHero()
{
	setTouchHero();
}

void GameInterface::onClickByDesant()
{
    setTouchDesantMove();
}

void GameInterface::onClickByTowerPlace()
{
	if( _touchActive == TouchType::hero || _touchActive == TouchType::desantmove )
		setTouchNormal();
}

void GameInterface::onEmptyTouch( const Point & touchlocation )
{
	auto folder = Config::shared().get( "resourceGameSceneFolder" );
	auto sprite = ImageManager::sprite( folder + "empty_touch.png" );
	if( sprite )
	{
		addChild( sprite, 9 );
		sprite->setPosition( touchlocation );
		sprite->setScale( 0 );
		static float duration( 0.5f );
		sprite->runAction( Sequence::createWithTwoActions(
			ScaleTo::create( duration, 1 ),
			CallFunc::create( std::bind( &Node::removeFromParent, sprite ) )
			) );
		sprite->runAction( FadeTo::create( duration, 128 ) );
	}
}

void GameInterface::onForbiddenTouch( const Point & touchlocation )
{
	auto folder = Config::shared().get( "resourceGameSceneFolder" );
	auto sprite = ImageManager::sprite( folder + "icon_x.png" );
	if( sprite )
	{
		addChild( sprite, 9 );
		sprite->setPosition( touchlocation );
		sprite->setScale( 0 );
		static float duration( 0.5f );
		sprite->runAction( Sequence::createWithTwoActions(
			EaseBounceOut::create( ScaleTo::create( duration, 1 ) ),
			CallFunc::create( std::bind( &Node::removeFromParent, sprite ) )
			) );
		sprite->runAction( FadeTo::create( duration, 128 ) );
	}
}

void GameInterface::createIconForWave( const Route & route, const WaveInfo & wave, UnitLayer::type type, const std::list<std::string> & icons, float delay )
{
	Point start = route.front();
	auto callback = std::bind( &GameLayer::startWave, _gameLayer,
		std::placeholders::_1,
		std::placeholders::_2,
		std::placeholders::_3
		);

	auto icon = WaveIcon::create( _gameLayer, start, delay, _waveCooldown, callback, type );
	icon->setName( "waveicon" );
	_waveIcons.push_back( icon );
	this->addChild( icon, zOrderInterfaceWaveIcon );

	std::string event = "level" + toStr( _gameLayer->getGameBoard().getCurrentLevelIndex() ) + "_waveicon";
	TutorialManager::shared().dispatch( event );
}

void GameInterface::removeIconsForWave()
{
	for( auto& icon : _waveIcons )
	{
		icon->removeFromParent();
	}
	_waveIcons.clear();
}

void GameInterface::showWaveIcons()
{
	for( auto icon : _waveIcons )
		icon->setActive( true );
}

void GameInterface::connectStatus( bool isConnected )
{
	if( _connectStatus != isConnected && !isConnected )
	{
		if( OnlineConnector::shared().isConnected() )
		{
			auto text = Language::shared().string( "online_noconnection_opponent_text" );
			auto caption = Language::shared().string( "online_noconnection_opponent_caption" );
			MessageBox( text.c_str(), caption.c_str() );
		}
		else
		{
			auto text = Language::shared().string( "online_noconnection_text" );
			auto caption = Language::shared().string( "online_noconnection_caption" );
			MessageBox( text.c_str(), caption.c_str() );
		}
	}
	_connectStatus = isConnected;
}

void GameInterface::menuFastModeEnabled( Ref*, bool enabled )
{
	if( _buttonRateFast ) _buttonRateFast->setVisible( !enabled );
	if( _buttonRateNormal ) _buttonRateNormal->setVisible( enabled );
	_gameLayer->setFastMode( enabled );
}

void GameInterface::menuSkill( Ref * sender, Skill skill )
{
	_boxMenu->close();
	
	_gameLayer->closeMenuDig();
	_gameLayer->closeMenuCreateTower();
	_gameLayer->closeMenuTower();
	_gameLayer->resetSelectedPlace();
	_gameLayer->selectUnit( nullptr );

	MenuItemCooldown * item = dynamic_cast<MenuItemCooldown*>(sender);
	if( _buttonSelectedSkill && item == _buttonSelectedSkill )
	{		
		item->showCancel( false );
		setTouchNormal();
	}
	else if( item != _buttonSelectedSkill )
	{
		setTouchNormal();
		updateTouchListeners();
		item->showCancel( true );
		_buttonSelectedSkill = item;
		setTouchSkill( sender, skill );
	}
//    CCLOG("skill clicked :: %s",_buttonSelectedSkill->getName().c_str());
	TutorialManager::shared().dispatch( "clickskillbutton" );
    if(_buttonSelectedSkill != NULL) {
        TutorialManager::shared().dispatch("level" + toStr( _gameScene->getLevelIndex() ) + _buttonSelectedSkill->getName() + "clickskillbutton" );
    }
}

void GameInterface::resetSkillButtons()
{
	if( _menu )
	{
		_buttonSelectedSkill = nullptr;
		_buttonBomb->showCancel( false );
		//#parth change
        _buttonDesant->showCancel( false );
        _buttonDesant2->showCancel(false);
        _buttonDesantHeal->showCancel(false);
		if( _buttonLootPicker && UserData::shared().get<bool>( k::user::LootPickerBought, false ) )
			_buttonLootPicker->showCancel( false );
		for( auto pair : this->_buttonsHeroSkill )
		{
			for( auto button : pair.second )
				button->showCancel( false );
		}
	}
}

void GameInterface::updateTouchListeners()
{
	if( _touchActive == _touchNext )
		return;
	if( _scrollBegan )
	{
		_gameLayer->getMainLayer()->touchesCancelled();
		_scrollBegan = false;
	}
	switch( _touchActive )
	{
		case TouchType::normal: _eventDispatcher->removeEventListener( _touchListenerNormal ); break;
		case TouchType::bomb: _eventDispatcher->removeEventListener( _touchListenerBomb ); break;
		case TouchType::desant: _eventDispatcher->removeEventListener( _touchListenerDesant ); break;
		case TouchType::lootpicker: _eventDispatcher->removeEventListener( _touchListenerLootPicker ); break;
		case TouchType::hero: _eventDispatcher->removeEventListener( _touchListenerHero ); break;
		case TouchType::heroskill: _eventDispatcher->removeEventListener( _touchListenerHeroSkill ); break;
        case TouchType::desantmove: _eventDispatcher->removeEventListener( _touchListenerDesantMove ); break;
		case TouchType::none:break;
		default:
			assert( 0 );
	}
	switch( _touchNext )
	{
		case TouchType::normal:
			_eventDispatcher->addEventListenerWithSceneGraphPriority( _touchListenerNormal, this );
			_skillModeActived = false;
			resetSkillButtons();
			_gameLayer->selectHero( nullptr );
            _gameLayer->selectDesant( nullptr );
			break;
		case TouchType::bomb:
			_eventDispatcher->addEventListenerWithSceneGraphPriority( _touchListenerBomb, this );
			_skillModeActived = true;
			break;
		case TouchType::desant:
			_eventDispatcher->addEventListenerWithSceneGraphPriority( _touchListenerDesant, this );
			_skillModeActived = true;
			break;
		case TouchType::lootpicker:
			_eventDispatcher->addEventListenerWithSceneGraphPriority( _touchListenerLootPicker, this );
			_skillModeActived = true;
			break;
		case TouchType::hero:
			_eventDispatcher->addEventListenerWithSceneGraphPriority( _touchListenerHero, this );
			_skillModeActived = false;
			resetSkillButtons();
            _gameLayer->selectDesant( nullptr );
			_gameLayer->selectHero( _gameLayer->getSelectedHero() );
			break;
        case TouchType::desantmove:
            _eventDispatcher->addEventListenerWithSceneGraphPriority( _touchListenerDesantMove, this );
            _skillModeActived = false;
            resetSkillButtons();
            _gameLayer->selectHero( nullptr );
            _gameLayer->selectDesant( _gameLayer->getSelectedDesant() );
            break;

		case TouchType::heroskill:
			_eventDispatcher->addEventListenerWithSceneGraphPriority( _touchListenerHeroSkill, this );
			_skillModeActived = true;
			break;
		case TouchType::none:break;
		default:
			assert( 0 );
	}
	_touchActive = _touchNext;
}

void GameInterface::setTouchDisabled()
{
	_touchNext = TouchType::none;
}

void GameInterface::setTouchNormal()
{
	runEvent( "select_none" );
	_touchNext = TouchType::normal;
}

void GameInterface::setTouchSkill( Ref*sender, Skill skill )
{
	MenuItemCooldown * button = dynamic_cast<MenuItemCooldown*>( sender );
	runEvent( "select_" + button->getName() );
	switch( skill )
	{
		case Skill::desant: _touchNext = TouchType::desant; break;
		case Skill::bomb: _touchNext = TouchType::bomb; break;
		case Skill::lootpicker: _touchNext = TouchType::lootpicker; break;
		case Skill::heroskill:
		{
            
			bool isMeleeSkill( false );

			UnitActiveSkill::Pointer activeSkill;

			for( auto pair : _buttonsHeroSkill )
			{
				int index = 0;
				for( auto menuItem : pair.second )
				{
					if( button == menuItem )
					{
						auto activeSkills = pair.first->getActiveSkills();
						if( activeSkills.size() > 0 )
						{
							activeSkill = activeSkills[index];
							if(isMeleeSkill = activeSkill->isMelee())
							{
								break;
							}
						}
					}

					++index;
				}
				if( isMeleeSkill ) break;
			}

			if( isMeleeSkill )
			{
				activeSkill->startExecution( Vec2::ZERO );

				if( button )
					button->run();

				TutorialManager::shared().dispatch( "usedesant" );
                
                if (activeSkill->getName().find("_shockwave") != std::string::npos) {
                    TutorialManager::shared().dispatch( "uselevel" + std::to_string(_gameScene->getLevelIndex()) + "_shockwave" );
                }
                
                if (UserData::shared().get<bool>("Vibration_Enabled",true) && activeSkill->getName().find("_shockwave") != std::string::npos) {
                    cocos2d::Device::vibrate(0.4f);
                }
                
				TutorialManager::shared().dispatch( "use" + activeSkill->getName() );

				_buttonSelectedSkill.reset( nullptr );

				setTouchNormal();
                if(skill == Skill::heroskill)
                {
                    _gameLayer->getGameBoard()._easterParams.isHeroUsed = true;
                }
                selectHeroAutomatically();
			}
			else
			{
                
                if (activeSkill->getName().find("_landmine") != std::string::npos) {
                    TutorialManager::shared().dispatch( "uselevel" + std::to_string(_gameScene->getLevelIndex()) + "_landmine" );
                }
               
				TutorialManager::shared().dispatch( "use" + activeSkill->getName() );
				_touchNext = TouchType::heroskill;
			}
			break;
		}
            
		default:
			assert( 0 );
	}
}

void GameInterface::setTouchHero()
{
	_touchNext = TouchType::hero;
    CCLOG("Hero selected");
    std::string event = "level" + toStr( _gameScene->getLevelIndex() ) + "_heroselect";
    TutorialManager::shared().dispatch( event );
    CCLOG("hero selected");
    
//    if (UserData::shared().get<bool>("Vibration_Enabled",true)) {
//        cocos2d::Device::vibrate(0.4f);
//    }
    
}

void GameInterface::setTouchDesantMove()
{
    _touchNext = TouchType::desantmove;
    CCLOG("desant selected");
    std::string event = "level" + toStr( _gameScene->getLevelIndex() ) + "_desantselect";
    TutorialManager::shared().dispatch( event );
}

BoxMenu* GameInterface::getBoxMenu()
{
	return _boxMenu;
}

void GameInterface::setEnabled( bool var )
{
	_enabled = var;
	if( _menu )
		_menu->setEnabled( var );
	if( _boxMenu )
		_boxMenu->setEnabled( var );
    if( _gameLayer->getMenuCreateTower() )
        _gameLayer->getMenuCreateTower()->disappearance();
    if( _gameLayer->getMenuTower() )
        _gameLayer->getMenuTower()->disappearance();
    if( _gameLayer->getMenuDig() )
        _gameLayer->getMenuDig()->disappearance();

}

bool GameInterface::getEnabled()
{
	return _enabled;
}

NS_CC_END
