//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 2016.03.02
//
//
#include "ml/audio/AudioEngine.h"
#include "ml/MenuItem.h"
#include "ml/KeyboardListener.h"
#include "services/plugins/AdsPlugin.h"
#include "online/WaitOpponentLayer.h"
#include "services/playservices/playservices.h"
#include "GameScene.h"
#include "GameInterface.h"
#include "GamePauseScene.h"
#include "Tutorial.h"
#include "ShopLayer.h"
#include "ShopLayer2.h"
#include "gameboard.h"
#include "UserData.h"
#include "ScoreCounter.h"
#include "consts.h"

NS_CC_BEGIN

GameScene::GameScene()
: _levelIndex( 0 )
, _levelMode( GameMode::_default )
, _layerPause( nullptr )
, _layerShop( nullptr )
, _multiplayerOpponentFinishAction( FinishAction::wait )
, _multiplayerLocalFinishAction( FinishAction::wait )
, _multiplayerSessionId(0)
{
	mlMenuItem::loadHotkeys( "ini/hotkeys.xml" );
	setName( "gameScene" );
}

GameScene::~GameScene()
{
	OnlineConnector::shared().onConnectionChanged.remove( _ID );
	mlMenuItem::unloadHotkeys();
}

bool GameScene::init( int levelIndex, GameMode mode, bool sessionWithBot )
{
	do
	{
		CC_BREAK_IF( !SmartScene::init( nullptr ) );
		_levelIndex = levelIndex;
		_levelMode = mode;
		_sessionWithBot = sessionWithBot;

		load( "ini/gamescene/scene.xml" );

		if( mode == GameMode::multiplayer )
			runMultiplayer();
		else
			runLevel();
#if PC == 1
		auto lostFocusCallback = [this]( EventCustom* ){
			this->openPause( nullptr );
		};
		auto lostFocusListener = EventListenerCustom::create( "lostFocus", lostFocusCallback );
		_eventDispatcher->addEventListenerWithSceneGraphPriority( lostFocusListener, this );
#endif 

		return true;
	}
	while( false );
	return false;
}

void GameScene::clearStack()
{
	SmartScene::clearStack();
	_gameLayers.clear();
	_layerPause.reset( nullptr );
	_layerShop.reset( nullptr );
	_layerStatistic.reset( nullptr );
}

void GameScene::onEnter()
{
	if( _levelMode == GameMode::multiplayer )
	{
		TutorialManager::shared().setEnabled( false );
	}

	SmartScene::onEnter();
#if PC == 1
	MouseHoverScroll::shared().enable();
	KeyboardListener::shared().enable();
#endif
}

void GameScene::onExit()
{
	if( _levelMode == GameMode::multiplayer )
	{
		if( !_sessionWithBot )
		{
			multiplayerOnOpponentActionEnable( false );
			multiplayerReportAction( FinishAction::abort );
		}
		TutorialManager::shared().setEnabled( true );
	}

	SmartScene::onExit();
#if PC == 1
	MouseHoverScroll::shared().disable();
	MouseHoverScroll::shared().disable();
#endif
}

void GameScene::closeLevel()
{
	Director::getInstance()->popScene();
}

void GameScene::restartLevel()
{
	_layerStatistic.reset( nullptr );
	clearStack();
	if( _levelMode == GameMode::multiplayer )
		runMultiplayer();
	else
		runLevel();
}

bool GameScene::tryRestartLevel( Layer * initiator )
{
	auto levelStatistic = dynamic_cast<LevelStatisticLayer*>( initiator );
	auto gamePause = dynamic_cast<GamePauseLayer*>( initiator );
	
	if( _levelMode == GameMode::multiplayer )
	{
		if( _sessionWithBot )
			return false;

		int const price = OnlineConnector::shared().getGameBet();
		int const money = ScoreCounter::shared().getMoney( kScoreCrystals );

		if( money < price )
		{
			if( levelStatistic )
				levelStatistic->onNotEnought( kScoreCrystals );
			else if( gamePause )
				gamePause->onNotEnought( kScoreCrystals );
			return false;
		}

		if( levelStatistic )
			levelStatistic->runEvent( "showtryagain_local" );
		multiplayerReportAction( FinishAction::restart );
		++_multiplayerSessionId;
		return multiplayerTryRestart();
	}
	else
	{
		int scoreType( -1 );
		int cost( 0 );
		LevelParams::shared().getPayForLevel( _levelIndex, _levelMode, scoreType, cost );
		int exist = ScoreCounter::shared().getMoney( scoreType );

		if( cost <= exist )
		{
			restartLevel();
			return true;
		}
		else
		{
			if( levelStatistic )
				levelStatistic->onNotEnought( kScoreFuel );
			else if( gamePause )
				gamePause->onNotEnought( kScoreFuel );
			return false;
		}
	}

	return false;
}

GameLayer* GameScene::getGameLayer()
{
	return _gameLayers.front();
}

void GameScene::openPause( Ref * sender )
{
	if( isCanOpenPause() )
	{
#if PC == 1
		//Added delay to give button sound finish playing.
		auto pauseAllEffects = []()
		{
			AudioEngine::shared().pauseAllEffects();
		};
		auto delay = DelayTime::create( 0.1 );
		auto call = CallFunc::create( std::bind( pauseAllEffects ) );
		auto action = Sequence::createWithTwoActions( delay, call );
		runAction( action );
#else
		AudioEngine::shared().pauseAllEffects();
#endif

		std::string path( "ini/gamescene/pause.xml" );
		if( getLevelMode() == GameMode::multiplayer )
			path = "ini/gamescene/online_pause.xml";

		_layerPause = GamePauseLayer::create( this, path );
		pushLayer( _layerPause, _levelMode != GameMode::multiplayer );

		bool showad = UserData::shared().get<int>( k::user::UnShowAd ) == 0;
		if( showad && Config::shared().get<bool>( "useAds" ) && Config::shared().get<bool>( "showAdInPause" ) )
			showAdOnPause();
	}
}

void GameScene::openShop( Ref*sender, bool gears )
{
	if( isCanOpenShop() )
	{
		_layerShop = gears ?
			ShopLayer::create( false, false, true, true ) :
			ShopLayer::create( false, true, false, true );

		if( _layerShop )
		{
			AudioEngine::shared().pauseAllEffects();
			pushLayer( _layerShop, _levelMode != GameMode::multiplayer );
			TutorialManager::shared().dispatch( "level_openshop" );
		}
	}
}

void GameScene::openShop2( Ref*sender, bool gears )
{
	if( isCanOpenShop() )
	{
		_layerShop = ShopLayer2::create( 2, gears ? 2 : 3 );
		pushLayer( _layerShop, _levelMode != GameMode::multiplayer );
		TutorialManager::shared().dispatch( "level_openshop" );
	}
}

LevelStatisticLayer::Pointer GameScene::openLevelStatistic( LevelStatisticLayer::Result outcome, int gold, int tickets, int costfuel, const FinishLevelParams & params )
{
	setPushMode( SmartScene::PushLayerMode::nonset );

	_layerStatistic = LevelStatisticLayer::create( this, outcome, gold, tickets, costfuel, params );
	

	if( _levelMode == GameMode::multiplayer )
	{
		if( _sessionWithBot )
		{
			auto delay = DelayTime::create( CCRANDOM_0_1() + 1.f );
			auto callfunc = CallFunc::create( std::bind( []( NodeExt*node ){node->runEvent( "multiplayer_bot" ); }, _layerStatistic.ptr() ) );
			auto action = Sequence::create( delay, callfunc, nullptr );
			_layerStatistic->runAction( action );
		}
		else
		{
			multiplayerReportAction( FinishAction::wait );
			multiplayerOnOpponentActionEnable( true );
		}
	}

	return _layerStatistic;
}

void GameScene::openWaitOpponentLayer()
{
	auto wol = WaitOpponentLayer::create();
	wol->setonExitTransitionDidStartCallback( std::bind( &GameScene::onCloseWaitOpponentLayer, this ) );
	if( _sessionWithBot )
		wol->activateBot();
	pushLayer( wol );

	_waitOpponentLayer = wol;
}

void GameScene::closeWaitOpponentLayer()
{
	if( _waitOpponentLayer )
	{
		_waitOpponentLayer->disappearance();
		_waitOpponentLayer = nullptr;
	}
}

void GameScene::onCloseWaitOpponentLayer()
{
	_waitOpponentLayer.reset( nullptr );
	for( auto & gl : _gameLayers )
	{
		GameBoardOnline * const gb = dynamic_cast<GameBoardOnline *>(&gl->getGameBoard());
		if( gb != nullptr )
			gb->startSync( _sessionWithBot );
	}
}

void GameScene::displayName( const std::string& name, bool opponent )
{
	_scoreNode->displayName( name, opponent );
}

void GameScene::connectStatus( bool isConnected )
{
	_gameLayers[0]->getInterface()->connectStatus( isConnected );
}

void GameScene::onLevelFinished( FinishLevelParams params)
{
	if( _levelMode == GameMode::survival )
		LevelParams::shared().onLevelFinishedSurvival( _levelIndex, params.wavesCompleteCount, _levelMode );
	else
		LevelParams::shared().onLevelFinished( _levelIndex, params.stars, _levelMode );
}

void GameScene::showAdOnPause()
{
	AdsPlugin::shared().cacheInterstitial();
	auto showAd = []()
	{
		AdsPlugin::shared().showInterstitialBanner();
	};
	auto delay = DelayTime::create( 0.5 );
	auto call = CallFunc::create( std::bind( showAd ) );
	auto action = Sequence::createWithTwoActions( delay, call );
	this->runAction( action );
}

void GameScene::onLayerPushed( LayerPointer layer )
{
	if( _gameLayers.size() > 0 && _gameLayers[0] && _gameLayers[0]->getInterface() && layer != _gameLayers[0] )
	{
		_gameLayers[0]->getInterface()->setEnabled( false );
	}
	if( _gameLayers.size() > 1 && _gameLayers[1] && _gameLayers[1]->getInterface() && layer != _gameLayers[1] )
	{
		_gameLayers[1]->getInterface()->setEnabled( false );
	}
}

void GameScene::onLayerPoped( LayerPointer layer )
{
	if( _gameLayers.size() > 0 )
		_gameLayers[0]->getInterface()->setEnabled( true );
	if( _gameLayers.size() > 1 )
		_gameLayers[1]->getInterface()->setEnabled( true );

	if( layer == _layerPause )_layerPause = nullptr;
	if( layer == _layerShop )_layerShop = nullptr;
}

bool GameScene::isCanOpenPause()const
{
	return !_layerPause && !_layerShop;
}

bool GameScene::isCanOpenShop()const
{
	return _levelMode == GameMode::multiplayer ? false : !_layerShop;
}

void GameScene::multiplayerReportAction( FinishAction action )
{
	_multiplayerLocalFinishAction = action;

	if( action == FinishAction::abort )
		OnlineConnector::shared().gameFinish( action );
}

void GameScene::multiplayerOnOpponentActionEnable( bool enable )
{
	OnlineConnector & oc = OnlineConnector::shared();

	if( enable )
	{
		oc.onOpponentFinishAction.add( _ID, std::bind( &GameScene::multiplayerOnOpponentAction, this, std::placeholders::_1, std::placeholders::_2 ) );
		oc.startReportingGameFinish( _multiplayerLocalFinishAction );
	}
	else
	{
		OnlineConnector::shared().onOpponentFinishAction.remove( _ID );
		oc.stopReportingGameFinish();
	}
}

void GameScene::multiplayerOnOpponentAction( bool succes, FinishAction action )
{
	if( !succes )
		return;

	if( action == FinishAction::restart && _multiplayerOpponentFinishAction != FinishAction::restart && _layerStatistic )
	{
		_layerStatistic->runEvent( "showtryagain_opponent" );
	}
	else if( action == FinishAction::abort && _multiplayerOpponentFinishAction != FinishAction::abort && _layerStatistic )
	{
		_layerStatistic->runEvent( "multiplayer_opponent_break" );
	}

	_multiplayerOpponentFinishAction = action;
	if( _multiplayerOpponentFinishAction == FinishAction::abort )
		_multiplayerLocalFinishAction = FinishAction::wait;

	multiplayerTryRestart();
}

bool GameScene::multiplayerTryRestart()
{
	if( _multiplayerOpponentFinishAction == FinishAction::restart && _multiplayerLocalFinishAction == FinishAction::restart ) {
		multiplayerOnOpponentActionEnable( false );
		restartLevel();
		return true;
	}

	return false;
}

void GameScene::runLevel()
{
	createGameLayer();
	createScoreNode();
	createInterfaceLocal();
	loadLevel();
    LevelParams::shared().onLevelStarted( _levelIndex, _levelMode );
    getGameLayer()->getInterface()->selectHeroAutomatically();
}

void GameScene::runMultiplayer()
{
	createGameLayerOnline( GameBoardType::local);
	createGameLayerOnline( GameBoardType::opponent );
	createScoreNode();
	createInterfaceLocal();
	createInterfaceOpponent();

	loadLevel();

	_gameLayers[0]->load( "ini/gamescene/online_gamelayer_local.xml" );
	_gameLayers[1]->load( "ini/gamescene/online_gamelayer_opponent.xml" );

	displayName( OnlineConnector::shared().getPlayerNickname(), false );
	openWaitOpponentLayer();

	_multiplayerOpponentFinishAction = _multiplayerLocalFinishAction = FinishAction::wait;
	setPushMode( SmartScene::PushLayerMode::force_nonblock );

	OnlineConnector::shared().onConnectionChanged.add( _ID, std::bind( &GameScene::onConnectionChanged, this, std::placeholders::_1 ) );
    LevelParams::shared().onLevelStarted( _levelIndex, _levelMode );
}

void GameScene::createGameLayer()
{
	auto board = make_intrusive<GameBoard>();
	auto layer = GameLayer::create( this, board );
	board->setLayer( layer );
	_gameLayers.push_back( layer );
	resetMainLayer( layer );
}

void GameScene::createGameLayerOnline( GameBoardType type )
{
	auto board = make_intrusive<GameBoardOnline>( type );
	auto layer = GameLayer::create( this, board );
	board->setLayer( layer );
	board->setSessionID( _multiplayerSessionId );
	if( type == GameBoardType::opponent && _sessionWithBot )
		board->loadBot();
	if( _gameLayers.empty() )
		resetMainLayer( layer );
	else
		pushLayer( layer, false );
	_gameLayers.push_back( layer );
}

void GameScene::createScoreNode()
{
	auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	std::string path = _levelMode == GameMode::multiplayer ?
		"ini/gamescene/online_scorenode.xml" :
		"ini/gamescene/scorenode.xml";
	_scoreNode = ScoreNode::create( _gameLayers.front(), path );
	_scoreNode->setPosition( 0, dessize.height );
	_scoreNode->setVisibleSurvivalScore( _levelMode == GameMode::survival );
	addChild( _scoreNode, 9 );
}

void GameScene::createInterfaceLocal()
{
	auto node = GameInterface::create( _gameLayers[0], this, false );
	std::string path = _levelMode == GameMode::multiplayer ?
		"ini/gamescene/online_interface.xml" :
		"ini/gamescene/interface.xml";
	node->load( path );
	_gameLayers[0]->addChild( node );
	_gameLayers[0]->setInterface( node );
}

void GameScene::createInterfaceOpponent()
{
	auto node = GameInterface::create( _gameLayers[1], this, true );
	_gameLayers[1]->addChild( node );
	_gameLayers[1]->setInterface( node );
}

void GameScene::loadLevel()
{
	for( auto layer : _gameLayers )
	{
		layer->getGameBoard().loadLevel( _levelIndex, _levelMode );
		std::string path = _levelMode == GameMode::multiplayer ?
			"ini/gamescene/online_gamelayer_actions.xml" :
			"ini/gamescene/gamelayer_actions.xml";
		layer->load( path );
		layer->runEvent( "oncreate" );
		layer->getGameBoard().onLoadFinished();
	}
}

void GameScene::onConnectionChanged( bool connection )
{
	std::string eventname = connection ? "on_connected" : "on_disconnected";
	runEvent( eventname );

	for( auto layer : _gameLayers )
	{
		layer->getInterface()->setEnabled( connection );
		layer->getGameBoard().onConnectionChanged( connection );
	}
}

NS_CC_END
