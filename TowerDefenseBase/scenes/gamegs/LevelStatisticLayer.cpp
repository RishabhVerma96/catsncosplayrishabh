#include "playservices/playservices.h"
#include "LevelStatisticLayer.h"
#include "ScoreCounter.h"
#include "GameScene.h"
#include "ShopLayer.h"
#include "ShopLayer2.h"
#include "ScoreLayer.h"
#include "HeroTestDrive.h"
#include "DialogLayer.h"
#include "UserData.h"
#include "consts.h"
#include "FinalLayer.h"

NS_CC_BEGIN


LevelStatisticLayer::LevelStatisticLayer()
: _outcome(Result::Win)
, _useDialog(false)
{
}

LevelStatisticLayer::~LevelStatisticLayer( )
{
	OnlineConnector & oc = OnlineConnector::shared();
	oc.onStatisticLocal.remove( _ID );
	oc.onStatisticOpponent.remove( _ID );
}

bool LevelStatisticLayer::init( GameScene* scene, Result outcome, int gold, int tickets, int costfuel, FinishLevelParams const & params )
{
	do
	{
		_gameScene = scene;
		CC_BREAK_IF( !LayerExt::init() );
		_outcome = outcome;

		GameMode mode = scene->getLevelMode();
		int level = scene->getLevelIndex();

		int starsMax = LevelParams::shared().getMaxStars( 0, mode );

		bool withHeroes = mode == GameMode::multiplayer ?
			true : level >= Config::shared().get<int>( "minLevelHero" );
		withHeroes = withHeroes && Config::shared().get<bool>( "useHero" );

		xmlLoader::macros::set( "awardgold", toStr( gold ) );
		xmlLoader::macros::set( "awardtickets", toStr( tickets ) );
		xmlLoader::macros::set( "costfuel", toStr( costfuel ) );
		xmlLoader::macros::set( "waves_count", toStr( params.wavesCompleteCount ) );
		xmlLoader::macros::set( "zombies_count", toStr( params.creepKillsCount ) );
		xmlLoader::macros::set( "score_count", toStr( params.scores ) );
		xmlLoader::macros::set( "with_heroes", toStr( withHeroes ) );
		xmlLoader::macros::set( "player_name", toStr( OnlineConnector::shared().getPlayerNickname() ) );
		xmlLoader::macros::set( "player_score", toStr( OnlineConnector::shared().getPlayerScore() ) );
		for( int i = 0; i < starsMax; ++i )
			xmlLoader::macros::set( "star" + toStr( i + 1 ) + "_visible", toStr( i < params.stars ) );

		std::string xmlFile; 
		if( mode != GameMode::multiplayer )
		{
			xmlFile =
				outcome == Lose ? "ini/gamescene/defeat2.xml" :
				outcome == Win ? "ini/gamescene/victory2.xml" :
				/*tcome == Neutral*/"ini/gamescene/neutral2.xml";
		}
		else
		{
			xmlFile = outcome == Lose ? 
				"ini/gamescene/online_defeat2.xml" : 
				"ini/gamescene/online_victory2.xml";
		}
		load( xmlFile );
		runEvent( "appearance" );

		if( mode == GameMode::multiplayer )
		{
			_multiplayerLocalWinCount = getNodeByPath<Label>( this, getParamCollection().get( "local_win_count" ) );
			_multiplayerOpponentWinCount = getNodeByPath<Label>( this, getParamCollection().get( "opponent_win_count" ) );

			OnlineConnector & oc = OnlineConnector::shared();
			oc.statisticLocal();
			oc.statisticOpponent();
			oc.onStatisticLocal.add( _ID, std::bind( &LevelStatisticLayer::multiplayerOnStatistic, this, std::placeholders::_1, std::placeholders::_2, true ) );
			oc.onStatisticOpponent.add( _ID, std::bind( &LevelStatisticLayer::multiplayerOnStatistic, this, std::placeholders::_1, std::placeholders::_2, false ) );
		}

		auto scene = Director::getInstance()->getRunningScene();
		NodePointer scores = scene->getChildByName( "scorelayer" );
		if( !scores )
		{
			scores = ScoreLayer::create();
			scene->addChild( scores, 999 );
		}

		return true;
	}
	while( false );
	return false;
}

void LevelStatisticLayer::onNotEnought( int scoreType )
{
	int const shopTab = scoreType == kScoreFuel ? 2 : 1;

	if( _useDialog == false )
	{
		auto shop = ShopLayer2::create( 1, shopTab );
		if( shop )
			getSmartScene()->pushLayer( shop, true );
	}
	else
	{
		DialogLayer::showForShop( "ini/dialogs/nofuel.xml", 1, shopTab );
	}
}

void LevelStatisticLayer::visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	if( getIsUseBlur() )
		LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags );
	else
		LayerExt::visit( renderer, parentTransform, parentFlags );
}

void LevelStatisticLayer::onEnter()
{
	LayerExt::onEnter();
	runAction( Sequence::create( DelayTime::create( 0.5) , CallFunc::create( [this](){
		HeroTestDriveLayer::onLevelStatisticLayerDidShowed( _gameScene, _outcome == Result::Win );
	} ), nullptr ) );
}

bool LevelStatisticLayer::isBlurActive()
{
	return !isRunning();
}

void LevelStatisticLayer::visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	LayerExt::visit( renderer, parentTransform, parentFlags );
}

ccMenuCallback LevelStatisticLayer::get_callback_by_description( const std::string & name )
{
	if( name == "restart" ) return std::bind( &LevelStatisticLayer::restart, this );
	if( name == "close" ) return std::bind( &LevelStatisticLayer::exit, this );
	else if( name == "leaderboard_level" )
	{
		int index = _gameScene->getGameLayer()->getGameBoard().getCurrentLevelIndex();
		return std::bind( [this]( Ref*, int index ){Leaderboard::shared().openLevel( index ); }, std::placeholders::_1, index );
	}
	return LayerExt::get_callback_by_description( name );
}

bool LevelStatisticLayer::setProperty( const std::string & stringproperty, const std::string & value )
{
	if( stringproperty == "usedialog" )
		_useDialog = strTo<bool>( value ) && Config::shared().get<bool>( "useDialogs" );
	else return LayerExt::setProperty( stringproperty, value );
	return true;
}

void LevelStatisticLayer::destroySelf()
{
	auto scene = getSmartScene();
	if( scene )
	{
		auto scores = scene->getChildByName( "scorelayer" );
		if( scores )
			scores->removeFromParent();
		removeFromParent();
	}
}

void LevelStatisticLayer::exit()
{
	if( _outcome == Result::Lose && HeroTestDriveLayer::didLaunched( _gameScene ) )
		return;
	bool needPopScene = true;
	int index = _gameScene->getGameLayer()->getGameBoard().getCurrentLevelIndex();
	int count = UserData::shared().get( kUser_locationsCount, 0 );
	if( _outcome == Result::Win && (index + 1) == count )
	{
		auto layer = FinalLayer::create();
		if( layer )
		{
			SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
			scene->pushLayer( layer, true );
			needPopScene = false;
		}
	}

	if( needPopScene )
		_gameScene->closeLevel();
}

void LevelStatisticLayer::restart()
{
	IntrusivePtr<Node> releaser( this );
	if( _outcome == Result::Lose && HeroTestDriveLayer::didLaunched( _gameScene ) )
	{
		destroySelf();
		return;
	}

	if( _gameScene->tryRestartLevel(this) )
	{
		destroySelf();
	}
}

void LevelStatisticLayer::multiplayerOnStatistic( bool success, OnlineConnector::PlayerInfo const & info, bool local )
{
	if( !success )
		return;

	auto label = local ? _multiplayerLocalWinCount : _multiplayerOpponentWinCount;

	if( label )
		label->setString( toStr( info.score ) );
}


NS_CC_END