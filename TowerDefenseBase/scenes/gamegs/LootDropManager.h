#ifndef __LootDropManaget_h__
#define __LootDropManaget_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "gameboard.h"
#include "Award.h"
#include "UnitUnstoppable.h"
#include "support/MenuItemCooldown.h"
NS_CC_BEGIN

class LootDropManager;

class Loot : public Node, public NodeExt
{
	DECLARE_BUILDER( Loot );
	bool init();
private:
	CC_SYNTHESIZE( bool, _picked, Picked );
};

class LootPicker : public UnitUnstoppable
{
	DECLARE_BUILDER( LootPicker );
	bool init( GameBoard* board, LootDropManager* parent );
public:
	virtual bool checkTargetByRadius( const Node * target )const override;
protected:
	virtual void on_movefinish() override;
private:
	LootDropManager* _parent;
};

class LootDropManager : public Ref
{
	DECLARE_BUILDER( LootDropManager );
	bool init(GameBoard * board);
public:
	virtual void update( float dt );

	Award::Pointer generateAward();
	void dropLoot( Unit::Pointer unit );
	void pickLoot( Loot::Pointer loot );
	bool createPicker(Point pos);
	void onPickerMoveFinish();
protected:
	void addLootObject( NodePointer );
	void removeLootObject( NodePointer );
private:
	std::vector<Award::Pointer> _awards;
	std::vector<float> _probabilities;
	std::vector<NodePointer> _lootObjects;
	GameBoard * _board;
	int _nodropProbability;
	int _fullProbability;
	LootPicker::Pointer _picker;
	bool _pickerMoveFinished;
};

NS_CC_END
#endif