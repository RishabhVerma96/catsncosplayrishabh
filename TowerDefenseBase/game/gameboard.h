//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __GAMEBOARD__
#define __GAMEBOARD__

#include "support/support.h"
#include "ml/pugixml/pugixml.hpp"
#include "ml/Events.h"
#include "WaveGenerator.h"
#include "unit/Unit.h"
#include "TowerPlace.h"
#include "Hero.h"
NS_CC_BEGIN;

struct FinishLevelParams
{
	int crystalscount = 0;
	int scores = 0;
	int spentscores = 0;
	int livecurrent = 0;
	int livetotal = 0;
	int stars = 0;
	int wavesCompleteCount = 0;
	int creepKillsCount = 0;
};

struct EasterEggsParams
{
    bool isDroneTowerUsed = false;
    bool isDesantUsed = false;
    bool isHeroUsed = false;
    bool isMineUsed = false;
    bool isEasterEnemyPassed = false;
    bool isMageTowerUsed = false;
    bool isIceTowerUsed = false;

};

struct SkillParams
{
	float distanceToRoute;

	struct Params
	{
		std::vector<unsigned> count;
		std::vector<float> cooldown;
		std::vector<float> lifetime;
	};
	std::map<std::string, Params> skills;
};

class GameLayer;
class mlBullet;

class GameBoard : public Ref
{
public:
	static std::vector<unsigned> heroesOnLevel;
	ObServer<GameBoard, std::function<void(bool)> > observerLevelFinished;
public:
	GameBoard( );
	~GameBoard();
	void setLayer( GameLayer* layer );
	void loadLevel( int index, GameMode mode );
	void loadLevelParams( pugi::xml_node & node );
	void update( float dt );
	void updateSkills( float dt );
    UnitDesant* getDesant();
    UnitDesant* getDesant2();

    virtual void addLevelScore( int score );
	virtual void subHealth( int score );
	virtual void setStartScore( int score );
	virtual void setStartHealth( int score );
	virtual int getStartHealth();
	virtual void setHealth( int score );
	virtual bool updateUnitSkills( Unit* unit, float dt );
	virtual void moveHero( Hero* hero, const Point& position );
    void moveDesant( UnitDesant* desant, const Point& position );

	virtual void onLoadFinished() {}

	void clear();

	static void loadRoute( Route & route, pugi::xml_node & node, float offset);
	static void loadRoutes( std::map<int, TripleRoute> & routes, const pugi::xml_node & node );
	static void loadTowerPlaces( std::vector<TowerPlaseDef> & places, const pugi::xml_node & node );
	static void loadEvents( std::map<std::string, EventsList> & eventsMap, const pugi::xml_node & node );
	static void loadUnits( std::list< std::pair<float, Unit::Pointer> > & units, const pugi::xml_node & node, const std::map<int, TripleRoute> & routes );

	void startGame();
	void dispatchLeaderboards();

	//void onDamage( mlObject * target, mlObject * damager, float rate );
	virtual void onPredelayWave( const WaveInfo & wave, float delay );
	virtual void onStartWave( const WaveInfo & wave );
	virtual void onFinishWave();
	virtual void onFinishWaves();
	virtual void onFinishGame( bool victory );
	virtual bool isGameStarted();
	virtual void onDamage( Unit* damager, Unit*target, float damage );
	virtual void onKill( Unit* damager, Unit*target );

	void pauseWaves();
	void resumeWaves();

	bool isTowerPlace( Point & location );

	Unit::Pointer createCreep( const std::string & name, const Route & route, const Point & position );
	Unit::Pointer createCreep( const std::string & name, RouteSubType rst, unsigned routeIndex );

	Unit::Pointer createTower( const std::string & name, bool spentScore = true );
	Unit::Pointer createTower( const std::string & name, const Point& position, bool spentScore = true );
	Unit::Pointer createTower( const std::string & name, TowerPlace* place, bool spentScore = true );
	Unit::Pointer createActiveSkillUnit( const std::string & name, const Point & position, float lifetime = 0.f, int count = 1, int skillDirection = -1 ); // -1 means left direction

	Unit::Pointer createBonusItem( const Point & position, const std::string & name );
	void addUnit( Unit::Pointer tower );
	Unit::Pointer upgradeTower( Unit::Pointer tower, bool ignoreMaxUserLevel, bool spentScore = true );
	void removeTower( Unit::Pointer tower, bool spentScore = true, bool dead = false );
	Unit* getTowerInLocation( const Point& location );

	const std::vector<Hero::Pointer>& getHeroes();

	void remove( Unit::Pointer creep );
	void preDeath( Unit::Pointer creep );
	void death( Unit::Pointer creep );

	bool checkWaveFinished();
	virtual bool checkGameFinished();

	void activateTowerPlace( TowerPlace::Pointer place );

	size_t getRoutesCount()const { return _creepsRoutes.size(); }
	const Route getRandomRoute( UnitLayer::type layer, unsigned& index, RouteSubType& type ) const;
	const TripleRoute getRoute( UnitLayer::type layer, const Point & position, float & distance, bool onlyCreepsRoutes = false )const;

	void getTargetsByRadius( Unit* requester, std::vector<Unit*> & out, const Point & center, float radius );
	void applyDamageBySector( Unit* base );
	virtual void createHeroes();
	void runEvent( const std::string& eventname );
	virtual int getScoreForUnit( Unit::Pointer unit );
	virtual void writeGameScore( int score );
    
    inline const std::string& getPeaceSound() { return _peaceSound; }
    inline const std::string& getBattleSound() { return _battleSound; }
    

	WaveGenerator& getWaveGenerator() { return _waveGenerator; }
	const WaveGenerator& getWaveGenerator()const { return _waveGenerator; }
protected:
	void createPreloadedUnits( float dt );
	Hero::Pointer createHero( int index, const Point& position, int level, const std::vector<unsigned>& skills );
    UnitDesant::Pointer createDesant(const Point& position, int type);
    Unit::Pointer buildTower( const std::string & name, int level, Unit* unit );
	Unit::Pointer buildCreep( const std::string & name );
	virtual bool isDeathUnit(Unit* unit);
	virtual void dispatchDeathUnits();
	void refreshTargetsForTowers(float dt);
	bool checkAvailableTarget( const Unit* target, const Unit* base )const;
	bool checkTargetByArea( const Unit* target )const;
	bool checkTargetByUnitType( const Unit* target, const Unit* base )const;
	bool checkTargetByUnitLayer( const Unit* target, const Unit* base )const;
	bool checkTargetByRadius( const Unit* target, const Point & center, float radius )const;
	bool checkTargetBySector( const Unit* target, const  Unit* base )const;
    bool checkTargetByDeadZone( const Unit* target, const Unit* base) const;
    bool checkByLineOfSight( const Unit* target, const Unit* base )const;
	void dispatchDamagers();
	void dispatchKillers();
public:
	void finishGame();
protected:
	void lockUnits();
	void unlockUnits();
public:
	void event_towerBuild( TowerPlace::Pointer place, Unit::Pointer unit );
	void event_towerUpgrade( Unit::Pointer unit );
	void event_towerSell( Unit::Pointer unit );
	void event_levelFinished();
	void event_startwave( int index );
    void stopAllUnitsSounds();
    
	virtual void onConnectionChanged( bool connection ) {}
	virtual void online_onGameStarted() {}
	virtual void online_onGameFinished( bool victory ) {}
	virtual void online_onWaveWasStarted() {}
	virtual void online_onCreepWasCreated( Unit* unit ) {}
	virtual void online_onTowerWasCreated( Unit* unit, TowerPlace* place ) {}
	virtual void online_onTowerWasUpgraded( Unit* unit ) {}
	virtual void online_onTowerWasSelled( Unit* unit ) {}
	virtual void online_onSkillWasUsed( Unit* unit, const Point& position ) {}
	virtual void online_onBonusItemWasUsed( const std::string& name, const Point& position ) {}
protected:
	int _unitsLocked;
	std::map<int, std::vector<Unit::Pointer>> _unitsAdd;
	std::map<int, std::vector<Unit::Pointer>> _units;
	std::vector<Unit::Pointer> _death;
	std::list<Unit::Pointer> _reserve;
	std::vector<Hero::Pointer> _heroes;

	//std::map<Unit::Pointer, float> _damagers;
	//std::map<Unit::Pointer, std::list<Unit::Pointer>> _killers;
	CC_SYNTHESIZE_PASS_BY_REF( std::vector<TripleRoute>, _creepsRoutes, CreepsRoutes );

	FinishLevelParams _statisticsParams;
    EasterEggsParams _easterParams;
	CC_SYNTHESIZE_READONLY( GameMode , _gameMode, GameMode);
	CC_SYNTHESIZE_READONLY( unsigned, _levelIndex, CurrentLevelIndex );
	CC_SYNTHESIZE_READONLY_PASS_BY_REF( SkillParams, _skillParams, SkillParams );
protected:
	CC_SYNTHESIZE( bool, _waitAfterLose, WaitAfterLose );
	CC_SYNTHESIZE_READONLY( GameLayer*, _gameLayer, GameLayer );
	WaveGenerator _waveGenerator;
	bool _isGameStarted;
	bool _isFinihedWaves;
	bool _isFinihedGame;
	bool _isFinishedWave;
	int _heartsForStar1;
	int _heartsForStar2;
	int _heartsForStar3;
	time_t _levelStartTime;
    std::string _peaceSound;
    std::string _battleSound;
    UnitDesant* _desant;
    UnitDesant* _desant2;
//    std::list< std::pair< Unit::Pointer, float > > _desants;
	std::map<std::string, EventsList> _events;
	std::list<std::pair<float, Unit::Pointer>> _preloadedUnit;
	Size _areaSize;
	int _startHealth;
	float _refreshTargetsCounter;
	float _refreshTargetsPeriod;
};

bool isExistCreep( const std::vector<Unit::Pointer> & units );

NS_CC_END;

#endif
