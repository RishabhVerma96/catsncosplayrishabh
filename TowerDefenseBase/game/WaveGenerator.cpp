//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "WaveGenerator.h"
#include "gameboard.h"
#include "Log.h"
#include "support.h"
NS_CC_BEGIN;

/*
samples:
<waves defaultdelayonewave="5">
    <wave index="0">
      <creep name="tank" delay="1" healthrate="1" score="10"/>
      <creep name="car" delay="0.5" healthrate="1" score="10"/>
      <creep name="car" delay="0.5" healthrate="1" score="10"/>
      <creep name="car" delay="0.5" healthrate="1" score="10"/>
      <creep name="car" delay="0.5" healthrate="1" score="10"/>
    </wave> 
    <wave index="0" defaultdelay="2.9" defaulthealthrate="1" defaultscore="10" defaultname="rotorplane0" count="5" />
</waves>
	
	*/

WaveGenerator::WaveGenerator( GameBoard* board )
: _board(*board)
, _waveIndex()
, _wavesCount()
, _waves()
, _currentWave()
, _delayUnits()
, _delayWaves()
, _delayWavesNow(true)
, _isRunning(true)
, _unitsRate(1.f)
, _unitsRateInc(0.f)
, _infinityModeWaveStretch(1.5f)
, totalNumberOfEnemies(0)
, totalRemainingNumberOfEnemies(0)
{
    
}

void WaveGenerator::load( const pugi::xml_node & node, bool infinityMode )
{
	_infinityMode = infinityMode;

	float defaultdelayonewave = node.attribute( "defaultdelayonewave" ).as_float();
	//_delayWaves.set( node.attribute("defaultdelayonewave").as_float() );
	_unitsRate = node.attribute( "rate" ).as_float(1.f);
	_unitsRateInc = node.attribute( "rateinc" ).as_float( 0.f ) + 1.f;
	_infinityModeWaveStretch = node.attribute( "infinitymodewavestrech" ).as_float( _infinityModeWaveStretch );

	int index(0);
    totalNumberOfEnemies = 0;
    
    FOR_EACHXML_BYTAG(node, waveXml, "wave")
	{
		auto dev = waveXml.attribute( "dev" ).as_bool( false );
		if( dev && isTestModeActive() == false )
			continue;

		_waves.push_back(WaveInfo());
		WaveInfo & wave = _waves.back();
		wave.index = index++;

        wave.soundOnWave = waveXml.attribute( "sound" ).as_string();
		wave.type = strToUnitLayer( waveXml.attribute("routetype").as_string() );
		wave.delayOneWave = waveXml.attribute( "delayonewave" ).as_float();
		if( wave.delayOneWave == 0 )
			wave.delayOneWave = defaultdelayonewave;
		std::string dname = waveXml.attribute("defaultname").as_string();
		float dhealthRate = waveXml.attribute("defaulthealthrate").as_float();
		float dscore = waveXml.attribute("defaultscore").as_float();
		float ddelay = waveXml.attribute("defaultdelay").as_float();
		RouteSubType droutest = strToRouteSubType( waveXml.attribute("defaultroutesubtype").as_string() );
		int drouteindex = waveXml.attribute("defaultrouteindex").as_int();
		int count = waveXml.attribute("count").as_int();
        
		if ( count != 0 )
		{
			assert(dname.empty() == false );
			assert(dhealthRate != 0 );
			assert(dscore != 0 );
			assert(ddelay != 0 );
			while( count-- )
			{
				wave.creeps.push_back(dname);
				wave.healthRate.push_back(dhealthRate);
				wave.scores.push_back(dscore);
				wave.delayOneUnit.push_back(ddelay);
				wave.routeIndex.push_back( drouteindex );
				wave.routeSubType.push_back( droutest );
			}
		}
		else
		{
			FOR_EACHXML(waveXml, creep)
			{
				std::string name = creep.attribute("name").as_string();
				float healthRate = creep.attribute("healthrate").as_float();
				float score = creep.attribute("score").as_int();
				float delay = creep.attribute("delay").as_float();
				RouteSubType routest = strToRouteSubType( creep.attribute("routesubtype").value() );
				int routeindex = creep.attribute("routeindex").as_int();
				if ( name.empty() ) name = dname;
				if ( healthRate == 0 ) healthRate = dhealthRate;
				if ( score == 0 ) score = dscore;
				if ( delay == 0 ) delay = ddelay;
				if ( routest == RouteSubType::defaultvalue ) routest = droutest;
				if ( routeindex == 0 ) routeindex = drouteindex;
				wave.creeps.push_back(name);
				wave.healthRate.push_back(healthRate);
				wave.scores.push_back(score);
				wave.delayOneUnit.push_back(delay);
				wave.routeIndex.push_back( routeindex );
				wave.routeSubType.push_back( routest );
                totalNumberOfEnemies++;
			}
		}
	}
    
    WaveInfo &wave = _waves.front();
    totalRemainingNumberOfEnemies = wave.creeps.size();
    _observerEnemiesCounter.pushevent(totalRemainingNumberOfEnemies,totalNumberOfEnemies);
    
}

void WaveGenerator::clear()
{
	_waveIndex = 0;
	_wavesCount = 0;

	_waves.clear();
	_currentWave = _waves.end();
	
	_delayUnits.set(0);
	_delayWaves.set(0);
	
	_delayWavesNow = false;

	_unitsRate = 1.f;
}

void WaveGenerator::start()
{
	_delayUnits.reset();
	_delayWaves.reset();
	_currentWave = _waves.end();
	
	_delayUnits.set( 0 );

	_waveIndex = 0;
	_wavesCount = _waves.size();
	_delayWavesNow = false;
	_observerWaveCounter.pushevent( _waveIndex, _wavesCount );
	resume();
}

void WaveGenerator::pause()
{
	_isRunning = false;
}
void WaveGenerator::resume()
{
	_isRunning = true;
}

void WaveGenerator::update(float dt)
{
	if( _isRunning == false )
		return;

	if ( _currentWave != _waves.end() && _currentWave->valid() == false )
	{
		_waves.pop_front();
		_currentWave = _waves.end();
		onFinishWave();
	}
	else
	if ( _currentWave != _waves.end() && _currentWave->valid() )
	{
		_delayUnits.tick( dt );
		if( _delayUnits )
		{
			generateCreep();
			if( _currentWave->valid() )
			{
				float delay = _currentWave->delayOneUnit.front();
				_delayUnits.set( delay );
			}
			else
			{
				_delayUnits.reset();
			}
		}
	}
	else
	{
		if( _delayWavesNow == false )
		{
			_delayWavesNow = true;
			if ( _waves.empty() == false )
			{
				_delayWaves.set( _waves.front().delayOneWave );
				onPredelayWave( _waves.front( ) );
			}
		}

		else
		{
			_delayWaves.reset();
			_delayWavesNow = false;
			if ( _waves.empty() == false )
			{
				_currentWave = _waves.begin();
				assert( _currentWave->valid() );
				onStartWave( *_currentWave );
				float delay = _currentWave->delayOneUnit.front();
				_delayUnits.set( delay );
			}
		}
	}
}

void WaveGenerator::onPredelayWave( const WaveInfo & wave )
{
	_observerWaveCounter.pushevent( _waveIndex, _wavesCount );
	_board.onPredelayWave( wave, (_waveIndex != 0 ? _delayWaves.value() : 0) );
}

void WaveGenerator::onStartWave( const WaveInfo & wave )
{
	if( isInfinityMode() )
		++_waveIndex;
	else
		_waveIndex = std::min<unsigned>( _waveIndex + 1, _wavesCount );
	_board.onStartWave( wave );
	_observerWaveCounter.pushevent( _waveIndex, _wavesCount );
    
    totalRemainingNumberOfEnemies = wave.creeps.size();
    _observerEnemiesCounter.pushevent(totalRemainingNumberOfEnemies,totalNumberOfEnemies);
}

void WaveGenerator::onFinishWave()
{
	_unitsRate = _unitsRate * _unitsRateInc;
	//TODO: refactoring this block.
	if( _infinityMode && _waves.size() == 2 )
	{
		WaveInfo & oldWave = _waves.front();
		WaveInfo & curWave = _waves.back();
		WaveInfo newWave = curWave;

		assert( !oldWave.creeps.empty() );

		float const stretchFactor = oldWave.creeps.empty()
			? _infinityModeWaveStretch
			: std::max( static_cast<float>(curWave.creeps.size()) / static_cast<float>(oldWave.creeps.size()), 1.1f );

		float const probability = stretchFactor - 1.0f;

		auto crIt = newWave.creeps.begin();
		auto doIt = newWave.delayOneUnit.begin();
		auto scIt = newWave.scores.begin();
		auto rtIt = newWave.routeSubType.begin();
		auto riIt = newWave.routeIndex.begin();
		auto hrIt = newWave.healthRate.begin();
		for( ;
			crIt != newWave.creeps.end() &&
			doIt != newWave.delayOneUnit.end() &&
			scIt != newWave.scores.end() &&
			rtIt != newWave.routeSubType.end() &&
			riIt != newWave.routeIndex.end() &&
			hrIt != newWave.healthRate.end();
			++crIt,
			++doIt,
			++scIt,
			++rtIt,
			++riIt,
			++hrIt			 
		) {
			if( rand_0_1() < probability ) {
				newWave.creeps.insert( crIt, *crIt );
				newWave.delayOneUnit.insert( doIt, *doIt );
				newWave.scores.insert( scIt, *scIt );
				newWave.routeSubType.insert( rtIt, *rtIt );
				newWave.routeIndex.insert( riIt, *riIt );
				newWave.healthRate.insert( hrIt, *hrIt );
			}
		}
		_waves.push_back( newWave );
	}

	_board.onFinishWave();

	if ( _waves.empty() )
		onFinishAllWaves();
}

void WaveGenerator::onFinishAllWaves()
{
	_board.onFinishWaves();
}

bool WaveGenerator::isInfinityMode() const
{
	return _infinityMode;
}

void WaveGenerator::generateCreep()
{
	assert( _currentWave != _waves.end() );
	assert(_currentWave->creeps.empty() == false );
	std::string name = _currentWave->creeps.front();
	auto rst = _currentWave->routeSubType.front();
	auto ri = _currentWave->routeIndex.front();
	auto creep = _board.createCreep(name, rst, ri);
	if ( creep )
	{
		unsigned cost = _currentWave->scores.front();
		float rate = _unitsRate * _currentWave->healthRate.front();
		creep->setCost( cost );
		creep->setRate( rate );
	}
	_currentWave->pop();
    totalRemainingNumberOfEnemies--;
    
    _observerEnemiesCounter.pushevent(totalRemainingNumberOfEnemies,totalNumberOfEnemies);
    CCLOG("*********** %d/%d Enemies ***********",totalRemainingNumberOfEnemies,totalNumberOfEnemies);
}


NS_CC_END;
