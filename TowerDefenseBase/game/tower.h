//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __TOWER__
#define __TOWER__

#include "ml/pugixml/pugixml.hpp"
#include "support.h"

NS_CC_BEGIN;

class mlCreep;

class mlTowersInfo : public Singlton<mlTowersInfo>
{
public:
	mlTowersInfo();

	bool isExist( const std::string& towerName );
	void fetch( std::list<std::string> & towers )const;
	
	unsigned getCost( const std::string & name, unsigned level )const;
	unsigned getCostLab( const std::string & name, unsigned level )const;
	unsigned getSellCost( const std::string & name, unsigned level )const;


	template < template <class T, class = std::allocator<T> > class CONTAINER >
	void fetch_playable_tower_names( CONTAINER<std::string> & result ) {
		for( auto towerInfo : m_towersInfo )
			if( towerInfo.second.playable )
				result.push_back( towerInfo.first );
	}


	bool get_playable( const std::string & name, bool fromSavedData = false) const;
	bool set_playable( const std::string & name, bool value ); ///< return true if setted
	std::string get_playable_tower_name( int num ) const;
	int get_playable_count() const;
	int get_playable_count_max() const;
	std::string get_inapp_id( const std::string & name ) const;
	int get_coin_price( const std::string & name ) const;
	int get_max_level (const std::string & name ) const;
	bool get_purchased( const std::string & name ) const;
	void set_purchased( const std::string & towerName ) const;
    void set_purchased_by_inapp( const std::string & inappId ) const;
	int get_dmg( const std::string & name, unsigned level )const;
	int get_rng( const std::string & name, unsigned level )const;
	int get_spd( const std::string & name, unsigned level )const;
	int get_dmg_forlab( const std::string & name )const;
	int get_rng_forlab( const std::string & name )const;
	int get_spd_forlab( const std::string & name )const;
	std::string get_desc( const std::string & name, unsigned level )const;

	float radiusInPixels( const std::string & name, unsigned level )const;
	int getCostFotDig( )const { return _digcost; }
	
	//float rate( const std::string & tower );
	void checkAvailabledTowers()const;
protected:
	void load();
protected:
	struct towerInfo
	{
		bool playable;
		float sellRate;
		unsigned minlevel;
		unsigned maxlevel;
		std::string inappId;
		int coinPrice;
		std::vector<unsigned>cost;
		std::vector<unsigned>costlab;
		std::vector<float> dmg;
		std::vector<float> rng;
		std::vector<float> spd;
		std::string desc;
		int order;
		struct
		{
			float dmg;
			float rng;
			float spd;
		}lab;
	};
	unsigned _digcost;
	std::map<std::string, towerInfo> m_towersInfo;
	std::set<std::string> m_playablesDefault;
	std::vector< float > _labUpgrade;

	float _max_dmg;
	float _max_rng;
	float _max_spd;
};


class mlUnitInfo : public Singlton < mlUnitInfo >
{
public:
	struct Info
	{
		UnitLayer::type layer;
		UnitType type;
		float radius;
		float damage;
		float health;
		float armor;
		float velocity;
	};
	bool isUnitExist( const std::string & name );
	const Info& info( const std::string & name );
protected:
	void fetch( const std::string & name );
private:
	std::map<std::string, Info > _info;
};

NS_CC_END;
#endif
