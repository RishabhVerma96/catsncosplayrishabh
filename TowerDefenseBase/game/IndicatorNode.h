//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __INDICATOR_NODE__
#define __INDICATOR_NODE__
#include "cocos2d.h"
#include "ml/macroses.h"
NS_CC_BEGIN;

class IndicatorNode : public Node
{
	DECLARE_BUILDER( IndicatorNode );
	bool init();
public:
	void setProgress(float progress); //0..1;
	void displayHP( float hp );
protected:
	Sprite * _bg;
	Sprite * _progressNode;
	IntrusivePtr<Label> _hpLabel;
	float _hp;
};

NS_CC_END;
#endif