#include "UnitHomebase.h"
#include "ml/common.h"
#include "gameboard.h"
#include "support/AutoPlayer.h"
NS_CC_BEGIN

UnitHomebase::UnitHomebase()
: _gameboardStartHealth( 0 )
, _gameboardCurrentHealth( 0 )
, _autotest( false )
, _destroyed(false)
{}

UnitHomebase::~UnitHomebase()
{}

bool UnitHomebase::init( GameBoard* board, const std::string & path, const std::string & xmlFile )
{
	auto player = AutoPlayer::getInstance();
	if( player )
	{
		_autotest = true;
	}
	return Unit::init( board, path, xmlFile );
}

void UnitHomebase::setGameBoard( GameBoard * board )
{
	Unit::setGameBoard( board );
	board->setWaitAfterLose( true );
	_gameboardStartHealth = board->getStartHealth();
	_gameboardCurrentHealth = _gameboardStartHealth;

}

void UnitHomebase::setCurrentHealth( float value )
{
	if( _autotest || _destroyed ) return;
	Unit::setCurrentHealth( value );
	auto percentage = value / _health;
	auto health = (int)( ceilf( percentage * _gameboardStartHealth ) );
	
	if( health != _gameboardCurrentHealth )
	{
		_gameboardCurrentHealth = health;
		_board->setHealth( health );
	}
}

void UnitHomebase::on_die()
{
	Unit::on_die();
	_currentHealth = 99999.f;
	_board->setHealth( 0 );
	_destroyed = true;
}

void UnitHomebase::on_die_finish()
{
	_board->setWaitAfterLose( false );
	runEvent( "on_die_finish" );
}

NS_CC_END