#ifndef __UnitUnstoppable_h__
#define __UnitUnstoppable_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "Unit.h"
NS_CC_BEGIN

class UnitUnstoppable : public Unit
{
	DECLARE_BUILDER( UnitUnstoppable );
	bool init( GameBoard* board, const std::string & path, const std::string & xmlFile );
public:
	void update( float dt );
	virtual void moveByRoute( const Route & route ) override;
    virtual void moveInLine( float angle, float startDelay ) override;
protected:
	virtual void on_move() override;
	virtual void move_update( float dt ) override;
	virtual void on_movefinish() override;
private:
	bool _moveStarted;
};

NS_CC_END
#endif
