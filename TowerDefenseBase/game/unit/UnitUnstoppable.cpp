#include "UnitUnstoppable.h"
NS_CC_BEGIN

UnitUnstoppable::UnitUnstoppable()
: _moveStarted( false )
{}

UnitUnstoppable::~UnitUnstoppable()
{}

bool UnitUnstoppable::init( GameBoard* board, const std::string & path, const std::string & xmlFile )
{
	return Unit::init( board, path, xmlFile );
}

void UnitUnstoppable::on_move()
{	
	_moveStarted = true;
	Unit::on_move();
}

void UnitUnstoppable::move_update( float dt )
{
	//Must be empty.
}

void UnitUnstoppable::on_movefinish()
{
	_moveStarted = false;
	Unit::on_movefinish();
}

void UnitUnstoppable::update( float dt )
{
	if (_moveStarted && getMoveFinished() == false) {
		getMover().update( dt );
	}
	Unit::update( dt );
}

void UnitUnstoppable::moveByRoute( const Route & route )
{
	Unit::moveByRoute( route );
	move();
}

void UnitUnstoppable::moveInLine( float angle, float startDelay )
{
    Unit::moveInLine( angle, startDelay );
}

NS_CC_END
