//
//  Skills.cpp
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 26.11.14.
//
//

#include "ActiveSkills.h"
#include "Unit.h"
#include "gameboard.h"
#include "consts.h"
#include "support/support.h"
#include "tower.h"

NS_CC_BEGIN;
/******************************************************************************/
//MARK: UnitActiveSkill
/******************************************************************************/
UnitActiveSkill::UnitActiveSkill()
	: _timer( 0 )
	, _timerDuration( 0 )
	, _eventsOnExecute()
	, _eventsOnStop()
	, _stopedUnit( false )
	, _resumeMoving( false )
	, _waitExecution( true )
	, _stopDuration( 0 )
	, _count( -1 )
	, _execution( false )
	, _melee( false )
{}

UnitActiveSkill::~UnitActiveSkill()
{}


bool UnitActiveSkill::init( const pugi::xml_node & xmlNode, Unit* unit )
{
	UnitSkill::init( xmlNode, unit );

	_name = xmlNode.attribute( "skillname" ).as_string();
	_unitName = xmlNode.attribute( "name" ).as_string();
	_melee = xmlNode.attribute( "melee" ).as_bool( false );
	_stopedUnit = xmlNode.attribute( "stopunit" ).as_bool( true );
	_stopDuration = xmlNode.attribute( "stopduration" ).as_float( 0.f );
	_waitExecution = xmlNode.attribute( "waitexecution" ).as_bool( true );
	_count = xmlNode.attribute( "count" ).as_int( -1 );
	_offset = strTo<Point>( xmlNode.attribute( "offset" ).as_string() );

	auto states = xmlNode.attribute( "onlystates" ).as_string();
	std::list<std::string> ss;
	split( ss, states );
	_allowStatesForExecute.insert( ss.begin(), ss.end() );

	auto xmlEvents = xmlNode.child( "eventlist" );
	auto xmlEventsStop = xmlNode.child( "eventlistonstop" );
	for (auto xmlEvent : xmlEvents)
	{
		auto event = xmlLoader::load_event( xmlEvent );
		if (event)
			_eventsOnExecute.push_back( event );
	}
	for (auto xmlEvent : xmlEventsStop)
	{
		auto event = xmlLoader::load_event( xmlEvent );
		if (event)
			_eventsOnStop.push_back( event );
	}
	return true;
}

void UnitActiveSkill::update( float dt, Unit* context )
{
	bool allow( true );
	if (_allowStatesForExecute.empty() == false)
	{
		auto state = context->current_state().get_string_name();
		auto iter = _allowStatesForExecute.find( state );
		allow = (iter != _allowStatesForExecute.end());
	}

	if (execution() == false || !allow)
		return;


	if (_stopedUnit)
	{
		_timer += dt;
		if (_timerDuration < 0.001f)
		{
			execute( context );
		}
		if (_timerDuration >= _stopDuration)
		{
			stop( context );
			_timer = 0;
			_timerDuration = 0;
		}
		else
		{
			_timerDuration += dt;
		}
	}
	else
	{
		_timer += dt;
		execute( context );
		stop( context );
	}
}


void UnitActiveSkill::execute( Unit* context )
{
	if (_count > 0)
	{
		--_count;

		auto hero = dynamic_cast<Hero*>(context);
		if (hero)
		{
			hero->skillExecution( true );
			
			if (_stopedUnit)
			{
				hero->stop();
				hero->moveTo( hero->getPosition() );
			}
		}

		_eventsOnExecute.execute( context );
		
		int direction = -1;
		if( _melee )
		{			
			_location = context->getPosition();
			float angle = hero->getMover().getCurrentAngle();
			if( angle > 90.f && angle < 270.f ){
				direction = -1;
			}
			else {
				direction = 1;
			}
			_location = _location + _offset * direction;
		}

		auto& board = *context->getGameBoard();
		auto& m_skillParams = board.getSkillParams();
		auto name = getName();

		std::vector<unsigned> skills;
		HeroExp::shared().skills( hero->getName(), skills );
		auto level = getNeedUnitSkillLevel();
		float lifetime( 0.f );
		int count( 1 );
		if( m_skillParams.skills.find( name ) != m_skillParams.skills.end() )
		{
			count = board.getSkillParams().skills.at( name ).count[level];
			lifetime = m_skillParams.skills.at( name ).lifetime[level];
		}
		board.createActiveSkillUnit( _unitName, _location, lifetime, count, direction );
	}
}

void UnitActiveSkill::stop( Unit* context )
{
	_eventsOnStop.execute( context );
	_execution = false;

	auto hero = dynamic_cast<Hero*>(context);
	if (hero) hero->skillExecution( false );
}

bool UnitActiveSkill::startExecution( Vec2 location )
{
	_location = location;
	const auto& board = *getUnit()->getGameBoard();
	auto& m_skillParams = board.getSkillParams();
	auto distance( 0.f );
	const mlUnitInfo::Info& info = mlUnitInfo::shared().info( _name );
	auto unitlayer = info.layer;

	if (checkPointOnRoute( board.getCreepsRoutes(), _location, m_skillParams.distanceToRoute, unitlayer, &distance ) || _melee)
		_execution = true;
	else
		_execution = false;

	return _execution;
}

bool UnitActiveSkill::execution()
{
	return _execution;
}

NS_CC_END;
