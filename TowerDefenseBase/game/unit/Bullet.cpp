/****************************************************************************
Copyright (c) 2014-2015 Vladimir Tolmachev (Volodar)

This file is part of game Greeco Defense

Greeco Defense is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Greeco Defense is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Greeco Defense.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#include "Bullet.h"
NS_CC_BEGIN


Bullet::Bullet()
: _steering( false )
, _isStuck( false )
, _missileThurstTime(1)
, _trajectory( Trajectory::line )
, _triggerRadius( -1.f )
, _turnOnAppear( false )
, _delay(0)
{
	_parabolicParams.H = 0;
	_parabolicParams.timer = 0;
	_parabolicParams.duration = CCRANDOM_MINUS1_1() * 0.2f + 0.5f;
}

Bullet::~Bullet( )
{
}

bool Bullet::init( GameBoard* board, const std::string& path, Unit::Pointer base, Unit::Pointer target, float startAngle, Point startPosition )
{
	do
	{
		CC_BREAK_IF( !Unit::init( board, "", path ) );
		_base = base;
		_target = target;

		setDamageBySector( base->getDamageBySector() );
		setAllowTargets( base->getAllowTargets() );
		setType( UnitType::skill );

		auto part = _target->getParamCollection().get( _bopyPart, "" );
		auto rand = _target->getParamCollection().get( "random_bullet", "" );
		_targetPointOffset = strTo<Point>( part );
		if( rand.empty() == false )
		{
			auto point = strTo<Point>( rand );
			_targetPointOffset.x += CCRANDOM_MINUS1_1() * point.x / 2;
			_targetPointOffset.y += CCRANDOM_MINUS1_1() * point.y / 2;
		}

		_startPoint = startPosition;
		updateTargetPoint();
		prepare();
    
		setPosition( startPosition );
		setRotation( startAngle );
		if( _turnOnAppear )
			updateAngle( _targetPoint );
		float z = _unitLayer == UnitLayer::sky ? 9000 : -startPosition.y;
		setLocalZOrder( z + _additionalZorder );
		return true;
	}
	while( false );
	return false;
}

bool Bullet::setProperty( const std::string & stringproperty, const std::string & value )
{
	if( stringproperty == "trajectory" )
	{
		if( value == "line" )
			_trajectory = Trajectory::line;
		else if( value == "parabolic" )
			_trajectory = Trajectory::parabolic;
        else if( value == "missile" )
            _trajectory = Trajectory::missile;
        else
			assert( 0 );
	}
	else if( stringproperty == "parabolicheight" )
		_parabolicParams.H = strTo<float>( value );
	else if( stringproperty == "steering" )
		_steering = strTo<bool>( value );
	else if( stringproperty == "bodypart" )
		_bopyPart = value;
	else if( stringproperty == "parabolic_duration" )
		_parabolicParams.duration = CCRANDOM_MINUS1_1() * 0.2f + strTo<float>( value );
	else if( stringproperty == "stuck" )
		_isStuck = strTo<bool>( value );
	else if( stringproperty == "trigger_radius" )
		_triggerRadius = strTo<float>( value );
	else if( stringproperty == "turn_on_appear" )
		_turnOnAppear = strTo<bool>( value );
	else 
		return Unit::setProperty( stringproperty, value );
	return true;
}

void Bullet::update( float dt )
{
	Unit::update( dt );
}

void Bullet::clear()
{
	_base.reset( nullptr );
	_target.reset( nullptr );
}

void Bullet::readyfire_update( float dt )
{
	Unit::readyfire_update( dt );

	Point pos = computePosition( dt );
	if( current_state().get_name() != state_death )
	{
		turn( pos );
		setPosition( pos );
		float z = _unitLayer == UnitLayer::sky ? 9000 : -pos.y;
		setLocalZOrder( z + _additionalZorder );
	}
}

void Bullet::on_die()
{
	if( _base && _target )
	{
		Point p0 = getPosition();
		Point p1 = _target->getPosition() + strTo<Point>( _target->getParamCollection().get( _bopyPart, "" ) );
		float distance = p0.getDistance( p1 );
		float radius = getRadius();

		getEffect().copyFrom( _base->getEffect() );
		bool applyDamage( false );
		applyDamage = applyDamage || (distance < radius);
		applyDamage = applyDamage || getDamageBySector();
		if( applyDamage )
		{
			setType( _base->getType() );
			applyDamageToTarget( _target );
		}
        Unit::on_die();
        
        if( _isStuck )
        {
            auto newParent = _target->getChildByName( "bullet_node" );
            if( !newParent )newParent = _target;
            
            auto pos = getPosition();
            pos = convertToWorldSpace( Point::ZERO );
            pos = newParent->convertToNodeSpace( pos );
            setPosition( pos );
            retain();
            removeFromParentAndCleanup( false );
            newParent->addChild( this, -10 );
            release();
        }
        
        _base.reset( nullptr );
        _target.reset( nullptr );
	}
}

void Bullet::updateAngle(const Point& newPos)
{
	Point p0 = getPosition();
	Point p1 = newPos;
	float angle = getDirectionByVector( p1 - p0 );
	setRotation( angle );
}

void Bullet::turn( const Point& newPos )
{
	if( _target )
	{
		switch( _trajectory )
		{
			case Trajectory::line:
			{
				updateAngle( newPos );
				break;
			}
			case Trajectory::parabolic:
			{
				updateAngle( newPos );
				break;
			}
            case Trajectory::missile:
            {
                break;
            }
            default:
				assert( 0 );
		}
	}
}

void Bullet::prepare()
{
	switch( _trajectory )
	{
		case Trajectory::line:
			break;
		case Trajectory::parabolic:
			if( _parabolicParams.H == 0 )
				_parabolicParams.H = 200;
			_parabolicParams.timer = 0;
			break;
        case Trajectory::missile:
            _missileThurstTime = 0.8 + (arc4random()%30)/100.0f;
            _isLooping = false;
            _shouldChase = false;
            _velocity = getMover().getVelocity() * (1+(arc4random()%100)/600);
            break;

	}
}

Point Bullet::updateTargetPoint()
{
	if( _targetPoint.equals( Point::ZERO ) || _steering )
	{
        _targetPoint = _target->getPosition() + _targetPointOffset;
	}
	return _targetPoint;
}

Point Bullet::computePosition( float dt )
{
	updateTargetPoint();

	switch( _trajectory )
	{
		case Trajectory::line:
			return computePositionLine( dt );
		case Trajectory::parabolic:
			return computePositionParabolic( dt );
        case Trajectory::missile:
            return computePositionMissile( dt );

        default:
			assert( 0 );
	}
	return getPosition();
}

Point Bullet::computePositionMissile( float dt )
{
    Point p0 = getPosition();

    _delay-=dt;
    if (_delay<=0)
    {
    
        _missileThurstTime-=dt;
        Point p1 = _targetPoint;
        float nodeCurrentAngle = getCurrentAngle(this);

        if (_missileThurstTime <= 0)
        {
            float rotationRate = _velocity*0.4;

            if (_shouldChase)
            {
                float angleNodeToRotateTo = getAngleOfTwoVectors(p0, p1);
                float diffAngle = getAngleDifference(angleNodeToRotateTo, nodeCurrentAngle);
                
                if (abs(diffAngle) > 7)
                {
                    if (diffAngle<0)
                        rotationRate *= -1;
                    
                    this->setRotation(nodeCurrentAngle + rotationRate*dt);
                }

            }
            else {
                if(!_isLooping)
                {
                    _isLooping = true;
                    float rotateTime = 0.8 + arc4random()%30/100.0f;
                    float animRotation = 50+arc4random()%100;
                    if(nodeCurrentAngle <90 && nodeCurrentAngle>-90)
                    {
                        animRotation*=-1;
                    }
                    runAction(Sequence::create(RotateBy::create(rotateTime, animRotation),CallFunc::create([this]{
                        _shouldChase = true;
                    }), NULL));
                }
            }
            
        }
        else
        {
            float animRotation = arc4random()% int(_velocity*0.1);

            if(nodeCurrentAngle <90 && nodeCurrentAngle>-90)
            {
                animRotation*=-1;
            }

            this->setRotation(nodeCurrentAngle + animRotation*dt);

        }
        float a = -CC_DEGREES_TO_RADIANS(getCurrentAngle(this));
        Vec2 velocityVector(cosf( a ), sinf( a ));
        velocityVector.normalize();
        Point p = p0 + velocityVector*dt*_velocity;
    //    setPosition( p );

        
        auto radius = _triggerRadius < 0.f ? getRadius() : _triggerRadius;
        if( checkRadiusByEllipse( p, _targetPoint, radius ) )
        {
            setPosition( _targetPoint );
            push_event( event_die );
            process();
        }
        return p;
    }
    return p0;
}


Point Bullet::computePositionLine( float dt )
{
	Point p0 = getPosition();
	Point p1 = _targetPoint;
	float velocity = getMover().getVelocity();
	float t = dt*velocity;
	t = std::min( t, (p1 - p0).getLength() );
	Point p = p0 + (p1 - p0).getNormalized() * t;

	auto radius = _triggerRadius < 0.f ? getRadius() : _triggerRadius;
	if( checkRadiusByEllipse( p, _targetPoint, radius ) )
	{
		setPosition( p );
		push_event( event_die );
		process();
	}

	return p;
}

Point Bullet::computePositionParabolic( float dt )
{
	/* t = [0;1] */
	auto parabolic = [this]( float t )
    { return -(2 * t - 1)*(2 * t - 1) + 1; };
	auto line = [this]( float t )
	{ return _startPoint + (_targetPoint - _startPoint) * t; };

    Point p0 = _startPoint;
    Point p1 = _targetPoint;
    float velocity = getMover().getVelocity();
    Point centerPoint = (_startPoint + (_targetPoint-_startPoint) / 2) + Vec2(0, _parabolicParams.H);
    float duration1 = p0.getDistance(centerPoint) / velocity;
    float duration2 = centerPoint.getDistance(p1) / velocity;
    _parabolicParams.duration = duration1 + duration2;
    
	_parabolicParams.timer += dt;
	float t = _parabolicParams.timer / _parabolicParams.duration; // 1 - duration;
    t = std::min( t, 1.f );
	Point pos = line( t );
    pos.y += parabolic( t ) * _parabolicParams.H;

	if( t >= 1.f )
	{
		setPosition( pos );
		push_event( event_die );
		process();
	}

	return pos;
}



float Bullet::getCurrentAngle(cocos2d::Node* node){
    float rotAng = node->getRotation();
    if(rotAng >= 180.f)
        rotAng -= 360.f;
    else if (rotAng < -180.f)
        rotAng +=360.f;
    return rotAng;
}
float Bullet::getAngleDifference(float angle1, float angle2){
    float diffAngle = (angle1 - angle2);
    if(diffAngle >= 180.f)
        diffAngle -= 360.f;
    else if (diffAngle < -180.f)
        diffAngle +=360.f;
    return diffAngle;
}

float Bullet::getAngleOfTwoVectors(cocos2d::Point vec1, cocos2d::Point vec2){
    auto vectorFromVec1ToVec2 = vec2 - vec1;
    return CC_RADIANS_TO_DEGREES(-vectorFromVec1ToVec2.getAngle());
}





NS_CC_END
