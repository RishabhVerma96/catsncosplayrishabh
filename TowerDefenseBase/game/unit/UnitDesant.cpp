#include "UnitDesant.h"
#include "ml/common.h"
#include "gameboard.h"

NS_CC_BEGIN





UnitDesant::UnitDesant()
: _handRadius( 60 )
, _handRadiusSector(30)
{
    _dieTimer= 0;
    _regeneration = 0;
    _extraRegenerationValue = 0;
    _extraRegenerationTime = 0;

}

UnitDesant::~UnitDesant()
{}

bool UnitDesant::moveTo( const Point & position )
{
    if (getType() == UnitType::hero) {
        CCLOG("move to called for hero");
    }
    
    Route myRoute;
    if( myRoute.empty() ) findOneRoute( position, myRoute );
    if( myRoute.empty() ) findTwoRoute( position, myRoute );
    
    if( myRoute.empty() == false )
    {
        finalizateRoute( position, myRoute );
        setBasePosition( position );
        getMover().setRoute( myRoute );
        if (!_isFirstRun && this->getPosition() != position) {
            runEvent("on_reposition");
        }
        move();
        _isFirstRun = false;
    }
    
    return myRoute.empty() == false;
}

void UnitDesant::checkRoute( const TripleRoute & tripleroute, const Point & A, const Point & B, Route & route, bool fromStartingPoint )
{
    float maxDistToRoute( 50 );
    float distance( -1 );
    int multiplier = fromStartingPoint ? 4 : 1;
    
    bool checkSelf = checkPointOnRoute( A, tripleroute, maxDistToRoute * multiplier, &distance );
    bool checkTarget = checkPointOnRoute( B, tripleroute, maxDistToRoute, &distance );
    if( checkSelf && checkTarget )
    {
        size_t i0 = -1;
        size_t i1 = -1;
        float min_0( 9999 );
        float min_1( 9999 );
        auto & r = tripleroute.main;
        for( size_t i = 0; i < r.size(); ++i )
        {
            float d0 = A.getDistance( r[i] );
            float d1 = B.getDistance( r[i] );
            if( d0 < min_0 )
            {
                min_0 = d0;
                i0 = i;
            }
            if( d1 < min_1 )
            {
                min_1 = d1;
                i1 = i;
            }
        }
        assert( i0 != -1 );
        assert( i1 != -1 );
        size_t step = i1 > i0 ? 1 : -1;
        for( size_t i = i0; i != i1; i += step )
        {
            route.push_back( r[i] );
        }
        if( i0 == i1 )
        {
            route.push_back( r[i0] );
        }
    }
}

void  UnitDesant::findOneRoute( const Point & position, Route & out )
{
    const std::vector<TripleRoute>& routes = _board->getCreepsRoutes();
    
    for( auto &route : routes )
    {
        checkRoute( route, getPosition(), position, out );
        CC_BREAK_IF( !out.empty() );
    }
}

void UnitDesant::findTwoRoute( const Point & position, Route & route )
{
    std::vector<TripleRoute> atroutes = _board->getCreepsRoutes();
    std::vector<TripleRoute> troutes;
    
    Route temp;
    for( size_t i = 0; i < atroutes.size(); ++i )
    {
        temp.clear();
        checkRoute( atroutes[i], getPosition(), atroutes[i].main[0], temp );
        if( temp.empty() == false )
            troutes.push_back( atroutes[i] );
    }
    for( size_t i = 0; i < atroutes.size(); ++i )
    {
        temp.clear();
        checkRoute( atroutes[i], position, atroutes[i].main[0], temp, false );
        if( temp.empty() == false )
            troutes.push_back( atroutes[i] );
    }
    if( troutes.empty() ) return;
    
    std::vector< Route > routes;
    
    for( size_t i = 0; i < troutes.size(); ++i )
    {
        for( auto point : troutes[i].main )
        {
            temp.clear();
            checkRoute( troutes[i], getPosition(), point, temp );
            size_t size = temp.size();
            if( size == 0 )
                continue;
            temp.push_back( point );
            routes.push_back( temp );
        }
    }
    
    for( size_t i = 0; i < routes.size(); )
    {
        bool path = false;
        for( auto troute : troutes )
        {
            temp.clear();
            checkRoute( troute, routes[i].back(), position, temp, false );
            size_t size = temp.size();
            if( size != 0 )
            {
                routes[i].insert( routes[i].end(), temp.begin(), temp.end() );
                path = true;
            }
        }
        if( path == false )
        {
            routes.erase( routes.begin() + i );
        }
        else
        {
            ++i;
        }
    }
    
    std::sort( routes.begin(), routes.end(),
              []( const Route & L, const Route & R )
              {
                  auto length = []( const Route & route )
                  {
                      float l( 0 );
                      for( unsigned i = 1; i < route.size(); ++i )
                      {
                          l += route[i - 1].getDistanceSq( route[i] );
                      }
                      return l;
                  };
                  return length( L ) < length( R );
              } );
    
    if( routes.empty() == false )
    {
        route = routes.front();
    }
}

void UnitDesant::finalizateRoute( const Point & position, Route & route )
{
    route.push_back( position );
    while( route.size() >= 2 )
    {
        Vec2 r1 = route[0] - getPosition();
        Vec2 r2 = route[1] - route[0];
        float angle = getAngle( r1, r2 );
        if( std::fabs( angle ) > 90 )
        {
            route.erase( route.begin() );
        }
        else
        {
            break;
        }
    }
    route.insert( route.begin(), getPosition() );
}


bool UnitDesant::init( GameBoard* board, const std::string & path, const std::string & xmlFile )
{
    MachineExt::add_event( Event::liveDesant ).set_string_name( "liveDesant" );

	return Unit::init( board, path, xmlFile );
}

bool UnitDesant::checkTargetByRadius( const Node * target )const
{
	assert( target );
	Point a = _basePosition;
	Point b = target->getPosition( );
	return  checkRadiusByEllipse( a, b, _radius );
}

void UnitDesant::capture_targets( const std::vector<Unit::Pointer> & targets )
{
	for( size_t i = 0; i < _targets.size(); )
	{
		auto& target = _targets[i];
		bool release = false;
		release = release || target->getMoveFinished();
		release = release || target->getCurrentHealth() <= 0;
		release = release || std::find( targets.begin(), targets.end(), target ) == targets.end();
		if( release )
			_targets.erase( _targets.begin() + i );
		else
			++i;
	}
	
	if( _targets.size() < _maxTargets )
	{
		for( auto target : targets )
		{
			auto iter = std::find( _targets.begin(), _targets.end(), target );
			if( iter == _targets.end() )
			{
				_targets.push_back( target );
			}
		}
	}
	for( auto target : _targets )
	{
		Point a = getPosition();
		Point b = target->getPosition();
		bool byradius = checkRadiusByEllipse( a, b, target->getRadius() );
		if( byradius )
			target->stop();
	}
	
	if( _targets.empty() == false )
	{
		if( current_state( ).get_name( ) != state_move &&
			isNearestTarget() == false )
		{
			Route route;
			buildRouteToTarget( route );
			getMover().setRoute( route );
			move();
		}
		else
		{
			std::vector<Unit::Pointer> t;
			t.push_back( _targets.front() );
			Unit::capture_targets( t );
		}
	}
	else
	{
		Unit::capture_targets( _targets );
		if( isNearestBase() == false )
		{
			if( current_state().get_name() != state_move )
			{
                if (getType() != UnitType::hero) {
                    Route route;
                    buildRouteToBase( route );
                    getMover().setRoute( route );
                    move();
                }
				
			}
		}
	}
}

bool UnitDesant::setProperty( const std::string & stringproperty, const std::string & value )
{
    if( stringproperty == "regeneration" )
        _regeneration = strTo<float>( value );
	else if( stringproperty == "handradius" )
		_handRadius = strTo<float>( value );
    else if( stringproperty == "desantType" )
        _desantType = strTo<int>( value );
    else if( stringproperty == "desantHeal" )
        _desantHeal = strTo<int>( value );
    else if( stringproperty == "desantHealRadius" )
        _desantHealRadius = strTo<int>( value );
	else if( stringproperty == "handradiussector" )
		_handRadiusSector = strTo<float>( value );
	else
		return Unit::setProperty( stringproperty, value );
	return true;
}

void UnitDesant::die_update( float dt )
{
    _dieTimer += dt;
    float dlife = (getDefaultHealth() * getRate()) / MachineUnit::_death.duration;
    float health = dlife * _dieTimer + getCurrentHealth();
    
    observerHealth.unlock();
    setCurrentHealth( health );
    observerHealth.lock();
    setCurrentHealth( 0 );
}

void UnitDesant::on_die()
{
    _dieTimer = 0;
    observerHealth.lock();
	Unit::on_die();
	for( auto target : _targets )
	{
		target->capture_targets(std::vector<Unit::Pointer>());
		target->move();
	}
}

void UnitDesant::on_die_finish()
{
    NodeExt::runEvent( "on_die_finish" );
    setCurrentHealth( getDefaultHealth() * getRate() );
    MachineExt::push_event( Event::liveDesant );
    observerHealth.unlock();
}

void UnitDesant::stop_update( float dt )
{
    float health = getCurrentHealth();
    health = std::min( getDefaultHealth() * getRate(), health + _regeneration * dt );
    setCurrentHealth( health );
}


//void UnitDesant::on_mover( const Point & position, const Vec2 & direction )
//{
//	if( current_state().get_name() != State::state_readyfire )
//		Unit::on_mover( position, direction );
//}

void UnitDesant::buildRouteToBase( std::vector<Point> & route )
{
	route.resize( 2 );
	route[0] = getPosition();
	route[1] = _basePosition;
}

void UnitDesant::buildRouteToTarget( std::vector<Point> & route )
{
	auto target = _targets.empty() ? nullptr : _targets.front();
	if( !target )
		return;

	Point radius = target->getPosition() - getPosition();

	Point add = Point( (_handRadius - 1) * (radius.x < 0 ? 1 : -1), 0 );


	Point a = getPosition();
	Point b = target->getPosition( );
	b += add;

	route.resize( 2 );
	route[0] = a;
	route[1] = b;
}

bool UnitDesant::isNearestTarget()
{
	auto target = _targets.empty() ? nullptr : _targets.front();
	if( !target )
		return false;
	bool result;
	Vec2 radius = getPosition() - target->getPosition();
	float dist = radius.length();
	result = dist <= _handRadius;

	if( result )
	{
		float angle = getDirectionByVector( radius );
		while( angle < 0 ) angle += 360;
		
		bool r = false;
		r = r || (angle <= 0 + _handRadiusSector || angle >= 360 - _handRadiusSector);
		r = r || (angle <= 180 + _handRadiusSector || angle >= 180 - _handRadiusSector);;
		result = result && r;
	}

	return result;
}

void UnitDesant::updateHealth(int amount)
{
    
    
    float currentHealth = getCurrentHealth();
    
    float health = std::min( getDefaultHealth() * getRate(), currentHealth+amount );

    setCurrentHealth(health);
}


bool UnitDesant::isNearestBase( )
{
	bool result = getPosition().getDistance( _basePosition ) < 10;
	return result;
}

int UnitDesant::getDesantType()
{
    return _desantType;
}

NS_CC_END
