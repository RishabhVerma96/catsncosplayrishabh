#ifndef __Hero_h__
#define __Hero_h__
#include "cocos2d.h"
#include "macroses.h"
#include "UnitDesant.h"
#include "ml/Singlton.h"
#include "inapp/purchase.h"
#include "ActiveSkills.h"
NS_CC_BEGIN



class HeroExp : public Singlton<HeroExp>
{
public:
	bool isHeroAvailabled( const std::string& name )const;
	bool isHeroAsInapp( const std::string& name );
	unsigned getLevelForUnlockHero( const std::string& name );
    unsigned getRequiredVideoCountToUnlockHero( const std::string& name );

	void heroBought( const std::string& name )const;
	inapp::SkuDetails getHeroSkuDetails( const std::string& name );

	void  setEXP( const std::string & name, float exp, bool dispatchAchievements = true );
	float getEXP( const std::string & name );

	float getLevel( float exp )const;
	float getExpOnLevelFinished( unsigned level )const;
	float getHeroLevelExtValue( unsigned level )const;
	float getCostLevelup( unsigned level )const;
	unsigned getMaxLevel()const;

	void skills( const std::string& name, std::vector<unsigned> & skills );
	unsigned skillPoints( const std::string& name );
	void setSkills( const std::string& name, const std::vector<unsigned> & skills );

	void checkUnlockedHeroes();
	unsigned getNonPurchasableHeroesCount();

	std::string getHeroOnTestDrive();
	void popHeroOnTestDrive();

	int getCostResurrection();

	virtual void onCreate() override;
protected:
private:
	std::vector<float> _heroLevels;
	std::vector<float> _levelAwards;
	std::vector<unsigned> _levelCosts;
	std::map<std::string, unsigned > _levelsForUnclockHeroes;
    std::map<std::string, unsigned > _videoCountToUnclockHeroes;
    std::map<std::string, std::vector<unsigned> > _skills;
	std::map<std::string, inapp::SkuDetails > _heroInappDetails;
	std::deque<std::string> _heroesOnTestDrive;
	int _resurrectionCost;
};

class Hero : public UnitDesant
{
	typedef ObServer< Hero, std::function<void( bool )> > Observer_OnChangeExecution;

	DECLARE_BUILDER( Hero );
	bool init( GameBoard* board = nullptr, const std::string & path = "", const std::string & xmlFile = "" );
public:
	void initSkills( const std::vector<unsigned>& predefinedSkills, int level = -1 );
	
	void update( float dt );

	void removeActiveSkill( UnitActiveSkill::Pointer skill );
	std::vector<UnitActiveSkill::Pointer>& getActiveSkills();
	const std::vector<UnitActiveSkill::Pointer>& getActiveSkills()const;
	void skillExecution( bool );

	Observer_OnChangeExecution& observerOnChangeExecution() { return _observerOnChangeExecution; }
protected:
	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;
	virtual void on_die() override;
    virtual void on_die_finish() override;

	virtual bool loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode ) override;
	virtual void loadXmlActiveSkills( const pugi::xml_node & xmlnode );
	virtual UnitActiveSkill::Pointer loadXmlActiveSkill( const pugi::xml_node & xmlnode );
	virtual void applyExtraRegenaration( float dt );
private:
	CC_SYNTHESIZE_READONLY_PASS_BY_REF( std::string, _skill, Skill );

	std::vector<UnitActiveSkill::Pointer> _activeSkills;
	Observer_OnChangeExecution _observerOnChangeExecution;
};


NS_CC_END
#endif // #ifndef Hero
