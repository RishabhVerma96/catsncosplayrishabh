//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "Unit.h"
#include "consts.h"
#include "GameLayer.h"
#include "tower.h"
#include "UserData.h"
#include "ml/Audio/AudioEngine.h"
#include "Bullet.h"
#include "Linetrap.h"
#include "ml/RandomNumberGenerator.h"
#include "EasterEggsHandler.h"
#include "Tutorial.h"

NS_CC_BEGIN

Unit::Unit()
: _board( nullptr )
, _effect( this )
, _extra()
, _mover()
, _angle( -1 )
, _turnOnAttack( true )
, _targets()
, _currentDamager( nullptr )
, _healthIndicator( nullptr )
, _currentHealth( 1 )
, _soundMoveID( -1 )
, _effectOnShoot()
, _level( 1 )
, _maxLevel( 1 )
, _maxLevelForLevel( -1 )
, _cost( 0 )
, _radius( 0 )
, _placeSound(std::string(""))
, _rate( 1 )
, _bodyType( BodyType::equipment )
, _defaultHealth( 1 )
, _health( 1 )
, _deadZone(Point(0,0))
, _showHealthIndicator( true )
, _moveFinished( false )
, _damageBySector( false )
, _damageBySectorAngle( 0 )
, _unitLayer( UnitLayer::earth )
, _allowTargets( UnitLayer::any )
, _maxTargets( 1 )
, _type()
, _soundMove()
, _lifecost( 0 )
, _additionalZorder( 0 )
, _damageShield( 1 )
, _damageRate( 1 )
, _exp( 0 )
, _id( 0 )
, _isComputeDamage( true )
, _currentHealthState( 0 )
, _extraTargetType( UnitType::none )
, _isIdleAnimationRunning(false)
, _droneSoundKey(-1)
, _shouldRunIdleAnimation(false)
, _isFirstRun(true)
, _isCriticalStrike(false)
, _handRadiusSector2(-1)
{
	static int id( 0 );
	_id = ++id;
}

Unit::~Unit()
{
    stopAllLoopedSounds();
}

bool Unit::init( GameBoard* board, const std::string & path, const std::string & xmlFile, Unit* upgradedUnit )
{
	
	if( init( board, path, xmlFile ) == false )
		return false;

	if( upgradedUnit )
	{
		auto state = MachineUnit::current_state().get_name();
		if( state != MachineUnit::current_state().get_name() )
			MachineUnit::start( state );
		MachineUnit::_timer = upgradedUnit->MachineUnit::_timer;
		MachineUnit::_fireReady.charge_volume =
			upgradedUnit->MachineUnit::_fireReady.charge_volume;
	}
    
    auto towerName = getName();
    if (towerName == "tower_fire" )
    {
        board->_easterParams.isDroneTowerUsed = true;
    }
    if (towerName == "tower_ice" )
    {
        board->_easterParams.isIceTowerUsed = true;
    }

    if(towerName == "tower_gun")
    {
        board->_easterParams.isMageTowerUsed = true;
    }

    
    Node* dronePlaceholder = (Node*) getChildByName("dronePlaceholder");

    if (dronePlaceholder)
    {
        Sprite* droneSprite = (Sprite*) getChildByName("drone");
        droneSprite->setGlobalZOrder(9999);

        int randomHeight = arc4random()%60;

        droneSprite->setPosition(droneSprite->getPosition()+Vec2(0,randomHeight));
        dronePlaceholder->setPosition(dronePlaceholder->getPosition()+Vec2(0,randomHeight));
        
        int randomX = 30+arc4random()%30;
        int randomY = 40 + arc4random()%50;
        
        if (arc4random()%2 ==0)
        {
            randomX*=-1;
        }


        Vec2 basePostion = dronePlaceholder->getPosition() ;
        _dronePoints.push_back(Point(0,0)+basePostion) ;
        _dronePoints.push_back(Point(-randomX,0)+basePostion) ;
        _dronePoints.push_back(Point(-randomX,randomY)+basePostion) ;
//        _dronePoints.push_back(Point(0,30)+basePostion) ;
        _dronePoints.push_back(Point(randomX,randomY)+basePostion) ;
        _dronePoints.push_back(Point(randomX,0)+basePostion) ;
       
        _droneCurrentIndex = 0;
        _droneShouldFollowEnemy = false;
        _droneCurrentPosture = 0;
        
//        cocos2d::Point _minPoint;
//        cocos2d::Point _maxPoint;
       
        for(auto point : _dronePoints)
        {
            if (point.x<_minPoint.x)
            {
                _minPoint.x = point.x;
            }
            if (point.x>_maxPoint.x)
            {
                _maxPoint.x = point.x;
            }
            if (point.y<_minPoint.y)
            {
                _minPoint.y = point.y;
            }
            if (point.y>_maxPoint.y)
            {
                _maxPoint.y = point.y;
            }
        }
		
        _droneSoundKey = AudioEngine::shared().playEffect( kSoundDrone, true);
		
    }

	return true;
}

bool Unit::init( GameBoard* board, const std::string & path, const std::string & xmlFile )
{
	do
	{
		_board = board;
		CC_BREAK_IF( !NodeExt::init() );
		CC_BREAK_IF( !MachineUnit::init() );
		//CC_BREAK_IF( !MachineMove::init() );

		_healthIndicator = IndicatorNode::create();
		_healthIndicator->setVisible( false );
		addChild( _healthIndicator );

		NodeExt::load( path, xmlFile );
        
        int level = UserData::shared().tower_upgradeLevel( getName() );
		setMaxLevelForLevel( level );

		auto cb = std::bind( &Unit::on_mover, this,
							std::placeholders::_1,
							std::placeholders::_2 );
		_mover.setOnChangePosition( cb );
		_mover.setOnFinish( std::bind( &Unit::on_movefinish, this ) );
        
        runEvent( "runIdleAnimation" );
        scheduleOnce(schedule_selector(Unit::runIdleAnimation), 0.6);
        _isIdleAnimationRunning = true;
		return true;
	}
	while( false );
	return false;
}

void Unit::load( pugi::xml_node & root )
{
	NodeExt::load( root );

	start( current_state().get_name() );
}

bool Unit::loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )
{
	if( tag == k::xmlTag::MachineUnit )
	{
		MachineUnit::load( xmlnode );
	}
	else if( tag == k::xmlTag::Effects )
	{
		_effect.load( xmlnode );
        if (getName() == "hero1")
        {
            bool isHero1SpeedEasterEggUnlocked = EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kHero1DoubleSpeed);
            if (isHero1SpeedEasterEggUnlocked)
            {
                _effect.positive.damage *= 2;
                _effect.positive.armor *= 2;
            }
            
        }
	}
	else if( tag == k::xmlTag::Mover )
	{
		_mover.load( xmlnode );
	}
	else if( tag == k::xmlTag::ExtraProperties )
	{
		_extra._electroPosition = strTo<Point>( xmlnode.attribute( "electro_pos" ).as_string() );
		_extra._electroSize = xmlnode.attribute( "electro_size" ).as_string();
		_extra._electroScale = xmlnode.attribute( "electro_scale" ).as_float( 1 );
		_extra._firePosition = strTo<Point>( xmlnode.attribute( "fire_pos" ).as_string() );
		_extra._fireScale = xmlnode.attribute( "fire_scale" ).as_float( 1 );
		_extra._freezingPosition = strTo<Point>( xmlnode.attribute( "freezing_pos" ).as_string() );
		_extra._freezingScale = xmlnode.attribute( "freezing_scale" ).as_float( 0.5 );
	}
	else if( tag == k::xmlTag::UnitSkills )
	{
		loadXmlSkills( xmlnode );
	}
	else
	{
		return NodeExt::loadXmlEntity( tag, xmlnode );
	}
	return true;
}

UnitSkill::Pointer Unit::loadXmlSkill( const pugi::xml_node & xmlnode )
{
	std::string type = xmlnode.name();
	UnitSkill::Pointer skill;

	if( type == "medic" )
		skill = UnitSkillMedic::create( xmlnode, this );
	else if( type == "runeventsbytime" )
		skill = UnitSkillRunTasksByTime::create( xmlnode, this );
	else if( type == "skillcounter" )
		skill = UnitSkillCounter::create( xmlnode, this );
	else if( type == "rateparameter" )
		skill = UnitSkillRateParameter::create( xmlnode, this );
	else if( type == "sniper" )
		skill = UnitSniper::create( xmlnode, this );

	return skill;
}

void Unit::loadXmlSkills( const pugi::xml_node & xmlnode )
{
	for( auto xml : xmlnode )
	{
		auto skill = loadXmlSkill( xml );
		if( skill )
			_skills.push_back( skill );
	}
}

bool Unit::setProperty( const std::string & name, const std::string & value )
{
	bool result = true;
	//setName( root.attribute( "name" ).as_string( "unnamed" ) );

	if( name == "radius" )
	{
		setRadius( strTo<float>( value ) );
	}
	else if( name == "tower_place_sound" ) {
		setPlaceSound(value);
		playTowerPlaceSound();
		CCLOG("play sound :: %s",value.c_str());
	}
	else if( name == "health" )
	{
		_currentHealth =
		_defaultHealth =
		_health = strTo<float>( value );
        if (getName() == "hero1")
        {
            bool isHero1SpeedEasterEggUnlocked = EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kHero1DoubleSpeed);
            if (isHero1SpeedEasterEggUnlocked)
            {
                _currentHealth *= 2;
                _defaultHealth *= 2;
                _health *= 2;
            }
        }
	}
    else if( name == "handradiussector2" )
    {
        _handRadiusSector2 = strTo<float>( value );
    }
    else if( name == "velocity" )
	{
		float velocity = strTo<float>( value );
		//float random = CCRANDOM_MINUS1_1() * 0.1f + 1;
		//velocity *= random;
        
        if (getName() == "hero1")
        {
            bool isHero1SpeedEasterEggUnlocked = EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kHero1DoubleSpeed);
            if (isHero1SpeedEasterEggUnlocked)
            {
                velocity*=1;
            }
            
        }
        
		_mover.setVelocity( velocity );
		_mover.setDefaultVelocity( velocity );
	}
    else if( name == "isFlappy" )
    {
        bool isFlappy = strTo<bool>( value );
        _mover.setIsFlappy(isFlappy);
    }
    else if( name == "jumpVelocity" )
    {
        float jumpV = strTo<float>( value );
        _mover.setJumpVelocity(jumpV);
        _mover.setCurrentVelocity(jumpV);
    }
    else if( name == "unittype" )
	{
		_type = strToUnitType( value );
	}
    else if( name == "effect_on_shoot" )
	{
		_effectOnShoot = value;
	}
	else if( name == "maxlevel" )
	{
		_maxLevel = strTo<int>( value );
	}
	else if( name == "damagebysector" )
	{
		_damageBySector = strTo<bool>( value );
	}
	else if( name == "sectorangle" )
	{
		_damageBySectorAngle = strTo<float>( value );
	}
	else if( name == "maxtargets" )
	{
		_maxTargets = strTo<int>( value );
	}
	else if( name == "unitlayer" )
	{
		_unitLayer = value.empty() ? UnitLayer::any : strToUnitLayer( value );
	}
	else if( name == "sound_onmove" )
	{
		_soundMove = xmlLoader::macros::parse( value );
	}
	else if( name == "lifecost" )
	{
		_lifecost = strTo<int>( value );
	}
	else if( name == "allowtargets" )
	{
		std::list<std::string> targets;
		split( targets, value );
		_allowTargets = 0;
		for( auto target : targets )
		{
			auto layer = strToUnitLayer( target );
			_allowTargets = _allowTargets | layer;
		}
	}
    else if( name == "deadzone" )
    {
        _deadZone = strToPoint( value );
    }
	else if( name == "additionalzorder" )
	{
		_additionalZorder = strTo<int>( value );
	}
	else if( name == "exp" )
	{
		_exp = strTo<float>( value );
	}
	else if( name == "bullet" )
	{
		_bulletXml = value;
	}
	else if( name == "bullet_params" )
	{
		//45,30,0x0:	135:150,0x0:	225,210,0x0:	315,330,0x0
		std::list<std::string> params;
		split( params, value, '|' );
		for( auto param : params )
		{
			std::vector<std::string> args;
			split( args, param, ',' );
			BulletParams bp;
			bp.byangle = strTo<int>( args[0] );
			bp.useangle = strTo<int>( args[1] );
			bp.offset = strTo<Point>( args[2] );
			_bulletParams[bp.byangle] = bp;
		}
	}
	else if( name == "linetrap" )
	{
		_linetrapXml = value;
	}
	else if( name == "linetrap_segment_size" )
	{
		_linetrapSegmentSize = strTo<float>( value );

	}
	else if (name == "velocityRate")
	{
		float _tmp = strTo<float>(value);
		_effect.positive.velocityRate = _tmp;
	}
	else if (name == "velocityTime")
	{
		_effect.positive.velocityTime = strTo<float>(value);
	}
	else if (name == "damage")
	{
		_effect.positive.damage = strTo<float>(value);
	}
    else if( name == "allowmenu" )
	{
		auto& params = getParamCollection();
		params["showmenu"] = value;
	}
	else if( name == "turnonattack" )
	{
		_turnOnAttack = strTo<bool>( value );
	}
	else if( name == "healthstates" )
	{
		std::list<std::string> states;
		split( states, value );
		for( auto state : states )
		{
			auto health = strTo<float>( state );
			_healthStates.push_back( health );
		}
		std::sort( _healthStates.begin(), _healthStates.end(), std::greater<float>() );
	}
	else if( name == "extraTargetType" )
	{
		_extraTargetType = strToUnitType( value );
	}
	else
	{
		result = NodeExt::setProperty( name, value );
	}
	return result;
}

ccMenuCallback Unit::get_callback_by_description( const std::string & name )
{
	if( name.find( "push_event:" ) == 0 )
	{
		auto len = std::string( "push_event:" ).size();
		std::string eventname = name.substr( len );
		auto& event = FiniteState::Machine::event( eventname );

		auto callback = std::bind( [this, event]( Ref* ){
			push_event( event.get_name() );
		}, std::placeholders::_1 );
		return callback;
	}
	else return NodeExt::get_callback_by_description( name );
}

mlEffect& Unit::getEffect() { return _effect; }
const mlEffect& Unit::getEffect()const { return _effect; }

Unit::Extra& Unit::extra()
{
	return _extra;
}

bool Unit::checkTargetByRadius( const Node * target )const
{
	return true;
}

void Unit::capture_targets( const std::vector<Unit::Pointer> & targets )
{
    assert( targets.size() <= _maxTargets );
	_targets = targets;
    if( _targets.empty() ){
        MachineUnit::capture_target( nullptr );
    }else{
        MachineUnit::capture_target( _targets.front() );
    }
}

void Unit::get_targets( std::vector<Unit::Pointer> & targets )const
{
	targets = _targets;
}

void Unit::stopAllLoopedSounds()
{
    if( _droneSoundKey != -1 )
    {
        AudioEngine::shared().stopEffect( _droneSoundKey );
        _droneSoundKey = -1;
    }
    
    if( _soundMoveID != -1 )
    {
        AudioEngine::shared().stopEffect( _soundMoveID );
        _soundMoveID = -1;
    }
}

void Unit::clear()
{
	_targets.clear();
	_currentDamager.reset( nullptr );
	_skills.clear();
	_healthIndicator.reset( nullptr );
	MachineUnit::start( MachineUnit::state_sleep );
}

void Unit::update( float dt )
{
	bool execution = _board->updateUnitSkills(this, dt);
	if( execution == false )
	{
		bool needturn = _turnOnAttack;
		needturn = needturn && current_state().get_name() != state_move;
		needturn = needturn && current_state().get_name() != state_death;
		needturn = needturn && current_state().get_name() != state_enter;
      
		if( needturn )
		{
			turn( dt );
		}
		MachineUnit::update( dt );

		if( _isComputeDamage)
		{
			_effect.update( dt );
			applyVelocityRate( dt );
			applyTimedDamage( dt );
		}
        
        moveDroneToEnemy(dt);
	}
}

void Unit::applyVelocityRate( float dt )
{
	float rate = _effect.computeMoveVelocityRate();
	float velocity = _mover.getDefaultVelocity() * rate;
	_mover.setVelocity( velocity );
}

void Unit::applyTimedDamage( float dt )
{
	float damage = _effect.computeExtendedDamage( dt ) * _damageShield;
	setCurrentHealth( _currentHealth - damage );
	if( damage != 0 ) on_damage( damage );
}

void Unit::turn( float dt )
{
	Unit::Pointer target = _targets.empty() ? nullptr : _targets.front();
	if( target )
    {
		Vec2 now = (target->getPosition() - getPosition()).getNormalized();
        
		_mover.setDirection( now );
		on_mover( getPosition(), now );
    }
}

void Unit::applyDamageToTarget( Unit::Pointer target )
{
	assert( target );

    checkForCriticalStrike();
    
	if( _damageBySector )
	{
		_board->applyDamageBySector( this );
	}
	else
	{
		if( target )
		{
			target->applyDamage( this );
//            if(getIsCriticalStrike())
//            {
//                _board->getGameLayer()->createEffect( this, target, "type:criticalStrike" );
//            }
			_board->getGameLayer()->createEffect( this, target, _effectOnShoot );
		}
	}
}

void Unit::applyDamage( Unit* damager, float time )
{
	_currentDamager = damager;

    if(damager->getIsCriticalStrike())
    {
        runEvent( "on_criticalHit" );
    }
    
	_effect.applyEffects( damager );
	float damage = _effect.computeDamage( damager ) * time * _damageShield * damager->_damageRate;
	
    
    auto towerName = damager->getName();
    if (towerName == "scorp_arrow")
    {
        bool isEasterEggDoubleDamageUnlcoked = EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kDoubleDamageMageTower);
        if (isEasterEggDoubleDamageUnlcoked)
        {
            damage*=2;
        }
    }
    
    
    setCurrentHealth( _currentHealth - damage );

	if( damage != 0 )
	{
		on_damage( damage );
		_board->onDamage( damager, this, damage );

	}

	for( auto skill : _skills )
	{
		skill->onDamage( damage );
	}

	if( _currentHealth <= 0 )
	{
		_board->onKill( damager, this );
	}
}

void Unit::applyDamageExtended( float time )
{
	_effect.update( time );
	float damage = _effect.computeExtendedDamage( time ) * _damageShield;
	setCurrentHealth( _currentHealth - damage );

	if( damage != 0 )
		on_damage( damage );
}

void Unit::on_shoot( unsigned index )
{
	if( dynamic_cast<Bullet*>(this) )
		return;
    _droneShouldFollowEnemy = false;
    float bulletDelay = 0;
    auto towerName = getName();

    if (towerName == "tower_fire" )
    {        
        runEvent( "stopHoverAnimation" );
        bulletDelay = 0.63;
        _shouldRunIdleAnimation = true;
        _delayAfterFire = 1.75;
        runEvent( "onDroneShoot" );
    }

    
    if(getName() == "tower_gun" && EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kHero1DoubleSpeed))
    {
        runEvent( "on_shoot_easter" );
    }
    else
    {
        runEvent( "on_shoot" );
    }
    
	runEvent( "on_shoot" + toStr( index ) );
	runEvent( "on_shoot" + toStr( index ) + "_byangle" + toStr( _angle ) );
	runEvent( "on_shoot_byangle" + toStr( _angle ) );
	if( _bulletXml.empty() && _linetrapXml.empty() )
	{
		for( auto target : _targets )
		{
			assert( target );
			applyDamageToTarget( target );
		}
	}
	else if( _bulletXml.empty() == false )
	{
		for( auto target : _targets )
		{
			assert( target );
			float angle = _bulletParams[getMover().getCurrentAngle()].useangle;
			Point position = _bulletParams[getMover().getCurrentAngle()].offset + getPosition();
            
            
            if (towerName == "tower_fire")
            {
                auto droneNode = getChildByName("drone");
                if (droneNode)
                {
                    
                    position =  getParent()->convertToNodeSpace(droneNode->getParent()->convertToWorldSpace(droneNode->getPosition()));
                    position.y-=10;
                }
            }
			auto bullet = Bullet::create( _board, _bulletXml, this, target, angle, position );
            bullet->_delay = bulletDelay;
            _board->addUnit( bullet );
		}
    }
	else if( _linetrapXml.empty() == false )
	{
		for( auto target : _targets )
		{
			assert( target );
			std::vector<Point> points;
			Linetrap::computePositions( this, target, points, _linetrapSegmentSize );
			for( auto point : points )
			{
				auto block = Linetrap::create( _board, _linetrapXml, this );
				block->setPosition( point );
				_board->addUnit( block );
			}
		}
	}
};

void Unit::on_sleep( float duration )
{
    runEvent( "on_sleep" );
    if (!_isIdleAnimationRunning)
    {
        runEvent( "runIdleAnimation" );
        scheduleOnce(schedule_selector(Unit::runIdleAnimation), 0.6);
        _isIdleAnimationRunning = true;
    }
};

void Unit::runIdleAnimation(float dt)
{
    runEvent( "runHoverAnimation" );
    animateDrone();
}

void Unit::stopAnimateDrone()
{
    Sprite* droneSprite = (Sprite*) getChildByName("drone");
    if (droneSprite)
    {
        droneSprite->stopAllActions();
//        Action* action = droneSprite->getActionByTag(1122);
//        action->stop();
    }
}


void Unit::moveDroneToEnemy(float dt)
{
    Sprite* droneSprite = (Sprite*) getChildByName("drone");
    if (_type == UnitType::tower && droneSprite)
    {
        
        _delayAfterFire -= dt;
        if (_shouldRunIdleAnimation && _delayAfterFire < 0)
        {
            _shouldRunIdleAnimation = false ;
            runEvent( "runHoverAnimation" );
        }

        
        if (_targets.size() > 0)
        {
            auto target = _targets.at(0);
            auto targetPosition = target->getPosition();
            auto dronePosition = droneSprite->getPosition();
            auto dronePositionWorld =  getParent()->convertToNodeSpace(droneSprite->getParent()->convertToWorldSpace(dronePosition));
           
            if (targetPosition.x < dronePositionWorld.x )
            {
                droneSprite->setFlippedX(false);
            }
            else if (targetPosition.x > dronePositionWorld.x )
            {
                droneSprite->setFlippedX(true);
            }
        }
    }
}

int Unit::getClosestPointIndex()
{
    int closestPointIndex = 0;
    float closestDistance = -1;
    const Node* dronePlaceholder = (Node*) getChildByName("dronePlaceholder");
    Sprite* droneSprite = (Sprite*) getChildByName("drone");
    Vec2 dronePostion = droneSprite->getPosition() ;
    Vec2 basePostion = dronePlaceholder->getPosition() ;

    int index = 0;
    for(auto point : _dronePoints)
    {
        float distance = dronePostion.getDistance(point);
        if (closestDistance == -1 || distance<closestDistance)
        {
            closestDistance=distance;
            closestPointIndex = index;
        }
        index++;
        
    }
    return closestPointIndex;
}

void Unit::hoverDrone()
{
    const float velocity = 15.0 + arc4random()%10;
    auto point = _dronePoints.at(_droneCurrentIndex);
    const int hoverDistance  = 10 + arc4random()%10;
    Sprite* droneSprite = (Sprite*) getChildByName("drone");

    float distance = droneSprite->getPosition().getDistance(point);
    float time = distance/velocity;
    
    
    
    int delta1 = arc4random()%2;
    int delta2 = arc4random()%2;
    Vec2 move1 = Vec2(delta1,delta2);
    Vec2 move2 = Vec2(-delta1,delta2);
    Vec2 move3 = Vec2(-delta1,-delta2);
    Vec2 move4 = Vec2(delta1,-delta2);
    
    float time1  = (hoverDistance/velocity);
    time1 += arc4random()%50/100;
    
    int repeateCount  = arc4random()%2 + 1;
    
    auto hover = Repeat::create(Sequence::create(MoveBy::create(time1, move1)
                                                       ,MoveBy::create(time1,move2)
                                                       ,MoveBy::create(time1,move3)
                                                        ,MoveBy::create(time1,move4), NULL),repeateCount);
    auto moveToBase = MoveTo::create(time, point);
    
    auto hover2 = Sequence::create(hover, moveToBase
                                   ,CallFunc::create([this](){
        _droneCurrentIndex++;
        if(_droneCurrentIndex >= _dronePoints.size())
        {
            _droneCurrentIndex = 0;
        }
        hoverDrone();
    }), NULL);
    hover2->setTag(1122);
    droneSprite->runAction(hover2);
}

void Unit::animateDrone()
{
    
    Sprite* droneSprite = (Sprite*) getChildByName("drone");
    if (droneSprite)
    {
        _droneCurrentIndex =  arc4random()%_dronePoints.size();
        hoverDrone();
    }

}

Vec2 Unit::getRandomPointForDrone()
{
    const Node* dronePlaceholder = (Node*) getChildByName("dronePlaceholder");
    if (dronePlaceholder)
    {
        const Vec2 allowedRadius = Vec2(50, 30);
        const int randomX = RandomNumberGenerator::getRandomInteger(dronePlaceholder->getPositionX() - allowedRadius.x, dronePlaceholder->getPositionX() + allowedRadius.x);
        const int randomY = RandomNumberGenerator::getRandomInteger(dronePlaceholder->getPositionY() - allowedRadius.y, dronePlaceholder->getPositionY() + allowedRadius.y);
        return Vec2(randomX, randomY);
    }
    return Vec2(0, 0);
}

void Unit::on_cocking( float duration )
{
    runEvent( "on_cocking" );
};

void Unit::on_relaxation( float duration )
{
    runEvent( "on_relaxation" );
};

void Unit::on_readyfire( float duration )
{
    
    if( _type == UnitType::tower )
    {
        CCLOG("Umair:: on_readyfire");
    }
    if (_isIdleAnimationRunning)
    {
        unschedule(schedule_selector(Unit::runIdleAnimation));
        stopAnimateDrone();
        _isIdleAnimationRunning = false;
        runEvent( "stopIdleAnimation" );
    }
    _droneShouldFollowEnemy = true;
    
    if (_targets.size()>0)
    {
        runEvent( "on_readyfire" );
    }
};

void Unit::on_charging( float duration )
{
    if( _type == UnitType::tower )
    {
        CCLOG("Umair:: on_charging");
    }

    _droneShouldFollowEnemy = true;
    runEvent( "on_charging" );
};

void Unit::on_waittarget( float duration )
{
    _droneShouldFollowEnemy = false;
    float angle = _mover.getRandomAngle();
	runEvent( "on_waittarget" );
	runEvent( "on_waittarget_" + toStr( angle ) );
    if (!_isIdleAnimationRunning)
    {
        _isIdleAnimationRunning = true;
        runEvent( "runIdleAnimation" );
        scheduleOnce(schedule_selector(Unit::runIdleAnimation), 0.6);
    }

	move();
};

void Unit::on_move()
{
    if( _mover.getRoute().empty() )
	{
		stop();
		return;
	}
	_angle = -1;
	if( _currentHealth > 0 )
	{
        if (!_isFirstRun)
        {
            runEvent( "on_move" );
            this->stopActionByTag(Unit::_turnLeftTag);
			std::string event = "level" + toStr( _board->getCurrentLevelIndex() ) + "_heromove";
			TutorialManager::shared().dispatch( event );
        }
		_moveFinished = false;
		if( _soundMove.empty() == false && _soundMoveID == -1)
		{
			_soundMoveID = AudioEngine::shared().playEffect( _soundMove, true, 0 );
		}
	}
}

void Unit::turnLeft(){
    Vec2 now = Vec2(-1.f,0.f);
    _mover.setDirection( now );
    on_mover( getPosition(), now );
    runEvent( "on_stop" );
}

void Unit::on_stop()
{
	if( _currentHealth > 0 )
	{
        if(_type == UnitType::hero || _type == UnitType::desant){
            CallFunc* turnLeftCallback = CallFunc::create(CC_CALLBACK_0(Unit::turnLeft, this));
            auto turnLeftSequence = Sequence::create(DelayTime::create(0.0f), turnLeftCallback, nullptr);
            turnLeftSequence->setTag(Unit::_turnLeftTag);
            this->runAction(turnLeftSequence);
        }
        runEvent( "on_stop" );
	}
    else {
        if( _type == UnitType::tower )
        {
            if(UserData::shared().get<bool>("Vibration_Enabled",true) && this->getName().find("_landmine") != std::string::npos) {
                cocos2d::Device::vibrate(0.4f);
            }
        }
    }
	if( _soundMoveID != -1 )
	{
		AudioEngine::shared().stopEffect( _soundMoveID );
		_soundMoveID = -1;
	}
	_angle = -1;
}

void Unit::on_die()
{
    MachineUnit::push_event( MachineUnit::event_notarget );
	runEvent( "on_die" );
	setCurrentHealth( 0 );
	if( _soundMoveID != -1 )
	{
		AudioEngine::shared().stopEffect( _soundMoveID );
		_soundMoveID = -1;
	}
}

void Unit::on_enter()
{
    runEvent( "on_enter" );
}

void Unit::moveInLineAfterDelay(float dt)
{
    setVisible(true);
    Point pos = getPosition();
    Route route;
    const int targetPositionX = _angle == 0 ? 4000 : 0;
    route.insert(route.begin(), Point(targetPositionX,pos.y));
    route.insert(route.begin(), Point(pos.x,pos.y));
    getMover().setRoute(route);
    move();
}

void Unit::moveInLine( const float angle, const float startDelay )
{
    _angle = angle;
    if (startDelay > 0)
    {
        setVisible(false);
        scheduleOnce(schedule_selector(Unit::moveInLineAfterDelay), startDelay);
    }
    else
    {
        moveInLineAfterDelay(0);
    }
}

void Unit::moveByRoute( const Route & aroute )
{
	Route route = aroute;

	Point pos = getPosition();
	size_t index( 0 );
	float min_dist( 99999 );
	for( size_t i = 0; i < route.size(); ++i )
	{
		float dist = pos.getDistance( route[i] );
		if( dist < min_dist )
		{
			index = i; 
			min_dist = dist;
		}
	}

	//if( index > 0 )
	route.erase( route.begin(), route.begin() + index+1 );
	route.insert( route.begin(), pos );
	getMover().setRoute( route );
}

void Unit::setCurrentHealth( float value )
{
	auto prev = _currentHealth > 0.f ? _currentHealth : 0.f;
	auto curr = value > 0.f ? value : 0.f;
	_currentHealth = value;
	if( _showHealthIndicator )
	{
		float progress = _currentHealth / (_health != 0 ? _health : 1);
		bool isVisible = _currentHealth < _defaultHealth * _rate && _currentHealth > 0;
		_healthIndicator->setProgress( progress );
		_healthIndicator->setVisible( isVisible );
	}

	observerHealth.pushevent( _currentHealth, _defaultHealth * _rate );

	if( _healthStates.size() != 0 && prev != curr )
	{
		updateHealthState( prev, curr );
	}
}

void Unit::updateHealthState( float previousHealth, float currentHealth )
{
	auto prev = previousHealth * 100.f / ( _health != 0 ? _health : 1 );
	auto curr = currentHealth * 100.f / ( _health != 0 ? _health : 1 );

	int step;
	step = ( curr < prev ) ? 1 : 0;
	step = ( curr > prev ) ? -1 : step;
	bool stateChanged = false;
	if( step > 0 )	
	{
		while( _currentHealthState < (int)_healthStates.size() && curr <= _healthStates[_currentHealthState] )
		{
			stateChanged = true;
			_currentHealthState += step;
		}
	}
	else if( step < 0 )
	{
		while( _currentHealthState >= 0 && curr >= _healthStates[_currentHealthState] )
		{
			stateChanged = true;
			_currentHealthState += step;
		}
	}
	if( stateChanged )
	{
		runEvent( "state" + toStr(_currentHealthState) );
	}
}

void Unit::removeSkill( UnitSkill::Pointer skill )
{
	auto it = std::find( _skills.begin(), _skills.end(), skill );
	if( it != _skills.end() )
		_skills.erase( it );
}

std::vector<UnitSkill::Pointer>& Unit::getSkills()
{
	return _skills;
}

const std::vector<UnitSkill::Pointer>& Unit::getSkills()const
{
	return _skills;
}

void Unit::forceShoot( Unit* target, const mlEffect& effect, bool onlyApplyDamage )
{
	const auto hist = _targets;
	_targets.clear();
	_targets.push_back( target );
	mlEffect histeffect = _effect;
	_effect = effect;

	if( onlyApplyDamage )
	{
		for( auto target : _targets )
		{
			assert( target );
			applyDamageToTarget( target );
		}
	}
	else 
		on_shoot( 0 );

	_targets = hist;
	_effect = histeffect;
}

void Unit::skillActivated( UnitSkill* skill )
{
	auto skillcounter = dynamic_cast<UnitSkillCounter*>(skill);
	if( skillcounter )
	{
		std::string type = skillcounter->getType();
		float rate = skillcounter->getValue();

		if( type == "shield" )
		{
			_damageShield = rate;
			runEvent( "skill_activated_shield" );
		}
		else if( type == "rage" )
		{
			_damageRate = rate;

			runEvent( "skill_activated_rage" );
		}
	}
}

void Unit::skillDeactivated( UnitSkill* skill )
{
	auto skillcounter = dynamic_cast<UnitSkillCounter*>(skill);
	if( skillcounter )
	{
		std::string type = skillcounter->getType();
		if( type == "shield" )
		{
			_damageShield = 1;
			runEvent( "skill_deactivated_shield" );
		}
		else if( type == "rage" )
		{
			_damageRate = 1;
			runEvent( "skill_deactivated_rage" );
		}
	}
}

void Unit::on_die_finish()
{
	runEvent( "on_die_finish" );
	_board->death( this );
}

void Unit::move_update( float dt )
{
	_mover.update( dt );
}

void Unit::stop_update( float dt )
{}

void Unit::on_mover( const Point & position, const Vec2 & direction )
{
	unsigned angle = _mover.getCurrentAngle();
	setPosition( position );
	
    if (_mover.getIsFlappy())
    {
        setRotation(_mover.getFlappyRotation());
    }
    
    if( angle != _angle && ( getType() != UnitType::creep || _mover.getVelocity() > 0 ) )
	{
		_angle = angle;
		runEvent( "on_rotate" + toStr( _angle ) );
	}

	float z = _unitLayer == UnitLayer::sky ? 9000 : -position.y;
	z += _additionalZorder;
	setZOrder( z );
}

void Unit::on_movefinish()
{
	stop();
	_moveFinished = true;
}

void Unit::on_damage( float value )
{}

void Unit::checkForCriticalStrike()
{
    int randomNumber = RandomNumberGenerator::getRandomInteger(1, 100);
    _isCriticalStrike = randomNumber < getEffect().positive.critChance;
}


/*****************************************************************************/
//MARK: Unit::Extra
/*****************************************************************************/

Unit::Extra::Extra()
	: _electroPosition()
, _electroSize()
, _electroScale( 0 )
, _firePosition()
, _fireScale( 0 )
, _freezingPosition()
, _freezingScale( 0 )
{}

const Point& Unit::Extra::getPositionElectro()const
{
	return _electroPosition;
}

const std::string& Unit::Extra::getSizeElectro()const
{
	return _electroSize;
}

const float Unit::Extra::getScaleElectro()const
{
	return _electroScale;
}

const Point& Unit::Extra::getPositionFire()const
{
	return _firePosition;
}

const float Unit::Extra::getScaleFire()const
{
	return _fireScale;
}

const Point& Unit::Extra::getPositionFreezing()const
{
	return _freezingPosition;
}

const float Unit::Extra::getScaleFreezing()const
{
	return _freezingScale;
}

float Unit::getRate()
{
	return _rate;
}

void Unit::setRate( float value )
{
	_rate = value;
	float health = getDefaultHealth() * _rate;
	setCurrentHealth( health );
	setHealth( health );
	_healthIndicator->displayHP( health );
}

/*****************************************************************************/
//MARK: Unit::play sound
/*****************************************************************************/

void Unit::playTowerPlaceSound() {
	AudioEngine::shared().playEffect(_placeSound);
}

NS_CC_END
