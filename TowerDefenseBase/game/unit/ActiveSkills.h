//
//  Skills.h
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 26.11.14.
//
//

#ifndef __IslandDefense__ActiveSkills__
#define __IslandDefense__ActiveSkills__

#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/pugixml/pugixml.hpp"
#include "ml/Events.h"
#include "effects.h"
#include "Skills.h"

NS_CC_BEGIN;

class UnitActiveSkill : public UnitSkill
{
	DECLARE_BUILDER( UnitActiveSkill );
	bool init( const pugi::xml_node & xmlNode, Unit* unit );

public:
	virtual void update( float dt, Unit* context ) override;
	virtual bool startExecution( Vec2 location );
	virtual bool execution() override;
	bool isMelee() { return _melee; }
protected:
	void execute( Unit* context );
	void stop( Unit* context );
private:
	float _timer;
	float _timerDuration;
	bool _stopedUnit;
	bool _resumeMoving;
	bool _waitExecution;
	float _stopDuration;
	unsigned _count;
	EventsList _eventsOnExecute;
	EventsList _eventsOnStop;
	std::set<std::string> _allowStatesForExecute;
	CC_SYNTHESIZE_READONLY( std::string, _name, Name );
	bool _execution;
	Vec2 _location;
	bool _melee;
	std::string _unitName;
	Vec2 _offset;
};

//class UnitActiveSkillCreateUnit : public UnitActiveSkill
//{
//	DECLARE_BUILDER( UnitActiveSkillCreateUnit );
//	bool init( const pugi::xml_node & xmlNode, Unit* unit );
//	
//};

NS_CC_END;

#endif /* defined(__IslandDefense__Skills__) */
