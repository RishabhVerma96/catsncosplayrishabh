#ifndef __Linetrap_h__
#define __Linetrap_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "Unit.h"
NS_CC_BEGIN





class Linetrap : public Unit
{
	DECLARE_BUILDER( Linetrap );
	bool init( GameBoard* board, const std::string& path, Unit::Pointer base );
public:
	static void computePositions( Unit::Pointer base, Unit::Pointer target, std::vector<Point> & points, float segmentWidth );
protected:
private:
};




NS_CC_END
#endif // #ifndef Linetrap