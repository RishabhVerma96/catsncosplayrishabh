//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __Unit_h__
#define __Unit_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/Observer.h"
#include "MachineUnit.h"
//#include "MachineMove.h"
#include "Mover.h"
#include "effects.h"
#include "ml/NodeExt.h"
#include "IndicatorNode.h"
#include "support.h"
#include "Skills.h"
NS_CC_BEGIN

class GameBoard;

class Unit : public Node, public NodeExt, public MachineUnit/*, public MachineMove*/
{
public:
	typedef ObServer<Unit, std::function<void( float, float )> > ObserverHealth;
private:
	class Extra
	{
		friend class Unit;
	public:
		Extra();
		const Point& getPositionElectro( )const;
		const std::string& getSizeElectro( )const;
		const float getScaleElectro( )const;

		const Point& getPositionFire()const;
		const float getScaleFire()const;
		const Point& getPositionFreezing()const;
		const float getScaleFreezing()const;
	private:
		Point _electroPosition;
		std::string _electroSize;
		float _electroScale;
		Point _firePosition;
		float _fireScale;
		Point _freezingPosition;
		float _freezingScale;

	};

	DECLARE_BUILDER( Unit );
	bool init( GameBoard* board, const std::string & path, const std::string & xmlFile = "ini.xml" );
	bool init( GameBoard* board, const std::string & path, const std::string & xmlFile, Unit* upgradedUnit );
public:
	/*
	getters and setters
	*/
	mlEffect& getEffect();
	const mlEffect& getEffect()const;

	Extra& extra();

	/*
	fight!
	*/
	virtual bool checkTargetByRadius( const Node * target )const;
	virtual void capture_targets( const std::vector<Unit::Pointer> & targets );
	virtual void get_targets( std::vector<Unit::Pointer> & targets )const;
	void applyDamageToTarget( Unit::Pointer target );
	void applyDamage( Unit* damager, float time = 1 );
	void applyDamageExtended( float time );
	void turn( float dt );
    

	/*
	Sounds:
	*/
	void stopAllLoopedSounds();

	void update( float dt );
	virtual void clear();
	Mover& getMover() { return _mover; }
	int getDirection()const { return _angle; }

	virtual void moveByRoute( const Route & route );
    virtual void moveInLine( const float angle, const float startDelay);
    void moveInLineAfterDelay(float dt);

	virtual void setCurrentHealth( float value );
	float getCurrentHealth()const { return _currentHealth; }

	void updateHealthState( float previousHealth, float currentHealth );

	void removeSkill( UnitSkill::Pointer skill );
	std::vector<UnitSkill::Pointer>& getSkills();
	const std::vector<UnitSkill::Pointer>& getSkills()const;

	void forceShoot(Unit* target, const mlEffect& effect, bool onlyApplyDamage = false );

	virtual void skillActivated( UnitSkill* skill );
	virtual void skillDeactivated( UnitSkill* skill );
    
	ObserverHealth observerHealth;
    float _handRadiusSector2;
	
protected:
	void applyVelocityRate( float dt );
	void applyTimedDamage( float dt );

	virtual void load( pugi::xml_node & root ) override;
	virtual bool loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )override;
	virtual void loadXmlSkills( const pugi::xml_node & xmlnode );
	virtual UnitSkill::Pointer loadXmlSkill( const pugi::xml_node & xmlnode );
	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
    void runIdleAnimation(float dt);
    Vec2 getRandomPointForDrone();
    void animateDrone();
    void hoverDrone();

    void moveDroneToEnemy(float dt);
    int getClosestPointIndex();
    void stopAnimateDrone();
	
	virtual void on_shoot( unsigned index ) override;
	virtual void on_sleep( float duration ) override;
	virtual void on_cocking( float duration ) override;
	virtual void on_relaxation( float duration ) override;
	virtual void on_readyfire( float duration ) override;
	virtual void on_charging( float duration ) override;
	virtual void on_waittarget( float duration ) override;
	virtual void on_move( ) override;
	virtual void on_stop( ) override;
	virtual void on_die( ) override;
	virtual void on_die_finish( ) override;
	virtual void on_enter();
    void turnLeft();

	virtual void move_update( float dt ) override;
	virtual void stop_update( float dt ) override;

	virtual void on_mover( const Point & position, const Vec2 & direction );
	virtual void on_movefinish( );
	virtual void on_damage( float value );
    void checkForCriticalStrike();


private:
	CC_SYNTHESIZE( GameBoard*, _board, GameBoard );
	mlEffect _effect;
	Extra _extra;
	Mover _mover;
	int _angle;
	bool _turnOnAttack;
    const int _turnLeftTag = 1024;

	std::vector<Unit::Pointer> _targets;
	CC_SYNTHESIZE( Unit::Pointer, _currentDamager, CurrentDamager );
	IndicatorNode::Pointer _healthIndicator;
	std::vector<UnitSkill::Pointer> _skills;

	float _currentHealth;
	unsigned int _soundMoveID;

	struct BulletParams
	{
		float byangle;
		float useangle;
		Point offset;
	};
	std::string _bulletXml;
	std::map<float, BulletParams> _bulletParams;
	std::string _linetrapXml;
	float _linetrapSegmentSize;

	CC_PROPERTY( float, _rate, Rate );
	CC_SYNTHESIZE_READONLY( std::string, _effectOnShoot, EffectOnShoot );
	CC_SYNTHESIZE( unsigned, _level, Level );
	CC_SYNTHESIZE( unsigned, _maxLevel, MaxLevel );
	CC_SYNTHESIZE( unsigned, _maxLevelForLevel, MaxLevelForLevel );
	CC_SYNTHESIZE( unsigned, _cost, Cost );
	CC_SYNTHESIZE( float, _radius, Radius );
	CC_SYNTHESIZE( BodyType, _bodyType, BodyType );
	CC_SYNTHESIZE_READONLY( float, _defaultHealth, DefaultHealth );
	CC_SYNTHESIZE( float, _health, Health );
	CC_SYNTHESIZE( bool, _showHealthIndicator, IsShowHealthIndicator );
	CC_SYNTHESIZE_READONLY( bool, _moveFinished, MoveFinished );
	CC_SYNTHESIZE( bool, _damageBySector, DamageBySector );
    CC_SYNTHESIZE( bool, _isCriticalStrike, IsCriticalStrike );
	CC_SYNTHESIZE( float, _damageBySectorAngle, DamageBySectorAngle );
	CC_SYNTHESIZE( UnitLayer::type, _unitLayer, UnitLayer );
	CC_SYNTHESIZE( UnitLayer::type, _allowTargets, AllowTargets );
	CC_SYNTHESIZE_READONLY( unsigned, _maxTargets, MaxTargets );
	CC_SYNTHESIZE( UnitType, _type, Type );
    CC_SYNTHESIZE_READONLY( std::string, _soundMove, SoundMove );
	CC_SYNTHESIZE( int, _lifecost, LifeCost );
    CC_SYNTHESIZE( Point, _deadZone, DeadZone );
	CC_SYNTHESIZE_READONLY( float, _exp, Exp );
	CC_SYNTHESIZE( std::string, _placeSound, PlaceSound );
	// TODO: ������� �� ������������� �����, ������ ������ ���� �������������� ���.
	CC_SYNTHESIZE_READONLY( UnitType, _extraTargetType, ExtraTargetType );
	int _additionalZorder;

	float _damageShield;
	float _damageRate;
    bool _isIdleAnimationRunning;
    bool _isFirstRun;
	//for online
	CC_SYNTHESIZE( int, _id, Id );
	CC_SYNTHESIZE( RouteSubType, _rst, RouteSubType );
	CC_SYNTHESIZE( int, _routeIndex, RouteIndex );
	CC_SYNTHESIZE( bool, _isComputeDamage, IsComputeDamage );

	std::vector<float> _healthStates;
	int _currentHealthState;
    
    
    std::vector<cocos2d::Point> _dronePoints;
    int _droneCurrentIndex;
    cocos2d::Point _minPoint;
    cocos2d::Point _maxPoint;
    
    bool _droneShouldFollowEnemy;
    bool _shouldRunIdleAnimation;
    int _droneCurrentPosture;
    float _delayAfterFire;
    int _droneSoundKey;
    
    void playTowerPlaceSound();
    
};


typedef std::vector<Unit::Pointer> TowersArray;



NS_CC_END
#endif // #ifndef Unit
