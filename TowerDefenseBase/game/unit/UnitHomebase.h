#ifndef __UnitHomebase_h__
#define __UnitHomebase_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "Unit.h"

NS_CC_BEGIN
class UnitHomebase : public Unit
{
	DECLARE_BUILDER( UnitHomebase );
	bool init( GameBoard* board = nullptr, const std::string & path = "", const std::string & xmlFile = "" );
public:
	virtual void setGameBoard( GameBoard* board ) override;
	virtual void setCurrentHealth( float value ) override;
protected:
	virtual void on_die_finish() override;
	virtual void on_die() override;
private:	
	bool _destroyed;
	int _gameboardStartHealth;
	int _gameboardCurrentHealth;

	bool _autotest;
	
};
NS_CC_END

#endif // #ifndef UnitHomebase