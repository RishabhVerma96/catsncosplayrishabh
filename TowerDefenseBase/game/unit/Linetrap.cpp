#include "Linetrap.h"
#include "gameboard.h"
NS_CC_BEGIN

Linetrap::Linetrap()
{
}

Linetrap::~Linetrap()
{}

bool Linetrap::init( GameBoard* board, const std::string& path, Unit::Pointer base)
{
	do
	{
		CC_BREAK_IF( !Unit::init( board, "", path ) );

		setDamageBySector( base->getDamageBySector() );
		setAllowTargets( base->getAllowTargets() );
		getEffect().copyFrom( base->getEffect() );
		return true;
	}
	while( false );
	return false;
}


void Linetrap::computePositions(Unit::Pointer base, Unit::Pointer target, std::vector<Point> & points, float segmentWidth)
{
	const float startRadius = 0.f;
	const float maxDistanceToRoad = 20.f;
	float radius = base->getRadius();
	Point basePosition = base->getPosition();
	auto angle = getDirectionByVector( target->getPosition() - basePosition ) * M_PI / 180.f;
	auto delta = Point( segmentWidth * cos( angle ), -segmentWidth * sin( angle ) );
	int count = 0;
	for( float r = startRadius + CCRANDOM_0_1() * segmentWidth; r < radius; r += segmentWidth )
	{
		count++;
		float dummy( 0 );
		auto point = basePosition + delta * count;
		auto routes = base->getGameBoard()->getCreepsRoutes();

		if( checkPointOnRoute( routes, point, maxDistanceToRoad, base->getUnitLayer(), &dummy ))
		{
			points.push_back( point );
		}
	}
}

NS_CC_END