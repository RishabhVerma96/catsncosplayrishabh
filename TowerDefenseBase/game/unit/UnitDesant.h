#ifndef __UnitDesant_h__
#define __UnitDesant_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "Unit.h"
NS_CC_BEGIN


/*
1. remove on_mover
2. uncomment block if(result)... in method isNearestTarget
*/


class UnitDesant : public Unit


{

	DECLARE_BUILDER( UnitDesant );
	bool init( GameBoard* board, const std::string & path, const std::string & xmlFile );
public:
	virtual bool checkTargetByRadius( const Node * target )const;
	virtual void capture_targets( const std::vector<Unit::Pointer> & targets ) override;

    void die_update( float dt ) override;

    bool moveTo( const Point & position );
    
    static void checkRoute( const TripleRoute & tripleroute, const Point & A, const Point & B, Route & route, bool fromStartPoint = true );
    void findOneRoute( const Point & position, Route & route );
    void findTwoRoute( const Point & position, Route & route );
    void finalizateRoute( const Point & position, Route & route );

    int getDesantType();
    
    void updateHealth(int amount);
protected:
    enum Event
    {
        liveHero = 100,
        liveDesant = 101,
    };

	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;
	virtual void on_die() override;
    virtual void on_die_finish() override;
    virtual void stop_update(float dt)override;

	//virtual void on_mover( const Point & position, const Vec2 & direction )override;

	void buildRouteToBase( std::vector<Point> & route );
	void buildRouteToTarget( std::vector<Point> & route );
	bool isNearestTarget( );
	bool isNearestBase( );

    float _dieTimer;
    float _regeneration;
    float _extraRegenerationValue;
    float _extraRegenerationTime;

    
    
private:

	float _handRadius;
	float _handRadiusSector;
	std::vector<Unit::Pointer> _targets;
	CC_SYNTHESIZE_PASS_BY_REF( Point, _basePosition, BasePosition );
    CC_SYNTHESIZE( int, _desantType, DesantType );
    CC_SYNTHESIZE( int, _desantHeal, DesantHeal );
    CC_SYNTHESIZE( int, _desantHealRadius, DesantHealRadius );

};




NS_CC_END
#endif // #ifndef UnitDesant
