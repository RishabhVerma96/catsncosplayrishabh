#include "ml/AStar.h"
#include "Hero.h"
#include "support.h"
#include "ml/pugixml/pugixml.hpp"
#include "UserData.h"
#include "consts.h"
#include "Achievements.h"
#include "configuration.h"
#include "ShopLayer2.h"
#include "gameboard.h"
NS_CC_BEGIN

void HeroExp::onCreate()
{
	pugi::xml_document doc;
	doc.load_file( "ini/herolevels.xml" );
	auto root = doc.root().first_child();
	_resurrectionCost = root.attribute( "resurrectioncost" ).as_int();
	auto xmlexp = root.child( "exp" );
	auto xmllevels = root.child( "levels" );
	auto xmlcosts = root.child( "upgradecost" );
	auto xmlHeroCosts = root.child( "heroesparams" );
	auto xmlTestDrive = root.child( "testdrive" );
	for( auto child : xmlexp )
	{
		float exp = child.attribute( "exp" ).as_float();
		_heroLevels.push_back( exp );
	}
	for( auto child : xmllevels )
	{
		float exp = child.attribute( "exp" ).as_float();
		_levelAwards.push_back( exp );
	}
	for( auto child : xmlcosts )
	{
		float exp = child.attribute( "exp" ).as_float();
		_levelCosts.push_back( exp );
	}
	std::vector<unsigned> skillsCount;

	for( auto child : xmlHeroCosts )
	{
		std::string name = child.attribute( "name" ).as_string();
		std::string productid = child.attribute( "productid" ).as_string();
		unsigned levelforunlock = child.attribute( "availabledafterlevel" ).as_uint( -1 );
        unsigned videoCountToUnlock = child.attribute( "videoCountToUnlock" ).as_uint( -1 );

        float exp = child.attribute( "exp" ).as_float();
		exp = std::max( getEXP( name ), exp );
		setEXP( name, exp );

		if( productid.empty() == false )
		{
			_heroInappDetails[name].result = inapp::Result::Fail;

			auto callback = [this, name]( inapp::SkuDetails details )
			{
				if( details.result == inapp::Result::Ok )
				{
					_heroInappDetails[name] = details;
				}
			};
			if( Config::shared().get<bool>( "useHeroRoom" ) )
			{
                productid = Config::shared().get( "inappPackage" ) + productid;
				_heroInappDetails[name] = inapp::SkuDetails();
				_heroInappDetails[name].productId = productid;
				_heroInappDetails[name].result = inapp::Result::Fail;
				inapp::details( productid, std::bind( callback, std::placeholders::_1 ) );
			}
		}
		if( levelforunlock != -1 )
			_levelsForUnclockHeroes[name] = levelforunlock;
        _videoCountToUnclockHeroes[name] = videoCountToUnlock;
		skillsCount.push_back( child.attribute( "skillscount" ).as_uint( 5 ) );
	}
	for( auto xml : xmlTestDrive )
	{
		std::string hero = xml.attribute( "name" ).as_string();
		auto runned = UserData::shared().get<bool>( "HeroTestDriveCountLaunched" + hero );
		if( runned == false )
			_heroesOnTestDrive.push_back( hero );
	}

	for( int i = 0; i < Config::shared().get<int>( "heroesCount" ); ++i )
		_skills["hero" + toStr( i + 1 )].resize( skillsCount[i] );
	
    for( auto& pair : _skills )
	{
		int index = 0;
		for( auto& skill : pair.second )
		{
			std::string key = k::user::HeroSkillPoints + pair.first + toStr( index );
			skill = UserData::shared().get <int> (key, skill);
			++index;
		}
	}

	checkUnlockedHeroes();
}

bool HeroExp::isHeroAvailabled( const std::string& name )const
{
	return UserData::shared().get <bool> (k::user::HeroBought + name);
}

bool HeroExp::isHeroAsInapp( const std::string& name )
{
	auto iter = _heroInappDetails.find( name );
	return iter == _heroInappDetails.end() ? false : iter->second.productId.empty() == false;
}

unsigned HeroExp::getLevelForUnlockHero( const std::string& name )
{
	auto iter = _levelsForUnclockHeroes.find( name );
	return iter != _levelsForUnclockHeroes.end() ? iter->second : 0;
}

unsigned HeroExp::getRequiredVideoCountToUnlockHero( const std::string& name )
{
    auto iter = _videoCountToUnclockHeroes.find( name );
    return iter != _videoCountToUnclockHeroes.end() ? iter->second : -1;
}


void HeroExp::heroBought( const std::string& name )const
{
	if (UserData::shared().get <bool> (k::user::HeroBought + name) == false)
	{
		UserData::shared().write( k::user::HeroBought + name, true );
		Achievements::shared().process( "heroes_open", 1 );
	}
}

inapp::SkuDetails HeroExp::getHeroSkuDetails( const std::string& name )
{
	if( _heroInappDetails[name].result != inapp::Result::Ok )
	{
		auto callback = [this, name]( inapp::SkuDetails details )
		{
			if( details.result == inapp::Result::Ok )
			{
				_heroInappDetails[name] = details;
			}
			else
			{
				auto products = ShopLayer2::Dispatcher::shared().products;
				auto& product = products[ name ];
				_heroInappDetails[name].result = inapp::Result::Fail;
				_heroInappDetails[name].priceText = product.price;
				_heroInappDetails[name].description = product.description;
			}
		};
		if( Config::shared().get<bool>( "useHeroRoom" ) && isHeroAsInapp( name ) )
		{
			inapp::details( _heroInappDetails[name].productId, std::bind( callback, std::placeholders::_1 ) );
		}
	}
	return _heroInappDetails.at( name );
}

void HeroExp::setEXP( const std::string & name, float exp, bool dispatchAchievements )
{
	assert( (exp >= 0) && (exp <= 10E+10) );
	float currEXP = getEXP( name );
	int curr = getLevel( currEXP );
	int next = getLevel( exp );
	next = std::min<int>( _heroLevels.size() - 1, next );
	if( dispatchAchievements )
	{
		for( int i = curr; i < next; ++i )
		{
			std::string key = "herolevel_" + toStr( i + 1 );
			if( UserData::shared().get <bool>( key + name ) == false )
			{
				Achievements::shared().process( key, 1 );
			}
		}
	}
	UserData::shared().write( k::user::HeroExp + name, exp );
}

float HeroExp::getEXP( const std::string & name )
{
	return UserData::shared().get <float>(k::user::HeroExp + name);
}

float HeroExp::getLevel( float exp )const
{
	float level( 0 );
	float diff( 0 );
	float prev( 0 );
	for( size_t i = 0; i < _heroLevels.size(); ++i )
	{
		diff = (i < (_heroLevels.size() - 1)) ? _heroLevels[i + 1] - _heroLevels[i] : 0.f;
		if( exp < _heroLevels[i] )
			break;
		prev = _heroLevels[i];
		++level;
	}
	level += diff != 0 ? (exp - prev) / diff : 0;
	return level;
}

float HeroExp::getExpOnLevelFinished( unsigned level )const
{
#if USE_CHEATS == 1
	if( level >= _levelAwards.size() )
		MessageBox( ("EXP for level [" + toStr( level ) + "] not defined\nSee ini/herolevels.xml").c_str(), "Hero exp not defined" );
#endif
	return level < _levelAwards.size() ? _levelAwards[level] : 0;
}

float HeroExp::getHeroLevelExtValue( unsigned level )const
{
	return level < _heroLevels.size() ? _heroLevels[level] : 0;
}

float HeroExp::getCostLevelup( unsigned level )const
{
	return _levelCosts[level];
}

unsigned HeroExp::getMaxLevel()const
{
	return static_cast<unsigned>(_heroLevels.size());
}

void HeroExp::skills( const std::string& name, std::vector<unsigned> & skills )
{
	skills = _skills[name];
	if( skills.empty() )
		skills.resize( 5 );
}

unsigned HeroExp::skillPoints( const std::string& name )
{
	//return UserData::shared().get_int( k::user::HeroSkillPoints + name );
	int points = getLevel( getEXP( name ) );
	std::vector<unsigned> skills;
	this->skills( name, skills );
	for( auto skill : skills )
		points -= skill;
	assert( points >= 0 );
	return static_cast<unsigned>(points);
}

void HeroExp::setSkills( const std::string& name, const std::vector<unsigned> & skills )
{
	_skills[name] = skills;

	int index = 0;
	for( int skill : skills )
	{
		std::string key = k::user::HeroSkillPoints + name + toStr( index );
		UserData::shared().write( key, skill );
		++index;
	}
}

void HeroExp::checkUnlockedHeroes()
{
	unsigned passed = UserData::shared().level_getCountPassed();
	for( auto pair : _levelsForUnclockHeroes )
	{
		auto hero = pair.first;
		auto level = pair.second;
		if( isHeroAvailabled( hero ) == false && level <= passed )
		{
			heroBought( hero );
		}
	}
}

unsigned HeroExp::getNonPurchasableHeroesCount()
{
	return _levelsForUnclockHeroes.size();
}

std::string HeroExp::getHeroOnTestDrive()
{
	std::string result;
	if( _heroesOnTestDrive.empty() == false )
		result = _heroesOnTestDrive.front();
	return result;
}

void HeroExp::popHeroOnTestDrive() 
{
	if( _heroesOnTestDrive.empty() == false )
		_heroesOnTestDrive.pop_front();
}

int HeroExp::getCostResurrection()
{
	return _resurrectionCost;
}



Hero::Hero()
{
    _dieTimer= 0;
    _regeneration = 0;
    _extraRegenerationValue = 0;
    _extraRegenerationTime = 0;
}

Hero::~Hero()
{}



bool Hero::init( GameBoard* board, const std::string & path, const std::string & xmlFile )
{
	do
	{
		MachineExt::add_event( Event::liveHero ).set_string_name( "liveHero" );

		CC_BREAK_IF( !UnitDesant::init( board, path, xmlFile ) );

		return true;
	}
	while( false );
	return false;
}

void Hero::initSkills( const std::vector<unsigned>& predefinedSkills, int level )
{
	if( level == -1 )
	{
		auto exp = HeroExp::shared().getEXP( getName() );
		level = HeroExp::shared().getLevel( exp );
	}
	setLevel( level );

	std::vector<unsigned> heroSkills = predefinedSkills;
	if( heroSkills.empty() )
		HeroExp::shared().skills( getName(), heroSkills );

	std::vector<UnitSkill::Pointer> skills = getSkills();
	for( auto skill : skills )
	{
		bool checkUnitSkill = skill->getNeedUnitSkill().empty() == false;
		if( checkUnitSkill == false ) continue;
		unsigned type = strTo<int>( skill->getNeedUnitSkill() );
		unsigned level = skill->getNeedUnitSkillLevel();

		if( heroSkills[type] != level )
			removeSkill( skill );
	}

	skills = getSkills();
	for( auto skill : skills )
	{
		auto paramSkill = dynamic_cast<UnitSkillRateParameter*>(skill.ptr());
		if( paramSkill )
		{
			std::string param = paramSkill->getParameter();
			float rate = paramSkill->getRate();

			if( param == "health" )
			{
				setProperty( param, toStr( getHealth() * rate ) );
			}
			else if( param == "armor" )
			{
				auto& effect = getEffect();
				effect.positive.armor *= rate;
			}
			else if( param == "damage" )
			{
				auto& effect = getEffect();
				effect.positive.damage *= rate;
				effect.positive.electroRate *= rate;
				effect.positive.fireRate *= rate;
				effect.positive.iceRate *= rate;
			}
		}
	}

	auto activeSkills = getActiveSkills();
	for( auto skill : activeSkills )
	{
		bool checkUnitSkill = skill->getNeedUnitSkill().empty() == false;
		if( checkUnitSkill == false ) continue;
		unsigned type = strTo<int>( skill->getNeedUnitSkill() );
		unsigned level = skill->getNeedUnitSkillLevel();

		std::vector<unsigned> heroSkills;
		HeroExp::shared().skills( getName(), heroSkills );
		if( heroSkills[type] != level )
			removeActiveSkill( skill );
	}

}

void Hero::removeActiveSkill( UnitActiveSkill::Pointer skill )
{
	auto it = std::find( _activeSkills.begin(), _activeSkills.end(), skill );
	if( it != _activeSkills.end() )
		_activeSkills.erase( it );
}



bool Hero::setProperty( const std::string & stringproperty, const std::string & value )
{
	if( stringproperty == "regeneration" )
		_regeneration = strTo<float>( value );
	else if( stringproperty == "skill" )
		_skill = value;
	else if( stringproperty == "extraRegenerationValue" )
		_extraRegenerationValue = strTo<float>( value );
	else if( stringproperty == "extraRegenerationTime" )
		_extraRegenerationTime = strTo<float>( value );
	else 
		return UnitDesant::setProperty( stringproperty, value );
	
	return true;
}

void Hero::on_die()
{
	UnitDesant::on_die();
}

void Hero::on_die_finish()
{
    NodeExt::runEvent( "on_die_finish" );
    setCurrentHealth( getDefaultHealth() * getRate() );
    MachineExt::push_event( Event::liveHero );
    observerHealth.unlock();
}

void Hero::update( float dt )
{
	bool execution( false );
   
	if (current_state().get_name() != state_death)
	{
		for (auto& skill : _activeSkills)
		{
			execution = execution || skill->execution();
		}
		for (auto& skill : _activeSkills)
		{
			bool allow = (execution == false) || (skill->execution());
			allow = allow && (skill->getOnlyState().empty() ? true : skill->getOnlyState() == current_state().get_string_name());
			if (allow)
				skill->update( dt, this );
		}
	}

	if( current_state().get_name() != state_death )
	{
		applyExtraRegenaration( dt );
	} 
	else
	{
		_extraRegenerationValue = 0.f;
		_extraRegenerationTime = 0.f;
	}

	if (execution == false)
	{
		Unit::update( dt );
	}
}

bool Hero::loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )
{
	if (tag == k::xmlTag::UnitActiveSkills)
	{
		loadXmlActiveSkills( xmlnode );
	}
	else
	{
		return UnitDesant::loadXmlEntity( tag, xmlnode );
	}
	return true;
}

void Hero::loadXmlActiveSkills( const pugi::xml_node & xmlnode )
{
	for (auto xml : xmlnode)
	{
		auto skill = loadXmlActiveSkill( xml );
		if (skill)
			_activeSkills.push_back( skill );
	}
}

UnitActiveSkill::Pointer Hero::loadXmlActiveSkill( const pugi::xml_node & xmlnode )
{
	std::string type = xmlnode.name();
	UnitActiveSkill::Pointer skill;

	if (type == "createunits")
		skill = UnitActiveSkill::create( xmlnode, this );

	return skill;
}

std::vector<UnitActiveSkill::Pointer>& Hero::getActiveSkills()
{
	return _activeSkills;
}

const std::vector<UnitActiveSkill::Pointer>& Hero::getActiveSkills() const
{
	return _activeSkills;
}

void Hero::skillExecution( bool value )
{
	_observerOnChangeExecution.pushevent( value );
}

void Hero::applyExtraRegenaration( float dt )
{
	float health = getCurrentHealth();
	health = std::min( getDefaultHealth() * getRate(), health + _extraRegenerationValue * dt );
	setCurrentHealth( health );

	_extraRegenerationTime = std::max( _extraRegenerationTime - dt, 0.f );
	if( _extraRegenerationTime <= 0.f )
		_extraRegenerationValue = 0.f;
}
NS_CC_END
