//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "Mover.h"
#include "ml/common.h"
NS_CC_BEGIN


Mover::Mover()
: _route()
, _syncSegment(0)
, _currentSegment(0)
, _position()
, _currentDirection()
, _truncatedDirection()
, _currentAngle( 0 )
, _velocity( 0 )
, _onChangePosition()
, _onFinish()
, _isFlappy(false)
, _flappyRotation(0)
{
    _gravity = 0.4;
}

void Mover::load( const pugi::xml_node & xmlnode )
{
	auto xmlparams = xmlnode.child( "params" );
	auto xmlangles = xmlparams.child( "allowangles" );
	auto xmlthresold = xmlparams.child( "thresold" );

	std::list<std::string> angles;
	split( angles, xmlangles.attribute( "value" ).as_string() );
	for( auto angle : angles )
	{
		_allowAngles.push_back( strTo<int>( angle ) );
	}
	_thresold = xmlthresold.attribute( "value" ).as_float();
}

void Mover::update( float dt )
{
	if( _currentSegment >= _route.size() )
		return;
	Point direction;

	float moveVelocity = _velocity;
	if( moveVelocity > 0 )
	{
		while( _currentSegment < _route.size() )
		{
			direction = _route[_currentSegment] - getPosition();
            float T;
            if (_isFlappy)
            {
                
                T = direction.getDistance( Point::ZERO ) / (moveVelocity*dt);
                if( T < 20 )
                {
                    ++_currentSegment;
                    //_route.erase( _route.begin() );
                }
                else
                    break;
                
            }
            else
            {
                
                T = direction.getDistance( Point::ZERO ) / (moveVelocity*dt);
                if( T < 2 )
                {
                    ++_currentSegment;
                    //_route.erase( _route.begin() );
                }
                else
                    break;
            }
            
            
            
            
            
		}
		direction.normalize();

		float isometric = (1 + fabs( direction.x )) / 2;
		Point shift = direction * moveVelocity * dt * isometric;

        
        if (_isFlappy)
        {
            if (getPosition().y < _route[_currentSegment].y)
            {
                _currentVelocity = _jumpVelocity + (arc4random()%10)/10.0;
            }
            _currentVelocity -= _gravity;
            shift.y += _currentVelocity;
            if (_currentVelocity < -1)
            {
                _flappyRotation = 7;
            }
            else{
                _flappyRotation = -7;
            }
        }
        
        
		setLocation( _position + shift );


		if( _currentSegment == _route.size() )
		{
			onFinish();
		}
	}
}

void Mover::setRoute( const std::vector<Point> & route )
{
	_route = route;
	_currentSegment = 0;

	if( _currentSegment == _route.size() )
	{
		onFinish();
	}
	else
	{
		Vec2 direction = _route.size() > 1 ? _route[1] - _route[0] : Vec2( 1, 0 );
		direction.normalize();
		setDirection( direction );
		setLocation( _route[0] );
	}
}

const std::vector<Point>& Mover::getRoute( )const
{
	return _route;
};

Point Mover::getPosition()const
{
	return _position;
}

void Mover::setRouteCurrentSegment( size_t points, bool instant )
{
	_syncSegment = points;

	if( instant )
	{
		_currentSegment = points;
	}
	//float acceleraterate = 1.1f;
	//if( _currentSegment < points )
	//	_velocity *= acceleraterate;
	//else if( _currentSegment > points )
	//	_velocity /= acceleraterate;
}

size_t Mover::getRouteCurrentSegment()const
{
	return _currentSegment;
}

void Mover::setRouteCurrentSegmentPercent( float percent )
{
	if( _currentSegment > 0 && _currentSegment < _route.size() )
	{
		//auto pos = _position;
		//auto l = _route[_currentSegment - 1];
		//auto r = _route[_currentSegment];
		//auto current = (pos - l).lengthSquared() / (l - r).lengthSquared();
		bool low = _syncSegment < _currentSegment;
		bool high = _syncSegment > _currentSegment;

		float acceleraterate = 1.1f;
		if( high )
			_velocity *= acceleraterate;
		if( low )
			_velocity /= acceleraterate;
	}
}

float Mover::getRouteCurrentSegmentPercent()const
{
	if( _currentSegment > 0 && _currentSegment < _route.size() )
	{
		auto pos = _position;
		auto l = _route[_currentSegment-1];
		auto r = _route[_currentSegment];
		auto p = (pos - l).lengthSquared() / (l - r).lengthSquared();
		return p;
	}
	return 0;
}

void Mover::onFinish()
{
	if( _onFinish )
		_onFinish();
}

void Mover::setLocation( const Point & position )
{
	Vec2 direction = (position - _position).getNormalized();
	_position = position;
	setDirection( direction );

	if( _onChangePosition )
	{
		_onChangePosition( _position, _truncatedDirection );
	}
}


const Vec2& Mover::setDirection( const Vec2 & direction )
{
	if( direction.equals( Point::ZERO ) )
		return _truncatedDirection;
	auto as = [this]( float dir, float a )
	{
		auto ra = []( float a ){ while( a < 0 ) a += 360; return a; };
		bool A = ra( a ) < dir + _thresold;
		bool B = ra( a ) > ra( dir - _thresold );
		if( (dir - _thresold) < 0 ) return A || B;
		else return A && B;
	};

	float a = getDirectionByVector( direction );

	for( auto angle : _allowAngles )
	{
		if( as( angle, a ) )
		{
			_currentAngle = angle;
			break;
		}
	}

	return _truncatedDirection;
}

const Vec2& Mover::getDirection()const
{
	return _truncatedDirection;
}

const float Mover::getRandomAngle( )const
{
	if( _allowAngles.size() == 0 )return 0;
	
	int index = rand() % _allowAngles.size();
	return _allowAngles[index];
}



NS_CC_END
