#include "Airbomb.h"
#include "ml/loadxml/xmlLoader.h"
#include "GameLayer.h"

NS_CC_BEGIN

Airbomb::Airbomb()
{}

Airbomb::~Airbomb()
{}

bool Airbomb::init( GameBoard* board, const std::string & path, const std::string & xmlFile, const Point & position )
{
	do
	{
		CC_BREAK_IF( !NodeExt::init() );

		_targetPoint = position;

		int count( 3 );
		std::vector<Point> positions;
		for( int i = 0; i < count; ++i )
		{
			Point pos = position;
			positions.push_back( pos );
			xmlLoader::macros::set( "airplane_bomb_posx" + toStr( i+1 ), toStr( pos.x ) );
			xmlLoader::macros::set( "airplane_bomb_posy" + toStr( i+1 ), toStr( pos.y ) );
		}

		Unit::init( board, path, xmlFile );

		for( int i = 0; i < count; ++i )
		{
			xmlLoader::macros::erase( "airplane_bomb_posx" + toStr( i + 1 ) );
			xmlLoader::macros::erase( "airplane_bomb_posy" + toStr( i + 1 ) );

			Point pos = positions[i];
			auto actionBomb = dynamic_cast<FiniteTimeAction*>(getAction( toStr( i + 1 ) + "_bomb_move" ).ptr( ));
			float expl = actionBomb->getDuration( );
			auto exp = Sequence::createWithTwoActions( DelayTime::create( expl ), CallFunc::create( [this, pos]( ){this->explosion( pos ); } ) );
			runAction( exp );
		}

		auto actionPlace = dynamic_cast<FiniteTimeAction*>(getAction( "3_place" ).ptr());
		float life = actionPlace->getDuration();
		auto die = Sequence::createWithTwoActions( DelayTime::create( life ), CallFunc::create( [this](){this->die(); } ) );
		runAction( die );

		runEvent( "run" );

		return true;
	}
	while( false );
	return false;
}

void Airbomb::explosion( const Point & position )
{
	Point pos = getPosition();
	int z = getLocalZOrder();
	setPosition( position );
	_board->applyDamageBySector( this );
	setPosition( pos );
	setLocalZOrder( z );
	_board->getGameLayer()->shake();
}

void Airbomb::die()
{
	removeFromParent();
}





NS_CC_END
