//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "gameboard.h"
#include "tower.h"
#include "GameLayer.h"
#include "GameScene.h"
#include "support.h"
#include "ScoreCounter.h"
#include "ml/pugixml/pugixml.hpp"
#include "consts.h"
#include "ml/Animation.h"
#include "UserData.h"
#include "ml/Audio/AudioEngine.h"
#include "ml/ImageManager.h"
#include "Log.h"
#include "Achievements.h"
#include "MenuCreateTower.h"
#include "flurry/flurry_.h"
#include "UnitWithFadeEffects.h"
#include "UnitDesant.h"
#include "UnitUnstoppable.h"
#include "Airbomb.h"
#include "LandMine.h"
#include "configuration.h"
#include "LoadLevelScene.h"
#include "playservices/playservices.h"
#include "online/CommandsDispatcher.h"
#include "online/GameBoardOnline.h"
#include "EasterEggsHandler.h"
#include "Tutorial.h"

NS_CC_BEGIN;

bool isExistCreep( const std::vector<Unit::Pointer> & units )
{
	for( auto& unit : units )
	{
		if( unit->getType() == UnitType::creep )
			return true;
	}
	return false;
};

void checkDefaultBonusesItems()
{
	std::string id = "bonusitemdefaultgetted";
	if (UserData::shared().get(id, true))
	{
#if CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID
		try
		{
#endif
			pugi::xml_document doc;
			doc.load_file( "ini/bonusitems.xml" );
			auto root = doc.root().first_child();
			for( auto xmlNode : root )
			{
				std::string name = xmlNode.name();
				auto index = xmlNode.attribute( "default" ).as_int();
				auto count = index;
				if( xmlNode.attribute( "initcount" ) )
					count = xmlNode.attribute( "initcount" ).as_int();
				UserData::shared().bonusitem_add( index, count );
			}

			UserData::shared().write( id, false );
#if CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID
		}
		catch( ... )
		{
			UserData::shared().bonusitem_add( 3, 1 );
			UserData::shared().bonusitem_add( 2, 1 );
			UserData::shared().bonusitem_add( 1, 1 );
		}
#endif
	}
}

std::vector<unsigned> GameBoard::heroesOnLevel;

GameBoard::GameBoard()
	: _gameLayer( nullptr )
, _waveGenerator( this )
, _unitsLocked( 0 )
, _units()
//, _damagers()
, _creepsRoutes()
, _statisticsParams()
, _gameMode( GameMode::normal )
, _levelIndex( 0 )
, _isGameStarted( false )
, _isFinihedWaves( false )
, _isFinihedGame( false )
, _isFinishedWave( false )
, _heartsForStar1( 0 )
, _heartsForStar2( 0 )
, _heartsForStar3( 0 )
, _levelStartTime( 0 )
, _startHealth( 0 )
, _waitAfterLose( false )
, _refreshTargetsCounter( 0.f )
, _refreshTargetsPeriod( 0.1f )
, _peaceSound("")
, _battleSound("")
{
    _desant = nullptr;
    _desant2 = nullptr;
	pugi::xml_document doc;
	doc.load_file( "ini/skills.xml" );
	auto root = doc.root().first_child();

	_skillParams.distanceToRoute = root.attribute( "distance_to_road" ).as_float();
	for( auto xmlSkill : root )
	{
		std::string name = xmlSkill.name();
		SkillParams::Params& params = _skillParams.skills[name];
		for( auto xmlLevel : xmlSkill )
		{
			params.count.push_back( xmlLevel.attribute( "count" ).as_uint( 1 ) );
			params.cooldown.push_back( xmlLevel.attribute( "cooldown" ).as_float() );
			params.lifetime.push_back( xmlLevel.attribute( "lifetime" ).as_float() );
		}
	}

	checkDefaultBonusesItems();
};

GameBoard::~GameBoard()
{
}

void GameBoard::setLayer( GameLayer* layer )
{
	_gameLayer = layer;
}

void GameBoard::clear()
{
	__push_auto_check( "GameBoard::clear" );
	dispatchLeaderboards();
	auto desSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();

	_units.clear();

	_creepsRoutes.clear();

	_statisticsParams.crystalscount = 0;
	_statisticsParams.scores = 0;
	_statisticsParams.spentscores = 0;
	_statisticsParams.livecurrent = 0;
	_statisticsParams.livetotal = 0;

	_levelIndex = -1;
	_isFinihedWaves = false;
	_isFinihedGame = false;
	_isFinishedWave = false;
	_heartsForStar1 = 1;
	_heartsForStar2 = 1;
	_heartsForStar3 = 1;

	//_damagers.clear();
}

void GameBoard::loadLevel( int index, GameMode mode )
{
	_levelIndex = index;
	_gameMode = mode;

	_units.emplace( 0, std::vector<Unit::Pointer>() );
	_units.emplace( 1, std::vector<Unit::Pointer>() );

	std::string mapFile;
	if( mode != GameMode::multiplayer )
		mapFile = "map" + toStr( _levelIndex ) + ".xml";
	else
		mapFile = "online_" + toStr( _levelIndex ) + ".xml";
	std::string pathToFile = FileUtils::getInstance()->fullPathForFilename( kDirectoryToMaps + mapFile );
	pugi::xml_document doc;
	doc.load_file( pathToFile.c_str() );
	auto root = doc.root().first_child();
	if( !root )
		log( "cannot parce file" );
	
	std::string xmlTagWaves;
	std::string xmlTagParams;
	std::string xmlTagUnits;
	switch( mode )
	{
		case GameMode::multiplayer:
		case GameMode::normal:
			xmlTagParams = k::xmlTag::LevelParams;
			xmlTagWaves = k::xmlTag::LevelWaves;
			xmlTagUnits = k::xmlTag::LevelUnits;
			break;
		case GameMode::hard:
			xmlTagParams = k::xmlTag::LevelParamsHard;
			xmlTagWaves = k::xmlTag::LevelWavesHard;
			xmlTagUnits = k::xmlTag::LevelUnitsHard;
			break;
		case GameMode::survival:
			xmlTagParams = k::xmlTag::LevelParamsSurvival;
			xmlTagWaves = k::xmlTag::LevelWavesSurvival;
			xmlTagUnits = k::xmlTag::LevelUnits;
			break;
	}
    
    for( auto attr = root.first_attribute(); attr; attr = attr.next_attribute() )
    {
        const std::string name = attr.name();
        if (name == "peaceSound")
        {
            _peaceSound = attr.value();
        }
        if (name == "battleSound")
        {
            _battleSound = attr.value();
        }
    }
	
	auto eventsXml = root.child( k::xmlTag::LevelEvents );
	auto routesXml = root.child( k::xmlTag::LevelRoutes );
	auto placesXml = root.child( k::xmlTag::LevelTowerPlaces );
	auto wavesXml = root.child( xmlTagWaves.c_str() );

	auto unitsXml = root.child( xmlTagUnits.c_str() );
	if( !unitsXml ) unitsXml = root.child( k::xmlTag::LevelUnits );

	auto paramsXml = root.child( xmlTagParams.c_str() );
	if( !paramsXml ) paramsXml = root;
	
	if( !routesXml )log( "routesXml Node not found" );
	if( !placesXml )log( "placesXml Node not found" );
	if( !wavesXml ) log( "wavesXml Node not found" );

	std::map<int, TripleRoute>routesload;
	std::vector<TripleRoute>routes;
	std::vector<TowerPlaseDef>places;

	loadLevelParams(paramsXml);
	loadRoutes( routesload, routesXml );
	loadTowerPlaces( places, placesXml );
	loadEvents( _events, eventsXml );
	loadUnits( _preloadedUnit, unitsXml, routesload );

	bool const infinityWaves =
		(_gameMode == GameMode::survival) ||
		(_gameMode == GameMode::multiplayer);

	_waveGenerator.load( wavesXml, infinityWaves );
	
	_gameLayer->loadLevel( index, root );
	_areaSize = _gameLayer->getMainLayer()->getContent();

	for( auto i : routesload )
		routes.push_back( i.second );
	setCreepsRoutes( routes );
	auto pos = routes.front().main.front();
//	CCLOG("position :: %f %f",pos.x,pos.y);
	for( auto i : places )
	{
		if( _gameMode == GameMode::multiplayer )
		{
			auto online = dynamic_cast<GameBoardOnline*>(this);
			i.opponent = online ? online->getType() == GameBoardType::opponent : false;
		}
		_gameLayer->addTowerPlace( i );
	}

	int minlevel = Config::shared().get<int>( "minLevelHero" );
	bool createHeroes = mode == GameMode::multiplayer;
	createHeroes = createHeroes || (Config::shared().get<bool>( "useHero" ) && minlevel <= index);
	if( createHeroes )
	{
		std::list<std::string> excluded;
		split( excluded, paramsXml.attribute( "exclude" ).as_string() );
		if( std::find( excluded.begin(), excluded.end(), "hero" ) == excluded.end() )
		{
			this->createHeroes();
		}
	}
	createPreloadedUnits( 0 );
}

void GameBoard::loadUnits( std::list< std::pair<float, Unit::Pointer> > & units, const pugi::xml_node & node, const std::map<int, TripleRoute>& routes )
{
	for( auto child : node )
	{
		size_t routeindex = child.attribute( "routeindex" ).as_uint( -1 );
		std::string name = child.attribute( "name" ).as_string();
		CCLOG("unit name :: %s",name.c_str());
		float delay = child.attribute( "delay" ).as_float();
		child.remove_attribute( "routeindex" );
		child.remove_attribute( "name" );

		std::string path = "ini/units/" + name + ".xml";
		child.append_attribute( xmlLoader::ksTemplate.c_str() ).set_value( path.c_str() );
		auto node = xmlLoader::load_node( child );
		auto unit = dynamic_cast<Unit*>( node.ptr() );

		if( routeindex != -1 )
		{
			auto iter = routes.find( routeindex );
			if( iter != routes.end() )
			{
				auto& route = iter->second;
				unit->getMover().setRoute( route.main );
				unit->move();
			}
		}
		units.emplace_back( delay, unit );
	}
}

void GameBoard::loadLevelParams( pugi::xml_node & node )
{
	int startscore = node.attribute( k::xmlAttr::LevelStartScore ).as_int();
	int healths = node.attribute( k::xmlAttr::LevelHealth ).as_int();
	_heartsForStar1 = node.attribute( k::xmlAttr::LevelStartStar1 ).as_int();
	_heartsForStar2 = node.attribute( k::xmlAttr::LevelStartStar2 ).as_int();
	_heartsForStar3 = node.attribute( k::xmlAttr::LevelStartStar3 ).as_int();
	std::list<std::string> exclude;
	split( exclude, node.attribute( "exclude" ).as_string() );
	
	if( Config::shared().get<bool>( "useBoughtLevelScoresOnEveryLevel" ) )
	{
        std::string key = k::user::BoughtScores + toStr( kScoreLevel );
        int boughtScores = UserData::shared().get(key, 0);

        auto onlineboard = dynamic_cast<GameBoardOnline*>(this);
        if( onlineboard && onlineboard->getType() == GameBoardType::opponent )
            boughtScores = 0;
		startscore += boughtScores;
	}

	for( const auto& tower : exclude )
	{
		_gameLayer->excludeTower( tower );
	}
    
    if(EasterEggsHandler::getInstance()->isEasterEggUnlocked(EasterEggType::kDroneTower))
    {
        startscore += 100;
    }
	
	setStartScore( startscore );
	setStartHealth( healths );

	_statisticsParams.livetotal = healths;
	_statisticsParams.livecurrent = healths;
	_statisticsParams.stars = 3;
}

void GameBoard::addLevelScore( int score )
{
	ScoreCounter::shared().addMoney( kScoreLevel, score, false );
}

void GameBoard::subHealth( int score )
{
	ScoreCounter::shared().subMoney( kScoreHealth, score, false, "game:playerdamage" );
}

void GameBoard::setStartScore( int score )
{
	ScoreCounter::shared().setMoney( kScoreLevel, score, false );
}

void GameBoard::setStartHealth( int score )
{
	_startHealth = score;
	setHealth( score );
}

int GameBoard::getStartHealth()
{
	return _startHealth;
}

void GameBoard::setHealth( int score )
{
	ScoreCounter::shared().setMoney( kScoreHealth, score, false );
}

void GameBoard::startGame()
{
	_waveGenerator.start();
	_gameLayer->startGame();
	time( &_levelStartTime );
	online_onGameStarted();
}

void GameBoard::dispatchLeaderboards()
{
	int score = _statisticsParams.scores;
	if( _gameMode != GameMode::survival )
		score *= _statisticsParams.livecurrent;
	Leaderboard::shared().fix( _levelIndex, score );
}

void GameBoard::loadRoutes( std::map<int, TripleRoute> & routes, const pugi::xml_node & node )
{
	routes.clear();
	
	for( auto child = node.first_child(); child; child = child.next_sibling() )
	{
		int index = child.attribute( k::xmlAttr::name ).as_int();
		auto unitLayer = strToUnitLayer( child.attribute( k::xmlAttr::type ).as_string() );
		auto onlyForHero = child.attribute( "onlyforhero" ).as_bool();
		auto i = routes.insert( std::pair<int, TripleRoute>( index, TripleRoute() ) );
		if( i.second )
		{
			pugi::xml_node main = child.child( "main" );
			pugi::xml_node left = child.child( "left" );
			pugi::xml_node right = child.child( "right" );
			loadRoute( i.first->second.main, main, 0.0f );
			loadRoute( i.first->second.left, left, .0f );
			loadRoute( i.first->second.right, right, .0f);

			i.first->second.type = unitLayer;
			i.first->second.onlyForHero = onlyForHero;
		}
		else
		{
			log( "duplicate routes by index. index[%d]", index );
		}
	}
}

void GameBoard::loadRoute( Route & route, pugi::xml_node & node, float offset )
{
	route.clear();
	for( auto child = node.first_child(); child; child = child.next_sibling() )
	{
		Point point;
		point.x = (child.attribute( "x" ).as_float() ) * DesignScale;
		point.y = (child.attribute( "y" ).as_float() + offset) * DesignScale;
		route.push_back( point );
	}
}

void GameBoard::loadTowerPlaces( std::vector<TowerPlaseDef> & places, const pugi::xml_node & node )
{
	places.clear();
	for( pugi::xml_node place = node.first_child(); place; place = place.next_sibling() )
	{
		TowerPlaseDef def;
		def.position.x = place.attribute( "x" ).as_float() * DesignScale;
		def.position.y = place.attribute( "y" ).as_float() * DesignScale;
		def.isActive = place.attribute( "active" ).as_bool();
		places.push_back( def );
	}
}

void GameBoard::loadEvents( std::map<std::string, EventsList> & eventsMap, const pugi::xml_node & node )
{
	for( auto xmlEventList : node )
	{
		std::string name = xmlEventList.attribute("name").as_string();
		auto& eventsList = eventsMap[name];
		for( auto xmlEvent : xmlEventList )
		{
			auto event = xmlLoader::load_event( xmlEvent );
			if( event )
				eventsList.push_back( event );
		}
	}
}

void GameBoard::runEvent( const std::string& eventname )
{
	auto iter = _events.find( eventname );
	if( iter != _events.end() )
		iter->second.execute( _gameLayer );
}

int GameBoard::getScoreForUnit( Unit::Pointer unit )
{
	auto damager = unit->getCurrentDamager();
	int score = unit->getHealth() * unit->getRate();
	if( damager )
		score = score / (damager->getLevel() + 1) * 3;
	return score;
}

void GameBoard::writeGameScore( int score )
{
	ScoreCounter::shared().setMoney( kScoreSurvival, score, false );
}

void GameBoard::onPredelayWave( const WaveInfo & wave, float delay )
{
	if( wave.index == 0 )
	{
		_gameLayer->onFirstWave();
	}
	bool wascreateIcon( false );
	for( size_t i = 0; i < _creepsRoutes.size(); ++i )
	{
		const auto& route = _creepsRoutes[i];
		auto routeindex = wave.routeIndex.begin();
		auto creep = wave.creeps.begin();
		bool create( false );
		for( ; routeindex != wave.routeIndex.end(); ++routeindex, ++creep )
		{
			if( *routeindex == i )
			{
				if( mlUnitInfo::shared().info( *creep ).layer == route.type )
				{
					create = true;
				}
			}
		}
		if( create )
		{
			if(!wascreateIcon && _gameLayer->getInterface() && !_gameLayer->getInterface()->getIsOnlyScroll())
            {
                _gameLayer->getInterface()->createIconForWave( route.main, wave, route.type, std::list<std::string>(), delay );
            }
            wascreateIcon = true;
		}
	}
	_waveGenerator.pause();
	if( wascreateIcon == false )
	{
#if USE_CHEATS == 1
		auto name = wave.creeps.empty() == false ? wave.creeps.front() : "empty wave";
		MessageBox( ("I cannt define route for creep in wave. \nCreep name: [" + name + "]").c_str(), "level waves error" );
#else
		_waveGenerator.resume();
#endif
	}
}

void GameBoard::onStartWave( const WaveInfo & wave )
{
	_isGameStarted = true;
	int index = _waveGenerator.getWaveIndex();

	runEvent( "startwave" + toStr( index ) );

	std::string gamemode;
	switch( _gameMode )
	{
	case GameMode::multiplayer:	gamemode = "multiplayer";	break;
	case GameMode::normal:		gamemode = "normal";		break;
	case GameMode::hard:		gamemode = "hard";			break;
	case GameMode::survival:	gamemode = "survival";		break;
	default:
		break;
	}
	runEvent( "startwave" + toStr( index ) + gamemode );
    
    const std::string& battleSound = wave.soundOnWave == "" ? getBattleSound() == "" ? kMusicGameBattle : getBattleSound()
                                                            : wave.soundOnWave;
    AudioEngine::shared().playMusic( battleSound );

	_gameLayer->onStartWave( wave );
	auto sound = xmlLoader::macros::parse( "##sound_wavestart##" );
	AudioEngine::shared().playEffect( sound, false, 0 );
	event_startwave( index );
	online_onWaveWasStarted();
}

void GameBoard::onFinishWave()
{
	_isFinishedWave = true;
	++_statisticsParams.wavesCompleteCount;
}

void GameBoard::onFinishWaves()
{
	_isFinihedWaves = true;
}

void GameBoard::finishGame()
{
	if( _isFinihedGame )
		return;
	lockUnits();
    
	for( auto& unit : _units[0] )
		unit->stop();
	for( auto& unit : _units[1] )
		unit->stop();

	unlockUnits();

	dispatchDamagers();
	dispatchKillers();
    
    _statisticsParams.livecurrent = ScoreCounter::shared().getMoney( kScoreHealth );

    if(_statisticsParams.livecurrent > 0){
        for( auto hero : _heroes )
        {
            // #change exp here
            float exp = HeroExp::shared().getEXP( hero->getName() );
            exp += HeroExp::shared().getExpOnLevelFinished( _levelIndex );
            HeroExp::shared().setEXP( hero->getName(), exp );
        }
    }
	_statisticsParams.stars = 0;
	if( _statisticsParams.livecurrent >= _heartsForStar1 ) _statisticsParams.stars = 1;
        if( _statisticsParams.livecurrent >= _heartsForStar2 ) _statisticsParams.stars = 2;
	if( _statisticsParams.livecurrent >= _heartsForStar3 ) _statisticsParams.stars = 3;

	if( _statisticsParams.livecurrent > 0 )
	{
		if( _gameMode != GameMode::multiplayer )
		{
			UserData::shared().level_complete( _levelIndex );
			UserData::shared().level_getScoresByIndex( _levelIndex );
			UserData::shared().level_setScoresByIndex( _levelIndex, _statisticsParams.stars );
		}
		//dispatchEvent( "finishgame" );

		if( this->_gameMode == GameMode::normal )
		{
			if( UserData::shared().get <bool>( "level_successfull" + toStr( _levelIndex ) ) == false )
			{
				Achievements::shared().process( "level_successfull", 1 );
				UserData::shared().write( "level_successfull" + toStr( _levelIndex ), true );
			}
			if( _statisticsParams.stars >= 3 && UserData::shared().get <bool>( "level_star3normal" + toStr( _levelIndex ) ) == false )
			{
				Achievements::shared().process( "level_star3normal", 1 );
				UserData::shared().write( "level_star3normal" + toStr( _levelIndex ), true );
			}
		}
		else
		{
			if( UserData::shared().get <bool>( "level_successfull_hard" + toStr( _levelIndex ) ) == false )
			{
				Achievements::shared().process( "level_successfull_hard", 1 );
				UserData::shared().write( "level_successfull_hard" + toStr( _levelIndex ), true );
			}
			if( _statisticsParams.stars >= 3 && UserData::shared().get <bool>( "level_star3hard" + toStr( _levelIndex ) ) == false )
			{
				Achievements::shared().process( "level_star3hard", 1 );
				UserData::shared().write( "level_star3hard" + toStr( _levelIndex ), true );
			}
		}

		if( _statisticsParams.stars >= 3 && UserData::shared().get <bool>( "level_3star" + toStr( _levelIndex ) ) == false )
		{
			Achievements::shared().process( "level_3star", 1 );
			UserData::shared().write( "level_3star" + toStr( _levelIndex ), true );
		}
	}
	else
	{
		Achievements::shared().process( "level_failed", 1 );
	}
	dispatchLeaderboards();
	mlTowersInfo::shared().checkAvailabledTowers();
	HeroExp::shared().checkUnlockedHeroes();

	_gameLayer->getGameScene()->onLevelFinished( _statisticsParams );

	_isFinihedGame = true;
	onFinishGame( _statisticsParams.livecurrent > 0 );
}

void GameBoard::lockUnits()
{
	++_unitsLocked;
}

void GameBoard::unlockUnits()
{
	--_unitsLocked;
	assert( _unitsLocked >= 0 );
	if( _unitsLocked == 0 && _unitsAdd.empty() == false )
	{
		_units[0].insert( _units[0].end(), _unitsAdd[0].begin(), _unitsAdd[0].end() );
		_units[1].insert( _units[1].end(), _unitsAdd[1].begin(), _unitsAdd[1].end() );
		_unitsAdd[0].clear();
		_unitsAdd[1].clear();
	}
}

void GameBoard::onFinishGame( bool victory )
{
	_gameLayer->onFinishGame( &_statisticsParams, &_easterParams );
	event_levelFinished();
	observerLevelFinished.pushevent( _statisticsParams.stars > 0 );
	online_onGameFinished( victory );
}

bool GameBoard::isGameStarted()
{
	return _isGameStarted;
}

void GameBoard::onDamage( Unit* damager, Unit*target, float damage )
{
	//_damagers[damager] += damage;
}

void GameBoard::onKill( Unit * damager, Unit * target )
{
	//_killers[damager].push_back( target );

	if( target != nullptr && target->getType() == UnitType::creep )
	{
		++_statisticsParams.creepKillsCount;
	}
}

void GameBoard::pauseWaves() 
{
}

void GameBoard::resumeWaves()
{
	_waveGenerator.resume();
}

bool GameBoard::isTowerPlace( Point & location )
{
	auto index = _gameLayer->getTowerPlaceIndex( location );
	return index != -1;
}

Unit::Pointer GameBoard::createTower( const std::string & name, bool spentScore )
{
	auto place = _gameLayer->getSelectedTowerPlaces();
	if( !place )
		return nullptr;
	return createTower( name, place, spentScore );
}

Unit::Pointer GameBoard::createTower( const std::string & name, const Point& position, bool spentScore )
{
	auto place = _gameLayer->getTowerPlaceInLocation( position );
	if( !place ) return nullptr;
	return createTower( name, place, spentScore );
}

Unit::Pointer GameBoard::createTower( const std::string & name, TowerPlace* place, bool spentScore )
{
	if( spentScore )
	{
		int cost = mlTowersInfo::shared().getCost( name, 1 );
		if( cost > ScoreCounter::shared().getMoney( kScoreLevel ) )
		{
			AudioEngine::shared().playEffect( kSoundGameTowerBuyCancel );
			return nullptr;
		}
		ScoreCounter::shared().subMoney( kScoreLevel, cost, false, "game:buildtower:" + name );
		_statisticsParams.spentscores += cost;
		AudioEngine::shared().playEffect( kSoundGameTowerBuy );
		Achievements::shared( ).process( "spend_gold", cost );
		Achievements::shared().process( "build_tower", 1 );
	}
	//#parth change
	int towerStartLevel = 1;//UserData::shared().tower_upgradeLevel( name );
	auto tower = buildTower( name, towerStartLevel, nullptr );
	tower->setPosition( place->getPosition() );
	tower->setCurrentHealth( 100 );
	
	addUnit( tower );

	online_onTowerWasCreated( tower, place );
	_gameLayer->eraseTowerPlace( place );
	_gameLayer->onCreateUnit( tower );
	event_towerBuild( place, tower );
	return tower;
}

void GameBoard::createPreloadedUnits( float dt )
{
	if( _preloadedUnit.empty() )
		return;
	for( auto iter = _preloadedUnit.begin(); iter != _preloadedUnit.end(); )
	{
		iter->first -= dt;
		if( iter->first <= 0 )
		{
			iter->second->setGameBoard( this );
			addUnit( iter->second );
			iter = _preloadedUnit.erase( iter );
		}
		else
		{
			++iter;
		}
	}
}

Unit::Pointer GameBoard::createActiveSkillUnit( const std::string & name, const Point& position, float lifetime, int count, int skillDirection )
{
	if (name.empty()) return NULL;

	std::string xmlfile = name + ".xml";
	
	auto distance( 0.f );
	Unit::Pointer unit;
	const mlUnitInfo::Info& info = mlUnitInfo::shared().info( name );
	auto unitlayer = info.layer;
	auto radius = info.radius;
	auto level = 0u;
	if( _skillParams.skills.find( name ) != _skillParams.skills.end() )
	{
		count = _skillParams.skills.at( name ).count[level];
		lifetime = _skillParams.skills.at( name ).lifetime[level];
	}

	for( auto i = 0; i < count;  ++i )
	{
		auto pos = count > 1 ? 
			getRandPointInPlace( position, info.radius / 4 ) :
			position;

		if( checkPointOnRoute( _creepsRoutes, position, _skillParams.distanceToRoute, unitlayer, &distance ) )
		{
			if( info.type == UnitType::desant )
			{
				UnitDesant::Pointer desant = UnitDesant::create( this, "ini/units", xmlfile );
				desant->setBasePosition( pos );
				unit = desant;
			}
			else if( info.type == UnitType::airbomb )
			{
				auto bomb = Airbomb::create( this, "ini/units", xmlfile, position );
				bomb->setName( name );
				_gameLayer->addObject( bomb, 9999 );
				online_onSkillWasUsed( bomb, position );
				return bomb;
			} 
			else if( info.type == UnitType::unstoppable )
			{
				UnitUnstoppable::Pointer unstoppable = UnitUnstoppable::create( this, "ini/units", xmlfile );
				unstoppable->setPosition( pos );
				unit = unstoppable;
			}
			else
			{
				unit = Unit::create( this, "ini/units", xmlfile );
			}
		}
		if( unit )
		{
			unit->setPosition( pos );
			unit->setScaleX(-1*skillDirection);
			addUnit( unit );
			if( info.type == UnitType::desant )
            {
                _desant =  dynamic_cast<UnitDesant*>(unit.ptr());
//                _desants.push_back( std::pair< Unit::Pointer, float >( unit, lifetime ) );
            }
		}
	}
	if( unit )
		online_onSkillWasUsed( unit, position );
	return unit;
}

Unit::Pointer GameBoard::createBonusItem( const Point & position, const std::string & name )
{
	float dist( 9999 );
	auto layer = mlUnitInfo::shared().info( name ).layer;
	if( !checkPointOnRoute( _creepsRoutes, position, _skillParams.distanceToRoute, layer, &dist ) )
		return nullptr;

	Unit::Pointer item;
	item = Unit::create( this, "ini/units", name + ".xml" );
	item->setPosition( position );
	addUnit( item );
	item->getMover().setLocation( position );

	online_onBonusItemWasUsed( name, position );

	return item;
}

void GameBoard::addUnit( Unit::Pointer tower )
{
	int index = tower->getType() == UnitType::creep ? 0 : 1;
	if( _unitsLocked == 0 )
		_units[index].push_back( tower );
	else
		_unitsAdd[index].push_back( tower );
	_gameLayer->addObject( tower, zorder::tower );
}


void GameBoard::stopAllUnitsSounds()
{
    for (auto unit : _units[1])
    {
        unit->stopAllLoopedSounds();
    }

}



void GameBoard::createHeroes()
{
	auto decorations = _gameLayer->getDecorations( "base_point" );
	size_t min( -1 );
	float dist_min( 99999999.f );
	size_t index( 0 );
	for( auto decor : decorations )
	{
		assert( decor );
		float dist( 0 );
		checkPointOnRoute( _creepsRoutes, decor->getPosition(), dist_min, UnitLayer::earth, &dist );
		if( dist < dist_min )
		{
			min = index;
			dist_min = dist;
		}
		++index;
	}
	if( min == -1 )
	{
#if USE_CHEATS
		MessageBox( "Level havent basepoint decoration", "Create hero error" );
#endif
		return;
	}
	auto decor = decorations[min];
	auto center = decor->getPosition();

	std::vector<unsigned> heroes;
	if( GameBoard::heroesOnLevel.empty() )
		heroes = UserData::shared().hero_getSelected();
	else
		heroes = GameBoard::heroesOnLevel;
	
	float radius = heroes.size() == 1 ? 0 : 20;
	std::vector<Point> positions;
	computePointsByRadius( positions, radius, heroes.size() );

	for( size_t i = 0; i < heroes.size(); ++i )
	{
		auto index = heroes[i];
		auto pos = positions[i] + center;
		std::vector<unsigned> dummySkills;
		createHero( index, pos, -1, dummySkills );
        if (i==0)
        {
			int minleveldesant = Config::shared().get<int>( "minLevelDesant" );
			if (_levelIndex >= minleveldesant) {
				createDesant(pos-Vec2(50,60),1);
			}
			
            bool isDuplicateDesantUnlocked = EasterEggsHandler::getInstance()->isEasterEggUnlocked(kDuplicateDesant);
            if(isDuplicateDesantUnlocked)
            {
                createDesant(pos-Vec2(40,-20),2);
            }
        }
	}
	for( auto hero : _heroes )
	{
		_gameLayer->onCreateUnit( hero );
	}
	if( _gameLayer->getInterface() )
		_gameLayer->getInterface()->createHeroMenu();

	runEvent( "oncreateheroes" );
}

Hero::Pointer GameBoard::createHero( int index, const Point& position, int level, const std::vector<unsigned>& skills )
{
	index = index + 1;
	auto hero = xmlLoader::load_node<Hero>( "ini/units/hero/hero" + toStr( index ) + ".xml" );
	hero->setGameBoard( this );
	hero->initSkills( skills, level );
    hero->setPosition( position + Vec2(1,0) );
    hero->moveTo( position );
	addUnit( hero );

	_heroes.push_back( hero );

	return hero;
}

UnitDesant::Pointer GameBoard::createDesant(const Point& position, int type)
{
    std::string xml;
    if (type == 1)
    {
        xml = "desant.xml";
    }
    else
    {
        xml = "desant2.xml";
    }
    UnitDesant::Pointer desant = UnitDesant::create( this, "ini/units", xml );
    desant->setBasePosition( position );
    desant->setPosition( position + Vec2(1,0) );
    desant->moveTo( position );
    addUnit( desant );
    if(type == 1)
    {
        _desant = desant.ptr();
    }
    else
    {
        _desant2 = desant.ptr();
    }
//    _desants.push_back( std::pair< Unit::Pointer, float >( desant, 0 ) );
    return desant;
}

UnitDesant* GameBoard::getDesant()
{
    return _desant;
}
UnitDesant* GameBoard::getDesant2()
{
    return _desant2;
}

Unit::Pointer GameBoard::buildTower( const std::string & name, int level, Unit* unit )
{
	std::string resource = name + toStr(level);
	if( LoadLevelScene::getInstance() )
		LoadLevelScene::getInstance()->loadInGameResources(resource);
	
	std::string xmlfile = name + toStr( level ) + ".xml";
	Unit::Pointer tower = Unit::create( this, "ini/units", xmlfile, unit );
	assert( tower );
	return tower;
}

Unit::Pointer GameBoard::buildCreep( const std::string & name )
{
	Unit::Pointer creep;
	if( !creep )
		creep = UnitWithFadeEffects::create( this, "ini/units", name + ".xml" );
	assert( creep );
	creep->setName( name );

	assert( creep );
	return creep;
}

Unit::Pointer GameBoard::upgradeTower( Unit::Pointer tower, bool ignoreMaxUserLevel, bool spentScore )
{
	if( tower == nullptr )
		return nullptr;
	std::string name = tower->getName();
	unsigned level = tower->getLevel();
	unsigned maxlevel = tower->getMaxLevel();
	unsigned maxlevel2 = tower->getMaxLevelForLevel();
    if( level >= maxlevel )
		return nullptr;
	if( !ignoreMaxUserLevel && level >= maxlevel2 )
		return nullptr;

	auto iter = std::find( _units[1].begin(), _units[1].end(), tower );
	if( iter == _units[1].end() )
		return nullptr;

	if( spentScore )
	{
		unsigned cost = mlTowersInfo::shared().getCost( name, level + 1 );
		unsigned score = ScoreCounter::shared().getMoney( kScoreLevel );
		if( score < cost )
			return nullptr;
		AudioEngine::shared().playEffect( kSoundGameTowerUpgrade );
		ScoreCounter::shared().subMoney( kScoreLevel, cost, false, "game:upgradetower:" + name );
		if( (level + 1) == maxlevel ) Achievements::shared().process( "upgrade_towermax", 1 );
		_statisticsParams.spentscores += cost;
		std::string nameevent = "tower_upgrade_" + tower->getName() + toStr( tower->getLevel() );
		Achievements::shared().process( nameevent, 1 );
	}

	std::string resource = name + toStr(level+1);
	if( LoadLevelScene::getInstance() )
		LoadLevelScene::getInstance()->loadInGameResources(resource);
	
	auto newTower = buildTower( name, level + 1, tower );
	newTower->setLevel( level + 1 );
	newTower->setMaxLevel( maxlevel );
	newTower->setMaxLevelForLevel( maxlevel2 );
	newTower->setPosition( tower->getPosition() );
	newTower->setId( tower->getId() );
	assert( _unitsLocked == 0 );
	_units[1].erase( iter );
	_units[1].push_back( newTower );

	_gameLayer->removeObject( tower );
	_gameLayer->addObject( newTower, zorder::tower );

	event_towerUpgrade( tower );
	online_onTowerWasUpgraded( newTower );
 
    TutorialManager::shared().dispatch("level" + toStr( _levelIndex ) + "_upgradetower" );
    
	return newTower;
}

void GameBoard::removeTower( Unit::Pointer tower, bool spentScore, bool dead )
{
	auto& collection = dead ? _death : _units[1];
	auto i = std::find( collection.begin(), collection.end(), tower );
	
	if( i != collection.end() )
	{		
		assert( _unitsLocked == 0 );
		collection.erase( i );
		TowerPlaseDef def;
		def.position = tower->getPosition();
		def.isActive = true;
		tower->stopAllLoopedSounds();
		_gameLayer->addTowerPlace( def );
		_gameLayer->removeObject( tower );

		if( spentScore )
		{
			unsigned costSell = mlTowersInfo::shared().getSellCost( tower->getName(), tower->getLevel() );
			ScoreCounter::shared().addMoney( kScoreLevel, costSell, false );
			_statisticsParams.spentscores -= costSell;
			Achievements::shared().process( "collect_gold", costSell );
			Achievements::shared().process( "sell_tower", 1 );
			event_towerSell( tower );
			online_onTowerWasSelled( tower );
		}
	}
}

Unit* GameBoard::getTowerInLocation( const Point& location )
{
	float distSq = 10 * 10;
	lockUnits();
	for( auto unit : _units[1] )
	{
		if( unit->getType() == UnitType::tower &&
			unit->getPosition().getDistanceSq( location ) < distSq )
		{
			unlockUnits();
			return unit;
		}
	}
	unlockUnits();
	return nullptr;
}

const std::vector<Hero::Pointer>& GameBoard::getHeroes()
{
	return _heroes;
}

Unit::Pointer GameBoard::createCreep( const std::string & name, const Route & route, const Point & position )
{
	auto creep = buildCreep( name );
	creep->getMover().setRoute( route );
	creep->move();
	creep->getMover().setLocation( position );

	_gameLayer->onCreateUnit( creep );
	addUnit( creep );
	online_onCreepWasCreated( creep );
	return creep;
}

Unit::Pointer GameBoard::createCreep( const std::string & name, RouteSubType rst, unsigned routeIndex )
{
	auto creep = buildCreep( name );
	auto layer = creep->getUnitLayer();
	creep->getMover( ).setRoute( getRandomRoute( layer, routeIndex, rst ) );
	creep->move( );
	creep->setRouteIndex( routeIndex );
	creep->setRouteSubType( rst );

	_gameLayer->onCreateUnit( creep );
	addUnit( creep );
	online_onCreepWasCreated( creep );
	return creep;
}

void GameBoard::remove( Unit::Pointer unit )
{
	unit->stopAllLoopedSounds();
	int cost = unit->getLifeCost();
	if( cost > 0 )
	{
		auto sound = xmlLoader::macros::parse( "##sound_gameplayerdamage##" );
		AudioEngine::shared().playEffect( sound, false, 0 );
		Achievements::shared().process( "skip_enemies", 1 );
		subHealth( cost );
        if (unit->getName() == "easteregg") {
            _easterParams.isEasterEnemyPassed = true;
        }
	}
	_gameLayer->removeObject( unit );

	for( auto pair : _units )
	{
		for( auto base : pair.second )
		{
			std::vector<Unit::Pointer> targets;
			base->get_targets( targets );
			for( auto& target : targets )
			{
				if( target == unit )
				{
					base->capture_targets( std::vector<Unit::Pointer>() );
					break;
				}
			}
		}
	}
}

void GameBoard::preDeath( Unit::Pointer unit )
{
	unsigned cost = unit->getCost();
	addLevelScore( cost );
	Achievements::shared().process( "collect_gold", cost );
	Achievements::shared().process( "kill_enemies", 1 );
	_gameLayer->onDeathUnit( unit );

	for( auto pair : _units )
	{
		for( auto i : pair.second )
		{
			if( i->get_target() == unit )
				i->capture_target( nullptr );
		}
	}
//    for( auto iter = _desants.begin(); iter != _desants.end(); ++iter )
//    {
//        if( iter->first == unit )
//        {
//            _desants.erase( iter );
//            break;
//        }
//    }
	unit->capture_targets( std::vector<Unit::Pointer>() );
	unit->stop();
	unit->die();
	_death.push_back( unit );
}

void GameBoard::death( Unit::Pointer unit )
{
	auto iter = std::find( _death.begin(), _death.end(), unit );

	if( iter != _death.end() )
	{
		if( unit->getType() == UnitType::tower && mlTowersInfo::shared().isExist( unit->getName() ) )
		{
			removeTower( unit, false, true );
		}
		else
		{
			auto score = getScoreForUnit( unit );
			_statisticsParams.scores += score;

			unit->stopAllLoopedSounds();
			_gameLayer->removeObject( unit );

			unit->removeFromParent();
			writeGameScore( _statisticsParams.scores );
			_death.erase( iter );
		}

	}
}

bool GameBoard::checkWaveFinished()
{
	bool result = _isFinishedWave && isExistCreep( _units[0] ) == false;
	if( result )
	{
		_gameLayer->onWaveFinished();
	}
	return result;
}

bool GameBoard::checkGameFinished()
{
	bool finishGame( false );
	finishGame = finishGame || ( ScoreCounter::shared().getMoney( kScoreHealth ) <= 0 ) && ( _waitAfterLose == false );
	finishGame = finishGame || (isExistCreep( _units[0] ) == false && _isFinihedWaves == true);
	if( finishGame )
	{
		this->finishGame();
	}

	return finishGame;
}

void GameBoard::activateTowerPlace( TowerPlace::Pointer place )
{
    if( !place ) return;
    if( place->getActive() ) return;
	assert( place );
	assert( place->getActive() == false );
	unsigned cost = mlTowersInfo::shared().getCostFotDig();
	ScoreCounter::shared().subMoney( kScoreLevel, cost, false, "game:dig" );
	AudioEngine::shared().playEffect( kSoundGameTowerPlaceActivate );
	place->setActive( true );
}

void GameBoard::update( float dt )
{
	auto startTime = std::chrono::system_clock::now();

	updateSkills( dt );
	_waveGenerator.update( dt );

	if( !_isFinihedGame )
		refreshTargetsForTowers( dt );

	createPreloadedUnits( dt );

	auto deathCopy = _death;
	for( auto i : deathCopy ) 
		i->update( dt );
	
	lockUnits();
	for( auto pair : _units )
	{
		auto size = pair.second.size();
		for( auto i : pair.second )
		{
			i->update( dt );
			assert( size == pair.second.size() );
		}
	}
	
	unlockUnits();

	dispatchDeathUnits();
	checkWaveFinished();
	checkGameFinished();

	auto finishTime = std::chrono::system_clock::now();
	float elapsed = std::chrono::duration_cast<std::chrono::milliseconds>
		(finishTime - startTime).count() / 1000.f;
	float limit = 1.f / 30.f;
	if( elapsed > limit )
	{
		_refreshTargetsPeriod = std::max( _refreshTargetsPeriod, std::min( 1.f, elapsed * 2 ) );
	}
}

void GameBoard::updateSkills( float dt )
{
//    for( auto iter = _desants.begin(); iter != _desants.end(); )
//    {
//        iter->second -= dt;
//        if( iter->second <= 0 )
//        {
//            auto iUnit = std::find( _units[1].begin(), _units[1].end(), iter->first );
//            assert( iUnit != _units[1].end() );
//            if( iUnit != _units[1].end() )
//            {
//                assert( _unitsLocked == 0 );
//                _units[1].erase( iUnit );
//            }
//
//            remove( iter->first );
//            iter = _desants.erase( iter );
//        }
//        else
//        {
//            ++iter;
//        }
//    }
}

bool GameBoard::updateUnitSkills( Unit* unit, float dt )
{
	bool execution( false );
	auto& skills = unit->getSkills();
	auto& state = unit->current_state();

	if( state.get_name() != Unit::state_death )
	{
		for( auto& skill : skills )
		{
			execution = execution || skill->execution();
		}
		for( auto& skill : skills )
		{
			bool allow = (execution == false) || (skill->execution());
			allow = allow && (skill->getOnlyState().empty() ? true : skill->getOnlyState() == state.get_string_name());
			if( allow )
				skill->update( dt, unit );
		}
	}
	return execution;
}

void GameBoard::moveHero( Hero* hero, const Point& position )
{
	hero->moveTo( position );
}

void GameBoard::moveDesant( UnitDesant* desant, const Point& position )
{
    desant->moveTo( position );
}


bool GameBoard::isDeathUnit( Unit* unit )
{
	return unit->getCurrentHealth() <= 0;
}

void GameBoard::dispatchDeathUnits()
{
	std::vector<Unit::Pointer> deathNow;
	std::vector<Unit::Pointer> removed;

	auto checkDeath = [&]( std::vector<Unit::Pointer>& units )
	{
		for( auto& i : units )
		{
			auto alreadyDid = std::find( _death.begin(), _death.end(), i ) != _death.end();
			if( !alreadyDid && isDeathUnit(i) )
				deathNow.push_back( i );
			else if( i->getMoveFinished() == true && i->getType() == UnitType::creep )
				removed.push_back( i );
		}
	};
	checkDeath( _units[0] );
	checkDeath( _units[1] );
	checkDeath( _death );

	for( auto& i : deathNow )
	{
		auto it = std::find( _units[0].begin(), _units[0].end(), i );
		if( it != _units[0].end() )
		{
			_units[0].erase( it );
			preDeath( i );
			continue;
		}
		
		it = std::find( _units[1].begin(), _units[1].end(), i );
		if( it != _units[1].end() )
		{
			_units[1].erase( it );
			preDeath( i );
		}
	}

	for( auto iter = _death.begin(); iter != _death.end(); )
	{
		Unit::Pointer unit = *iter;
		if( (*iter)->getCurrentHealth() > 0 )
		{
			_gameLayer->onDeathCanceled( unit );
			int index = unit->getType() == UnitType::creep ? 0 : 1;
			if( _unitsLocked == 0 )
			{
				_units[index].push_back( unit );
			}
			else
			{
				_unitsAdd[index].push_back( unit );
			}
			iter = _death.erase( iter );
		}
		else
		{
			++iter;
		}
	}

	for( auto& i : removed )
	{
		auto it = std::find( _units[0].begin(), _units[0].end(), i );
		if( it != _units[0].end() )
		{
			assert( _unitsLocked == 0 );
			_units[0].erase( it );
			remove( i );
			continue;
		}

		it = std::find( _units[1].begin(), _units[1].end(), i );
		if( it != _units[1].end() )
		{
			assert( _unitsLocked == 0 );
			_units[0].erase( it );
			remove( i );
			continue;
		}
	}
}

void GameBoard::refreshTargetsForTowers(float dt)
{
	_refreshTargetsCounter += dt;
	if( _refreshTargetsCounter < _refreshTargetsPeriod )
		return;
	_refreshTargetsCounter -= _refreshTargetsPeriod;
	lockUnits();

	auto refresh = [&]( int baseIndex, int targetIndex)
	{
		for( auto base : _units.at( baseIndex ) )
		{
			std::vector<Unit::Pointer> targets;
			base->get_targets( targets );
			for( unsigned i = 0; i < targets.size(); )
			{
				assert( targets[i] );
				if( checkAvailableTarget( targets[i], base ) )
					++i;
				else
					targets.erase( targets.begin() + i );
			}
			unsigned max = base->getMaxTargets();
			
			for( auto target : _units.at( targetIndex ) )
			{
				if( targets.size() >= max )
					break;

				bool cont( false );
				for( size_t i = 0; i < targets.size(); ++i )
				{
					if( target == targets[i] )
					{
						cont = true;
						break;
					}
				}
				if( cont )continue;

				bool availabled = checkAvailableTarget( target, base );
				if( availabled )
					targets.push_back( target );
			}
			base->capture_targets( targets );
		}
	};
	refresh( 0, 1 );
	refresh( 1, 0 );

	unlockUnits();
}

bool GameBoard::checkAvailableTarget( const Unit* target, const Unit* base )const
{
	bool result = target != base;
	result = result && base != nullptr;
	result = result && target != nullptr;
	result = result && target->getCurrentHealth() > 0;
	result = result && target->current_state().get_name() != MachineUnit::State::state_enter;
	result = result && checkTargetByUnitLayer( target, base );
	result = result && checkTargetByUnitType( target, base );
	result = result && checkTargetByArea( target );
	result = result && checkTargetByRadius( target, base->getPosition(), base->getRadius() );
	result = result && base->checkTargetByRadius( target );
    result = result && checkTargetByDeadZone(target, base);
    result = result && checkByLineOfSight(target, base);

	return result;
}



bool GameBoard::checkByLineOfSight( const Unit* target, const Unit* base )const
{
    bool result = true;
    if (base->_handRadiusSector2 >= 0)
    {
        
        Vec2 radius = base->getPosition() - target->getPosition();
        if(radius.x < 0)
        {
            radius = target->getPosition() - base->getPosition();
        }
        float angle = getDirectionByVector( radius );
        
        if(abs(angle) > base->_handRadiusSector2)
        {
            result = false;
        }
//        if( angle > 0)
//        {
//            result = result || (angle <= 0 + base->_handRadiusSector2 || angle >= 360 - base->_handRadiusSector2);
//        }
//        else
//        {
//            while( angle < 0 ) angle += 360;
//            result = result || (angle <= 180 + base->_handRadiusSector2 || angle >= 180 - base->_handRadiusSector2);;
//        }
        
        
        

    }
    return result;
}

bool GameBoard::checkTargetByArea( const Unit* target )const
{
	float border = 30;

	float lx = border;
	float rx = _areaSize.width - border;
	float ty = _areaSize.height - border;
	float by = border;
	const Point& pos = target->getPosition();
	bool result( true );
	result = (pos.x > lx) && (pos.y > by) && (pos.x < rx) && (pos.y < ty);
	return result;
}

bool GameBoard::checkTargetByUnitType( const Unit* target, const Unit* base )const
{
	UnitType tt = target->getType();
	UnitType bt = base->getType();
	UnitType ett = base->getExtraTargetType();
	if( tt == ett )
		return true;
	switch( bt )
	{
		case UnitType::creep: return tt == UnitType::desant || tt == UnitType::hero;
		case UnitType::other: return tt == UnitType::other;
		default:return tt == UnitType::creep;
	}
	return false;
}

bool GameBoard::checkTargetByUnitLayer( const Unit* target, const  Unit* base )const
{
	auto target_layer = target->getUnitLayer();
	auto allow_targets = base->getAllowTargets();
	bool result = (target_layer & allow_targets) != 0;
	return result;
}

bool GameBoard::checkTargetByRadius( const Unit* target, const Point & center, float radius )const
{
	assert( target );
	Point a = center;
	Point b = target->getPosition();
	bool byradius = checkRadiusByEllipse( a, b, radius );
	return byradius;
}

bool GameBoard::checkTargetByDeadZone( const Unit* target, const Unit* base) const
{
    Vec2 deadZonePixels = base->getDeadZone();
    if (deadZonePixels != Point::ZERO)
    {
        Rect deadZoneRect = Rect(base->getPositionX() - deadZonePixels.x, base->getPositionY() - deadZonePixels.y, deadZonePixels.x*2, deadZonePixels.y*2);
        if (deadZoneRect.containsPoint(target->getPosition()))
        {
            return false;
        }
    }
    return true;
}

bool GameBoard::checkTargetBySector( const Unit* target, const Unit* base )const
{
	bool result( true );
	if( base->getDamageBySector() )
	{
		float direction = (float)base->getDirection();
		float radius = getDirectionByVector( target->getPosition() - base->getPosition() );

		float sector = base->getDamageBySectorAngle();
		while( direction < 0 ) direction += 360;
		while( radius < 0 ) radius += 360;

		result = fabsf( direction - radius ) <= sector;
	}
	return result;
}

void GameBoard::dispatchDamagers()
{
	//if( _hero )
	//{
	//	auto iter = _damagers.find( _hero );
	//	if( iter != _damagers.end() )
	//	{
	//		float exp = HeroExp::shared().getEXP( _hero->getName() );
	//		exp += iter->second;
	//		HeroExp::shared().setEXP( _hero->getName(), exp );
	//	}
	//}
}

void GameBoard::dispatchKillers()
{
	//for( auto hero : _heroes )
	//{
	//	auto iter = _killers.find( hero );
	//	if( iter != _killers.end() )
	//	{
	//		float exp = HeroExp::shared().getEXP( hero->getName() );
	//		for( auto& target : iter->second )
	//		{
	//			exp += target->getExp();
	//		}
	//		HeroExp::shared().setEXP( hero->getName(), exp );
	//	}
	//}
}

void GameBoard::getTargetsByRadius( Unit* requester, std::vector<Unit*> & out, const Point & center, float radius )
{
    for( auto & typedUnits : _units )
        for( auto & target : typedUnits.second )
            if( checkTargetByRadius( target, center, radius ) )
                out.push_back( target.ptr() );
}


void GameBoard::applyDamageBySector( Unit* base )
{
	int indexBase = base->getType() == UnitType::creep ? 0 : 1;
	int indexTargets = 1 - indexBase;
	assert( base );
	for( auto target : _units.at(indexTargets) )
	{
		bool result = true;
		result = result && checkTargetByUnitType( target, base );
		result = result && checkTargetByUnitLayer( target, base );
		result = result && checkTargetByRadius( target, base->getPosition(), base->getRadius() );
		result = result && checkTargetBySector( target, base );
		if( result )
		{
			target->applyDamage( base );
			_gameLayer->createEffect( base, target, base->getEffectOnShoot() );
		}
	}
}

const Route GameBoard::getRandomRoute( UnitLayer::type layer, unsigned& index, RouteSubType& type )const
{
	std::vector<TripleRoute> routes;
	if( index < (int)_creepsRoutes.size() && _creepsRoutes[index].type == layer && _creepsRoutes[index].onlyForHero == false )
	{
		routes.push_back( _creepsRoutes[index] );
	}
	else
	{
		for( unsigned i = 0; i < _creepsRoutes.size(); ++i )
		{
			if( layer == _creepsRoutes[i].type )
			{
				routes.push_back( _creepsRoutes[i] );
			}
		}
	}
	if( routes.empty() )
	{
#if USE_CHEATS == 1
		MessageBox( ("cannot find route [" + unitLayerToStr(layer) + "]").c_str(), "error map" );
		return _creepsRoutes.front().main;
#endif
	}

	auto ntype = static_cast<RouteSubType>((rand() % static_cast<int>(RouteSubType::max)));
	int nindex = index % routes.size();
	assert( nindex < (int)routes.size() );
	switch( type )
	{
		case RouteSubType::random: 
			//index = nindex;
			type = ntype;
			return getRandomRoute( layer, index, type );
		case RouteSubType::main:  
			return routes[nindex].main;
		case RouteSubType::left0:  
			return routes[nindex].left;
		case RouteSubType::right0: 
			return routes[nindex].right;
		case RouteSubType::max: assert( 0 );
	};
	assert( 0 );
	return routes[index].main;
}

const TripleRoute GameBoard::getRoute( UnitLayer::type layer, const Point & position, float & distance, bool onlyCreepsRoutes )const
{
	const TripleRoute* result( nullptr );
	int i( 0 );
	float minDistance;

	for( auto& route : _creepsRoutes )
	{
		if( onlyCreepsRoutes && route.onlyForHero )
			continue;
		bool check = checkPointOnRoute( position, route, distance, &minDistance );
		if( check )
		{
			distance = minDistance;
			result = &route;
		}		

		++i;
	}
	if( result )
		return *result;
	return TripleRoute();
}

void GameBoard::event_towerBuild( TowerPlace::Pointer place, Unit::Pointer unit )
{
	ParamCollection p;
	p["event"] = "TurretAtPlace";
	p["tower"] = unit->getName();
	p["level"] = toStr( unit->getLevel() );
	p["mode"] = _gameMode == GameMode::hard?"hard" : "normal";
	flurry::logEvent( p );
}

void GameBoard::event_towerUpgrade( Unit::Pointer unit )
{
	ParamCollection p;
	p["event"] = "TurretUpgrade";
	p["tower"] = unit->getName();
	p["level"] = toStr( unit->getLevel() );
	p["mode"] = _gameMode == GameMode::hard?"hard" : "normal";
	flurry::logEvent( p );
}
void GameBoard::event_towerSell( Unit::Pointer unit )
{
	ParamCollection p;
	p["event"] = "TurretSell";
	p["tower"] = unit->getName();
	p["level"] = toStr( unit->getLevel() );
	p["mode"] = _gameMode == GameMode::hard?"hard" : "normal";
	flurry::logEvent( p );
}
void GameBoard::event_levelFinished()
{
	auto userKey = "GameBoard_event_levelFinished_flurry" + toStr( _levelIndex ) + "_mode_" + toStr( _gameMode == GameMode::normal ? 1 : 2 );
	auto sended = UserData::shared().get<bool>( userKey );
	time_t currtime;
	time( &currtime );
	int leveltime = currtime - _levelStartTime;
	ParamCollection p;
	if( ScoreCounter::shared().getMoney( kScoreHealth ) > 1 )
		p["event"] = "LevelComplete";
	else
		p["event"] = "LevelFailed";

	p["level"] = toStr( _levelIndex );
	p["mode"] = _gameMode == GameMode::hard?"hard" : "normal";
	p["stars"] = toStr( _statisticsParams.stars );
	p["health"] = toStr( ScoreCounter::shared().getMoney( kScoreHealth ) );
	p["gear"] = toStr( ScoreCounter::shared().getMoney( kScoreLevel ) );
	p["gametime"] = toStr( ScoreByTime::shared().getTotalGameTime() );
	p["leveltime"] = toStr( leveltime );
	flurry::logEvent( p );

	bool first = !sended;
	if( first )
	{
		UserData::shared().write( userKey, true );
		p["event"] = "LevelFirstFinish";
		p["win"] = toStr(ScoreCounter::shared().getMoney( kScoreHealth ) > 1);
		flurry::logEvent( p );
	}

//	bool logevent = true;
//	logevent = logevent && UserData::shared().level_getCountPassed() == 5;
//	logevent = logevent && UserData::shared().get_int( "appsflyer_level" ) == 0;
//	if( logevent )
//	{
//		p.clear();
//		p["event"] = "Level";
//		p["value"] = "5";
//		appsflyer::logEvent( p );
//	}
}
void GameBoard::event_startwave( int index )
{
	ParamCollection p;
	p["event"] = "WaveStart";
	p["level"] = toStr( _levelIndex );
	p["mode"] = _gameMode == GameMode::hard?"hard" : "normal";
	p["waveindex"] = toStr( index );
	flurry::logEvent( p );
}

NS_CC_END;
