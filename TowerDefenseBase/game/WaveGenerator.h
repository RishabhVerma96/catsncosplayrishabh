//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __WAVE_GENERATOR__
#define __WAVE_GENERATOR__
#include "cocos2d.h"
#include "ml/pugixml/pugixml.hpp"
#include "ml/ObServer.h"
#include "support.h"

NS_CC_BEGIN;

struct WaveInfo
{
	WaveInfo()
	: creeps()
	, delayOneUnit()
	, scores()
	, routeSubType()
	, routeIndex()
	, healthRate()
	, type( UnitLayer::any )
	, index( -1 )
	, delayOneWave(0)
    , soundOnWave("")
	{}
	bool valid() const
	{
		return delayOneUnit.empty() == false;
	}
	std::list<std::string> creeps;
	std::list<float> delayOneUnit;
	std::list<unsigned> scores;
	std::list<RouteSubType> routeSubType;
	std::list<unsigned> routeIndex;
	std::list<float> healthRate;
	UnitLayer::type type;
	unsigned index;
	float delayOneWave;
    std::string soundOnWave;
public:
	void pop()
	{
		creeps.pop_front();
		delayOneUnit.pop_front();
		scores.pop_front();
		healthRate.pop_front();
		routeSubType.pop_front();
		routeIndex.pop_front();
	}
};

class GameBoard;

class WaveGenerator
{
	typedef ObServer<WaveGenerator, std::function<void( int, int )> > ObserverWave;
public:
	WaveGenerator( GameBoard* board );
	ObserverWave& getObserverWaveCounter() { return _observerWaveCounter; }
    ObserverWave& getObserverEnemiesCounter() { return _observerEnemiesCounter; }
    
	void load( const pugi::xml_node & node, bool infinityMode = false );
	void clear();
	void start();
	void pause();
	void resume();
	void update( float dt );
	void generateCreep();
	void onPredelayWave( const WaveInfo & wave );
	void onStartWave( const WaveInfo & wave );
	void onFinishWave();
	void onFinishAllWaves();
	bool isInfinityMode() const;
protected:
	GameBoard& _board;
	ObserverWave _observerWaveCounter;
    ObserverWave _observerEnemiesCounter;
	CC_SYNTHESIZE_READONLY( size_t, _waveIndex, WaveIndex );
	CC_SYNTHESIZE_READONLY( size_t, _wavesCount, WavesCount );
	std::list< WaveInfo > _waves;
	std::list< WaveInfo >::iterator _currentWave;

	bool _infinityMode;
	TimeCounter _delayUnits;
	TimeCounter _delayWaves;
	bool _delayWavesNow;
	bool _isRunning;
	CC_SYNTHESIZE(float, _unitsRate, UnitsRate);
	CC_SYNTHESIZE(float, _unitsRateInc, UnitsRateInc);
	CC_SYNTHESIZE(float, _infinityModeWaveStretch, InfinityModeWaveStretch);
    
private:
    int totalNumberOfEnemies;
    int totalRemainingNumberOfEnemies;
    
};

NS_CC_END;

#endif
