#ifndef __TestScene_h__
#define __TestScene_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "ml/SmartScene.h"
#include "Tutorial.h"
NS_CC_BEGIN





class TestScene : public SmartScene, public NodeExt
{
	DECLARE_BUILDER( TestScene );
	bool init();
public:
	void tutorialInit();
	void tutorialShow( int index );
protected:
private:
	std::vector< std::string > _tutorialPaths;
	Tutorial::Pointer _tutorial;
};




NS_CC_END
#endif // #ifndef TestScene