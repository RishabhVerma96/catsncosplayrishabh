//
//  EditorDeclarations.h
//  JungleDefense
//
//  Created by Vladimir Tolmachev on 25.05.14.
//
//

#if EDITOR==1
#ifndef JungleDefense_EditorDeclarations_h
#define JungleDefense_EditorDeclarations_h
#include "configuration.h"
#include "support.h"

static auto kEditorLevelsMenuPosX = [](){ return cocos2d::Config::shared().get<cocos2d::Size>( "levelMapSize" ).width + 35; };
const int kEditorLevelsMenuPosY(750);

static auto kEditorLayersMenuPosX = [](){ return cocos2d::Config::shared().get<cocos2d::Size>( "levelMapSize" ).width + 150; };
const int kEditorLayersMenuPosY(700);
const int kEditorLayersMenuPosY_add(550);


#endif
#endif