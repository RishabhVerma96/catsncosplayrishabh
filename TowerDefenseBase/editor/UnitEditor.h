/******************************************************************************/
/*
 * Copyright 2014-2015 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: Defense of Greece
 * e-mail: tolm_vl@hotmail.com    skype: volodar13
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#if EDITOR==1
#ifndef __UnitEditor_h__
#define __UnitEditor_h__

#include "cocos2d.h"
#include "platform/DropFileProtocol.h"
#include "game/unit/Unit.h"

NS_CC_BEGIN;

class UnitEditor : public cocos2d::Layer, public IMEDelegate, public DropFileProtocol
{
public:
	static UnitEditor * create();
	virtual bool init();  

	virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
	virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event);
	virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event);
	virtual void onDropBegin( const cocos2d::Point & locationInView );
	virtual void onDropEnded( const cocos2d::Point & locationInView, const std::list<std::string> & files );
	virtual void onDropMoved( const cocos2d::Point & locationInView );
	virtual void onDropCanceled( );

	void setEnabled( bool var );
	void selectLevel( int level, const pugi::xml_node& root );
public:
    virtual void insertText(const char * text, int len);
    virtual void deleteBackward();
	virtual bool canDetachWithIME() { return true; }
    virtual bool canAttachWithIME() { return true; }
	void loadLevel( const pugi::xml_node& root );
	void saveLevel( pugi::xml_node& root );
protected:
	void remove();
protected:
	CC_SYNTHESIZE_READONLY(bool, m_enabled, IsEnabled);
	int m_levelIndex;
	struct unit
	{
		unit( Unit::Pointer node, const std::string & _name )
		: name( _name )
		, body( node ) 
		, addZ( 0 )
		, routeindex( -1 )
		, velocity( 0.f )
		, delay( 0.f )
		{}

		std::string name;
		std::string name2;
		std::string pairunitname;
		Unit::Pointer body;
		int addZ;
		int routeindex;
		float velocity;
		float delay;
	};
	std::vector<unit>m_units;
	std::vector<unit>::iterator m_touchedUnit;
};

NS_CC_END;

#endif
#endif