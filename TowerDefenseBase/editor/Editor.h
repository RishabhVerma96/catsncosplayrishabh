//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#if EDITOR==1
#ifndef __EDITOR_SCENE__
#define __EDITOR_SCENE__
#include "cocos2d.h"
#include "ml/SmartScene.h"
#include "loadxml/xmlLoader.h"
#include "gameboard.h"
#include "editor/UnitEditor.h"
NS_CC_BEGIN;

class DecorationEditor;
class PathEditor;
class TowerPlacesEditor;

class EditorScene : public SmartScene
{
public:
	~EditorScene();
	static EditorScene& shared();
	static EditorScene* create();
	//virtual void visit();

	void changeBackground( int level, const pugi::xml_node& root, float&scale );
	void selectLevel(int level, GameMode mode);

private:


	void hideGUI(Ref * sender);
	void disableTouches();
	void enableTouches0( );
	void enableTouches1( );
	void enableTouches2();
	void enableTouches3();
	void enableTouches4();
	void save();
	EditorScene();
private:
	static EditorScene * s_instance;
	CC_SYNTHESIZE_READONLY( Sprite*, m_bg, Bg );
	Menu * m_menu;
	Menu * m_levelsMenu;
	DecorationEditor * m_decorations;
	PathEditor * m_paths;
	UnitEditor * m_units;
	TowerPlacesEditor * m_places;
	int m_currentLevelIndex;
	GameMode _mode;
};

NS_CC_END;
#endif
#endif