#if EDITOR==1
//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "TowerPlacesEditor.h"
#include "MenuItemTextBG.h"
#include "support.h"
#include "ml/pugixml/pugixml.hpp"
#include "gameboard.h"
#include "GameLayer.h"
#include "consts.h"
#include "EditorDeclarations.h"
#include "Editor.h"
#include "ImageManager.h"
NS_CC_BEGIN;

float radiusPlace( 50 );

TowerPlacesEditor * TowerPlacesEditor::create( EditorScene* editor )
{
	TowerPlacesEditor * ptr = new TowerPlacesEditor;
	if( ptr && ptr->init(editor) )
	{
		ptr->autorelease();
		return ptr;
	}
	CC_SAFE_DELETE( ptr );
	return nullptr;
}


bool TowerPlacesEditor::init( EditorScene* editor )
{
	if( !Layer::init() )
	{
		return false;
	}
	m_area = nullptr;
	m_editor = editor;
	setContentSize( Size::ZERO );
	m_touchedPlace = m_places.end();

	//Size visibleSize = Director::getInstance()->getVisibleSize();

	m_menuMaps = Menu::create();
	addChild( m_menuMaps );
	m_menuMaps->setPosition( Point::ZERO );

	Point position( kEditorLayersMenuPosX(), kEditorLayersMenuPosY_add );

	auto item = MenuItemTextBG::create( "Add place", Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), CC_CALLBACK_0( TowerPlacesEditor::createPlace, this, true ) );
	item->setPosition( position ); position.y -= 30;
	m_menuMaps->addChild( item );

	item = MenuItemTextBG::create( "Add non active place", Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), CC_CALLBACK_0( TowerPlacesEditor::createPlace, this, false ) );
	item->setPosition( position ); position.y -= 30;
	m_menuMaps->addChild( item );

	item = MenuItemTextBG::create( "Remove place", Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), CC_CALLBACK_0( TowerPlacesEditor::removePlace, this ) );
	item->setPosition( position ); position.y -= 30;
	m_menuMaps->addChild( item );

	m_levelIndex = 0;

	setEnabled( false );
	return true;
}

void TowerPlacesEditor::setEnabled( bool var )
{
	if( var )
		attachWithIME();
	else
		detachWithIME();
	m_menuMaps->setVisible( var );
	m_enabled = var;
	setTouchMode( Touch::DispatchMode::ONE_BY_ONE );
	setTouchEnabled( var );
	if( m_area )
		m_area->setVisible( var );
}

void TowerPlacesEditor::insertText( const char * text, int len )
{
	if( len && text[0] == 'e' )
		removePlace();
}

void TowerPlacesEditor::deleteBackward()
{
	removePlace();
}

bool TowerPlacesEditor::onTouchBegan( Touch *touch, Event *event )
{
	auto location = touch->getLocation() / getScale();
	m_touchedPlace = m_places.end();
	float minDist( 9999 );
	for( auto i = m_places.begin(); i != m_places.end(); ++i )
	{
		float distance = location.getDistance( (*i)->getPosition() );
		if( distance < minDist && distance < radiusPlace )
		{
			m_touchedPlace = i;
		}
	}
	return true;
}

void TowerPlacesEditor::onTouchMoved( Touch *touch, Event *event )
{
	auto location = touch->getLocation();
	auto delta = touch->getDelta() / getScale();
	if( m_touchedPlace != m_places.end() )
	{
		Point pos = (*m_touchedPlace)->getPosition() + delta;
		(*m_touchedPlace)->setPosition( pos );
	}
}

void TowerPlacesEditor::onTouchEnded( Touch *touch, Event *event )
{
	auto location = touch->getLocation();
	if( m_touchedPlace != m_places.end() )
	{
		Point pos = (*m_touchedPlace)->getPosition();
		pos.x = int( pos.x );
		pos.y = int( pos.y );
		(*m_touchedPlace)->setPosition( pos );
	}
}

void TowerPlacesEditor::selectLevel( int level, const pugi::xml_node& root )
{
	m_levelIndex = level;
	loadLevel( root );
	buildBorders();
}

void TowerPlacesEditor::buildBorders()
{
	if( !m_area )
	{
		m_area = Node::create();
		addChild( m_area, 999 );
	}
	m_area->removeAllChildren();

	Sprite* sprite(nullptr);
	float x = m_editor->getBg()->getContentSize().width;
	float y = m_editor->getBg()->getContentSize().height;
	sprite = Sprite::create( "images/square.png" ); sprite->setAnchorPoint( Point( 0, 0 ) ); sprite->setPosition( 0, 0 ); m_area->addChild( sprite ); sprite->setScale( 150, y ); sprite->setColor( Color3B::GRAY ); sprite->setOpacity( 128 );
	sprite = Sprite::create( "images/square.png" ); sprite->setAnchorPoint( Point( 0, 0 ) ); sprite->setPosition( 0, 0 ); m_area->addChild( sprite ); sprite->setScale( x, 130 ); sprite->setColor( Color3B::GRAY ); sprite->setOpacity( 128 );
	sprite = Sprite::create( "images/square.png" ); sprite->setAnchorPoint( Point( 1, 1 ) ); sprite->setPosition( x, y ); m_area->addChild( sprite ); sprite->setScale( 150, y ); sprite->setColor( Color3B::GRAY ); sprite->setOpacity( 128 );
	sprite = Sprite::create( "images/square.png" ); sprite->setAnchorPoint( Point( 1, 1 ) ); sprite->setPosition( x, y ); m_area->addChild( sprite ); sprite->setScale( x, 130 ); sprite->setColor( Color3B::GRAY ); sprite->setOpacity( 128 );
	m_area->setVisible( m_enabled );
}

void TowerPlacesEditor::newPlace( const Point & position, bool active )
{
	std::string texture = active ?
		Config::shared().get( "resourceGameSceneFolder" ) + "active_slot.png" :
		Config::shared().get( "resourceGameSceneFolder" ) + "unactive_slot.png";
	m_places.push_back( ImageManager::sprite( texture ) );
	m_placesActive.push_back( active );

	addChild( m_places.back() );
	m_places.back()->setPosition( position );
	m_touchedPlace = m_places.end();
	rebuildIndexes();
}

void TowerPlacesEditor::rebuildIndexes()
{
	int index( 0 );
	for( auto place : m_places )
	{
		auto label = place->getChildByName<LabelTTF*>( "index" );
		if( !label )
		{
			label = LabelTTF::create( toStr( index ), "Arial", 25 );
			label->setPosition( Point( place->getContentSize() / 2 ) );
			label->setName( "index" );
			label->setColor( Color3B::BLACK );
			place->addChild( label );
		}
		label->setString( toStr( index ) );

		++index;
	}
}

void TowerPlacesEditor::removePlace()
{
	if( m_touchedPlace != m_places.end() )
	{
		unsigned index = m_touchedPlace - m_places.begin();
		assert( index < m_placesActive.size() );
		m_placesActive.erase( m_placesActive.begin() + index );
		removeChild( *m_touchedPlace );
		m_touchedPlace = m_places.erase( m_touchedPlace );
	}
	rebuildIndexes();
}

void TowerPlacesEditor::loadLevel( const pugi::xml_node& root )
{
	for( auto i : m_places )
		removeChild( i );
	m_places.clear();
	m_placesActive.clear();

	std::vector<TowerPlaseDef> places;
	GameBoard::loadTowerPlaces( places, root.child( "towerplaces" ) );
	for( auto i : places )
	{
		newPlace( i.position, i.isActive );
	}
	m_touchedPlace = m_places.end();

	std::string nameBG = "images/maps/map" + toStr( m_levelIndex ) + ".jpg";
}

void TowerPlacesEditor::saveLevel( pugi::xml_node& mapNode )
{
	pugi::xml_node placesNode = mapNode.child( "towerplaces" );
	if( placesNode )
	{
		mapNode.remove_child( placesNode );
	}
	placesNode = mapNode.append_child( "towerplaces" );

	int index( 0 );
	for( auto i : m_places )
	{
		pugi::xml_node node = placesNode.append_child( "place" );
		node.append_attribute( "x" ).set_value( toStr( i->getPosition().x ).c_str() );
		node.append_attribute( "y" ).set_value( toStr( i->getPosition().y ).c_str() );
		node.append_attribute( "active" ).set_value( m_placesActive[index] ? "yes" : "no" );
		++index;
	}
}

NS_CC_END;
#endif