#if EDITOR==1
#include "PreviewTowers.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#endif
#include <stdio.h>
#include "TowerPlacesEditor.h"
#include "ml/loadxml/xmlLoader.h"
#include "configuration.h"
#include "MenuItemTextBG.h"

NS_CC_BEGIN
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
#	pragma comment(lib, "User32.lib")
#endif

PreviewTowers::PreviewTowers()
{
}

PreviewTowers::~PreviewTowers( )
{
	removeAllChildren();
	Director::getInstance()->getTextureCache()->removeUnusedTextures();
}

bool PreviewTowers::init()
{
	auto createTower = [this]( const std::string& name, float y )
	{
		std::string actionstring = ""
							 "RepeatForever[Sequence["
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:1/100.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:1/200.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:1/300.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:1/400.png,01:30]],"
							 ""
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:2/100.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:2/200.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:2/300.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:2/400.png,01:30]],"
							 ""
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:3/100.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:3/200.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:3/300.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:3/400.png,01:30]],"
							 ""
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:4/100.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:4/200.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:4/300.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:4/400.png,01:30]],"
							 ""
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:5/100.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:5/200.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:5/300.png,01:30]],"
							 "Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:5/400.png,01:30]]"
							 "]]";
		auto action = xmlLoader::load_action( actionstring );
		auto sprite = Sprite::create();
		sprite->runAction( action );
		addChild( sprite );
		sprite->setPosition( -200, y );

		actionstring = "\
							 RepeatForever[Sequence[\
							 Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:1/100.png,01:30]],\
							 Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:2/100.png,01:30]],\
							 Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:3/100.png,01:30]],\
							 Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:4/100.png,01:30]],\
							 Animate[0.5,[folder:../sources/towers/" + name + "/,indexes:5/100.png,01:30]]\
							 ]]";
		action = xmlLoader::load_action( actionstring );
		sprite = Sprite::create();
		sprite->runAction( action );
		sprite->setPosition( 200, y );
		addChild( sprite );
	};

	createTower( "tower_air", -250 );
	createTower( "tower_fire", -150 );
	createTower( "tower_gun", -50 );
	createTower( "tower_ice", 50 );
	createTower( "tower_shootgun", 150 );
	createTower( "tower_tesla", 250 );
	createTower( "tower_teslay", 270 );

	setPosition( Point( Config::shared().get<Size>( "levelMapSize" ) / 2 ) );


	MenuItemLabel * layer0 = MenuItemTextBG::create( "Close", Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), std::bind( [this](Ref*){removeFromParent(); }, std::placeholders::_1 ) );
	layer0->setPosition( -Point( Config::shared().get<Size>( "levelMapSize" ) / 2 ) );
	auto menu = Menu::create();
	menu->addChild( layer0 );
	addChild( menu );
	menu->setPosition( 100, 50 );


	return true;
}


NS_CC_END
#endif