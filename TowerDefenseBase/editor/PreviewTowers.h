#if EDITOR==1
#ifndef __PreviewTowers_h__
#define __PreviewTowers_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "gameboard.h"
NS_CC_BEGIN





class PreviewTowers : public LayerExt
{
	DECLARE_BUILDER( PreviewTowers );
	bool init();
public:
protected:
private:
	GameBoard _board;
};




NS_CC_END
#endif // #ifndef PreviewTowers
#endif