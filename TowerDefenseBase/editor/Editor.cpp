//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#if EDITOR==1
#include "Editor.h"
#include "EditorPath.h"
#include "TowerPlacesEditor.h"
#include "DecorationEditor.h"
#include "support.h"
#include "consts.h"
#include "MenuItemTextBG.h"
#include "EditorDeclarations.h"
#include "ml/ImageManager.h"
#include "PreviewTowers.h"
NS_CC_BEGIN;

EditorScene * EditorScene::s_instance(nullptr);

EditorScene::EditorScene()
{
	//ImageManager::shared().load_plist( "images/gamescene.plist", "gamescene" );
	pugi::xml_document doc;
	doc.load_file( "dev/editor_resources.xml" );
	auto root = doc.root().first_child();
	for( auto child : root )
	{
		auto name = child.attribute( "name" ).as_string();
		auto path = child.attribute( "path" ).as_string();
		ImageManager::shared().load_plist( path, name );
	}


	Layer* layer;
	init( layer = Layer::create() );

	s_instance = this;

	Point center( Director::getInstance()->getWinSize().width / 2, Director::getInstance()->getWinSize().height / 2 );

	m_bg = Sprite::create();
	m_decorations = DecorationEditor::create();
	m_paths = PathEditor::create();
	m_places = TowerPlacesEditor::create( this );
	m_units = UnitEditor::create();

	layer->addChild( m_bg, -1 );
	layer->addChild( m_decorations, 999 );
	layer->addChild( m_paths, 999 );
	layer->addChild( m_places, 999 );
	layer->addChild( m_units, 999 );

	selectLevel( 0, GameMode::normal );
	disableTouches( );

	const int levelsCount = 150;
	m_levelsMenu = Menu::create();
	layer->addChild( m_levelsMenu, 9999 );
	m_levelsMenu->setPosition( Point::ZERO );
	float X = kEditorLevelsMenuPosX();
	float Y = kEditorLevelsMenuPosY;
	for( int i = 0; i < levelsCount; ++i )
	{
		MenuItemLabel * item = MenuItemTextBG::create( StringUtils::format( "Lvl #%03u", i + 1 ), Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), CC_CALLBACK_0( EditorScene::selectLevel, this, i, GameMode::normal ) );
		item->setTag(i);
		item->setPosition(Point(X, Y));
		m_levelsMenu->addChild( item );
		
		Y -= item->getContentSize().height * ( i % 5 == 4 ? 1.75f : 1.2f );
	}

	//create multiplayer level
	X = kEditorLevelsMenuPosX() + 150;
	Y = kEditorLevelsMenuPosY;
	for( int i = 0; i < 1; ++i )
	{
		auto callback = CC_CALLBACK_0( EditorScene::selectLevel, this, i, GameMode::multiplayer );
		MenuItemLabel * item = MenuItemTextBG::create( StringUtils::format( "Online #%02u", i), Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), callback );
		item->setTag( i );
		item->setPosition( Point( X, Y ) );
		m_levelsMenu->addChild( item );
	}

	m_menu = Menu::create();
	layer->addChild( m_menu, 99999 );
	m_menu->setPosition( Point::ZERO );

	MenuItemLabel * upButton = MenuItemTextBG::create( " /\\   Up    ", Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), [this, levelsCount]( Ref * ){
		if( this->m_levelsMenu->getPosition().y > 0 )
			this->m_levelsMenu->runAction( EaseOut::create( MoveBy::create( 0.5f, Vec2( 0, -250.0f ) ), 3.0f ) );
	} );
	MenuItemLabel * downButton = MenuItemTextBG::create( " \\/ Down ", Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), [this, levelsCount]( Ref * ){
		if( this->m_levelsMenu->getPosition().y < levelsCount * 19.0f )
			this->m_levelsMenu->runAction( EaseOut::create( MoveBy::create( 0.5f, Vec2( 0, 250.0f ) ), 3.0f ) );
	} );
	upButton->setPosition( Point( kEditorLayersMenuPosX(), 50 ) );
	downButton->setPosition( Point( kEditorLayersMenuPosX(), 25 ) );
	m_menu->addChild( upButton );
	m_menu->addChild( downButton );


	MenuItemLabel * layer0 = MenuItemTextBG::create( "Paths", Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), CC_CALLBACK_0( EditorScene::enableTouches0, this ) );
	MenuItemLabel * layer1 = MenuItemTextBG::create( "Places", Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), CC_CALLBACK_0( EditorScene::enableTouches1, this ) );
	MenuItemLabel * layer2 = MenuItemTextBG::create( "Decor", Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), CC_CALLBACK_0( EditorScene::enableTouches2, this ) );
	MenuItemLabel * layer3 = MenuItemTextBG::create( "Units", Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), CC_CALLBACK_0( EditorScene::enableTouches4, this ) );
	MenuItemLabel * layer4 = MenuItemTextBG::create( "Save", Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), CC_CALLBACK_0( EditorScene::save, this ) );
	MenuItemLabel * layer5 = MenuItemTextBG::create( "Towers Viewer", Color4F( 0.7f, 0.7f, 0.7f, 1 ), Color3B( 0, 0, 0 ), CC_CALLBACK_0( EditorScene::enableTouches3, this ) );

	layer0->setPosition( Point( kEditorLayersMenuPosX(), kEditorLayersMenuPosY - 0 * 19 ) );
	layer1->setPosition( Point( kEditorLayersMenuPosX(), kEditorLayersMenuPosY - 1 * 19 ) );
	layer2->setPosition( Point( kEditorLayersMenuPosX(), kEditorLayersMenuPosY - 2 * 19 ) );
	layer3->setPosition( Point( kEditorLayersMenuPosX(), kEditorLayersMenuPosY - 3 * 19 ) );
	layer4->setPosition( Point( kEditorLayersMenuPosX(), kEditorLayersMenuPosY - 5 * 19 ) );
	layer5->setPosition( Point( kEditorLayersMenuPosX(), kEditorLayersMenuPosY - 6 * 19 ) );
	m_menu->addChild(layer0);
	m_menu->addChild(layer1);
	m_menu->addChild(layer2);
	m_menu->addChild(layer3);
	m_menu->addChild(layer4);
	m_menu->addChild(layer5);
}


EditorScene::~EditorScene()
{
	s_instance = nullptr;
}

EditorScene& EditorScene::shared()
{
	assert( s_instance );
	return *s_instance;
}

EditorScene* EditorScene::create()
{
	assert( s_instance == nullptr );
	EditorScene * scene = new EditorScene;
	if( scene )
		scene->autorelease();
	return scene;
}

void EditorScene::selectLevel( int tag, GameMode mode )
{
	m_currentLevelIndex = tag;
	_mode = mode;

	std::string mapFile;
	if( mode != GameMode::multiplayer )
		mapFile = "map" + toStr( tag ) + ".xml";
	else
		mapFile = "online_" + toStr( tag ) + ".xml";
	std::string pathToFile = FileUtils::getInstance()->fullPathForFilename( kDirectoryToMaps + mapFile );
	pugi::xml_document doc;
	doc.load_file( pathToFile.c_str() );
	auto root = doc.root().first_child();

	float scale( 1 );
	changeBackground( tag, root, scale );
	m_decorations->selectLevel( tag, root );
	m_paths->selectLevel( tag, root );
	m_places->selectLevel( tag, root );
	m_units->selectLevel( tag, root );

	m_bg->setScale( scale );
	m_decorations->setScale( scale );
	m_paths->setScale( scale );
	m_places->setScale( scale );
	m_units->setScale( scale );
}

void EditorScene::changeBackground( int level, const pugi::xml_node& root, float& scale )
{
	std::string nameBG = "images/maps/map" + toStr(level + 1) + ".jpg";
	if( root.attribute( "bg" ) )nameBG = root.attribute( "bg" ).as_string();
	m_bg->setTexture( nameBG );
	m_bg->setPosition( Point( 0,0 ) );
	m_bg->setAnchorPoint( Point( 0, 0 ) );

	auto size = Config::shared().get<cocos2d::Size>( "levelMapSize" );
	auto bgSize = m_bg->getContentSize();
	float sx = std::min( size.width / bgSize.width, 1.f );
	float sy = std::min( size.height / bgSize.height, 1.f );
	scale = std::min( sx, sy );
}

void EditorScene::hideGUI(Ref * sender)
{
	static bool visible(true);
	visible = !visible;

	static bool layer0 = m_decorations->getIsEnabled();
	static bool layer1 = m_paths->getIsEnabled();
	static bool layer2 = m_places->getIsEnabled();
	static bool layer3 = m_units->getIsEnabled();
	if( visible == false )
	{
		layer0 = m_decorations->getIsEnabled();
		layer1 = m_paths->getIsEnabled();
		layer2 = m_places->getIsEnabled();
		layer3 = m_units->getIsEnabled();
	}
	Director::getInstance()->setDisplayStats( visible );

	if( layer0 ) m_decorations->setEnabled( visible );
	if( layer1 ) m_paths->setEnabled( visible );
	if( layer2 ) m_places->setEnabled( visible );
	if( layer3 ) m_units->setEnabled( visible );

	m_menu->setVisible( visible );
	MenuItemTextBG * item = dynamic_cast<MenuItemTextBG*>( sender );
	item->getLabel()->setOpacity( visible? 255 : 1 );
	item->getBG()->setOpacity( visible? 255 : 1 );
	 
}

void EditorScene::disableTouches()
{
	m_decorations->setEnabled( false );
	m_paths->setEnabled( false );
	m_places->setEnabled( false );
	m_units->setEnabled( false );
}

void EditorScene::enableTouches0( )
{
	disableTouches();
	m_paths->setEnabled( true );
}

void EditorScene::enableTouches1( )
{
	disableTouches();
	m_places->setEnabled( true );
}

void EditorScene::enableTouches2( )
{
	disableTouches();
	m_decorations->setEnabled( true );
}

void EditorScene::enableTouches3()
{
	auto layer = PreviewTowers::create();
	addChild( layer, 99999 );

}

void EditorScene::enableTouches4()
{
	disableTouches();
	m_units->setEnabled( true );
}

void EditorScene::save( )
{
	std::string mapFile;
	if( _mode != GameMode::multiplayer )
		mapFile = "map" + toStr( m_currentLevelIndex ) + ".xml";
	else
		mapFile = "online_" + toStr( m_currentLevelIndex ) + ".xml";
	std::string pathToFile = FileUtils::getInstance()->fullPathForFilename( kDirectoryToMaps + mapFile );
	pugi::xml_document doc;
	doc.load_file( pathToFile.c_str() );
	auto root = doc.root().first_child();

	m_paths->saveLevel( root );
	m_decorations->saveLevel( root );
	m_places->saveLevel(root);
	m_units->saveLevel( root );
	doc.save_file( pathToFile.c_str() );
}


NS_CC_END;
#endif