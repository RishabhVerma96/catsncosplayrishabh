#include "TestScene.h"
#include "ml/loadxml/xmlLoader.h"
#include "ml/loadxml/xmlProperties.h"
#include "MenuItemTextBG.h"
NS_CC_BEGIN





TestScene::TestScene()
{
}

TestScene::~TestScene( )
{
}

bool TestScene::init( )
{
	do
	{
		auto layer = LayerExt::create();
		layer->setName( "main" );

		CC_BREAK_IF( !NodeExt::init() );
		CC_BREAK_IF( !SmartScene::init( layer ) );

		load( "dev/testscene.xml" );
		tutorialInit();

		EventListenerKeyboard * event = EventListenerKeyboard::create();

		auto lambda = []( EventKeyboard::KeyCode key, Event* )mutable
		{
			if( key == EventKeyboard::KeyCode::KEY_BACK )
				Director::getInstance()->popScene();
		};
		event->onKeyReleased = std::bind( lambda, std::placeholders::_1, std::placeholders::_2 );

		layer->getEventDispatcher()->addEventListenerWithSceneGraphPriority( event, layer );


		return true;
	} while( false );
	return false;
}

void TestScene::tutorialInit()
{
	auto size = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	TutorialManager::shared();
	pugi::xml_document doc;
	doc.load_file( "ini/tutorial/tutorials.xml" );
	auto root = doc.root().first_child();
	auto list = root.child( "list" );
	auto menu = getNodeByPath<Menu>( this, "menu_tutorials" );
	if( !menu )
		return;
	int index = 0;
	Point pos( 0, 20 );
	for( auto child : list )
	{
		auto path = child.attribute( "filename" ).as_string();
		auto name = child.name();
		_tutorialPaths.push_back( path );

		auto callback = [this]( Ref*, int index )
		{
			this->tutorialShow( index );
		};
		auto item = MenuItemTextBG::create( name, Color4F::GRAY, Color3B::BLACK, std::bind( callback, std::placeholders::_1, index++ ) );
		menu->addChild( item );
		item->setPosition( pos );
		pos.y += 20;
		if( pos.y > size.height - 20 )
		{
			pos.y = 20;
			pos.x += 300;
		}
	}

}

void TestScene::tutorialShow( int index )
{
	if( _tutorial )_tutorial->removeFromParent();
	_tutorial = Tutorial::create( _tutorialPaths[index], true );
	setKeyDispatcherBackButton( _tutorial );
	if( _tutorial->getParent() ) _tutorial->removeFromParent();
	pushLayer( _tutorial, true );
	_tutorial->enter();
}



NS_CC_END
