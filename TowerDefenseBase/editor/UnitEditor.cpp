/******************************************************************************/
/*
 * Copyright 2014-2015 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: Defense of Greece
 * e-mail: tolm_vl@hotmail.com    skype: volodar13
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#if EDITOR==1
#include "UnitEditor.h"
#include "support/MenuItemTextBG.h"
#include "support/support.h"
#include "ml/pugixml/pugixml.hpp"
#include "game/gameboard.h"
#include "gamegs/GameScene.h"
#include "consts.h"
#include "editor/EditorDeclarations.h"
#include "ml/ImageManager.h"
#include "resources.h"
#include "support/MenuItemTextBG.h"
#include "ml/loadxml/xmlLoader.h"

NS_CC_BEGIN;

UnitEditor * UnitEditor::create()
{
	UnitEditor * ptr = new UnitEditor;
	if( ptr && ptr->init() )
	{
		ptr->autorelease();
		return ptr;
	}
	CC_SAFE_DELETE( ptr );
	return nullptr;
}


bool UnitEditor::init()
{
	ImageManager::shared().load_plist( "images/maps/animations/animations.plist", "maps.animations" );
	if( !Layer::init() )
	{
		return false;
	}
	DropFileProtocol::setInstance( this );
	m_touchedUnit = m_units.end();
	setContentSize( Size::ZERO );
	m_levelIndex = 0;
	setEnabled( false );
	return true;
}

void UnitEditor::setEnabled( bool var )
{
	if( var )
		attachWithIME();
	else
		detachWithIME();

	m_enabled = var;
	setTouchMode( Touch::DispatchMode::ONE_BY_ONE );
	setTouchEnabled( var );

	if( var )
		DropFileProtocol::setInstance( this );
}

bool UnitEditor::onTouchBegan( Touch *touch, Event *event )
{
	auto location = touch->getLocation();

	m_touchedUnit = m_units.end();
	for( int i = m_units.size() - 1; i >= 0; --i )
	{
		auto iter = m_units.begin() + i;
		auto point = iter->body->convertToNodeSpace( location );
		bool innode = checkPointInNode( iter->body->getChildByName( "skin" ), point );
		if( innode )
		{
			m_touchedUnit = iter;
			break;
		}
	}
	return true;
}

void UnitEditor::onTouchMoved( Touch *touch, Event *event )
{
	auto location = touch->getLocation() / getScale();
	auto delta = touch->getDelta() / getScale();
	if( m_touchedUnit != m_units.end() )
	{
		Point pos = m_touchedUnit->body->getPosition() + delta;
		m_touchedUnit->body->setPosition( pos );
		m_touchedUnit->body->setLocalZOrder( -pos.y + m_touchedUnit->addZ );
	}
}

void UnitEditor::onTouchEnded( Touch *touch, Event *event )
{
	m_touchedUnit = m_units.end();
}

void UnitEditor::onDropBegin( const cocos2d::Point & locationInView )
{

}

void UnitEditor::onDropEnded( const cocos2d::Point & locationInView, const std::list<std::string> & files )
{
	if( files.empty() ) return;
	std::string path = files.front();
	int k = path.find( "ini\\units\\" );
	if( k == std::string::npos )return;
	path = path.substr( k + std::string( "ini\\units\\" ).size() );

	std::string name = path;
	k = name.find_last_of( "." );
	name = name.substr( 0, k );
	std::replace( name.begin(), name.end(), '\\', '/' );

	auto unit = xmlLoader::load_node<Unit>( "ini/units/" + name + ".xml" );
	if( unit )
	{
		unit->setPosition( locationInView );
		addChild( unit, -locationInView.y );
		m_units.push_back( UnitEditor::unit( unit, name ) );
		m_touchedUnit = m_units.end();
	}
}

void UnitEditor::onDropMoved( const cocos2d::Point & locationInView )
{}

void UnitEditor::onDropCanceled()
{}

void UnitEditor::selectLevel( int level, const pugi::xml_node& root )
{
	m_levelIndex = level;
	loadLevel(root);
}

void UnitEditor::insertText( const char * text, int len )
{
	if( len && text[0] == 'e' )
		remove();
}

void UnitEditor::deleteBackward()
{
	remove();
}

void UnitEditor::remove()
{
}

void UnitEditor::loadLevel( const pugi::xml_node& root_ )
{
	for( auto i : m_units ) removeChild( i.body );
	m_units.clear();

	pugi::xml_node root = root_;
	pugi::xml_node unitsXml = root.child( "units" );
	std::list< std::pair<float, Unit::Pointer> > units;
	GameBoard::loadUnits( units, unitsXml, std::map<int, TripleRoute>() );

	std::string pathToFile = kDirectoryToMaps + "map" + toStr( m_levelIndex ) + ".xml";
	pugi::xml_document doc;
	doc.load_file( pathToFile.c_str() );
	root = doc.root().first_child();
	unitsXml = root.child( "units" );
	auto xmlIter = unitsXml.first_child();
	for( auto i : units )
	{
		auto name = xmlIter.attribute( "name" ).as_string();
		auto name2 = xmlIter.attribute( "name2" ).as_string();
		auto pairunitname = xmlIter.attribute( "pairunitname" ).as_string();
		auto addZ = xmlIter.attribute( "additionalZorder" ).as_int();
		auto route = xmlIter.attribute( "routeindex" ).as_int(-1);
		auto delay = xmlIter.attribute( "delay" ).as_float();
		auto velocity = xmlIter.attribute( "velocity" ).as_float();

		m_units.push_back( UnitEditor::unit( i.second, name ) );
		m_units.back().name2 = name2;
		m_units.back().pairunitname = pairunitname;
		m_units.back().addZ = addZ;
		m_units.back().routeindex = route;
		m_units.back().delay = delay;
		m_units.back().velocity = velocity;
		addChild( m_units.back().body );
		auto pos = m_units.back().body->getPosition();
		m_units.back().body->setLocalZOrder( -pos.y + m_units.back().addZ );

		xmlIter = xmlIter.next_sibling();
	}
}

void UnitEditor::saveLevel( pugi::xml_node& root_ )
{
	std::string pathToFile = kDirectoryToMaps + toStr( m_levelIndex ) + ".xml";
	pathToFile = FileUtils::getInstance()->fullPathForFilename( pathToFile.c_str() );

	pugi::xml_node root = root_.root();
	pugi::xml_node mapNode = root.child( "map" );
	pugi::xml_node unitsNode = mapNode.child( "units" );
	if( unitsNode )
		mapNode.remove_child( unitsNode );

	unitsNode = mapNode.append_child( "units" );
	for( auto& i : m_units )
	{
		pugi::xml_node node = unitsNode.append_child( "unit" );
		Point pos = i.body->getPosition();
		node.append_attribute( "name" ).set_value( i.name.c_str() );
		node.append_attribute( "pos" ).set_value( toStr( pos ).c_str( ) );
		if( i.addZ != 0 )node.append_attribute( "additionalZorder" ).set_value( i.addZ );
		if( i.name2.empty() == false )node.append_attribute( "name2" ).set_value( i.name2.c_str() );
		if( i.pairunitname.empty() == false )node.append_attribute( "pairunitname" ).set_value( i.pairunitname.c_str() );
		if( i.routeindex > -1 )node.append_attribute( "routeindex" ).set_value( i.routeindex );
		if( i.delay > 0 )node.append_attribute( "delay" ).set_value( i.delay );
		if( i.velocity > 0 )node.append_attribute( "velocity" ).set_value( i.velocity );
	}
}

NS_CC_END;
#endif