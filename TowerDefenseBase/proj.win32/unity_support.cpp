#if TESTING != 1	

#include "support\AutoPlayer.cpp"
#include "support\Award.cpp"
#include "support\EventsGame.cpp"
#include "support\Animations.cpp"
#include "support\GarbageParams.cpp"
#include "support\Log.cpp"
#include "support\MenuItemTextBG.cpp"
#include "support\ScoreCounter.cpp"
#include "support\support.cpp"
#include "support\UserData.cpp"
#include "support\MenuItemCooldown.cpp"

#endif