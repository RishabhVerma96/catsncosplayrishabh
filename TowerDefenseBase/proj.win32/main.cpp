#include "main.h"
#include "AppDelegate.h"
#include "cocos2d.h"
#include "support.h"
#include "testing/Testing.h"

#if STEAMBUILD == 1
#include "steam_api.h"
#endif

USING_NS_CC;

#if STEAMBUILD == 1
void MiniDumpFunction(unsigned int nExceptionCode, EXCEPTION_POINTERS *pException)
{
	// You can build and set an arbitrary comment to embed in the minidump here,
	// maybe you want to put what level the user was playing, how many players on the server,
	// how much memory is free, etc...
	SteamAPI_SetMiniDumpComment("Minidump comment: SteamworksExample.exe\n");

	// The 0 here is a build ID, we don't set it
	SteamAPI_WriteMiniDump(nExceptionCode, pException, 0);
}
#endif



std::string getAttribute(const std::vector<std::string>& attrs, const std::string& name)
{
	if (attrs.empty() == false)
		for (size_t i = 0; i < attrs.size() - 1; ++i)
		{
			if (attrs[i] == name)
				return attrs[i + 1];
		}
	return "";
}

bool hasAttrubute(const std::vector<std::string>& attrs, const std::string& name)
{
	if (attrs.empty() == false)
		for (size_t i = 0; i < attrs.size(); ++i)
		{
			if (attrs[i] == name)
				return true;
		}
	return false;
}

int APIENTRY _tWinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPTSTR    lpCmdLine,
	int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);

#if STEAMBUILD == 1
	if (!IsDebuggerPresent())
		_set_se_translator(MiniDumpFunction);
#endif
	if (lpCmdLine && nCmdShow > 0)
	{
		char buf[MAX_LOG_LENGTH];
		WideCharToMultiByte(CP_ACP, 0, lpCmdLine, -1, buf, sizeof(buf), nullptr, FALSE);

		std::vector<std::string> attrs;
		split(attrs, std::string(buf), ' ');
		if (attrs.size() > 0)
		{
			auto path = attrs[0];
			AppDelegate::gamePath = path;
		}
#if TESTING == 1
		Testing::logToConsole = strTo<bool>(getAttribute(attrs, "--console"));
#endif
		AppDelegate::isTestDevice = hasAttrubute(attrs, "--iamtester");
	}

	RECT desktop;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);
	AppDelegate::screenResolutionX = desktop.right - desktop.left;
	AppDelegate::screenResolutionY = desktop.bottom - desktop.top;



	// create the application instance
	AppDelegate app;
	return Application::getInstance()->run();
}
