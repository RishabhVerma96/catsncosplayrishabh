#if TESTING != 1	

#include "Services\admob\AdMob_dummy.cpp"
#include "Services\appgratis\appgratis_dummy.cpp"
#include "Services\appodeal\appodeal_dummy.cpp"
#include "Services\chartboost\chartboost_dummy.cpp"
#include "Services\deltadna\deltadna_dummy.cpp"
#include "Services\dosads\DOSAds_dummy.cpp"
#include "Services\playservices\playservices.cpp"
#include "Services\playservices\playservices_dummy.cpp"
#include "Services\flurry\flurry_dummy.cpp"
#include "Services\fyber\fyber_dummy.cpp"
#include "Services\plugins\AdsPlugin.cpp"
#include "Services\vungle\vungle_dummy.cpp"
#include "Services\inapp\Purchase.cpp"
#include "Services\inapp\Purchase_win32.cpp"
#endif