//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __ml_SUPPORT__
#define __ml_SUPPORT__
#pragma warning (disable : 4996)
#include "cocos2d.h"
#include "ml/pugixml/pugixml.hpp"
#include "ml/common.h"
#include "ml/Singlton.h"
#include "ml/Animation.h"
#include "ml/types.h"
#include "ml/macroses.h"
#include "ml/Config.h"
#include "ml/Actions.h"
#include "NodeExt.h"

#define DESSIZE   Director::getInstance()->getOpenGLView()->getDesignResolutionSize()
#define FRAMESIZE Director::getInstance()->getOpenGLView()->getFrameSize()

#define IS18_9Ratio (((FRAMESIZE.width/FRAMESIZE.height) >= 2.0) ? true : false)

NS_CC_BEGIN


typedef std::vector<Point> Route;

namespace UnitLayer
{
	typedef int type;
	const type earth = 1;
	const type sky = 2;
	const type sea = 4;
	const type any = earth | sky | sea;
};

enum class UnitType
{
	creep = 0,
	tower,
	desant,
	hero,
	airbomb,
	unstoppable,
	skill,
	other,
	none,
};

enum class RouteSubType
{
	defaultvalue = -1,
	random = defaultvalue,
	main = 0,
	left0 = 1,
	right0 = 2,
	max = 3,
};

struct TripleRoute
{
	UnitLayer::type type;
	bool onlyForHero;
	Route main;
	Route left;
	Route right;
};

enum class BodyType
{
	defaultvalue = 0,
	equipment = defaultvalue,
	meat = 1,
};

enum class GameMode
{
	normal = 1,
	hard,
	survival,
	multiplayer,
	_default = normal,
};


void showRadius( Node* parent, const Point & position, float radius );
void hideRadius( Node* parent );
bool radiusTowerIsHidden();

bool checkRadiusByEllipse( const Point & a, const Point & b, float radius );


struct ScrollTouchInfo : public Ref
{
	Point touchBegan;
	Point nodeposBegan;
	Point lastShift;
	NodePointer node;
	unsigned touchID;

	Point fitPosition( const Point & position, const Size & winsize );
};

class MouseHoverScroll : public Singlton<MouseHoverScroll>
{
public:
	MouseHoverScroll();
	~MouseHoverScroll();
	void update(float dt);
public:
	void enable();
	void disable();
	void mouseHover(Event*event);
	void disable_schedule( float );
private:
	float _scrollMouseHoverX;
	float _scrollMouseHoverY;
	Size _winSize;
	EventListenerMouse* _touchListener;
	bool _enabled;
	CC_SYNTHESIZE( IntrusivePtr< ScrollTouchInfo >, _scroller, Scroller );
	CC_SYNTHESIZE(NodePointer, _node, Node);
	CC_SYNTHESIZE(float, _velocity, Velocity);
	CC_SYNTHESIZE(float, _border, Border);
};

template <class T>
T compute_bezier( const T & a, const T &  b, const T &  c, const T &  d, float  t )
{
	return (powf( 1 - t, 3 ) * a + 3 * t*(powf( 1 - t, 2 ))*b + 3 * powf( t, 2 )*(1 - t)*c + powf( t, 3 )*d);
}
const Route & getNearestRouteWithSegment( size_t & segment, const std::vector<TripleRoute>& routes, const cocos2d::Point & basePoint, UnitLayer::type allowLayer, bool forHero = false );
Point getNearestPointInRoute( const std::vector<TripleRoute>& routes, const cocos2d::Point & basePoint, UnitLayer::type allowLayer, bool forHero = false);

bool checkPointOnRoute( const std::vector<TripleRoute>& routes, const cocos2d::Point & point, float maxDistanceToRoad, UnitLayer::type allowLayer, float * distance = nullptr );
bool checkPointOnRoute( const cocos2d::Point & point, const TripleRoute & route, float maxDistanceToRoad, float * distance = nullptr );

int getElapsedTimeFromPreviosLaunch( const std::string & timeId );

UnitLayer::type strToUnitLayer( const std::string & value );
std::string unitLayerToStr( UnitLayer::type unitLayer );
UnitType strToUnitType( const std::string & value );
std::string unitTypeToStr( UnitType unittype );
RouteSubType strToRouteSubType( const std::string & value );
BodyType strToBodyType( const std::string & value );


inline void setKeyDispatcherBackButton( LayerExt* layer )
{
	EventListenerKeyboard * event = EventListenerKeyboard::create();

	auto lambda = [layer]( EventKeyboard::KeyCode key, Event* )mutable
	{
		if( key == EventKeyboard::KeyCode::KEY_BACK )
			layer->runEvent( "onexit" );
	};
	event->onKeyReleased = std::bind( lambda, std::placeholders::_1, std::placeholders::_2);
	
	layer->getEventDispatcher()->addEventListenerWithSceneGraphPriority(event, layer);
};

std::string getDeviceID();
void setDeviceID( const std::string& deviceID );
void addTestDevice( const std::string& deviceID );
bool isTestDevice();
void setTestModeActive( bool active );
bool isTestModeActive();


bool requestInternetConnectionStatus();

void showCursor( bool isFullscreenMode );

NS_CC_END
#endif
