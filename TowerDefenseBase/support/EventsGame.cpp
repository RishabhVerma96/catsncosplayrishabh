//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 26.10.2014.
//
//
#include "EventsGame.h"
#include "gameboard.h"
#include "GameScene.h"
#include "GameLayer.h"
#include "ml/common.h"
#include "ml/SmartScene.h"
#include "ScoreCounter.h"
#include "scenes/gamegs/HeroTestDrive.h"
NS_CC_BEGIN

/*****************************************************************************/
//MARK: class EventCreateLaserBullet
/*****************************************************************************/
EventCreateLaserBullet::EventCreateLaserBullet()
: _startDelay(0)
{}

EventCreateLaserBullet::~EventCreateLaserBullet()
{}

bool EventCreateLaserBullet::init()
{
    return EventCreateUnit::init();
}

void EventCreateLaserBullet::execute( NodeExt * context )
{
    Unit* holder = dynamic_cast<Unit*>(context);
    auto units = createUnits( context );
    for( auto unit : units )
    {
        Point pos = holder->getPosition();
        if (unit->_bulletParams.size() > 0)
        {
            Point offset = unit->_bulletParams[holder->getMover().getCurrentAngle()].offset;
            pos += offset;
        }
        unit->setPosition(pos);
        unit->moveInLine( holder->getMover().getCurrentAngle(), _startDelay );
    }
}

void EventCreateLaserBullet::setParam( const std::string & name, const std::string & value )
{
    if (name == "startDelay") _startDelay = strTo<float>( value );
    else EventCreateUnit::setParam( name, value );
}

/*****************************************************************************/
//MARK: class EventCreateUnit
/*****************************************************************************/
EventCreateUnit::EventCreateUnit()
: _radius( 0 )
, _lifetime(30)
, _unitType( UnitType::creep )
, _runToRoad(false)
, _unitLayer(UnitLayer::any)
{}

EventCreateUnit::~EventCreateUnit()
{}

bool EventCreateUnit::init()
{
	return true;
}

void EventCreateUnit::execute( NodeExt * context )
{
	createUnits( context );
}

std::vector<Unit::Pointer> EventCreateUnit::createUnits( NodeExt * context )
{
	std::vector<Unit::Pointer> units;
	Unit * holder = dynamic_cast<Unit*>(context);
	assert( holder );
	if( _units.empty() )
	{
#if USE_CHEATS == 1
		auto name = holder->getName();
		MessageBox( ("See creep [" + name + "] event [createunit]. List units is empty :(").c_str(), name.c_str() );
		return units;
#endif
	}
	assert( _units.size() > 1 ? _radius > 0 : true );

	float startangle = CCRANDOM_MINUS1_1() * (360 / _units.size());
	std::vector<Point>points;
	computePointsByRadius( points, _radius, _units.size(), startangle );
	auto& board = *holder->getGameBoard();
	int index( 0 );
	for( const auto& unitname : _units )
	{
		auto& position = holder->getPosition() + points[index];
		++index;
		Unit::Pointer unit;
		if( _unitType == UnitType::creep )
		{
			Route route;
			size_t segment;
			bool holderIsCreep = holder->getType() == UnitType::creep;
			if( holderIsCreep )
				route = holder->getMover().getRoute();
			else
				route = getNearestRouteWithSegment( segment, board.getCreepsRoutes(), position, _unitLayer );
			unit = board.createCreep( unitname, route, position );
			if( unit == nullptr ) continue;
			if( holderIsCreep )
			{
				auto cost = holder->getCost();
				auto rate = holder->getRate();
				auto routeIndex = holder->getRouteIndex();
				auto rst = holder->getRouteSubType();
				auto routeSeg = holder->getMover().getRouteCurrentSegment();
				auto routePer = holder->getMover().getRouteCurrentSegmentPercent();
				unit->setCost( cost );
				unit->setRate( rate );
				units.push_back( unit );
				unit->setRouteIndex( routeIndex );
				unit->setRouteSubType( rst );
				unit->getMover().setRouteCurrentSegment( routeSeg, true );
				unit->getMover().setRouteCurrentSegmentPercent( routePer );
			}
			else
			{
				units.push_back( unit );
				unit->getMover().setRouteCurrentSegment( segment, true );
			}
		}
		else if( _unitType == UnitType::desant )
		{			
			auto basePosition = position;
			if( _runToRoad )
			{
				basePosition = getNearestPointInRoute( board.getCreepsRoutes(), position, _unitLayer );
			}
			unit = board.createActiveSkillUnit( unitname, basePosition, _lifetime );
			if( unit )
			{
				units.push_back( unit );
				unit->setPosition( position );
			}
		} 
		else if( _unitType == UnitType::unstoppable )
		{
			unit = board.createActiveSkillUnit( unitname, position );
			if( unit )
			{
				units.push_back( unit );
			}
		}
		else if( _unitType == UnitType::airbomb )
		{
			unit = board.createActiveSkillUnit( unitname, position );
			if( unit )
			{
				units.push_back( unit );
			}
		}
	}

	return units;
}

void EventCreateUnit::setParam( const std::string & name, const std::string & value )
{
	if( name == "units" ) split( _units, value );
	else if( name == "radius" ) _radius = strTo<float>( value );
	else if( name == "unittype" ) _unitType = strToUnitType( value );
	else if( name == "unitlayer" ) _unitLayer = strToUnitLayer( value );
	else if( name == "lifetime" ) _lifetime = strTo<float>( value );
	else if( name == "runtoroad" ) _runToRoad = strTo<bool>( value );
	else EventBase::setParam( name, value );
}

std::string EventCreateUnit::getParam( const std::string & name )
{
	return "";
}


/*****************************************************************************/
//MARK: class EventCreateUnitReverseRoute
/*****************************************************************************/

EventCreateUnitReverseRoute::EventCreateUnitReverseRoute()
	: _runFullRoute( false )
{}

EventCreateUnitReverseRoute::~EventCreateUnitReverseRoute()
{}

bool EventCreateUnitReverseRoute::init()
{
	return EventCreateUnit::init();
}

void EventCreateUnitReverseRoute::execute( NodeExt * context )
{
	Unit * holder = dynamic_cast<Unit*>(context);
	auto& board = *holder->getGameBoard();
	auto units = createUnits( context );
	float distance( 200 );

	//Can be changed for all units with reverse route.
	//Need to be tested, when it's spawned while hero is running between creeps routes.
	bool onlyCreepsRoutes = _runFullRoute;

	for( auto unit : units )
	{	
		TripleRoute route = board.getRoute( unit->getUnitLayer(), unit->getPosition(), distance, onlyCreepsRoutes );
		if( route.main.empty() )
			continue;
		std::reverse( route.main.begin(), route.main.end() );
		
		if (_runFullRoute) 
		{
			unit->setPosition( route.main.front() );
		}
		unit->moveByRoute( route.main );
	}
}

void EventCreateUnitReverseRoute::setParam( const std::string & name, const std::string & value )
{
	if (name == "runfullroute") _runFullRoute = strTo<bool>( value );
	else EventCreateUnit::setParam( name, value );
}

/*****************************************************************************/
//MARK: class EventAreaDamage
/*****************************************************************************/
EventAreaDamage::EventAreaDamage()
: _radius( 0 )
, _sector( 360 )
, _asUnitType( UnitType::tower )
, _damage(-1.f)
{}

EventAreaDamage::~EventAreaDamage()
{}

bool EventAreaDamage::init()
{
	return true;
}

void EventAreaDamage::execute( NodeExt * context )
{
	Unit * holder = dynamic_cast<Unit*>(context);
	assert( context );
	assert( holder );
	auto & board = *holder->getGameBoard();

	float radius = holder->getRadius();
	float sector = holder->getDamageBySectorAngle();
	auto unittype = holder->getType();
	auto damage = holder->getEffect().positive.damage;
	holder->setRadius( _radius );
	holder->setType( _asUnitType );
	holder->setDamageBySectorAngle( _sector );
	if(_damage != -1.f )
		holder->getEffect().positive.damage = _damage;	

	board.applyDamageBySector( holder );

	holder->setRadius( radius );
	holder->setDamageBySectorAngle( sector );
	holder->setType( unittype );
	holder->getEffect().positive.damage = damage;
}

void EventAreaDamage::setParam( const std::string & name, const std::string & value )
{
	if( name == "radius" )_radius = strTo<float>( value );
	else if( name == "sector" ) _sector = strTo<float>( value );
	else if( name == "asunittype" ) _asUnitType= strToUnitType( value );
	else if( name == "damage" ) _damage = strTo<float>( value );
	else EventBase::setParam( name, value );
}

std::string EventAreaDamage::getParam( const std::string & name )
{
	return "";
}


/*****************************************************************************/
//MARK: class EventCreateEffect
/*****************************************************************************/
EventCreateEffect::EventCreateEffect()
{}

EventCreateEffect::~EventCreateEffect()
{}

bool EventCreateEffect::init()
{
	return true;
}

void EventCreateEffect::execute(NodeExt * context)
{
	Unit * holder = dynamic_cast<Unit*>(context);
	assert(context);
	assert(holder);
	holder->getGameBoard()->getGameLayer()->createEffect( holder, holder, _effectDescription );
}

void EventCreateEffect::setParam(const std::string & name, const std::string & value)
{
	if (name == "description") _effectDescription = value;
	else EventBase::setParam(name, value);
}

std::string EventCreateEffect::getParam(const std::string & name)
{
	return "";
}


/*****************************************************************************/
//MARK: class EventDestroyTarget
/*****************************************************************************/
EventDestroyTarget::EventDestroyTarget()
: _gears( 0 )
, _count( 0 )
, _radius( 0 )
{}

EventDestroyTarget::~EventDestroyTarget()
{}

bool EventDestroyTarget::init()
{
	return true;
}

void EventDestroyTarget::execute( NodeExt * context )
{
	auto unit = dynamic_cast<Unit*>(context);
	assert( unit );
	if( !unit )
		return;
	auto& board = *unit->getGameBoard();

	std::vector<Unit*> units;
	std::vector<Unit*> targets;
	board.getTargetsByRadius( unit, units, unit->getPosition(), _radius );
	for( auto unit : units )
	{
		if( _units.find( unit->getName() ) != _units.end() )
			targets.push_back( unit );
		if( static_cast<int>(targets.size()) > _count )
			break;
	}

	int counter(0);
	for( auto target : targets )
	{
		++counter;
		if( counter > _count )
			break;
		target->setCurrentHealth( 0 );
		auto pos = target->getPosition();
		
		if( _action )
		{
			auto sprite = Sprite::create();
			board.getGameLayer()->getMainLayer()->addChild( sprite );
			sprite->runAction( _action->clone() );
			sprite->setPosition( pos );
		}

		if( _gears > 0 )
		{
			pos = board.getGameLayer()->getMainLayer()->convertToWorldSpace( pos );
			ScoreCounter::shared().addMoney( kScoreLevel, _gears, false );
		}
	}
}

void EventDestroyTarget::setParam( const std::string & name, const std::string & value )
{
	if( name == "allowunits" )
	{
		std::vector<std::string> units;
		split( units, value );
		for( auto unit : units )
		{
			_units.insert( unit );
		}
	}
	else if( name == "action" )
		_action = xmlLoader::load_action( value );
	else if( name == "count" )
		_count = strTo<int>( value );
	else if( name == "gears" )
		_gears = strTo<int>( value );
	else if( name == "radius" )
		_radius = strTo<float>( value );
	else
		EventBase::setParam( name, value );
}

std::string EventDestroyTarget::getParam( const std::string & name )
{
	return "";
}


/*****************************************************************************/
//MARK: class EventDestroyTarget
/*****************************************************************************/
EventRateTowers::EventRateTowers()
: _rate(1)
, _radius(0)
, _inverted(false)
{}

EventRateTowers::~EventRateTowers()
{}

bool EventRateTowers::init()
{
	return true;
}

void EventRateTowers::execute( NodeExt * context )
{
	auto unit = dynamic_cast<Unit*>(context);
	assert( unit );
	if( !unit ) return;
	auto& board = *unit->getGameBoard();

	std::vector<Unit*> towers;
	board.getTargetsByRadius( unit, towers, unit->getPosition(), _radius );
	for( auto iter = towers.begin(); iter != towers.end(); )
	{
		auto unit = *iter;
		auto type = unit->getType();
		if( type != UnitType::tower )
			iter = towers.erase( iter );
		else if( unit == context )
			iter = towers.erase( iter );
		else
			++iter;
	}
	for( auto tower : towers )
	{
		auto pos = tower->getPosition();

		if( _action )
		{
			auto sprite = Sprite::create();
			board.getGameLayer()->getMainLayer()->addChild( sprite, 9999 );
			sprite->runAction( _action->clone() );
			sprite->setPosition( pos );
		}

		if( _property == "rate" )
		{
			auto value = tower->getFireReadyDelay();
			if( _inverted )
				value /= _rate;
			else
				value *= _rate;
			tower->setFireReadyDelay( value );
		}
	}
}

void EventRateTowers::setParam( const std::string & name, const std::string & value )
{
	if( name == "property" )
		_property = value;
	else if( name == "rate" )
		_rate = strTo<float>( value );
	else if( name == "radius" )
		_radius = strTo<float>( value );
	else if( name == "action" )
		_action = xmlLoader::load_action( value );
	else if( name == "inverted" )
		_inverted = strTo<bool>( value );
	else
		EventBase::setParam( name, value );
}

std::string EventRateTowers::getParam( const std::string & name )
{
	return "";
}


/*****************************************************************************/
//MARK: class EventHeroTestDrive
/*****************************************************************************/
EventHeroTestDrive::EventHeroTestDrive() {}
EventHeroTestDrive::~EventHeroTestDrive() 
{}

bool EventHeroTestDrive::init()
{
	return true;
}

void EventHeroTestDrive::execute( NodeExt * context )
{
	bool useHeroes = Config::shared().get<bool>( "useHero" );
	if( !useHeroes )
		return;
	auto scene = dynamic_cast<GameScene*>(context->as_node_pointer()->getScene());
	auto layer = HeroTestDriveLayer::create( scene, _heroName, false );
	if( scene && layer )
	{
		scene->pushLayer( layer, true );
	}
}

void EventHeroTestDrive::setParam( const std::string & name, const std::string & value )
{
	if( name == "hero" )
		_heroName = value;
	else
		EventBase::setParam( name, value );
}

std::string EventHeroTestDrive::getParam( const std::string & name )
{
	if( name == "hero" )
		return _heroName;
	return "";
}

/*****************************************************************************/
//MARK: class EventHeroTestDrive
/*****************************************************************************/
EventHeroMoveTo::EventHeroMoveTo() {}
EventHeroMoveTo::~EventHeroMoveTo() {}
bool EventHeroMoveTo::init()
{
	return true;
}

void EventHeroMoveTo::execute( NodeExt * context )
{
	auto scene = dynamic_cast<GameScene*>(context->as_node_pointer()->getScene());
	auto& board = scene->getGameLayer()->getGameBoard();
	auto heroes = board.getHeroes();
	Hero::Pointer hero( nullptr );
	for( auto i : heroes )
	{
		if( i->getName() == _heroName )
		{
			hero = i;
			break;
		}
	}
	if( hero )
	{
		hero->moveTo( _to );
	}
}

void EventHeroMoveTo::setParam( const std::string & name, const std::string & value )
{
	if( name == "hero" || name == "target" )
		_heroName = value;
	else if( name == "to" )
		_to = strTo<Point>( value );
	else
		EventBase::setParam( name, value );
}

std::string EventHeroMoveTo::getParam( const std::string & name )
{
	return "";
}



NS_CC_END
