//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 26.10.2014.
//
//
#ifndef __EventsGame_h__
#define __EventsGame_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/Events.h"
#include "Unit.h"
NS_CC_BEGIN


class EventCreateUnit : public EventBase
{
	DECLARE_BUILDER( EventCreateUnit );
	bool init();
public:
	virtual void execute( NodeExt * context );
	virtual void setParam( const std::string & name, const std::string & value );
	virtual std::string getParam( const std::string & name );
protected:
	std::vector<Unit::Pointer> createUnits( NodeExt * context );
private:
	std::list<std::string> _units;
	float _radius;
	float _lifetime;
	UnitType _unitType;
	UnitLayer::type _unitLayer;
	bool _runToRoad;
};

class EventCreateLaserBullet : public EventCreateUnit
{
    DECLARE_BUILDER( EventCreateLaserBullet );
    bool init();
public:
    virtual void execute( NodeExt * context );
    virtual void setParam( const std::string & name, const std::string & value ) override;
private:
    float _startDelay;
};

class EventCreateUnitReverseRoute : public EventCreateUnit
{
	DECLARE_BUILDER( EventCreateUnitReverseRoute );
	bool init();
public:
	virtual void execute( NodeExt * context );
	virtual void setParam( const std::string & name, const std::string & value ) override;
private:
	bool _runFullRoute;
};

class EventAreaDamage : public EventBase
{
	DECLARE_BUILDER(EventAreaDamage);
	bool init();
public:
	virtual void execute(NodeExt * context);
	virtual void setParam(const std::string & name, const std::string & value);
	virtual std::string getParam(const std::string & name);
protected:
private:
	float _radius;
	float _sector;
	UnitType _asUnitType;
	float _damage;
};

class EventCreateEffect : public EventBase
{
	DECLARE_BUILDER(EventCreateEffect);
	bool init();
public:
	virtual void execute(NodeExt * context);
	virtual void setParam(const std::string & name, const std::string & value);
	virtual std::string getParam(const std::string & name);
protected:
private:
	std::string _effectDescription;
};

class EventDestroyTarget : public EventBase
{
	DECLARE_BUILDER( EventDestroyTarget );
	bool init();
public:
	virtual void execute(NodeExt * context);
	virtual void setParam(const std::string & name, const std::string & value);
	virtual std::string getParam(const std::string & name);
protected:
private:
	std::set<std::string> _units;
	ActionPointer _action;
	int _gears;
	int _count;
	float _radius;
};

class EventRateTowers : public EventBase
{
	DECLARE_BUILDER( EventRateTowers );
	bool init();
public:
	virtual void execute(NodeExt * context);
	virtual void setParam(const std::string & name, const std::string & value);
	virtual std::string getParam(const std::string & name);
protected:
private:
	float _rate;
	float _radius;
	bool _inverted;
	std::string _property;
	ActionPointer _action;
};

class EventHeroTestDrive : public EventBase
{
	DECLARE_BUILDER( EventHeroTestDrive );
	bool init();
public:
	virtual void execute( NodeExt * context );
	virtual void setParam( const std::string & name, const std::string & value );
	virtual std::string getParam( const std::string & name );
private:
	std::string _heroName;
};

class EventHeroMoveTo : public EventBase
{
	DECLARE_BUILDER( EventHeroMoveTo );
	bool init();
public:
	virtual void execute( NodeExt * context );
	virtual void setParam( const std::string & name, const std::string & value );
	virtual std::string getParam( const std::string & name );
private:
	std::string _heroName;
	Point _to;
};

NS_CC_END
#endif // #ifndef EventsGame
