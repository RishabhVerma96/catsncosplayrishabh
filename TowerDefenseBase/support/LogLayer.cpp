#include "LogLayer.h"
#include "ml/ImageManager.h"
NS_CC_BEGIN

#ifdef _DEBUG
LogLayer::LogLayer()
{}

LogLayer::~LogLayer()
{}

void LogLayer::onCreate()
{
	retain();
	LayerExt::init();
	auto size = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	auto shadow = ImageManager::shared().sprite( "images/square.png" );
	shadow->setScale( size.width / 3, size.height );
	shadow->setAnchorPoint( Point::ZERO );
	shadow->setColor( Color3B::WHITE );
	shadow->setOpacity( 128 );
	addChild( shadow );

	_cursor = ImageManager::shared().sprite( "images/square.png" );
	_cursor->setScale( 10 );
	_cursor->setAnchorPoint( Point( 0, 0.5f ) );
	_cursor->setColor( Color3B::RED );
	addChild( _cursor );

	setContentSize( Size( size.width / 3, size.height ) );
	setAnchorPoint( Point::ZERO );
	setGlobalZOrder( 9999 );

	auto touchL = EventListenerTouchOneByOne::create();
	touchL->onTouchBegan = std::bind( [&]( Touch*touch, Event* ){
		return getParent() && touch->getLocation().x < getContentSize().width;
	}, std::placeholders::_1, std::placeholders::_2 );

	touchL->onTouchMoved = std::bind( [&]( Touch*touch, Event* ){
		float min = std::min( 0.f, -(_contentHeight - getContentSize().height) );
		float max = 0;
		_textPosition += touch->getDelta().y;
		_textPosition = std::max( min, _textPosition );
		_textPosition = std::min( max, _textPosition );
		refreshCursor();

	}, std::placeholders::_1, std::placeholders::_2 );

	touchL->onTouchEnded = std::bind( [&]( Touch*touch, Event* ){
	}, std::placeholders::_1, std::placeholders::_2 );
	_eventDispatcher->addEventListenerWithFixedPriority( touchL, -99999 );

	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = std::bind( [&]( EventKeyboard::KeyCode keycode, Event* )
	{
		if( keycode == EventKeyboard::KeyCode::KEY_TAB )
		{
			if( getParent() ) hide();
			else show();
		}
	}, std::placeholders::_1, std::placeholders::_2 );
	_eventDispatcher->addEventListenerWithFixedPriority( listener, -99999 );
	_keyboardListener = listener;

	Director::getInstance()->getScheduler()->schedule( std::bind( &LogLayer::update, this, std::placeholders::_1 ), this, 0, false, "loglayer" );
}

void LogLayer::show()
{
	if( getParent() == nullptr )
		Director::getInstance()->getRunningScene()->addChild( this, 9999 );
}

void LogLayer::hide()
{
	removeFromParent();
}

void LogLayer::log( const std::string& msg, Color3B color )
{
	cocos2d::log( "%s", msg.c_str() );
	auto label = Label::createWithTTF( msg, "dev/font.ttf", 14 );
	label->setWidth( getContentSize().width - 20 );
	label->setAnchorPoint( Point::ZERO );
	label->setColor( color );
	addChild( label );
	_logsNotSee.insert( _logsNotSee.begin(), label );

	if( _textPosition >= -10 )
	{
		_logs.insert( _logs.begin(), _logsNotSee.begin(), _logsNotSee.end() );
		_logsNotSee.clear();
	}
	refreshCursor();
}

void LogLayer::update( float dt )
{
	auto parent = getParent();
	auto scene = Director::getInstance()->getRunningScene();
	if( parent && parent != scene )
	{
		removeFromParent();
		scene->addChild( parent );
	}
}

void LogLayer::refreshCursor()
{
	int index( 0 );
	float X( 10 );
	float Y( _textPosition );
	float H = getContentSize().height;
	_contentHeight = 0;
	for( auto label : _logs )
	{
		float h = label->getContentSize().height;
		label->setPosition( X, Y );
		Y += h + 5;
		++index;
		if( index >= 200 )
			break;
	}
	_contentHeight = (Y + 10) - _textPosition;
	for( int i = index; i < _logs.size(); ++i )
		removeChild( _logs[i] );
	_logs.erase( _logs.begin() + index, _logs.end() );

	float diff = -(_contentHeight - getContentSize().height);
	float y = (_textPosition / diff) * getContentSize().height;
	_cursor->setPositionY( y );
}
#else
LogLayer::LogLayer() {}
LogLayer::~LogLayer() {}
void LogLayer::onCreate() {}
void LogLayer::show() {}
void LogLayer::hide() {}
void LogLayer::log( const std::string& msg, Color3B color ) {}
void LogLayer::update( float dt ) {}
void LogLayer::refreshCursor() {}

#endif
NS_CC_END