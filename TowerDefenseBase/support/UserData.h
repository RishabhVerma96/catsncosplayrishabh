//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __USER_DATA__
#define __USER_DATA__

#include "cocos2d.h"
#include "support.h"
#include "Observer.h"
NS_CC_BEGIN;

class UserData : public Singlton<UserData>
{
public:
	UserData();
	virtual ~UserData();
	void clear( );
	void save();

public:
	ObServer<UserData, std::function<void()>> observerHeroes;
	ObServer<UserData, std::function<void(const std::string& name)>> observerTowerUpgrade;
public:
	void level_complete( unsigned index );
	int level_incrementPassed();
	int level_getCountPassed();
	int level_getScoresByIndex(int levelIndex);
	void level_setCountPassed(int value);
	void level_setScoresByIndex(int levelIndex, int value);

	int tower_upgradeLevel( const std::string & name );
	void tower_upgradeLevel( const std::string & name, int level );
	float tower_damage( const std::string & name );
	float tower_radius( const std::string & name );
	float tower_rateshoot( const std::string & name );
	void tower_damage( const std::string & name, float value );
	void tower_radius( const std::string & name, float value );
	void tower_rateshoot( const std::string & name, float value );

	int bonusitem_add( unsigned index, int count );
	int bonusitem_sub( unsigned index, int count = 1 );
	int bonusitem_count( unsigned index );
	
	void hero_setActiveSlot( int slot );
	void hero_select( unsigned index );
	void hero_unselect( unsigned index );
	std::vector<unsigned> hero_getSelected();
	void hero_writeArray( std::vector<unsigned> heroes );
    
    int getWatchedVideoCountForHero(const std::string & name);
    int incrementWatchedVideoCountForHero(const std::string & name);

    
public:
	template <typename T> void write(const std::string &key, const T& value) { write_string(key, toStr(value)); }
	template <typename T = std::string> T get(const std::string &key, const T& defaultValue = T()) { return strTo <T>(get_string(key, toStr(defaultValue))); }

private:
	pugi::xml_node getXmlRoot();
	void write_string(const std::string &key, const std::string &value);
	std::string get_string( const std::string &key, const std::string &defaultValue = "" );

	void cloudInit();
	void cloudSync();
	void syncFromCloud( const std::map<std::string, std::string>& values );
	void syncFromCloud( const std::string& values );
	void onRecieveCloudData();
private:
	int _currentHeroSlot;
	std::map<std::string, std::string> _cloudData;
};

NS_CC_END;
#endif
