//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "UserData.h"
#include "consts.h"
#include "configuration.h"
#include "ScoreCounter.h"
#include "ml/pugixml/pugixml.hpp"
#include "playservices/playservices.h"

NS_CC_BEGIN;

namespace UD
{
	static std::string kFile( "xmlfile_pugi.xml" );
	pugi::xml_document Doc;
	void open()
	{
		std::string path = FileUtils::getInstance()->getWritablePath();
		kFile = path + kFile;
		CCLOG("User data xml file path :: %s",kFile.c_str());
	#ifdef _DEBUG
		//clear( );
	#endif
		if( FileUtils::getInstance()->isFileExist( kFile ) )
			Doc.load_file( kFile.c_str() );
	}

	void save()
	{
		Doc.save_file( kFile.c_str() );
	}
}

UserData::UserData()
: _currentHeroSlot(-1)
{
	UD::open();
	cloudInit();
	//testMagicSquare();
	//test_encryption();
}

UserData::~UserData()
{}

pugi::xml_node UserData::getXmlRoot()
{
	auto root = UD::Doc.root().child( "root" );
	return root;
}

void UserData::write_string(const std::string &key, const std::string &value)
{
	std::string V = value;
	auto root = getXmlRoot();
	if( !root )
		root = UD::Doc.root().append_child( "root" );
	auto node = root.child( key.c_str( ) );
	if( !node )
		node = root.append_child( key.c_str( ) );
	auto attr = node.attribute( "value" );
	if( !attr )
		attr = node.append_attribute( "value" );
	attr.set_value( V.c_str() );	
}

std::string UserData::get_string(const std::string &key, const std::string &defaultvalue)
{
	auto root = getXmlRoot();
	auto node = root.child( key.c_str( ) );
	std::string value = node.attribute( "value" ).as_string( "" );
	if( value.empty() == false )
		value = value;
	else
		value = defaultvalue;
	return value;
}


void UserData::clear()
{
	UD::Doc.reset();
	UD::Doc.save_file( UD::kFile.c_str() );
}

void UserData::save()
{
	UD::save();
	cloudSync();
}

void UserData::level_complete( unsigned index )
{
	std::string key = kUser_Level_prefix + toStr( index ) + kUser_Complite_suffix;
	write( key, kUserValue_CompliteYes );

	int countPasses = level_getCountPassed();
	if( (int)index >= countPasses )
		level_incrementPassed();

}

int UserData::level_incrementPassed()
{
	int count = level_getCountPassed();
#ifdef DEMO_VERSION
	if( count >= 2 )
		return count;
#endif

	level_setCountPassed( count + 1 );

	return count + 1;
}

int UserData::level_getCountPassed()
{
	return get <int> (kUser_passedLevels);
}

int UserData::level_getScoresByIndex( int levelIndex )
{
	std::string key = kUser_Level_prefix + toStr( levelIndex ) + kUser_Scores_suffix;
	return get <int> (key);
}

void UserData::level_setCountPassed( int value )
{
	write( kUser_passedLevels, value );
}

void UserData::level_setScoresByIndex( int levelIndex, int value )
{
	int saved = level_getScoresByIndex( levelIndex );
	if( value >= saved )
	{
		std::string key = kUser_Level_prefix + toStr( levelIndex ) + kUser_Scores_suffix;
		write( key, value );
	}
}
int UserData::getWatchedVideoCountForHero(const std::string & name)
{
    std::string key = kHeroPrefix + name;
    return get <int> (key);
}
int UserData::incrementWatchedVideoCountForHero(const std::string & name)
{
    std::string key = kHeroPrefix + name;
    int currentCount = getWatchedVideoCountForHero(name);
    currentCount++;
    write( key, currentCount );
    save();
    return currentCount;
}



/*********************************************************************/
//	tower upgrade
/*********************************************************************/
int UserData::tower_upgradeLevel( const std::string & name )
{
	std::string key = kUserTowerUpgradeLevel + name;
	return get <int> (key);
}

void UserData::tower_upgradeLevel( const std::string & name, int level )
{
	assert( level >= 0 );
	std::string key = kUserTowerUpgradeLevel + name;
	write( key, level );
	observerTowerUpgrade.pushevent( name );
}

float UserData::tower_damage( const std::string & name )
{
	std::string key = kUserTowerUpgradeDamage + name;
	return get(key, 1.0f);
}

float UserData::tower_radius( const std::string & name )
{
	std::string key = kUserTowerUpgradeRadius + name;
	return get(key, 1.0f);
}

float UserData::tower_rateshoot( const std::string & name )
{
	std::string key = kUserTowerUpgradeRateShoot + name;
	return get(key, 1.0f);
}

void UserData::tower_damage( const std::string & name, float value )
{
	assert( value >= 1 );
	std::string key = kUserTowerUpgradeDamage + name;
	write( key, value );
}

void UserData::tower_radius( const std::string & name, float value )
{
	assert( value >= 1 );
	std::string key = kUserTowerUpgradeRadius + name;
	write( key, value );
}

void UserData::tower_rateshoot( const std::string & name, float value )
{
	assert( value >= 1 );
	std::string key = kUserTowerUpgradeRateShoot + name;
	write( key, value );
}

int UserData::bonusitem_add( unsigned index, int count )
{
	int have = bonusitem_count( index );
	have = std::max( 0, count + have );
	write( k::user::BonuseItem + toStr( index ), have );
	return have;
}

int UserData::bonusitem_sub( unsigned index, int count )
{
	return bonusitem_add( index, -count );
}

int UserData::bonusitem_count( unsigned index )
{
	return get <int> (k::user::BonuseItem + toStr(index));
}

void UserData::hero_setActiveSlot( int slot )
{
	_currentHeroSlot = slot;
}

void UserData::hero_select( unsigned index )
{
	//write( k::user::HeroCurrent, (int)index );
	int max = Config::shared().get<int>( "heroesCountSelectMax" );
	auto current = hero_getSelected();
	assert( _currentHeroSlot < max );

	auto iter = std::find( current.begin(), current.end(), index );
	if( iter != current.end() )
	{
		current.erase( iter );
	}

	if( _currentHeroSlot != -1 )
	{
		auto size = std::max<size_t>( _currentHeroSlot + 1, current.size() );
		current.resize( size );
		current[_currentHeroSlot] = index;
	}
	else
	{
		if( (int)current.size() >= max )
		{
			current.erase( current.begin() );
		}

		current.push_back( index );
	}
	hero_writeArray( current );
}

void UserData::hero_unselect( unsigned index )
{
	auto current = hero_getSelected();
	auto iter = std::find( current.begin(), current.end(), index );
	if( iter != current.end() )
		current.erase( iter );
	hero_writeArray( current );
}

std::vector<unsigned> UserData::hero_getSelected()
{
	std::string str = get <std::string> ( k::user::HeroCurrent );
	std::vector<std::string> sheroes;
	std::vector<unsigned> heroes;
	split( sheroes, str );
	for( auto hero : sheroes )
		heroes.push_back( strTo<int>( hero ) );
	int i = 0;
	while( (int)heroes.size() < Config::shared().get<int>( "heroesCountSelectMin" ) )
	{
		heroes.push_back( i++ );
	}
	while( (int)heroes.size() > Config::shared().get<int>( "heroesCountSelectMax" ) )
	{
		heroes.pop_back();
	}
	return heroes;
}

void UserData::hero_writeArray( std::vector<unsigned> heroes )
{
	bool repeat = true;
	while( repeat )
	{
		repeat = false;
		for( auto iter = heroes.begin(); iter != heroes.end(); ++iter )
		{
			auto find = std::find( iter + 1, heroes.end(), *iter );
			if( find != heroes.end() )
			{
				heroes.erase( find );
				repeat = true;
				break;
			}
		}
	}

	std::string rec;
	if( heroes.empty() == false )
	{
		for( auto i : heroes )
		{
			rec += toStr( i ) + ",";
		}
		rec.erase( rec.begin() + (rec.size() - 1) );
	}
	write( k::user::HeroCurrent, rec );

	observerHeroes.pushevent();
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 || CLOUDSYNC == 0)

void UserData::cloudInit() {}
void UserData::cloudSync() {}
void UserData::syncFromCloud( const std::string& values ) {}
void UserData::syncFromCloud( const std::map<std::string, std::string>& values ) {}
void UserData::onRecieveCloudData() {}

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
void UserData::cloudInit() {
	PlayServises::CloudUserData::cloudNotify = std::bind<void(UserData::*)(const std::string&)>(&UserData::syncFromCloud, this, std::placeholders::_1 );
}
void UserData::cloudSync() {
	int useiCould = get<int>( "icloud_sync", 0 );
	if( useiCould == 1 )
		return;

	ParamCollection pc;
	auto root = getXmlRoot();
	for( auto node : root )
	{
		auto k = node.name();
		auto v = node.attribute( "value" ).as_string( "" );
		pc.set( k, v );
	}
	log("UserData::cloudSync()");
	log("pc = %s", pc.string().c_str());
	int localGameTime = get<int>( "gametime" );
	log("localGameTime = %d", localGameTime);
	PlayServises::CloudUserData::save( pc.string() );
}
void UserData::syncFromCloud( const std::string& values ) {	
	ParamCollection pc( values );
	syncFromCloud( pc );
}

void UserData::syncFromCloud( const std::map<std::string, std::string>& values )
{
	log("UserData::syncFromCloud()");
	int localGameTime = get<int>( "gametime" );
	auto iter = values.find( "gametime"  );
	int cloudGameTime = iter != values.end() ? strTo<int>(iter->second) : 0;
	_cloudData = values;
	log("cloudGameTime = %d, localGameTime = %d", cloudGameTime, localGameTime);
	if( _cloudData.empty() == false && cloudGameTime >= localGameTime )
		onRecieveCloudData();
}

void UserData::onRecieveCloudData() {
	auto sync = [&]( bool use )
	{
		if( use )
		{
			write( "icloud_sync", 2 );
			for( auto pair : _cloudData )
				write_string( pair.first, pair.second );
			ScoreCounter::shared().onCreate();
			save();
		}
		else
		{
			write( "icloud_sync", 1 );
		}
	};

	PlayServises::CloudUserData::UIAlertNotify = std::bind( sync, std::placeholders::_1 );

	int useiCould = get<int>( "icloud_sync", 0 );
	if( useiCould == 0 )
	{
		PlayServises::CloudUserData::showAlertDialog();
	}
	else
	{
		sync( useiCould == 2 );
	}
}

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//implemented in ios_impl/UserData.mm
#endif

#undef user
NS_CC_END;
