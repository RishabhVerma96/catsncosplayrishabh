#ifndef __Award_h__
#define __Award_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/loadxml/xmlLoader.h"
NS_CC_BEGIN

enum class AwardType
{
	score,
	towerUpgrade,
	heroPoints,
	bonus,
	unknown,
};

class Award : public Ref
{
public:
	typedef IntrusivePtr<Award> Pointer;
public:
	virtual AwardType type() = 0;
	virtual void get() = 0;
	virtual void load( const pugi::xml_node& node ) = 0;
protected:
private:
};

class AwardScore : public Award
{
	DECLARE_BUILDER( AwardScore );
	bool init();
public:
	virtual AwardType type() override { return AwardType::score; }
	virtual void get()override;
	virtual void load( const pugi::xml_node& node )override;

	std::string getScoreName()const;
	int getCount()const;
public:
protected:
private:
	int _score;
	int _count;
};

class AwardTowerUpgrade : public Award
{
	DECLARE_BUILDER( AwardTowerUpgrade );
	bool init();
public:
	virtual AwardType type() override { return AwardType::towerUpgrade; }
	virtual void get()override;
	virtual void load( const pugi::xml_node& node )override;

	std::string getTowerName()const;
protected:
private:
	std::string _towerName;
};

class AwardHeroPoint : public Award
{
	DECLARE_BUILDER( AwardHeroPoint );
	bool init();
public:
	virtual AwardType type() override { return AwardType::heroPoints; }
	virtual void get()override;
	virtual void load( const pugi::xml_node& node )override;
protected:
private:
	int _points;
};

class AwardBonus : public Award
{
	DECLARE_BUILDER( AwardBonus );
	bool init();
public:
	virtual AwardType type() override { return AwardType::bonus; }
	virtual void get()override;
	virtual void load( const pugi::xml_node& node )override;	

	int getBonusIndex()const;
	int getCount()const;
protected:
private:
	int _bonusIndex;
	int _count;
};

NS_CC_END
#endif // #ifndef Award