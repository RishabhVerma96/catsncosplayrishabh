//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __SCORE_COUNTER__
#define __SCORE_COUNTER__

#include "support.h"
#include "ml/Observer.h"

#include "network/HttpClient.h"

using namespace cocos2d::network;

NS_CC_BEGIN;

const int kScoreLevel( 0 );
const int kScoreCrystals( 1 );
const int kScoreHealth( 2 );
const int kScoreFuel( 3 );
const int kScoreTime( 4 );
const int kScoreStars( 5 );
const int kScoreTicket( 6 );
const int kScoreSurvival( 7 );

const int kScoreOpponentLevel( 10 );
const int kScoreOpponentHealth( 11 );
const int kScoreOpponentSurvival( 12 );

const int kScoreLootPicker( 20 );

class ScoreCounter : public Singlton<ScoreCounter>
{
	friend class Singlton<ScoreCounter>;
	typedef ObServer< ScoreCounter, std::function<void( int )> > Observer_OnChangeScore;
protected:
	ScoreCounter(void);
	~ScoreCounter(void);
public:
    virtual void onCreate( ) override;
    
   
   
	void setMoney(int id, int value, bool saveValueToUserData);
	void addMoney(int id, int value, bool saveValueToUserData);
	void subMoney(int id, int value, bool saveValueToUserData, const std::string& wherespent);
  
	int getMoney(int id)const;
	Observer_OnChangeScore& observer( int id );
private:
	void change(int id, int value, bool saveValueToUserData);
protected:
	std::map<unsigned, int> m_scores;

	std::map<int, Observer_OnChangeScore> _onChangeScores;
};

class ScoreByTime : public Singlton<ScoreByTime>, public Ref
{
protected:
	friend class Singlton<ScoreByTime>;
	ScoreByTime( );
	~ScoreByTime();
	virtual void onCreate( );
public:
	void checktime( );
	void savetime( );
    
    bool isInternetConnected();
    void setInternetConnected(bool status);

	void checkMaxValue();
	int max_fuel()const { return _max; }
protected:
	void update( float dt );
	void changeTime( int score );
private:
	float _timer;
	int _time;
	int _interval;
	int _max;
    bool _isInternetConnected;
	CC_SYNTHESIZE_READONLY( int, _gameTime, TotalGameTime );
    
    void sendTimeRequest(std::string tag);
    void onTimeResponseRecieved(HttpClient *sender, HttpResponse *response);
    
};


class LevelParams : public Singlton<LevelParams>
{
	friend class Singlton<LevelParams>;
	virtual void onCreate( );
public:
	struct MultiplayerBet
	{
		int type;
		int cost;
		int award;
	};
public:
	size_t getMultiplayerCount()const;
	MultiplayerBet getMultiplayerBet( int index )const;
	void setPayForMultiplayer( int indexBet );
    
    bool isLastLevelWon() const;
    void setLastLevelWon(bool isLastLevelWon);

	void onLevelFinished( int levelIndex, int stars, GameMode mode );
	void onLevelFinishedSurvival( int levelIndex, int wavesComplete, GameMode mode );
	void onLevelStarted( int levelIndex, GameMode mode );

	int getMaxStars( int level, GameMode mode )const;
	int getAwardGold( int levelIndex, int stars, GameMode mode )const;
	
	void getPayForLevel( int levelIndex, GameMode mode, int& scoreType, int& score );
	int getTickets( int level, GameMode mode )const;

	int getStartGear( int level, GameMode mode )const;
	int getWaveCount( int level, GameMode mode )const;
	int getLives( int level, GameMode mode )const;
	std::string getExclude( int level, GameMode mode )const;
protected:
	int _getFuel( int levelIndex, GameMode mode )const;
	int _getGold( int levelIndex, GameMode mode )const;
	void loadRealParams();
	void loadLevelParams();
	void parceLevel( int index );
private:
	struct Level
	{
		struct 
		{
			std::vector<int> stars;
			int fuel;
			int gold;
			int waves;
			int lifes;
			int gear;
			int tickets;
			std::string exclude;
		}normal;
		struct hard
		{
			int stars;
			int award;
			int fuel;
			int gold;
			int waves;
			int lifes;
			int gear;
			int tickets;
			std::string exclude;
		}hard;
	};
	std::map<int, Level> _params;
	std::vector<MultiplayerBet> _multiplayerBet;
	size_t _currentBetIndex;
	int _goldOnFirstRun;
    bool _isLastLevelWon = false;
	
};
NS_CC_END;
#endif
