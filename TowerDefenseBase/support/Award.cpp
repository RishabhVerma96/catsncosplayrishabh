#include "Award.h"
#include "UserData.h"
#include "ScoreCounter.h"
#include "game/tower.h"

NS_CC_BEGIN

/***************************************************************************/
//MARK: AwardScore
/***************************************************************************/

AwardScore::AwardScore()
: _score(0)
, _count(0)
{}

AwardScore::~AwardScore()
{}

bool AwardScore::init()
{
	return true;
}

void AwardScore::get()
{
	ScoreCounter::shared().addMoney( _score, _count, true );
}

void AwardScore::load( const pugi::xml_node& node )
{
	_count = node.attribute( "count" ).as_int();
	std::string score = node.attribute( "score" ).as_string();
	if( score == "gold" ) _score = kScoreCrystals;
	else if( score == "ticket" ) _score = kScoreTicket;
	else if( score == "gears" ) _score = kScoreLevel;
	else if( score == "fuel" ) _score = kScoreFuel;
	else assert( 0 );
}

std::string AwardScore::getScoreName()const
{
	switch( _score )
	{
		case kScoreCrystals: return "gold";
		case kScoreTicket: return "ticket";
		case kScoreLevel: return "gears";
		case kScoreFuel: return "fuel";
		default:
			assert( 0 );
	}
	return "";
}

int AwardScore::getCount()const
{
	return _count;
}

/***************************************************************************/
//MARK: AwardTowerUpgrade
/***************************************************************************/

AwardTowerUpgrade::AwardTowerUpgrade()
{}

AwardTowerUpgrade::~AwardTowerUpgrade()
{}

bool AwardTowerUpgrade::init()
{
	return true;
}

void AwardTowerUpgrade::get()
{
	int level = UserData::shared().tower_upgradeLevel( _towerName );
	level = std::min( level + 1, 5 );
	UserData::shared().tower_upgradeLevel( _towerName, level );
}

void AwardTowerUpgrade::load( const pugi::xml_node& node )
{
	_towerName = node.attribute( "tower" ).as_string();
	if( _towerName == "random" )
	{
		std::list<std::string> towers;
		mlTowersInfo::shared().fetch( towers );
		int index = rand() % towers.size();
		auto iter = towers.begin();
		while( index > 0 )
		{
			++iter;
			--index;
		}
		_towerName = *iter;
	}
}

std::string AwardTowerUpgrade::getTowerName()const
{
	return _towerName;
}

/***************************************************************************/
//MARK: AwardHeroPoint
/***************************************************************************/

AwardHeroPoint::AwardHeroPoint()
: _points(0)
{}

AwardHeroPoint::~AwardHeroPoint()
{}

bool AwardHeroPoint::init()
{
	return true;
}

void AwardHeroPoint::get()
{
	auto current = UserData::shared().get <int>("heropoints_gift");
	current += _points;
	UserData::shared().write("heropoints_gift", current);
}

void AwardHeroPoint::load( const pugi::xml_node& node )
{
	_points = node.attribute( "points" ).as_int();
}

/***************************************************************************/
//MARK: AwardBonus
/***************************************************************************/

AwardBonus::AwardBonus()
{}

AwardBonus::~AwardBonus()
{}

bool AwardBonus::init()
{
	return true;
}

void AwardBonus::get()
{
	UserData::shared().bonusitem_add( _bonusIndex, _count );
}

void AwardBonus::load( const pugi::xml_node& node )
{
	_bonusIndex = node.attribute( "index" ).as_int();
	_count = node.attribute( "count" ).as_int();
}

int AwardBonus::getBonusIndex() const
{
	return _bonusIndex;
}

int AwardBonus::getCount() const
{
	return _count;
}
NS_CC_END