#ifndef __LogLayer_h__
#define __LogLayer_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "ml/Singlton.h"
NS_CC_BEGIN





class LogLayer : public LayerExt, public Singlton<LogLayer>
{
public:
	LogLayer();
	~LogLayer();
	virtual void onCreate()override;

	void show();
	void hide();

	void log( const std::string& msg, Color3B color = Color3B::BLACK );
	
protected:
	void update( float dt );
	void refreshCursor();
private:
	std::vector<LabelPointer> _logs;
	std::vector<LabelPointer> _logsNotSee;
	SpritePointer _cursor;
	float _textPosition;
	float _contentHeight;
};




NS_CC_END
#endif // #ifndef LogLayer