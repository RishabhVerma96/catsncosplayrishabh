//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "ScoreCounter.h"
#include "GameLayer.h"
#include "consts.h"
#include "resources.h"
#include "UserData.h"
#include <ctime>
#include "Log.h"
#include "configuration.h"
#include "ml/Config.h"
#include "flurry/flurry_.h"
#include "RapidJson.h"
#include<iostream>

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "LocalNotification.h"
#endif

NS_CC_BEGIN;

using namespace rapidjson;

ScoreCounter::ScoreCounter( void )
{}

void ScoreCounter::onCreate()
{
	for( unsigned i = 0; i<7; ++i )
	{
		std::string s( kUser_Scores_suffix );
		s += toStr( i );
		int score = UserData::shared().get(s, 0);
		if (score > 0)
			setMoney(i, score, false);
	}
}

ScoreCounter::~ScoreCounter( void )
{}

void ScoreCounter::change( int id, int value, bool saveValueToUserData )
{
	auto i = m_scores.find( id );
	if( i == m_scores.end() )
	{
		i = m_scores.insert( std::pair<unsigned, int>( id, 0 ) ).first;
	}
	assert( i != m_scores.end() );
	i->second += value;
    
	auto& observer = this->observer( id );
	observer.pushevent( i->second );

	if( saveValueToUserData )
	{
		std::string s( kUser_Scores_suffix );
		s += toStr( id );
		UserData::shared().write( s.c_str(), i->second );
	}
}

void ScoreCounter::setMoney( int id, int value, bool saveValueToUserData )
{
	int current = getMoney( id );
	addMoney( id, -current + value, saveValueToUserData );
}

void ScoreCounter::addMoney( int id, int value, bool saveValueToUserData )
{
	change( id, value, saveValueToUserData );
}

void ScoreCounter::subMoney( int id, int value, bool saveValueToUserData, const std::string& wherespent )
{

	change( id, -value, saveValueToUserData );
	if( wherespent.empty() == false && Config::shared().get<bool>( "useStatistic" ) )
	{
		ParamCollection pc;
		switch( id )
		{
			case kScoreCrystals: pc["event"] = "SpentGold"; break;
			case kScoreLevel: pc["event"] = "SpentGears"; break;
			case kScoreTicket: pc["event"] = "SpentTickets"; break;
		}
		if( pc.find( "event" ) != pc.end() )
		{
			pc["where"] = wherespent;
			pc["count"] = toStr( value );
			flurry::logEvent( pc );
		}
	}
}

int ScoreCounter::getMoney( int id )const
{

	auto i = m_scores.find( id );
	if( i != m_scores.end() )
		return i->second;
	else
		return 0;
}

ScoreCounter::Observer_OnChangeScore& ScoreCounter::observer( int id )
{
	return _onChangeScores[id];
}

/***************************************************************/

ScoreByTime::ScoreByTime()
: _timer( 0 )
, _time( 0 )
, _interval( 60 )
, _max( 10 )
, _gameTime(0)
{
	__log_line( "" );
    _isInternetConnected = false;
	ScoreCounter::shared().observer( kScoreTime ).add( _ID, std::bind( &ScoreByTime::changeTime, this, std::placeholders::_1 ) );
}

ScoreByTime::~ScoreByTime()
{
	__log_line( "" );
	savetime();
}

void ScoreByTime::onCreate()
{
	pugi::xml_document doc;
	doc.load_file( "ini/fuel.xml" );
	auto root = doc.root().first_child();
	_max = root.child( "max" ).attribute( "value" ).as_int();
	_interval = root.child( "delay" ).attribute( "value" ).as_int();

	auto currtime = UserData::shared().get<int>( "gametime" );
	_gameTime = currtime;

	Director::getInstance()->getScheduler()->schedule( std::bind( &ScoreByTime::update, this, std::placeholders::_1 ), this, 1, false, "scorebytime" );

	checkMaxValue();
    checktime();
}

void ScoreByTime::setInternetConnected(bool status){
    _isInternetConnected = status;
}

bool ScoreByTime::isInternetConnected(){
    return _isInternetConnected;
}

void ScoreByTime::sendTimeRequest(std::string tag){
    HttpRequest* request = new HttpRequest();
    request->setRequestType(cocos2d::network::HttpRequest::Type::GET);
    request->setUrl("http://api.timezonedb.com/v2/get-time-zone?key=WLWML9YJIMNM&format=json&by=zone&zone=America/Chicago");
    request->setResponseCallback(CC_CALLBACK_2(ScoreByTime::onTimeResponseRecieved, this));
    request->setTag(tag.c_str());
    HttpClient::getInstance()->send(request);
    request->release();
}

void ScoreByTime::onTimeResponseRecieved(HttpClient *sender, HttpResponse *response) {
    const char* tag = response->getHttpRequest()->getTag();
    if (response && response->getResponseCode() == 200 && response->getResponseData()) {
        
        setInternetConnected(true);
        
        std::vector<char>* responseData = response->getResponseData();
        
        rapidjson::Document document;
        
        std::string responseJson(responseData->begin(), responseData->end());
        responseJson = responseJson.insert(0, "{\"root\":");
        responseJson = responseJson.append("}");
       
        document.Parse(responseJson.c_str());

        rapidjson::Value& root = document["root"];
        std::string time = root["formatted"].GetString();
        
        if(strcmp(tag, "GetTime") == 0){
          __log_line( "" );
            auto times = UserData::shared().get("score_timer");

            if (times.empty() == false)
            {
                struct tm last;
                sscanf( times.c_str(), "%d-%d-%d-%d", &last.tm_sec, &last.tm_min, &last.tm_hour, &last.tm_yday );

                struct tm now;
                strptime(time.c_str(), "%Y-%m-%d %T", &now);

                long long last_sec =
                last.tm_sec +
                last.tm_min * 60 +
                last.tm_hour * 60 * 60 +
                last.tm_yday * 60 * 60 * 24;
                long long now_sec =
                now.tm_sec +
                now.tm_min * 60 +
                now.tm_hour * 60 * 60 +
                now.tm_yday * 60 * 60 * 24;
                long long diff = now_sec - last_sec;
                diff = std::max<long long>( diff, 0 );
                __log_line( "" );
                ScoreCounter::shared().subMoney( kScoreTime, diff, true, "" );

                __log_line( "ok0" );
            }
            else
            {
                ScoreCounter::shared().setMoney( kScoreTime, _interval, true );
                ScoreCounter::shared().setMoney( kScoreFuel, _max, true );
            }
            savetime();
        }else{
           
            char buf[128];
            //time_t time = ::time( 0 );
           // struct tm now = *localtime( &time );
            struct tm now;
            strptime(time.c_str(), "%Y-%m-%d %T", &now);
            sprintf( buf, "%d-%d-%d-%d", now.tm_sec, now.tm_min, now.tm_hour, now.tm_yday );
            UserData::shared().write( "score_timer", std::string(buf) );
            UserData::shared().write( "gametime", _gameTime );
            //UserData::shared().save();
        }
    }
    else {
        CCLOG("%s", ("Error " + std::to_string(response->getResponseCode()) + " in request").c_str()); //error
        setInternetConnected(false);
    }
}

void ScoreByTime::checkMaxValue( )
{	
	_max = UserData::shared().get(k::user::MaxFuelValue, _max);
}

void ScoreByTime::checktime()
{
    sendTimeRequest("GetTime");
}

void ScoreByTime::update( float dt )
{
    if(!isInternetConnected()){
        return;
    }
    
    if( ScoreCounter::shared().getMoney( kScoreFuel ) >= _max )
    {
        ScoreCounter::shared().setMoney( kScoreTime, _interval, true );
        savetime();
    }
    _timer += dt;
    _gameTime += dt;
    if( _timer > 1.f )
    {
        _timer -= 1.f;
        ScoreCounter::shared().subMoney( kScoreTime, 1, true, "" );
    }
}

void ScoreByTime::changeTime( int score )
{
    
	if( score <= 0 )
	{
		int addfuel = (-score) / ( _interval == 0 ? 1 : _interval) + 1;
		int addtime = addfuel * _interval;

		if( addtime > 0 )
			ScoreCounter::shared().addMoney( kScoreTime, addtime, true );

		int fuel = ScoreCounter::shared().getMoney( kScoreFuel );
		int nfuel = fuel + addfuel;
		if( nfuel > _max ) addfuel = _max - fuel;
		addfuel = std::max( addfuel, 0 );
        if( addfuel > 0 ) {
			ScoreCounter::shared().addMoney( kScoreFuel, addfuel, true );
        }
		savetime();
	}
	else if( score > 10000 )
	{
		assert(false && "Wait time is too far");
		ScoreCounter::shared().setMoney( kScoreTime, 0, true );
	}
}

void ScoreByTime::savetime()
{
    sendTimeRequest("SaveTime");
}

void LevelParams::onCreate()
{
	_currentBetIndex = 0;
	loadRealParams();
	loadLevelParams();

	const char * key = "award_was_obtained_for_real_default";
	if (UserData::shared().get <int>(key) == 0)
	{
		ScoreCounter::shared().addMoney( kScoreCrystals, _goldOnFirstRun, true );
		UserData::shared().write( key, 1 );
	}
}

void LevelParams::loadRealParams()
{
	pugi::xml_document doc;
	doc.load_file( "ini/realgold.xml" );
	auto root = doc.root().first_child();
	_goldOnFirstRun = root.child( "default" ).attribute( "value" ).as_int();
}

void LevelParams::loadLevelParams()
{
	pugi::xml_document doc;
	doc.load_file( "ini/levels.xml" );
	auto root = doc.root().first_child();

	int i = 0;
	pugi::xml_node node;
	do
	{
		std::string tag = "level_" + toStr( i + 1 );
		node = root.child( tag.c_str() );
		if( !node )
			break;
		_params[i].normal.stars.resize( 3 );
		_params[i].normal.stars[0] = node.attribute( "star1" ).as_int( 1 );
		_params[i].normal.stars[1] = node.attribute( "star2" ).as_int( 1 );
		_params[i].normal.stars[2] = node.attribute( "star3" ).as_int( 1 );
		_params[i].normal.tickets = node.attribute( "tickets" ).as_int( 0 );

		_params[i].hard.award = node.attribute( "starhard" ).as_int( 0 );
		_params[i].hard.stars = node.attribute( "starshard" ).as_int( 0 );
		_params[i].hard.tickets = node.attribute( "ticketshard" ).as_int( 0 );
		_params[i].hard.exclude = node.attribute( "excludetext" ).as_string();
	
		if( Config::shared().get<bool>( "useFuel" ) )
		{
			_params[i].normal.fuel = node.attribute( "fuel" ).as_int( 1 );
			_params[i].hard.fuel = node.attribute( "fuelhard" ).as_int( 1 );
		}
		else
		{
			_params[i].normal.fuel = 0;
			_params[i].hard.fuel = 0;
		}
		_params[i].normal.gold = node.attribute( "gold" ).as_int( 0 );
		_params[i].hard.gold = node.attribute( "gold" ).as_int( 0 );

		parceLevel( i );
	}
	while( ++i );

	auto multi = root.child( "multiplayer_bets" );
	for( auto node : multi )
	{
		MultiplayerBet bet;
		std::string type = node.attribute( "type" ).as_string();
		if( type == "gold" ) bet.type = kScoreCrystals;
		else if( type == "fuel" ) bet.type = kScoreFuel;
		else if( type == "ticket" ) bet.type = kScoreTicket;
		bet.cost = node.attribute( "cost" ).as_int();
		bet.award = node.attribute( "award" ).as_int();
		_multiplayerBet.push_back( bet );
	}
}

void LevelParams::parceLevel(int index)
{
	pugi::xml_document doc;
	doc.load_file( (kDirectoryToMaps + "map" + toStr( index ) + ".xml").c_str() );
	auto root = doc.root().first_child();
	
	auto waveN = root.child( k::xmlTag::LevelWaves );
	auto waveH = root.child( k::xmlTag::LevelWavesHard );
	auto normal = root.child( k::xmlTag::LevelParams );
	auto hard = root.child( k::xmlTag::LevelParamsHard );
	if( !normal ) normal = root;

	_params[index].normal.gear = normal.attribute( "startscore" ).as_int();
	_params[index].hard.gear = hard.attribute( "startscore" ).as_int();
	_params[index].normal.lifes = normal.attribute( "healths" ).as_int();
	_params[index].hard.lifes = hard.attribute( "healths" ).as_int();
	_params[index].normal.exclude = normal.attribute( "exclude" ).as_string();

	_params[index].normal.waves = 0;
	_params[index].hard.waves = 0;
	FOR_EACHXML( waveN, wave ) { _params[index].normal.waves++; }
	FOR_EACHXML( waveH, wave ) { _params[index].hard.waves++; }
}

int LevelParams::getMaxStars( int level, GameMode mode )const
{
	auto iter = _params.find( level );
	if( iter != _params.end() )
	{
		switch( mode )
		{
			case GameMode::normal:
				return iter->second.normal.stars.size();
			case GameMode::hard:
				return iter->second.hard.stars;
			default:
				return 0;
		}
	}
	else
	{
		cocos2d::MessageBox(
			("Sorry, I have not award for level [" + toStr( level ) + "]").c_str(),
			"Error" );
	}
	return 0;
}

int LevelParams::getAwardGold( int levelIndex, int stars, GameMode mode )const
{
	if( mode != GameMode::multiplayer )
	{
		auto iter = _params.find( levelIndex );
		if( iter != _params.end() )
		{
			switch( mode )
			{
				case GameMode::normal:
					return stars > 0 ? iter->second.normal.stars[stars - 1] : 0;
				case GameMode::hard:
					return stars > 0 ? iter->second.hard.award : 0;
				case GameMode::survival:
					return stars > 0 ? iter->second.normal.stars[stars - 1] : 0;
				default:
					return 0;
			}
		}
		else
		{
			cocos2d::MessageBox(
				("Sorry, I have not award for level [" + toStr( levelIndex ) + "]").c_str(),
				"Error" );
		}
	}
	else
	{
		return _multiplayerBet.at( _currentBetIndex ).award;
	}
	return 0;
}

int LevelParams::getTickets( int levelIndex, GameMode mode )const
{
	auto iter = _params.find( levelIndex );
	if( iter != _params.end() )
	{
		switch( mode )
		{
			case GameMode::normal:
				return iter->second.normal.tickets;
			case GameMode::hard:
				return iter->second.hard.tickets;
			default:
				return 0;
		}
	}
	return 0;
}

void LevelParams::getPayForLevel( int levelIndex, GameMode mode, int& scoreType, int& score )
{
	if( mode == GameMode::multiplayer )
	{
		assert( _currentBetIndex < _multiplayerBet.size() );
		auto bet = _multiplayerBet.at( _currentBetIndex );
		scoreType = bet.type;
		score = bet.cost;
	}
	else
	{
		scoreType = -1;
		score = 0;
		int fuel = _getFuel( levelIndex, mode );
		int gold = _getGold( levelIndex, mode );
		if( fuel > 0 )
		{
			scoreType = kScoreFuel;
			score = fuel;
		}
		else if( gold > 0 )
		{
			scoreType = kScoreCrystals;
			score = gold;
		}
	}
}

int LevelParams::_getFuel( int levelIndex, GameMode mode )const
{
	auto iter = _params.find( levelIndex );
	if( iter != _params.end() )
	{
		switch( mode )
		{
			case GameMode::survival:
			case GameMode::normal:
				return iter->second.normal.fuel;
			case GameMode::hard:
				return iter->second.hard.fuel;
			default:
				return 0;
		}
	}
	else
	{
		cocos2d::MessageBox(
			("Sorry, I have not fuel for level [" + toStr( levelIndex ) + "]").c_str(),
			"Error" );
	}
	return 0;
}

int LevelParams::_getGold( int levelIndex, GameMode mode )const
{
	auto iter = _params.find( levelIndex );
	if( iter != _params.end() )
	{
		switch( mode )
		{
			case GameMode::normal:
				return iter->second.normal.gold;
			case GameMode::hard:
				return iter->second.hard.gold;
			default:
				return 0;
		}
	}
	else
	{
		cocos2d::MessageBox(
			("Sorry, I have not gold for level [" + toStr( levelIndex ) + "]").c_str(),
			"Error" );
	}
	return 0;
}

int LevelParams::getStartGear( int level, GameMode mode )const
{
	switch( mode )
	{
		case GameMode::normal:
			return _params.at( level ).normal.gear;
		case GameMode::hard:
			return _params.at( level ).hard.gear;
		default:
			return 0;
	}
}

int LevelParams::getWaveCount( int level, GameMode mode )const
{
	switch( mode )
	{
		case GameMode::normal:
			return _params.at( level ).normal.waves;
		case GameMode::hard:
			return _params.at( level ).hard.waves;
		default:
			return 0;
	}
}

bool LevelParams::isLastLevelWon() const{
    return _isLastLevelWon;
}

void LevelParams::setLastLevelWon(bool isLastLevelWon){
    _isLastLevelWon = isLastLevelWon;
}

int LevelParams::getLives( int level, GameMode mode )const
{
	switch( mode )
	{
		case GameMode::normal:
			return _params.at( level ).normal.lifes;
		case GameMode::hard:
			return _params.at( level ).hard.lifes;
		default:
			return 0;
	}
}

std::string LevelParams::getExclude( int level, GameMode mode )const
{
	switch( mode )
	{
		case GameMode::normal:
			return _params.at( level ).normal.exclude;
		case GameMode::hard:
			return _params.at( level ).hard.exclude;
		default:
			return 0;
	}
}

size_t LevelParams::getMultiplayerCount()const
{
	return _multiplayerBet.size();
}

LevelParams::MultiplayerBet LevelParams::getMultiplayerBet( int index )const
{
	return _multiplayerBet.at( index );
}

void LevelParams::setPayForMultiplayer( int indexBet )
{
	_currentBetIndex = indexBet;
}

void LevelParams::onLevelStarted( int levelIndex, GameMode mode )
{
	int type( -1 );
	int score( 0 );
	getPayForLevel( levelIndex, mode, type, score );
	if( type != -1 )
	{
		ScoreCounter::shared().subMoney( type, score, true, "level_pay" );
        if(type == kScoreFuel) {
            int remainingFuel = ScoreByTime::shared().max_fuel() - ScoreCounter::shared().getMoney(kScoreFuel);
            int interval = ScoreCounter::shared().getMoney(kScoreTime) + (remainingFuel - 1) * 15.0f * 60.0f;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            LocalNotification::cancel(1);
            LocalNotification::show("Battery fully charged!", interval, 1);
#endif
        }
	}
	UserData::shared().save();
}

void LevelParams::onLevelFinished( int index, int stars, GameMode mode )
{
	if( mode == GameMode::multiplayer )
	{
		assert( _currentBetIndex < _multiplayerBet.size() );
		auto bet = _multiplayerBet.at( _currentBetIndex );
		if( stars> 0 )
			ScoreCounter::shared().addMoney( bet.type, bet.award, true );
	}
	else
	{
		int crystals = getAwardGold( index, stars, mode );
		ScoreCounter::shared().addMoney( kScoreCrystals, crystals, true );

		int tickets = getTickets( index, mode );
		ScoreCounter::shared().addMoney( kScoreTicket, tickets, true );

		if( stars > 0 )
		{
			if( mode == GameMode::hard )
				stars = getMaxStars( index, mode ) + getMaxStars( index, mode );

			int obtained = UserData::shared().get <int>( k::user::LevelStars + toStr( index ) );
			int diff = std::max( 0, stars - obtained );
			ScoreCounter::shared().addMoney( kScoreStars, diff, true );
			int all = diff;
			all += UserData::shared().get <int>( k::user::LevelStars + toStr( index ) );
			UserData::shared().write( k::user::LevelStars + toStr( index ), all );
		}
	}
	UserData::shared().save();
}

void LevelParams::onLevelFinishedSurvival( int levelIndex, int wavesComplete, GameMode mode )
{
	int score = getAwardGold( levelIndex, 1, mode );
	score *= wavesComplete;
	ScoreCounter::shared().addMoney( kScoreCrystals, score, true );
}


NS_CC_END;
