//
//  IslandDefense
//
//  Created by Vladimir Tolmachev‚ on 03.11.14.
//
//
#ifndef __MenuItemCooldown_h__
#define __MenuItemCooldown_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/MenuItem.h"
NS_CC_BEGIN





class MenuItemCooldown : public mlMenuItem
{
	DECLARE_BUILDER( MenuItemCooldown );
public:
	bool init( const std::string & back, const std::string & forward, float duration, const ccMenuCallback & callback, const std::string & resourceCancel );
	virtual bool setProperty( const std::string & stringproperty, const std::string & value )override;

	void run();
	void stop();
	void pauseItem();
	void resumeItem();
	void showCancel( bool mode );
	void setAnimationOnFull( const std::string & animationFrame );
	void setPercentage( float percent );//0..1000

	bool isActive()const { return _active; }
	virtual void selected();
	virtual void unselected();

	bool isRunning()const { return _running; }
protected:
	virtual void on_click( Ref*sender );
	virtual void onFull(  );
private:
	ProgressTimerPointer _timer;
	IntrusivePtr< FiniteTimeAction > _action;
	float _duration;
	bool _active;
	SpritePointer _cancelImage;
	std::string _cancelImageResource;
	std::string _animationFrame;
	bool _running;
	IntrusivePtr< FiniteTimeAction > getMenuAction(float startingPercentage);
	
};




NS_CC_END
#endif // #ifndef MenuItemCooldown
