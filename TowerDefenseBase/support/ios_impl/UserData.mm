//
//  UserData.m
//  FantasyDefense
//
//  Created by �������� �������� on 04.04.16.
//
//

#if CLOUDSYNC == 1

#import <Foundation/Foundation.h>
#import "AppController.h"
#include "support/UserData.h"
#include "ScoreCounter.h"

NS_CC_BEGIN;
void UserData::cloudInit()
{
	if( [[AppController getInstance] iCloudAccountIsSigned] )
		[AppController getInstance].iCloudNotify = std::bind<void(UserData::*)(const std::map<std::string, std::string>&)>(&UserData::syncFromCloud, this, std::placeholders::_1);
}

void UserData::cloudSync()
{
	int useiCould = get<int>( "icloud_sync", 0 );
	if( useiCould == 1 )
		return;

	AppController* app = [AppController getInstance];
	if( [app iCloudAccountIsSigned] )
	{
		auto root = getXmlRoot();
		for( auto node : root )
		{
			auto k = node.name();
			auto v = node.attribute( "value" ).as_string( "" );
			[app setICloudValue:v forKey:k];
		}
		[app syncICloud];
	}
}

void UserData::syncFromCloud( const std::string& values ) {
	assert(0);
}

void UserData::syncFromCloud( const std::map<std::string, std::string>& values )
{
	_cloudData = values;
	if( _cloudData.empty() == false )
		onRecieveCloudData();
}

void UserData::onRecieveCloudData()
{
	auto sync = [&](bool use)
	{
		if( use )
		{
			write( "icloud_sync", 2 );
			for( auto pair : _cloudData )
				write_string(pair.first, pair.second);
			ScoreCounter::shared().onCreate();
			save();
		}
		else
		{
			write( "icloud_sync", 1 );
		}
	};
	
	AppController* app = [AppController getInstance];
	app.UIAlertNotify = std::bind( sync, std::placeholders::_1);

	int useiCould = get<int>( "icloud_sync", 0 );
	if( useiCould == 0 )
	{
		UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle: @"Choose iCloud Option"
							  message: @"Do you want use game progress from iCloud Drive?"
							  delegate: app
							  cancelButtonTitle: @"No"
							  otherButtonTitles: @"Yes", nil];
		alert.tag = 0x101;
		[alert show];
	}
	else
	{
		sync( useiCould==2 );
	}
}

NS_CC_END;

#endif