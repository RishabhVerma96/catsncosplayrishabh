#include "support.h"

#import "ios_impl/Reachability.h"

bool ::cocos2d::requestInternetConnectionStatus()
{
    Reachability * reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    return status != NotReachable;
}