//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "ml/ImageManager.h"
#include "ml/Config.h"
#include "support.h"
#include "consts.h"
#include "GameLayer.h"
#include "UserData.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#	include <jni.h>
#	include "platform/android/jni/JniHelper.h"
#endif

NS_CC_BEGIN;

namespace
{
	SpritePointer SpriteForRadius;
};


void showRadius( Node* parent, const cocos2d::Point & position, float radius )
{
	if( !SpriteForRadius )
	{
		SpriteForRadius.reset( ImageManager::sprite( Config::shared().get("resourceGameSceneFolder") + "circle.png" ) );
	}
	float scalex = radius / (SpriteForRadius->getContentSize( ).width / 2);
	float scaley = radius / (SpriteForRadius->getContentSize( ).height / 2) / Config::shared().get<float>("isometricValue");
	SpriteForRadius->setScale( scalex, scaley);
	SpriteForRadius->setPosition( position );

 
	if( SpriteForRadius->getParent( ) )
		SpriteForRadius->removeFromParent();
	parent->addChild( SpriteForRadius, zorder::sky );
}

void hideRadius( Node* parent )
{
	parent->removeChild( SpriteForRadius );
}

bool radiusTowerIsHidden()
{
	return SpriteForRadius->getParent() == nullptr;
}

bool checkRadiusByEllipse( const Point & a, const Point & b, float radius )
{
	static float isometric = Config::shared().get<float>( "isometricValue" );
	float x = fabs( a.x - b.x );
	float y = fabs( a.y - b.y ) * isometric;
	float d = ( x*x + y*y );
	return d <= radius * radius;
	return true;
}

const Route & getNearestRouteWithSegment( size_t & segment, const std::vector<TripleRoute>& routes, const cocos2d::Point & basePoint, UnitLayer::type allowLayer, bool forHero )
{
	float distance_min( ( float )2E+36 );
	int index = -1;
	int min_index = -1;
	for( const auto& route : routes )
	{
		index++;
		if( (forHero == false) && route.onlyForHero ) continue;
		if( route.type != allowLayer && allowLayer != UnitLayer::any )
			continue;
		const Route & road = route.main;
		for( size_t i = 1; i < road.size(); ++i )
		{
			const Point p0 = road[i - 1];
			const Point p1 = road[i];
			Point point = nearestPointOnLineSegment( p0, p1, basePoint );
			float dist = point.getDistance( basePoint );
			if( dist < distance_min && point != Point::ZERO )
			{
				min_index = index;
				distance_min = dist;
				segment = i;
			}
		}
	}
	return routes[min_index].main;
}

Point getNearestPointInRoute( const std::vector<TripleRoute>& routes, const cocos2d::Point & basePoint, UnitLayer::type allowLayer, bool forHero )
{
	Point result = Point::ZERO;
	float distance_min( ( float )2E+36 );
	int index = -1;
	int min_index = -1;
	for( const auto& route : routes )
	{
		if( ( forHero == false ) && route.onlyForHero ) continue;
		if( route.type != allowLayer && allowLayer != UnitLayer::any )
			continue;
		const Route & road = route.main;
		for( size_t i = 1; i < road.size(); ++i )
		{
			const Point p0 = road[i - 1];
			const Point p1 = road[i];
			Point point = nearestPointOnLineSegment( p0, p1, basePoint );
			float dist = point.getDistance( basePoint );
			if( dist < distance_min && point != Point::ZERO)
			{
				distance_min = dist;
				result = point;
			}
		}
	}
	return result;
}

bool checkPointOnRoute( const std::vector<TripleRoute>& routes, const cocos2d::Point & point, float maxDistanceToRoad, UnitLayer::type allowLayer, float * distance )
{
	for( const auto& route : routes )
	{
		if( (route.type == allowLayer || allowLayer == UnitLayer::any) &&
			checkPointOnRoute( point, route, maxDistanceToRoad, distance ) )
			return true;
	}
	return false;
}

bool checkPointOnRoute( const cocos2d::Point & point, const TripleRoute & route, float maxDistanceToRoad, float * distance )
{
	size_t index_min( -1 );
	float distance_min( (float)2E+36 );

	const Route & road = route.main;
	for( size_t i = 1; i < road.size( ); ++i )
	{
		const Point p0 = road[i - 1];
		const Point p1 = road[i];
		float dist = distanse_pointToLineSegment( p0, p1, point );
		if( dist < distance_min )
		{
			distance_min = dist;
			index_min = i;
		}
	}
	if( distance )
		*distance = distance_min;
	return distance_min < maxDistanceToRoad;
};

int getElapsedTimeFromPreviosLaunch( const std::string & timeId )
{
	int elapsed( 0 );
	auto times = UserData::shared().get(timeId);
	if( times.empty( ) == false )
	{
		struct tm last;
		sscanf( times.c_str( ), "%d-%d-%d-%d", &last.tm_sec, &last.tm_min, &last.tm_hour, &last.tm_yday );
		time_t time = ::time( 0 );
		struct tm now = *localtime( &time );
		long long last_sec =
			last.tm_sec +
			last.tm_min * 60 +
			last.tm_hour * 60 * 60 +
			last.tm_yday * 60 * 60 * 24;
		long long now_sec =
			now.tm_sec +
			now.tm_min * 60 +
			now.tm_hour * 60 * 60 +
			now.tm_yday * 60 * 60 * 24;
		long long diff = now_sec - last_sec;
		elapsed = static_cast<int>(diff);
	}
	return elapsed;
}


UnitLayer::type strToUnitLayer( const std::string & value )
{
	if( value == "earth" )return UnitLayer::earth;
	if( value == "sky" )return UnitLayer::sky;
	if( value == "sea" )return UnitLayer::sea;
	if( value == "any" )return UnitLayer::any;
	return UnitLayer::earth;
}

std::string unitLayerToStr( UnitLayer::type unitLayer )
{
	switch( unitLayer )
	{
		case UnitLayer::earth: return "earth";
		case UnitLayer::sky: return "sky";
		case UnitLayer::sea: return "sea";
		case UnitLayer::any: return "any";
	}
	assert( 0 );
	return "";
}

UnitType strToUnitType( const std::string & value )
{
	if( value == "creep" ) return UnitType::creep;
	if( value == "tower" ) return UnitType::tower;
	if( value == "desant" ) return UnitType::desant;
	if( value == "hero" ) return UnitType::hero;
	if( value == "airbomb" ) return UnitType::airbomb;
	if( value == "unstoppable" ) return UnitType::unstoppable;
	if( value == "skill" ) return UnitType::skill;
	if( value == "other" ) return UnitType::other;
	return UnitType::none;
}

std::string unitTypeToStr( UnitType unittype )
{
	switch( unittype )
	{
		case UnitType::creep: return "creep";
		case UnitType::tower: return "tower";
		case UnitType::desant: return "desant";
		case UnitType::hero: return "hero";
		case UnitType::unstoppable: return "unstoppable";
		case UnitType::skill: return "skill";
		case UnitType::other: return "other";
		case UnitType::none: return "none";
	}
	assert( 0 );
	return "";
}


RouteSubType strToRouteSubType( const std::string & value )
{
	if( value == "random" )return RouteSubType::random;
	if( value == "-1" )return RouteSubType::random;
	if( value == "main" )return RouteSubType::main;
	if( value == "left" )return RouteSubType::left0;
	if( value == "right" )return RouteSubType::right0;
	return RouteSubType::random;
}

BodyType strToBodyType( const std::string & value )
{
	if( value == "equipment" )return BodyType::equipment;
	if( value == "meat" )return BodyType::meat;
	return BodyType::defaultvalue;
}

Point ScrollTouchInfo::fitPosition( const Point & position, const Size & winsize )
{
	assert( node );
	Point pos = position;
	if( node )
	{
		float scale = 1.f;
		Node*root = node;
		do
		{
			scale *= std::fabs( root->getScaleX() );
			root = root->getParent();
		}
		while( root );

		auto size = node->getContentSize();
		size = size * scale;

		float H = (std::fabs( winsize.width ) - std::fabs( size.width) ) * (std::fabs( node->getScaleX() ) / scale);
		float V = (std::fabs( winsize.height ) - std::fabs( size.height) ) * (std::fabs( node->getScaleX() ) / scale);

		pos.x = std::min<float>( pos.x, 0 );
		pos.x = std::max<float>( pos.x, H );
		pos.y = std::min<float>( pos.y, 0 );
		pos.y = std::max<float>( pos.y, V );

		if( winsize.height > size.height )
			pos.y = V / 2;
		if( winsize.width > size.width )
			pos.y = H / 2;
	}
	return pos;
}

MouseHoverScroll::MouseHoverScroll()
	: _scrollMouseHoverX(0)
	, _scrollMouseHoverY(0)
	, _scroller(nullptr)
	, _winSize()
	, _touchListener(nullptr)
	, _node(nullptr)
	, _velocity(300.f)
	, _border(100.f)
	, _enabled(true)
{
	_winSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	_touchListener = EventListenerMouse::create();
	_touchListener->retain();
	_touchListener->onMouseMove = std::bind( &MouseHoverScroll::mouseHover, this, std::placeholders::_1 );

	_touchListener->onMouseDown = std::bind( &MouseHoverScroll::disable, this );
	_touchListener->onMouseUp = std::bind( &MouseHoverScroll::enable, this );

	auto eventDispatcher = Director::getInstance()->getEventDispatcher();
	eventDispatcher->addEventListenerWithFixedPriority( _touchListener, -9999 );
}

MouseHoverScroll::~MouseHoverScroll()
{
	auto eventDispatcher = Director::getInstance()->getEventDispatcher();
	eventDispatcher->removeEventListener( _touchListener );
	CC_SAFE_RELEASE_NULL( _touchListener );
}

void MouseHoverScroll::update(float dt)
{
	if( !_node || !_scroller || !_enabled )
		return;
	Point pos = _node->getPosition();
	pos.x += _scrollMouseHoverX * dt * _velocity;
	pos.y += _scrollMouseHoverY * dt * _velocity;
	_scroller->node = _node;
	pos = _scroller->fitPosition( pos, _winSize );
	_node->setPosition(pos);
}

void MouseHoverScroll::enable()
{
	_enabled = true;
}

void MouseHoverScroll::disable()
{
	_enabled = false;
}

void MouseHoverScroll::disable_schedule( float )
{
}

void MouseHoverScroll::mouseHover(Event*event)
{
	EventMouse* em = dynamic_cast<EventMouse*>(event);
	assert(em);
	if( !em ) 
		return;

	float border = _border;
	float sx(0.f), sy(0.f);

	if (em->getCursorX() < border)
		sx = +std::fabs( 1 - em->getCursorX() / border);
	else if( em->getCursorX() > (_winSize.width - border) )
		sx = -std::fabs( 1 - (_winSize.width - em->getCursorX()) / border );

	if (em->getCursorY() < border)
		sy = +std::fabs(1 - em->getCursorY() / border);
	else if( em->getCursorY() > (_winSize.height - border) )
		sy = -std::fabs( 1 - (_winSize.height - em->getCursorY()) / border );

	_scrollMouseHoverX = sx;
	_scrollMouseHoverY = sy;

	_scrollMouseHoverX = std::min(_scrollMouseHoverX, _velocity);
	_scrollMouseHoverY = std::min(_scrollMouseHoverY, _velocity);
}

class DebugIDs : public Singlton<DebugIDs>
{
public:
	DebugIDs()
	: _deviceID("__noid1234435641asdfggesrnoid__")
	, _testModeActive( false )
	{
	}
public:
	std::string _deviceID;
	std::set<std::string> _testDevices;
	bool _testModeActive;
};

void setDeviceID( const std::string& deviceID )
{
	DebugIDs::shared()._deviceID = deviceID;
}

std::string getDeviceID()
{
	return DebugIDs::shared()._deviceID;
}

void addTestDevice( const std::string& deviceID )
{
	DebugIDs::shared()._testDevices.insert( deviceID );
}

bool isTestDevice()
{
	return
		Config::shared().get<bool>("allowAllDevicesToTest") ||
		( !DebugIDs::shared()._deviceID.empty() && DebugIDs::shared()._testDevices.find( getDeviceID() ) != DebugIDs::shared()._testDevices.end() );
}

void setTestModeActive( bool active )
{
	Director::getInstance()->setDisplayStats( active );
	DebugIDs::shared()._testModeActive = active;
}

bool isTestModeActive()
{
	return DebugIDs::shared()._testModeActive;
}

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#define JAVA(method)JNIEXPORT void JNICALL  Java_org_cocos2dx_cpp_AppActivity_##method(JNIEnv*  env, jobject thiz, jstring arg0)
extern
"C"
{

	JAVA(nativeSetPhoneID) {
		std::string str = JniHelper::jstring2string(arg0);
		DebugIDs::shared()._deviceID = str;
	}

}
#undef JAVA
#endif


#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include "ml/JniBind.h"

bool requestInternetConnectionStatus()
{
	JavaBind bind("org.cocos2dx.cpp", "AppActivity", "hasInternetConnection", "");
	return bind.bool_call();
}

#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS

// Implemented in InternetConnection.mm

#else
bool requestInternetConnectionStatus() { return true; }

#endif

void showCursor(bool isFullscreenMode)
{
#if PC == 1
	auto director = Director::getInstance();
	float scale =
		director->getOpenGLView()->getDesignResolutionSize().width /
		director->getOpenGLView()->getFrameSize().width;

	auto glview = static_cast<GLViewImpl*>(director->getOpenGLView());
	glview->setCursorVisible( false );


	if( isFullscreenMode )
	{
		HWND hwnd = glview->getWin32Window();
		RECT rect;
		GetWindowRect( hwnd, &rect );
		ClipCursor( &rect );
	}

	auto sprite = ImageManager::shared().sprite( "images/cursor.png" );
	sprite->retain();
	sprite->setLocalZOrder( 99999 );
	sprite->setAnchorPoint( Point( 0, 1 ) );
	sprite->setScale( scale );
	EventDispatcher * _eventDispatcher = director->getEventDispatcher();

	auto touchBegan = []( Touch*, Event* ){ return true; };
	auto touchMoved = [sprite, director]( Event*event )mutable
	{
		auto scene = sprite->getScene();
		auto run = Director::getInstance()->getRunningScene();
		if( scene != run && run )
		{
			sprite->removeFromParent();
			run->addChild( sprite );
		}
		EventMouse* em = (EventMouse*)event;
		auto pos = Point( em->getCursorX(), em->getCursorY() );
		sprite->setPosition( pos );
	};
	auto touchEnded = []( Touch*, Event* ){; };

	auto touchListener = EventListenerMouse::create();
	touchListener->onMouseMove = std::bind( touchMoved, std::placeholders::_1 );
	_eventDispatcher->addEventListenerWithFixedPriority( touchListener, -9999 );

	auto callback = std::bind( [glview, sprite]( float dt )
	{
		HWND hwnd = glview->getWin32Window();
		POINT point;
		RECT rect;
		GetCursorPos( &point );
		GetWindowRect( hwnd, &rect );
		if( PtInRect( &rect, point ) ) return;

		float scaleX = glview->getScaleX();
		float scaleY = glview->getScaleY();

		ScreenToClient( hwnd, &point );
		float cursorX = point.x / scaleX;
		float cursorY = (glview->getFrameSize().height - point.y) / scaleY;
		cursorX = std::max( cursorX, 0.f );
		cursorY = std::max( cursorY, 0.f );
		cursorX = std::min( cursorX, glview->getDesignResolutionSize().width );
		cursorY = std::min( cursorY, glview->getDesignResolutionSize().height );

		EventMouse *em = new EventMouse( EventMouse::MouseEventType::MOUSE_MOVE );
		em->setCursorPosition( cursorX, cursorY );
		Director::getInstance()->getEventDispatcher()->dispatchEvent( em );
	}, std::placeholders::_1 );
	director->getScheduler()->schedule( callback, director, 0, false, "mouseleave" );

#endif
}

NS_CC_END;
