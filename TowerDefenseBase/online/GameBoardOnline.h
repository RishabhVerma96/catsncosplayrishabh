#ifndef __GameBoardOnline_h__
#define __GameBoardOnline_h__
#include "cocos2d.h"
#include "GameBoard.h"
#include "json/document.h"
#include "onlineutils.h"
NS_CC_BEGIN

enum class GameBoardType
{
	local,
	opponent,
};

class GameBoardOnline : public GameBoard
{
public:
	GameBoardOnline( GameBoardType type );
	~GameBoardOnline();

	virtual bool checkGameFinished()override;
	virtual void createHeroes()override;
	virtual void moveHero( Hero* hero, const Point& position )override;
	virtual void onLoadFinished()override;

	void startSync( bool gameWithBot );
	static void stopSync();
	void loadBot();
protected:
	void recvState( float dt );
	void sendState( float dt );
	void botUpdate( float dt );
	void recieveLoadBot( const std::string& jsonBot );
	void applyState( const GameBoardState& state );
	void forceFinish();
	void requestOpponentInfo();

	virtual void addLevelScore( int score )override;
	virtual void subHealth( int score )override;
	virtual void setStartScore( int score )override;
	virtual void setStartHealth( int score )override;
	virtual bool updateUnitSkills( Unit* unit, float dt )override;
	virtual bool isDeathUnit( Unit* unit )override;
	virtual int getScoreForUnit( Unit::Pointer unit )override;
	virtual void writeGameScore( int score )override;
	virtual void onPredelayWave( const WaveInfo & wave, float delay );

	virtual void onConnectionChanged( bool connection );
	virtual void online_onGameStarted()override;
	virtual void online_onGameFinished( bool victory )override;
	virtual void online_onWaveWasStarted()override;
	virtual void online_onCreepWasCreated( Unit* unit )override;
	virtual void online_onTowerWasCreated( Unit* unit, TowerPlace* place )override;
	virtual void online_onTowerWasUpgraded( Unit* unit )override;
	virtual void online_onTowerWasSelled( Unit* unit )override;
	virtual void online_onSkillWasUsed( Unit* tower, const Point& position )override;
	virtual void online_onBonusItemWasUsed( const std::string& name, const Point& position )override;

protected:
	void syncTowers( const std::vector<GameBoardState::Tower>& towers );
	void syncCreeps( const std::vector<GameBoardState::Creep>& creeps );
	void syncHeroes( const std::vector<GameBoardState::Hero>& heroes );
	void syncBonusItems( const std::vector<GameBoardState::UnitItem>& items );
	void syncActiveSkills( const std::vector<GameBoardState::UnitItem>& items );
	void syncCreepsHealth( const std::vector<GameBoardState::Creep>& creeps );
	void syncCreepsPosition( const std::vector<GameBoardState::Creep>& creeps );

	Unit::Pointer getTower( int id );
	Unit::Pointer getCreep( int id );
	Hero::Pointer getHero( int id );
private:
	CC_SYNTHESIZE_READONLY( GameBoardType, _type, Type );
	std::vector<Unit::Pointer> _towers;
	std::vector<Unit::Pointer> _creeps;
	std::vector<Hero::Pointer> _heroes;
	std::map<std::string, bool> _heroSend;

	std::vector<GameBoardState::UnitItem> _bonusItems;
	std::vector<GameBoardState::UnitItem> _activeSkills;
	GameBoardState::State _gameStateResult;
	float _timer;
	float _disconnectTime;
	float _disconnectTimer;
	bool _connected;
	CC_SYNTHESIZE( int, _sessionId, SessionID );

	struct Bot
	{
        bool active = false;
		RapidJsonNode json;
        int currentAction = 0;
	}_bot;

	class Sync
	{
	public:
		typedef std::function<void( GameBoardOnline *, float )> GameBoardOnlineMethod;
		Sync( std::string const & id, GameBoardOnlineMethod callback ) : _id( id ), _callback( callback ), _delegate(nullptr) {}

		void start( GameBoardOnline * _delegate );
		void stop();

	private:
		std::string const _id;
		GameBoardOnlineMethod _callback;
		GameBoardOnline * _delegate;
	};

	static Sync _localSync;
	static Sync _opponentSync;
};

NS_CC_END
#endif // #ifndef GameBoardOnline