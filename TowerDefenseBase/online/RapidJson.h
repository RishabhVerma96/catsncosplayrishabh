#ifndef __RapidJson_h__
#define __RapidJson_h__
#include "cocos2d.h"
#include "json/document.h"
NS_CC_BEGIN;

class RapidJsonNode
{
public:
	RapidJsonNode();
	RapidJsonNode( const std::string& jsonString );
	RapidJsonNode( const RapidJsonNode* );
	~RapidJsonNode();
	RapidJsonNode& operator=(const RapidJsonNode&);
private:
	void __set( rapidjson::Value* value );
	RapidJsonNode __append( const std::string& key, rapidjson::Type type );
public:
	void parse( const std::string& jsonString );
	void toString( std::string& jsonString )const;
	bool saveFile( const std::string& file )const;
	bool loadFile( const std::string& file );

	RapidJsonNode append_node( const std::string& key );
	RapidJsonNode append_array( const std::string& key );
	RapidJsonNode push_back();

	RapidJsonNode node( const std::string& key );
	RapidJsonNode node( size_t index );
	RapidJsonNode node( const std::string& key )const;
	RapidJsonNode node( size_t index )const;

	template< class T> bool set( T value );

	template< class T> T get( const std::string& key)const;
	template< class T> T get( size_t index )const;
	template< class T> T operator[]( const std::string& key )const { return get<T>( key ); }
	template< class T> T operator[]( size_t index )const { return get<T>( index ); }

	bool contain( const std::string & key ) const;

	operator bool()const;
	bool operator !()const;
	bool operator == (const RapidJsonNode& node)const;

	size_t size()const;
	RapidJsonNode at( size_t index )const;
private:
	std::shared_ptr<rapidjson::Document> _doc;
	RapidJsonNode const* _rootNode;
	rapidjson::Value *_value;
};

NS_CC_END;
#endif
