#ifndef __OnlineConnector_h__
#define __OnlineConnector_h__
#include "cocos2d.h"
#include "ml/Singlton.h"
#include "ml/Observer.h"
#include "RapidJson.h"
#include "network/HttpClient.h"

NS_CC_BEGIN

enum class SearchOpponentState
{
	wait,
	ready,
	cancel,
};

enum class FinishAction
{
	restart,
	abort,
	wait,
};

class OnlineConnector : public Singlton<OnlineConnector>
{
	friend class Singlton<OnlineConnector>;
	OnlineConnector();
	virtual void onCreate()override;
	typedef std::function< void( const std::string& body, float ) > ResponseCallback;
public:
	struct PlayerInfo
	{
		std::string nickname;
		int score = 0;
		int id = -1;
	};

public:
	ObServer<OnlineConnector, std::function<void( bool )> > onConnectionChanged; //result, playerId
	ObServer<OnlineConnector, std::function<void( bool, int )> > onLogged; //result, playerId
	ObServer<OnlineConnector, std::function<void( bool, float, SearchOpponentState )> > onSearhOpponent; //result, timeElapsed, state
	ObServer<OnlineConnector, std::function<void( bool, const RapidJsonNode& )> > onGameSession; //result, response json
	ObServer<OnlineConnector, std::function<void( bool, const RapidJsonNode& )> > onStatistic; //result, response json
	ObServer<OnlineConnector, std::function<void( bool, PlayerInfo const & )> > onStatisticLocal; //result, local info
	ObServer<OnlineConnector, std::function<void( bool, PlayerInfo const & )> > onStatisticOpponent; //result, opponent info
	ObServer<OnlineConnector, std::function<void( bool, int )> > onLoadingReport; //result, opponent loading percent
	ObServer<OnlineConnector, std::function<void( bool, FinishAction )> > onOpponentFinishAction; //result, opponent action
	ObServer<OnlineConnector, std::function<void( bool, bool )> > onGameFinish; //result, win
	ObServer<OnlineConnector, std::function<void( const std::string& )> > onRequestFile; //file body
public:
	bool isConnected() { return _connected != Disconnected; }
    bool checkUserInfo();
	void login();
	void searchOpponent( SearchOpponentState state );
	void statistic( int count, int firstPosition, int playerId = -1 );
	void statisticLocal();
	void statisticOpponent();
	void loadingReport( int percent );
	void winReport( bool win, bool botMode );
	void requestFile( const std::string& path, const std::string& name = "" ); //any file from directory if name is empty

public:
	void startReportingGameState( std::string & act ) { _gameStateReporter.start( act ); }
	void stopReportingGameState() { _gameStateReporter.stop(); }
	void gameState( const std::string& state );

	void startReportingGameFinish( FinishAction & act ) { _gameFinishReporter.start( act ); }
	void stopReportingGameFinish() { _gameFinishReporter.stop(); }
	void gameFinish( FinishAction const & act );

public:
	int getPlayerId()const { return _playerInfo.id; }

	std::string getPlayerNickname()const { return _playerInfo.nickname; }
	int getPlayerScore()const { return _playerInfo.score; }
	std::string getOpponentNickname()const { return _opponentInfo.nickname; }
	int getOpponentScore()const { return _opponentInfo.score; }
	int getGameBet()const { return _gameBet; }
	void setGameBet( int bet ) { _gameBet = bet; }

	void setOpponentName( const std::string& name ) { _opponentInfo.nickname = name; }
	void setOpponentScore( int score ) { _opponentInfo.score = score; }
protected:
	bool checkPlayerData();
	void request( const std::string& url, const std::string& data, ResponseCallback callback );
	void response( ResponseCallback callback, network::HttpClient * client, network::HttpResponse * response );
	void checkConnection( network::HttpResponse * response );
	void responseLogin( const std::string& body, float lagTime );
	void responseSearchOpponent( const std::string& body, float lagTime );
	void responseGameState( const std::string& body, float lagTime );
	void responseStatistic( const std::string& body, float lagTime, int id );
	void responseLoadingReport( const std::string& body, float lagTime );
	void responseGameFinish( const std::string& body, float lagTime );
	void responseRequestFile( const std::string& body );
private:
	std::string _unicalId;
	enum { NotSet, Connected, Disconnected, } _connected;
	int _gameId;
	clock_t _requestTime;

	PlayerInfo _opponentInfo, _playerInfo;
	int _gameBet = 50;

private:
	template<class Reportable>
	class Reporter
	{
	public:
		typedef std::function<void( Reportable const & )> RequestCallback;
		typedef std::function<void( std::string const & )> ResponseCallback;

	public:
		Reporter( RequestCallback request, ResponseCallback response ) : _reportable( nullptr ), _request( request ), _response( response ), _inProgress( false ) {}
		void start( Reportable & reportable ) { _reportable = &reportable; request(); }
		void stop() { _reportable = nullptr; }

		void request()
		{
			if( _reportable && !_inProgress )
				_request( *_reportable );
		}
		void response( std::string const & body, float lagTime )
		{
			_inProgress = false;
			_response( body );
			request();
		}

	private:
		Reportable * _reportable;
		RequestCallback _request;
		ResponseCallback _response;
		bool _inProgress;
	};

	Reporter<FinishAction> _gameFinishReporter;
	Reporter<std::string> _gameStateReporter;
};

NS_CC_END
#endif // #ifndef OnlineConnector