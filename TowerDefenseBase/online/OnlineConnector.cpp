#include "OnlineConnector.h"
#include "ml/Config.h"
#include "playservices/playservices.h"
#include "support.h"
#include "RapidJson.h"
#include "support/LogLayer.h"
NS_CC_BEGIN

/*

OnlineConnector::~OnlineConnector( )
{
}
*/

namespace
{
	std::string const serverLoginPath = "/login.php";
	std::string const serverFindOpponentPath = "/findopponent.php";
	std::string const serverGameState = "/synchronize.php";
	std::string const serverStatistic = "/statistic.php";
	std::string const serverLoadingReport = "/loadingreport.php";
	std::string const serverGameFinish = "/gamefinish.php";
	std::string const serverRequestFile = "/file.php";
};

struct PostString : public std::string
{
	PostString& set( const std::string& id, const std::string& value )
	{
		auto prefix = empty() ? "" : "&";
		(*this) += prefix + id + "=" + value;
		return *this;
	};
};

OnlineConnector::OnlineConnector()
	: _connected( NotSet )
	, _gameId( 0 )
	, _requestTime( 0 )
	, _gameFinishReporter( std::bind( &OnlineConnector::gameFinish, this, std::placeholders::_1 ), std::bind( &OnlineConnector::responseGameFinish, this, std::placeholders::_1, 0.0f ) )
	, _gameStateReporter( std::bind( &OnlineConnector::gameState, this, std::placeholders::_1 ), std::bind( &OnlineConnector::responseGameState, this, std::placeholders::_1, 0.0f ) )
{
}

void OnlineConnector::onCreate() {
	//Config::shared().set( "multiplayerServerURL", "127.0.0.1" );

}

bool OnlineConnector::checkUserInfo()
{
	if( _unicalId.empty() == false )
		return true;

	_unicalId = PlayServises::getUserId();
	if( _unicalId.empty() == false )
    {
        _playerInfo.nickname = PlayServises::getUserDisplayName();
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
        _unicalId += toStr<int>( clock() );
        _playerInfo.nickname += "(" + toStr<int>( clock() ) + ")";
#endif
        log( "%s", _unicalId.c_str() );
    }
    return _unicalId.empty() == false;
}

void OnlineConnector::login()
{
	auto url = Config::shared().get( "multiplayerServerURL" ) + serverLoginPath;
	auto post = PostString()
		.set( "client_id", _unicalId )
		.set( "nickname", _playerInfo.nickname );
	auto callback = std::bind( &OnlineConnector::responseLogin, this, std::placeholders::_1, std::placeholders::_2 );
	request( url, post, callback );
}

void OnlineConnector::searchOpponent( SearchOpponentState state )
{
	auto url = Config::shared().get( "multiplayerServerURL" ) + serverFindOpponentPath;
	auto post = PostString()
		.set( "id", toStr( _playerInfo.id ) )
		.set( "bet", toStr( _gameBet ) );
	if( state == SearchOpponentState::wait ) post.set( "state", "wait" );
	if( state == SearchOpponentState::ready ) post.set( "state", "ready" );
	if( state == SearchOpponentState::cancel ) post.set( "state", "break" );

	auto callback = std::bind( &OnlineConnector::responseSearchOpponent, this, std::placeholders::_1, std::placeholders::_2 );
	request( url, post, callback );
}

void OnlineConnector::gameState( const std::string& state )
{
	auto url = Config::shared().get( "multiplayerServerURL" ) + serverGameState;
	auto post = PostString()
		.set( "id", toStr( _playerInfo.id ) )
		.set( "game_id", toStr( _gameId ) )
		.set( "game_data", state );
	auto callback = std::bind( &Reporter<std::string>::response, &_gameStateReporter, std::placeholders::_1, std::placeholders::_2 );
	request( url, post, callback );
}

void OnlineConnector::statistic( int count, int firstPosition, int playerId )
{
	auto url = Config::shared().get( "multiplayerServerURL" ) + serverStatistic;
	auto callback = std::bind( &OnlineConnector::responseStatistic, this, std::placeholders::_1, std::placeholders::_2, playerId );
	auto post = PostString()
		.set( "scores_count", toStr( count ) )
		.set( "scores_skip", toStr( firstPosition ) );
	if( playerId != -1 )
		post.set( "id", toStr( playerId ) );
	request( url, post, callback );
}

void OnlineConnector::statisticLocal()
{
	statistic( 0, 0, _playerInfo.id );
}
void OnlineConnector::statisticOpponent()
{
	statistic( 0, 0, _opponentInfo.id );
}

void OnlineConnector::loadingReport( int percent )
{
	auto url = Config::shared().get( "multiplayerServerURL" ) + serverLoadingReport;
	auto post = PostString()
		.set( "id", toStr( _playerInfo.id ) )
		.set( "game_id", toStr( _gameId ) )
		.set( "loading_percent", toStr( percent ) );
	auto callback = std::bind( &OnlineConnector::responseLoadingReport, this, std::placeholders::_1, std::placeholders::_2 );
	request( url, post, callback );
}

void OnlineConnector::winReport( bool win, bool botMode )
{
	auto url = Config::shared().get( "multiplayerServerURL" ) + serverGameFinish;
	auto post = PostString()
		.set( "id", toStr( _playerInfo.id ) )
		.set( "game_id", toStr( _gameId ) )
		.set( "state", win ? "win" : "lose" );

	if( botMode )
		post.set( "bot", "yes" );

	auto callback = std::bind( &OnlineConnector::responseGameFinish, this, std::placeholders::_1, std::placeholders::_2 );

	request( url, post, callback );
}

void OnlineConnector::gameFinish( FinishAction const & act )
{
	std::string const actName =
		act == FinishAction::wait    ? "wait" :
		act == FinishAction::restart ? "restart" :
		/*t == FinishAction::abort  */ "break";

	auto url = Config::shared().get( "multiplayerServerURL" ) + serverGameFinish;
	auto post = PostString()
		.set( "id", toStr( _playerInfo.id ) )
		.set( "game_id", toStr( _gameId ) )
		.set( "action", actName );
	auto callback = std::bind( &Reporter<FinishAction>::response, &_gameFinishReporter, std::placeholders::_1, std::placeholders::_2 );
	request( url, post, callback );
}

void OnlineConnector::requestFile( const std::string& path, const std::string& name )
{
	auto url = Config::shared().get( "multiplayerServerURL" ) + serverRequestFile;
	auto post = PostString()
		.set( "dir", path );
	if( name.empty() == false )
		post.set( "name", name );
	auto callback = std::bind( &OnlineConnector::responseRequestFile, this, std::placeholders::_1 );
	request( url, post, callback );
}

void OnlineConnector::request( const std::string& url, const std::string& data, ResponseCallback callback )
{
	log( "s: %s", data.c_str() );
	LogLayer::shared().log( "s:" + data );

	_requestTime = ::clock();
	auto request = make_intrusive<network::HttpRequest>();
	request->setUrl( url.c_str() );
	request->setResponseCallback( std::bind( &OnlineConnector::response, this, callback, std::placeholders::_1, std::placeholders::_2 ) );
	request->setRequestType( cocos2d::network::HttpRequest::Type::POST );
	request->setRequestData( data.c_str(), data.size() );
	cocos2d::network::HttpClient::getInstance()->send( request );
}

void OnlineConnector::response( ResponseCallback callback, network::HttpClient * client, network::HttpResponse * response )
{
	checkConnection( response );

	auto data = response ? response->getResponseData() : nullptr;
	std::string body = data ? std::string( data->data(), data->size() ) : "";
	log( "r: %s", body.c_str() );
	LogLayer::shared().log( "r:" + body );

	auto timeElapsed = static_cast<float>((clock() - _requestTime)) / CLOCKS_PER_SEC;

	if( callback )
		callback( body, timeElapsed );
}

void OnlineConnector::checkConnection( network::HttpResponse * response )
{
	bool success = response ? response->isSucceed() : false;
	if( (_connected == Connected && !success) || (_connected == Disconnected && success) )
	{
		onConnectionChanged.pushevent( success );
	}
	_connected = success ? Connected : Disconnected;
}

void OnlineConnector::responseLogin( const std::string& body, float lag )
{
	RapidJsonNode json( body );
	bool result = json && json.get<std::string>( "result" ) == "ok";
	_playerInfo.id = json.get<int>( "id" );
	onLogged.pushevent( result, _playerInfo.id );
}

void OnlineConnector::responseSearchOpponent( const std::string& body, float lag )
{
	RapidJsonNode json( body );
	bool result = json.get<std::string>( "result" ) == "ok";
	_gameId = json.get<int>( "game_id" );
	if( result )
	{
		auto node = json.node( "opponent" );
		if( node )
		{
			_opponentInfo.id = strTo<int>( node.get<std::string>( "id" ) );
			_opponentInfo.nickname = node.get<std::string>( "nickname" );
			_opponentInfo.score = strTo<int>( node.get<std::string>( "score" ) );
		}
	}
	std::string statestr = json.get<std::string>("opponent_state");
	SearchOpponentState state =
		statestr == "wait"  ? SearchOpponentState::wait :
		statestr == "ready" ? SearchOpponentState::ready :
		/*      else       */ SearchOpponentState::cancel;
	result = result && !statestr.empty();

	onSearhOpponent.pushevent( result, lag, state );
}

void OnlineConnector::responseGameState( const std::string& body, float lag )
{
	RapidJsonNode json( body );
	bool result = json.get<std::string>( "result" ) == "ok";
	onGameSession.pushevent( result, json );
}

void OnlineConnector::responseStatistic( const std::string& body, float lag, int id )
{
	RapidJsonNode json( body );
	bool result = json.get<std::string>( "result" ) == "ok";
	if( result )
	{
		if( id == -1 )
		{
			auto node = json.node( "scores" );
			size_t size = node.size();
			for( size_t i = 0; i < size; ++i )
			{
				if( strTo<int>(node.at(i).get<std::string>("id")) == _playerInfo.id )
					_playerInfo.score = strTo<int>( node.at( i ).get<std::string>( "score" ) );
			}
		}
		else if( id == _playerInfo.id )
		{
			_playerInfo.score = strTo<int>( json.get<std::string>( "score" ) );
			onStatisticLocal.pushevent( result, _playerInfo );
		}
		else
		{
			_opponentInfo.id = id;
			_opponentInfo.score = strTo<int>( json.get<std::string>( "score" ) );
			_opponentInfo.nickname = json.get<std::string>( "nickname" );
			onStatisticOpponent.pushevent( result, _opponentInfo );
		}
	}
	onStatistic.pushevent( result, json );
}

void OnlineConnector::responseLoadingReport( const std::string& body, float lag )
{
	RapidJsonNode json( body );
	bool const result = json.get<std::string>( "result" ) == "ok";
	onLoadingReport.pushevent( result, json.get<int>( "opponent_loading_percent" ) );
}

void OnlineConnector::responseGameFinish( const std::string& body, float lag )
{
	RapidJsonNode json( body );
	bool const result = json.get<std::string>( "result" ) == "ok";


	if( json.contain( "opponent_action" ) )
	{
		std::string const oppoActName = json.get<std::string>( "opponent_action" );
		FinishAction const oppoAct =
			oppoActName == "wait" ? FinishAction::wait :
			oppoActName == "restart" ? FinishAction::restart :
			/*poActName == "break" ?*/ FinishAction::abort;

		onOpponentFinishAction.pushevent( result, oppoAct );
	}

	if( json.contain( "state" ) )
	{
		std::string const winState = json.get<std::string>( "state" );
		onGameFinish.pushevent( result, winState == "win" );
	}
}

void OnlineConnector::responseRequestFile( const std::string& body )
{
	onRequestFile.pushevent( body );
}

NS_CC_END