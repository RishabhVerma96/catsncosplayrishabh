#include "GameBoardOnline.h"
#include "CommandsDispatcher.h"
#include "tower.h"
#include "GameScene.h"
#include "ScoreCounter.h"
#include "OnlineConnector.h"
NS_CC_BEGIN

GameBoardOnline::GameBoardOnline( GameBoardType type )
: _type( type )
, _gameStateResult( GameBoardState::Battle )
, _timer(0)
, _disconnectTime( 0 )
, _disconnectTimer(0)
, _sessionId(-1)
, _connected(true)
{
	_bot.active = false;
	_bot.currentAction = 0;
}

GameBoardOnline::~GameBoardOnline()
{
	OnlineConnector::shared().onRequestFile.remove( _ID );
	OnlineConnector::shared().onStatistic.remove( _ID );

	auto scheduler = Director::getInstance()->getScheduler();
	scheduler->unscheduleAllForTarget( this );

	stopSync();
}

void GameBoardOnline::onConnectionChanged( bool connection )
{
	_connected = connection;
}

void GameBoardOnline::online_onGameStarted()
{
	requestOpponentInfo();
}

void GameBoardOnline::online_onGameFinished( bool victory )
{
	if( _type == GameBoardType::local )
	{
		_gameStateResult = victory ? GameBoardState::Win : GameBoardState::Lose;
	}
}

bool GameBoardOnline::checkGameFinished()
{
	if( _type == GameBoardType::local )
	{
		return GameBoard::checkGameFinished();
	}
	else if( _bot.active == false )
	{
		forceFinish();
	}
	else //bot
	{
		bool finishGame( false );
		finishGame = finishGame || (ScoreCounter::shared().getMoney( kScoreOpponentHealth ) <= 0);
		finishGame = finishGame || (isExistCreep( _units[0] ) == false && _isFinihedWaves == true);
		if( finishGame )
		{
			this->finishGame();
		}

		return finishGame;
	}

	return false;
}

void GameBoardOnline::createHeroes()
{
	if( _type == GameBoardType::local )
		GameBoard::createHeroes();
}

void GameBoardOnline::moveHero( Hero* hero, const Point& position )
{
	GameBoard::moveHero( hero, position );

	if( _type == GameBoardType::local && isTestDevice() && isTestModeActive() )
	{
		auto path = FileUtils::getInstance()->getWritablePath() + "bot.json";
		RapidJsonNode json;
		json.loadFile( path );
		if( !json || !json.node( "actions" ) )
			json.append_array( "actions" );
		auto node = json.node( "actions" ).push_back();
		node.append_node( "action" ).set( "move_hero" );
		node.append_node( "hero" ).set( hero->getName() );
		node.append_node( "pos" ).set( toStr( position ) );
		node.append_node( "time" ).set( _timer );
		json.saveFile( path );
	}
}

void GameBoardOnline::onLoadFinished()
{
	if( _type == GameBoardType::local && isTestDevice() && isTestModeActive() )
	{
		auto path = FileUtils::getInstance()->getWritablePath() + "bot.json";
		RapidJsonNode json;
		//json.loadFile( path );
		if( !json || !json.node( "actions" ) )
			json.append_array( "actions" );
		for( auto hero : GameBoard::_heroes )
		{
			std::string name = hero->getName();
			int index = strTo<int>( name.substr( strlen( "hero" ) ) ) - 1;
			auto pos = hero->getPosition();

			auto node = json.node( "actions" ).push_back();
			node.append_node( "action" ).set( "create_hero" );
			node.append_node( "index" ).set( index );
			node.append_node( "pos" ).set( toStr(pos) );
			node.append_node( "time" ).set( 0 );
		}
		json.saveFile( path );
	}
}

void GameBoardOnline::recvState( float dt )
{
	GameBoardState state;
	if( CommandsDispatcher::shared().recvState( state ) )
	{
		_disconnectTimer = 0;
		applyState( state );
	}
	
	if( !_isFinihedGame )
		_disconnectTimer += dt;	

	_gameLayer->getGameScene()->connectStatus( _disconnectTimer < (_disconnectTime - 1) );
	if( _disconnectTimer > _disconnectTime )
	{
		_gameStateResult = OnlineConnector::shared().isConnected() ? GameBoardState::Win : GameBoardState::Lose;
		forceFinish();
	}
}

void GameBoardOnline::sendState( float dt )
{
	_timer += dt;

	int health = ScoreCounter::shared().getMoney( kScoreHealth );;
	GameBoardState state;
	state._sessionId = _sessionId;
	state._health = static_cast<unsigned>(std::max( 0, health ));
	state._score = _statisticsParams.scores;
	state._gears = ScoreCounter::shared().getMoney( kScoreLevel );
	state._state = _gameStateResult;
	
	for( auto pair: _units )
	for( auto unit : pair.second )
	{
		auto type = unit->getType();
		switch( type )
		{
			case UnitType::creep:
			{
				GameBoardState::Creep creep;
				creep.id = unit->getId();
				creep.name = unit->getName();
				creep.health = unit->getCurrentHealth();
				creep.rate = unit->getRate();
				creep.routeIndex = unit->getRouteIndex();
				creep.routeSubType = unit->getRouteSubType();
				creep.routeCurrentSegment = unit->getMover().getRouteCurrentSegment();
				creep.routeCurrentSegmentPercent = unit->getMover().getRouteCurrentSegmentPercent();

				state._creeps.push_back( creep );
				break;
			}
			case UnitType::tower:
			{
				GameBoardState::Tower tower;
				tower.id = unit->getId();
				tower.name = unit->getName();
				tower.pos = unit->getPosition();
				tower.level = unit->getLevel();
				state._towers.push_back( tower );
				break;
			}
			case UnitType::hero:
			{
				GameBoardState::Hero desc;
				auto hero = dynamic_cast<Hero*>(unit.ptr());
				if( hero )
				{
					desc.id = unit->getId();
					desc.name = unit->getName();
					desc.health = unit->getCurrentHealth();
					desc.level = unit->getLevel();
					desc.pos = unit->getPosition();
					auto state = unit->current_state().get_name();
					switch( state )
					{
						case Unit::state_death: desc.state = GameBoardState::Hero::State::death; break;
						case Unit::state_move: desc.state = GameBoardState::Hero::State::move; break;
						default: desc.state = GameBoardState::Hero::State::idle; break;
					}
					if( _heroSend[desc.name] == false )
					{
						HeroExp::shared().skills( desc.name, desc.skills );
						_heroSend[desc.name] = true;
					}
				}
				state._heroes.push_back( desc );
				break;
			}
		}
	}
	state._bonusItems = _bonusItems;
	state._activeSkills = _activeSkills;
	_bonusItems.clear();
	_activeSkills.clear();

	state.incID();
	CommandsDispatcher::shared().sendState( state );
}

void GameBoardOnline::botUpdate( float dt )
{
	assert( _type == GameBoardType::opponent );
	assert( _bot.active );
	assert( _bot.json );
	assert( _bot.currentAction >= 0 );
	
	_timer += dt;

	if( !_connected )
		return;
	if( static_cast<size_t>(_bot.currentAction) >= _bot.json.node("actions").size() )
		return;

	auto actionJson = _bot.json.node( "actions" ).node( _bot.currentAction );
	std::string actionName = actionJson.get<std::string>( "action" );
	float time = actionJson.get<float>( "time" );
	if( _timer < time )
		return;
	
	bool executeSuccess( false );
	if( actionName == "build_tower" )
	{
		std::string towername = actionJson.get<std::string>( "tower" );
		int placeIndex = actionJson.get<int>( "place_index" );
		int id = actionJson.get<int>( "tower_id" );
		auto place = _gameLayer->getTowerPlace( "towerplace" + toStr(placeIndex) );
		int cost = mlTowersInfo::shared().getCost( towername, 1 );
		if( place && cost <= ScoreCounter::shared().getMoney( kScoreOpponentLevel ) )
		{
			auto tower = createTower( towername, place, false );
			tower->setId( id );
			ScoreCounter::shared().subMoney( kScoreOpponentLevel, cost, false, "" );
			executeSuccess = true;
		}
		if( !place )
		{
			executeSuccess = true;
		}
	}
	else if( actionName == "upgrade_tower" )
	{
		int id = actionJson.get<int>( "tower_id" );
		Unit::Pointer tower;

		for( auto pair : _units )
		for( auto unit : pair.second )
		{
			if( unit->getId() == id )
			{
				tower = unit;
				break;
			}
		}

		assert( tower );
		if( tower )
		{
			int cost = mlTowersInfo::shared().getCost( tower->getName(), tower->getLevel() + 1 );
			if( cost <= ScoreCounter::shared().getMoney( kScoreOpponentLevel ) )
			{
				upgradeTower( tower, false );
				ScoreCounter::shared().subMoney( kScoreOpponentLevel, cost, false, "" );
				executeSuccess = true;
			}
		}
		else
		{
			executeSuccess = false;
		}
	}
	else if( actionName == "create_hero" )
	{
		int index = actionJson.get<int>( "index" );
		auto pos = strTo<Point>( actionJson.get<std::string>( "pos" ) );
		std::vector<unsigned> dummySkills;
		createHero( index, pos, -1, dummySkills );
		executeSuccess = true;
	}
	else if( actionName == "move_hero" )
	{
		auto pos = strTo<Point>( actionJson.get<std::string>( "pos" ) );
		auto name = actionJson.get<std::string>( "hero" );
		for( auto hero : GameBoard::_heroes )
		{
			if( hero->getName() == name )
				GameBoard::moveHero( hero, pos );
		}
		executeSuccess = true;
	}
	else if( actionName == "skill" )
	{
		auto pos = strTo<Point>( actionJson.get<std::string>( "pos" ) );
		auto name = actionJson.get<std::string>( "name" );
		createActiveSkillUnit( name, pos );
		executeSuccess = true;
	}
	else if( actionName == "bonusitem" )
	{
		auto pos = strTo<Point>( actionJson.get<std::string>( "pos" ) );
		auto name = actionJson.get<std::string>( "name" );
		createBonusItem( pos, name );
		executeSuccess = true;
	}
	else
	{
		executeSuccess = true;
	}

	if( executeSuccess )
		++_bot.currentAction;
}

void GameBoardOnline::applyState( const GameBoardState& state )
{
	if( state._sessionId != _sessionId || _sessionId == -1 )
		return;

	if( _gameStateResult == GameBoardState::Battle && state._state != GameBoardState::Battle )
	{
		_gameStateResult = state._state;
		stopSync();
		return;
	}

	syncTowers( state._towers );
	syncCreeps( state._creeps );
	syncHeroes( state._heroes );
	syncBonusItems( state._bonusItems );
	syncActiveSkills( state._activeSkills );

	ScoreCounter::shared().setMoney( kScoreOpponentHealth, state._health, false );
	ScoreCounter::shared().setMoney( kScoreOpponentLevel, state._gears, false );
	ScoreCounter::shared().setMoney( kScoreOpponentSurvival, state._score, false );
}

void GameBoardOnline::forceFinish()
{
	if( _gameStateResult == GameBoardState::Win )
	{
		auto score = ScoreCounter::shared().getMoney( kScoreHealth );
		score = std::max( 1, score );
		ScoreCounter::shared().setMoney( kScoreHealth, score, false );
		stopSync();
		finishGame();
	}
	else if( _gameStateResult == GameBoardState::Lose )
	{
		ScoreCounter::shared().setMoney( kScoreHealth, 0, false );
		stopSync();
		finishGame();
	}
}

void GameBoardOnline::requestOpponentInfo()
{
	auto opponentName = OnlineConnector::shared().getOpponentNickname();
	_gameLayer->getGameScene()->displayName( opponentName, true );
}

void GameBoardOnline::addLevelScore( int score )
{
	if( _type == GameBoardType::local )
		ScoreCounter::shared().addMoney( kScoreLevel, score, false );
	else if( _bot.active )
		ScoreCounter::shared().addMoney( kScoreOpponentLevel, score, false );
}

void GameBoardOnline::subHealth( int score )
{
	if( _type == GameBoardType::local )
		ScoreCounter::shared().subMoney( kScoreHealth, score, false, "" );
	else if( _bot.active )
		ScoreCounter::shared().subMoney( kScoreOpponentHealth, score, false, "" );
}

void GameBoardOnline::setStartScore( int score )
{
	if( _type == GameBoardType::local )
		ScoreCounter::shared().setMoney( kScoreLevel, score, false );
	else if( _bot.active )
		ScoreCounter::shared().setMoney( kScoreOpponentLevel, score, false );
}

void GameBoardOnline::setStartHealth( int score )
{
	if( _type == GameBoardType::local )
		ScoreCounter::shared().setMoney( kScoreHealth, score, false );
	else if( _bot.active )
		ScoreCounter::shared().setMoney( kScoreOpponentHealth, score, false );
}

bool GameBoardOnline::updateUnitSkills( Unit* unit, float dt )
{
	//if( _type == GameBoardType::local )
		return GameBoard::updateUnitSkills( unit, dt );
	//return false;
}

bool GameBoardOnline::isDeathUnit( Unit* unit )
{
	bool isDeath = GameBoard::isDeathUnit( unit );
	if( isDeath )
	{
		bool alive = false; 
		IntrusivePtr<Unit> ptr(unit);
		alive = alive || std::find( _creeps.begin(), _creeps.end(), ptr ) != _creeps.end();
		isDeath = !alive;
	}
	return isDeath;
}

int GameBoardOnline::getScoreForUnit( Unit::Pointer unit )
{
	if( _type == GameBoardType::local || _bot.active )
		return GameBoard::getScoreForUnit( unit );
	return 0;
}

void GameBoardOnline::writeGameScore( int score )
{
	if( _type == GameBoardType::local )
		ScoreCounter::shared().setMoney( kScoreSurvival, score, false );
}

void GameBoardOnline::onPredelayWave( const WaveInfo & wave, float delay )
{
	if( wave.index == 0 )
	{
		_gameLayer->onFirstWave();
		delay = 5;
	}
	_waveGenerator.pause();
	if( _type == GameBoardType::local || _bot.active )
		_gameLayer->startWaveAfter( wave, delay );
}

void GameBoardOnline::online_onWaveWasStarted()
{
}

void GameBoardOnline::online_onCreepWasCreated( Unit* unit )
{
	if( _type == GameBoardType::opponent )
		unit->setIsShowHealthIndicator( false );
}

void GameBoardOnline::online_onTowerWasCreated( Unit* tower, TowerPlace* place )
{
	if( _type == GameBoardType::local && isTestDevice() && isTestModeActive() )
	{
		auto path = FileUtils::getInstance()->getWritablePath() + "bot.json";
		RapidJsonNode json;
		json.loadFile( path );
		if( !json || !json.node( "actions" ) )
			json.append_array( "actions" );
		auto node = json.node( "actions" ).push_back();
		node.append_node( "action" ).set( "build_tower" );
		node.append_node( "place_index" ).set( place->getIndex() );
		node.append_node( "tower" ).set( tower->getName() );
		node.append_node( "id" ).set( tower->getId() + 1000000 );
		node.append_node( "time" ).set( _timer );
		json.saveFile( path );
	}
}

void GameBoardOnline::online_onTowerWasUpgraded( Unit* tower )
{
	if( _type == GameBoardType::local && isTestDevice() && isTestModeActive() )
	{
		auto path = FileUtils::getInstance()->getWritablePath() + "bot.json";
		RapidJsonNode json;
		json.loadFile( path );
		if( !json || !json.node( "actions" ) )
			json.append_array( "actions" );
		auto node = json.node( "actions" ).push_back();
		node.append_node( "action" ).set( "upgrade_tower" );
		node.append_node( "id" ).set( tower->getId() + 1000000 );
		node.append_node( "time" ).set( _timer );
		json.saveFile( path );
	}
}

void GameBoardOnline::online_onTowerWasSelled( Unit* tower )
{
}

void GameBoardOnline::online_onSkillWasUsed( Unit* unit, const Point& position )
{
	GameBoardState::UnitItem item;
	item.pos = position;
	item.name = unit->getName();
	_activeSkills.push_back( item );

	if( _type == GameBoardType::local && isTestDevice() && isTestModeActive() )
	{
		auto path = FileUtils::getInstance()->getWritablePath() + "bot.json";
		RapidJsonNode json;
		json.loadFile( path );
		if( !json || !json.node( "actions" ) )
			json.append_array( "actions" );
		auto node = json.node( "actions" ).push_back();
		node.append_node( "action" ).set( "skill" );
		node.append_node( "name" ).set( unit->getName() );
		node.append_node( "pos" ).set( toStr( position ) );
		node.append_node( "time" ).set( _timer );
		json.saveFile( path );
	}
}

void GameBoardOnline::online_onBonusItemWasUsed( const std::string& name, const Point& position )
{
	GameBoardState::UnitItem item;
	item.pos = position;
	item.name = name;
	_bonusItems.push_back(item);
	if( _type == GameBoardType::local && isTestDevice() && isTestModeActive() )
	{
		auto path = FileUtils::getInstance()->getWritablePath() + "bot.json";
		RapidJsonNode json;
		json.loadFile( path );
		if( !json || !json.node( "actions" ) )
			json.append_array( "actions" );
		auto node = json.node( "actions" ).push_back();
		node.append_node( "action" ).set( "bonusitem" );
		node.append_node( "name" ).set( name );
		node.append_node( "pos" ).set( toStr( position ) );
		node.append_node( "time" ).set( _timer );
		json.saveFile( path );
	}
}

void GameBoardOnline::startSync( bool gameWithBot )
{
	if( _bot.active == false )
	{
		if( gameWithBot == false )
		{
			_disconnectTime = Config::shared().get<float>( "online_disconnectGameDuration" );

			if( _type == GameBoardType::local )
				_localSync.start( this );
			else
				_opponentSync.start( this );
		}
	}
	else
	{
		auto callbackUpdate = std::bind( &GameBoardOnline::botUpdate, this, std::placeholders::_1 );
		float frequency = 0.5;
		auto key = "GameBoardOnline_bot";
		auto scheduler = Director::getInstance()->getScheduler();
		scheduler->schedule( callbackUpdate, this, frequency, false, key );
	}
}

void GameBoardOnline::stopSync()
{
	_localSync.stop();
	_opponentSync.stop();
}

void GameBoardOnline::loadBot()
{
	_bot.active = true;
	OnlineConnector::shared().onRequestFile.add( _ID, std::bind( &GameBoardOnline::recieveLoadBot, this, std::placeholders::_1 ) );
	OnlineConnector::shared().requestFile( "bots" );
	//auto string = FileUtils::getInstance()->getStringFromFile( "bot2.json" );
	//recieveLoadBot( string );
}

void GameBoardOnline::recieveLoadBot( const std::string& jsonBot )
{
	_bot.json.parse( jsonBot );
	if( jsonBot.empty() == false )
	{
		_gameLayer->getGameScene()->closeWaitOpponentLayer();
	}
}

void GameBoardOnline::syncTowers( const std::vector<GameBoardState::Tower>& towers )
{
	for( auto iter = _towers.begin(); iter != _towers.end(); )
	{
		auto id = (*iter)->getId();
		bool remove( true );
		for( auto desc : towers )
		{
			if( desc.id == id )
			{
				remove = false;
				break;
			}
		}
		if( remove )
		{
			removeTower( (*iter), false );
			iter = _towers.erase( iter );
		}
		else
		{
			++iter;
		}
	}
	for( auto desc : towers )
	{
		auto tower = getTower( desc.id );
		if( !tower )
		{
			if( mlTowersInfo::shared().isExist( desc.name ) )
			{
				auto tower = createTower( desc.name, desc.pos, false );
				if( tower )
				{
					tower->setId( desc.id );
					_towers.push_back( tower );
				}
			}
			else
			{
				log( "undef: %s", desc.name.c_str() );
			}
		}
		if( tower && tower->getLevel() != desc.level )
		{
			auto newTower = upgradeTower( tower, true, false );
			auto iter = std::find( _towers.begin(), _towers.end(), tower );
			assert( iter != _towers.end() );
			if( iter != _towers.end() )
				_towers.erase( iter );
			_towers.push_back( newTower );
		}
	}
}

void GameBoardOnline::syncCreeps( const std::vector<GameBoardState::Creep>& creeps )
{
	for( auto iter = _creeps.begin(); iter != _creeps.end(); )
	{
		auto id = (*iter)->getId();
		bool remove( true );
		for( auto desc : creeps )
		{
			if( desc.id == id )
			{
				remove = false;
				break;
			}
		}
		if( remove )
		{
			(*iter)->setCurrentHealth( 0 );
			iter = _creeps.erase( iter );
		}
		else
		{
			++iter;
		}
	}
	for( auto desc : creeps )
	{
		auto creep = getCreep( desc.id );
		if( !creep )
		{
			auto name = desc.name;
			auto routeSubType = desc.routeSubType;
			auto routeIndex = desc.routeIndex;

			auto creep = createCreep( name, routeSubType, routeIndex );
			creep->setId( desc.id );
			creep->setLifeCost( 0 );
			creep->setRate( desc.rate );
			creep->setIsComputeDamage( false );
			_creeps.push_back( creep );
		}
	}
	syncCreepsHealth( creeps );
	syncCreepsPosition( creeps );
}

void GameBoardOnline::syncHeroes( const std::vector<GameBoardState::Hero>& heroes )
{
	for( auto desc : heroes )
	{
		auto hero = getHero( desc.id );
		if( !hero )
		{
			if( desc.name.find( "hero" ) == 0 )
			{
				auto name = desc.name;
				int index = strTo<int>( name.substr( 4 ) ) - 1; //"hero..";
				hero = createHero( index, desc.pos, desc.level, desc.skills );
				hero->setId( desc.id );
				_heroes.push_back( hero );
			}
		}
		if( hero )
		{
			hero->setCurrentHealth( desc.health );
			if( desc.state == GameBoardState::Hero::State::move )
			{
				hero->moveTo( desc.pos );
			}
			if( (hero->getBasePosition() - desc.pos).lengthSquared() > 10 )
			{
				hero->moveTo( desc.pos );
			}
		}
	}
}

void GameBoardOnline::syncBonusItems( const std::vector<GameBoardState::UnitItem>& items )
{
	for( auto& desc : items )
	{
		createBonusItem( desc.pos, desc.name );
	}
}

void GameBoardOnline::syncActiveSkills( const std::vector<GameBoardState::UnitItem>& items )
{
	for( auto& desc : items )
	{
		createActiveSkillUnit( desc.name, desc.pos );
	}
}

void GameBoardOnline::syncCreepsHealth( const std::vector<GameBoardState::Creep>& creeps )
{
	for( auto desc : creeps )
	{
		auto health = desc.health;
		auto creep = getCreep( desc.id );
		if( creep )
		{
			creep->setCurrentHealth( health );
		}
		else
		{
			//assert( 0 );
			log( "not founded creep" );
		}
	}
}

void GameBoardOnline::syncCreepsPosition( const std::vector<GameBoardState::Creep>& creeps )
{
	for( auto desc : creeps )
	{
		auto points = desc.routeCurrentSegment;
		auto percent = desc.routeCurrentSegmentPercent;
		auto creep = getCreep( desc.id );
		if( creep )
		{
			creep->getMover().setRouteCurrentSegment( points, false );
			creep->getMover().setRouteCurrentSegmentPercent( percent );
		}
		else
		{
			//assert( 0 );
		}
	}
}

Unit::Pointer GameBoardOnline::getTower( int id )
{
	for( auto& unit : _towers )
	{
		if( unit->getId() == id )
			return unit;
	}
	return nullptr;
}

Unit::Pointer GameBoardOnline::getCreep( int id )
{
	for( auto& unit : _creeps )
	{
		if( unit->getId() == id )
			return unit;
	}
	return nullptr;
}

Hero::Pointer GameBoardOnline::getHero( int id )
{
	for( auto& unit : _heroes )
	{
		if( unit->getId() == id )
			return unit;
	}
	return nullptr;
}


GameBoardOnline::Sync GameBoardOnline::_localSync( "GameBoardOnline_local", std::bind( &GameBoardOnline::sendState, std::placeholders::_1, std::placeholders::_2 ) );
GameBoardOnline::Sync GameBoardOnline::_opponentSync( "GameBoardOnline_opponent", std::bind( &GameBoardOnline::recvState, std::placeholders::_1, std::placeholders::_2 ) );

void GameBoardOnline::Sync::start( GameBoardOnline * gb )
{
	_delegate = gb;
	CommandsDispatcher::shared().startSession( _delegate, _delegate->_type );

	float const interval = _delegate->_type == GameBoardType::local ? Config::shared().get<float>( "online_frequencyGameUpdate" ) : 0;

	Director::getInstance()->getScheduler()->schedule( std::bind( _callback, _delegate, std::placeholders::_1 ), _delegate, interval, false, _id );
}

void GameBoardOnline::Sync::stop()
{
	CommandsDispatcher::shared().breakSession();

	Director::getInstance()->getScheduler()->unschedule( _id, _delegate );

	_delegate = nullptr;
}

NS_CC_END
