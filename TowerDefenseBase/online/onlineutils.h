#ifndef __onlineutils_h__
#define __onlineutils_h__
#include "cocos2d.h"
#include "json/document.h"
#include "network/HttpClient.h"
#include "support.h"
#include "RapidJson.h"
#include <stdint.h>

NS_CC_BEGIN;

struct GameBoardState
{
	static unsigned resetCounter();
	GameBoardState();
	unsigned incID();

	struct Creep
	{
		std::string name;
		unsigned id;
		float health;
		float rate;
		RouteSubType routeSubType;
		unsigned routeIndex;
		unsigned routeCurrentSegment;
		float routeCurrentSegmentPercent;
	};
	struct Tower
	{
		std::string name;
		Point pos;
		unsigned id;
		unsigned level;
	};
	struct Hero
	{
		enum class State
		{
			death = 0,
			move = 1,
			idle = 2,
		};
		unsigned id;
		std::string name;
		Point pos;
		State state;
		float health;
		unsigned level;
		std::vector<unsigned> skills; /*only one time*/
	};
	struct UnitItem
	{
		std::string name;
		Point pos;
	};

	std::vector<Creep> _creeps;
	std::vector<Tower> _towers;
	std::vector<Hero> _heroes;
	std::vector<UnitItem> _bonusItems;
	std::vector<UnitItem> _activeSkills;
	unsigned _id;
	unsigned _health;
	unsigned _gears;
	unsigned _score;
	int _sessionId;
	enum State
	{
		Battle = 0,
		Win = 1, 
		Lose = 2, 
	}_state;

	void parse( const RapidJsonNode& json );
	void parse( const std::string& jsonString );
	void write( std::string& jsonString )const;
};

/*
class SafeRequest
{
public:
	struct Info {
		float timeElapsed;
	};
	typedef std::function<void( network::HttpResponse & response, Info & info )> Callback;

public:
	static SafeRequest & create( std::string const & url, Callback onDone, IntrusivePtr<Ref> recipient );

public:
	void addPostParam( std::string const & key, std::string const & value );
	void send();

private:
	SafeRequest( SafeRequest const & );
	SafeRequest( std::string const & url, Callback onDone, IntrusivePtr<Ref> recipient );
	virtual ~SafeRequest();

private:
	network::HttpRequest * const _request;
	IntrusivePtr<Ref> const _recipient;
	Callback const _onDone;
	std::string _body;
	clock_t _sendTime;
	Info _info;
};
*/
NS_CC_END;

#endif
