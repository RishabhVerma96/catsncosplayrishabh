#include "onlineutils.h"
#include "Generics.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/writer.h"

NS_CC_BEGIN;

//*****************************************************************************
// class GameBoardState
//*****************************************************************************

static unsigned GameBoardState_id( 0 );

const std::string kId( "id" );
const std::string kSessionId( "sessionid" );
const std::string kCreepsNode( "creeps" );
const std::string kTowersNode( "towers" );
const std::string kHeroesNode( "heroes" );
const std::string kBonusItemsNode( "bonusitems" );
const std::string kActiveSkillsNode( "activeskills" );
const std::string kName( "name" );
const std::string kHealth( "health" );
const std::string kRate( "rate" );
const std::string kRouteSubType( "rst" );
const std::string kRouteIndex( "routeindex" );
const std::string kRouteSegment( "routesegment" );
const std::string kRouteSegmentPercent( "routesegmentpersent" );
const std::string kPosition( "pos" );
const std::string kState( "state" );
const std::string kLevel( "level" );
const std::string kSkills( "skills" );
const std::string kScore( "score" );
const std::string kGears( "gears" );

unsigned GameBoardState::resetCounter()
{
	GameBoardState_id = 0;
	return GameBoardState_id;
}

GameBoardState::GameBoardState()
: _id(0)
, _health(0)
, _sessionId( -1 )
, _state(Battle)
{
}

unsigned GameBoardState::incID()
{
	return _id = GameBoardState_id++;
}

void GameBoardState::parse( const RapidJsonNode& doc )
{
	_id = doc.get<unsigned>( kId );
	_sessionId = doc.get<int>( kSessionId );
	_health = doc.get<unsigned>( kHealth );
	_score = doc.get<unsigned>( kScore );
	_gears = doc.get<unsigned>( kGears );
	_state = static_cast<State>(doc.get<int>( kState ));

	auto creeps = doc.node( kCreepsNode );
	auto towers = doc.node( kTowersNode );
	auto heroes = doc.node( kHeroesNode );
	auto bonusItems = doc.node( kBonusItemsNode );
	auto activeSkills = doc.node( kActiveSkillsNode );
	for( size_t i = 0; i < creeps.size(); ++i )
	{
		auto node = creeps.at( i );
		if( !node )continue;
		Creep creep;

		creep.name = node.get<std::string>( kName );
		creep.id = node.get<unsigned>( kId );
		creep.health = node.get<float>( kHealth );
		creep.rate = node.get<float>( kRate );
		creep.routeSubType = static_cast<RouteSubType>(node.get<int>( kRouteSubType ));
		creep.routeIndex = node.get<unsigned>( kRouteIndex );
		creep.routeCurrentSegment = node.get<unsigned>( kRouteSegment );
		creep.routeCurrentSegmentPercent = node.get<float>( kRouteSegmentPercent );

		_creeps.push_back( creep );
	}
	for( size_t i = 0; i < towers.size(); ++i )
	{
		auto node = towers.at( i );
		if( !node )continue;
		Tower tower;

		tower.id = node.get<unsigned>( kId );
		tower.name = node.get<std::string>( kName );
		tower.pos = strTo<Point>( node.get<std::string>( kPosition ) );
		tower.level = node.get<unsigned>( kLevel );

		_towers.push_back( tower );
	}
	for( size_t i = 0; i < heroes.size(); ++i )
	{
		auto node = heroes.at( i );
		if( !node )continue;
		Hero hero;

		hero.id = node.get<unsigned>( kId );
		hero.name = node.get<std::string>( kName );
		hero.pos = strTo<Point>( node.get<std::string>( kPosition ) );
		hero.state = static_cast<GameBoardState::Hero::State>(node.get<int>( kState ));
		hero.health = node.get<float>( kHealth );
		hero.level = node.get<unsigned>( kLevel );
		std::string str = node.get<std::string>( kSkills );
		if( str.empty() == false )
		{
			std::vector<std::string> skills;
			split( skills, str );
			for( auto& s : skills )
				hero.skills.push_back( strTo<unsigned>( s ) );
		}
		_heroes.push_back( hero );
	}
	auto readUnits = [&]( std::vector<UnitItem>&units, const RapidJsonNode& node )
	{
		for( size_t i = 0; i < node.size(); ++i )
		{
			auto child = node.at( i );
			if( !child )continue;
			UnitItem item;
			item.pos = strTo<Point>( child.get<std::string>( kPosition ) );
			item.name = child.get<std::string>( kName );
			units.push_back( item );
		}
	};
	readUnits( _bonusItems, bonusItems );
	readUnits( _activeSkills, activeSkills );
}

void GameBoardState::parse( const std::string& jsonString )
{
	RapidJsonNode doc( jsonString );
	parse( doc );
}

void GameBoardState::write( std::string& jsonString )const
{
	RapidJsonNode doc;
	doc.append_node( kId ).set( _id );
	doc.append_node( kSessionId ).set( _sessionId );
	doc.append_node( kHealth ).set( _health );
	doc.append_node( kState ).set( static_cast<int>(_state) );
	doc.append_node( kScore ).set( static_cast<int>(_score) );
	doc.append_node( kGears ).set( static_cast<int>(_gears) );

	auto creeps = doc.append_array( kCreepsNode );
	auto towers = doc.append_array( kTowersNode );
	auto heroes = doc.append_array( kHeroesNode );
	auto bonusitems = doc.append_array( kBonusItemsNode );
	auto activeSkills = doc.append_array( kActiveSkillsNode );
	for( auto creep : _creeps )
	{
		auto node = creeps.push_back();
		node.append_node( kName ).set( creep.name );
		node.append_node( kId ).set( creep.id );
		node.append_node( kHealth ).set( creep.health );
		node.append_node( kRate ).set( creep.rate );
		node.append_node( kRouteSubType ).set( static_cast<int>(creep.routeSubType) );
		node.append_node( kRouteIndex ).set( creep.routeIndex );
		node.append_node( kRouteSegment ).set( creep.routeCurrentSegment );
		node.append_node( kRouteSegmentPercent ).set( creep.routeCurrentSegmentPercent );
	}
	for( auto tower : _towers )
	{
		auto node = towers.push_back();
		node.append_node( kId ).set( tower.id );
		node.append_node( kName ).set( tower.name );
		node.append_node( kPosition ).set( toStr( tower.pos ) );
		node.append_node( kLevel ).set( tower.level );
	}
	for( auto hero : _heroes )
	{
		auto node = heroes.push_back();
		node.append_node( kId ).set( hero.id );
		node.append_node( kName ).set( hero.name );
		node.append_node( kPosition ).set( toStr( hero.pos ) );
		node.append_node( kState ).set( static_cast<int>(hero.state) );
		node.append_node( kHealth ).set( hero.health );
		node.append_node( kLevel ).set( hero.level );
		if( hero.skills.empty() == false )
		{
			std::string str;
			for( auto s : hero.skills )
				str += toStr( s ) + ",";
			str.erase( str.begin() + (str.size() - 1) );
			node.append_node( kSkills ).set( str );
		}
	}
	auto writeUnits = [&]( const std::vector<UnitItem>& units, RapidJsonNode& node )
	{
		for( auto desc : units )
		{
			auto child = node.push_back();
			child.append_node( kName ).set( desc.name );
			child.append_node( kPosition ).set( toStr( desc.pos ) );
		}
	};
	writeUnits( _bonusItems, bonusitems );
	writeUnits( _activeSkills, activeSkills );
	doc.toString( jsonString );
}

//*****************************************************************************
// class SafeRequest
//*****************************************************************************
/*
SafeRequest & SafeRequest::create( std::string const & url, Callback onDone, IntrusivePtr<Ref> recipient )
{
	return *(new SafeRequest( url, onDone, recipient ));
}

SafeRequest::SafeRequest( std::string const & url, Callback onDone, IntrusivePtr<Ref> recipient )
	: _request( new network::HttpRequest() )
	, _recipient( recipient )
	, _onDone( onDone )
{
	_request->setUrl( url.c_str() );
	_request->setRequestType( cocos2d::network::HttpRequest::Type::POST );
	_request->setResponseCallback( [this]( network::HttpClient * client, network::HttpResponse * response )
	{
		if( _recipient->getReferenceCount() > 1 && response != nullptr )
		{
			_info.timeElapsed = static_cast<float>((clock() - _sendTime) * 1000) / CLOCKS_PER_SEC;
			
			_onDone( *response, _info );
		}
		delete this;
	} );
}

SafeRequest::~SafeRequest()
{
	_request->release();
}

void SafeRequest::addPostParam( std::string const & key, std::string const & value )
{
	if( _body.empty() )
		_body = StringUtils::format( "%s=%s", key.c_str(), value.c_str() );
	else
		_body.append( StringUtils::format( "&%s=%s", key.c_str(), value.c_str() ) );
}

void SafeRequest::send()
{
	_request->setRequestData( _body.data(), _body.size() );
	cocos2d::network::HttpClient::getInstance()->send( _request );
	_sendTime = clock();
}
*/
NS_CC_END;
