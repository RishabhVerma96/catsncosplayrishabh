#ifndef __CommandsDispatcher_h__
#define __CommandsDispatcher_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/Singlton.h"
#include "gameboardonline.h"
#include "RapidJson.h"
NS_CC_BEGIN

class CommandsDispatcher : public Singlton<CommandsDispatcher>
{
	friend class Singlton<CommandsDispatcher>;
	CommandsDispatcher();
	void onCreate();
public:
	void onRecieve( bool result, const RapidJsonNode& json );
public:
	void startSession( GameBoardOnline* board, GameBoardType type );
	void breakSession();

	bool recvState( GameBoardState& state );
	void sendState( const GameBoardState& state );

	void updateStates( float dt );
protected:
private:
	std::string _currentSendState;
	std::list<GameBoardState> _receivedStateQueue;
	int _currentStateId;

	GameBoardOnline* _local;
	GameBoardOnline* _opponent;
};




NS_CC_END
#endif // #ifndef CommandsDispatcher