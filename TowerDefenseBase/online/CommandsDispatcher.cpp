#include "CommandsDispatcher.h"
#include "OnlineConnector.h"
NS_CC_BEGIN

//std::deque< std::string > _onSend;
//std::deque< std::string > _recieved;
//void dummy_send( const std::string& json )
//{
//	_mutexHtml.lock();
//	_onSend.push_back( json );
//	_mutexHtml.unlock();
//}
//
//void dummy_sender()
//{
//	while( true )
//	{
//		Sleep( 100 );
//		_mutexHtml.lock();
//		if( _onSend.empty() == false )
//		{
//			_recieved.push_back( _onSend.front() );
//			_onSend.pop_front();
//		}
//		_mutexHtml.unlock();
//	}
//}
//
//void dummy_reciever()
//{
//	while( true )
//	{
//		Sleep( 100 );
//		_mutexHtml.lock();
//		while( _recieved.empty() == false )
//		{
//			CommandsDispatcher::shared().onRecieve( _recieved.front() );
//			_recieved.pop_front();
//		}
//		_mutexHtml.unlock();
//	}
//}
//
CommandsDispatcher::CommandsDispatcher()
:_currentStateId( 0 )
, _local( nullptr )
, _opponent( nullptr )
{}

void CommandsDispatcher::onCreate()
{
	//std::thread sender( dummy_sender );
	//std::thread reciever( dummy_reciever );
	//sender.detach();
	//reciever.detach();
}

void CommandsDispatcher::onRecieve( bool result, const RapidJsonNode & doc )
{
	auto str = doc.get<std::string>( "game_data" );
	std::string const outcome = doc.get<std::string>( "outcome" );
	if( str.empty() && outcome.empty() )
		return;
	RapidJsonNode gamestateNode( str );
	GameBoardState state;
	state.parse( gamestateNode );

	if( !outcome.empty() )
		state._state = outcome == "win" ? GameBoardState::State::Win : GameBoardState::State::Lose;

	_receivedStateQueue.push_back( state );
	auto compare = []( const GameBoardState & l, const GameBoardState & r )
	{
		return l._id < r._id;
	};
	_receivedStateQueue.sort( compare );
}

void CommandsDispatcher::startSession( GameBoardOnline* board, GameBoardType type )
{
	_currentSendState.clear();
	_receivedStateQueue.clear();

	if( type == GameBoardType::local ) _local = board;
	if( type == GameBoardType::opponent ) _opponent = board;

	auto callbackUpdate = std::bind( &CommandsDispatcher::updateStates, this, std::placeholders::_1 );
	auto scheduler = Director::getInstance()->getScheduler();
	std::string key = "CommandsDispatcher_updateStates";
	if( scheduler->isScheduled( key, this ) == false )
		scheduler->schedule( callbackUpdate, this, 0, false, key );

	_currentStateId = GameBoardState::resetCounter();
	OnlineConnector & oc = OnlineConnector::shared();
	oc.startReportingGameState( _currentSendState );
	oc.onGameSession.add( 1, std::bind( &CommandsDispatcher::onRecieve, this, std::placeholders::_1, std::placeholders::_2 ) );
}

void CommandsDispatcher::breakSession()
{
	_local = _opponent = nullptr;

	OnlineConnector & oc = OnlineConnector::shared();
	oc.stopReportingGameState();
	oc.onGameSession.remove( 1 );
}

bool CommandsDispatcher::recvState( GameBoardState& state )
{
	bool result( false );
	if( _receivedStateQueue.empty() == false )
	{
		//if( _receivedStateQueue.front()._id <= _currentStateId )
		{
			state = _receivedStateQueue.front();
			_currentStateId += 1;
			_receivedStateQueue.pop_front();
			result = true;
		}
		//else
		//{
		//	log( "wait" );
		//}
	}
	return result;
}

void CommandsDispatcher::sendState( const GameBoardState& state )
{
	std::string json;
	state.write( json );
	_currentSendState = json;
}

void CommandsDispatcher::updateStates( float dt )
{
}



NS_CC_END