#ifndef __Configuration_h__
#define __Configuration_h__
#include <string>
#include "cocos2d.h"

extern const std::string kGameFolder;

/*
#define GAME_ISLANDDEFENSE 0 
#define GAME_STEAMPUNK 1 
#define GAME_STEAMPUNKPRO 2
#define GAME_ISLANDDEFENSE_THUMPSTAR 3
#define GAME_STEAM 4
#define GAME_STEAMPUNKWIN8 5

namespace cocos2d
{
namespace k
{
	extern const std::string resourceGameSceneFolder;
	extern const std::string resourceHeroRoomFolder;
	extern const float isometricValue;

namespace configuration
{

extern const std::string gameName;
extern const bool useInapps;
extern const bool useFuel;
extern const bool useFreeFuel;
extern const bool useFreeGold;
extern const bool freeGoldAsVideo;
extern const bool useStarsForUnlock;
extern const bool useBoughtLevelScoresOnEveryLevel;
extern const bool useBoughtLevelScoresOnlyRestartLevel;
extern const bool useAds;
extern const bool useLeaderboards;
extern const bool useEula;
extern const bool useUsersGift;
extern const bool useStatistic;
extern const bool useRateSpeedButton;
extern const bool useDailyReward;

extern const bool useHero;
extern const int  minLevelHero;
extern const bool useHeroRoom;
extern const bool useHeroesPromo;
extern const int heroesCount;
extern const int heroesCountSelectMax;
extern const bool hideMainLogo;
extern const bool desertBuild;


extern const bool hideMoreButton;
extern const bool useRateMe;
extern const std::string linkToStore;
extern const std::string linkToStorePaidVersion;
extern const std::string iconForPaidGame;
extern const bool useLinkToPaidVersion;

static const int InterstitialNo( 0x0 );
static const int InterstitialAdmob( 0x1 );
static const int InterstitialChartboost( 0x2 );
static const int InterstitialSupersonic( 0x3 );
static const int InterstitialFyber( 0x4 );
static const int InterstitialDeltaDNA( 0x5 );
static const int InterstitialOgury( 0x6 );
static const int InterstitialChartboostAdmob( 0x7 );
static const int InterstitialAppodeal( 0x8 );

static const int RewardVideoNo( 0x0 );
static const int RewardVideoVungle( 0x10 );
static const int RewardVideoSupersonic( 0x11 );
static const int RewardVideoFyber( 0x12 );
static const int RewardVideoDeltaDNA( 0x13 );
static const int RewardVideoAppodeal( 0x14 );

static const int OfferWallNo( 0x20 );
static const int OfferWallSupersonic( 0x21 );

extern const int AdsTypeRewardVideo;
extern const int AdsTypeInterstitial;
extern const int AdsTypeOfferWall;

extern const bool useRestoreButton;
extern const std::string kInappPackage;
extern const std::string kInappGold1;
extern const std::string kInappGold2;
extern const std::string kInappGold3;
extern const std::string kInappGold4;
extern const std::string kInappGold5;
extern const std::string kInappGear1;
extern const std::string kInappGear2;
extern const std::string kInappGear3;
extern const std::string kInappFuel1;
extern const std::string kInappHero2;
extern const std::string kInappHero3;
extern const std::string kInappAllHeroes;
extern const std::string kInappPackHeroes1;

//GameScene
extern const Size levelMapSize;
}
}
}
*/
#endif
