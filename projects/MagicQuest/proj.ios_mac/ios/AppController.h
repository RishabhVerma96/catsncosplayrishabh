#import <UIKit/UIKit.h>
#include <functional>
#include "ParamCollection.h"
#import <Batch/Batch.h>
#import "IronSource/IronSource.h"

@class RootViewController;

@interface AppController : NSObject <UIApplicationDelegate, BatchUnlockDelegate,ISOfferwallDelegate>
{
    UIWindow *window;
}

+ (AppController*)getInstance;

+ (void)admobShowInterstitial;

+ (void)vunglePlayVideo;
+ (void)vungleWillShow;
+ (void)vungleDidClosed:(bool)compilted;

+ (void)flurryEvent:(const ParamCollection&)params;

+ (void)openURL:(const char*)URL;

+ (void)requestEngage: (const char*) name;

+(void)fyberPlayVideo;
+(void)fyberInterstitial;

+(void)initialiseIronSource;
+(void)showOfferWallOfIronSource;

+(void)chartboostShowInterstitial;
+(bool)chartboostInterstitialIsAvailabled;
+(void)chartboostShowMoreApps;
+(void)chartboostShowRewardedVideo;
+(bool)chartboostRewardedVideoIsAvailabled;

+(bool)appodealIsVideoAvailabled;
+(void)appodealPlayVideo;
+(void)appodealInterstitial;
-(void)appodealVideoStarted;
-(void)appodealVideoFail;
-(void)appodealVideoFinished;
-(void)appodealExistOffer;
-(void)appodealNoOffer;

//- (void)offerwallHasChangedAvailability:(BOOL)available;
//- (void)offerwallDidShow;
//- (void)offerwallDidFailToShowWithError:(NSError *)error;
- (void)didReceiveOfferwallCredits:(NSDictionary *)creditInfo;
- (void)didFailToReceiveOfferwallCreditsWithError:(NSError *)error;
//- (void)offerwallDidClose;
//- (void)offerwallHasChangedAvailability:(BOOL)available;

@property(nonatomic, readonly) RootViewController* viewController;
@property std::function<void()> vungleCallbackWillShow;
@property std::function<void(bool)> vungleCallback;
@property std::function<void(bool)> supersonicCallback;
@property std::function<void(unsigned int)> supersonicCallbackOfferWall;
@property std::function<void(const char*)> BatchCallback;

@property std::function<void(bool)> fyberStarted;
@property std::function<void(bool)> fyberFinished;
@property std::function<void(bool)> appodealStarted;
@property std::function<void(bool)> appodealFinished;

@end
