/****************************************************************************
 Copyright (c) 2010 cocos2d-x.org

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import "AppController.h"
#import "platform/ios/CCEAGLView-ios.h"
#import "cocos2d.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "support.h"
#include "configuration.h"
#include "AdsPlugin.h"

#import <CommonCrypto/CommonDigest.h>
#import "Flurry.h"
#import <Appodeal/Appodeal.h>
#import <Chartboost/Chartboost.h>
#import <Batch/Batch.h>
#import <Tapjoy/Tapjoy.h>
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>
#import <AdSupport/ASIdentifierManager.h>

NSString* flurryApp = @"ZD4GSG27BNGJ4V5RRS5F";
NSString* chartboostAppID = @"55e56e035b14534a6f11952e";
NSString* chartboostAppSignature = @"a34466dfad7706d224ac4f020deee404f5ef3efa";
NSString* appodealAppID = @"49bc7fecc22a86480959bb3f7f32b7c7b60453e68427155c";
//NSString* batchID = @"55E57CD462123097C24A7FB459D39D";
NSString* batchID = @"DEV55E57CD4686F986061ACCE2C9FF";
NSString* tapjoyID = @"wnnzgMroRJS6K83AZe5P2gEBMxk2ARFQdQaf4PhnZCezXlse91AYWK1ykLXq";
NSString* ironSourceAppId = @"7131ff0d";

#define kUserId @"demo"

bool kSupersonicRWAvailabled(false);

RootViewController * rootView;

NS_CC_BEGIN

void openUrl(const std::string &url)
{
	NSString *stringUrl = [NSString stringWithUTF8String: url.c_str()];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString: stringUrl]];
}

void rateMe()
{
	NSString *templateUrl = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software";
	
	// TODO: add app id
	NSString *url = [NSString stringWithFormat: templateUrl, @""];
	openUrl([url cStringUsingEncoding: NSUTF8StringEncoding]);
}

NS_CC_END

@interface AppController () <AppodealVideoDelegate, ChartboostDelegate>

//@property(nonatomic, strong) GADInterstitial *admobInterstitial;
@property (nonatomic) BOOL isRVInitiated;
@property (nonatomic) BOOL isISInitiated;
@property (nonatomic) IBOutlet UIActivityIndicatorView *rewardedVideoSpinner;
@property (nonatomic) IBOutlet UIActivityIndicatorView *interstitialSpinner;
@property (nonatomic) IBOutlet UIActivityIndicatorView *offerWallSpinner;

@end

@implementation AppController

#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
static AppDelegate s_sharedApplication;

AppController*instance;

+(AppController*) getInstance
{
    return instance;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    auto interstitial = AdsPlugin::Service::appodeal;
    //MARK: Appodeal SDK Initialization
    AppodealAdType ADs = (AppodealAdType)(AppodealAdTypeRewardedVideo);
    if( interstitial == AdsPlugin::Service::appodeal )
        ADs = ADs | (AppodealAdType)(AppodealAdTypeInterstitial);
    
    [Appodeal initializeWithApiKey:appodealAppID types: ADs];
    
   
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    CCEAGLView *eaglView = [CCEAGLView viewWithFrame: [window bounds]
                                         pixelFormat: kEAGLColorFormatRGBA8
                                         depthFormat: GL_DEPTH24_STENCIL8_OES
                                  preserveBackbuffer: NO
                                          sharegroup: nil
                                       multiSampling: NO
                                     numberOfSamples: 0];
    [eaglView setMultipleTouchEnabled:YES];
    _viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    _viewController.view = eaglView;
    
    rootView = _viewController;
    
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        [window addSubview: _viewController.view];
    }
    else
    {
        [window setRootViewController:_viewController];
    }
    
    [window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setStatusBarHidden:true];
    
    instance = self;
    
    UIDevice *device = [UIDevice currentDevice];
    if( device )
    {
        NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
        if( currentDeviceId )
            cocos2d::setDeviceID( [currentDeviceId UTF8String] );
    }
    
    cocos2d::GLView *glview = cocos2d::GLViewImpl::createWithEAGLView(eaglView);
    cocos2d::Director::getInstance()->setOpenGLView(glview);
    cocos2d::Application::getInstance()->run();
    
    //MARK: Flurry
    [Flurry startSession:flurryApp];
    
    //MARK: Chartboost
    if (interstitial == AdsPlugin::Service::chartboost)
        [Chartboost startWithAppId:chartboostAppID appSignature:chartboostAppSignature delegate:self];
	
	//MARK: Batch
	[BatchUnlock setupUnlockWithDelegate:self];
	[Batch startWithAPIKey:batchID];
	
    //MARK: Tapjoy
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tjcConnectSuccess:)
                                                 name:TJC_CONNECT_SUCCESS
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tjcConnectFail:)
                                                 name:TJC_CONNECT_FAILED
                                               object:nil];
    [Tapjoy setDebugEnabled:NO];
    [Tapjoy connect:tapjoyID];
    
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound)
                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                              // Enable or disable features based on authorization.
                          }];
    
    
    [IronSource setOfferwallDelegate:self];
    [AppController initialiseIronSource];
    
    return YES;
}

-(void)automaticOfferRedeemed:(id<BatchOffer>)offer
{
	std::string string;
	
	for (id<BatchFeature> feature in [offer features])
	{
		NSString *reference = feature.reference;
		NSString *value = feature.value;
		
		if( [reference UTF8String] != nullptr )
			string += std::string([reference UTF8String]) + ":";
		if( [value UTF8String] != nullptr )
			string += std::string([value UTF8String]) + ",";
		
		if( self.BatchCallback )
			self.BatchCallback( string.c_str() );
	}
}

//MARK: Flurry
+ (void)flurryEvent:(const ParamCollection&)params
{
	NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
	for( auto& pair : params )
	{
		NSString * obj = [NSString stringWithUTF8String:pair.second.c_str()];
		NSString * key = [NSString stringWithUTF8String:pair.first.c_str()];
		[dictionary setObject:obj forKey:key];
	}
	
	NSString * event = [NSString stringWithUTF8String: params.at("event").c_str()];
	[Flurry logEvent:event withParameters:dictionary];
}

//MARK: Ironsource offerwall

+(void)initialiseIronSource
{
    [IronSource setUserId:@"parth16parikh"];
    [IronSource initWithAppKey:ironSourceAppId adUnits:@[IS_OFFERWALL]];
}

+(void)showOfferWallOfIronSource
{
//    [ISIntegrationHelper validateIntegration];
    [IronSource showOfferwallWithViewController:rootView placement:@"DefaultOfferWall"];
}
/*
- (void)offerwallHasChangedAvailability:(BOOL)available {
    if (available) {
        NSLog(@"offerwall available now");
    }else {
         NSLog(@"offerwall not available now");
    }
    [AppController showOfferWallOfIronSource];
}*/
/*
- (void)offerwallDidShow {
     NSLog(@"offerwall visible now");
}*/
/*
- (void)offerwallDidFailToShowWithError:(NSError *)error {
     NSLog(@"offerwall failed to show %@",error);
}*/

- (void)didReceiveOfferwallCredits:(NSDictionary *)creditInfo {
    
}

- (void)didFailToReceiveOfferwallCreditsWithError:(NSError *)error {
    
}
/*
- (void)offerwallDidClose {
    
}*/

//MARK: Appodeal
bool _appodealVideoAvailabled(false);
+(bool)appodealIsVideoAvailabled
{
    return _appodealVideoAvailabled;
}
+(void)appodealPlayVideo
{
    [Appodeal showAd:AppodealShowStyleRewardedVideo rootViewController:rootView];
}

-(void)appodealVideoStarted
{
    if( _appodealStarted )
        _appodealStarted( true );
}

-(void)appodealVideoFail
{
    if( _appodealFinished )
        _appodealFinished(false);
}

-(void)appodealVideoFinished
{
    if( _appodealFinished )
        _appodealFinished(true);
}
-(void)appodealExistOffer
{
    _appodealVideoAvailabled = true;
}

-(void)appodealNoOffer
{
    _appodealVideoAvailabled = false;
}

//MARK: Chartboost

- (void)didCacheInterstitial:(CBLocation)location
{
    cocos2d::log("Chartboost didCacheInterstitial");
}

- (void)didFailToLoadInterstitial:(CBLocation)location
                        withError:(CBLoadError)error
{
    cocos2d::log("Chartboost didFailToLoadInterstitial");
}

+(void)chartboostShowInterstitial
{
    [Chartboost showInterstitial:CBLocationHomeScreen];
}

+ (bool)chartboostInterstitialIsAvailabled
{
    return true;
    return [Chartboost hasInterstitial:CBLocationHomeScreen];
}

+(void)chartboostShowMoreApps
{
    if( [Chartboost hasMoreApps:CBLocationHomeScreen ] )
    {
        [Chartboost showMoreApps:CBLocationHomeScreen];
    }
}

+(void)chartboostShowRewardedVideo
{
    [Chartboost showRewardedVideo:CBLocationHomeScreen];
}

+(bool)chartboostRewardedVideoIsAvailabled
{
    return true;
    return [Chartboost hasRewardedVideo:CBLocationHomeScreen];
}

//MARK: Charboost Delegate
- (void)didDisplayMoreApps:(CBLocation)location;
{
}

- (void)didCacheMoreApps:(CBLocation)location;
{
}

- (void)didDismissMoreApps:(CBLocation)location;
{
}

- (void)didFailToLoadMoreApps:(CBLocation)location
                    withError:(CBLoadError)error
{
}

//MARK: Tapjoy
-(void)tjcConnectSuccess:(NSNotification*)notifyObj
{
    cocos2d::log("Tapjoy connect Succeeded");
}
-(void)tjcConnectFail:(NSNotification*)notifyObj
{
    cocos2d::log("Tapjoy connect Failed");
}


+(void)appodealInterstitial
{
    [Appodeal showAd:AppodealShowStyleInterstitial rootViewController:rootView];
}

+ (void)openURL:(const char *)URL
{
    NSString * string = [[NSString alloc] initWithCString:URL encoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
}

+ (void)requestEngage: (const char*) name
{
    
}

- (void)applicationWillResignActive:(UIApplication *)application {}
- (void)applicationDidBecomeActive:(UIApplication *)application {}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    cocos2d::Application::getInstance()->applicationDidEnterBackground();
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    cocos2d::Application::getInstance()->applicationWillEnterForeground();
}
- (void)applicationWillTerminate:(UIApplication *)application {}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    cocos2d::Application::getInstance()->onReceivedMemoryWarning();
}


- (void)dealloc {
    [window release];
    [super dealloc];
}

-(void)initializeStoryBoardBasedOnScreenSize {
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {    // The iOS device = iPhone or iPod Touch
        
        
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 480)
        {   // iPhone 3GS, 4, and 4S and iPod Touch 3rd and 4th generation: 3.5 inch screen (diagonally measured)
            
            NSLog(@"using iphone 4 screen height");
            
            // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone35
            UIStoryboard *iPhone35Storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone_4" bundle:nil];
            
            // Instantiate the initial view controller object from the storyboard
            UIViewController *initialViewController = [iPhone35Storyboard instantiateInitialViewController];
            
            // Instantiate a UIWindow object and initialize it with the screen size of the iOS device
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            
            // Set the initial view controller to be the root view controller of the window object
            self.window.rootViewController  = initialViewController;
            
            // Set the window object to be the key window and show it
            [self.window makeKeyAndVisible];
        }
        
        if (iOSDeviceScreenSize.height == 568)
        {   // iPhone 5 and iPod Touch 5th generation: 4 inch screen (diagonally measured)
            
            NSLog(@"using iphone 5 screen height");
            
            // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone4
            UIStoryboard *iPhone4Storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            
            // Instantiate the initial view controller object from the storyboard
            UIViewController *initialViewController = [iPhone4Storyboard instantiateInitialViewController];
            
            // Instantiate a UIWindow object and initialize it with the screen size of the iOS device
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            
            // Set the initial view controller to be the root view controller of the window object
            self.window.rootViewController  = initialViewController;
            
            // Set the window object to be the key window and show it
            [self.window makeKeyAndVisible];
        }
        
    } else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        
    {   // The iOS device = iPad
        
        //UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
        //UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
        //splitViewController.delegate = (id)navigationController.topViewController;
        
        // Instantiate a new storyboard object using the storyboard file named Storyboard_iPhone4
        UIStoryboard *iPhone4Storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        // Instantiate the initial view controller object from the storyboard
        UIViewController *initialViewController = [iPhone4Storyboard instantiateInitialViewController];
        
        // Instantiate a UIWindow object and initialize it with the screen size of the iOS device
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        
        // Set the initial view controller to be the root view controller of the window object
        self.window.rootViewController  = initialViewController;
        
        // Set the window object to be the key window and show it
        [self.window makeKeyAndVisible];
        
        
    }
}

@end
