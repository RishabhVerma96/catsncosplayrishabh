<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.1</string>
        <key>fileName</key>
        <string>/Users/danielhuynh/Desktop/Denis/projects/MagicQuest/Sources/tp/tower_fire.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../towers/fire/1/10001.png</key>
            <key type="filename">../towers/fire/1/10002.png</key>
            <key type="filename">../towers/fire/1/10003.png</key>
            <key type="filename">../towers/fire/1/10004.png</key>
            <key type="filename">../towers/fire/1/10005.png</key>
            <key type="filename">../towers/fire/1/10006.png</key>
            <key type="filename">../towers/fire/1/10007.png</key>
            <key type="filename">../towers/fire/1/10008.png</key>
            <key type="filename">../towers/fire/1/10009.png</key>
            <key type="filename">../towers/fire/1/10011.png</key>
            <key type="filename">../towers/fire/1/10012.png</key>
            <key type="filename">../towers/fire/1/10013.png</key>
            <key type="filename">../towers/fire/1/10014.png</key>
            <key type="filename">../towers/fire/1/10015.png</key>
            <key type="filename">../towers/fire/1/10016.png</key>
            <key type="filename">../towers/fire/1/10017.png</key>
            <key type="filename">../towers/fire/1/10018.png</key>
            <key type="filename">../towers/fire/1/idle/10001.png</key>
            <key type="filename">../towers/fire/1/idle/10002.png</key>
            <key type="filename">../towers/fire/1/idle/10003.png</key>
            <key type="filename">../towers/fire/1/idle/10004.png</key>
            <key type="filename">../towers/fire/1/idle/10005.png</key>
            <key type="filename">../towers/fire/1/idle/10006.png</key>
            <key type="filename">../towers/fire/1/idle/10007.png</key>
            <key type="filename">../towers/fire/1/idle/10008.png</key>
            <key type="filename">../towers/fire/1/idle/10009.png</key>
            <key type="filename">../towers/fire/1/idle/10011.png</key>
            <key type="filename">../towers/fire/1/idle/10012.png</key>
            <key type="filename">../towers/fire/1/idle/10013.png</key>
            <key type="filename">../towers/fire/1/idle/10014.png</key>
            <key type="filename">../towers/fire/1/idle/10015.png</key>
            <key type="filename">../towers/fire/1/idle/10016.png</key>
            <key type="filename">../towers/fire/1/idle/10017.png</key>
            <key type="filename">../towers/fire/1/idle/10018.png</key>
            <key type="filename">../towers/fire/1/static1.png</key>
            <key type="filename">../towers/fire/2/10001.png</key>
            <key type="filename">../towers/fire/2/10002.png</key>
            <key type="filename">../towers/fire/2/10003.png</key>
            <key type="filename">../towers/fire/2/10004.png</key>
            <key type="filename">../towers/fire/2/10005.png</key>
            <key type="filename">../towers/fire/2/10006.png</key>
            <key type="filename">../towers/fire/2/10007.png</key>
            <key type="filename">../towers/fire/2/10008.png</key>
            <key type="filename">../towers/fire/2/10009.png</key>
            <key type="filename">../towers/fire/2/10011.png</key>
            <key type="filename">../towers/fire/2/10012.png</key>
            <key type="filename">../towers/fire/2/10013.png</key>
            <key type="filename">../towers/fire/2/10014.png</key>
            <key type="filename">../towers/fire/2/10015.png</key>
            <key type="filename">../towers/fire/2/10016.png</key>
            <key type="filename">../towers/fire/2/10017.png</key>
            <key type="filename">../towers/fire/2/10018.png</key>
            <key type="filename">../towers/fire/2/idle/10001.png</key>
            <key type="filename">../towers/fire/2/idle/10002.png</key>
            <key type="filename">../towers/fire/2/idle/10003.png</key>
            <key type="filename">../towers/fire/2/idle/10004.png</key>
            <key type="filename">../towers/fire/2/idle/10005.png</key>
            <key type="filename">../towers/fire/2/idle/10006.png</key>
            <key type="filename">../towers/fire/2/idle/10007.png</key>
            <key type="filename">../towers/fire/2/idle/10008.png</key>
            <key type="filename">../towers/fire/2/idle/10009.png</key>
            <key type="filename">../towers/fire/2/idle/10011.png</key>
            <key type="filename">../towers/fire/2/idle/10012.png</key>
            <key type="filename">../towers/fire/2/idle/10013.png</key>
            <key type="filename">../towers/fire/2/idle/10014.png</key>
            <key type="filename">../towers/fire/2/idle/10015.png</key>
            <key type="filename">../towers/fire/2/idle/10016.png</key>
            <key type="filename">../towers/fire/2/idle/10017.png</key>
            <key type="filename">../towers/fire/2/idle/10018.png</key>
            <key type="filename">../towers/fire/2/static2.png</key>
            <key type="filename">../towers/fire/3/10001.png</key>
            <key type="filename">../towers/fire/3/10002.png</key>
            <key type="filename">../towers/fire/3/10003.png</key>
            <key type="filename">../towers/fire/3/10004.png</key>
            <key type="filename">../towers/fire/3/10005.png</key>
            <key type="filename">../towers/fire/3/10006.png</key>
            <key type="filename">../towers/fire/3/10007.png</key>
            <key type="filename">../towers/fire/3/10008.png</key>
            <key type="filename">../towers/fire/3/10009.png</key>
            <key type="filename">../towers/fire/3/10011.png</key>
            <key type="filename">../towers/fire/3/10012.png</key>
            <key type="filename">../towers/fire/3/10013.png</key>
            <key type="filename">../towers/fire/3/10014.png</key>
            <key type="filename">../towers/fire/3/10015.png</key>
            <key type="filename">../towers/fire/3/10016.png</key>
            <key type="filename">../towers/fire/3/10017.png</key>
            <key type="filename">../towers/fire/3/10018.png</key>
            <key type="filename">../towers/fire/3/idle/10001.png</key>
            <key type="filename">../towers/fire/3/idle/10002.png</key>
            <key type="filename">../towers/fire/3/idle/10003.png</key>
            <key type="filename">../towers/fire/3/idle/10004.png</key>
            <key type="filename">../towers/fire/3/idle/10005.png</key>
            <key type="filename">../towers/fire/3/idle/10006.png</key>
            <key type="filename">../towers/fire/3/idle/10007.png</key>
            <key type="filename">../towers/fire/3/idle/10008.png</key>
            <key type="filename">../towers/fire/3/idle/10009.png</key>
            <key type="filename">../towers/fire/3/idle/10011.png</key>
            <key type="filename">../towers/fire/3/idle/10012.png</key>
            <key type="filename">../towers/fire/3/idle/10013.png</key>
            <key type="filename">../towers/fire/3/idle/10014.png</key>
            <key type="filename">../towers/fire/3/idle/10015.png</key>
            <key type="filename">../towers/fire/3/idle/10016.png</key>
            <key type="filename">../towers/fire/3/idle/10017.png</key>
            <key type="filename">../towers/fire/3/idle/10018.png</key>
            <key type="filename">../towers/fire/3/static3.png</key>
            <key type="filename">../towers/fire/4/10001.png</key>
            <key type="filename">../towers/fire/4/10002.png</key>
            <key type="filename">../towers/fire/4/10003.png</key>
            <key type="filename">../towers/fire/4/10004.png</key>
            <key type="filename">../towers/fire/4/10005.png</key>
            <key type="filename">../towers/fire/4/10006.png</key>
            <key type="filename">../towers/fire/4/10007.png</key>
            <key type="filename">../towers/fire/4/10008.png</key>
            <key type="filename">../towers/fire/4/10009.png</key>
            <key type="filename">../towers/fire/4/10011.png</key>
            <key type="filename">../towers/fire/4/10012.png</key>
            <key type="filename">../towers/fire/4/10013.png</key>
            <key type="filename">../towers/fire/4/10014.png</key>
            <key type="filename">../towers/fire/4/10015.png</key>
            <key type="filename">../towers/fire/4/10016.png</key>
            <key type="filename">../towers/fire/4/10017.png</key>
            <key type="filename">../towers/fire/4/10018.png</key>
            <key type="filename">../towers/fire/4/idle/10001.png</key>
            <key type="filename">../towers/fire/4/idle/10002.png</key>
            <key type="filename">../towers/fire/4/idle/10003.png</key>
            <key type="filename">../towers/fire/4/idle/10004.png</key>
            <key type="filename">../towers/fire/4/idle/10005.png</key>
            <key type="filename">../towers/fire/4/idle/10006.png</key>
            <key type="filename">../towers/fire/4/idle/10007.png</key>
            <key type="filename">../towers/fire/4/idle/10008.png</key>
            <key type="filename">../towers/fire/4/idle/10009.png</key>
            <key type="filename">../towers/fire/4/idle/10011.png</key>
            <key type="filename">../towers/fire/4/idle/10012.png</key>
            <key type="filename">../towers/fire/4/idle/10013.png</key>
            <key type="filename">../towers/fire/4/idle/10014.png</key>
            <key type="filename">../towers/fire/4/idle/10015.png</key>
            <key type="filename">../towers/fire/4/idle/10016.png</key>
            <key type="filename">../towers/fire/4/idle/10017.png</key>
            <key type="filename">../towers/fire/4/idle/10018.png</key>
            <key type="filename">../towers/fire/4/static4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,50,100,100</rect>
                <key>scale9Paddings</key>
                <rect>50,50,100,100</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../towers/fire/1/drone1/idle/10001.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10002.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10003.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10004.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10005.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10006.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10007.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10008.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10009.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10010.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10011.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10012.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10013.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10014.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10015.png</key>
            <key type="filename">../towers/fire/1/drone1/idle/10016.png</key>
            <key type="filename">../towers/fire/1/drone1/shoot/10001.png</key>
            <key type="filename">../towers/fire/1/drone1/shoot/10002.png</key>
            <key type="filename">../towers/fire/1/drone1/shoot/10003.png</key>
            <key type="filename">../towers/fire/1/drone1/shoot/10004.png</key>
            <key type="filename">../towers/fire/1/drone1/shoot/10005.png</key>
            <key type="filename">../towers/fire/1/drone1/shoot/10006.png</key>
            <key type="filename">../towers/fire/1/drone1/shoot/10007.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10001.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10002.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10003.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10004.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10005.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10006.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10007.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10008.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10009.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10010.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10011.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10012.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10013.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10014.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10015.png</key>
            <key type="filename">../towers/fire/2/drone2/idle/10016.png</key>
            <key type="filename">../towers/fire/2/drone2/shoot/10001.png</key>
            <key type="filename">../towers/fire/2/drone2/shoot/10002.png</key>
            <key type="filename">../towers/fire/2/drone2/shoot/10003.png</key>
            <key type="filename">../towers/fire/2/drone2/shoot/10004.png</key>
            <key type="filename">../towers/fire/2/drone2/shoot/10005.png</key>
            <key type="filename">../towers/fire/2/drone2/shoot/10006.png</key>
            <key type="filename">../towers/fire/2/drone2/shoot/10007.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10001.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10002.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10003.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10004.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10005.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10006.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10007.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10008.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10009.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10010.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10011.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10012.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10013.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10014.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10015.png</key>
            <key type="filename">../towers/fire/3/drone3/idle/10016.png</key>
            <key type="filename">../towers/fire/3/drone3/shoot/10001.png</key>
            <key type="filename">../towers/fire/3/drone3/shoot/10002.png</key>
            <key type="filename">../towers/fire/3/drone3/shoot/10003.png</key>
            <key type="filename">../towers/fire/3/drone3/shoot/10004.png</key>
            <key type="filename">../towers/fire/3/drone3/shoot/10005.png</key>
            <key type="filename">../towers/fire/3/drone3/shoot/10006.png</key>
            <key type="filename">../towers/fire/3/drone3/shoot/10007.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10001.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10002.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10003.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10004.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10005.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10006.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10007.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10008.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10009.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10010.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10011.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10012.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10013.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10014.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10015.png</key>
            <key type="filename">../towers/fire/4/drone4/idle/10016.png</key>
            <key type="filename">../towers/fire/4/drone4/shoot/10001.png</key>
            <key type="filename">../towers/fire/4/drone4/shoot/10002.png</key>
            <key type="filename">../towers/fire/4/drone4/shoot/10003.png</key>
            <key type="filename">../towers/fire/4/drone4/shoot/10004.png</key>
            <key type="filename">../towers/fire/4/drone4/shoot/10005.png</key>
            <key type="filename">../towers/fire/4/drone4/shoot/10006.png</key>
            <key type="filename">../towers/fire/4/drone4/shoot/10007.png</key>
            <key type="filename">../towers/fire/static.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,38,75,75</rect>
                <key>scale9Paddings</key>
                <rect>38,38,75,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../towers/fire/1/missile/10001.png</key>
            <key type="filename">../towers/fire/1/missile/10002.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../towers/fire/explow/10001.png</key>
            <key type="filename">../towers/fire/explow/10002.png</key>
            <key type="filename">../towers/fire/explow/10003.png</key>
            <key type="filename">../towers/fire/explow/10004.png</key>
            <key type="filename">../towers/fire/explow/10005.png</key>
            <key type="filename">../towers/fire/explow/10006.png</key>
            <key type="filename">../towers/fire/explow/10007.png</key>
            <key type="filename">../towers/fire/explow/10008.png</key>
            <key type="filename">../towers/fire/explow/10009.png</key>
            <key type="filename">../towers/fire/explow/10010.png</key>
            <key type="filename">../towers/fire/explow/10011.png</key>
            <key type="filename">../towers/fire/explow/10012.png</key>
            <key type="filename">../towers/fire/explow/10013.png</key>
            <key type="filename">../towers/fire/explow/10014.png</key>
            <key type="filename">../towers/fire/explow/10015.png</key>
            <key type="filename">../towers/fire/explow/10016.png</key>
            <key type="filename">../towers/fire/explow/10017.png</key>
            <key type="filename">../towers/fire/explow/10018.png</key>
            <key type="filename">../towers/fire/explow/10019.png</key>
            <key type="filename">../towers/fire/explow/10020.png</key>
            <key type="filename">../towers/fire/explow/10021.png</key>
            <key type="filename">../towers/fire/explow/10022.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,39,100,78</rect>
                <key>scale9Paddings</key>
                <rect>50,39,100,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../towers/fire/fire/10001.png</key>
            <key type="filename">../towers/fire/fire/10002.png</key>
            <key type="filename">../towers/fire/fire/10003.png</key>
            <key type="filename">../towers/fire/fire/10004.png</key>
            <key type="filename">../towers/fire/fire/10005.png</key>
            <key type="filename">../towers/fire/fire/10006.png</key>
            <key type="filename">../towers/fire/fire/10007.png</key>
            <key type="filename">../towers/fire/fire/10008.png</key>
            <key type="filename">../towers/fire/fire/10009.png</key>
            <key type="filename">../towers/fire/fire/10010.png</key>
            <key type="filename">../towers/fire/fire/10011.png</key>
            <key type="filename">../towers/fire/fire/10012.png</key>
            <key type="filename">../towers/fire/fire/10013.png</key>
            <key type="filename">../towers/fire/fire/10014.png</key>
            <key type="filename">../towers/fire/fire/10015.png</key>
            <key type="filename">../towers/fire/fire/10016.png</key>
            <key type="filename">../towers/fire/fire/10017.psd</key>
            <key type="filename">../towers/fire/fire/10018.png</key>
            <key type="filename">../towers/fire/fire/10019.png</key>
            <key type="filename">../towers/fire/fire/10020.png</key>
            <key type="filename">../towers/fire/fire/10021.png</key>
            <key type="filename">../towers/fire/fire/10022.png</key>
            <key type="filename">../towers/fire/fire/10023.png</key>
            <key type="filename">../towers/fire/fire/10024.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,25,30,50</rect>
                <key>scale9Paddings</key>
                <rect>15,25,30,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../towers/fire</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
