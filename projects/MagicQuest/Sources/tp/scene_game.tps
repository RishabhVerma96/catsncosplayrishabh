<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.3.3</string>
        <key>fileName</key>
        <string>/Users/danielhuynh/Desktop/2016.10.27_MagicQuest_src/projects/MagicQuest/Sources/tp/scene_game.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../scenes/choose/button_fuel.png</key>
            <key type="filename">../scenes/choose/gear_for_diff.png</key>
            <key type="filename">../scenes/choose/heals_for_diff.png</key>
            <key type="filename">../scenes/choose/star_for_diff.png</key>
            <key type="filename">../scenes/choose/wave_for_diff.png</key>
            <key type="filename">../scenes/choose/x_for_diff.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/choose/button_tobattle.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,18,100,35</rect>
                <key>scale9Paddings</key>
                <rect>50,18,100,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/choose/coin_diff.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,13,13</rect>
                <key>scale9Paddings</key>
                <rect>6,6,13,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/choose/lock2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,31,50,61</rect>
                <key>scale9Paddings</key>
                <rect>25,31,50,61</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/choose/win_for_level2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>73,92,147,185</rect>
                <key>scale9Paddings</key>
                <rect>73,92,147,185</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/choose/win_hard_diff.png</key>
            <key type="filename">../scenes/choose/win_normal_diff.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>70,106,139,213</rect>
                <key>scale9Paddings</key>
                <rect>70,106,139,213</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/_icon_gold1.png</key>
            <key type="filename">../scenes/gamescene/icon_gold1.png</key>
            <key type="filename">../scenes/gamescene/icon_lifes.png</key>
            <key type="filename">../scenes/gamescene/icon_wave1.png</key>
            <key type="filename">../scenes/gamescene/victory/victory_coin.png</key>
            <key type="filename">../scenes/gamescene/victory/victory_cristal.png</key>
            <key type="filename">../scenes/gamescene/victory_cristal.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,20</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/active_slot.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,13,36,25</rect>
                <key>scale9Paddings</key>
                <rect>18,13,36,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_01.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_02.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_03.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_04.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_05.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_06.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_07.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_08.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_09.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_10.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_11.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_12.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_13.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_14.png</key>
            <key type="filename">../scenes/gamescene/active_slot/active_slot_15.png</key>
            <key type="filename">../scenes/gamescene/unactive_slot.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,50,100,100</rect>
                <key>scale9Paddings</key>
                <rect>50,50,100,100</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/box_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,17,169,34</rect>
                <key>scale9Paddings</key>
                <rect>84,17,169,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/box_close.png</key>
            <key type="filename">../scenes/gamescene/box_open.png</key>
            <key type="filename">../scenes/gamescene/button_desant_1.png</key>
            <key type="filename">../scenes/gamescene/button_desant_1_1.png</key>
            <key type="filename">../scenes/gamescene/button_desant_1_3.png</key>
            <key type="filename">../scenes/gamescene/button_desant_2.png</key>
            <key type="filename">../scenes/gamescene/button_desant_2_1.png</key>
            <key type="filename">../scenes/gamescene/button_desant_2_3.png</key>
            <key type="filename">../scenes/gamescene/button_itemshop.png</key>
            <key type="filename">../scenes/gamescene/button_landmine_1.png</key>
            <key type="filename">../scenes/gamescene/button_landmine_2.png</key>
            <key type="filename">../scenes/gamescene/button_landmine_3.png</key>
            <key type="filename">../scenes/gamescene/button_shop.png</key>
            <key type="filename">../scenes/gamescene/icon_pause.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,23,46,46</rect>
                <key>scale9Paddings</key>
                <rect>23,23,46,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/circle.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>128,128,256,256</rect>
                <key>scale9Paddings</key>
                <rect>128,128,256,256</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/defeat/defeat1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>138,110,277,220</rect>
                <key>scale9Paddings</key>
                <rect>138,110,277,220</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/defeat/defeat_quit.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,21,92,43</rect>
                <key>scale9Paddings</key>
                <rect>46,21,92,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/defeat/defeat_restart.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,20,90,41</rect>
                <key>scale9Paddings</key>
                <rect>45,20,90,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/empty_touch.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/hero1_1.png</key>
            <key type="filename">../scenes/gamescene/hero1_2.png</key>
            <key type="filename">../scenes/gamescene/hero1_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,23,43,45</rect>
                <key>scale9Paddings</key>
                <rect>22,23,43,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/hero_progressbar1.png</key>
            <key type="filename">../scenes/gamescene/hero_progressbar2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,3,32,6</rect>
                <key>scale9Paddings</key>
                <rect>16,3,32,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/icon_x.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/icon_x_10.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,3,5,5</rect>
                <key>scale9Paddings</key>
                <rect>3,3,5,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/item1.png</key>
            <key type="filename">../scenes/gamescene/item1_d.png</key>
            <key type="filename">../scenes/gamescene/item2.png</key>
            <key type="filename">../scenes/gamescene/item2_d.png</key>
            <key type="filename">../scenes/gamescene/item3.png</key>
            <key type="filename">../scenes/gamescene/item3_d.png</key>
            <key type="filename">../scenes/gamescene/pause/button_x.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,17,34,34</rect>
                <key>scale9Paddings</key>
                <rect>17,17,34,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/menucreatetower/hint_panel.png</key>
            <key type="filename">../scenes/gamescene/menutower/hint_panel.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>62,62,123,125</rect>
                <key>scale9Paddings</key>
                <rect>62,62,123,125</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_lock2.png</key>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_ok1.png</key>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_tower_fire1.png</key>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_tower_fire2.png</key>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_tower_gun1.png</key>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_tower_gun2.png</key>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_tower_ice1.png</key>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_tower_ice2.png</key>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_tower_tesla1.png</key>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_tower_tesla2.png</key>
            <key type="filename">../scenes/gamescene/menudig/menu_dig1.png</key>
            <key type="filename">../scenes/gamescene/menudig/menu_dig2.png</key>
            <key type="filename">../scenes/gamescene/menudig/menu_ok1.png</key>
            <key type="filename">../scenes/gamescene/menudig/menu_ok2.png</key>
            <key type="filename">../scenes/gamescene/menutower/menu_lock2.png</key>
            <key type="filename">../scenes/gamescene/menutower/menu_ok1.png</key>
            <key type="filename">../scenes/gamescene/menutower/menu_upgrade1.png</key>
            <key type="filename">../scenes/gamescene/menutower/menu_upgrade2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,20,39,41</rect>
                <key>scale9Paddings</key>
                <rect>19,20,39,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_mainpanel1.png</key>
            <key type="filename">../scenes/gamescene/menudig/menu_digpanel1.png</key>
            <key type="filename">../scenes/gamescene/menutower/menu_upgrade0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>59,49,118,99</rect>
                <key>scale9Paddings</key>
                <rect>59,49,118,99</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_ok2.png</key>
            <key type="filename">../scenes/gamescene/menutower/menu_ok2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,15,31,30</rect>
                <key>scale9Paddings</key>
                <rect>15,15,31,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_options.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>62,19,123,38</rect>
                <key>scale9Paddings</key>
                <rect>62,19,123,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/menucreatetower/menu_price.png</key>
            <key type="filename">../scenes/gamescene/menudig/menu_price.png</key>
            <key type="filename">../scenes/gamescene/menutower/menu_price.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,8,28,17</rect>
                <key>scale9Paddings</key>
                <rect>14,8,28,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/menutower/menu_sell1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,23,39,45</rect>
                <key>scale9Paddings</key>
                <rect>19,23,39,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/pause/button_music.png</key>
            <key type="filename">../scenes/gamescene/pause/button_music2.png</key>
            <key type="filename">../scenes/gamescene/pause/button_shop.png</key>
            <key type="filename">../scenes/gamescene/pause/button_sound1.png</key>
            <key type="filename">../scenes/gamescene/pause/button_sound2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,50,50</rect>
                <key>scale9Paddings</key>
                <rect>25,25,50,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/pause/button_quit.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>47,22,93,44</rect>
                <key>scale9Paddings</key>
                <rect>47,22,93,44</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/pause/button_restart.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>47,22,94,43</rect>
                <key>scale9Paddings</key>
                <rect>47,22,94,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/pause/pause_window.png</key>
            <key type="filename">../scenes/gamescene/victory/victory_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>138,112,277,224</rect>
                <key>scale9Paddings</key>
                <rect>138,112,277,224</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/victory/separator.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>70,4,139,8</rect>
                <key>scale9Paddings</key>
                <rect>70,4,139,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/victory/victory_cont.png</key>
            <key type="filename">../scenes/gamescene/victory/victory_restart.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>47,22,94,44</rect>
                <key>scale9Paddings</key>
                <rect>47,22,94,44</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/victory/victory_star.png</key>
            <key type="filename">../scenes/gamescene/victory/victory_star2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>23,23,47,45</rect>
                <key>scale9Paddings</key>
                <rect>23,23,47,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/wave_direction0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,17,51,33</rect>
                <key>scale9Paddings</key>
                <rect>25,17,51,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/wave_direction1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,17,33,33</rect>
                <key>scale9Paddings</key>
                <rect>17,17,33,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/wave_direction2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,10,17</rect>
                <key>scale9Paddings</key>
                <rect>5,8,10,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/gamescene/wave_direction3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,18,35,35</rect>
                <key>scale9Paddings</key>
                <rect>18,18,35,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/ads_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>60,23,120,45</rect>
                <key>scale9Paddings</key>
                <rect>60,23,120,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/back.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>342,166,683,332</rect>
                <key>scale9Paddings</key>
                <rect>342,166,683,332</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/close_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,19,36,38</rect>
                <key>scale9Paddings</key>
                <rect>18,19,36,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero1/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero1/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero1/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero1/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero1/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero10/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero10/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero10/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero10/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero10/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero2/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero2/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero2/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero2/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero2/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero3/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero3/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero3/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero3/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero3/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero4/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero4/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero4/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero4/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero4/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero5/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero5/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero5/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero5/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero5/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero6/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero6/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero6/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero6/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero6/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero7/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero7/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero7/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero7/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero7/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero8/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero8/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero8/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero8/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero9/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero9/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero9/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero9/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero9/5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,15,31,31</rect>
                <key>scale9Paddings</key>
                <rect>16,15,31,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero8/5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,15,30,30</rect>
                <key>scale9Paddings</key>
                <rect>15,15,30,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/exp_back.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>73,8,146,16</rect>
                <key>scale9Paddings</key>
                <rect>73,8,146,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/exp_progress.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>73,8,145,16</rect>
                <key>scale9Paddings</key>
                <rect>73,8,145,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/header_back.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>310,54,619,108</rect>
                <key>scale9Paddings</key>
                <rect>310,54,619,108</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/icon_gold.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,14,14</rect>
                <key>scale9Paddings</key>
                <rect>7,7,14,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/icon_heroes/0.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero1.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero10.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero4.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero5.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero6.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero7.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero8.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>87,133,174,266</rect>
                <key>scale9Paddings</key>
                <rect>87,133,174,266</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/icon_heroes/hero2.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>87,133,175,266</rect>
                <key>scale9Paddings</key>
                <rect>87,133,175,266</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/icons_skills/hero1/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero1/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero1/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero1/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero1/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero10/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero10/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero10/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero10/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero10/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero2/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero2/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero2/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero2/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero2/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero3/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero3/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero3/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero3/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero3/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero4/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero4/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero4/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero4/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero4/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero5/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero5/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero5/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero5/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero5/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero6/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero6/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero6/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero6/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero6/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero7/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero7/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero7/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero7/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero7/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero8/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero8/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero8/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero8/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero8/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero9/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero9/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero9/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero9/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero9/5.png</key>
            <key type="filename">../scenes/heroroom/ok_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,18,41,36</rect>
                <key>scale9Paddings</key>
                <rect>21,18,41,36</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/in_squad.png</key>
            <key type="filename">../scenes/heroroom/out_squad.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,14,16</rect>
                <key>scale9Paddings</key>
                <rect>7,8,14,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/info_back.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>216,136,431,272</rect>
                <key>scale9Paddings</key>
                <rect>216,136,431,272</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/levelup_button.png</key>
            <key type="filename">../scenes/heroroom/levelup_button_disabled.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>97,24,195,47</rect>
                <key>scale9Paddings</key>
                <rect>97,24,195,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/reset_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,24,47,47</rect>
                <key>scale9Paddings</key>
                <rect>24,24,47,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/skill_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,33,59,66</rect>
                <key>scale9Paddings</key>
                <rect>30,33,59,66</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/switch_ball.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,11,18,22</rect>
                <key>scale9Paddings</key>
                <rect>9,11,18,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/switch_off.png</key>
            <key type="filename">../scenes/heroroom/switch_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,13,84,26</rect>
                <key>scale9Paddings</key>
                <rect>42,13,84,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../scenes</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
