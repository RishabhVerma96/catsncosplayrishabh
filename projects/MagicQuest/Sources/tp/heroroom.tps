<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.1</string>
        <key>fileName</key>
        <string>/Users/danielhuynh/Desktop/Denis/projects/MagicQuest/Sources/tp/heroroom.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../scenes/heroroom/ads_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>60,23,119,45</rect>
                <key>scale9Paddings</key>
                <rect>60,23,119,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/back.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>342,166,683,331</rect>
                <key>scale9Paddings</key>
                <rect>342,166,683,331</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/close_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,19,36,37</rect>
                <key>scale9Paddings</key>
                <rect>18,19,36,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero1/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero1/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero1/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero1/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero1/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero10/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero10/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero10/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero10/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero10/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero2/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero2/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero2/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero2/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero2/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero3/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero3/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero3/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero3/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero3/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero4/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero4/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero4/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero4/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero4/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero5/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero5/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero5/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero5/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero5/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero6/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero6/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero6/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero6/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero6/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero7/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero7/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero7/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero7/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero7/5.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero8/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero8/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero8/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero8/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero9/1.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero9/2.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero9/3.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero9/4.png</key>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero9/5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,15,31,31</rect>
                <key>scale9Paddings</key>
                <rect>16,15,31,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/desc_icon_skills/hero8/5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,15,30,30</rect>
                <key>scale9Paddings</key>
                <rect>15,15,30,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/exp_back.png</key>
            <key type="filename">../scenes/heroroom/exp_progress.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>73,8,145,16</rect>
                <key>scale9Paddings</key>
                <rect>73,8,145,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/header_back.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>310,54,619,108</rect>
                <key>scale9Paddings</key>
                <rect>310,54,619,108</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/icon_gold.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,14,13</rect>
                <key>scale9Paddings</key>
                <rect>7,7,14,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/icon_heroes/0.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero1.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero10.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero4.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero5.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero6.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero7.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero8.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>87,133,174,266</rect>
                <key>scale9Paddings</key>
                <rect>87,133,174,266</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/icon_heroes/hero2.png</key>
            <key type="filename">../scenes/heroroom/icon_heroes/hero3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>87,133,175,266</rect>
                <key>scale9Paddings</key>
                <rect>87,133,175,266</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/icons_skills/hero1/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero1/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero1/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero1/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero1/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero10/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero10/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero10/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero10/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero10/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero2/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero2/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero2/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero2/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero2/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero3/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero3/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero3/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero3/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero3/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero4/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero4/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero4/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero4/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero4/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero5/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero5/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero5/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero5/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero5/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero6/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero6/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero6/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero6/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero6/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero7/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero7/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero7/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero7/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero7/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero8/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero8/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero8/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero8/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero8/5.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero9/1.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero9/2.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero9/3.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero9/4.png</key>
            <key type="filename">../scenes/heroroom/icons_skills/hero9/5.png</key>
            <key type="filename">../scenes/heroroom/ok_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,18,41,35</rect>
                <key>scale9Paddings</key>
                <rect>21,18,41,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/in_squad.png</key>
            <key type="filename">../scenes/heroroom/out_squad.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,14,15</rect>
                <key>scale9Paddings</key>
                <rect>7,8,14,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/info_back.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>216,136,431,272</rect>
                <key>scale9Paddings</key>
                <rect>216,136,431,272</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/levelup_button.png</key>
            <key type="filename">../scenes/heroroom/levelup_button_disabled.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>97,24,195,47</rect>
                <key>scale9Paddings</key>
                <rect>97,24,195,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/reset_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,24,47,47</rect>
                <key>scale9Paddings</key>
                <rect>24,24,47,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/skill_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,33,59,66</rect>
                <key>scale9Paddings</key>
                <rect>30,33,59,66</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/switch_ball.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,11,18,21</rect>
                <key>scale9Paddings</key>
                <rect>9,11,18,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../scenes/heroroom/switch_off.png</key>
            <key type="filename">../scenes/heroroom/switch_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,13,83,26</rect>
                <key>scale9Paddings</key>
                <rect>42,13,83,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../scenes/heroroom</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
