<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.5.0</string>
        <key>fileName</key>
        <string>/Users/danielhuynh/Desktop/magicquest/projects/MagicQuest/Sources/tp/deocrations.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../decorations/aligator/aligator_0001.png</key>
            <key type="filename">../decorations/aligator/aligator_0002.png</key>
            <key type="filename">../decorations/aligator/aligator_0003.png</key>
            <key type="filename">../decorations/aligator/aligator_0004.png</key>
            <key type="filename">../decorations/aligator/aligator_0005.png</key>
            <key type="filename">../decorations/aligator/aligator_0006.png</key>
            <key type="filename">../decorations/aligator/aligator_0007.png</key>
            <key type="filename">../decorations/aligator/aligator_0008.png</key>
            <key type="filename">../decorations/aligator/aligator_0009.png</key>
            <key type="filename">../decorations/aligator/aligator_0010.png</key>
            <key type="filename">../decorations/aligator/aligator_0011.png</key>
            <key type="filename">../decorations/aligator/aligator_0012.png</key>
            <key type="filename">../decorations/aligator/aligator_0013.png</key>
            <key type="filename">../decorations/aligator/aligator_0014.png</key>
            <key type="filename">../decorations/aligator/aligator_0015.png</key>
            <key type="filename">../decorations/aligator/aligator_0016.png</key>
            <key type="filename">../decorations/aligator/aligator_0017.png</key>
            <key type="filename">../decorations/aligator/aligator_0018.png</key>
            <key type="filename">../decorations/aligator/aligator_0019.png</key>
            <key type="filename">../decorations/aligator/aligator_0020.png</key>
            <key type="filename">../decorations/aligator/aligator_0021.png</key>
            <key type="filename">../decorations/aligator/aligator_0022.png</key>
            <key type="filename">../decorations/aligator/aligator_0023.png</key>
            <key type="filename">../decorations/aligator/aligator_0024.png</key>
            <key type="filename">../decorations/aligator/aligator_0025.png</key>
            <key type="filename">../decorations/aligator/aligator_0026.png</key>
            <key type="filename">../decorations/aligator/aligator_0027.png</key>
            <key type="filename">../decorations/aligator/aligator_0028.png</key>
            <key type="filename">../decorations/aligator/aligator_0029.png</key>
            <key type="filename">../decorations/aligator/aligator_0030.png</key>
            <key type="filename">../decorations/aligator/aligator_0031.png</key>
            <key type="filename">../decorations/aligator/aligator_0032.png</key>
            <key type="filename">../decorations/aligator/aligator_0033.png</key>
            <key type="filename">../decorations/aligator/aligator_0034.png</key>
            <key type="filename">../decorations/aligator/aligator_0035.png</key>
            <key type="filename">../decorations/aligator/aligator_0036.png</key>
            <key type="filename">../decorations/aligator/aligator_0037.png</key>
            <key type="filename">../decorations/aligator/aligator_0038.png</key>
            <key type="filename">../decorations/aligator/aligator_0039.png</key>
            <key type="filename">../decorations/aligator/aligator_0040.png</key>
            <key type="filename">../decorations/aligator/aligator_0041.png</key>
            <key type="filename">../decorations/aligator/aligator_0042.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,7,26,14</rect>
                <key>scale9Paddings</key>
                <rect>13,7,26,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/base_icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,50,50</rect>
                <key>scale9Paddings</key>
                <rect>25,25,50,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/bird/bird/bird_0001.png</key>
            <key type="filename">../decorations/bird/bird/bird_0002.png</key>
            <key type="filename">../decorations/bird/bird/bird_0003.png</key>
            <key type="filename">../decorations/bird/bird/bird_0004.png</key>
            <key type="filename">../decorations/bird/bird/bird_0005.png</key>
            <key type="filename">../decorations/bird/bird/bird_0006.png</key>
            <key type="filename">../decorations/bird/bird/bird_0007.png</key>
            <key type="filename">../decorations/bird/bird/bird_0008.png</key>
            <key type="filename">../decorations/bird/bird/bird_0009.png</key>
            <key type="filename">../decorations/bird/bird/bird_0010.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,13,22,27</rect>
                <key>scale9Paddings</key>
                <rect>11,13,22,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/bird/shadow/bird_shadow_0001.png</key>
            <key type="filename">../decorations/bird/shadow/bird_shadow_0002.png</key>
            <key type="filename">../decorations/bird/shadow/bird_shadow_0003.png</key>
            <key type="filename">../decorations/bird/shadow/bird_shadow_0004.png</key>
            <key type="filename">../decorations/bird/shadow/bird_shadow_0005.png</key>
            <key type="filename">../decorations/bird/shadow/bird_shadow_0006.png</key>
            <key type="filename">../decorations/bird/shadow/bird_shadow_0007.png</key>
            <key type="filename">../decorations/bird/shadow/bird_shadow_0008.png</key>
            <key type="filename">../decorations/bird/shadow/bird_shadow_0009.png</key>
            <key type="filename">../decorations/bird/shadow/bird_shadow_0010.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,7,37,14</rect>
                <key>scale9Paddings</key>
                <rect>18,7,37,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/blackhole/g1_0001.png</key>
            <key type="filename">../decorations/blackhole/g1_0002.png</key>
            <key type="filename">../decorations/blackhole/g1_0003.png</key>
            <key type="filename">../decorations/fountain/g1_0001.png</key>
            <key type="filename">../decorations/fountain/g1_0002.png</key>
            <key type="filename">../decorations/fountain/g1_0003.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,38,75,75</rect>
                <key>scale9Paddings</key>
                <rect>38,38,75,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/cave.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>600,375,1200,750</rect>
                <key>scale9Paddings</key>
                <rect>600,375,1200,750</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/enot/enot_0001.png</key>
            <key type="filename">../decorations/enot/enot_0002.png</key>
            <key type="filename">../decorations/enot/enot_0003.png</key>
            <key type="filename">../decorations/enot/enot_0004.png</key>
            <key type="filename">../decorations/enot/enot_0005.png</key>
            <key type="filename">../decorations/enot/enot_0006.png</key>
            <key type="filename">../decorations/enot/enot_0007.png</key>
            <key type="filename">../decorations/enot/enot_0008.png</key>
            <key type="filename">../decorations/enot/enot_0009.png</key>
            <key type="filename">../decorations/enot/enot_0010.png</key>
            <key type="filename">../decorations/enot/enot_0011.png</key>
            <key type="filename">../decorations/enot/enot_0012.png</key>
            <key type="filename">../decorations/enot/enot_0013.png</key>
            <key type="filename">../decorations/enot/enot_0014.png</key>
            <key type="filename">../decorations/enot/enot_0015.png</key>
            <key type="filename">../decorations/enot/enot_0016.png</key>
            <key type="filename">../decorations/enot/enot_0017.png</key>
            <key type="filename">../decorations/enot/enot_0018.png</key>
            <key type="filename">../decorations/enot/enot_0019.png</key>
            <key type="filename">../decorations/enot/enot_0020.png</key>
            <key type="filename">../decorations/enot/enot_0021.png</key>
            <key type="filename">../decorations/enot/enot_0022.png</key>
            <key type="filename">../decorations/enot/enot_0023.png</key>
            <key type="filename">../decorations/enot/enot_0024.png</key>
            <key type="filename">../decorations/enot/enot_0025.png</key>
            <key type="filename">../decorations/enot/enot_0026.png</key>
            <key type="filename">../decorations/enot/enot_0027.png</key>
            <key type="filename">../decorations/enot/enot_0028.png</key>
            <key type="filename">../decorations/enot/enot_0029.png</key>
            <key type="filename">../decorations/enot/enot_0030.png</key>
            <key type="filename">../decorations/enot/enot_0031.png</key>
            <key type="filename">../decorations/enot/enot_0032.png</key>
            <key type="filename">../decorations/enot/enot_0033.png</key>
            <key type="filename">../decorations/enot/enot_0034.png</key>
            <key type="filename">../decorations/enot/enot_0035.png</key>
            <key type="filename">../decorations/enot/enot_0036.png</key>
            <key type="filename">../decorations/enot/enot_0037.png</key>
            <key type="filename">../decorations/enot/enot_0038.png</key>
            <key type="filename">../decorations/enot/enot_0039.png</key>
            <key type="filename">../decorations/enot/enot_0040.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,34,42,69</rect>
                <key>scale9Paddings</key>
                <rect>21,34,42,69</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/flag.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,29,51,58</rect>
                <key>scale9Paddings</key>
                <rect>25,29,51,58</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/flower1/g1_0001.png</key>
            <key type="filename">../decorations/flower1/g1_0002.png</key>
            <key type="filename">../decorations/flower1/g1_0003.png</key>
            <key type="filename">../decorations/flower1/g1_0004.png</key>
            <key type="filename">../decorations/flower1/g1_0005.png</key>
            <key type="filename">../decorations/flower1/g1_0006.png</key>
            <key type="filename">../decorations/flower1/g1_0007.png</key>
            <key type="filename">../decorations/flower1/g1_0008.png</key>
            <key type="filename">../decorations/flower1/g1_0009.png</key>
            <key type="filename">../decorations/flower2/g1_0001.png</key>
            <key type="filename">../decorations/flower2/g1_0002.png</key>
            <key type="filename">../decorations/flower2/g1_0003.png</key>
            <key type="filename">../decorations/flower2/g1_0004.png</key>
            <key type="filename">../decorations/flower2/g1_0005.png</key>
            <key type="filename">../decorations/flower2/g1_0006.png</key>
            <key type="filename">../decorations/flower2/g1_0007.png</key>
            <key type="filename">../decorations/flower2/g1_0008.png</key>
            <key type="filename">../decorations/flower2/g1_0009.png</key>
            <key type="filename">../decorations/flower3/g1_0001.png</key>
            <key type="filename">../decorations/flower3/g1_0002.png</key>
            <key type="filename">../decorations/flower3/g1_0003.png</key>
            <key type="filename">../decorations/flower3/g1_0004.png</key>
            <key type="filename">../decorations/flower3/g1_0005.png</key>
            <key type="filename">../decorations/flower3/g1_0006.png</key>
            <key type="filename">../decorations/flower3/g1_0007.png</key>
            <key type="filename">../decorations/flower3/g1_0008.png</key>
            <key type="filename">../decorations/flower3/g1_0009.png</key>
            <key type="filename">../decorations/flower4/g1_0001.png</key>
            <key type="filename">../decorations/flower4/g1_0002.png</key>
            <key type="filename">../decorations/flower4/g1_0003.png</key>
            <key type="filename">../decorations/flower4/g1_0004.png</key>
            <key type="filename">../decorations/flower4/g1_0005.png</key>
            <key type="filename">../decorations/flower4/g1_0006.png</key>
            <key type="filename">../decorations/flower4/g1_0007.png</key>
            <key type="filename">../decorations/flower4/g1_0008.png</key>
            <key type="filename">../decorations/flower4/g1_0009.png</key>
            <key type="filename">../decorations/weed1/g1_0001.png</key>
            <key type="filename">../decorations/weed1/g1_0002.png</key>
            <key type="filename">../decorations/weed1/g1_0003.png</key>
            <key type="filename">../decorations/weed1/g1_0004.png</key>
            <key type="filename">../decorations/weed1/g1_0005.png</key>
            <key type="filename">../decorations/weed1/g1_0006.png</key>
            <key type="filename">../decorations/weed1/g1_0007.png</key>
            <key type="filename">../decorations/weed1/g1_0008.png</key>
            <key type="filename">../decorations/weed2/g1_0001.png</key>
            <key type="filename">../decorations/weed2/g1_0002.png</key>
            <key type="filename">../decorations/weed2/g1_0003.png</key>
            <key type="filename">../decorations/weed2/g1_0004.png</key>
            <key type="filename">../decorations/weed2/g1_0005.png</key>
            <key type="filename">../decorations/weed2/g1_0006.png</key>
            <key type="filename">../decorations/weed2/g1_0007.png</key>
            <key type="filename">../decorations/weed2/g1_0008.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,25,25</rect>
                <key>scale9Paddings</key>
                <rect>13,13,25,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/flower5/g1_0001.png</key>
            <key type="filename">../decorations/flower5/g1_0002.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,20</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/fox1/10001.png</key>
            <key type="filename">../decorations/fox1/10002.png</key>
            <key type="filename">../decorations/fox1/10003.png</key>
            <key type="filename">../decorations/fox1/10004.png</key>
            <key type="filename">../decorations/fox1/10005.png</key>
            <key type="filename">../decorations/snowman/10000.png</key>
            <key type="filename">../decorations/snowman/10001.png</key>
            <key type="filename">../decorations/snowman/10002.png</key>
            <key type="filename">../decorations/snowman/10003.png</key>
            <key type="filename">../decorations/snowman/10004.png</key>
            <key type="filename">../decorations/snowman/10005.png</key>
            <key type="filename">../decorations/snowman/10006.png</key>
            <key type="filename">../decorations/snowman/10007.png</key>
            <key type="filename">../decorations/snowman/10008.png</key>
            <key type="filename">../decorations/snowman/10009.png</key>
            <key type="filename">../decorations/snowman/10010.png</key>
            <key type="filename">../decorations/snowman/10011.png</key>
            <key type="filename">../decorations/snowman/10012.png</key>
            <key type="filename">../decorations/snowman/10013.png</key>
            <key type="filename">../decorations/snowman/10014.png</key>
            <key type="filename">../decorations/snowman/10015.png</key>
            <key type="filename">../decorations/snowman/10016.png</key>
            <key type="filename">../decorations/snowman/10017.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,75,150,150</rect>
                <key>scale9Paddings</key>
                <rect>75,75,150,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/grass/g1_0001.png</key>
            <key type="filename">../decorations/grass/g1_0002.png</key>
            <key type="filename">../decorations/grass/g1_0003.png</key>
            <key type="filename">../decorations/grass/g1_0004.png</key>
            <key type="filename">../decorations/grass/g1_0005.png</key>
            <key type="filename">../decorations/grass/g1_0006.png</key>
            <key type="filename">../decorations/grass/g1_0007.png</key>
            <key type="filename">../decorations/grass/g1_0008.png</key>
            <key type="filename">../decorations/grass/g1_0009.png</key>
            <key type="filename">../decorations/grass/g1_0010.png</key>
            <key type="filename">../decorations/grass/g1_0011.png</key>
            <key type="filename">../decorations/grass/g2_0001.png</key>
            <key type="filename">../decorations/grass/g2_0002.png</key>
            <key type="filename">../decorations/grass/g2_0003.png</key>
            <key type="filename">../decorations/grass/g2_0004.png</key>
            <key type="filename">../decorations/grass/g2_0005.png</key>
            <key type="filename">../decorations/grass/g2_0006.png</key>
            <key type="filename">../decorations/grass/g2_0007.png</key>
            <key type="filename">../decorations/grass/g2_0008.png</key>
            <key type="filename">../decorations/grass/g3_0001.png</key>
            <key type="filename">../decorations/grass/g3_0002.png</key>
            <key type="filename">../decorations/grass/g3_0003.png</key>
            <key type="filename">../decorations/grass/g3_0004.png</key>
            <key type="filename">../decorations/grass/g3_0005.png</key>
            <key type="filename">../decorations/grass/g3_0006.png</key>
            <key type="filename">../decorations/grass/g3_0007.png</key>
            <key type="filename">../decorations/grass2/g4_0001.png</key>
            <key type="filename">../decorations/grass2/g4_0002.png</key>
            <key type="filename">../decorations/grass2/g4_0003.png</key>
            <key type="filename">../decorations/grass2/g4_0004.png</key>
            <key type="filename">../decorations/grass2/g4_0005.png</key>
            <key type="filename">../decorations/grass2/g4_0006.png</key>
            <key type="filename">../decorations/grass2/g4_0007.png</key>
            <key type="filename">../decorations/grass2/g4_0008.png</key>
            <key type="filename">../decorations/grass2/g4_0009.png</key>
            <key type="filename">../decorations/grass2/g4_0010.png</key>
            <key type="filename">../decorations/grass2/g5_0001.png</key>
            <key type="filename">../decorations/grass2/g5_0002.png</key>
            <key type="filename">../decorations/grass2/g5_0003.png</key>
            <key type="filename">../decorations/grass2/g5_0004.png</key>
            <key type="filename">../decorations/grass2/g5_0005.png</key>
            <key type="filename">../decorations/grass2/g5_0006.png</key>
            <key type="filename">../decorations/grass2/g5_0007.png</key>
            <key type="filename">../decorations/grass2/g5_0008.png</key>
            <key type="filename">../decorations/grass2/g6_0001.png</key>
            <key type="filename">../decorations/grass2/g6_0002.png</key>
            <key type="filename">../decorations/grass2/g6_0003.png</key>
            <key type="filename">../decorations/grass2/g6_0004.png</key>
            <key type="filename">../decorations/grass2/g6_0005.png</key>
            <key type="filename">../decorations/grass2/g6_0006.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/lizard1/lizard_0001.png</key>
            <key type="filename">../decorations/lizard1/lizard_0002.png</key>
            <key type="filename">../decorations/lizard1/lizard_0003.png</key>
            <key type="filename">../decorations/lizard1/lizard_0004.png</key>
            <key type="filename">../decorations/lizard1/lizard_0005.png</key>
            <key type="filename">../decorations/lizard1/lizard_0006.png</key>
            <key type="filename">../decorations/lizard1/lizard_0007.png</key>
            <key type="filename">../decorations/lizard1/lizard_0008.png</key>
            <key type="filename">../decorations/lizard1/lizard_0009.png</key>
            <key type="filename">../decorations/lizard1/lizard_0010.png</key>
            <key type="filename">../decorations/lizard1/lizard_0011.png</key>
            <key type="filename">../decorations/lizard1/lizard_0012.png</key>
            <key type="filename">../decorations/lizard1/lizard_0013.png</key>
            <key type="filename">../decorations/lizard1/lizard_0014.png</key>
            <key type="filename">../decorations/lizard1/lizard_0015.png</key>
            <key type="filename">../decorations/lizard1/lizard_0016.png</key>
            <key type="filename">../decorations/lizard1/lizard_0017.png</key>
            <key type="filename">../decorations/lizard1/lizard_0018.png</key>
            <key type="filename">../decorations/lizard1/lizard_0019.png</key>
            <key type="filename">../decorations/lizard1/lizard_0020.png</key>
            <key type="filename">../decorations/lizard1/lizard_0021.png</key>
            <key type="filename">../decorations/lizard1/lizard_0022.png</key>
            <key type="filename">../decorations/lizard1/lizard_0023.png</key>
            <key type="filename">../decorations/lizard1/lizard_0024.png</key>
            <key type="filename">../decorations/lizard1/lizard_0025.png</key>
            <key type="filename">../decorations/lizard1/lizard_0026.png</key>
            <key type="filename">../decorations/lizard1/lizard_0027.png</key>
            <key type="filename">../decorations/lizard1/lizard_0028.png</key>
            <key type="filename">../decorations/lizard1/lizard_0029.png</key>
            <key type="filename">../decorations/lizard1/lizard_0030.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,5,39,11</rect>
                <key>scale9Paddings</key>
                <rect>20,5,39,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/lizard2/lizadr_0001.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0002.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0003.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0004.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0005.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0006.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0007.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0008.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0009.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0010.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0011.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0012.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0013.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0014.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0015.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0016.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0017.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0018.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0019.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0020.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0021.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0022.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0023.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0024.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0025.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0026.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0027.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0028.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0029.png</key>
            <key type="filename">../decorations/lizard2/lizadr_0030.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,8,45,15</rect>
                <key>scale9Paddings</key>
                <rect>22,8,45,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/native1/native1_0001.png</key>
            <key type="filename">../decorations/native1/native1_0002.png</key>
            <key type="filename">../decorations/native1/native1_0003.png</key>
            <key type="filename">../decorations/native1/native1_0004.png</key>
            <key type="filename">../decorations/native1/native1_0005.png</key>
            <key type="filename">../decorations/native1/native1_0006.png</key>
            <key type="filename">../decorations/native1/native1_0007.png</key>
            <key type="filename">../decorations/native1/native1_0008.png</key>
            <key type="filename">../decorations/native1/native1_0009.png</key>
            <key type="filename">../decorations/native1/native1_0010.png</key>
            <key type="filename">../decorations/native1/native1_0011.png</key>
            <key type="filename">../decorations/native1/native1_0012.png</key>
            <key type="filename">../decorations/native1/native1_0013.png</key>
            <key type="filename">../decorations/native1/native1_0014.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,14,24,27</rect>
                <key>scale9Paddings</key>
                <rect>12,14,24,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/native2/native2_0001.png</key>
            <key type="filename">../decorations/native2/native2_0002.png</key>
            <key type="filename">../decorations/native2/native2_0003.png</key>
            <key type="filename">../decorations/native2/native2_0004.png</key>
            <key type="filename">../decorations/native2/native2_0005.png</key>
            <key type="filename">../decorations/native2/native2_0006.png</key>
            <key type="filename">../decorations/native2/native2_0007.png</key>
            <key type="filename">../decorations/native2/native2_0008.png</key>
            <key type="filename">../decorations/native2/native2_0009.png</key>
            <key type="filename">../decorations/native2/native2_0010.png</key>
            <key type="filename">../decorations/native2/native2_0011.png</key>
            <key type="filename">../decorations/native2/native2_0012.png</key>
            <key type="filename">../decorations/native2/native2_0013.png</key>
            <key type="filename">../decorations/native2/native2_0014.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,14,26,29</rect>
                <key>scale9Paddings</key>
                <rect>13,14,26,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/native3/native3_0001.png</key>
            <key type="filename">../decorations/native3/native3_0002.png</key>
            <key type="filename">../decorations/native3/native3_0003.png</key>
            <key type="filename">../decorations/native3/native3_0004.png</key>
            <key type="filename">../decorations/native3/native3_0005.png</key>
            <key type="filename">../decorations/native3/native3_0006.png</key>
            <key type="filename">../decorations/native3/native3_0007.png</key>
            <key type="filename">../decorations/native3/native3_0008.png</key>
            <key type="filename">../decorations/native3/native3_0009.png</key>
            <key type="filename">../decorations/native3/native3_0010.png</key>
            <key type="filename">../decorations/native3/native3_0011.png</key>
            <key type="filename">../decorations/native3/native3_0012.png</key>
            <key type="filename">../decorations/native3/native3_0013.png</key>
            <key type="filename">../decorations/native3/native3_0014.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,12,21,23</rect>
                <key>scale9Paddings</key>
                <rect>10,12,21,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/rain/rain_0001.png</key>
            <key type="filename">../decorations/rain/rain_0002.png</key>
            <key type="filename">../decorations/rain/rain_0003.png</key>
            <key type="filename">../decorations/rain/rain_0004.png</key>
            <key type="filename">../decorations/rain/rain_0005.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,5,14,10</rect>
                <key>scale9Paddings</key>
                <rect>7,5,14,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/rain/rain_wather.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1,4,2,8</rect>
                <key>scale9Paddings</key>
                <rect>1,4,2,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/snow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/snowman.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,265,128,530</rect>
                <key>scale9Paddings</key>
                <rect>64,265,128,530</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/tree/g1_0001.png</key>
            <key type="filename">../decorations/tree/g1_0001.psd</key>
            <key type="filename">../decorations/tree/g1_0002.png</key>
            <key type="filename">../decorations/tree/g1_0003.png</key>
            <key type="filename">../decorations/tree/g1_0004.png</key>
            <key type="filename">../decorations/tree/g1_0005.png</key>
            <key type="filename">../decorations/tree/g2_0001.png</key>
            <key type="filename">../decorations/tree/g3_0001.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>125,125,250,250</rect>
                <key>scale9Paddings</key>
                <rect>125,125,250,250</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/yolka.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>607,341,1214,683</rect>
                <key>scale9Paddings</key>
                <rect>607,341,1214,683</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../decorations/zay1/10000.png</key>
            <key type="filename">../decorations/zay1/10001.png</key>
            <key type="filename">../decorations/zay1/10002.png</key>
            <key type="filename">../decorations/zay1/10003.png</key>
            <key type="filename">../decorations/zay1/10004.png</key>
            <key type="filename">../decorations/zay1/10005.png</key>
            <key type="filename">../decorations/zay1/10006.png</key>
            <key type="filename">../decorations/zay1/10007.png</key>
            <key type="filename">../decorations/zay1/10008.png</key>
            <key type="filename">../decorations/zay1/10009.png</key>
            <key type="filename">../decorations/zay1/10010.png</key>
            <key type="filename">../decorations/zay1/10011.png</key>
            <key type="filename">../decorations/zay1/10012.png</key>
            <key type="filename">../decorations/zay1/10013.png</key>
            <key type="filename">../decorations/zay1/10014.png</key>
            <key type="filename">../decorations/zay1/10015.png</key>
            <key type="filename">../decorations/zay1/10016.png</key>
            <key type="filename">../decorations/zay1/10017.png</key>
            <key type="filename">../decorations/zay2/10000.png</key>
            <key type="filename">../decorations/zay2/10001.png</key>
            <key type="filename">../decorations/zay2/10002.png</key>
            <key type="filename">../decorations/zay2/10003.png</key>
            <key type="filename">../decorations/zay2/10004.png</key>
            <key type="filename">../decorations/zay2/10005.png</key>
            <key type="filename">../decorations/zay2/10006.png</key>
            <key type="filename">../decorations/zay2/10007.png</key>
            <key type="filename">../decorations/zay2/10008.png</key>
            <key type="filename">../decorations/zay2/10009.png</key>
            <key type="filename">../decorations/zay2/10010.png</key>
            <key type="filename">../decorations/zay2/10011.png</key>
            <key type="filename">../decorations/zay2/10012.png</key>
            <key type="filename">../decorations/zay2/10013.png</key>
            <key type="filename">../decorations/zay2/10014.png</key>
            <key type="filename">../decorations/zay2/10015.png</key>
            <key type="filename">../decorations/zay2/10016.png</key>
            <key type="filename">../decorations/zay2/10017.png</key>
            <key type="filename">../decorations/zay3/Run_0000.png</key>
            <key type="filename">../decorations/zay3/Run_0001.png</key>
            <key type="filename">../decorations/zay3/Run_0002.png</key>
            <key type="filename">../decorations/zay3/Run_0003.png</key>
            <key type="filename">../decorations/zay3/Run_0004.png</key>
            <key type="filename">../decorations/zay3/Run_0005.png</key>
            <key type="filename">../decorations/zay3/Run_0006.png</key>
            <key type="filename">../decorations/zay3/Run_0007.png</key>
            <key type="filename">../decorations/zay3/Run_0008.png</key>
            <key type="filename">../decorations/zay3/Run_0009.png</key>
            <key type="filename">../decorations/zay3/Run_0010.png</key>
            <key type="filename">../decorations/zay3/Run_0011.png</key>
            <key type="filename">../decorations/zay3/Run_0012.png</key>
            <key type="filename">../decorations/zay3/Run_0013.png</key>
            <key type="filename">../decorations/zay3/Run_0014.png</key>
            <key type="filename">../decorations/zay3/Run_0015.png</key>
            <key type="filename">../decorations/zay3/Run_0016.png</key>
            <key type="filename">../decorations/zay3/Run_0017.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,50,100,100</rect>
                <key>scale9Paddings</key>
                <rect>50,50,100,100</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../decorations</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
