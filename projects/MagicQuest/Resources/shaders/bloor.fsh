#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

void main()
{
	float delta = 0.001;
	vec4 sum = vec4( 0.0, 0.0, 0.0, 0.0 );
	
	float y_2 = -0.003;
	float y2 = 0.003;
	float x_2 = -0.002;
	float x2 = 0.002;

	sum += texture2D( CC_Texture0, vec2( v_texCoord.x + x_2, v_texCoord.y + y2 ) );
	sum += texture2D( CC_Texture0, vec2( v_texCoord.x + x2, v_texCoord.y + y2 ) );
	
	sum += texture2D( CC_Texture0, vec2( v_texCoord.x + x_2, v_texCoord.y + y_2 ) );
	sum += texture2D( CC_Texture0, vec2( v_texCoord.x + x2, v_texCoord.y + y_2 ) );
	
	sum = sum / 5.0;
	gl_FragColor = sum;
}
