/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.cpp;

import org.cocos2dx.lib.Cocos2dxActivity;

import com.stereo7.extensions.*;
import com.stereo7.flurry.Flurry;
import com.stereo7.tapjoy.*;
import com.stereo7.appodeal.*;
//import com.stereo7.chartboost.*;

import android.content.Intent;
import android.os.Bundle;
import android.net.Uri;
import android.provider.Settings.Secure;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.content.Intent;
import com.sdkbox.plugin.SDKBox;

public class AppActivity extends Cocos2dxActivity 
{
	private static final String HOCKEY_ID = "27248d12f0be3ed754625c598addc9d6";

	@SuppressWarnings("unused")
	private static TapJoy tapjoy;
	private static Flurry flurry;
	private static InApps inapp;
	private static AppActivity me;
	private static AppGratisBatch batch;
	private static mlAppodeal appodeal;
//	private static ChartBoost charboost;
	
	String batchAppId = "55E57D3624A72B12D0A22CF1044C9B";
	//String batchAppId = "DEV55E57D3628CDB0383E22161EF1C";
    String flurryAppID = "T7MK3TJQGZZKS7YMG3FK";
    String inappsLicenseKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkS/yKzVu9lJQLgkzoJsrcNVAiD+F9BS93g1SxysTAjGG0n+uKz8AqS7w8Hgun/5do5gDwv+ZmtcBvnOVgGyEUo9Z+PqtKacRV8YxA1CR9Iwde4xDBz1dlOo1Sac43zpWgU8T6uPOhy4ocq1vEVWGFQGgHBV0V4X13psKW7E+DsNVPm/0iOahrIOn/hgYFBXZ9p8ppDGJMgi8zHYJCTCYb2Mzo02CiAi0FMf/H5VU5sZ5dv16YvDiujlIB8QgWNZC+p9Ay+Wa84MIpaOpgEtGkPxRd4ZSML5dexb/22fWtIBJ6FooGlO7fvGDWAPbpAvKKv5wQ2a+4xQlSt0vSf4iyQIDAQAB";
    String tapjoyAppID = "EUjwsQh1RAid4j9B6o1DzQECjN80qoeNEJxS0gLKoKm9LM37kDqc9GUR06vA";
    String appodealAppID = "4562447954eccb76689aa3e0eaa18134a97dd7bdaa23d5df";
//	String chartboostID = "55e56e5df6cd450eb5132b89";
//	String chartboostSignature = "993d127ff425fcd6d15d7d34538c2e3669ff7d60";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		me = this;

		String android_id = Secure.getString(getContext().getContentResolver(), Secure.ANDROID_ID);
		nativeSetPhoneID(android_id);

		
        try {
			tapjoy = new TapJoy(this, tapjoyAppID);
			flurry = new Flurry(this, flurryAppID);
	        inapp = new InApps(this, inappsLicenseKey);
	        batch = new AppGratisBatch(this, batchAppId);
	        appodeal = new mlAppodeal( this, appodealAppID, true, true );
//			charboost = new ChartBoost(this, chartboostID, chartboostSignature);
			SDKBox.init(this);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
    }
	private native void nativeSetPhoneID(String id);

	
	@Override
    public void onDestroy() {
		if( inapp != null) inapp.onDestroy();
		if( batch != null) batch.onDestroy();
		//if( sonic != null) sonic.onDestroy();
//		if( charboost != null) charboost.onDestroy();
        super.onDestroy();
    }
	
	@Override
	protected void onPause() {
	    super.onPause();
		SDKBox.onPause();
	    //if( sonic != null) sonic.onPause();
//		if( charboost != null) charboost.onPause();
        
        //Hockey
        //UpdateManager.unregister();
	}
	
	@Override
	protected void onResume() {
	    super.onResume();
		SDKBox.onResume();
	    //if( sonic != null) sonic.onResume();
	    //if( fyber != null) fyber.onResume();
//		if( charboost != null) charboost.onResume();
        //Hockey
        //CrashManager.register(this, HOCKEY_ID);

    }

	@Override
	protected void onStart()
	{
		super.onStart();
		if( flurry != null) flurry.onStart();
		if( batch != null) batch.onStart();
		//if( sonic != null) sonic.onStart();
//		if( charboost != null) charboost.onStart();
		SDKBox.onStart();

        super.setKeepScreenOn(true);
	}

	@Override
	protected void onStop()
	{
		super.onStop();	
		if( flurry != null) flurry.onStop();
		if( batch != null) batch.onStop();
//		if( charboost != null) charboost.onStop();
		SDKBox.onStop();

	}
	
	@Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
		if( batch != null )batch.onNewIntent( intent );
    }
  
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (inapp != null) inapp.onActivityResult(requestCode, resultCode, data);
		if (!SDKBox.onActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		}
		//if( fyber != null ) fyber.onActivityResult(requestCode, resultCode, data);
	}
	
	public static void openUrl(String URL)
	{
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(URL));
		me.startActivity(i);
	}
	
	private static boolean hasInternetConnection() 
	{
		ConnectivityManager cm = (ConnectivityManager)getContext().getSystemService(CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
	}
	
	public static void openStorePage()
	{
		String packagename = me.getPackageName();
		String link = "market://details?id=" + packagename;
		openUrl( link );
	}
	@Override
	public void onBackPressed() {
		if(!SDKBox.onBackPressed()) {
			super.onBackPressed();
		}
	}
}