package com.stereo7games.fantasydefense;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Parcelable;
import android.util.Log;

import com.easyndk.classes.AndroidNDKHelper;

import org.cocos2dx.cpp.AppActivity;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by zeeshanamjad on 13/11/2017.
 */

public class ShareHandler {
    private final String TAG = ShareHandler.class.getSimpleName();
    public static final int XPLATFORMSHARE_RESULTCODE = 1100;

    public void initialize()
    {
        AndroidNDKHelper.AddNDKReceiver(this, "SHARING_MODULE");
    }

    public void showRatePopup(JSONObject params) throws JSONException
    {
        AppActivity.openStorePage();
    }

    public void showShareScreen(JSONObject params) throws JSONException
    {
        final String shareMessage = params.getString("message");
        final String shareURL = params.getString("ShareURL");
        final String shareTitle = params.getString("title");
        final String shareText = String.format("%s\n%s", shareMessage, shareURL);

        ArrayList<String> activitiesToSkip = new ArrayList<String>();
        activitiesToSkip.add("com.android.bluetooth"); //disable Bluetooth
        activitiesToSkip.add("com.google.android.apps.docs"); //disable GoogleDrive
        activitiesToSkip.add("com.samsung.android.app.memo"); //disable Memo
        activitiesToSkip.add("com.google.android.apps.translate"); //disable Google translate
        activitiesToSkip.add("org.mozilla.firefox"); //disable Firefox browser

        //Create sharing intent just to request all available share options
        Intent sharingIntent = buildSendIntent(null, shareTitle, shareText);
        //Allocate array for included share intents
        List<Intent> targetedShareIntents = new ArrayList<Intent>();
        //Request all activities that can be used for sharingIntent (ACTION_SEND)
        List<ResolveInfo> resInfo = AppActivity.getContext().getPackageManager().queryIntentActivities(sharingIntent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            //Get activity name to filter that out
            final String packageName = resolveInfo.activityInfo.packageName;
            //Check if activity is not into skipped array
            if (!activitiesToSkip.contains(packageName)) {
                Intent targetedShareIntent = buildSendIntent(packageName, shareTitle, shareText);
                //Add intent to all intents
                targetedShareIntents.add(targetedShareIntent);
            }
        }

        Intent chooserIntent = null;
        //Check if any intent was selected and suggest sharing with those options
        if (targetedShareIntents.size() > 0) {
            chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Share using");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
        }
        else { //all the intents were dismissed/skipped (barely can happen. Just the check for safety) -> share with default intents
            chooserIntent = Intent.createChooser(sharingIntent, "Share using");
        }

        //Run chooser
        ((AppActivity)AppActivity.getContext()).startActivityForResult(chooserIntent, XPLATFORMSHARE_RESULTCODE);
    }

    private final Intent buildSendIntent(final String packageName, final String title, final String text)
    {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TITLE, title);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, title);
        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        if (packageName != null) {
            sendIntent.setPackage(packageName);
        }
        return sendIntent;
    }

    public static void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == ShareHandler.XPLATFORMSHARE_RESULTCODE) {
            HashMap<String, Boolean> map = new HashMap<String, Boolean>();
            map.put("isShareDone", true);
            final JSONObject jsonObject = new JSONObject(map);
            ((AppActivity)AppActivity.getContext()).runOnGLThread(new Runnable()
            {
                public void run()
                {
                    AndroidNDKHelper.SendMessageWithParameters("sharingCompleteCallback", jsonObject);
                }
            });
        }
    }
}
