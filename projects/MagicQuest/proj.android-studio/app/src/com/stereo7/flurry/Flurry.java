package com.stereo7.flurry;

import android.app.Activity;

import com.flurry.android.FlurryAgent;

import com.stereo7.extensions.ParamCollection;

public class Flurry
{
	private static Activity app;
	private static String appID;
	private static Flurry me;

	public Flurry(Activity application, String id) {
		me = this;
		app = application;
		appID = id;
		FlurryAgent.init(application, appID);
    }
	
	public void onStart()
	{
		FlurryAgent.onStartSession(app, appID);
	}

	public void onStop()
	{
		FlurryAgent.onEndSession(app);
	}

	static public void logEvent(String params)
    {
		if( me == null )
			return;
		
		if( params.isEmpty() == false )
		{
	        ParamCollection pc = new ParamCollection();
	        pc.fromString(params);
	        String event = pc.value("event");
	        FlurryAgent.logEvent(event, pc.Map());
		}
    }
}
