//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "ml/ParamCollection.h"
#include "ml/loadxml/xmlLoader.h"

USING_NS_CC;

const char ParamCollection::delimiter_pair = ':';
const char ParamCollection::delimiter = ',';
const char ParamCollection::delimiter_invalue = static_cast<char>(0x1);

ParamCollection::ParamCollection(const std::string &string)
{
	parse(string);
}

void ParamCollection::parse(const std::string &string)
{
	size_type l = 0;
	do
	{
		size_type k = string.find(delimiter, l);
		if (k == std::string::npos) k = string.size();
		std::string s = string.substr(l, k - l);
		l = k + 1;

		size_type d = s.find(delimiter_pair);
		bool value = d != std::string::npos;
		std::string n = value ? s.substr(0, d) : s;
		std::string v = value ? s.substr(d + 1) : std::string();
		std::string::size_type l(0);
		while (true)
		{
			std::string::size_type k = v.find(delimiter_invalue, l);
			if (k == std::string::npos)
				break;
			v[k] = delimiter;
			l = k + 1;
		}
		if( n.empty() == false )
		{
			set( n, xmlLoader::macros::parse( v ) );
		}
	} while (l < string.size());
}

std::string ParamCollection::string() const
{
	std::string result;
	for (auto &iter : *this)
	{
		if (result.empty() == false) result += delimiter;

		std::string name = iter.first;
		std::string value = iter.second;
		std::string::size_type l(0);
		do
		{
			std::string::size_type k = value.find(delimiter, l);
			if (k == std::string::npos)
				break;
			value[k] = delimiter_invalue;
			l = k + 1;
		} while (true);
		result += name + (value.empty() ? value : (delimiter_pair + value));
	}
	return result;
}

const std::string ParamCollection::get(const std::string &name, const std::string &defaultValue)
{
	auto iter = find(name);
	return iter != end() ? iter->second : defaultValue;
}

const std::string ParamCollection::get( const std::string &name, const std::string &defaultValue )const
{
	auto iter = find( name );
	return iter != end() ? iter->second : defaultValue;
}

const void ParamCollection::set(const std::string &name, const std::string &value, bool rewrite)
{
	auto parsedValue = xmlLoader::macros::parse( value );
	auto pair = insert( std::pair<std::string, std::string>( name, parsedValue ) );
	if (pair.second == false && rewrite)
		pair.first->second = value;
}

void ParamCollection::tolog() const
{
	CCLOG("ParamCollection::tolog begin:");
	for (auto &iter : *this)
	{
		CCLOG("%s=[%s]", iter.first.c_str(), iter.second.c_str());
	}
	CCLOG("ParamCollection::tolog end:");
}

bool ParamCollection::isExist(const std::string &name)
{
	return find(name) != end();
}