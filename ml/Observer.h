//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __Observer_h__
#define __Observer_h__
#include <functional>
#include <vector>
#include <assert.h>
#include <memory>
#include <map>
#include "IntrusivePtr.h"

template <class Handler, class Function>
class ObServer
{
public:
	ObServer( )
		: _lockCounter(0)
		, _locked(0)
		, _functions()
		, _holder(nullptr)
	{}

	ObServer( Handler * holder ) : ObServer()
	{}

	~ObServer( void )
	{
	}
	
	void add( int tag, const Function & function )
	{
		assert( function );
		if( _locked > 0 )
		{
			_functionsAdd[tag] = function;
		}
		else
		{
			_functions[tag] = function;
		}
	}

	void remove( int tag )
	{
		if( _locked == 0 )
		{
			auto iter = _functions.find( tag );
			if( iter != _functions.end() )
				_functions.erase( iter );
		}
		else
		{
			_functionsRem.push_back( tag );
		}
	}
	
	template<class...TArgs>
	void pushevent( TArgs && ... _Args )
	{
		if( _lockCounter == 0 )
		{
			++_locked;
			for( auto& func : _functions )
			{
				func.second( std::forward<TArgs>( _Args )... );
			}
			--_locked;
		}
		refreshFunctions();
	}

	template<class...TArgs>
	void pushconsumableevent( TArgs && ..._Args )
	{
		if( _lockCounter == 0 )
		{
			++_locked;
			for( auto& func : _functions )
			{
				bool handled = func.second( std::forward<TArgs>( _Args )... );
				if( handled ) break;
			}
			--_locked;
		}
		refreshFunctions();
	}

	void lock()
	{
		++_lockCounter;
		assert( _lockCounter >= 0 );
	}

	void unlock()
	{
		--_lockCounter;
		assert( _lockCounter >= 0 );
	}

	Handler * holder()
	{
		return _holder;
	}

protected:
	void refreshFunctions()
	{
		if( _locked == 0 )
		{
			_functions.insert( _functionsAdd.begin(), _functionsAdd.end() );
			for( auto tag : _functionsRem )
			{
				auto iter = _functions.find( tag );
				if( iter != _functions.end() )
					_functions.erase( iter );
			}
			_functionsAdd.clear();
			_functionsRem.clear();
		}
	}
private:
	int _lockCounter;
	int _locked;
	std::map<int, Function > _functions;
	std::map<int, Function > _functionsAdd;
	std::vector<int> _functionsRem;
	Handler * _holder;
};



#endif