//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "ml/NodeExt.h"
#include "ml/common.h"
#include "ml/loadxml/xmlProperties.h"
#include "ml/SmartScene.h"
#include "ml/Config.h"
#include "ml/ImageManager.h"
#include "ml/ShadersCache.h"
NS_CC_BEGIN

NodeExt::NodeExt()
: _events()
, _actions()
{}

NodeExt::~NodeExt()
{}

bool NodeExt::init()
{
	return true;
}

Node* NodeExt::as_node_pointer()
{
	return dynamic_cast<Node*>(this);
}

Node * NodeExt::getChildByPath( const std::string & path_names )
{
	Node * self = as_node_pointer();
	Node * result = self ? getNodeByPath( self, path_names ) : nullptr;
	return result;
}

Node * NodeExt::getChildByPath( const std::list<int> path_tags )
{
	Node * self = as_node_pointer();
	Node * result = self ? getNodeByTagsPath( self, path_tags ) : nullptr;
	return result;
}

ParamCollection& NodeExt::getParamCollection()
{
	return _params;
}

const ParamCollection& NodeExt::getParamCollection()const
{
	return _params;
}

void NodeExt::load( const std::string & path, const std::string & xmlFile )
{
	std::string file = path;
	if( file.empty() == false && file.back() != '/' )
		file += '/';
	file += xmlFile;
	load( file );
}

void NodeExt::load( const std::string & file )
{
	if( file.empty() )
		return;
	pugi::xml_document doc;
	doc.load_file( file.c_str() );
	auto root = doc.root().first_child();
	
	load( root );
}

void NodeExt::load( pugi::xml_node & root )
{
	xmlLoader::bookDirectory( this );
	xmlLoader::load( as_node_pointer(), root );
	xmlLoader::unbookDirectory( this );
	onLoaded();
}

void NodeExt::onLoaded()
{
	auto node = as_node_pointer();
	if( node )
	{
		auto children = node->getChildren();
		for( auto child : children )
		{
			auto nodeext = dynamic_cast<NodeExt*>(child);
			if( nodeext )
				nodeext->onLoaded();
		}
	}
}

bool NodeExt::runEvent( const std::string & eventname )
{
	//Node* node = as_node_pointer();
	//if( node )
	//{
	//	log( "NodeExt::runEvent, name:[%s], eventname:[%s],refs:[%d]", node->getName().c_str(), eventname.c_str(), node->getReferenceCount() );
	//	std::string path;// = node->getName().c_str();
	//	while( node )
	//	{
	//		path = node->getName() + "/" + path;
	//		node = node->getParent();
	//	}
	//	log( "NodeExt::runEvent, path:[%s]", path.c_str() );
	//}

	auto iter = _events.find( eventname );
	if( iter != _events.end() )
	{
		iter->second.execute( this );
	}
	else
	{
		std::string name = as_node_pointer() ? as_node_pointer()->getName() : "Not node inherited";
		//log_once( "NodeExt[%s]: event with name [%s] not dispatched", name.c_str( ), eventname.c_str( ) );
	}
	return iter != _events.end();
}

ActionPointer NodeExt::getAction( const std::string & name )
{
	auto iter = _actions.find( name );
	if( iter != _actions.end() )
		return iter->second;
	return nullptr;
}

bool NodeExt::setProperty( int intproperty, const std::string & value )
{
	return false;
}

bool NodeExt::setProperty( const std::string & stringproperty, const std::string & value )
{
	return false;
}

void NodeExt::loadActions( const pugi::xml_node & xmlnode )
{
	for( auto xmlaction : xmlnode )
	{
		std::string actionname = xmlaction.attribute( "name" ).as_string();
		auto action = xmlLoader::load_action( xmlaction );
		assert( action );
		_actions[actionname] = action;
	}
}

void NodeExt::loadEvents( const pugi::xml_node & xmlnode )
{
	for( auto xmllist : xmlnode )
	{
		std::string listname = xmlLoader::macros::parse( xmllist.attribute( "name" ).as_string() );
		for( auto xmlevent : xmllist )
		{
			auto event = xmlLoader::load_event( xmlevent );
			assert( event );
			_events[listname].push_back( event );
		}
	}
}

void NodeExt::loadParams( const pugi::xml_node & xmlnode )
{
	xmlLoader::load_paramcollection( _params, xmlnode );
}

bool NodeExt::loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )
{
	if( tag == xmlLoader::k::xmlTag::ParamCollection )
	{
		loadParams( xmlnode );
	};
	return false;
}


LayerExt::LayerExt()
: _firstEntrance(true)
{}
LayerExt::~LayerExt() {}

bool LayerExt::init()
{
	do
	{
		CC_BREAK_IF( !Layer::init() );
		CC_BREAK_IF( !NodeExt::init() );
	}
	while( false );
	return true;
}

void LayerExt::onEnter()
{
	Layer::onEnter();
	if( _firstEntrance )
	{
		_firstEntrance = false;
		appearance();
	}
}

ccMenuCallback LayerExt::get_callback_by_description( const std::string & name )
{
	if( name == "close" || name == "disappearance" )
	{
		return std::bind( &LayerExt::disappearance, this );
	}
	else if( name.find( "runevent:" ) == 0 )
	{
		auto callback = []( NodeExt* node, const std::string& name ){
			node->runEvent( name );
		};
		auto eventname = name.substr( strlen( "runevent:" ) );
		return std::bind( callback, this, eventname );
	}
	else if( name.find( "openurl:" ) == 0 )
	{
		auto urlName = name.substr( strlen( "openurl:" ) );
		auto url = Config::shared().get( urlName );

		auto cb = [url]( Ref* )
		{
            Application::getInstance()->openURL( url );
		};

		return cb;
	}
	else if( name == "popscene" )
	{
		return [](Ref*){Director::getInstance()->popScene();};
	}
	else if( name.find("pushlayer:") ==0 )
	{
		std::string path = name.substr( strlen( "pushlayer:" ) );
		auto callback =  []( Ref*,LayerExt*layer, const std::string& path )
		{
			auto newLayer = xmlLoader::load_node<LayerExt>( path );
			assert( newLayer );
			if( newLayer && layer->getSmartScene() )
			{
				layer->getSmartScene()->pushLayer( newLayer );
			}
		};
		return std::bind( callback, std::placeholders::_1, this, path );
	}

	return NodeExt::get_callback_by_description(name);
}

void LayerExt::appearance()
{
	runEvent( "appearance" );
}

void LayerExt::disappearance()
{
	if( !runEvent( "disappearance" ) )
	{
		runAction( CallFunc::create( [this](){removeFromParent(); } ) );		
	}
}

SmartScene* LayerExt::getSmartScene()
{
	return dynamic_cast<SmartScene*>(getScene());
}

void LayerExt::setDisapparanceOnBackButton()
{
	EventListenerKeyboard * event = EventListenerKeyboard::create();
	event->onKeyReleased = std::bind( [this]( EventKeyboard::KeyCode key, Event* )mutable
	{
		if( key == EventKeyboard::KeyCode::KEY_BACK )
			this->disappearance();
	}, std::placeholders::_1, std::placeholders::_2 );

	getEventDispatcher()->addEventListenerWithSceneGraphPriority( event, this );
}

void LayerExt::initBlockLayer( const std::string& image )
{
	_blockLayerWaitProcess = ImageManager::shared().sprite( image );
	_blockLayerWaitProcess->setPosition( Point( Director::getInstance()->getOpenGLView()->getDesignResolutionSize() / 2 ) );
	_blockLayer = LayerExt::create();
	_blockLayer->addChild( _blockLayerWaitProcess );
	_blockLayerAction = RepeatForever::create( RotateBy::create( 1, 360.f ) );
}

void LayerExt::pushBlockLayer( bool showWaitProcess, float duration )
{
	assert( _blockLayer != nullptr );
	SmartScene * scene = dynamic_cast<SmartScene*>(getScene());
	if( !scene )
	{
		assert( scene );
		return;
	}
	_blockLayerWaitProcess->setVisible( showWaitProcess );
	if( showWaitProcess )
		_blockLayerWaitProcess->runAction( _blockLayerAction->clone() );
	
	scene->pushLayer( _blockLayer, true );
	auto action = Sequence::create( DelayTime::create( duration ), RemoveSelf::create(), nullptr );
	_blockLayer->runAction( action );
}

void LayerExt::popBlockLayer()
{
	assert( _blockLayer != nullptr );
	if( _blockLayer )
		_blockLayer->removeFromParent();
}


LayerBlur::LayerBlur()
: _useBlur(false)
, _blurCounter(0)
, _blurSamples(10)
{}

bool LayerBlur::init()
{
	return true;
}

void LayerBlur::visitWithBlur( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )
{
	assert( _useBlur );
	assert( _rendererBloor );

	if( isBlurActive() )
	{
		if( _blurCounter == 0 )
		{
			renderer->render();
			_rendererBloor->beginWithClear( 0, 0, 0, 0 );
			visitContent( renderer, parentTransform, parentFlags );
			_rendererBloor->end();
			renderer->render();
		}
		else if( _blurCounter < _blurSamples )
		{
			_rendererBloor->begin();
			_rendererBloor->getSprite()->visit( renderer, parentTransform, parentFlags );
			_rendererBloor->end();
			renderer->render();
		}
		_rendererBloor->visit( renderer, parentTransform, parentFlags );
		renderer->render();
		_blurCounter++;
	}
	else
	{
		_blurCounter = 0;
		visitContent( renderer, parentTransform, parentFlags );
	}
}

void LayerBlur::setIsUseBlur( bool var )
{
	_useBlur = var;
	if( _useBlur )
	{
		auto desSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
		auto program = CustomShadersCache::shared().program( "shaders/bloor" );
		_rendererBloor = RenderTexture::create( desSize.width, desSize.height );
		_rendererBloor->setPosition( Point( desSize / 2 ) );
		_rendererBloor->getSprite()->setGLProgram( program );
	}
	else
	{
		_rendererBloor.reset( nullptr );
	}
}

bool LayerBlur::getIsUseBlur()const
{
	return _useBlur;
}

NodeExt_::NodeExt_() {}
NodeExt_::~NodeExt_() {}
SpriteExt::SpriteExt() {}
SpriteExt::~SpriteExt() {}
MenuExt::MenuExt() {}
MenuExt::~MenuExt() {}
NS_CC_END