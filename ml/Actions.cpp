#include "Actions.h"
#include "common.h"
#include "ui/UIText.h"

NS_CC_BEGIN

ActionText::ActionText() 
{
}

ActionText::~ActionText()
{
}

bool ActionText::init(float duration, float endValue, bool floatTruncation, const std::string& prefix, const std::string& postfix)
{
	if (ActionInterval::initWithDuration(duration))
	{
		_prefix = prefix;
		_postfix = postfix;
		_floatTruncation = floatTruncation;
		_endValue = endValue;
		return true;
	}
	return false;
}
void ActionText::startWithTarget(cocos2d::Node *target)
{
	ActionInterval::startWithTarget(target);
	auto label = dynamic_cast<LabelProtocol*>(target);
	assert(label);
	_startValue = strTo<float>(label->getString());
}

void ActionText::update(float t)
{
	auto label = dynamic_cast<LabelProtocol*>(getTarget());
	assert(label);

	float value = _startValue + (_endValue - _startValue) * t;
	if (_floatTruncation)
	{
		label->setString(_prefix + toStr(int(value)) + _postfix);
	}
	else
	{
		label->setString(_prefix + toStr(value) + _postfix);
	}

}

ActionInterval* ActionText::reverse() const
{
	return ActionText::create(getDuration(), _startValue, _floatTruncation, _prefix, _postfix);
};

ActionInterval* ActionText::clone() const
{
	return ActionText::create(getDuration(), _endValue, _floatTruncation, _prefix, _postfix);
}

//
// TypeAction
//

TypeAction::~TypeAction()
{

}

TypeAction::TypeAction() :
	_delay(0.f),
	_isDone(false)
{

}

bool TypeAction::init(const std::string & text, float delay)
{
	if (!ActionInterval::initWithDuration(text.length() * delay))
		return false;

	_text = text;
	_delay = delay;

	return true;
}

void TypeAction::startWithTarget(Node * target)
{
	ActionInterval::startWithTarget(target);
	setString("");
	_isDone = false;
}

void TypeAction::update(float dt)
{
	size_t length = floor(_elapsed / _delay);

	if (length >= _text.length())
	{
		stop();
		return;
	} 

	setString(_text.substr(0, length));
}

void TypeAction::stop()
{
	setString(_text);

	_isDone = true;
	ActionInterval::stop();
}

bool TypeAction::isDone() const
{
	return _isDone || ActionInterval::isDone();
}

ActionInterval * TypeAction::clone() const
{
	return TypeAction::create(_text, _delay);
}

void TypeAction::setString(const std::string & string)
{
	if (_target == nullptr)
		return;

	ui::Text * uiText = dynamic_cast<ui::Text *>(_target);
	
	if (uiText != nullptr)
	{
		uiText->setString(string);
		return;
	}

	dynamic_cast<LabelProtocol *>(_target)->setString(string);
}

NS_CC_END