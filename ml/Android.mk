LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../cocos2d/cocos)

LOCAL_MODULE := ml_static

LOCAL_MODULE_FILENAME := libml

LOCAL_SRC_FILES := \
Animation.cpp \
AStar.cpp \
Share/ShareHandler.cpp \
CutSceneHandler.cpp \
Audio/AudioEngine.cpp \
Audio/AudioMenu.cpp \
common.cpp \
Config.cpp \
Generics.cpp \
Actions.cpp \
Events.cpp \
FiniteStateMachine.cpp \
ImageManager.cpp \
IntrusivePtr.cpp \
KeyboardListener.cpp \
Language.cpp \
loadxml/xmlLoader.cpp \
loadxml/xmlProperties.cpp \
loadxml/xmlTests.cpp \
MenuItem.cpp \
Scroller.cpp \
Slider.cpp \
NodeExt.cpp \
ObjectFactory.cpp \
ParamCollection.cpp \
pugixml/pugixml.cpp \
ScrollMenu.cpp \
SmartScene.cpp \
Text.cpp \
RandomNumberGenerator.cpp \
ShadersCache.cpp

LOCAL_C_INCLUDES += $(LOCAL_PATH)/..

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static

include $(BUILD_STATIC_LIBRARY)

$(call import-module,.)
$(call import-module,audio/android)