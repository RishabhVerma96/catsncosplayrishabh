//
//  ShareHandler.hpp
//  Easter Island
//
//  Created by Zeeshan Amjad on 13/10/2017.
//

#ifndef ShareHandler_h
#define ShareHandler_h

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class ShareHandler
{
private:
    ShareHandler();
    
public:
    static ShareHandler* getInstance();
    ~ShareHandler();
    
    void showSharePopup();
    void showRatePopup();
    void sharingCompleteCallback(Node *sender, cocos2d::Value data);
    void setupEasyNDK(const char *packageName, const char* methodName, bool isStatic = false);
};

#endif /* ShareHandler_hpp */
