//
//  ShareHandler.cpp
//  Easter Island
//
//  Created by Zeeshan Amjad on 13/10/2017.
//

#include "ShareHandler.h"
#include "Services/NDKHelper/NDKHelper/NDKHelper.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
#include "ShareHandlerBridge.h"
#endif
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include "../../cocos2d/cocos/platform/android/jni/JniHelper.h"
#include <jni.h>
#endif

static ShareHandler* m_instance = nullptr;

ShareHandler::ShareHandler()
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    ShareHandlerBridge::initialize();
#endif
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    setupEasyNDK("com/stereo7games/fantasydefense/ShareHandler", "initialize");
#endif
    
    NDKHelper::addSelector("SHARING_MODULE", "sharingCompleteCallback",
                           CC_CALLBACK_2(ShareHandler::sharingCompleteCallback, this), nullptr);
}

ShareHandler::~ShareHandler()
{
    
}

ShareHandler* ShareHandler::getInstance()
{
    if (m_instance == nullptr)
    {
        m_instance = new ShareHandler();
    }
    return m_instance;
}

void ShareHandler::showSharePopup()
{
    ValueMap valueMap;
    valueMap["title"] = "Testing Title";
    valueMap["message"] = "Testing Message";
    valueMap["ShareURL"] = "http://www.google.com";
    sendMessageWithParams("showShareScreen", cocos2d::Value(valueMap), "SHARING_MODULE");
}

void ShareHandler::showRatePopup()
{
    CCLOG("Zuluu:: sending Rate call");
    sendMessageWithParams("showRatePopup", cocos2d::Value(), "SHARING_MODULE");
}

void ShareHandler::sharingCompleteCallback(Node *sender, cocos2d::Value data)
{
    
}

void ShareHandler::setupEasyNDK(const char *packageName, const char* methodName, bool isStatic)
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    if(strcmp(packageName, "") == 0)
    {
        return;
    }
    
    if(isStatic)
    {
        cocos2d::JniMethodInfo t;
        if (cocos2d::JniHelper::getStaticMethodInfo(t, packageName, methodName, "()V"))
        {
            
            t.env->CallStaticVoidMethod(t.classID, t.methodID);
            t.env->DeleteLocalRef(t.classID);
        }
    }
    else
    {
        JNIEnv *env = JniHelper::getEnv();
        if (!env)
        {
            return;
        }
        
        jclass globalClass = env->FindClass(packageName);
        if (! globalClass)
        {
            CCLOG("Failed to create class Obj");
            return;
        }
        CCLOG("Success1");
        jmethodID mid = env->GetMethodID(globalClass, methodName, "()V");
        if (!mid)
        {
            CCLOG("Failed to create method id Obj");
            return;
        }
        jobject intent = env->AllocObject(globalClass);
        
        env->CallVoidMethod(intent, mid);
    }
#endif
}
