//
//  ShareHandler_ios.h
//  Easter Island
//
//  Created by Zeeshan Amjad on 13/10/2017.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import <SystemConfiguration/SystemConfiguration.h>

@interface ShareHandler_ios : NSObject

- (void)initialize;
- (void)windowDidBecomeVisibleNotification:(NSNotification *)notification;
- (void) showRatePopup:(NSObject*) params;
- (void) showShareScreen:(NSObject*) params;

@end
