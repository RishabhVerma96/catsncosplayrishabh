
#include "ShareHandlerBridge.h"
#import "ShareHandler_ios.h"

void ShareHandlerBridge::initialize()
{
    ShareHandler_ios* xPlatformShare = [[ShareHandler_ios alloc] init];
    [xPlatformShare initialize];
}
