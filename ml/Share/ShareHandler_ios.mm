//
//  ShareHandler_ios.m
//  Easter Island
//
//  Created by Zeeshan Amjad on 13/10/2017.
//

#import <Foundation/Foundation.h>
#import "ShareHandler_ios.h"
#import "IOSNDKHelper.h"
#import "AppController.h"

@implementation ShareHandler_ios

- (void)initialize
{
    [IOSNDKHelper addNDKReceiver:self moduleName:@"SHARING_MODULE"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(windowDidBecomeVisibleNotification:)
                                                 name:UIWindowDidBecomeVisibleNotification
                                               object:nil];
}
    
- (void)windowDidBecomeVisibleNotification:(NSNotification *)notification {
    if ([notification.object isKindOfClass:NSClassFromString(@"SKStoreReviewPresentationWindow")]) {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [[UIApplication sharedApplication] performSelector:@selector(endIgnoringInteractionEvents) withObject:nil afterDelay:5];
    }
}
    
-(void) showRatePopup:(NSObject*) params
{
    if ([SKStoreReviewController class]) {
        [SKStoreReviewController requestReview] ;
    }
    else
    {
        NSString* msg = [NSString stringWithCString:"https://itunes.apple.com/us/app/denis-2017/id1309147152?ls=1&mt=8" encoding:NSUTF8StringEncoding];
        NSURL* nsUrl = [NSURL URLWithString:msg];
        [[UIApplication sharedApplication] openURL:nsUrl];
    }
}

-(void) showShareScreen:(NSObject*) params
{
    NSDictionary* parameters = (NSDictionary*)params;
    NSString* shareMessage = [parameters valueForKey:@"message"];
    NSString* shareURL = [parameters valueForKey:@"ShareURL"];
    NSString* title = [parameters valueForKey:@"title"];

    NSURL* urlToUse = [NSURL URLWithString:shareURL];
    NSDictionary* infoPlist = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"]];
    NSString* icon = [[infoPlist valueForKeyPath:@"CFBundleIcons.CFBundlePrimaryIcon.CFBundleIconFiles"] lastObject];
    UIImage* image = [UIImage imageNamed:icon];

    NSArray* dataToShare = @[shareMessage, image, urlToUse];//image

    AppController* appDelegate = (AppController*)[[UIApplication sharedApplication] delegate];
    UIViewController* controller = (UIViewController*)[appDelegate viewController];

    UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    [activityViewController setValue:title forKey:@"subject"];

    [activityViewController setExcludedActivityTypes:@[UIActivityTypePrint,
                                                       UIActivityTypeAddToReadingList,
                                                       UIActivityTypeSaveToCameraRoll,
                                                       UIActivityTypeAssignToContact,
                                                       UIActivityTypeCopyToPasteboard,
                                                       @"com.apple.reminders.RemindersEditorExtension", //disable 'Reminders'
                                                       @"com.apple.mobilenotes.SharingExtension", //disable 'Add to Notes'
                                                       @"com.google.Drive.ShareExtension" //disable 'Google Drive'
                                                       ]];

    if([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] != NSOrderedAscending)
    {
        activityViewController.popoverPresentationController.sourceView = controller.view;
        activityViewController.popoverPresentationController.sourceRect = CGRectMake(0,controller.view.frame.size.height, controller.view.frame.size.width, 400);
    }

    [controller presentViewController:activityViewController animated:YES completion:nil];

    //NOTE: check for selector support in order to use latest available (not deprecated) api for newwest iOS versions
    if([[UIApplication sharedApplication] respondsToSelector:(@selector(setCompletionWithItemsHandler:))])
    {
        [activityViewController setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError)
         {
             [IOSNDKHelper sendMessage:@"sharingCompleteCallback" withParameters:@{
                                                                                   @"isShareDone" : [NSNumber numberWithBool:completed]
                                                                                   }];
         }];
    }
    else
    {
        [activityViewController setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError)
         {
             [IOSNDKHelper sendMessage:@"sharingCompleteCallback" withParameters:@{
                                                                                   @"isShareDone" : [NSNumber numberWithBool:completed]
                                                                                   }];
         }];
    }
}

@end
