//
//  MapLayer.h
//  Strategy
//
//  Created by Vladimir Tolmachev on 27.06.14.
//
//
#ifndef __ScrollMenu_h__
#define __ScrollMenu_h__

#include "cocos2d.h"
#include "ml/macroses.h"
#include <map>
#include "ml/Scissor.h"
#include "ml/common.h"
#include "ml/MenuItem.h"

NS_CC_BEGIN;

class ScrollMenu : public Scissor<Layer>
{
	DECLARE_BUILDER( ScrollMenu );
	bool init();
public:
	bool onTouchBegan( Touch*, Event* );
	void onTouchEnded( Touch*, Event* );
	void onTouchMoved( Touch*, Event* );
	void onTouchCancelled( Touch*, Event* );

	void setEnabled( bool var );
	bool isEnabled()const;

	void setTouchEnabled( bool var );
	bool isTouchEnabled( )const;
	void setScrollEnabled( bool var );
	bool isScrollEnabled( )const;
	void setMouseScrollEnabled( bool var );
	bool isMouseScrollEnabled()const;

	mlMenuItem::Pointer push(
		const std::string & normalImage,
		const std::string & selectedImage,
		const std::string & disabledImage,
		const std::string & fontBMP,
		const std::string & text,
		const ccMenuCallback & callback );

	mlMenuItem::Pointer push(
		const std::string & normalImage,
		const std::string & selectedImage,
		const std::string & fontBMP,
		const std::string & text,
		const ccMenuCallback & callback );

	mlMenuItem::Pointer push(
		const std::string & normalImage,
		const std::string & selectedImage,
		const ccMenuCallback & callback );

	mlMenuItem::Pointer push(
		const std::string & normalImage,
		const ccMenuCallback & callback );

	void addItem( NodePointer item );
	void removeItem( NodePointer item );
	void removeAllItems();

	virtual Node* getChildByName( const std::string& name ) const override;

	void align( int cols );
	void divide();
	void clearDivide();

public:
	size_t getItemsCount( )const;
	Node* getItem( unsigned index );
	const Node* getItem( unsigned index )const;
	Node* getItemByName( const std::string & name );
	const Node* getItemByName( const std::string & name )const;

	MenuItem* getMenuItem( unsigned index );
	const MenuItem* getMenuItem( unsigned index )const;
	MenuItem* getMenuItemByName( const std::string & name );
	const MenuItem* getMenuItemByName( const std::string & name )const;

	std::vector<std::vector<NodePointer>> getRows();
private:
	Node* getItemForTouch( Touch *touch );

	void scrollBegan( Touch* );
	void scrollMoved( Touch* );
	void scrollEnded( Touch* );
	void scrollCanceled( Touch* );

	Point fitPosition( const Point & pos );
	Point fitPositionByGrid( const Point & pos );

	void select( Node * item );
	void unselect( Node * item );
	void activate( Node * item );

	void onMouseScroll( EventMouse * );
	void onArrowReleased( EventKeyboard::KeyCode code, Event* event );
	void scrollMoved( const Point & shift, bool smooth = false );

	void refreshScrollIndicator();
private:
	bool _touchEnabled;
	bool _scrollEnabled;
	bool _mouseScrollEnabled;

	NodePointer _selected;
	NodePointer _selectedOnTouchBegan;
	std::vector<NodePointer> _items;
	std::vector<std::vector<NodePointer>> _rows;
	std::vector<int> _divide;

	bool _scrolled;
	CC_SYNTHESIZE_READONLY( Point, _scrollAreaPos, ScrollAreaPos );

	CC_PROPERTY_PASS_BY_REF( int, _alignedCols, AlignedColums );

	CC_SYNTHESIZE( bool, _allowScrollX, AllowScrollByX );
	CC_SYNTHESIZE( bool, _allowScrollY, AllowScrollByY );

	CC_SYNTHESIZE_PASS_BY_REF( Size, _gridSize, GrisSize );
	CC_SYNTHESIZE_PASS_BY_REF( Point, _alignedStartPosition, AlignedStartPosition );
		
	CC_SYNTHESIZE( float, _mouseScrollSpeed, MouseScrollSpeed );
	NodePointer _scrollIndicator;

	EventListenerMouse * _mouseListener;
	EventListenerKeyboard* _arrowListener;
};

NS_CC_END;
#endif