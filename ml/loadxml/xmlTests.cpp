#include "xmlTests.h"
#include "xmlLoader.h"
#include "xmlProperties.h"
#include "ml/NodeExt.h"
#include "ml/ObjectFactory.h"

NS_CC_BEGIN;

#if DEV == 1
#define RETURN_ERROR(msg) {log(msg); return false;}

namespace xmlLoader
{
	static const char* xmlTest1File = "dev/xmltest/xmltesst1.xml";
	static const char* xmlTest2File1 = "dev/xmltest/xmltest2_1.xml";
	static const char* xmlTest2File2 = "dev/xmltest/xmltest2_2.xml";
	static const char* xmlTest2File3 = "dev/xmltest/xmltest2_3.xml";
	class xmlTest1 : public Node, public NodeExt
	{
		DECLARE_BUILDER( xmlTest1 );
		bool init()
		{
			initCounter++;
			return Node::init() && NodeExt::init();
		}
		bool init( const std::string& path )
		{
			initCounter++;
			if( Node::init() && NodeExt::init() )
			{
				NodeExt::load( path );
				return true;
			}
			return false;
		}
	public:
		virtual bool setProperty( int intproperty, const std::string & value )override
		{
			propertiesCounter[propertyTypeToStr( intproperty )]++;
			return NodeExt::setProperty( intproperty, value );
		}
		virtual bool loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode )override
		{
			loadXmlEntityCounter[tag]++;
			return NodeExt::loadXmlEntity( tag, xmlnode );
		}
		virtual void onLoaded()override
		{
			onLoadedCounter++;
			NodeExt::onLoaded();
		}

		bool test()
		{
			for( auto pair : propertiesCounter )
			{
				auto name = pair.first;
				auto count = strTo<int>( getParamCollection().get( name ) );
				if( count != 0 && count != pair.second )
					RETURN_ERROR( ("xmlLoaderTest: property [" + name + "] set [" + toStr( pair.second ) + "] time. should be [" + toStr( count ) + "] time").c_str() );
			}
			for( auto pair : loadXmlEntityCounter )
			{
				auto name = pair.first;
				auto count = strTo<int>( getParamCollection().get( name ) );
				if( count != 0 && count != pair.second )
					RETURN_ERROR( ("xmlLoaderTest: xmlEntity [" + name + "] set [" + toStr( pair.second ) + "] time. should be [" + toStr( count ) + "] time").c_str() );
			}
			if( initCounter != 1 )
			{
				RETURN_ERROR( ("xmlLoaderTest: init event calls [" + toStr( initCounter ) + "] time. should be one time").c_str() );
			}
			if( onLoadedCounter != 1 )
			{
				RETURN_ERROR( ("xmlLoaderTest: onLoaded event calls [" + toStr( onLoadedCounter ) + "] time. should be one time").c_str() );
			}
			return true;
		}
	public:
		int initCounter = 0;
		int onLoadedCounter = 0;
		std::map<std::string, int>  loadXmlEntityCounter;
		std::map<std::string, int> propertiesCounter;
	};
	xmlTest1::xmlTest1() {}
	xmlTest1::~xmlTest1() {}

	//test remove attributes from loaded doc
	bool test0()
	{
		pugi::xml_document doc;
		doc.load_file( xmlTest1File );
		auto root = doc.root().first_child();
		while( root.first_attribute() )
		{
			root.remove_attribute( root.first_attribute() );
		}
		auto root2 = doc.root().first_child();
		return !root.first_attribute();
	}

	//test loading: 
	//	every property should be set only once time
	//	on loaded chould be call once time
	bool test1()
	{
		auto node = xmlLoader::load_node<xmlTest1>( xmlTest1File );
		if( !node )
			RETURN_ERROR( "xmlLoaderTest: cannot load xmlTest1 from file" );
		if( !dynamic_cast<xmlTest1*>(node.ptr()) )
			RETURN_ERROR( "xmlLoaderTest: cannot cast loaded object to 'xmlTest1' class" );

		if( !node->test() )
			RETURN_ERROR( "xmlLoaderTest: execute xmlTest1::test with error" );

		auto child = node->getChildByName<xmlTest1*>( "testchildren" );
		if( !child )
			RETURN_ERROR( "xmlLoaderTest: cannot find children" );
		if( !child->test() )
			RETURN_ERROR( "xmlLoaderTest: execute xmlTest1::test for children with error" );
		log( "xmlLoaderTest: execution success. \nreturn true;" );
		return true;
	}
	bool test1_2()
	{
		auto node = xmlTest1::create( xmlTest1File );
		if( !node )
			RETURN_ERROR( "xmlLoaderTest: cannot load xmlTest1 from file" );
		if( !dynamic_cast<xmlTest1*>(node.ptr()) )
			RETURN_ERROR( "xmlLoaderTest: cannot cast loaded object to 'xmlTest1' class" );

		if( !node->test() )
			RETURN_ERROR( "xmlLoaderTest: execute xmlTest1::test with error" );

		auto child = node->getChildByName<xmlTest1*>( "testchildren" );
		if( !child )
			RETURN_ERROR( "xmlLoaderTest: cannot find children" );
		if( !child->test() )
			RETURN_ERROR( "xmlLoaderTest: execute xmlTest1::test for children with error" );
		log( "xmlLoaderTest: execution success. \nreturn true;" );
		return true;
	}

	bool test2()
	{
		auto node = xmlLoader::load_node( xmlTest2File1 );
		if( !node )
			RETURN_ERROR( "xmlLoaderTest: execute xmlTest2: (1)cannot load node " );
		node = xmlLoader::load_node( xmlTest2File2 );
		if( !node ) 
			RETURN_ERROR( "xmlLoaderTest: execute xmlTest2: (2)cannot load node " );
		auto menu = xmlLoader::load_node( xmlTest2File3 );
		if( !menu ||!dynamic_cast<Menu*>(menu.ptr()) )
			RETURN_ERROR( "xmlLoaderTest: execute xmlTest2: (3)cannot menu node " );
		return true;
	}

	bool tests()
	{
		if( FileUtils::getInstance()->isFileExist( xmlTest1File ) == false )
			return true;

		Factory::shared().book<xmlTest1>( "xmltest1" );
		Factory::shared().book<xmlTest1>( "xmltest2" );
		auto result =
			test0() &&
			test1() &&
			test1_2() &&
			test2();
		return result;
	}
}
#else
namespace xmlLoader{
	bool tests(){
		return true;
	}
}

#endif
NS_CC_END;