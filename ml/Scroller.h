#ifndef __SCROLLER_H__
#define __SCROLLER_H__
#include "cocos2d.h"
#include "ml/macroses.h"

NS_CC_BEGIN
class Scroller : public Layer
{
	DECLARE_BUILDER( Scroller );
	virtual bool init( )override;
public:
	const Size getContent() const;

	void setContent( const Size& size );
	void setVisibled( const Size& size );
	void setMaxScale( float scale );

	virtual void update( float dt )override;
	virtual void onEnter()override;
	virtual void onExit()override;

	void showMinScroll( bool useLowerScale );

	bool touchesBegan( const std::vector<Touch*>& touches );
	bool touchesMoved( const std::vector<Touch*>& touches );
	bool touchesEnded( const std::vector<Touch*>& touches );
	void touchesCancelled();
	void touchesCancelled( const std::vector<Touch*>& touches );
	
	const Vec2& getScrollPosition()const;
	void setScrollPosition( const Vec2& position );
	void setScrollScale( float scale );
	void setScroll( float scale, const Vec2& anchor );
	void enableScroller( bool enable );

	void setPosition( const Vec2& position ) override;
	void setPosition3D( const Vec3& position ) override;

	void mouseHover( Event*event );
	void enableMouseHover();
	void disableMouseHover();

	void setVisibledSizeScale( float scale );
protected:
	Vec2 fitPosition( const Vec2& position )const;
	float fitScale( float scale )const;
	void initTouchListener();
	void initMouseListener();
	void initKeyboardListener();
private:
	enum State
	{
		wait = 0,
		scroll = 1,
		scaling = 2,
	}_state;
	bool _wasAction;

	IntrusivePtr<Touch> _touchA;
	IntrusivePtr<Touch> _touchB;
	Point _startTouchA;
	Point _startTouchB;
	float _touchDistance;
	float _distanceOnStartScroll;

	Point _visibled;
	Point _content;
	float _maxScale;
	float _contentScale;

	Point _positionOnTouchBegan;
	Point _position;
	float _scale;

	bool _scaleEnabled;
	
	bool _mouseDown;
	float _scrollMouseHoverX;
	float _scrollMouseHoverY;

	float _scrollArrowKeyX;
	float _scrollArrowKeyY;

	Size _winSize;
	CC_SYNTHESIZE( float, _velocity, Velocity );
	CC_SYNTHESIZE( float, _border, Border );
	CC_SYNTHESIZE( float, _scaleLower, ScaleLower );
	CC_SYNTHESIZE( bool, _mouseHoverEnabled, MouseHoverEnabled );
};

NS_CC_END

#endif