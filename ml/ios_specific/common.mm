//
//  RateMeLayer.c
//  IslandDefense
//
//  Created by Владимир Толмачев on 04.01.15.
//
//

#include "AppController.h"
#include "Config.h"
#include "common.h"

NS_CC_BEGIN

void openUrl( const std::string& url )
{
	const char * URL = url.c_str();
	[AppController openURL:URL];
}


NS_CC_END;