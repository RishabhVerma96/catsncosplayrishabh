#ifndef __MainThreadFunc_h__
#define __MainThreadFunc_h__
#include "cocos2d.h"
#include "Singlton.h"

#define RUIN_MAINTHREAD_BEGIN MainThreadFunc::shared().push_back( []()
#define RUIN_MAINTHREAD_END );

class MainThreadFunc : private cocos2d::Node, public Singlton<MainThreadFunc>
{
public:
	virtual void onCreate() override
	{
		_mainThreadId = std::this_thread::get_id();
		auto scheduler = cocos2d::Director::getInstance()->getScheduler();
		auto callback = std::bind( &MainThreadFunc::update, this, std::placeholders::_1 );
		scheduler->schedule( callback, this, 0, false, "MainThreadFunc" );
	}

	void push_back( std::function<void()> func )
	{
		auto id = std::this_thread::get_id();
		if( _mainThreadId != id ) _mutex.lock();
		_functions.push_back( func );
		if( _mainThreadId != id ) _mutex.unlock();
	}

	void push_front( std::function<void()> func )
	{
		auto id = std::this_thread::get_id();
		if( _mainThreadId != id ) _mutex.lock();
		_functions.push_front( func );
		if( _mainThreadId != id ) _mutex.unlock();
	}

private:
	virtual void update( float dt ) override
	{
		_mutex.lock();
		for( auto f : _functions )
		{
			f();
		}
		_functions.clear();
		_mutex.unlock();
	}
private:
	std::list<std::function<void()>> _functions;
	std::mutex _mutex;
	std::thread::id _mainThreadId;
};

#endif