#ifndef __KeyboardListener__
#define __KeyboardListener__
#include "Observer.h"
#include "ml/macroses.h"
#include "Singlton.h"

NS_CC_BEGIN
class KeyboardListener : public Singlton<KeyboardListener>
{
public:
	KeyboardListener();
	~KeyboardListener();
	void enable();
	void disable();
protected:
	void onKeyReleased( EventKeyboard::KeyCode key, Event *event );
public:
	ObServer < KeyboardListener, std::function<bool( EventKeyboard::KeyCode )>> observer;
private:
	bool _enabled;
	EventListenerKeyboard* _listener;
};
NS_CC_END
#endif
