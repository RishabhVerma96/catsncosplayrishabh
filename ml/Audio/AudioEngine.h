//
//  AudioEngine.h
//  TowerDefence
//
//  Created by Vladimir Tolmachev on 04.02.14.
//
//

#ifndef __TowerDefence__AudioEngine__
#define __TowerDefence__AudioEngine__
#ifndef USE_COCOSDENSHION
#define USE_COCOSDENSHION 0
#endif

#include "cocos2d/cocos/audio/include/AudioEngine.h"
#include "ml/Singlton.h"
#include "cocos2d.h"
NS_CC_BEGIN;

class AudioEngine : public Singlton<AudioEngine>
{
	friend class Singlton<AudioEngine>;
	friend class AudioMenu;
public:
	typedef experimental::AudioEngine CocosAudioEngine;
public:
	static void callback_isSoundEnabled( std::function<bool()> isSoundEnabled );
	static void callback_isMusicEnabled( std::function<bool()> isMusicEnabled );
	static void callback_setSoundEnabled( std::function<void(bool)> setSoundEnabled );
	static void callback_setMusicEnabled( std::function<void(bool)> setMusicEnabled );

	static void callback_getSoundVolume( std::function<float()> loadSoundVolume );
	static void callback_getMusicVolume( std::function<float()> loadMusicVolume );
	static void callback_setSoundVolume( std::function<void( float )> saveSoundVolume );
	static void callback_setMusicVolume( std::function<void( float )> saveMusicVolume );
public:
	virtual ~AudioEngine();
	void soundEnabled( bool mode );
	void musicEnabled( bool mode );
	bool isSoundEnabled()const;
	bool isMusicEnabled()const;
	
	void setSoundVolume( float volume );
	void setMusicVolume( float volume );
	float getSoundVolume();
	float getMusicVolume();
	float getActualSoundVolume();
	float getActualMusicVolume();

	void playMusic( const std::string & path, bool lopped = true );
	int  playEffect( const std::string & path, bool lopped = false, float pan = 0 );
	/*
	if I want use sound as callback.? need use this method
	*/
	void playEffect2( const std::string & path );
	void stopEffect( int id );
	void pauseEffect( int id );
	void resumeEffect( int id );
	
	void stopMusic();
	
	void stopAllEffects();
	void pauseAllEffects();
	void resumeAllEffects();

	void pauseBackgroundMusic();
	void resumeBackgroundMusic();
private:
	AudioEngine();
	virtual void onCreate();
private:
	std::string m_currentMusic;
	bool _soundEnabled;
	bool _musicEnabled;
	std::list<int> _effectIDs;
	int _backgroundMusicID;
};

NS_CC_END;
#endif /* defined(__TowerDefence__AudioEngine__) */
