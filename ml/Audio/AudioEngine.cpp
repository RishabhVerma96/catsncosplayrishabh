//
//  AudioEngine.cpp
//  TowerDefence
//
//  Created by Vladimir Tolmachev on 04.02.14.
//
//

#if USE_ENGINE == COCOSDENSHION
#include "audio/include/SimpleAudioEngine.h"
#endif
#include "ml/Audio/AudioEngine.h"
#include "ml/loadxml/xmlLoader.h"
//#include "UserData.h"


NS_CC_BEGIN;
#if USE_ENGINE == COCOSDENSHION
using namespace CocosDenshion;
#endif

#define COCOSDENSHION 1
#define COCOSENGINE 2

#if USE_ENGINE == COCOSDENSHION
#	define USE_ENGINE COCOSDENSHION
#else 
#	define USE_ENGINE COCOSENGINE
#endif

std::function<bool()> getIsSoundEnabled;
std::function<bool()> getIsMusicEnabled;
std::function<void(bool)> setSoundEnabled;
std::function<void(bool)> setMusicEnabled;

void AudioEngine::callback_isSoundEnabled( std::function<bool()> func )
{ getIsSoundEnabled = func; }
void AudioEngine::callback_isMusicEnabled( std::function<bool()> func )
{ getIsMusicEnabled = func; }
void AudioEngine::callback_setSoundEnabled( std::function<void(bool)> func )
{ setSoundEnabled = func; }
void AudioEngine::callback_setMusicEnabled( std::function<void(bool)> func )
{ setMusicEnabled = func; }


std::function<float()> loadSoundVolume;
std::function<float()> loadMusicVolume;
std::function<void( float )> saveSoundVolume;
std::function<void( float )> saveMusicVolume;

void AudioEngine::callback_getSoundVolume( std::function<float()> func )
{
	loadSoundVolume = func;
}
void AudioEngine::callback_getMusicVolume( std::function<float()> func )
{
	loadMusicVolume = func;
}
void AudioEngine::callback_setSoundVolume( std::function<void( float )> func )
{
	saveSoundVolume = func;
}
void AudioEngine::callback_setMusicVolume( std::function<void( float )> func )
{
	saveMusicVolume = func;
}

AudioEngine::AudioEngine()
#if PC == 1
: _backgroundMusicID( -1 )
#endif
{}

void AudioEngine::onCreate()
{
	soundEnabled( isSoundEnabled() );
	musicEnabled( isMusicEnabled() );

	setSoundVolume( getSoundVolume() );
	setMusicVolume( getMusicVolume() );
}

AudioEngine::~AudioEngine()
{}

void AudioEngine::soundEnabled( bool mode )
{
	_soundEnabled = mode;
	if( setSoundEnabled ) setSoundEnabled( mode );	
	setSoundVolume( getSoundVolume() );
}

void AudioEngine::musicEnabled( bool mode )
{
	bool playMusic = mode;
	_musicEnabled = mode;
	if( setMusicEnabled ) setMusicEnabled( mode );
	setMusicVolume( getMusicVolume() );

#if USE_ENGINE == COCOSDENSHION && CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 && PC != 1
	if( playMusic )
	{
		auto music = m_currentMusic;
		m_currentMusic.clear();
		this->playMusic( music, true );
	}
	else
	{
		stopMusic();
	}
#endif
}

bool AudioEngine::isSoundEnabled()const
{
	if( getIsSoundEnabled ) return getIsSoundEnabled();
	return true;
}

bool AudioEngine::isMusicEnabled()const
{
	if( getIsMusicEnabled ) return getIsMusicEnabled();
	return true;
}

void AudioEngine::setSoundVolume( float volume )
{
	assert( volume >= 0.f && volume <= 1.f );

	if( saveSoundVolume ) saveSoundVolume( volume );

	float actualVolume = _soundEnabled ? volume : 0.f;
#if USE_ENGINE == COCOSDENSHION
	SimpleAudioEngine::getInstance()->setEffectsVolume( actualVolume );
#elif USE_ENGINE == COCOSENGINE
	for( int id : _effectIDs )
	{
		CocosAudioEngine::setVolume(id, actualVolume);
	}
#endif 

#if USE_ENGINE == COCOSDENSHION && CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 && PC != 1
	if( actualVolume == 0.f )
	{
		SimpleAudioEngine::getInstance()->stopAllEffects();
	}
#endif
}

void AudioEngine::setMusicVolume( float volume )
{
	assert( volume >= 0.f && volume <= 1.f );

	if( saveMusicVolume ) saveMusicVolume( volume );

	float actualVolume = _musicEnabled ? volume : 0.f;
#if USE_ENGINE == COCOSDENSHION
	SimpleAudioEngine::getInstance()->setBackgroundMusicVolume( actualVolume );
#elif USE_ENGINE == COCOSENGINE
	CocosAudioEngine::setVolume( _backgroundMusicID, actualVolume );
#endif
}

float AudioEngine::getSoundVolume()
{
	if( loadSoundVolume ) return loadSoundVolume();
	return 1.f;
}

float AudioEngine::getMusicVolume()
{
	if( loadMusicVolume ) return loadMusicVolume();
	return 0.3f;
}

void AudioEngine::playMusic( const std::string & path, bool lopped )
{
#if USE_ENGINE == COCOSDENSHION && CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 && PC != 1
	if( isMusicEnabled() == false )
	{
		return;
	}
#endif
	std::string pathmusic = path;
	pathmusic = xmlLoader::macros::parse( pathmusic );
	if( pathmusic == m_currentMusic )
	{
		return;
	}
	m_currentMusic = pathmusic;
	if( FileUtils::getInstance()->isFileExist(pathmusic) )
	{
		pathmusic = FileUtils::getInstance()->fullPathForFilename( pathmusic );
#if USE_ENGINE == COCOSDENSHION
		SimpleAudioEngine::getInstance()->playBackgroundMusic( pathmusic.c_str(), lopped );
#elif USE_ENGINE == COCOSENGINE
		float actualVolume = _musicEnabled ? getMusicVolume() : 0.f;
		CocosAudioEngine::stop( _backgroundMusicID );
		_backgroundMusicID = CocosAudioEngine::play2d( pathmusic, lopped, actualVolume );

		auto callback = std::bind( [this](int, std::string){
			_backgroundMusicID = -1;
		}, std::placeholders::_1, std::placeholders::_2 );
		CocosAudioEngine::setFinishCallback( _backgroundMusicID, callback );
#endif
	}
}

int AudioEngine::playEffect( const std::string & path, bool lopped, float pan )
{
#if USE_ENGINE == COCOSDENSHION && CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 && PC != 1
	if( isSoundEnabled() == false )
	{
		return -1;
	}
#endif
	std::string sound = path;
	sound = xmlLoader::macros::parse( path );
	if( FileUtils::getInstance()->isFileExist(sound) )
	{
		sound = FileUtils::getInstance()->fullPathForFilename(sound);
#if USE_ENGINE == COCOSDENSHION
		return SimpleAudioEngine::getInstance()->playEffect( sound.c_str(), lopped );
#elif USE_ENGINE == COCOSENGINE
		float actualVolume = _soundEnabled ? getSoundVolume() : 0.f;
		int id = CocosAudioEngine::play2d( sound, lopped, actualVolume );
		_effectIDs.push_back( id );

		auto callback = std::bind( [this]( int id, std::string ){
			_effectIDs.remove( id );
		}, std::placeholders::_1, std::placeholders::_2 );
		CocosAudioEngine::setFinishCallback( id, callback );

		return id;
#endif
	}
	return -1;
}

void AudioEngine::playEffect2( const std::string & path )
{
	playEffect( path, false, 0 );
}

void AudioEngine::stopEffect( int id )
{
#if USE_ENGINE == COCOSDENSHION
	SimpleAudioEngine::getInstance()->stopEffect( id );
#elif USE_ENGINE == COCOSENGINE
	CocosAudioEngine::stop( id );
	_effectIDs.remove( id );
#endif
}

void AudioEngine::pauseEffect( int id )
{
#if USE_ENGINE == COCOSDENSHION
	SimpleAudioEngine::getInstance()->pauseEffect( id );
#elif USE_ENGINE == COCOSENGINE
	CocosAudioEngine::pause( id );
#endif
}

void AudioEngine::resumeEffect( int id )
{
#if USE_ENGINE == COCOSDENSHION
	SimpleAudioEngine::getInstance()->resumeEffect( id );
#elif USE_ENGINE == COCOSENGINE
	CocosAudioEngine::resume( id );
#endif
}

void AudioEngine::stopMusic()
{
#if USE_ENGINE == COCOSDENSHION
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
#elif USE_ENGINE == COCOSENGINE
	CocosAudioEngine::stop( _backgroundMusicID );
	_backgroundMusicID = -1;
#endif
}

void AudioEngine::stopAllEffects()
{
#if USE_ENGINE == COCOSDENSHION
	SimpleAudioEngine::getInstance()->stopAllEffects();
#elif USE_ENGINE == COCOSENGINE 
	auto effectIDs = _effectIDs;
	for( int id : effectIDs )
	{
		stopEffect( id );
	}
#endif
}

void AudioEngine::pauseAllEffects()
{
#if USE_ENGINE == COCOSDENSHION
	SimpleAudioEngine::getInstance()->pauseAllEffects();
#elif USE_ENGINE == COCOSENGINE
	auto backgroundState = CocosAudioEngine::getState( _backgroundMusicID );
	CocosAudioEngine::pauseAll();
	if( backgroundState == CocosAudioEngine::AudioState::PLAYING )
		CocosAudioEngine::resume( _backgroundMusicID );
#endif
}

void AudioEngine::resumeAllEffects()
{
#if USE_ENGINE == COCOSDENSHION
	SimpleAudioEngine::getInstance()->resumeAllEffects();
#elif USE_ENGINE == COCOSENGINE
	auto backgroundState = CocosAudioEngine::getState( _backgroundMusicID );
	CocosAudioEngine::resumeAll();
	if( backgroundState == CocosAudioEngine::AudioState::PAUSED )
		CocosAudioEngine::pause( _backgroundMusicID );
#endif
}

void AudioEngine::pauseBackgroundMusic()
{
#if USE_ENGINE == COCOSDENSHION
	SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
#elif USE_ENGINE == COCOSENGINE
	CocosAudioEngine::pause( _backgroundMusicID );
#endif
}

void AudioEngine::resumeBackgroundMusic()
{
#if USE_ENGINE == COCOSDENSHION
	SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
#elif USE_ENGINE == COCOSENGINE
	CocosAudioEngine::resume( _backgroundMusicID );
#endif
}

NS_CC_END;
