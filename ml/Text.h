/******************************************************************************/
/*
 * Copyright 2014-2015 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: Defense of Greece
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef __Text_h__
#define __Text_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "ml/NodeExt.h"
#include "ml/loadxml/xmlProperties.h"
#include "ml/common.h"
NS_CC_BEGIN

//Text::createWithBMFont
class Text : public Label, public NodeExt
{
	friend bool xmlLoader::setProperty( Node* node, const int property, const std::string & rawvalue );
	DECLARE_BUILDER( Text );
	bool init();
	bool init( const std::string& font, const std::string& text, float width = 0, TextHAlignment alignment = TextHAlignment::LEFT, const Vec2& imageOffset = Vec2::ZERO );
public:
	virtual bool setBMFontFilePath( const std::string& bmfontFilePath, const Vec2& imageOffset = Vec2::ZERO, float fontSize = 0 )override;
	virtual void setString( const std::string& newString ) override;
	virtual void setScale( float scaleX, float scaleY )override;
	void setWidth( float width );
	Strech getStretch()const { return _strech; }
	void setStretch( const Strech& strech );
	void setSourceString( const std::string& newString );
	void useLocation( bool mode );
	unsigned getFontSize()const;
	void setFontSize( unsigned size );
protected:
	void onChangeLocalisation();
private:
	bool _useStrech;
	Strech _strech;
	Vec2 _defaultScale;
	float _defaultWidth;
	std::string _fontBM;
	std::string _sourceString;
	CC_PROPERTY_PASS_BY_REF( std::string, _ttfFontName, TTFFontName );
	CC_PROPERTY_PASS_BY_REF( std::string, _systemFontName, SystemFontName );
	unsigned _fontSize;
};

NS_CC_END
#endif // #ifndef Text
