#include "RandomNumberGenerator.h"

std::random_device   RandomNumberGenerator::m_rd;
std::mt19937         RandomNumberGenerator::m_rng = std::mt19937(RandomNumberGenerator::m_rd());

int RandomNumberGenerator::getRandomInteger(const int rangeStart, const int rangeEnd)
{
    std::uniform_int_distribution<> randomizer(rangeStart, rangeEnd);
    return randomizer(m_rng);
}

double RandomNumberGenerator::getRandomNumber(const double rangeStart, const double rangeEnd)
{
    std::uniform_real_distribution<> randomizer(rangeStart, rangeEnd);
    return randomizer(m_rng);
}