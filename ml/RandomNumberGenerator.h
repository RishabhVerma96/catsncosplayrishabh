#ifndef RandomNumberGenerator_h__
#define RandomNumberGenerator_h__

#include <stdio.h>
#include <random>

class RandomNumberGenerator
{
private:
    static std::random_device   m_rd;
    static std::mt19937         m_rng;

public:
    static int    getRandomInteger(const int rangeStart, const int rangeEnd);
    static double getRandomNumber(const double rangeStart, const double rangeEnd);
    static double getRandomPercent();
    static bool   getRandomBool();
};

inline /*static*/ double RandomNumberGenerator::getRandomPercent()
{
    return getRandomNumber(0.0, 100.0);
}

inline /*static*/ bool RandomNumberGenerator::getRandomBool()
{
    return getRandomInteger(0, 1) == 1;
}

#endif //RandomNumberGenerator_h__
