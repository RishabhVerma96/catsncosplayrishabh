#include "Scroller.h"

NS_CC_BEGIN

Scroller::Scroller()
: _state( State::wait )
, _touchA( nullptr )
, _touchB( nullptr )
, _touchDistance( 0 )
, _maxScale( 2 )
, _scale( 1 )
, _wasAction(false)
, _scaleEnabled(false)
, _velocity(300.f)
, _border(20.f)
, _scaleLower(1.f)
, _mouseHoverEnabled(false)
, _mouseDown(false)
, _scrollArrowKeyX(0.f)
, _scrollArrowKeyY(0.f)
, _scrollMouseHoverX(0.f)
, _scrollMouseHoverY(0.f)
, _contentScale(1.f)
{
}

Scroller::~Scroller()
{}

bool Scroller::init()
{
	do
	{
		CC_BREAK_IF( !Layer::init() );

		_scale = getScale();
		_position = getPosition();
		_winSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();

		initTouchListener();
		initMouseListener();
#if PC == 1
		initKeyboardListener();
#endif


		return true;
	}
	while( false );
	return false;
}

void Scroller::initTouchListener()
{
	auto touchListener = EventListenerTouchAllAtOnce::create();
	touchListener->onTouchesBegan = CC_CALLBACK_2( Scroller::onTouchesBegan, this );
	touchListener->onTouchesMoved = CC_CALLBACK_2( Scroller::onTouchesMoved, this );
	touchListener->onTouchesEnded = CC_CALLBACK_2( Scroller::onTouchesEnded, this );
	touchListener->onTouchesCancelled = CC_CALLBACK_2( Scroller::onTouchesCancelled, this );
	_eventDispatcher->addEventListenerWithSceneGraphPriority( touchListener, this );
}

void Scroller::initMouseListener()
{
	auto listener = EventListenerMouse::create();
	listener->onMouseScroll = [this]( cocos2d::Event* event ){
		EventMouse* mouseEvent = dynamic_cast<EventMouse*>(event);
		float z = mouseEvent->getScrollY();
		Point p = mouseEvent->getLocation();
		p.y = Director::getInstance()->getOpenGLView()->getDesignResolutionSize().height - p.y;

		float scale = getScale();
		scale += z *0.2f;
		if( _scaleEnabled )
			setScroll( scale, p );
	};
	listener->onMouseMove = std::bind( &Scroller::mouseHover, this, std::placeholders::_1 );
	listener->onMouseDown = [this]( cocos2d::Event* event )
	{
		_mouseDown = true;
	};
	listener->onMouseUp = [this]( cocos2d::Event* event )
	{
		_mouseDown = false;
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority( listener, this );
}

void Scroller::initKeyboardListener()
{
	const float speed = 1.f;
	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = [this, speed](EventKeyboard::KeyCode code, Event* event ){
		switch( code )
		{
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			_scrollArrowKeyY = -speed;
			break;
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			_scrollArrowKeyY = speed;
			break;
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			_scrollArrowKeyX = speed;
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			_scrollArrowKeyX = -speed;
			break;
		default:
			break;
		}
	};
	listener->onKeyReleased = [this]( EventKeyboard::KeyCode code, Event* event ) {
		switch( code )
		{
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			_scrollArrowKeyY = 0.f;
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			_scrollArrowKeyX = 0.f;
		default:
			break;
		}
	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority( listener, this );
}

const Size Scroller::getContent() const
{
	return Size( _content );
}

void Scroller::setContent( const Size& size )
{
	_content = Point( size );
}

void Scroller::setVisibled( const Size& size )
{
	_visibled = Point( size );
	setContentSize( size );
}

void Scroller::setMaxScale( float scale )
{
	_maxScale = scale;
}

void Scroller::update( float dt )
{
	setScale( _scale );

	if( _mouseHoverEnabled && !_mouseDown )
	{
		_position.x += _scrollMouseHoverX * dt * _velocity;
		_position.y += _scrollMouseHoverY * dt * _velocity;
	}

	_position.x += _scrollArrowKeyX * dt * _velocity;
	_position.y += _scrollArrowKeyY * dt * _velocity;

	setPosition( _position );
}

void Scroller::onEnter()
{
	Layer::onEnter();
	scheduleUpdate();
	setScroll( _scale, _position );
	_state = State::wait;
}

void Scroller::onExit()
{
	Layer::onExit();
	_state = State::wait;
	_touchA.reset( nullptr );
	_touchB.reset( nullptr );
}

void Scroller::showMinScroll( bool useLowerScale )
{
	float k = useLowerScale ? _scaleLower : 1.f;
	float scale = 0.1f;
	float minx = _content.x != 0 ? _visibled.x / (_content.x*k) : 1;
	float miny = _content.y != 0 ? _visibled.y / (_content.y*k) : 1;
	scale = std::max( scale, minx );
	scale = std::max( scale, miny );
	scale = std::min( scale, _maxScale );
	setScale( scale );
}

bool Scroller::touchesBegan( const std::vector<Touch*>& touches )
{
	for( auto touch : touches )
	{
		switch( _state )
		{
			case State::wait:
				if( !_touchA )
				{
					_touchA = touch;
					_startTouchA = touch->getLocation();
				}
				else if( !_touchB )
				{
					_touchB = touch;
					_startTouchB = touch->getLocation();
				}
				_state = State::scroll;
				_positionOnTouchBegan = _position;
				_wasAction = false;
				break;
			case State::scroll:
				if( !_touchA )
				{
					_touchA = touch;
				}
				else if( !_touchB )
				{
					_touchB = touch;
				}
				assert( _touchB && _touchB );
				_startTouchA = _touchA->getLocation();
				_startTouchB = _touchB->getLocation();
				_touchDistance = _startTouchB.getDistance( _startTouchA );
				_distanceOnStartScroll = _touchDistance;
				_state = State::scaling;
				break;
			default:
				break;
		}
	}
	return _wasAction;
}

bool Scroller::touchesMoved( const std::vector<Touch*>& touches )
{
	switch( _state )
	{
		case State::scroll:
		{
			auto touch = touches[0];

			auto location = touch->getLocation();
			auto delta = touch->getDelta();

			_position += delta;
			setScroll( _scale, _position );

			_wasAction = _wasAction || _positionOnTouchBegan.getDistance( _position ) > 50;
			break;
		}
		case State::scaling:
		{
			if( _touchA && _touchB )
			{
				auto locA = _touchA->getLocation();
				auto locB = _touchB->getLocation();
				auto center = (locA + locB) / 2;

				float distance = locA.getDistance( locB );
				float ds = _touchDistance > 0 ? distance / _touchDistance : 0;
				_touchDistance = distance;
				float scale = _scale;
				scale *= ds;
				if( _scaleEnabled ) 
					setScroll( scale, center );

				_wasAction = _wasAction || std::fabs( _distanceOnStartScroll - distance ) > 50;
				break;
			}
		}
		default:
			break;
	}
	return _wasAction;
}

bool Scroller::touchesEnded( const std::vector<Touch*>& touches )
{
	for( auto touch : touches )
	{
		switch( _state )
		{
			case State::scroll:
				_touchA.reset( nullptr );
				_touchB.reset( nullptr );
				_state = State::wait;
				break;
			case State::scaling:
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
				_touchA.reset(nullptr);
				_touchB.reset(nullptr);
				_state = State::wait;
#else
				if( touch == _touchA || (_touchA && _touchA->getID() == touch->getID()) )
				{
					_touchA.reset( nullptr );
					_state = State::scroll;
				}
				else if( touch == _touchB || (_touchB && _touchB->getID() == touch->getID()) )
				{
					_touchB.reset( nullptr );
					_state = State::scroll;
				}
#endif
				break;
			default:
				break;
		}
	}
	return _wasAction;
}

void Scroller::touchesCancelled()
{
	_state = State::wait;
	_touchA.reset( nullptr );
	_touchB.reset( nullptr );
}

void Scroller::touchesCancelled( const std::vector<Touch*>& touches )
{
	_state = State::wait;	
	_touchA.reset( nullptr );
	_touchB.reset( nullptr );
}

const Vec2& Scroller::getScrollPosition()const
{
	return _position;
}

void Scroller::setScrollPosition( const Vec2& position )
{
	_position = position;
}

void Scroller::setScrollScale( float scale )
{
	_scale = scale;
}

void Scroller::setScroll( float newscale, const Vec2& anchor )
{
	float scale = _scale;
	float fitscale = fitScale( newscale );
	Vec2 position = _position - anchor;

	position *= 1.f/ scale;
	position *= fitscale;
	position += anchor;
	setScale( fitscale );
	position = fitPosition( position );
	setScale( scale );

	_position = position;
	_scale = fitscale;
}

Vec2 Scroller::fitPosition( const Vec2& position )const
{
	float scale = getScale();
	auto root = getParent();
	while( root )
	{
		scale = scale * std::fabs( root->getScaleX() );
		root = root->getParent();
	}
	Vec2 min;
	min = Vec2(
		_visibled.x * _contentScale - _content.x * scale,
		_visibled.y * _contentScale - _content.y * scale
		);
	auto max = Vec2::ZERO;

	auto pos = position;
	if( min.x < 0 )
	{
		pos.x = std::max( pos.x, min.x );
		pos.x = std::min( pos.x, max.x );
	}
	else
	{
		pos.x = min.x *0.5f;
	}
	if( min.y < 0 )
	{
		pos.y = std::max( pos.y, min.y );
		pos.y = std::min( pos.y, max.y );
	}
	else
	{
		pos.y = min.y *0.5f;
	}
	return pos;
}

float Scroller::fitScale( float scale )const
{
	float minx = _content.x != 0 ? _visibled.x / (_content.x*_scaleLower) : 1;
	float miny = _content.y != 0 ? _visibled.y / (_content.y*_scaleLower) : 1;
	scale = std::max( scale, minx );
	scale = std::max( scale, miny );
	scale = std::min( scale, _maxScale );
	return scale;
}


void Scroller::enableScroller( bool enable )
{
	_scaleEnabled = enable;
}

void Scroller::setPosition( const Vec2& position )
{
	_position = fitPosition( position );
	Layer::setPosition( _position );
}


void Scroller::setPosition3D( const Vec3& position )
{
	setPositionZ( position.z );
	setPosition( Point( position.x, position.y ) );
}

void Scroller::mouseHover( Event*event )
{
	EventMouse* em = dynamic_cast<EventMouse*>(event);
	assert( em );
	if( !em )
		return;

	float border = _border;
	float sx( 0.f ), sy( 0.f );

	if( em->getCursorX() < border )
		sx = +std::fabs( 1 - em->getCursorX() / border );
	else if( em->getCursorX() > (_winSize.width - border) )
		sx = -std::fabs( 1 - (_winSize.width - em->getCursorX()) / border );

	if( em->getCursorY() < border )
		sy = +std::fabs( 1 - em->getCursorY() / border );
	else if( em->getCursorY() > (_winSize.height - border) )
		sy = -std::fabs( 1 - (_winSize.height - em->getCursorY()) / border );

	_scrollMouseHoverX = sx;
	_scrollMouseHoverY = sy;

	_scrollMouseHoverX = std::min( _scrollMouseHoverX, _velocity );
	_scrollMouseHoverY = std::min( _scrollMouseHoverY, _velocity );
}

void Scroller::enableMouseHover()
{
	_mouseHoverEnabled = true;
}

void Scroller::disableMouseHover()
{
	_mouseHoverEnabled = false;
}

void Scroller::setVisibledSizeScale( float scale )
{
	_contentScale = scale;
}

NS_CC_END
