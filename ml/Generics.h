#pragma once

#include <string>

NS_CC_BEGIN;

template <typename T> T strTo(const std::string &value);
template <typename T> std::string toStr(T value);

NS_CC_END;