//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "ObjectFactory.h"
#include "Events.h"
#include "ScrollMenu.h"
#include "ml/MenuItem.h"
#include "Slider.h"
#include "ml/NodeExt.h"
#include "ml/Text.h"
NS_CC_BEGIN;



Factory::Factory()
{
	book<Node>("node");
	book<Sprite>( "sprite" );
	book<Text>( "bmlabel" );
	book<Text>( "label" );
	book<Menu>( "menu" );
	book<ScrollMenu>( "scrollmenu" );
	book<mlMenuItem>( "menuitem" );
	book<Layer>( "layer" );
	book<LayerExt>( "layerext" );
	book<NodeExt_>( "nodeext" );
	book<SpriteExt>( "spriteext" );
	book<MenuExt>( "menuext" );
	book_pointer<ProgressTimer>( "progresstimer" );
	book<mlSlider>( "slider" );

	book<EventAction>( "action" );
	book<EventRunAction>( "runaction" );
	book<EventRunEvent>("runevent");
	book<EventStopAction>("stopaction");
	book<EventStopAllAction>( "stopallaction" );
	book<EventSetProperty>( "setproperty" );
	book<EventPlaySound>( "playsound" );
	book<EventCreateNode>( "createnode" );
}

IntrusivePtr<cocos2d::Ref> Factory::build( const std::string & key )
{
	bool isreg = m_objects.find( key ) != m_objects.end();
	if( !isreg && key.empty() == false )
		CCLOG( "Class with key [%s] not registred", key.c_str() );
	return isreg ? m_objects[key]->build() : nullptr;
};


NS_CC_END
