#include "ml/Config.h"
#include "ml/common.h"
#include "ml/pugixml/pugixml.hpp"

NS_CC_BEGIN

void Config::onCreate()
{
	std::string platform("other");
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	platform = "android";
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
	platform = "ios";
#elif CC_TARGET_PLATFORM == CC_PLATFORM_WINRT
	platform = "winrt";
#elif PC == 1
	platform = "steam";
#endif

	pugi::xml_document doc;
	doc.load_file("ini/config.xml");
	auto root = doc.root().first_child();
	auto platformXml = root.child("platforms").child(platform.c_str());

	auto parse = [this] (pugi::xml_node node)
	{
		for (auto child : node)
		{
			auto name = child.name();
			auto value = child.text().as_string();
			set(name, value);
		}
	};
	parse(root);
	parse(platformXml);
}

void Config::setDefaultValue(const std::string& name, const std::string& value)
{
	if (isExist(name) == false)
	{
		set(name, value);
	}
}

std::string Config::get(const std::string& name)
{
#if USE_CHEATS == 1
	if (isExist(name) == false)
	{
		MessageBox(("Configuration: \n not exist parameter [" + name + "]. Please define it (ini/config.xml)").c_str(), "Config");
	}
#endif
	return ParamCollection::get(name, "");
}

NS_CC_END