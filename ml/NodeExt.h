//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#ifndef __NodeExt_h__
#define __NodeExt_h__
#include "cocos2d.h"
#include "ml/macroses.h"
#include "Events.h"
#include "loadxml/xmlLoader.h"
#include "ml/common.h"
#include "ml/ParamCollection.h"
NS_CC_BEGIN

class SmartScene;

class NodeExt
{
public:
	bool runEvent( const std::string & eventname );
	ActionPointer getAction( const std::string & name );
	virtual Node* as_node_pointer();

	Node * getChildByPath( const std::string & path_names );
	Node * getChildByPath( const std::list<int> path_tags );
	virtual ccMenuCallback get_callback_by_description( const std::string & name ) { return nullptr; }

	ParamCollection& getParamCollection();
	const ParamCollection& getParamCollection()const;
public:
	NodeExt();
	virtual ~NodeExt();

	virtual bool init();
	virtual void load( const std::string & directory, const std::string & xmlFile )final;
	virtual void load( const std::string & pathToXmlFile )final;
	virtual void load( pugi::xml_node & root );
	virtual void onLoaded();

public:
	virtual bool setProperty( int intproperty, const std::string & value );
	virtual bool setProperty( const std::string & stringproperty, const std::string & value );
	virtual bool loadXmlEntity( const std::string & tag, pugi::xml_node & xmlnode );
	void loadActions( const pugi::xml_node & xmlnode );
	void loadEvents( const pugi::xml_node & xmlnode );
	void loadParams( const pugi::xml_node & xmlnode );
private:
	std::map<std::string, EventsList> _events;
	std::map<std::string, ActionPointer> _actions;
	ParamCollection _params;
};

class LayerExt: public Layer, public NodeExt
{
	DECLARE_BUILDER( LayerExt );
	bool init();
public:
	virtual void onEnter()override;
	virtual ccMenuCallback get_callback_by_description( const std::string & name )override;
	virtual void appearance();
	virtual void disappearance();
	SmartScene* getSmartScene();
	void setDisapparanceOnBackButton();

	void initBlockLayer(const std::string& image);
	void pushBlockLayer( bool showWaitProcess, float duration );
	void popBlockLayer();
private:
	LayerExt::Pointer _blockLayer;
	NodePointer _blockLayerWaitProcess;
	ActionPointer _blockLayerAction;
	bool _firstEntrance;
};

class LayerBlur
{
public:
	LayerBlur();
	bool init();

	bool getIsUseBlur()const;
	void setIsUseBlur( bool var );
protected:
	virtual bool isBlurActive() = 0;
	virtual void visitContent( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags ) = 0;
protected:
	void visitWithBlur( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags );
private:
	IntrusivePtr< RenderTexture > _rendererBloor;
	bool _useBlur;
	size_t _blurCounter;
	size_t _blurSamples;
};

#define DEFAULT_BLUR_METHODS \
		virtual bool isBlurActive() override { return !isRunning(); } \
		virtual void visit( Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags )override { if( getIsUseBlur() )LayerBlur::visitWithBlur( renderer, parentTransform, parentFlags ); else LayerExt::visit( renderer, parentTransform, parentFlags ); } \
		virtual void visitContent( Renderer * renderer, Mat4 const & parentTransform, uint32_t parentFlags ) override { LayerExt::visit( renderer, parentTransform, parentFlags ); }

class NodeExt_ : public Node, public NodeExt
{
	DECLARE_BUILDER( NodeExt_ );
	bool init() { return Node::init() && NodeExt::init(); }
};

class SpriteExt : public Sprite, public NodeExt
{
	DECLARE_BUILDER( SpriteExt );
	bool init() { return Sprite::init() && NodeExt::init(); }
};

class MenuExt : public Menu, public NodeExt
{
	DECLARE_BUILDER( MenuExt );
	bool init() { return Menu::init() && NodeExt::init(); }
};

NS_CC_END
#endif // #ifndef NodeExt