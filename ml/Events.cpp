//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//
#include "Events.h"
#include "ObjectFactory.h"
#include "ml/common.h"
#include "ml/common.h"
#include "ml/Generics.h"
#include "ml/NodeExt.h"
#include "loadxml/xmlProperties.h"
#include "loadxml/xmlLoader.h"
#include "Audio/AudioEngine.h"
#include "ml/RandomNumberGenerator.h"
NS_CC_BEGIN;

EventBase::Pointer EventBase::create( const std::string & type )
{
	return Factory::shared().build<EventBase>( type );
}

EventBase::EventBase()
{}

EventBase::~EventBase()
{}

void EventBase::setParam( const std::string & name, const std::string & value )
{
	if( name == "targettags" )
	{
		std::list<std::string>list;
		split( list, value );
		for( auto tag : list )
			_targetTags.push_back( strTo<int>( tag ) );
	}
	else if( name == "target" )
	{
		_targetPath = value;
	}
	else
	{
		MessageBox( ("unknown param :" + name).c_str(), "EventBase" );
	}
}

Node* EventBase::getTarget( NodeExt * context )
{
	Node * target = nullptr;
	if( _targetTags.empty() == false )
		target = context->getChildByPath( _targetTags );
	else if( _targetPath.empty() == false )
		target = context->getChildByPath( _targetPath );
	else
		target = context->as_node_pointer();
	return target;
}


/****************************************************************************************/
//MARK:	EventAction
/****************************************************************************************/
EventAction::EventAction()
{}

EventAction::~EventAction()
{}

bool EventAction::init()
{
	return true;
}


void EventAction::execute( NodeExt * context )
{
	//log( "EventAction: target:[%s], action:[%s]", _targetPath.c_str(), _actionname.c_str() );
	assert( context );
	auto action = context->getAction( _actionname );

	Node * target = getTarget( context );

	if(context->as_node_pointer()->getName().c_str() == std::string("shaman")) {
		CCLOG("shaman action needs to perform :: %s",_actionname.c_str());
	}
	
	if( target == nullptr )
		CCLOG( "EventAction::execute: cannot find target. context: [%s], path: [%s]",
		context->as_node_pointer()->getName().c_str(), getPathToTarget().c_str() );
	if( action == nullptr )
		CCLOG( "EventAction::execute: cannot find action. context: [%s], action name: [%s]",
		context->as_node_pointer()->getName().c_str(), _actionname.c_str() );

	if( !(action && target) )
		return;

	if( _state == "run" )
	{
		auto clone = action->clone();
		//auto tag = static_cast<int>(*((int*)action.ptr()));
		auto tag = action->_ID;
		target->runAction( clone );
		clone->setTag( tag );
	}
	else if( _state == "stop" )
	{
		//auto tag = static_cast<int>(*((int*)action.ptr()));
		auto tag = action->_ID;
		target->stopActionByTag( tag );
	}
	else
		assert( 0 );
}

void EventAction::setParam( const std::string & name, const std::string & value )
{
	if( name == "action" )
		_actionname = value;
	else if( name == "state" )
	{
		_state = value;
		assert( _state == "run" || _state == "stop" );
	}
	else
		EventBase::setParam( name, value );
}

std::string EventAction::getParam( const std::string & name )
{
	if( name == "action" )
		return _actionname;
	return "";
}

EventRunAction::EventRunAction() {}
EventRunAction::~EventRunAction() {}
bool EventRunAction::init()
{
	if( EventAction::init() ) { setParam( "state", "run" ); return true; }
	return false;
}

EventStopAction::EventStopAction() {}
EventStopAction::~EventStopAction() {}
bool EventStopAction::init()
{
	if( EventAction::init() ) { setParam( "state", "stop" ); return true; }
	return false;
}

/****************************************************************************************/
//MARK:	EventStopAllAction
/****************************************************************************************/

EventStopAllAction::EventStopAllAction() {}
EventStopAllAction::~EventStopAllAction() {}
bool EventStopAllAction::init() { return true; }
void EventStopAllAction::execute( NodeExt * context )
{
	//log( "EventStopAllAction::execute: [%s]", _targetPath.c_str() );
	Node * target = getTarget( context );
	assert( target );
	if( target )
		target->stopAllActions();
}

/****************************************************************************************/
//MARK:	EventRunEvent
/****************************************************************************************/

EventRunEvent::EventRunEvent() {}
EventRunEvent::~EventRunEvent() {}
bool EventRunEvent::init() { return true; }

void EventRunEvent::execute(NodeExt *context)
{
	//log( "EventRunEvent: [%s]", eventName.c_str() );
	assert( context );
	NodeExt * target = dynamic_cast<NodeExt*>(getTarget( context ));
	if( target )
		target->runEvent( eventName );
}

void EventRunEvent::setParam(const std::string & name, const std::string & value)
{
	if (name == "event")
		eventName = value;
	else
		EventBase::setParam(name, value);
}

std::string EventRunEvent::getParam(const std::string & name)
{
	assert(0);
	return "";
}

/****************************************************************************************/
//MARK:	EventSetProperty
/****************************************************************************************/
EventSetProperty::EventSetProperty()
{}

EventSetProperty::~EventSetProperty()
{}

bool EventSetProperty::init()
{
	return true;
}

void EventSetProperty::execute( NodeExt * context )
{
	//log( "EventSetProperty: target:[%s], property:[%s], value:[%s]", _targetPath.c_str(), _stringProperty.c_str(), _stringProperty.c_str() );
	Node * target = getTarget( context );
	if( target )
	{
		if( xmlLoader::setProperty( target, _property, _value ) == false )
			xmlLoader::setProperty( target, _stringProperty, _value );
	}
}

void EventSetProperty::setParam( const std::string & name, const std::string & value )
{
	if( name == "property" )
	{
		_property = xmlLoader::strToPropertyType( value );
		_stringProperty = value;
	}
	else if( name == "value" ) _value = value;
	else EventBase::setParam( name, xmlLoader::macros::parse( value ) );
}

std::string EventSetProperty::getParam( const std::string & name )
{
	assert( 0 );
	return "";
}


/****************************************************************************************/
//MARK:	EventCreateNode
/****************************************************************************************/

EventCreateNode::EventCreateNode()
: _additionalZOrder(0)
{
	_positionInfo.method = PositionInfo::byContext;
}

EventCreateNode::~EventCreateNode()
{}

bool EventCreateNode::init()
{
	return true;
}

void EventCreateNode::execute( NodeExt * context )
{
	auto target = getTarget( context );
	auto node = _node;
	if( !node )
		node = xmlLoader::load_node( _path );
	if( target && node && node->getParent() == nullptr )
	{
		target->addChild( node );
		switch( _positionInfo.method )
		{
			case PositionInfo::byContext:
			{
				Node* contextNode = context ? context->as_node_pointer() : nullptr;
				Point pos = contextNode ? contextNode->getPosition() : Point::ZERO;
				pos += node->getPosition();
				pos += _positionInfo.offset;
				node->setPosition( pos );
				break; 
			}

		}
		node->setLocalZOrder( -node->getPositionY() + _additionalZOrder );
	}
}

void EventCreateNode::setParam( const std::string & name, const std::string & value )
{
	if( 0 );
	else if( name == "additionalzorder" ) _additionalZOrder = strTo<int>( value );
	else if( name == "posinfo_offset" ) _positionInfo.offset = strTo<Point>( value );
	else if( name == "posinfo_method" ) _positionInfo.method = PositionInfo::byContext;
	else EventBase::setParam( name, value );
}

std::string EventCreateNode::getParam( const std::string & name )
{
	//return EventBase::getParam( name );
	return "";
}

void EventCreateNode::loadXmlEntity( const std::string& tag, const pugi::xml_node& node )
{
	if( tag == "node" )
		_node = xmlLoader::load_node( const_cast<pugi::xml_node&>(node) );
	else if( tag == "path" )
		_path = node.attribute( "file" ).as_string();
	else
		EventBase::loadXmlEntity( tag, node );
}



/****************************************************************************************/
//MARK:	EventPlaySound
/****************************************************************************************/

EventPlaySound::EventPlaySound()
: _sounds()
, _soundID( -1 )
, _looped( 0 )
, _predelay( 0 )
, _duration( 0 )
, _panoram( 0 )
{}

EventPlaySound::~EventPlaySound()
{}

bool EventPlaySound::init()
{
	return true;
}

void EventPlaySound::execute( NodeExt * context )
{
	assert( _looped ? _duration > 0 : true );
	retain();
	if( _predelay == 0 )
	{
		play( 0 );
	}
	else
	{
		auto func = std::bind( &EventPlaySound::play, this, std::placeholders::_1 );
		auto key = "EventPlaySound::play" + toStr( _ID );
		if( Director::getInstance()->getScheduler()->isScheduled( key, this ) == false )
		{
			Director::getInstance()->getScheduler()->schedule( func, this, _predelay, 0, 0, false, key );
		}
	}

}

void EventPlaySound::setParam( const std::string & name, const std::string & value )
{
    if( name == "sound" )
    {
        std::istringstream ss(value);
        std::string token;
        while(std::getline(ss, token, ','))
        {
            _sounds.push_back(xmlLoader::macros::parse( token ));
        }
    }
	else if( name == "looped" ) _looped = strTo<bool>( xmlLoader::macros::parse( value ) );
	else if( name == "predelay" ) _predelay = strTo<float>( xmlLoader::macros::parse( value ) );
	else if( name == "duration" ) _duration = strTo<float>( xmlLoader::macros::parse( value ) );
	else if( name == "panoram" ) _panoram = strTo<float>( xmlLoader::macros::parse( value ) );
	assert( _panoram >= -1 && _panoram <= 1 );
}

std::string EventPlaySound::getParam( const std::string & name )
{
	if( name == "sound" ) return _sounds[0];
	else if( name == "looped" ) return toStr( _looped );
	else if( name == "predelay" ) return toStr( _predelay );
	else if( name == "duration" ) return toStr( _duration );
	else if( name == "panoram" ) return toStr( _panoram );
	return "";
}

void EventPlaySound::play( float dt )
{
    int soundRandomIndex = RandomNumberGenerator::getRandomInteger(0, _sounds.size()-1);
	_soundID = AudioEngine::shared().playEffect( _sounds[soundRandomIndex], _looped, _panoram );
	if( _looped )
	{
		auto func = std::bind( &EventPlaySound::stop, this, std::placeholders::_1 );
		auto key = "EventPlaySound::stop" + toStr( _ID );
		if( Director::getInstance()->getScheduler()->isScheduled( key, this ) == false )
		{
			retain();
			Director::getInstance()->getScheduler()->schedule( func, this, _duration, 0, 0, false, key );
		}
	}
	release();
}

void EventPlaySound::stop( float dt )
{
	if( _soundID != -1 )
	{
		AudioEngine::shared().stopEffect( _soundID );
	}
	release();
}


/****************************************************************************************/
//MARK:	EventsList
/****************************************************************************************/

void EventsList::execute( NodeExt * context )
{
	//log( "EventsList::execute" );
	int index( 0 );
	for( auto event : *this )
	{
		//log( "EventsList::execute event index [%d]", index++ );
		event->execute( context );
	}
}

NS_CC_END;
