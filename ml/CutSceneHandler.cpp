//
//  CutSceneHandler.cpp
//  Easter Island
//
//  Created by Zeeshan Amjad on 14/11/2017.
//

#include "CutSceneHandler.h"
#include "ml/audio/AudioEngine.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

static CutSceneHandler* m_instance = nullptr;

CutSceneHandler::CutSceneHandler()
: m_videoPlayer(nullptr)
, m_callback(nullptr)
{
    Rect _visibleRect = Rect(Director::getInstance()->getVisibleOrigin(), Director::getInstance()->getVisibleSize());
    auto centerPos = Vec2(_visibleRect.origin.x + _visibleRect.size.width / 2,_visibleRect.origin.y + _visibleRect.size.height /2);
    
    m_videoPlayer = VideoPlayer::create();
    m_videoPlayer->setPosition(centerPos);
    m_videoPlayer->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    m_videoPlayer->setContentSize(_visibleRect.size);
    m_videoPlayer->setFullScreenEnabled(false);
    m_videoPlayer->setHighlighted(true);
    m_videoPlayer->retain();
    
    m_blockLayer = LayerColor::create(Color4B(0, 0, 0, 255), _visibleRect.size.width, _visibleRect.size.height);
    m_blockLayer->setPosition(Vec2::ZERO);
    m_blockLayer->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    m_blockLayer->setContentSize(_visibleRect.size);
    m_blockLayer->setGlobalZOrder(1000);
    m_blockLayer->retain();
    
    auto dispatcher = Director::getInstance()->getEventDispatcher();
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(CutSceneHandler::onTouchBegan, this);
    dispatcher->addEventListenerWithSceneGraphPriority(listener, m_blockLayer);
    
    m_videoPlayer->setVisible(false);
    m_blockLayer->setVisible(false);
}

CutSceneHandler::~CutSceneHandler()
{
    
}

CutSceneHandler* CutSceneHandler::getInstance()
{
    if (m_instance == nullptr)
    {
        m_instance = new CutSceneHandler();
    }
    return m_instance;
}

bool CutSceneHandler::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    if(m_videoPlayer->isPlaying())
    {
        endVideo();
    }
    return true;
}

void CutSceneHandler::showCutScene(int cutsceneValue, Node* container, const VideoCloseCB& closeCB )
{
    CCLOG("Zuluu:: Showing Video %d", cutsceneValue);
    AudioEngine::shared().pauseAllEffects();
    AudioEngine::shared().pauseBackgroundMusic();
    m_callback = closeCB;
    
    container->addChild(m_blockLayer);
    container->addChild(m_videoPlayer);
    
    m_videoPlayer->addEventListener(CC_CALLBACK_2(CutSceneHandler::videoEventCallback, this));
    const std::string& filename = StringUtils::format("video/cutscene%d.mp4",cutsceneValue);
    m_videoPlayer->setFileName(filename);
    m_videoPlayer->play();
    
    m_videoPlayer->setVisible(true);
    m_blockLayer->setVisible(true);
}

void CutSceneHandler::videoEventCallback(Ref* sender, VideoPlayer::EventType eventType)
{
    CCLOG("Zuluu:: VideoEventCallback %d", (int) eventType);
    if (eventType == VideoPlayer::EventType::COMPLETED && m_videoPlayer && m_callback)
    {
        endVideo();
    }
}

void CutSceneHandler::endVideo()
{
    CCLOG("Zuluu:: Removing Video");
    AudioEngine::shared().resumeAllEffects();
    AudioEngine::shared().resumeBackgroundMusic();
    
    m_videoPlayer->addEventListener(nullptr);
    m_videoPlayer->stop();
    
    m_videoPlayer->setVisible(false);
    m_blockLayer->setVisible(false);
    m_videoPlayer->removeFromParentAndCleanup(true);
    m_blockLayer->removeFromParentAndCleanup(true);
    
    m_callback();
    m_callback = nullptr;
}

#endif
