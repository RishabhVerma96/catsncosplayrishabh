#include "Slider.h"
#include "ml/ImageManager.h"

NS_CC_BEGIN

mlSlider::mlSlider()
{}

mlSlider::~mlSlider()
{}


mlSlider::Pointer mlSlider::create(
	const std::string& barImage,
	const std::string& progressBarImage,
	const std::string& slidBallNormalImage,
	const std::string& slidBallPressedImage,
	const std::string& slidBallDisableImage,
	const ccMenuCallback& callback )
{
	Pointer ptr = make_intrusive<mlSlider>();
	if( ptr && ptr->init( barImage, progressBarImage, slidBallNormalImage, slidBallPressedImage, slidBallDisableImage, callback ) )
		return ptr;
	ptr.reset( nullptr );
	return ptr;
}

mlSlider::Pointer mlSlider::create()
{
	const std::string kEmpty;
	return create( kEmpty, kEmpty, kEmpty, kEmpty, kEmpty, nullptr );
}

bool mlSlider::init( const std::string& barImage,
	const std::string& progressBarImage,
	const std::string& slidBallNormalImage,
	const std::string& slidBallPressedImage,
	const std::string& slidBallDisableImage,
	const ccMenuCallback& callback )
{
	ui::Slider::init();
	setCallback( callback );

	setBarTexture( barImage );
	setProgressBarTexture( progressBarImage );
	setSlidBallTextureNormal( slidBallNormalImage );
	setSlidBallTexturePressed( slidBallPressedImage );
	setSlidBallTextureDisabled( slidBallDisableImage );

	return true;
}

void mlSlider::setBarTexture( const std::string& fileName )
{
	auto frame = ImageManager::shared().spriteFrame( fileName );
	if( frame )
		loadBarTexture( fileName, ui::Widget::TextureResType::PLIST );
	else
		loadBarTexture( fileName, ui::Widget::TextureResType::LOCAL );
}

void mlSlider::setProgressBarTexture( const std::string& fileName )
{
	auto frame = ImageManager::shared().spriteFrame( fileName );
	if( frame )
		loadProgressBarTexture( fileName, ui::Widget::TextureResType::PLIST );
	else
		loadProgressBarTexture( fileName, ui::Widget::TextureResType::LOCAL );
}

void mlSlider::setSlidBallTextureNormal( const std::string& fileName )
{
	auto frame = ImageManager::shared().spriteFrame( fileName );
	if( frame )
		loadSlidBallTextureNormal( fileName, ui::Widget::TextureResType::PLIST );
	else
		loadSlidBallTextureNormal( fileName, ui::Widget::TextureResType::LOCAL );
}

void mlSlider::setSlidBallTexturePressed( const std::string& fileName )
{
	auto frame = ImageManager::shared().spriteFrame( fileName );
	if( frame )
		loadSlidBallTexturePressed( fileName, ui::Widget::TextureResType::PLIST );
	else
		loadSlidBallTexturePressed( fileName, ui::Widget::TextureResType::LOCAL );
}

void mlSlider::setSlidBallTextureDisabled( const std::string& fileName )
{
	auto frame = ImageManager::shared().spriteFrame( fileName );
	if( frame )
		loadSlidBallTextureDisabled( fileName, ui::Widget::TextureResType::PLIST );
	else
		loadSlidBallTextureDisabled( fileName, ui::Widget::TextureResType::LOCAL );
}

void mlSlider::setCallback( const ccMenuCallback& callback )
{
	auto listener = std::bind([this, callback](Ref* sender, ui::Slider::EventType event){
		if( event == ui::Slider::EventType::ON_PERCENTAGE_CHANGED )
			callback( sender );
	}, std::placeholders::_1, std::placeholders::_2 );
	
	addEventListener( listener );
}

void mlSlider::setProgress( float value )
{
	setPercent( value * 100 );
}

float mlSlider::getProgress()
{
	return (float)getPercent() / 100.f;
}
NS_CC_END