#pragma once

#include "cocos2d.h"
#include "ml/macroses.h"

NS_CC_BEGIN

class ActionText: public ActionInterval
{
	DECLARE_BUILDER(ActionText);
	bool init(float duration, float endValue, bool floatTruncation = true, const std::string& prefix = "", const std::string& postfix = "");

public:
	virtual void startWithTarget(Node *target) override;
	virtual void update(float t) override;
	virtual ActionInterval* reverse() const override;
	virtual ActionInterval *clone() const override;

private:
	bool _floatTruncation;
	float _startValue;
	float _endValue;
	std::string _prefix;
	std::string _postfix;
};

class TypeAction : public ActionInterval
{
	DECLARE_BUILDER(TypeAction);
	bool init(const std::string & text, float delay);

public:
	void startWithTarget(Node * target) override;
	void update(float dt) override;
	void stop() override;
	bool isDone() const override;
	ActionInterval * clone() const override;

private:
	void setString(const std::string & string);

private:
	float       _delay;
	bool        _isDone;
	std::string _text;
};

NS_CC_END