//
//  IslandDefense
//
//  Created by Vladimir Tolmachev on 27.09.14.
//
//

#pragma once

#include <map>
#include <string>
#include "cocos2d.h"

class ParamCollection : public std::map<std::string, std::string>
{
	static const char delimiter_pair;
	static const char delimiter;
	static const char delimiter_invalue;

public:
	ParamCollection(const std::string &string = "");

	void parse(const std::string &string);
	std::string string() const;

	const std::string get( const std::string &name, const std::string &defaultValue = "" );
	const std::string get( const std::string &name, const std::string &defaultValue = "" )const;
	const void set(const std::string &name, const std::string &value, bool rewrite = true);

	void tolog() const;
	bool isExist(const std::string &name);
};