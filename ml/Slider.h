#ifndef __ml_Slider_h__
#define __ml_Slider_h__
#include "ml/macroses.h"
#include "ui/CocosGUI.h"

NS_CC_BEGIN;

class mlSlider : public cocos2d::ui::Slider
{
private:
	DECLARE_POINTER( mlSlider )
	bool init(
		const std::string& barImage,
		const std::string& progressBarImage,
		const std::string& slidBallNormalImage,
		const std::string& slidBallPressedImage,
		const std::string& slidBallDisableImage,
		const ccMenuCallback& callback );
public:
	static Pointer create(
		const std::string& barImage,
		const std::string& progressBarImage,
		const std::string& slidBallNormalImage,
		const std::string& slidBallPressedImage,
		const std::string& slidBallDisableImage,
		const ccMenuCallback& callback );
	static Pointer create();

public:
	void setBarTexture( const std::string& fileName );
	void setProgressBarTexture( const std::string& fileName );
	void setSlidBallTextureNormal( const std::string& filename );
	void setSlidBallTexturePressed( const std::string& filename );
	void setSlidBallTextureDisabled( const std::string& filename );
	void setCallback( const ccMenuCallback& callback );

	void setProgress( float value );
	float getProgress();
};

NS_CC_END;

#endif