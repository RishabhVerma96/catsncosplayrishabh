
#include "KeyboardListener.h"
NS_CC_BEGIN
#if PC == 1

KeyboardListener::KeyboardListener()
: _listener( nullptr )
, _enabled( false )
{	
	_listener = EventListenerKeyboard::create();
	_listener->retain();
	_listener->onKeyReleased = std::bind( &KeyboardListener::onKeyReleased, this, std::placeholders::_1, std::placeholders::_2 );

	auto eventDispatcher = Director::getInstance()->getEventDispatcher();
	eventDispatcher->addEventListenerWithFixedPriority( _listener, -9999 );
}

KeyboardListener::~KeyboardListener()
{
	auto eventDispatcher = Director::getInstance()->getEventDispatcher();
	eventDispatcher->removeEventListener( _listener );
	CC_SAFE_RELEASE_NULL( _listener );
}

void KeyboardListener::onKeyReleased( EventKeyboard::KeyCode key, Event *event )
{
	if( _enabled )
	{
		observer.pushconsumableevent( key );
	}
}

void KeyboardListener::enable()
{
	_enabled = true;
}
void KeyboardListener::disable()
{
	_enabled = false;
}
#else
KeyboardListener::KeyboardListener()
{}

KeyboardListener::~KeyboardListener()
{}
#endif
NS_CC_END