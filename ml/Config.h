#pragma once

#include "cocos2d.h"
#include "ml/Singlton.h"
#include "ml/ParamCollection.h"
#include "ml/Generics.h"

NS_CC_BEGIN

class Config: public Singlton <Config>, public ParamCollection
{
	friend class Singlton <Config>;
	virtual void onCreate() override;

public:
	template <class T> T get(const std::string& name) { return strTo<T>(ParamCollection::get(name)); }
	std::string get(const std::string &name);

	void setDefaultValue(const std::string &name, const std::string &value);
};

NS_CC_END