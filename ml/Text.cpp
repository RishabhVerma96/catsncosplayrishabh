/******************************************************************************/
/*
 * Copyright 2014-2015 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: Defense of Greece
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#include "Text.h"
#include "ml/loadxml/xmlLoader.h"
#include "ml/loadxml/xmlProperties.h"
#include "ml/Language.h"
NS_CC_BEGIN

Text::Text()
: _useStrech( true )
, _fontSize( 40 )
, _systemFontName( "Tahoma" )
, _defaultScale( 1.f, 1.f )
, _defaultWidth( 0.f )
{
	useLocation( true );
}

Text::~Text( )
{
}

bool Text::init()
{
	return Label::init();
}

bool Text::init( const std::string& font, const std::string& text, float width, TextHAlignment alignment, const Vec2& imageOffset )
{
	if( font.empty() == false && Label::setBMFontFilePath( font, imageOffset ) )
	{
		setMaxLineWidth( width );
		setString( text );
		setAlignment( alignment );
		return true;
	}
	else
	{
		setString( text );
		setAlignment( alignment );
		setSystemFontName( "Arial" );
		setSystemFontSize( 27 );
		return true;
	}
	return false;
}

bool Text::setBMFontFilePath( const std::string& bmfontFilePath, const Vec2& imageOffset, float fontSize )
{
	if( Language::shared().useOnlySystemFont() == false )
	{
		return Label::setBMFontFilePath( bmfontFilePath, imageOffset, fontSize );
	}
	else
	{
		setSystemFontName( Language::shared().getUsingSystemFontDefault() );
	}
	return false;
}

void Text::setString( const std::string& newString )
{
	if( _sourceString.empty() )
		_sourceString = newString;
	Label::setString( WORD(newString) );
	if( _useStrech )
		strechNode( this, _strech );
}

void Text::setScale( float scaleX, float scaleY )
{
	Node::setScale( scaleX, scaleY );
	//_useStrech = false;
	_defaultScale = Vec2( scaleX, scaleY );
	Label::setWidth( _defaultWidth / _defaultScale.x );
}

void Text::setWidth( float width )
{
	_defaultWidth = width;
	Label::setWidth( _defaultWidth / _defaultScale.x );
}

void Text::setStretch( const Strech& strech )
{
	_strech = strech;
}

void Text::setSourceString( const std::string& newString )
{
	_sourceString = newString;
}

void Text::useLocation( bool mode )
{
}

void Text::onChangeLocalisation()
{
	setBMFontFilePath( _bmFontPath );
	std::string string = Language::shared().string( xmlLoader::macros::parse( _sourceString ) );
	setString( string );
}

void Text::setTTFFontName( const std::string& font )
{
	_ttfFontName = font;
	TTFConfig ttfConfig( _ttfFontName.c_str(), _fontSize, GlyphCollection::DYNAMIC );
	setTTFConfig( ttfConfig );
	setString( getString() );
}

void Text::setSystemFontName( const std::string& font )
{
	_systemFontName = font;
	Label::setSystemFontSize( _fontSize );
	Label::setSystemFontName( _systemFontName );
}

const std::string& Text::getTTFFontName()const
{
	return _ttfFontName;
}

const std::string& Text::getSystemFontName()const
{
	return _systemFontName;
}

void Text::setFontSize( unsigned size )
{
	_fontSize = size;
	if( _ttfFontName.empty() == false )
	{
		TTFConfig ttfConfig( _ttfFontName.c_str(), _fontSize, GlyphCollection::DYNAMIC );
		setTTFConfig( ttfConfig );
		setString( getString() );
	}
	else if( _systemFontName.empty() == false && _bmFontPath.empty() )
	{
		Label::setSystemFontSize( _fontSize );
		Label::setSystemFontName( _systemFontName );
	}
}

unsigned Text::getFontSize()const
{
	return _fontSize;
}

NS_CC_END