//
//  CutSceneHandler.h
//  Easter Island
//
//  Created by Zeeshan Amjad on 14/11/2017.
//

#ifndef CutSceneHandler_h
#define CutSceneHandler_h

#include <stdio.h>
#include "cocos2d.h"
#include "../../cocos/ui/CocosGUI.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

using namespace cocos2d;
using namespace cocos2d::ui;
using namespace cocos2d::experimental::ui;

typedef std::function<void()> VideoCloseCB;

class CutSceneHandler
{
private:
    VideoPlayer* m_videoPlayer;
    VideoCloseCB m_callback;
    Layer* m_blockLayer;
    
    CutSceneHandler();
    
    bool onTouchBegan(Touch *pTouch, Event *pEvent);
    void videoEventCallback(Ref* sender, VideoPlayer::EventType eventType);
    void endVideo();
public:
    static CutSceneHandler* getInstance();
    ~CutSceneHandler();
    
    void showCutScene(int cutsceneValue, Node* container, const VideoCloseCB& closeCB );
};

#endif /* CutSceneHandler_h */
#endif
