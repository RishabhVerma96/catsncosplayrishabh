package com.stereo7.supersonic;

import java.io.IOException;
import java.util.Map;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.supersonicads.sdk.*;
import com.supersonicads.sdk.data.AdUnitsReady;
import com.supersonicads.sdk.listeners.OnInterstitialListener;
import com.supersonicads.sdk.listeners.OnOfferWallListener;
import com.supersonicads.sdk.listeners.OnRewardedVideoListener;

@SuppressLint("HandlerLeak") 
public class Supersonic implements
OnRewardedVideoListener,
OnInterstitialListener,
OnOfferWallListener
{
	private static Activity app;
    
	static Supersonic me;
    static SSAPublisher ssaPub;
    static String appKey;
    static String userId;

    private static boolean PVexist = false;
    public final int MSG_SONIC_ONREWARD = 1;
    public final int MSG_SONIC_ONCLOSED = 2;
    

    Handler handler;

    public native static void nativeSonicReward(int result);
    public native static void nativeSonicRewardOfferwall(int result);

    public Supersonic(Activity application, String id) {
        app = application;
        Supersonic.me = this;
        appKey = id;

        //Initialize the SDK, passing the current context to the method
        ssaPub = SSAFactory.getPublisherInstance(app);
        finished(null);
        
        handler = new Handler()
        {
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case MSG_SONIC_ONREWARD:
                    	if( Supersonic.me == null ) return;
                        nativeSonicReward(msg.arg1); break;
                    case MSG_SONIC_ONCLOSED:
                    	if( Supersonic.me == null ) return;
                        //nativeSonicReward(0); break;
                    //other cases
                }
            };
        };

    }
    
	public void onStart() {
		if( me == null ) return;
		if( Supersonic.userId == null )
		{
		    Thread thr = new Thread(new Runnable() {
		        @Override
		        public void run() {
		            try {
		                Context ctx = app.getApplicationContext();
		                AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo(ctx);
		                finished(adInfo);
		            } catch (IllegalStateException e) {
		    			e.printStackTrace();
		    		} catch (GooglePlayServicesRepairableException e) {
		    			e.printStackTrace();
		    		} catch (IOException e) {
		    			e.printStackTrace();
		    		} catch (GooglePlayServicesNotAvailableException e) {
		    			e.printStackTrace();
		    		}
	
		        }
		    });
		    thr.start();
		}
	}

	private void finished(final AdvertisingIdClient.Info adInfo){
		if( me == null ) return;
	    if(adInfo!=null){
	        // In case you need to use adInfo in UI thread
	        app.runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	            	Supersonic.userId = adInfo.getId();
	                ssaPub.initInterstitial(appKey, userId, null, Supersonic.this);
                    ssaPub.initRewardedVideo (appKey, userId, null, Supersonic.this);
            		ssaPub.getOfferWallCredits(appKey, userId, me);
	            }
	        });
	    }
	}
	
    static public void showInterstitial()
    {
    	if( Supersonic.me == null ) return;
    	app.runOnUiThread(new Runnable() {
    		@Override
            public void run() {
		        if (ssaPub.isInterstitialAdAvailable()) {
		            ssaPub.showInterstitial();
		        } else {
		        	Log.i("SUPERSONIC", "no available interstitial");
		        }
        	};
    	} );
    }
    
    static public void playAd()
    {
    	if( Supersonic.me == null ) return;
    	app.runOnUiThread(new Runnable() {
    		@Override
            public void run() {
		    	if( PVexist )
		    		ssaPub.showRewardedVideo();
		    	else
		    		nativeSonicReward(0);;
        	};
    	} );
    }
    
    public static boolean isVideoAvailabled()
    {
    	if( Supersonic.me == null ) return false;
    	return PVexist;
    }
    
    static public void showOfferwall()
    {
    	if( Supersonic.me == null ) return;
    	
    	Map<String, String> params = null;
    	ssaPub.showOfferWall(appKey, userId, params, me);
    }
    
    
    //New Interstitial Api
    @Override
    public void onInterstitialAdClicked() {}
    
    @Override
    public void onInterstitialAdClosed() {}
    
    @Override
    public void onInterstitialAvailability(boolean available) {}
    
    @Override
    public void onInterstitialInitFail(String arg0) {}
    
    @Override
    public void onInterstitialInitSuccess() {}
    
    @Override
    public void onInterstitialShowFail(String arg0) {}
    
    @Override
    public void onInterstitialShowSuccess() {}
    
    
    //rewarded video
    public void onRVInitSuccess(AdUnitsReady adUnitsReady) {
		if( me == null ) return;
    	PVexist = true;
    }
    
    public void onRVInitFail(String errMsg) {
		if( me == null ) return;
    	PVexist = false;
    }
    
    public void onRVNoMoreOffers() {
		if( me == null ) return;
    	PVexist = false;
    }
    
    public void onRVAdCredited(int credits) {
		if( me == null ) return;
        Message msg = handler.obtainMessage(MSG_SONIC_ONREWARD, credits, 0, null);
        handler.sendMessage(msg);
    }
    
    @Override
    public void onRVGeneric(String arg0, String arg1)
    {
    	if( me == null ) return;
    }
    
    @Override
    public void onRVAdClosed() {
		if( me == null ) return;
        Message msg = handler.obtainMessage(MSG_SONIC_ONCLOSED, 0, 0, null);
        handler.sendMessage(msg);
    }
    
    public void onResume()
    { 
		if( me == null ) return;
    	if (ssaPub != null) {
    		ssaPub.onResume(app);
    	}
    }

    public void onPause()
	{
		if( me == null ) return;
		if (ssaPub != null) {
			ssaPub.onPause(app);
		}
	}
    
	public void onDestroy()
	{
		if( me == null ) return;
		if (ssaPub != null) {
			ssaPub.release(app);
		}
    }

	@Override
	public void onGetOWCreditsFailed(String arg0) {}

	@Override
	public void onOWAdClosed() {
		if( me == null ) return;
		ssaPub.getOfferWallCredits(appKey, userId, me);
	}

	@Override
	public boolean onOWAdCredited(int arg0, int arg1, boolean arg2) {
		if( me == null ) return false;
		final int reward = arg0;
		app.runOnUiThread(new Runnable() {
    		@Override
            public void run() {
		 		nativeSonicRewardOfferwall(reward);
    		};
    	} );
		return true;
	}

	@Override
	public void onOWGeneric(String arg0, String arg1) {}

	@Override
	public void onOWShowFail(String arg0) {}

	@Override
	public void onOWShowSuccess() {}
}
