package com.stereo7.appodeal;

import android.app.Activity;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.RewardedVideoCallbacks;

public class mlAppodeal implements RewardedVideoCallbacks
{
	private static Activity app;
	static mlAppodeal me;
    static String appId;
	
    public mlAppodeal(Activity application, String id, boolean rewardVideo, boolean interstitial) {
    	mlAppodeal.app = application;
    	mlAppodeal.me = this;
    	mlAppodeal.appId = id;
    	
    	int flags = 0;
    	if( rewardVideo ) flags = flags | Appodeal.REWARDED_VIDEO;
    	if( interstitial ) flags = flags | Appodeal.INTERSTITIAL;
    	//Appodeal.setTesting(true);
		Appodeal.disableNetwork(application, "flurry");
    	Appodeal.setLogging(true);
    	Appodeal.initialize(application, id, flags ); 
    	Appodeal.setRewardedVideoCallbacks(this);
    }
    

	public void onResume() {
		if( mlAppodeal.me == null ) return;
        
        try {
        } catch (RuntimeException e){
        }
	}

    static public boolean isVideoAvailabled()
    {
    	return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
    }
    
    static public boolean isInterstitialAvailabled()
    {
    	return Appodeal.isLoaded(Appodeal.INTERSTITIAL);
    }
    
    static public void playAd()
    {
    	if( mlAppodeal.me == null ) return;
    	//mlAppodeal.app.runOnUiThread(new Runnable() { @Override public void run() {
    		if( Appodeal.isLoaded(Appodeal.REWARDED_VIDEO ) )
    		{
    			Appodeal.show(mlAppodeal.app, Appodeal.REWARDED_VIDEO);
    		}
    		else
    		{
    			mlAppodeal.nativeStartVideo( false );
    			mlAppodeal.nativeNoOffersVideo();
    		}
        //} });
    }

    static public void showInterstitial()
    {
    	if( mlAppodeal.me == null ) return;
    	//mlAppodeal.app.runOnUiThread(new Runnable() { @Override public void run() {
    		if( Appodeal.isLoaded(Appodeal.INTERSTITIAL ) )
    			Appodeal.show(mlAppodeal.app, Appodeal.INTERSTITIAL);
        //} });
    }


    public native static void nativeStartVideo( boolean result );
    public native static void nativeFinishedVideo( boolean result );
    public native static void nativeNoOffersVideo();
    
	@Override
	public void onRewardedVideoClosed(boolean var1) {
	}

	@Override
	public void onRewardedVideoFailedToLoad() {
		//nativeNoOffersVideo();
	}

	@Override
	public void onRewardedVideoFinished(int amount, String currency) {
		mlAppodeal.app.runOnUiThread(new Runnable() { @Override public void run() {
			nativeFinishedVideo( true );
		} });
	}

	@Override
	public void onRewardedVideoLoaded() {
	}

	@Override
	public void onRewardedVideoShown() {
		mlAppodeal.app.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				nativeStartVideo(true);
			}
		});
	}

	public void setTesting(boolean value) {
		Appodeal.setTesting(value);
	}
}
