package com.google.example.games.basegameutils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.Snapshots;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import com.google.example.games.basegameutils.BaseGameUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

public class PlayServises implements
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener
{
	private static PlayServises me;
	private static Activity app;
	private static GoogleApiClient mGoogleApiClient;

	private static final int RC_SIGN_IN = 9001;
	private boolean mResolvingConnectionFailure = false;
	private boolean mAutoStartSignInflow = true;
	private boolean mSignInClicked = false;
	private static boolean mStarted = false;
	private static final String TAG = "Cocos2dxPlayServises";
	private static final String mCurrentSaveName = "save";
	private static boolean mCloudSyncEnabled = false;
	private static boolean mCloudInit = false;
	
	public PlayServises(Activity app )
	{
		me = this;
		PlayServises.app = app;
	}

	public PlayServises(Activity app, boolean withDrive )
	{
		me = this;
		PlayServises.app = app;
		mCloudSyncEnabled = withDrive;
	}

	public static void connect()
	{
		if( me == null )return;

		app.runOnUiThread(new Runnable() {
            @Override
            public void run() {
				me.mAutoStartSignInflow = true;
				GoogleApiClient.Builder builder = new GoogleApiClient.Builder(app)
						.addConnectionCallbacks(me)
						.addOnConnectionFailedListener(me)
						.addApi(Games.API).addScope(Games.SCOPE_GAMES)
						.addApi(Plus.API, Plus.PlusOptions.builder().build())
						.addScope(Plus.SCOPE_PLUS_LOGIN);
				if (mCloudSyncEnabled)
					builder.addApi(Drive.API).addScope(Drive.SCOPE_APPFOLDER);
				mGoogleApiClient = builder.build();
                if (mStarted && mGoogleApiClient != null )
                    mGoogleApiClient.connect();
            }
        });
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		if( mGoogleApiClient == null )return;
		if (mResolvingConnectionFailure) {
	        return;
	    }

		if (mSignInClicked || mAutoStartSignInflow) {
	        mAutoStartSignInflow = false;
	        mSignInClicked = false;
	        mResolvingConnectionFailure = true;
			if (!BaseGameUtils.resolveConnectionFailure(app,
	                mGoogleApiClient, arg0,
	                RC_SIGN_IN, "OtherError")) {
	            mResolvingConnectionFailure = false;
	        }
	    }
		playConnectFailed();
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		if( mGoogleApiClient == null )return;
		if( mStarted )
			mGoogleApiClient.connect();

        try {
            Person person  = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            userDisplayName( person.getDisplayName());
            userNickname(person.getNickname() );
			userId( person.getId() );
			playConnected();
			if( mCloudSyncEnabled )
				loadSnapshot();
		}catch( RuntimeException e ){
            Log.d("PlayServices", "excception: " + e.getMessage());
        }

    }

	@Override
	public void onConnectionSuspended(int cause) {
		if( mGoogleApiClient == null )return;
	    mGoogleApiClient.connect();
		playConnectionSuspended();
    }
	
	public void onStart()
	{
		if( me == null )return;
		mStarted = true;
		if( mGoogleApiClient == null )return;
		mGoogleApiClient.connect();
	}
	
	public void onStop()
	{
		if( me == null )return;
		mStarted = false; 
		if( mGoogleApiClient == null )return;
		mGoogleApiClient.disconnect();
	}
	
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if( me == null )return;
    	if( mGoogleApiClient == null )return;
    	if (requestCode == RC_SIGN_IN) {
            mSignInClicked = false;
            mResolvingConnectionFailure = false;
            if (resultCode == Activity.RESULT_OK) {
                mGoogleApiClient.connect();
                activityConnectionSuspended();
            } else {
            	BaseGameUtils.showActivityResultError(app, requestCode, resultCode, 0);
            	activityConnectionFailed();
            }
        }
        
    }
    
	public static void leaderboardRecord( String LeaderBoardId, int scores )
	{
		if( me == null )return;
		if( mGoogleApiClient == null )return;
		try {
			Log.d("PlayServices", "leaderboardRecord: " + LeaderBoardId);
			Games.Leaderboards.submitScore(mGoogleApiClient, LeaderBoardId, scores);
		}catch( RuntimeException e ){
			Log.d("PlayServices", "excception: " + e.getMessage());
		}
	}
	
	public static void leaderboardOpen( String LeaderBoardId )
	{
		if( me == null )return;
		if( mGoogleApiClient == null )return;
		Log.d("PlayServices", "leaderboardRecord: " + LeaderBoardId);
		try {
			if (LeaderBoardId == "all")
				app.startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(mGoogleApiClient), 5);
			else
				app.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClient, LeaderBoardId), 5);
		}catch( RuntimeException e ){
			Log.d("PlayServices", "excception: " + e.getMessage());
		}

}
	
	public static void achievementsUnlock( String ID )
	{
		if( me == null )return;
		if( mGoogleApiClient == null )return;
		Games.Achievements.unlock(mGoogleApiClient, ID);
	}
	
	public static void achievementsIncrement( String ID, int value )
	{
		if( me == null )return;
		if( mGoogleApiClient == null )return;
		Games.Achievements.increment(mGoogleApiClient, ID, value);
	}
	
	public static void achievementsOpen()
	{
		if( me == null )return;
		if( mGoogleApiClient == null )return;
		app.startActivityForResult(Games.Achievements.getAchievementsIntent(mGoogleApiClient), 4);
	}

	public static void signInClicked() {
		if( me == null )return;
		if( mGoogleApiClient == null )return;
	    me.mSignInClicked = true;
	    mGoogleApiClient.connect();
	}

	public static void signOutclicked() {
		if( me == null )return;
		if( mGoogleApiClient == null )return;
		me.mSignInClicked = false;
		Games.signOut(mGoogleApiClient);
	}

	public static void loadSnapshot() {
		if( me == null )return;
		if( mGoogleApiClient == null )return;
		if( mCloudSyncEnabled == false ) return;
		if( mCloudInit ) return;
		mCloudInit = true;
		final ProgressDialog dialog = ProgressDialog.show(app, "Syncing with Google Drive",
				"Syncing...", true);
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
			@Override
			protected void onPostExecute(Void result) {
				app.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						dialog.dismiss();
					}
				});
			}
			@Override
			protected Void doInBackground(Void... params) {
				// Open the saved game using its name.
				Snapshots.OpenSnapshotResult result = Games.Snapshots.open(mGoogleApiClient,
						mCurrentSaveName, true, Snapshots.RESOLUTION_POLICY_MOST_RECENTLY_MODIFIED).await();

				// Check the result of the open operation
				if (result.getStatus().isSuccess()) {
					Snapshot snapshot = result.getSnapshot();
					// Read the byte content of the saved game.
					try {
						readSavedGame(snapshot.getSnapshotContents().readFully());
					} catch (IOException e) {
						Log.e(TAG, "Error while reading Snapshot.", e);
					}
				} else{
					Log.e(TAG, "Error while loading: " + result.getStatus().getStatusCode());
				}
				return null;
			}

		};

		task.execute();
	}

	public static void saveSnapshot(final String str) {
		if( me == null )return;
		if( mGoogleApiClient == null )return;
		if( mCloudSyncEnabled == false ) return;
		AsyncTask<Void, Void, Void> task =
				new AsyncTask<Void, Void, Void>() {
					@Override
					protected Void doInBackground(Void... params) {
						Snapshots.OpenSnapshotResult result = Games.Snapshots.open(mGoogleApiClient,
								mCurrentSaveName, true).await();
						me.writeSavedGame(result.getSnapshot(), str);
						return null;
					}
				};
		task.execute();
	}

	public static void showCloudAlertDialog() {
		if( me == null )return;
		if( mGoogleApiClient == null )return;
		if( mCloudSyncEnabled == false ) return;

		app.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(app);
// Add the buttons
				builder.setTitle("Choose Cloud Option");
				builder.setMessage("Do you want use game progress from Google Drive?");
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// User clicked OK button
						me.UIAlertNotify(true);
					}
				});
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// User cancelled the dialog
						me.UIAlertNotify(false);
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});
	}

	public static void readSavedGame(byte[] data) {
		String result = new String(data);
		me.userDataLoad(result);
	}

	public static void writeSavedGame(Snapshot snapshot, String str) {
		snapshot.getSnapshotContents().writeBytes(str.getBytes());
		Games.Snapshots.commitAndClose(mGoogleApiClient, snapshot, SnapshotMetadataChange.EMPTY_CHANGE);
	}

	private native void playConnected();
	private native void playConnectFailed();
	private native void playConnectionSuspended();
	private native void activityConnectionFailed();
    private native void activityConnectionSuspended();

    private native void userDisplayName( String name );
	private native void userNickname( String nickname );
	private native void userId( String nickname );

	private native void userDataLoad( String str );
	private native void UIAlertNotify(boolean result);
}
