package com.stereo7.tapjoy;

import android.app.Activity;

import com.tapjoy.TapjoyConnect;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.IOException;

public class TapJoy
{
	private static Activity app;

	private String appId; 
	private String secretKey;

	public class GetterID implements Runnable {

	    private String AdID;
    	private String AdKey;

	    public GetterID(String appkey) {
	    	AdKey = appkey;
	    }

	    public void run() {
	        try {
				getIdThread();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (GooglePlayServicesRepairableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	TapjoyConnect.requestTapjoyConnect(app, AdID, AdKey);
	    }
	    
		public void getIdThread() throws IllegalStateException, GooglePlayServicesRepairableException {

			  Info adInfo = null;
			  try {
			    adInfo = AdvertisingIdClient.getAdvertisingIdInfo(app);

			  } catch (IOException e) {
			    // Unrecoverable error connecting to Google Play services (e.g.,
			    // the old version of the service doesn't support getting AdvertisingId).
			 
			  } catch (GooglePlayServicesNotAvailableException e) {
			    // Google Play services is not available entirely.
			  }
			  AdID = adInfo.getId();
			  //final boolean isLAT = adInfo.isLimitAdTrackingEnabled();
		}
		
	}
	
	public TapJoy(Activity application, String id, String key) 
			throws IllegalStateException, GooglePlayServicesRepairableException
	{
		app = application;
        TapjoyConnect.requestTapjoyConnect(app, appId, secretKey);
    }
	
}
