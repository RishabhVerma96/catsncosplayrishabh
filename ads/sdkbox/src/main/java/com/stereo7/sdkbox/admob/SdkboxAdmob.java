package com.stereo7.sdkbox.admob;

/**
 * Created by Dmitriy on 11.04.2016.
 */

public class SdkboxAdmob {
    public native static void nativeInit(String bannerName, String interstitialName);

    public static void  init(String bannerName, String interstitialName) {
        nativeInit(bannerName, interstitialName);
    }
}
