LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := adssdkbox_static

LOCAL_MODULE_FILENAME := libadssdkbox

LOCAL_CPPFLAGS := -DSDKBOX_ENABLED
LOCAL_WHOLE_STATIC_LIBRARIES := PluginAdMob \
PluginSdkboxPlay \
sdkbox 

LOCAL_LDLIBS := -landroid \
-llog

include $(BUILD_STATIC_LIBRARY)
$(call import-add-path,$(LOCAL_PATH))

$(call import-module, ./sdkbox)
$(call import-module, ./pluginadmob)
$(call import-module, ./pluginsdkboxplay)
