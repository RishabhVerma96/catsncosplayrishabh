package com.stereo7.tapjoy;

import java.util.Hashtable;

import android.app.Activity;
import android.util.Log;

import com.tapjoy.TJConnectListener;
import com.tapjoy.Tapjoy;
import com.tapjoy.TapjoyConnectFlag;

public class TapJoy
{
	static Activity app;
	static TapJoy me;
	static String appID;
	
	public TapJoy(Activity app, String appID)
	{
		TapJoy.app = app;
		TapJoy.appID = appID;
		TapJoy.me = this;
		
		Hashtable<String, Object> connectFlags = new Hashtable<String, Object>();
		connectFlags.put(TapjoyConnectFlag.ENABLE_LOGGING, "false");
		
		Tapjoy.connect(app, appID, connectFlags, new TJConnectListener() {
			@Override
			public void onConnectSuccess() {
				TapJoy.onConnectSuccess();
			}

			@Override
			public void onConnectFailure() {
				TapJoy.onConnectFail();
			}
		});
	}
	
	private static void onConnectSuccess()
	{
		Log.d( "TapJoy", "onConnectSuccess");
	}
	private static void onConnectFail()
	{
		Log.d( "TapJoy", "onConnectFail");
	}
}