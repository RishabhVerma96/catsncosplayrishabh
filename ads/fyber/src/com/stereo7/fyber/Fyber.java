package com.stereo7.fyber;

import android.app.Activity;
import android.content.Intent;

import com.sponsorpay.SponsorPay;
import com.sponsorpay.publisher.SponsorPayPublisher;
import com.sponsorpay.publisher.mbe.*;
import com.sponsorpay.publisher.interstitial.SPInterstitialRequestListener;

public class Fyber  implements SPBrandEngageRequestListener, SPInterstitialRequestListener
{
	private static Activity app;
	static Fyber me;
    static String appId;
    static String appKey;
	static Intent intentVideo;
	static Intent intentInterstitial;
	static int RESULT_OK = -1;
	static final int MSG_VIDEO_FINISHED_OK = 1;
	static final int MSG_VIDEO_FINISHED_NO = 2;
	static final int MSG_VIDEO_STARTED_OK = 3;
	static final int MSG_VIDEO_STARTED_NO = 4;
	static final int MSG_VIDEO_NOOFFERS = 5;
	static boolean started = false;
	
    public Fyber(Activity application, String id, String key) {
    	Fyber.app = application;
        Fyber.me = this;
        Fyber.appKey = key;
        Fyber.appId = id;
    }
    
	public void onResume() {
		if( me == null ) return;
        
        try {
        	if( started == false )
        	{
        		started = true;
        		SponsorPay.start(appId, null, appKey, Fyber.app);
        	}
        } catch (RuntimeException e){
        }
	}

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if( me == null ) return;

		if (resultCode == RESULT_OK && requestCode == 1234) 
    	{
    		String engagementResult = data.getStringExtra(SPBrandEngageClient.SP_ENGAGEMENT_STATUS);
    		if( engagementResult.equals( SPBrandEngageClient.SP_REQUEST_STATUS_PARAMETER_FINISHED_VALUE ) )
    		{
    			Fyber.app.runOnUiThread(new Runnable() { @Override public void run() {
    					nativeFinished(true);
    	            } });
    		}
    		else 
    		{
    			Fyber.app.runOnUiThread(new Runnable() { @Override public void run() {
    					nativeFinished(false);
    	            } });
    		}
    	}
    }

	@Override
	public void onSPBrandEngageError(String arg0) {
		intentVideo = null;
		nativeStart(false);
	}

	@Override
	public void onSPBrandEngageOffersAvailable(Intent arg0) {
		intentVideo = arg0;
		Fyber.app.runOnUiThread(new Runnable() { @Override public void run() {
			if( intentVideo != null )
			{
				Fyber.app.startActivityForResult(intentVideo, 1234);
				nativeStart(true);
				intentVideo = null;
			}
			else
			{
				nativeNoOffers();
			}
        } });
	}

	@Override
	public void onSPBrandEngageOffersNotAvailable() {
		intentVideo = null;
		nativeStart(false);
		nativeNoOffers();
	}

	@Override
	public void onSPInterstitialAdAvailable(Intent arg0) {
    	if( Fyber.me == null ) return;
    	intentInterstitial = arg0;

    	Fyber.app.runOnUiThread(new Runnable() { @Override public void run() {
    		if( intentInterstitial != null )
    		{
    			Fyber.app.startActivityForResult(intentInterstitial, 5678);
    		}
        } });    	
	}

	@Override
	public void onSPInterstitialAdError(String arg0) {
    	if( Fyber.me == null ) return;
	}

	@Override
	public void onSPInterstitialAdNotAvailable() {
    	if( Fyber.me == null ) return;
	}
    

    public native static void nativeStart( boolean result );
    public native static void nativeFinished( boolean result );
    public native static void nativeNoOffers();
    
    static public void cacheVideo() {}

    static public void cacheInterstitial() { }

    static public void playAd()
    {
    	if( Fyber.me == null ) return;
    	Fyber.app.runOnUiThread(new Runnable() { @Override public void run() {
			if( intentVideo == null )
				SponsorPayPublisher.getIntentForMBEActivity(Fyber.app, Fyber.me);
        } });
    }

    static public void showInterstitial()
    {
    	if( Fyber.me == null ) return;
    	Fyber.app.runOnUiThread(new Runnable() { @Override public void run() {
   			SponsorPayPublisher.getIntentForInterstitialActivity(Fyber.app, Fyber.me);
        } });
    }
}
