package com.stereo7.admob;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;

import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.LinearLayout.LayoutParams;

import com.google.android.gms.ads.*;

public class AdMob {

	private static AdMob me;
	private static Activity app;
	private static InterstitialAd interstitial;
    private static String interstitialID;
	
	private static Handler handler;
	private static final int SHOW_Interstitial = 1;
	private static boolean interstitialLoaded;

	public AdMob(Activity application, String interstitialID) {
		
		app = application;
		me = this;
		interstitialLoaded = false;
		AdMob.interstitialID = interstitialID;
		initIntertitial();
		
	    try{
			handler = new Handler()
	        {
	  	      public void handleMessage(android.os.Message msg) {
	  	        switch (msg.what) {
		        	case SHOW_Interstitial:
		        		if( interstitial != null )
		        		{
		        			if( interstitial.isLoaded() )
		        			{
		        				interstitialLoaded = false;
		        				interstitial.show();
		        			}
		        		}
		        		break;
	  	        }
	  	      };
	  	    };
	    } catch(Exception e) { }
	}
	
	private void initIntertitial()
	{
	    try{
			interstitial = new InterstitialAd(app);
		    interstitial.setAdUnitId(AdMob.interstitialID);
		    interstitial.setAdListener(new AdListener() {
		        @Override
		        public void onAdClosed() {
		          AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdMob.interstitialID).build();
		          interstitial.loadAd(adRequest);
		        }
		        
		        @Override public void onAdFailedToLoad( int errorCode ) 
		        {
		        	interstitialLoaded = false;
		        	Log.d("Admob", "onAdFailedToLoad");
		        }
		        @Override public void onAdLoaded() 
		        {
		        	interstitialLoaded = true;
		        	Log.d("Admob", "onAdLoaded");
		        }
		    });
		    
		    AdRequest adRequest = new AdRequest.Builder().build();
		    interstitial.loadAd(adRequest);
	    } catch(Exception e) { }
	}

	public static void showInterstitial()
	{
		if( me ==null )return;
		
		app.runOnUiThread(new Runnable() { @Override public void run() {
	    	try{
	    	handler.sendEmptyMessage(SHOW_Interstitial);
	    	} catch(Exception e) { }
        } });
	}

    static public boolean interstitialAvailabled()
    {
    	return interstitialLoaded;
    }

}
